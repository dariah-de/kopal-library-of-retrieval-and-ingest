/*                                                                           */
/* clear_kolibriDbTest.sql                                                        */
/*                                                                           */
/* Script to delete all entries of the kolibri-database (mysql), DB-name     */
/* "kolibriDbTest", please change if needed!                                 */
/*                                                                           */
/* CAUTION!!!!! The database will be empty afterwards!!!!!                   */
/*                                                                           */

/* --- 17 --- techmd ---                                                 --- */
delete from .techmd;

/* --- 16 --- fileformat ---                                             --- */
delete from kolibriDbTest.fileformat

/* --- 15 --- file ---                                                   --- */
delete from kolibriDbTest.file;

/* --- 14 --- dublincore ---                                             --- */
delete from kolibriDbTest.dublincore;

/* --- 13 --- identifier ---                                             --- */
delete from kolibriDbTest.identifier;

/* --- 12 --- various ---                                                --- */
delete from kolibriDbTest.various;

/* --- 11 --- dias ---                                                   --- */
delete from kolibriDbTest.dias;

/* --- 10 --- event ---                                                  --- */
delete from kolibriDbTest.event;

/* --- 9 --- input ---                                                   --- */
delete from kolibriDbTest.input;

/* --- 8 --- idtype ---                                                  --- */
delete from kolibriDbTest.idtype;

/* --- 7 --- dcelement ---                                               --- */
delete from kolibriDbTest.dcelement;

/* --- 6 --- description ---                                             --- */
delete from kolibriDbTest.description;

/* --- 5 --- checksumtype ---                                            --- */
delete from kolibriDbTest.checksumtype;

/* --- 4 --- modulestatus ---                                            --- */
delete from kolibriDbTest.modulestatus;

/* --- 3 --- server ---                                                  --- */
delete from kolibriDbTest.server;

/* --- 2 --- owner ---                                                   --- */
delete from kolibriDbTest.owner;

/* --- 1 --- format ---                                                  --- */
delete from kolibriDbTest.format;
