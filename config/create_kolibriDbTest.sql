/*                                                                           */
/* create_kolibriDbTest.sql (MySQL5.1.51 proved :-)                          */
/*                                                                           */
/* Script to create the koLibRI-database (mysql), DB-name "kolibriDbTest".   */
/* Create all tables incl. primary, foreign and unique keys.                 */
/* int8 is an alias for bigint and used for all date-values, all unsigned.   */
/* Tables dcelement (DublinCore-elements), checksumtype and modulestatus are */
/* filled.            
/*                                                                           */

CREATE DATABASE kolibriDbTest CHARACTER SET utf8 COLLATE utf8_general_ci;

/* --- 1 --- format ---                                                  --- */
CREATE TABLE kolibriDbTest.format (
  formatid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  format VARCHAR(255) NOT NULL DEFAULT '',
  tmdformat VARCHAR(255) NOT NULL DEFAULT '',
  tmdmimetype VARCHAR(255) NOT NULL DEFAULT '',
  tmdversion VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY(formatid),
  UNIQUE format_unique(format)
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

/* --- 2 --- owner ---                                                   --- */
CREATE TABLE kolibriDbTest.owner (
  ownerid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  owner VARCHAR(255) NOT NULL DEFAULT '',
  pass varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY(ownerid),
  UNIQUE owner_unique(owner)
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

insert into kolibriDbTest.owner (owner, pass)
values ('TEST', 'ENGAGE!');

/* --- 3 --- server ---                                                  --- */
CREATE TABLE kolibriDbTest.server (
  serverid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  server VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY(serverid),
  UNIQUE server_unique(server)
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

insert into kolibriDbTest.server (server)
values ('localhost');

/* --- 4 --- modulestatus ---                                            --- */
CREATE TABLE kolibriDbTest.modulestatus (
  modulestatusid INTEGER UNSIGNED NOT NULL,
  modulestatus VARCHAR(45) NOT NULL DEFAULT '',
  PRIMARY KEY(modulestatusid),
  UNIQUE modulestatus_unique(modulestatus)
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

insert into kolibriDbTest.modulestatus (modulestatusid, modulestatus)
values (0, 'todo'), (1, 'done'), (2, 'running'), (3, 'error'), (4, 'cancelled'), (5, 'warning');

/* --- 5 --- checksumtype ---                                            --- */
CREATE TABLE kolibriDbTest.checksumtype (
  checksumtypeid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  checksumtype VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY(checksumtypeid),
  UNIQUE checksumtype_unique(checksumtype)
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

insert into kolibriDbTest.checksumtype (checksumtype)
values ('xor of sha1 file checksums'), ('SHA-1'), ('MD5');

/* --- 6 --- description ---                                             --- */
CREATE TABLE kolibriDbTest.description (
  descriptionid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  description TEXT NOT NULL,
  PRIMARY KEY(descriptionid),
  UNIQUE description_unique (description(255))
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

/* --- 7 --- dcelement ---                                               --- */
CREATE TABLE kolibriDbTest.dcelement (
  dcelementid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  dcelement VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY(dcelementid),
  UNIQUE dcelement_unique(dcelement)
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

insert into kolibriDbTest.dcelement (dcelementid, dcelement)
values
(1, 'title'),
(2, 'creator'),
(3, 'subject'),
(4, 'description'),
(5, 'publisher'),
(6, 'contributor'),
(7, 'date'),
(8, 'type'),
(9, 'format'),
(10, 'identifier'),
(11, 'source'),
(12, 'language'),
(13, 'relation'),
(14, 'coverage'),
(15, 'rights');

/* --- 8 --- idtype ---                                                  --- */
CREATE TABLE kolibriDbTest.idtype (
  idtypeid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  idtype VARCHAR(255) NOT NULL DEFAULT '',
  text VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY(idtypeid),
  UNIQUE idtype_unique(idtype)
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

insert into kolibriDbTest.idtype (idtype, text)
values ('LPI', 'Local Persistent Identifier');

/* --- 9 --- input ---                                                   --- */
CREATE TABLE kolibriDbTest.input (
  inputid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  ownerid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  serverid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  checksumtypeid INTEGER UNSIGNED,
  checksum VARCHAR(255),
  startdate INT8 UNSIGNED NOT NULL DEFAULT 0,
  serialdata TEXT,
  PRIMARY KEY(inputid),
  UNIQUE input_unique(ownerid,serverid,checksumtypeid,checksum,startdate),
  CONSTRAINT FK_input_owner FOREIGN KEY FK_input_owner (ownerid)
    REFERENCES owner (ownerid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
 CONSTRAINT FK_input_server FOREIGN KEY FK_input_server (serverid)
    REFERENCES server (serverid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
 CONSTRAINT FK_input_checksumtype FOREIGN KEY FK_input_checksumtype (checksumtypeid)
    REFERENCES checksumtype (checksumtypeid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

/* --- 10 --- event ---                                                  --- */

CREATE TABLE kolibriDbTest.event (
  eventid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  inputid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  modulestatusid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  descriptionid INTEGER UNSIGNED NOT NULL,
  eventdate INT8 UNSIGNED NOT NULL DEFAULT 0,
  module VARCHAR(255) NOT NULL DEFAULT '',
  filename VARCHAR(255),
  PRIMARY KEY(eventid),
  CONSTRAINT FK_event_input FOREIGN KEY FK_event_input (inputid)
    REFERENCES input (inputid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT FK_event_modulestatus FOREIGN KEY FK_event_modulestatus (modulestatusid)
    REFERENCES modulestatus (modulestatusid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT FK_event_description FOREIGN KEY FK_event_description (descriptionid)
    REFERENCES description (descriptionid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

/* --- 11 --- dias ---                                                   --- */
CREATE TABLE kolibriDbTest.dias (
  diasid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  inputid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  checksumtypeid INTEGER UNSIGNED,
  diasintid VARCHAR(255),
  diasdate INT8 UNSIGNED NOT NULL DEFAULT 0,
  deleted BOOLEAN NOT NULL DEFAULT 0,
  urn VARCHAR(255) NOT NULL DEFAULT '',
  checksum_new VARCHAR(255),
  diasoldobjid VARCHAR(255),
  PRIMARY KEY(diasid),
  CONSTRAINT FK_dias_input FOREIGN KEY FK_dias_input (inputid)
    REFERENCES input (inputid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT FK_dias_checksumtype FOREIGN KEY FK_dias_checksumtype (checksumtypeid)
    REFERENCES checksumtype (checksumtypeid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

/* --- 12 --- various ---                                                --- */
CREATE TABLE kolibriDbTest.various (
  variousid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  inputid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  type VARCHAR(255) NOT NULL DEFAULT '',
  value VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY(variousid),
  UNIQUE various_unique(inputid, type, value),
  CONSTRAINT FK_various_input FOREIGN KEY FK_various_input (inputid)
    REFERENCES input (inputid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

/* --- 13 --- identifier ---                                             --- */
CREATE TABLE kolibriDbTest.identifier (
  identifierid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  inputid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  idtypeid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  idvalue VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY(identifierid),
  UNIQUE identifier_unique(inputid, idtypeid),
  CONSTRAINT FK_identifier_input FOREIGN KEY FK_identifier_input (inputid)
    REFERENCES input (inputid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT FK_identifier_idtype FOREIGN KEY FK_identifier_idtypes (idtypeid)
    REFERENCES idtype (idtypeid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

/* --- 14 --- dublincore ---                                             --- */
CREATE TABLE kolibriDbTest.dublincore (
  dublincoreid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  inputid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  dcelementid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  dcvalue TEXT NOT NULL,
  PRIMARY KEY(dublincoreid),
  CONSTRAINT FK_dublincore_input FOREIGN KEY FK_dublincore_input (inputid)
    REFERENCES input (inputid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT FK_dublincore_dcelement FOREIGN KEY FK_dublincore_dcelement (dcelementid)
    REFERENCES dcelement (dcelementid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

/* --- 15 --- file ---                                                   --- */
CREATE TABLE kolibriDbTest.file (
  fileid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  inputid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  checksumtypeid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  checksum VARCHAR(255) NOT NULL DEFAULT '',
  filedate int8 UNSIGNED NOT NULL DEFAULT 0,
  size int8 NOT NULL DEFAULT 0,
  filename VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY(fileid),
  CONSTRAINT FK_file_input FOREIGN KEY FK_file_input (inputid)
    REFERENCES input (inputid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT FK_file_checksumtype FOREIGN KEY FK_file_checksumtype (checksumtypeid)
    REFERENCES checksumtype (checksumtypeid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

/* --- 16 --- fileformat ---                                             --- */
CREATE TABLE kolibriDbTest.fileformat (
  fileformatid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  fileid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  formatid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  isembedded BOOLEAN,
  PRIMARY KEY(fileformatid),
  UNIQUE fileformat_unique(formatid, fileid),
  CONSTRAINT FK_fileformat_file FOREIGN KEY FK_fileformat_file (fileid)
    REFERENCES file (fileid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT FK_fileformat_format FOREIGN KEY FK_fileformat_format (formatid)
    REFERENCES format (formatid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;

/* --- 17 --- techmd ---                                                 --- */
CREATE TABLE kolibriDbTest.techmd (
  techmdid INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  fileid INTEGER UNSIGNED NOT NULL DEFAULT 0,
  techmd LONGTEXT NOT NULL,
  PRIMARY KEY(techmdid),
  CONSTRAINT FK_techmd_file FOREIGN KEY FK_techmd_file (fileid)
    REFERENCES file (fileid)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
)
ENGINE = InnoDB DEFAULT CHARSET = utf8;
