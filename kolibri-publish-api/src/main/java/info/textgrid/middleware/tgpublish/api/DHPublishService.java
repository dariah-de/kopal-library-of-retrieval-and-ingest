/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * SUB Göttingen (https://sub.uni-goettingen.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.tgpublish.api;

import info.textgrid.middleware.tgpublish.api.jaxb.InfoResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2020-06-25 - Funk - Add @Produces annotation to publish() method.
 * 
 * 2019-05-14 - Funk - Removed ePPN from publish() params.
 * 
 * 2019-04-09 - Funk - Added JSON MediaType for JSON responses.
 * 
 * 2019-04-02 - Funk - Made logID mandatory.
 * 
 * 2019-02-26 - Funk - Removing publish token. We only need the storage token for authentication!
 * 
 * 2017-11-20 - Funk - Added logID header X-Transaction-ID.
 * 
 * 2017-11-13 - Funk - Changed GET to POST for publish API call. Changed Auth QueryParams to
 * HeaderParams.
 * 
 * 2017-09-07 - Funk - Added publish token.
 * 
 * 2017-01-18 - Funk - Added storage token.
 * 
 * 2016-07-16 - Funk - Added updateInfo method.
 * 
 * 2016-05-10 - Funk - Removed ePPN from some calls.
 * 
 * 2015-03-18 - Funk - Added InfoType and InfoResponse.
 * 
 * 2014-12-17 - Funk - Adapted to new DH-publish API.
 * 
 * 2012-10-15 - Funk - Copied from TGPublishService.
 */

/**
 * <p>
 * <b>The DHPublishService interface</b>
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2019-05-15
 * @since 2014-10-15
 */

public interface DHPublishService {

  // **
  // STATIC FINALS
  // **

  public static final String STORAGE_ID = "uri";
  public static final String DRY_RUN = "dryRun";
  public static final String STORAGE_TOKEN = "X-Storage-Token";
  public static final String SEAFILE_TOKEN = "X-Seafile-Token";
  public static final String TRANSACTION_ID = "X-Transaction-ID";
  public static final String CAN_READ = "read";
  public static final String CAN_WRITE = "write";
  public static final String CAN_PUBLISH = "publish";

  /**
   * <p>
   * Publish an edition or collection.
   * </p>
   * 
   * @param uri The DARIAH storage ID.
   * @param dryRun
   * @param storageToken The DARIAH storage token.
   * @param seafileToken
   * @param logID
   * @return
   */
  @POST
  @Path("/{uri: .+}/publish")
  @Produces(MediaType.TEXT_PLAIN)
  public abstract Response publish(@PathParam(STORAGE_ID) String uri,
      @FormParam(DRY_RUN) @DefaultValue("false") boolean dryRun,
      @HeaderParam(STORAGE_TOKEN) String storageToken,
      @HeaderParam(SEAFILE_TOKEN) String seafileToken, @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * @param uri The DARIAH storage ID.
   * @param storageToken The DARIAH storage token.
   * @param logID The log ID to identify the process in the log files.
   * @return A publish response containing status information of all objects.
   */
  @GET
  @Path("/{uri: .+}/status")
  @Produces({MediaType.TEXT_XML, MediaType.APPLICATION_JSON})
  public abstract PublishResponse getStatus(@PathParam(STORAGE_ID) String uri,
      @HeaderParam(STORAGE_TOKEN) String storageToken, @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * @param uri The DARIAH storage ID.
   * @param storageToken The DARIAH storage token.
   * @param logID The log ID to identify the process in the log files.
   * @return A publish response containing status information of all objects.
   */
  @GET
  @Path("/{uri: .+}/ministatus")
  @Produces({MediaType.TEXT_XML, MediaType.APPLICATION_JSON})
  public abstract PublishResponse getMinistatus(@PathParam(STORAGE_ID) String uri,
      @HeaderParam(STORAGE_TOKEN) String storageToken, @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * @param uri The DARIAH storage ID.
   * @param storageToken The DARIAH storage token.
   * @param logID The log ID to identify the process in the log files.
   * @return A info response containing status information of the publish GUI publish process.
   */
  @GET
  @Path("/{uri: .+}/info")
  @Produces({MediaType.TEXT_XML, MediaType.APPLICATION_JSON})
  public abstract InfoResponse getInfo(@PathParam(STORAGE_ID) String uri,
      @HeaderParam(STORAGE_TOKEN) String storageToken, @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Returns TG-ublish's version.
   * </p>
   * 
   * @return DH-publish version string.
   */
  @GET
  @Path("/version")
  @Produces(MediaType.TEXT_PLAIN)
  public abstract String getVersion();

}
