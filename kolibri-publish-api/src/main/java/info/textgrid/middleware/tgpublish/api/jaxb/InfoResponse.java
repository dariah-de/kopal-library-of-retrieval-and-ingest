/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public (http://www.gnu.org/licenses/lgpl-3.0.txt) License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.tgpublish.api.jaxb;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * <p>
 * Just used to report status about overall publish GUI publish status.
 * </p>
 */

@XmlRootElement
public class InfoResponse {

  public @XmlAttribute InfoType status;
  public @XmlAttribute String pid;
  public @XmlAttribute String doi;
  public @XmlAttribute String crid;
  public @XmlAttribute String uri;
  public @XmlAttribute String module;
  public @XmlAttribute String task;
  public @XmlAttribute int progress;
  public @XmlAttribute String rdf;

  /**
   * <p>
   * Constructor.
   * </p>
   */
  public InfoResponse() {
    this.status = InfoType.NEW;
    this.progress = 0;
  }

  /**
   * <p>
   * Constructor.
   * </p>
   * 
   * @param theUri
   */
  public InfoResponse(String theUri) {
    this.status = InfoType.NEW;
    this.progress = 0;
    this.uri = theUri;
  }

}
