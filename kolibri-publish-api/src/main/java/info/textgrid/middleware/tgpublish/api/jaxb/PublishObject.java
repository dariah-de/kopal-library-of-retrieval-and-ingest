/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.tgpublish.api.jaxb;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

/**
 * 
 */

public class PublishObject {

  private static final String OUTSIDE_REFERENCES_WARNING_STRING =
      "There are URIs referenced in some objects, that are not part of the object to publish";

  public @XmlAttribute String uri;
  public @XmlAttribute String destUri;
  public @XmlAttribute String pid;
  public @XmlAttribute StatusType status = StatusType.OK;

  private List<PublishError> errors;
  private List<PublishWarning> warnings;
  private ReferencedUris referencedUris;

  /**
   * 
   */
  public PublishObject() {
    this.errors = new ArrayList<PublishError>();
    this.warnings = new ArrayList<PublishWarning>();
  }

  /**
   * @param uri
   */
  public PublishObject(String uri) {
    this();
    this.uri = uri;
  }

  /**
   * @param type
   * @param message
   */
  public void addError(ErrorType type, String message) {
    PublishError e = new PublishError(type, message);
    this.errors.add(e);
    changeStatus(StatusType.ERROR);
  }

  /**
   * @param errors
   */
  public void setErrors(List<PublishError> errors) {
    this.errors = errors;
    changeStatus(StatusType.ERROR);
  }

  /**
   * @return
   */
  @XmlElement(name = "error")
  public List<PublishError> getErrors() {
    return this.errors;
  }

  /**
   * @param type
   * @param message
   */
  public void addWarning(WarningType type, String message) {
    PublishWarning e = new PublishWarning(type, message);
    this.warnings.add(e);
    changeStatus(StatusType.WARNING);
  }

  /**
   * @param warnings
   */
  public void setWarnings(List<PublishWarning> warnings) {
    this.warnings = warnings;
    changeStatus(StatusType.WARNING);
  }

  /**
   * @return
   */
  @XmlElement(name = "warning")
  public List<PublishWarning> getWarnings() {
    return this.warnings;
  }

  /**
   * Change status, one way only: OK -> WANRING -> ERROR
   * 
   * @param newStatus
   */
  private void changeStatus(StatusType newStatus) {

    // if already error, nothing to be done
    if (this.status == StatusType.ERROR) {
      return;
    }

    // setting back to OK not possible
    if (newStatus == StatusType.OK) {
      return;
    }

    this.status = newStatus;
  }

  /**
   * @param referencedUris
   */
  public void setReferencedUris(ReferencedUris referencedUris) {

    // Change object status to warning.
    changeStatus(StatusType.WARNING);
    // Add the warning to the warning list.
    addWarning(WarningType.CHECK_REFERENCES, OUTSIDE_REFERENCES_WARNING_STRING);

    // Set referenced URI list.
    this.referencedUris = referencedUris;
  }

  /**
   * @return
   */
  public ReferencedUris getReferencedUris() {
    return this.referencedUris;
  }

}
