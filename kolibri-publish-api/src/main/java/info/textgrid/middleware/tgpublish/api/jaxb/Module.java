/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public (http://www.gnu.org/licenses/lgpl-3.0.txt) License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.tgpublish.api.jaxb;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

/**
 *
 */

public class Module {

  public @XmlAttribute String name;
  public @XmlAttribute StatusType status;

  private List<String> messages;

  /**
   * 
   */
  public Module() {
    this("not yet named", StatusType.OK);
  }

  /**
   * @param name
   */
  public Module(String name) {
    this(name, StatusType.OK);
  }

  /**
   * @param name
   * @param status
   */
  public Module(String name, StatusType status) {
    this.name = name;
    this.status = status;
    this.messages = new ArrayList<String>();
  }

  /**
   * @param messages
   */
  @XmlElement(name = "message")
  public void setMessages(List<String> messages) {
    this.messages = messages;
  }

  /**
   * @return
   */
  public List<String> getMessages() {
    return this.messages;
  }

  /**
   * @param message
   */
  public void addMessage(String message) {
    this.messages.add(message);
  }

}
