/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public (http://www.gnu.org/licenses/lgpl-3.0.txt) License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.tgpublish.api.jaxb;

/**
 *
 */

public enum ErrorType {

  /**
   * Error not specified.
   */
  NOT_SPECIFIED,

  /**
   * Object not existing or not authorized.
   */
  AUTH,

  /**
   * Object is no Edition or Collection, or not one of the types allowed for setting worldReadable.
   */
  WRONG_CONTENT_TYPE,

  /**
   * No right to publish object.
   */
  NO_PUBLISH_RIGHT,

  /**
   * No PID could be generated.
   */
  PID_GENERATION_FAILED,

  /**
   * required metadata field is missing.
   */
  MISSING_METADATA,

  /**
   * the object is already published
   */
  ALREADY_PUBLISHED,

  /**
   * there is a warning in the metadata
   */
  METADATA_WARNINGS_EXIST,

  /**
   * serverside error, e.g. wrong service configuration, some host not reachable, etc
   */
  SERVER_ERROR

}
