/*******************************************************************************
 * This software is copyright (c) 2014 by
 * 
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.tgpublish.api;

import java.util.List;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.WorldReadableMimetypes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

/*******************************************************************************
 * PACKAGE
 * 			info.textgrid.middleware.tgpublish.api
 * 
 * FILE
 * 			PublishService.java
 *
 *******************************************************************************
 * TODOLOG
 * 
 *******************************************************************************
 * CHANGELOG
 * 
 * 	2014-11-25	Funk	Added getMinistatus method.
 * 	2012-03-14	Funk	Added abort method.
 * 	2012-03-09	Funk	Added copy method.
 * 	2011-03-21	Funk	Changed variable dryrun to dryRun.
 * 	2011-01-05	Funk	Created file from Ubbos version.
 *
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * <b>The TGPublishService interface</b>
 * </p>
 * 
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Stefan E. Funk, DAASI International
 * @version 2014-11-25
 * @since 2011-01-04
 ******************************************************************************/

public interface TGPublishService {

	/**
	 * <p>
	 * Publishes an edition or collection.
	 * </p>
	 * 
	 * @param sid
	 *            TG-auth session ID.
	 * @param log
	 *            TextGrid log parameter.
	 * @param uri
	 *            TextGrid URI of an edition or collection to be published.
	 * @param ignoreWarnings
	 *            Publishing will happen even if warnings do exist.
	 * @param dryRun
	 *            Do everything except of really publishing.
	 * @return An HTTP response.
	 */
	@GET
	@Path("/{uri}/publish")
	@Produces("text/plain")
	public abstract Response publish(@QueryParam("sid") String sid,
			@QueryParam("log") @DefaultValue("") String log,
			@PathParam("uri") String uri,
			@QueryParam("ignoreWarnings") @DefaultValue("false") boolean ignoreWarnings,
			@QueryParam("dryRun") @DefaultValue("true") boolean dryRun);

	/**
	 * <p>
	 * Copies a bunch of TextGrid objects (recursively).
	 * </p>
	 * 
	 * @param sid
	 *            TG-auth session ID.
	 * @param log
	 *            TextGrid log parameter.
	 * @param uriList
	 *            A list of TextGrid URIs to be copied.
	 * @param projectId
	 *            The project ID of the project the objects shall be copied to.
	 * @param newRevision
	 *            Copy will create new revisions.
	 * @return An HTTP response.
	 */
	@GET
	@Path("/copy")
	@Produces("text/plain")
	public abstract Response copy(@QueryParam("sid") String sid,
			@QueryParam("log") @DefaultValue("") String log,
			@QueryParam("uri") List<String> uriList,
			@QueryParam("projectId") String projectId,
			@QueryParam("newRevision") @DefaultValue("false") boolean newRevision);

	/**
	 * <p>
	 * Publish a technical object by just setting the TG-auth' isPublic flag.
	 * </p>
	 * 
	 * @param sid
	 *            TG-auth session ID.
	 * @param log
	 *            TextGrid log parameter.
	 * @param uri
	 *            A TextGrid URI of an edition or collection to be published.
	 * @param ignoreWarnings
	 *            Publishing will happen even if warnings do exist.
	 * @param dryRun
	 *            Do everything except of really publishing.
	 * @return An HTTP response.
	 */
	@GET
	@Path("/{uri}/publishWorldReadable")
	@Produces("text/plain")
	public abstract Response publishWorldReadable(@QueryParam("sid") String sid,
			@QueryParam("log") @DefaultValue("") String log,
			@PathParam("uri") String uri,
			@QueryParam("ignoreWarnings") @DefaultValue("false") boolean ignoreWarnings,
			@QueryParam("dryRun") @DefaultValue("true") boolean dryRun);

	/**
	 * <p>
	 * Get a list of RegExps that matches all objects to be allowed to use with
	 * worldReadable.
	 * </p>
	 * 
	 * @return A list of mimetypes being able to publish world readable.
	 */
	@GET
	@Path("/listWorldReadables")
	@Produces("text/xml")
	public abstract WorldReadableMimetypes listWorldReadable();

	/**
	 * @param sid
	 *            TG-auth session ID.
	 * @param log
	 *            TextGrid log parameter.
	 * @param uri
	 *            A TextGrid URI of an edition or collection to be published or
	 *            session UUID of copy service.
	 * @return A publish response containing status information of all objects.
	 */
	@GET
	@Path("/{uri}/status")
	@Produces("text/xml")
	public abstract PublishResponse getStatus(@QueryParam("sid") String sid,
			@QueryParam("log") @DefaultValue("") String log,
			@PathParam("uri") String uri);

	/**
	 * @param sid
	 *            TG-auth session ID.
	 * @param log
	 *            TextGrid log parameter.
	 * @param uri
	 *            A TextGrid URI of an edition or collection to be published or
	 *            session UUID of copy service.
	 * @return A publish response containing status information only of the
	 *         publish status, without the list of objects.
	 */
	@GET
	@Path("/{uri}/ministatus")
	@Produces("text/xml")
	public abstract PublishResponse getMinistatus(@QueryParam("sid") String sid,
			@QueryParam("log") @DefaultValue("") String log,
			@PathParam("uri") String uri);

	/**
	 * @param sid
	 *            TG-auth session ID.
	 * @param log
	 *            TextGrid log parameter.
	 * @param uri
	 *            A TextGrid URI of an edition or collection to be published or
	 *            session UUID of copy service.
	 * @return An HTTP response.
	 */
	@GET
	@Path("/{uri}/abort")
	@Produces("text/plain")
	public abstract Response abort(@QueryParam("sid") String sid,
			@QueryParam("log") @DefaultValue("") String log,
			@PathParam("uri") String uri);

	/**
	 * <p>
	 * Returns the TG-publish service version.
	 * </p>
	 * 
	 * @return An HTTP response.
	 */
	@GET
	@Path("/version")
	@Produces("text/plain")
	public abstract Response getVersion();

	/**
	 * <p>
	 * Publishes sandbox data only. Calls TG-auth#PUBLISH, and deletes the
	 * isNearlyPublic relation from the RDF database.
	 * </p>
	 * 
	 * @param sid
	 *            TG-auth session ID.
	 * @param log
	 *            TextGrid log parameter.
	 * @param uri
	 *            A TextGrid URI of an edition or collection t o be published
	 * @return An HTTP response.
	 */
	@GET
	@Path("/{uri}/publishSandboxData")
	@Produces("text/plain")
	public abstract Response publishSandboxData(@QueryParam("sid") String sid,
			@QueryParam("log") @DefaultValue("") String log,
			@PathParam("uri") String uri);

}
