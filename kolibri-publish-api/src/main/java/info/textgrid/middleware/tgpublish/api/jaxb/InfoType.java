/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public (http://www.gnu.org/licenses/lgpl-3.0.txt) License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.tgpublish.api.jaxb;

/**
 * <p>
 * The overall info status for publish GUI.
 * </p>
 */

public enum InfoType {

  /**
   * <p>
   * Not yet published and no information available at all in DH-publish.
   * </p>
   */
  NEW,

  /**
   * <p>
   * Not yet published, but information available in DH-publish (maybe from a dry run). Please call
   * status/ministatus for more information!
   * </p>
   */
  DRAFT,

  /**
   * <p>
   * A publish process is currently running.
   * </p>
   */
  RUNNING,

  /**
   * <p>
   * Warning occurred during publish process. Please call status/ministatus for more information!
   * </p>
   */
  WARNING,

  /**
   * <p>
   * Error occurred during publish process. Please call status/ministatus for more information!
   * </p>
   */
  ERROR,

  /**
   * <p>
   * Publish process successfully completed. Collection was published in the DARIAH Repository.
   * </p>
   */
  PUBLISHED

}
