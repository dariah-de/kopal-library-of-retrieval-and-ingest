/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public (http://www.gnu.org/licenses/lgpl-3.0.txt) License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.tgpublish.api.jaxb;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlElement;

/**
 * 
 */

public class ReferencedUris {

  private List<String> uriList;

  /**
   * 
   */
  public ReferencedUris() {
    this.uriList = new ArrayList<String>();
  }

  /**
   * @param uriList
   */
  public void setUriList(List<String> uriList) {
    this.uriList = uriList;
  }

  /**
   * @param uri
   */
  public void addUri(String uri) {
    this.uriList.add(uri);
  }

  /**
   * @return
   */
  @XmlElement(name = "uri")
  public List<String> getUriList() {
    return this.uriList;
  }

}
