/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public (http://www.gnu.org/licenses/lgpl-3.0.txt) License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.tgpublish.api.jaxb;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;

/**
 * 
 */

public class PublishStatus {

  public @XmlAttribute int progress;
  public @XmlAttribute ProcessStatusType processStatus;
  public @XmlAttribute String activeModule;

  private List<Module> modules;
  private @JsonIgnore long noChangesSince;

  /**
   * 
   */
  public PublishStatus() {
    this.noChangesSince = System.currentTimeMillis();
    this.modules = new ArrayList<Module>();
  }

  /**
   * @param modules
   */
  @XmlElement(name = "module")
  public void setModules(List<Module> modules) {
    this.modules = modules;
  }

  /**
   * @return
   */
  public List<Module> getModules() {
    return this.modules;
  }

  /**
   * @param module
   */
  public void addModule(Module module) {
    this.modules.add(module);
  }

  /**
   * @param theName
   * @return
   */
  public Module getOrCreateModuleByName(String theName) {
    for (Module m : this.modules) {
      if (m.name.equals(theName)) {
        return m;
      }
    }

    return new Module(theName);
  }

  /**
   * @return
   */
  public long getNoChangesSince() {
    return this.noChangesSince;
  }

  /**
   * @param noChangesSince
   */
  public void updateNoChangesSince() {
    this.noChangesSince = System.currentTimeMillis();
  }

}
