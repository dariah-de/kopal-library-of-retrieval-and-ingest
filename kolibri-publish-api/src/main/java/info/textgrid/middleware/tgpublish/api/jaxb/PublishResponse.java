/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public (http://www.gnu.org/licenses/lgpl-3.0.txt) License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.tgpublish.api.jaxb;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 *
 */

@XmlRootElement
public class PublishResponse {

  public @XmlAttribute Boolean dryRun;
  public @XmlAttribute Boolean objectListComplete = false;

  private PublishStatus publishStatus;
  private List<PublishObject> publishObjects;

  /**
   * 
   */
  public PublishResponse() {
    this.publishStatus = new PublishStatus();
    this.publishObjects = new ArrayList<PublishObject>();
  }

  /**
   * 
   */
  public PublishResponse(boolean dryRun) {
    this.publishStatus = new PublishStatus();
    this.publishObjects = new ArrayList<PublishObject>();
    this.dryRun = new Boolean(dryRun);
  }

  /**
   * @param uri
   */
  public void addPublishObject(String uri) {
    // Only add if not yet existing.
    if (!this.publishObjectAlreadyContained(uri)) {
      this.publishObjects.add(new PublishObject(uri));
    }
  }

  /**
   * @param p
   */
  public void addPublishObject(PublishObject p) {
    // Only add if not yet existing.
    if (!this.publishObjectAlreadyContained(p.uri)) {
      this.publishObjects.add(p);
    }
  }

  /**
   * @param publishObjects
   */
  public void setPublishObjects(List<PublishObject> publishObjects) {
    this.publishObjects = publishObjects;
  }

  /**
   * @return
   */
  @XmlElement(name = "PublishObject")
  public List<PublishObject> getPublishObjects() {
    return this.publishObjects;
  }

  /**
   * @param publishStatus
   */
  @XmlElement(name = "PublishStatus")
  public void setPublishStatus(PublishStatus publishStatus) {
    this.publishStatus = publishStatus;
  }

  /**
   * @return
   */
  public PublishStatus getPublishStatus() {
    return this.publishStatus;
  }

  /**
   * @param theUri
   * @return
   */
  private boolean publishObjectAlreadyContained(String theUri) {

    for (PublishObject o : this.getPublishObjects()) {
      if (o.uri.equals(theUri)) {
        return true;
      }
    }

    return false;
  }

}
