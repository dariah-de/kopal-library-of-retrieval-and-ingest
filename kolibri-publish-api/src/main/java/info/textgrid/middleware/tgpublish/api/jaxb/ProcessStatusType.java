/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public (http://www.gnu.org/licenses/lgpl-3.0.txt) License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.tgpublish.api.jaxb;

/**
 * <p>
 * The overall process status for a publish process.
 * </p>
 */

public enum ProcessStatusType {

  /**
   * <p>
   * Publish or copy process was successfully completed. Could have been a dryRun call, too.
   * </p>
   */
  FINISHED,

  /**
   * <p>
   * Publish or copy process is running at the moment (also dryRun).
   * </p>
   */
  RUNNING,

  /**
   * <p>
   * Publish or copy process has failed. Please see process response error messages.
   * </p>
   */
  FAILED,

  /**
   * <p>
   * Publish or copy process is not queued in the published queue (anymore).
   * </p>
   */
  NOT_QUEUED,

  /**
   * <p>
   * Publish or copy process was aborted by the user.
   * </p>
   */
  ABORTED,

  /**
   * <p>
   * Publish or copy process has been queued for processing.
   * </p>
   */
  QUEUED

}
