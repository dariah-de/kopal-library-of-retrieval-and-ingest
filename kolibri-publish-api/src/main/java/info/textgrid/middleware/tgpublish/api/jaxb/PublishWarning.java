/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public (http://www.gnu.org/licenses/lgpl-3.0.txt) License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.tgpublish.api.jaxb;

/**
 *
 */

public class PublishWarning {

  private WarningType type;
  private String message;

  /**
   * 
   */
  public PublishWarning() {
    this(WarningType.NOT_SPECIFIED, "");
  }

  /**
   * @param type
   * @param message
   */
  public PublishWarning(WarningType type, String message) {
    this.type = type;
    this.message = message;
  }

  /**
   * @param type
   */
  public void setType(WarningType type) {
    this.type = type;
  }

  /**
   * @return
   */
  public WarningType getType() {
    return this.type;
  }

  /**
   * @param message
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * @return
   */
  public String getMessage() {
    return this.message;
  }

}
