/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public (http://www.gnu.org/licenses/lgpl-3.0.txt) License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.tgpublish.api.jaxb;

/**
 * 
 */

public enum StatusType {

  /**
   * <p>
   * Everything is OK.
   * </p>
   */
  OK,

  /**
   * <p>
   * Warnings do exist.
   * </p>
   */
  WARNING,

  /**
   * <p>
   * Errors do exist.
   * </p>
   */
  ERROR,

  /**
   * <p>
   * Object is not yet published.
   * </p>
   */
  NOT_YET_PUBLISHED,

  /**
   * <p>
   * Object is already published.
   * </p>
   */
  ALREADY_PUBLISHED

}
