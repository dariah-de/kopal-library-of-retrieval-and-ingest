/*******************************************************************************
 * This software is copyright (c) 2009 by
 * 
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package info.textgrid.middleware.tgpublish.api;

import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.ProcessStatusType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishStatus;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import jakarta.xml.bind.JAXB;

/*******************************************************************************
 * <p>
 * Just some API test output.
 * </p>
 * 
 * TODO Refactor into a JUnit test!
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 ******************************************************************************/

public class TestTGPublishService {

	public static String	MESSAGE	= "";

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// *********************************************************************
		MESSAGE = "EXAMPLE 1: STATUS CALL ON NOT YET PUBLISHED OBJECT";
		// *********************************************************************

		PublishResponse r = new PublishResponse();
		r.dryRun = true;
		PublishStatus s = new PublishStatus();
		PublishObject p = new PublishObject("textgrid:1");

		s.processStatus = ProcessStatusType.NOT_QUEUED;
		s.activeModule = "TGPublishServiceImpl";
		r.setPublishStatus(s);
		p.status = StatusType.NOT_YET_PUBLISHED;
		r.addPublishObject(p);

		printResponse(r);

		// *********************************************************************
		MESSAGE = "EXAMPLE 2: STATUS CALL ON ALREADY PUBLISHED OBJECT";
		// *********************************************************************

		r = new PublishResponse();
		r.dryRun = true;
		s = new PublishStatus();
		p = new PublishObject("textgrid:1");

		s.processStatus = ProcessStatusType.FINISHED;
		s.activeModule = "TGPublishServiceImpl";
		r.setPublishStatus(s);
		p.status = StatusType.ALREADY_PUBLISHED;
		r.addPublishObject(p);

		printResponse(r);

		// *********************************************************************
		MESSAGE = "EXAMPLE 3: STATUS CALL ON SUCCESSFULLY PUBLISHED OBJECT";
		// *********************************************************************

		r = new PublishResponse();
		r.dryRun = true;

		s = new PublishStatus();
		s.processStatus = ProcessStatusType.FINISHED;
		s.activeModule = "PublishComplete";
		r.setPublishStatus(s);

		p = new PublishObject("textgrid:1");
		r.addPublishObject(p);
		p = new PublishObject("textgrid:2");
		r.addPublishObject(p);
		p = new PublishObject("textgrid:3");
		r.addPublishObject(p);

		printResponse(r);

		// *********************************************************************
		MESSAGE = "EXAMPLE 4: STATUS CALL ON RUNNING PUBLICATION";
		// *********************************************************************

		r = new PublishResponse();
		r.dryRun = true;

		s = new PublishStatus();
		s.processStatus = ProcessStatusType.RUNNING;
		s.activeModule = "CheckReferences";
		r.setPublishStatus(s);

		p = new PublishObject("textgrid:1");
		r.addPublishObject(p);
		p = new PublishObject("textgrid:2");
		r.addPublishObject(p);
		p = new PublishObject("textgrid:3");
		r.addPublishObject(p);

		printResponse(r);

		// *********************************************************************
		MESSAGE = "EXAMPLE 5: STATUS CALL ON FAILED PUBLICATION";
		// *********************************************************************

		r = new PublishResponse();
		r.dryRun = true;

		s = new PublishStatus();
		s.processStatus = ProcessStatusType.FAILED;
		s.activeModule = "PublishCheckEditon";
		r.setPublishStatus(s);

		p = new PublishObject("textgrid:1");
		r.addPublishObject(p);
		p = new PublishObject("textgrid:2");
		r.addPublishObject(p);
		p = new PublishObject("textgrid:3");
		p.addError(ErrorType.MISSING_METADATA, "Some metadata are missing");
		p.status = StatusType.ERROR;
		r.addPublishObject(p);

		printResponse(r);

	}

	/**
	 * @param r
	 */
	private static void printResponse(PublishResponse r) {
		System.out.println();
		System.out.println(MESSAGE);
		for (int i = 0; i < MESSAGE.length(); i++) {
			System.out.print("-");
		}
		System.out.println();
		System.out.println();
		JAXB.marshal(r, System.out);
	}

}
