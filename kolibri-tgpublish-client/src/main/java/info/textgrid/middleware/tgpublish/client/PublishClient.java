/*******************************************************************************
 * This software is copyright (c) 2014 by
 * 
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.tgpublish.client;

import java.util.List;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import info.textgrid.middleware.tgpublish.api.TGPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.WorldReadableMimetypes;
import jakarta.ws.rs.core.Response;

/*******************************************************************************
 * <p>
 * TextGrid Publish Client Class.
 * </p>
 * 
 * @version 2014-10-25
 ******************************************************************************/

public class PublishClient {

	private String				sid;
	private String				logstring	= "";
	private TGPublishService	publish;

	/**
	 * <p>
	 * Constructor: Initialise new publish client for given tgauth sessionid.
	 * </p>
	 * 
	 * @param endpoint
	 *            http adresinfo.textgrid.middleware.tgpublish.api.jaxbs of
	 *            tgpublish endpoint
	 * @param sid
	 *            tgauth sessionid
	 */
	public PublishClient(String endpoint, String sid) {
		this.sid = sid;
		this.publish = JAXRSClientFactory.create(endpoint + "/",
				TGPublishService.class);
	}

	/**
	 * <p>
	 * Constructor: initialize new publishclient for given tgauth sessionid and
	 * tglog logstring.
	 * </p>
	 * 
	 * @param endpoint
	 *            http adress of tgpublish endpoint
	 * @param sid
	 *            tgauth sessionid
	 * @param logstring
	 *            tglog logstring
	 */
	public PublishClient(String endpoint, String sid, String logstring) {
		this(endpoint, sid);
		this.logstring = logstring;
	}

	/**
	 * <p>
	 * Publish an edition.
	 * </p>
	 * 
	 * <p>
	 * Status of publishing process can be monitored @see{getStatus}.
	 * </p>
	 * 
	 * <p>
	 * Publishing is stopped if errors or warnings found by server. Also look at @see
	 * #publishIgnoreWarnings.
	 * </p>
	 * 
	 * @param textgridUri
	 *            URI of edition to be published
	 * @return HTTP response of the TG-publish call
	 */
	public Response publish(String textgridUri) {
		return this.publish.publish(this.sid, this.logstring, textgridUri,
				false, false);
	}

	/**
	 * <p>
	 * Copy a bunch of TextGrid objects.
	 * </p>
	 * 
	 * <p>
	 * Status of copying process can be monitored @see{getStatus}.
	 * </p>
	 * 
	 * <p>
	 * Copying is stopped if errors found by server.
	 * </p>
	 * 
	 * @param textgridUris
	 *            URIs of TextGrid objects to be published (recursively)
	 * @param projectId
	 * @return HTTP response of the TG-copy call
	 */
	public Response copy(List<String> textgridUris, String projectId) {
		return this.publish.copy(this.sid, this.logstring, textgridUris,
				projectId, false);
	}

	/**
	 * <p>
	 * Copy a bunch of TextGrid objects as new revisions.
	 * </p>
	 * 
	 * <p>
	 * Status of copying process can be monitored @see{getStatus}.
	 * </p>
	 * 
	 * <p>
	 * Copying is stopped if errors found by server.
	 * </p>
	 * 
	 * @param textgridUris
	 *            URIs of TextGrid objects to be published (recursively)
	 * @param projectId
	 * @return HTTP response of the TG-copy call
	 */
	public Response copyAsNewRevisions(List<String> textgridUris,
			String projectId) {
		return this.publish.copy(this.sid, this.logstring, textgridUris,
				projectId, true);
	}

	/**
	 * <p>
	 * Publish an edition.
	 * </p>
	 * 
	 * <p>
	 * Status of publishing process can be monitored @see{getStatus}.
	 * </p>
	 * 
	 * <p>
	 * Publishing is stopped if errors are found by server. Warnings are
	 * ignored.
	 * </p>
	 * 
	 * @param textgridUri
	 *            URI of edition to be published
	 * @return HTTP response of the TG-publish call
	 */
	public Response publishIgnoreWarnings(String textgridUri) {
		return this.publish.publish(this.sid, this.logstring, textgridUri,
				true, false);
	}

	/**
	 * <p>
	 * Only simulate publish process (dry run)
	 * </p>
	 * 
	 * @param textgridUri
	 * @return HTTP response of the TG-publish call
	 */
	public Response publishSimulate(String textgridUri) {
		return this.publish.publish(this.sid, this.logstring, textgridUri,
				false, true);
	}

	/**
	 * <p>
	 * Publish object world readable only.
	 * </p>
	 * 
	 * @param textgridUri
	 * @return HTTP response of the TG-publish call
	 */
	public Response publishWorldReadable(String textgridUri) {
		return this.publish.publishWorldReadable(this.sid, this.logstring,
				textgridUri, false, false);
	}

	/**
	 * @param textgridUri
	 * @return Response object containing information about processing state
	 */
	public Response publishWorldReadableIgnoreWarnings(String textgridUri) {
		return this.publish.publishWorldReadable(this.sid, this.logstring,
				textgridUri, true, false);
	}

	/**
	 * @param textgridUri
	 * @return HTTP response of the TG-publish call
	 */
	public Response publishWorldReadableSimulate(String textgridUri) {
		return this.publish.publishWorldReadable(this.sid, this.logstring,
				textgridUri, false, true);
	}

	/**
	 * <p>
	 * Get publishing status for TextGrid URI or copy UUID.
	 * </p>
	 * 
	 * @param uri
	 * @return Response object containing information about processing state
	 */
	public PublishResponse getStatus(String uri) {
		return this.publish.getStatus(this.sid, this.logstring, uri);
	}

	/**
	 * <p>
	 * Get publishing ministatus for TextGrid URI or copy UUID (without object
	 * list).
	 * </p>
	 * 
	 * @param uri
	 * @return Response object containing information about processing state
	 */
	public PublishResponse getMinistatus(String uri) {
		return this.publish.getMinistatus(this.sid, this.logstring, uri);
	}

	/**
	 * <p>
	 * Abort copy.
	 * </p>
	 * 
	 * TODO Use for aborting publish, too?
	 * 
	 * @param uri
	 * @return HTTP response of the abort call
	 */
	public Response abort(String uri) {
		return this.publish.abort(this.sid, this.logstring, uri);
	}

	/**
	 * <p>
	 * Get world readables list.
	 * </p>
	 * 
	 * @return a list of mimetypes, that are world readable
	 */
	public WorldReadableMimetypes getWorldReadables() {
		return this.publish.listWorldReadable();
	}

	/**
	 * <p>
	 * Publishes final data from the TG-sandbox.
	 * </p>
	 * 
	 * @param textgridUri
	 * @return HTTP response of the TG-publish call
	 */
	public Response publishSandboxData(String textgridUri) {
		return this.publish.publishSandboxData(this.sid, this.logstring,
				textgridUri);
	}

}
