/*******************************************************************************
 * This software is copyright (c) 2012 by
 * 
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware;

import java.io.IOException;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.client.PublishClient;
import jakarta.ws.rs.core.Response;
import jakarta.xml.bind.JAXB;

/**
 * Hello world!
 */

public class App {

	static String	sid	= "";

	// static String sid ="";

	public static void main(String[] args) throws IOException,
			InterruptedException {

		String uri = "";
		// PublishClient pc = new PublishClient(
		// "http://textgrid-ws3.sub.uni-goettingen.de/tgpublish", sid);
		PublishClient pc = new PublishClient(
				"http://vm1/kolibri-tgpublish-service", sid);

		Response res = pc.publishSimulate(uri);

		System.out.println("res:" + res);

		/*
		 * InputStream in = (InputStream) res.getEntity(); byte buffer[] = new
		 * byte[ 4000 ];
		 * 
		 * int len = in.read( buffer, 0, 4000 ); String str = new String(
		 * buffer, 0, len );
		 * 
		 * System.out.println( str );
		 */

		for (int i = 1; i <= 20; i++) {
			Thread.sleep(1000);
			PublishResponse res2 = pc.getStatus(uri);
			JAXB.marshal(res2, System.out);
		}
	}

}
