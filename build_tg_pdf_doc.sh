#!/bin/bash

# create tgpublish pdf
cd kolibri-tgpublish-service/docs;
make clean latexpdf;
cp -f _build/latex/tgpublish_doc.pdf ../..
cd ../..

# create tgimport pdf
cd kolibri-addon-textgrid-import/docs;
make clean latexpdf;
cp -f _build/latex/tgimport_doc.pdf ../..
cd ../..
