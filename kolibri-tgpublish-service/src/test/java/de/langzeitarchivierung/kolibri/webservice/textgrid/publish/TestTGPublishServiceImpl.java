/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.webservice.textgrid.publish;


import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import jakarta.xml.bind.JAXB;

/**
 * <p>
 * Tests for getting the status responses right.
 * </p>
 * 
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-03-11
 * @since 2024-03-11
 */

public class TestTGPublishServiceImpl {

  private static final String RESPONSE_NULL_NOT_PUBLISHED =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<publishResponse objectListComplete=\"false\">\n"
          + "    <PublishObject uri=\"textgrid:abcd.0\" status=\"NOT_YET_PUBLISHED\"/>\n"
          + "    <PublishStatus progress=\"0\" processStatus=\"NOT_QUEUED\" activeModule=\"de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl\"/>\n"
          + "</publishResponse>";
  private static final String RESPONSE_NULL_ALREADY_PUBLISHED =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<publishResponse objectListComplete=\"false\">\n"
          + "    <PublishObject uri=\"textgrid:abcd.0\" status=\"ALREADY_PUBLISHED\"/>\n"
          + "    <PublishStatus progress=\"0\" processStatus=\"FINISHED\" activeModule=\"de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl\"/>\n"
          + "</publishResponse>";
  private static final String RESPONSE_FAILED_OBJECT_LIST_COMPLETE =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<publishResponse dryRun=\"true\" objectListComplete=\"true\">\n"
          + "    <PublishObject uri=\"textgrid:abcd.0\" status=\"ERROR\">\n"
          + "        <error>\n"
          + "            <message>Content type of [textgrid:abcd.0] is not publishable (text/tg.aggregation+xml)! Needs to be text/tg.edition+tg.aggregation+xml or text/tg.collection+tg.aggregation+xml!</message>\n"
          + "            <type>WRONG_CONTENT_TYPE</type>\n"
          + "        </error>\n"
          + "    </PublishObject>\n"
          + "    <PublishStatus progress=\"10\" processStatus=\"FAILED\" activeModule=\"de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish.PublishCheckEdition\"/>\n"
          + "</publishResponse>";
  private static final String RESPONSE_MAP_OBJECT_LIST_INCOMPLETE =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<publishResponse dryRun=\"true\" objectListComplete=\"false\">\n"
          + "    <PublishObject uri=\"textgrid:abcd.0\" status=\"OK\">\n"
          + "    </PublishObject>\n"
          + "    <PublishObject uri=\"textgrid:wxyz.0\" status=\"OK\">\n"
          + "    </PublishObject>\n"
          + "    <PublishObject uri=\"textgrid:opqr.0\" status=\"OK\">\n"
          + "    </PublishObject>\n"
          + "    <PublishStatus progress=\"10\" processStatus=\"RUNNING\" activeModule=\"de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish.PublishCheckEdition\"/>\n"
          + "</publishResponse>";
  private static final String RESPONSE_OBJECT_LIST_INCOMPLETE =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
          + "<publishResponse dryRun=\"true\" objectListComplete=\"false\">\n"
          + "    <PublishObject uri=\"textgrid:abcd.0\" status=\"OK\"/>\n"
          + "    <PublishStatus progress=\"10\" processStatus=\"RUNNING\" activeModule=\"de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish.PublishCheckEdition\"/>\n"
          + "</publishResponse>";

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  /**
   * <p>
   * Tests the status response with empty response (null) and not yet published.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testGetStatusNullResponseNotPublished() throws IOException {

    String uri = "textgrid:abcd.0";
    boolean alreadyPublished = false;
    PublishResponse oldResponse = null;
    String expectedResponse = RESPONSE_NULL_NOT_PUBLISHED;

    PublishResponse response =
        TGPublishServiceImpl.getCurrentPublishResponse(uri, oldResponse, alreadyPublished);

    String stringResponse = "";
    try (StringWriter writer = new StringWriter()) {
      JAXB.marshal(response, writer);
      stringResponse = writer.toString();
      writer.close();

      boolean likeTheResult = expectedResponse.trim().equals(stringResponse.trim());
      if (!likeTheResult) {
        System.out.println("RESPONSE  -->");
        System.out.println(stringResponse);
        System.out.println("EXPECTED RESPONSE  -->");
        System.out.println(expectedResponse);
        assertTrue(likeTheResult);
      }
    }
  }


  /**
   * <p>
   * Tests the status response with empty response (null) and already published.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testGetStatusNullResponseAlreadyPublished() throws IOException {

    String uri = "textgrid:abcd.0";
    boolean alreadyPublished = true;
    PublishResponse oldResponse = null;
    String expectedResponse = RESPONSE_NULL_ALREADY_PUBLISHED;

    PublishResponse response =
        TGPublishServiceImpl.getCurrentPublishResponse(uri, oldResponse, alreadyPublished);

    String stringResponse = "";
    try (StringWriter writer = new StringWriter()) {
      JAXB.marshal(response, writer);
      stringResponse = writer.toString();
      writer.close();

      boolean likeTheResult = expectedResponse.trim().equals(stringResponse.trim());
      if (!likeTheResult) {
        System.out.println("RESPONSE  -->");
        System.out.println(stringResponse);
        System.out.println("EXPECTED RESPONSE  -->");
        System.out.println(expectedResponse);
        assertTrue(likeTheResult);
      }
    }
  }

  /**
   * <p>
   * Tests the status response with existing response in response map and object list is complete.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testGetStatusObjectListComplete() throws IOException {

    String uri = "textgrid:abcd.0";
    boolean alreadyPublished = false;
    // Must be exactly the same as expected answer, because the expected answer is what we get from
    // the response map here!
    PublishResponse oldResponse =
        JAXB.unmarshal(new StringReader(RESPONSE_FAILED_OBJECT_LIST_COMPLETE),
            PublishResponse.class);
    String expectedResponse = RESPONSE_FAILED_OBJECT_LIST_COMPLETE;

    PublishResponse response =
        TGPublishServiceImpl.getCurrentPublishResponse(uri, oldResponse, alreadyPublished);

    String stringResponse = "";
    try (StringWriter writer = new StringWriter()) {
      JAXB.marshal(response, writer);
      stringResponse = writer.toString();
      writer.close();

      boolean likeTheResult = expectedResponse.trim().equals(stringResponse.trim());
      if (!likeTheResult) {
        System.out.println("RESPONSE  -->");
        System.out.println(stringResponse);
        System.out.println("EXPECTED RESPONSE  -->");
        System.out.println(expectedResponse);
        assertTrue(likeTheResult);
      }
    }
  }

  /**
   * <p>
   * Tests the status response with existing response in response map and object list is incomplete.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testGetStatusObjectListIncomplete() throws IOException {

    String uri = "textgrid:abcd.0";
    boolean alreadyPublished = false;
    PublishResponse oldResponse =
        JAXB.unmarshal(new StringReader(RESPONSE_MAP_OBJECT_LIST_INCOMPLETE),
            PublishResponse.class);
    String expectedResponse = RESPONSE_OBJECT_LIST_INCOMPLETE;

    PublishResponse response =
        TGPublishServiceImpl.getCurrentPublishResponse(uri, oldResponse, alreadyPublished);

    String stringResponse = "";
    try (StringWriter writer = new StringWriter()) {
      JAXB.marshal(response, writer);
      stringResponse = writer.toString();
      writer.close();

      boolean likeTheResult = expectedResponse.trim().equals(stringResponse.trim());
      if (!likeTheResult) {
        System.out.println("RESPONSE  -->");
        System.out.println(stringResponse);
        System.out.println("EXPECTED RESPONSE  -->");
        System.out.println(expectedResponse);
        assertTrue(likeTheResult);
      }
    }
  }

}
