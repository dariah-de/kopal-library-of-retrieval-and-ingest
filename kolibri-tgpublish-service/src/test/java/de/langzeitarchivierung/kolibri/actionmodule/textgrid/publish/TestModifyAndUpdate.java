/**
 * This software is copyright (c) 2021 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import static org.junit.Assert.assertEquals;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import javax.xml.stream.XMLStreamException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import de.langzeitarchivierung.kolibri.util.FileUtils;
import info.textgrid._import.RewriteMethod;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter;
import info.textgrid.utils.linkrewriter.ImportMapping;

/**
 * <p>
 * JUnit test class for testing the TextGrid ActionModule ModifyAndUpdate.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-09-17
 * @since 2011-01-18
 */

public class TestModifyAndUpdate {

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {}

  /**
   * <p>
   * Tests the XML link rewriter.
   * </p>
   * 
   * @throws FileNotFoundException
   * @throws IOException
   * @throws XMLStreamException
   * @throws URISyntaxException
   */
  @Test
  public void testXmlLinkRewriterEditionData()
      throws FileNotFoundException, IOException, XMLStreamException, URISyntaxException {

    // Create data from file.
    URL resource = getClass().getClassLoader().getResource("edition.xml");
    String resourceString = FileUtils.readFile(new File(resource.toURI()));
    InputStream resourceStream =
        new ByteArrayInputStream(resourceString.getBytes(Charset.forName("UTF-8")));

    // Rewrite first to get correct syntax string.
    ImportMapping mapping = new ImportMapping();
    ConfigurableXMLRewriter metadataRewriter = new ConfigurableXMLRewriter(mapping, false);
    metadataRewriter.configure(URI.create(ModifyAndUpdate.MODE_AGGREGATION_REWRITE));
    ByteArrayOutputStream firstAgg = new ByteArrayOutputStream();
    metadataRewriter.rewrite(resourceStream, firstAgg);

    System.out.println("Starting aggregation:");
    System.out.println(firstAgg.toString("UTF-8"));

    // Rewrite.
    mapping = new ImportMapping();
    mapping.add(URI.create("pid:about.0"), URI.create("textgrid:about.0"), null, RewriteMethod.XML);
    mapping.add(URI.create("pid:uri1.0"), URI.create("textgrid:uri1.0"), null, RewriteMethod.XML);
    mapping.add(URI.create("pid:uri2.0"), URI.create("textgrid:uri2.0"), null, RewriteMethod.XML);
    metadataRewriter = new ConfigurableXMLRewriter(mapping, false);
    metadataRewriter.configure(URI.create(ModifyAndUpdate.MODE_AGGREGATION_REWRITE));
    ByteArrayOutputStream secondAgg = new ByteArrayOutputStream();
    metadataRewriter.rewrite(new ByteArrayInputStream(firstAgg.toByteArray()), secondAgg);

    System.out.println("Rewritten aggregation:");
    System.out.println(secondAgg.toString("UTF-8"));

    // Rewrite back.
    mapping = new ImportMapping();
    mapping.add(URI.create("textgrid:about.0"), URI.create("pid:about.0"), null, RewriteMethod.XML);
    mapping.add(URI.create("textgrid:uri1.0"), URI.create("pid:uri1.0"), null, RewriteMethod.XML);
    mapping.add(URI.create("textgrid:uri2.0"), URI.create("pid:uri2.0"), null, RewriteMethod.XML);
    metadataRewriter = new ConfigurableXMLRewriter(mapping, false);
    metadataRewriter.configure(URI.create(ModifyAndUpdate.MODE_AGGREGATION_REWRITE));
    ByteArrayOutputStream thirdAgg = new ByteArrayOutputStream();
    metadataRewriter.rewrite(new ByteArrayInputStream(secondAgg.toByteArray()), thirdAgg);

    System.out.println("Re-rewritten aggregation:");
    System.out.println(thirdAgg.toString("UTF-8"));

    assertEquals(firstAgg.toString("UTF-8"), thirdAgg.toString("UTF-8"));

    firstAgg.close();
    secondAgg.close();
    thirdAgg.close();
  }

  /**
   * <p>
   * Tests the XML link rewriter.
   * </p>
   * 
   * @throws FileNotFoundException
   * @throws IOException
   * @throws XMLStreamException
   * @throws URISyntaxException
   */
  @Test
  public void testXmlLinkRewriterEditionMetadata()
      throws FileNotFoundException, IOException, XMLStreamException, URISyntaxException {

    // Create data from file.
    URL resource = getClass().getClassLoader().getResource("edition.meta.xml");
    String resourceString = FileUtils.readFile(new File(resource.toURI()));
    InputStream resourceStream =
        new ByteArrayInputStream(resourceString.getBytes(Charset.forName("UTF-8")));

    // Rewrite first to get correct syntax string.
    ImportMapping mapping = new ImportMapping();
    ConfigurableXMLRewriter metadataRewriter = new ConfigurableXMLRewriter(mapping, false);
    metadataRewriter.configure(URI.create(ModifyAndUpdate.MODE_METADATA_REWRITE));
    ByteArrayOutputStream firstAgg = new ByteArrayOutputStream();
    metadataRewriter.rewrite(resourceStream, firstAgg);

    System.out.println("Starting aggregation:");
    System.out.println(firstAgg.toString("UTF-8"));

    // Rewrite.
    mapping = new ImportMapping();
    mapping.add(URI.create("textgrid:3tmgx.0"), URI.create("textgrid:3tmgx"), null,
        RewriteMethod.XML);
    metadataRewriter = new ConfigurableXMLRewriter(mapping, false);
    metadataRewriter.configure(URI.create(ModifyAndUpdate.MODE_METADATA_REWRITE));
    ByteArrayOutputStream secondAgg = new ByteArrayOutputStream();
    metadataRewriter.rewrite(new ByteArrayInputStream(firstAgg.toByteArray()), secondAgg);

    System.out.println("Rewritten aggregation:");
    System.out.println(secondAgg.toString("UTF-8"));

    // Rewrite back.
    mapping = new ImportMapping();
    mapping.add(URI.create("textgrid:3tmgx"), URI.create("textgrid:3tmgx.0"), null,
        RewriteMethod.XML);
    metadataRewriter = new ConfigurableXMLRewriter(mapping, false);
    metadataRewriter.configure(URI.create(ModifyAndUpdate.MODE_METADATA_REWRITE));
    ByteArrayOutputStream thirdAgg = new ByteArrayOutputStream();
    metadataRewriter.rewrite(new ByteArrayInputStream(secondAgg.toByteArray()), thirdAgg);

    System.out.println("Re-rewritten aggregation:");
    System.out.println(thirdAgg.toString("UTF-8"));

    assertEquals(firstAgg.toString("UTF-8"), thirdAgg.toString("UTF-8"));

    // ObjectType tgObjectMetadata = JAXB
    // .unmarshal(new ByteArrayInputStream(thirdAgg.toString("UTF-8").getBytes()),
    // ObjectType.class);
    // JAXB.marshal(tgObjectMetadata, System.out);

    firstAgg.close();
    secondAgg.close();
    thirdAgg.close();
  }

}
