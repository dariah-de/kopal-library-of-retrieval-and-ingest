///*******************************************************************************
// * This software is copyright (c) 2019 by
// * 
// * TextGrid Consortium (http://www.textgrid.de)
// * 
// * DAASI International GmbH (http://www.daasi.de)
// *
// * This is free software. You can redistribute it and/or modify it under the terms described in the
// * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
// * download it from
// *
// * http://www.gnu.org/licenses/lgpl-3.0.txt
// *
// * @copyright TextGrid Consortium (http://www.textgrid.de)
// * @copyright DAASI International GmbH (http://www.daasi.de)
// * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
// * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
// * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
// * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
// ******************************************************************************/
//
//package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;
//
//import static org.junit.Assert.assertTrue;
//import java.io.FileNotFoundException;
//import java.net.URI;
//import java.net.URISyntaxException;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
//import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
//import jakarta.xml.bind.JAXB;
//
///*******************************************************************************
// * <p>
// * Tests for checking the usage of the eXist response for references, that are not contained in the
// * edition/collection to publish.
// * </p>
// * 
// * @version 2019-12-05
// * @since 2011-01-18
// ******************************************************************************/
//
//public class TestCheckReferences {
//
//  /**
//   * @throws Exception
//   */
//  @BeforeClass
//  public static void setUpBeforeClass() throws Exception {}
//
//  /**
//   * @throws java.lang.Exception
//   */
//  @AfterClass
//  public static void tearDownAfterClass() throws Exception {}
//
//  /**
//   * @throws java.lang.Exception
//   */
//  @Before
//  public void setUp() throws Exception {}
//
//  /**
//   * @throws java.lang.Exception
//   */
//  @After
//  public void tearDown() throws Exception {}
//
//  /**
//   * <p>
//   * Tests the references response.
//   * </p>
//   * 
//   * @throws URISyntaxException
//   * @throws FileNotFoundException
//   */
//  @Test
//  public void testReferenceListResponse() throws URISyntaxException,
//      FileNotFoundException {
//
//    // Get publish response object from file.
//    URI resource = getClass().getClassLoader().getResource("publishResponse.xml").toURI();
//    PublishResponse response = JAXB.unmarshal(resource, PublishResponse.class);
//
//    System.out.println("Publish response objects:");
//    for (PublishObject o : response.getPublishObjects()) {
//      System.out.println("\t" + o.uri);
//    }
//
//    // Get the reference list publish response object from file.
//    resource = getClass().getClassLoader().getResource("referenceListResponse.xml").toURI();
//    PublishResponse references = JAXB.unmarshal(resource, PublishResponse.class);
//
//    // If response is empty, fail test!
//    if (references.getPublishObjects().isEmpty()) {
//      assertTrue(false);
//    }
//
//    System.out.println("Reference list objects:");
//    for (PublishObject o : references.getPublishObjects()) {
//      System.out.println("\t" + o.uri + ": " + o.getReferencedUris().getUriList());
//    }
//
//    // Merge the two lists using the method in class CheckReferences.
//    CheckReferences cm = new CheckReferences();
//    cm.mergeLists(response, references);
//
//    // Output response.
//    System.out.println("Merged response:");
//    JAXB.marshal(response, System.out);
//  }
//
//}
