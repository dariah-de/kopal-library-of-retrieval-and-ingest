/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/*******************************************************************************
 * <p>
 * Tests for checking the usage of the content type regexps.
 * </p>
 * 
 * 
 * TODO Implement!!
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2019-12-05
 * @since 2011-03-22
 ******************************************************************************/

public class TestWorldReadableCheck {

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {}

  /**
   * <p>
   * Tests the referenced response.
   * </p>
   * 
   * @throws URISyntaxException
   * @throws FileNotFoundException
   */
  @Test
  public void testContentTypeRegexps() {
    // TODO
  }

}
