/**
 * This software is copyright (c) 2024 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 **/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.util.HashMap;
import java.util.List;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.publish.TGPublishWorldReadable;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

/**
 * <p>
 * Checks if the given URI really is a WorldReadable type.
 * </p>
 *
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-09-02
 * @since 2011-01-11
 **/

public class PublishCheckWorldReadable extends PublishCheck {

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Checks if a WorldReadable object is suitable for being published, this check includes:
   * </p>
   *
   * <ol>
   * <li>loading the data and metadata for the incoming object</li>
   * <li>is it WorldReadable?</li>
   * <li>does the user have rights to publish the object</li>
   * </ol>
   * </p>
   *
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    this.uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
    this.sid = (String) customData.get(TGPublishServiceImpl.SESSION_ID);
    this.log = (String) customData.get(TGPublishServiceImpl.LOG_PARAMETER);
    this.response = (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);
    this.dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);

    // Start log entry.
    String message = "Checking if content type publishable [" + this.uri + "]";
    PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);

    // Reference on requested object is hold to change state later if necessary.
    this.incomingObject = new PublishObject(this.uri);
    this.response.addPublishObject(this.incomingObject);

    Response searchResponse =
        TGPublishAbs.getTgsearch().infoQuery().setSid(this.sid).getMetadata(this.uri);

    if (searchResponse.getResult().size() < 1) {
      finishWithError(this.incomingObject, ErrorType.AUTH,
          "URI not existing or access denied [" + this.uri + "]");
      return;
    }

    ResultType result1 = searchResponse.getResult().get(0);

    // // Check if object is already published.
    // if (alreadyPublished(this.uri)) {
    // // Get current publish object, and set status.
    // this.response.getPublishObjects().get(0).status = StatusType.ALREADY_PUBLISHED;
    //
    // PublishUtils.setStatusError(this.step, "Object has already been published", this.response,
    // this, this.dryRun);
    //
    // // Update response and return.
    // PublishUtils.updateResponse(this.uri, this.response);
    // return;
    // } else {
    // PublishUtils.setStatusRunning(this.step, "Object has not yet been published", this.response,
    // this, this.dryRun);
    // }

    // Check content type.
    String contentType = result1.getObject().getGeneric().getProvided().getFormat();

    message = "Content type is: " + contentType;
    PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);

    // Check all the mimetypes from listWorldReadable.
    List<String> contentTypeRegexps = TGPublishWorldReadable.getMimetypeRegExps();

    boolean contentTypeMatch = false;
    for (String s : contentTypeRegexps) {
      if (contentType.matches(s)) {
        contentTypeMatch = true;
        message = "Content type matches regexp: " + s;
        PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);
      }
    }

    if (!contentTypeMatch) {
      finishWithError(this.incomingObject, ErrorType.WRONG_CONTENT_TYPE,
          "Content type mismatch! Must be one of: " + contentTypeRegexps);
      return;
    }

    // Check if warnings are existing in the metadata.
    if (checkWarnings(result1)) {
      return;
    }

    // Finally check all publish rights for the object only (skip in dry-run!)
    if (!this.dryRun) {
      checkPublishRights();
    }

    // Set objectListComplete flag for flagging the list is ready to deliver publish status now
    // (means no objects are added anymore to the list!).
    this.response.objectListComplete = true;

    // Call finish before leaving go().
    finish();
  }

}
