/**
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 **/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.util.HashMap;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.TGPublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;

/**
 * <p>
 * Unlocks all objects again that have been published.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2019-03-19
 * @since 2011-01-19
 **/

public class UnlockObjects implements ActionModule {

  // **
  // STATE
  // **

  ProcessData processData;
  Step step;

  // **
  // CREATION
  // **

  public UnlockObjects() {
    super();
  }

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Unlocks every URI contained in the PublishObject list.
   * </p>
   * 
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>A filled list of PublishObjects set in the PublishResponse of the customData.</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>All URIs are unlocked afterwards.</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String sid = (String) customData.get(TGPublishServiceImpl.SESSION_ID);
    String log = (String) customData.get(TGPublishServiceImpl.LOG_PARAMETER);
    boolean dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
    PublishResponse response =
        (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

    // Start log entry.
    String message = "Unlocking all objects";
    PublishUtils.setStatusRunning(this.step, message, response, this, dryRun);

    // Do unlock all objects.
    if (!dryRun) {
      TGPublishUtils.unlock(sid, log, response);
    }

    // Set module to status DONE.
    // TODO Hopefully because we are ignoring all exceptions thrown, i.e. public objects cannot be
    // unlocked!).
    message = "Successfully unlocked published objects (hopefully :-)";
    PublishUtils.setStatusDone(this.step, message, response, this, dryRun);
  }

  // **
  // GET & SET METHODS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

}
