/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 **/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.util.HashMap;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.TGPublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;

/**
 * <p>
 * Moves all the data and metadata files to the static GRID storage.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-11-11
 * @since 2011-02-02
 **/

public class MoveToStaticGridStorage implements ActionModule {

  // **
  // STATE
  // **

  private Step step;
  private ProcessData processData;
  private boolean dryRun;
  private String tgcrudSecret;
  private boolean anyError = false;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Move data to static GRID storage.
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
    String log = (String) customData.get(TGPublishServiceImpl.LOG_PARAMETER);
    this.dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
    PublishResponse response =
        (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

    // Start log entry.
    String message = "Moving metadata and data files to the static GRID storage";
    PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

    // Get tgcrud stub.
    TGCrudService tgcrudMTOM = TGPublishAbs.getTgcrud();

    // Go through all objects and move data.
    for (PublishObject o : response.getPublishObjects()) {

      String loguri = "[" + o.uri + "] ";

      // Do TG-crud#MOVEPUBLIC (only if not dryRun and not yet published!).
      if (!this.dryRun && !o.status.equals(StatusType.ALREADY_PUBLISHED)) {
        try {
          tgcrudMTOM.movePublic(this.tgcrudSecret, log, o.uri);
        } catch (ObjectNotFoundFault e) {
          message = loguri + ": " + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
          o.addError(ErrorType.NOT_SPECIFIED, message);
          this.anyError = true;
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
        } catch (IoFault e) {
          message = loguri + ": " + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
          o.addError(ErrorType.NOT_SPECIFIED, message);
          message = loguri + " " + e.getMessage();
          this.anyError = true;
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
        } catch (AuthFault e) {
          message = loguri + ": " + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
          o.addError(ErrorType.AUTH, message);
          message = loguri + " " + e.getMessage();
          this.anyError = true;
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
        }

        message = loguri + "TG-crud#MOVESTATIC complete";
        PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
      } else {
        String alreadyPublished =
            (o.status.equals(StatusType.ALREADY_PUBLISHED) ? " [ALREADY PUBLISHED]" : "");
        message = loguri + "TG-crud#MOVESTATIC skipped" + alreadyPublished;
        PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
      }
    }

    // Check if errors occurred.
    if (this.anyError) {
      message = "Moving GRID data failed";
      PublishUtils.setStatusError(this.step, message, response, this, this.dryRun);
    } else {
      message = "Moving GRID data complete";
      PublishUtils.setStatusDone(this.step, message, response, this, this.dryRun);
    }

    // Finally write response to the response.
    PublishUtils.updateResponse(uri, response);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param secret
   */
  public void setTgcrudSecret(String secret) {
    this.tgcrudSecret = secret;
  }

}
