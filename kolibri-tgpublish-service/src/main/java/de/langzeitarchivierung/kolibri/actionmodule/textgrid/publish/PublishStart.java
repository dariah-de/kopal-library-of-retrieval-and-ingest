/*******************************************************************************
 * This software is copyright (c) 2015 by
 * 
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.util.HashMap;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;

/*******************************************************************************
 * <p>
 * Just sets the status DONE and marks publishing started.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2015-01-20
 * @since 2012-03-13
 ******************************************************************************/

public class PublishStart implements ActionModule {

	// **
	// STATE
	// **

	ProcessData	processData;
	Step		step;

	// **
	// CREATION
	// **

	public PublishStart() {
		super();
	}

	// MANIPULATION (Manipulation - what the object does) ******************

	/**
	 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
	 */
	public void go() {

		// Get the data out of the custom data.
		HashMap<Object, Object> customData = this.processData.getCustomData();

		String uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
		Boolean dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
		PublishResponse response = (PublishResponse) customData
				.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

		// Set module to status DONE.
		String message = "PUBLISHING PROCESS STARTED!!";
		PublishUtils.setStatusDone(this.step, message, response, this, dryRun);

		// Finally write response to the response.
		PublishUtils.updateResponse(uri, response);
	}

	// **
	// GET & SET METHODS
	// **

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
	 * (de.langzeitarchivierung.kolibri.ProcessData)
	 */
	public void setProcessData(ProcessData processData) {
		this.processData = processData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
	 * langzeitarchivierung.kolibri.Step)
	 */
	public void setStep(Step step) {
		this.step = step;
	}

}
