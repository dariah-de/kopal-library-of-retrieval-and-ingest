/**
 * This software is copyright (c) 2015 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.HashMap;
import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpStatus;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import info.textgrid.utils.sesameclient.SesameClient;

/**
 * <p>
 * Copies the relation data from the RDF database to the 2nd RDF database.
 * </p>
 * 
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Stefan E. Funk, DAASI International GmbH
 */

public class CopyRelationData implements ActionModule {

  // **
  // STATIC FINAL
  // **

  private static final String UNKNOWN_REPOSITORY = "Unknown repository:";
  private static final String UNKNOWN_REPOSITORY_ERROR =
      "Unknown Sesame repository! Please check CopyRelationData module configuration!";

  // **
  // STATE
  // **

  private Step step;
  private ProcessData processData;
  private boolean dryRun;
  private String sesameReadHost;
  private String sesameReadUser;
  private String sesameReadPassword;
  private String sesameWriteHost;
  private String sesameWriteUser;
  private String sesameWritePassword;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Just takes the 1st relation data and copies all new data to the 2nd index.
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
    this.dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
    PublishResponse response =
        (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

    // Start log entry.
    String message = "Copying non-public Sesame index to public Sesame instance";
    PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

    // Copy relational data here.
    SesameClient sesameRead = null;
    SesameClient sesameWrite = null;
    try {
      sesameRead =
          new SesameClient(this.sesameReadHost, this.sesameReadUser, this.sesameReadPassword);

      message = "Created Sesame client to read from: " + sesameRead.getEndpoint();
      PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

      sesameWrite =
          new SesameClient(this.sesameWriteHost, this.sesameWriteUser, this.sesameWritePassword);

      message = "Created Sesame client to write to: " + sesameWrite.getEndpoint();
      PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

      // Loop.
      for (PublishObject o : response.getPublishObjects()) {

        String logPrefix = "[" + o.uri + "] ";
        String queryStringS = "CONSTRUCT { <" + o.uri + "> ?p ?o } WHERE { <" + o.uri + "> ?p ?o }";
        String queryStringO =
            "CONSTRUCT { ?s ?p <" + o.uri + "> }" + " WHERE { ?s ?p <" + o.uri + "> }";

        // Only if object is not already published!
        if (!o.status.equals(StatusType.ALREADY_PUBLISHED)) {

          // READ subject relation data.
          message = logPrefix + "READ subject query: " + queryStringS;
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

          InputStream res = sesameRead.constructN3(queryStringS);
          String n3 = IOUtils.readStringFromStream(res).trim();
          if (n3.contains(UNKNOWN_REPOSITORY)) {
            throw new IOException(UNKNOWN_REPOSITORY_ERROR);
          }

          String count[] = n3.split("textgrid:");
          int c = count.length / 2;
          message = logPrefix + "Found " + c + " relation" + (c != 1 ? "s" : "")
              + " where URI is subject";
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

          // WRITE subject relation data.
          if (!this.dryRun) {
            int statusCode = sesameWrite.uploadN3(new ByteArrayInputStream(n3.getBytes()));

            PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

            if (statusCode != HttpStatus.SC_NO_CONTENT) {
              throw new IOException(UNKNOWN_REPOSITORY_ERROR);
            }

            message = logPrefix + "Copied " + c + " relation" + (c != 1 ? "s" : "") + " (s): "
                + statusCode;
            PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
          }

          // READ object relation data.
          message = logPrefix + "READ object query: " + queryStringO;
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

          InputStream res2 = sesameRead.constructN3(queryStringO);
          n3 = IOUtils.readStringFromStream(res2).trim();
          if (n3.contains(UNKNOWN_REPOSITORY)) {
            throw new IOException(UNKNOWN_REPOSITORY_ERROR);
          }

          count = n3.split("textgrid:");
          c = count.length / 2;
          message =
              logPrefix + "Found " + c + " relation" + (c != 1 ? "s" : "") + " where URI is object";
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

          // WRITE object relation data.
          if (!this.dryRun) {
            int statusCode = sesameWrite.uploadN3(new ByteArrayInputStream(n3.getBytes()));

            PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

            if (statusCode != HttpStatus.SC_NO_CONTENT) {
              throw new IOException(UNKNOWN_REPOSITORY_ERROR);
            }

            message = logPrefix + "Copied " + c + " relation" + (c != 1 ? "s" : "") + " (o): "
                + statusCode;
            PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
          }
        }

        // If already published, log only.
        else {
          message = "URI is already published and will not be copied";
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
        }
      }

      // Everything went well, set status DONE!
      message = "Relation data copy complete";
      PublishUtils.setStatusDone(this.step, message, response, this, this.dryRun);

    } catch (MalformedURLException e) {
      message = "Error creating Sesame client: " + e.getMessage();
      PublishUtils.setStatusError(this.step, message, response, this, this.dryRun);
    } catch (IOException e) {
      message = "Error querying Sesame client: " + e.getMessage();
      PublishUtils.setStatusError(this.step, message, response, this, this.dryRun);
    }

    // Finally write response to the response.
    PublishUtils.updateResponse(uri, response);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   *
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   *
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param host
   */
  public void setSesameReadHost(String host) {
    this.sesameReadHost = host;
  }

  /**
   * @param user
   */
  public void setSesameReadUser(String user) {
    this.sesameReadUser = user;
  }

  /**
   * @param pw
   */
  public void setSesameReadPassword(String pw) {
    this.sesameReadPassword = pw;
  }

  /**
   * @param host
   */
  public void setSesameWriteHost(String host) {
    this.sesameWriteHost = host;
  }

  /**
   * @param user
   */
  public void setSesameWriteUser(String user) {
    this.sesameWriteUser = user;
  }

  /**
   * @param pw
   */
  public void setSesameWritePassword(String pw) {
    this.sesameWritePassword = pw;
  }

}
