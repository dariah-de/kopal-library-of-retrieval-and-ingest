/**
 * This software is copyright (c) 2018 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 **/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.util.HashMap;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;

/**
 * <p>
 * Just sets the status DONE and marks publishing completed. Unlocks root collection or edition!
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2018-08-15
 * @since 2011-01-19
 **/

public class PublishComplete implements ActionModule {

  // **
  // STATE
  // **

  ProcessData processData;
  Step step;

  // **
  // CREATION
  // **

  public PublishComplete() {
    super();
  }

  // **
  // MANIPULATION
  // **

  /**
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
    String sid = (String) customData.get(TGPublishServiceImpl.SESSION_ID);
    String log = (String) customData.get(TGPublishServiceImpl.LOG_PARAMETER);
    Boolean ignoreWarnings = (Boolean) customData.get(TGPublishServiceImpl.IGNORE_WARNINGS);
    Boolean dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
    PublishResponse response =
        (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

    // Unlock root object.
    try {
      String message = "Finally unlocking root aggregation [" + uri + "]";
      PublishUtils.setStatusRunning(this.step, message, response, this, dryRun);

      TGPublishAbs.getTgcrud().unlock(sid, log, uri);

    } catch (ObjectNotFoundFault | AuthFault | IoFault e) {
      String message = "Unlocking root aggregation [" + uri + "] failed!";
      PublishUtils.setStatusRunning(this.step, message, response, this, dryRun);
    }

    // Set module to status DONE and overall process status to FINISHED.
    String message = "CONGRATULATIONS! OVERALL PUBLISHING PROCESS COMPLETE!!"
        + (ignoreWarnings ? " (ANY WARNINGS HAVE BEEN IGNORED)" : "");
    PublishUtils.setFinished(this.step, message, response, this, dryRun);
  }

  // **
  // GET & SET METHODS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

}
