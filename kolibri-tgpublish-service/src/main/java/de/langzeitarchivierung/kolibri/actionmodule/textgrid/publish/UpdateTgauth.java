/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.io.IOException;
import java.util.HashMap;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgauth_crud.NearlyPublishRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.PortTgextraCrud;
import info.textgrid.namespaces.middleware.tgauth_crud.PublishRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault;

/**
 * <p>
 * Updates all resources in TG-auth with the isPublic flag.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-11-10
 * @since 2011-02-02
 */

public class UpdateTgauth implements ActionModule {

  // **
  // STATE
  // **

  private Step step;
  private ProcessData processData;
  private boolean dryRun;
  private boolean anyError = false;
  private boolean nearlyPublish = false;
  private String tgauthSecret;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Move data to static GRID storage.
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
    String sid = (String) customData.get(TGPublishServiceImpl.SESSION_ID);
    String log = (String) customData.get(TGPublishServiceImpl.LOG_PARAMETER);
    this.dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
    PublishResponse response =
        (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

    // Start log entry.
    String message =
        "Updating TG-auth in " + (this.nearlyPublish ? "NEARLY " : "") + "publishing mode";
    PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

    // Adding URI to PublishObject if only called with root URI.
    // NOTE This is only used with koLibRI TextGrid import module right now! Maybe we can put in a
    // URI list, too?
    if (response.getPublishObjects().isEmpty()) {
      response.getPublishObjects().add(new PublishObject(uri));
    }

    // Create TgextraCrud stub.
    PortTgextraCrud tgauthCrud = TGPublishAbs.getTgauthCrud();

    // Loop and set isPublic.
    for (PublishObject o : response.getPublishObjects()) {

      String loguri = "[" + o.uri + "]";

      // #PUBLISH (only if not dry run and not already published!).
      //
      // NOTE ALREADY_PUBLISHED is not set, until CheckIsPublic class is called first! So we have no
      // problem publishing nearlyPublished objects here if NOT calling CheckIsPublic before
      // (because they would be ALREADY_PUBLISHED)! So just be careful what policy you use here!
      if (!this.dryRun && !o.status.equals(StatusType.ALREADY_PUBLISHED)) {
        try {
          // Do a NEARLY publish only!
          if (this.nearlyPublish) {
            NearlyPublishRequest req = new NearlyPublishRequest();
            req.setAuth(sid);
            req.setLog(log);
            req.setResource(o.uri);
            req.setSecret(this.tgauthSecret);

            if (!tgauthCrud.nearlyPublish(req).isResult()) {
              throw new IOException();
            }
          }

          // Do a publish!
          else {
            PublishRequest req = new PublishRequest();
            req.setAuth(sid);
            req.setLog(log);
            req.setResource(o.uri);
            req.setSecret(this.tgauthSecret);

            if (!tgauthCrud.publish(req).isResult()) {
              throw new IOException();
            }
          }

          message = loguri + " has been " + (this.nearlyPublish ? "NEARLY " : "") + "published";
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

        } catch (IOException e) {
          message = loguri + " Unable to set isPublic flag: " + e.getMessage();
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
          o.addError(ErrorType.NOT_SPECIFIED, message);
          this.anyError = true;
        } catch (AuthenticationFault e) {
          message = loguri + " No publish right: " + e.getMessage();
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

          // TODO REMOVE DEBUG OUTPUT!!
          //
          // FIXME Use this output for publish errors in the next
          // version --> e.getFaultInfo().getCause() and
          // e.getFaultInfo().getFaultMessage()!

          //
          // message = "ERRORS:\n";
          // if (e.getCause() != null) {
          // message += e.getCause().getMessage() + "\n";
          // }
          // if (e.getFaultInfo() != null) {
          // message += e.getFaultInfo().getCause() + "\n";
          // message += e.getFaultInfo().getFaultMessage() + "\n";
          // message += e.getFaultInfo().getFaultNo();
          // }
          // PublishUtils.setStatusRunning(this.step, message,
          // response,
          // this, this.dryRun);
          // TODO REMOVE DEBUG OUTPUT!!

          o.addError(ErrorType.AUTH, message);
          this.anyError = true;
        } catch (UnknownResourceFault e) {
          message = loguri + " Resource is not known: " + e.getMessage();
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
          o.addError(ErrorType.NOT_SPECIFIED, message);
          this.anyError = true;
        }
      } else {
        // Just log in DRY RUN or already published mode.
        message = loguri + " has been " + (this.nearlyPublish ? "NEARLY " : "") + "published";
        PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
      }
    }

    // Check if errors occurred.
    if (this.anyError) {

      // FIXME We must assure failures are put into the ministatus
      // response!!
      message = "TG-auth update failed";
      PublishUtils.setStatusError(this.step, message, response, this,
          this.dryRun);
    } else {
      message = "TG-auth update complete";
      PublishUtils.setStatusDone(this.step, message, response, this,
          this.dryRun);
    }

    // Finally write response to the response.
    PublishUtils.updateResponse(uri, response);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param nearlyPublish
   * @return nearly publish or publish?
   */
  public void setNearlyPublish(boolean nearlyPublish) {
    this.nearlyPublish = nearlyPublish;
  }

  /**
   * @param secret
   */
  public void setTgauthSecret(String secret) {
    this.tgauthSecret = secret;
  }

}
