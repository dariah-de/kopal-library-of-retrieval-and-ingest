/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.copy;


import java.util.HashMap;
import java.util.List;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.clients.SearchClient;
import info.textgrid.clients.tgsearch.InfoQuery;
import info.textgrid.clients.tgsearch.NavigationQuery;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.tgpublish.api.jaxb.ProcessStatusType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import info.textgrid.namespaces.middleware.tgauth.IsPublicRequest;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

/**
 * <p>
 * Collects all the URIs from the objects to copy.
 * </p>
 * 
 * @version 2023-05-11
 * @since 2012-03-09
 */

public class GatherObjectUris implements ActionModule {

  // **
  // FINALS
  // **

  private static final String ADDED = "added";
  private static final String NOT_ADDED = "not existing";
  private static final String ACCESS_DENIED = "access denied";
  private static final String PUBLIC = "public";
  private static final String NON_PUBLIC = "non-public";
  private static final boolean NO_DRYRUN = false;

  // **
  // STATE
  // **

  private Step step;
  private ProcessData processData;
  private String tgsearchUrl;
  private String tgsearchUrlPublic;
  private boolean abort = false;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Gathers TextGrid URIs of TextGrid objects to copy.
   * </p>
   * 
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>A process UUID in the custom data as creates and set in TGPublishServiceImpl#COPY.</li>
   * <li>A list of TextGrid URIs in the custom data object as set in TGPublishServiceImpl#COPY.</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>Increases the progress in percent.</li>
   * <li>All Objects from the URI list, and furthermore all URIs included in any aggregation of that
   * URI list are put into the PublishResponse as PublishObjects, if existing and access granted
   * with the given RBAC session ID.</li>
   * </ul>
   * </p>
   * 
   * <p>
   * Can be aborted by using the abort() method.
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    List<String> uriList = (List<String>) customData.get(TGPublishServiceImpl.TEXTGRID_URI_LIST);
    String sid = (String) customData.get(TGPublishServiceImpl.SESSION_ID);
    String log = (String) customData.get(TGPublishServiceImpl.LOG_PARAMETER);
    String uuid = (String) customData.get(TGPublishServiceImpl.SESSION_UUID);
    PublishResponse response =
        (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

    // Start log entry.
    String message = "Gathering object URIs";
    PublishUtils.setStatusRunningCheckAbort(this.step, message, response, this, NO_DRYRUN);

    // Get tgauth client.
    PortTgextra tgauth = TGPublishAbs.getTgauth();

    // Get tgsearch client info queries.
    SearchClient tgsearch = new SearchClient(this.tgsearchUrl);
    InfoQuery infoQueryNonPublic = tgsearch.infoQuery().setSid(sid);
    NavigationQuery navogationQueryNonPublic = tgsearch.navigationQuery().setSid(sid);

    SearchClient tgsearchPublic = new SearchClient(this.tgsearchUrlPublic);
    InfoQuery infoQueryPublic = tgsearchPublic.infoQuery();
    NavigationQuery navogationQueryPublic = tgsearchPublic.navigationQuery();

    // Check URI list for emptiness.
    if (uriList == null || uriList.isEmpty()) {
      message = "URI list is empty, nothing to copy";
      PublishUtils.setStatusError(this.step, message, response, this, NO_DRYRUN);
      PublishUtils.updateResponse(uuid, response);
      return;
    }

    // Add the URI list to the response, and check for aggregations to add recursively.
    // NOTE We assume, every given URI AND its aggregations are either in the non-public or in the
    // public search index!
    for (String uri : uriList) {

      // Check for user abortion at loop start.
      if (response.getPublishStatus().processStatus == ProcessStatusType.ABORTED) {
        // Set abort flag and leave loop.
        this.abort = true;
        break;
      }

      // Set log URI.
      String uriString = "[" + uri + "] ";

      // Set default search client info and navigation queries (non-public).
      InfoQuery currentInfoQuery = infoQueryNonPublic;
      NavigationQuery currentNavigationQuery = navogationQueryNonPublic;
      String isPublic = NON_PUBLIC;

      // Check isPublic for each object (gather public URIs from the public search, non-public URIs
      // from the non-public search).
      IsPublicRequest req = new IsPublicRequest();
      req.setAuth(sid);
      req.setLog(log);
      req.setResource(uri);

      if (tgauth.isPublic(req).isResult()) {
        currentInfoQuery = infoQueryPublic;
        currentNavigationQuery = navogationQueryPublic;
        isPublic = PUBLIC;
      }

      PublishUtils.setStatusRunningCheckAbort(this.step,
          uriString + "is " + isPublic + " > using " + isPublic + " search index", response, this,
          NO_DRYRUN);

      // Check URI for existence.
      Response searchResponse = currentInfoQuery.getMetadata(uri);
      if (searchResponse.getResult().size() > 0) {
        // Add URI and publish status to response.
        PublishObject po = new PublishObject(uri);
        if (isPublic.equals(PUBLIC)) {
          po.status = StatusType.ALREADY_PUBLISHED;
        }
        response.addPublishObject(po);
        message = uriString + ADDED + " (" + isPublic + ")";
        PublishUtils.setStatusRunningCheckAbort(this.step, message, response, this, NO_DRYRUN);
        // Now gather.
        gather(currentNavigationQuery, uri, response, isPublic);
      } else {
        message = uriString + NOT_ADDED + " or " + ACCESS_DENIED;
        PublishUtils.setStatusRunningCheckAbort(this.step, message, response, this, NO_DRYRUN);
      }
    }

    // Only set process status, if not aborted by user.
    int objectListSize = response.getPublishObjects().size();
    if (this.abort) {
      message = "PROCESS ABORTED BY USER";
      PublishUtils.setStatusAborted(this.step, message, response, this, NO_DRYRUN);
    } else if (objectListSize == 0) {
      message = "Gathering of object URIs failed: No objects available to copy";
      PublishUtils.setStatusError(this.step, message, response, this, NO_DRYRUN);
    } else {
      message = "Gathering of object URIs complete: " + objectListSize
          + " object" + (objectListSize != 1 ? "s" : "") + " found";
      PublishUtils.setStatusDone(this.step, message, response, this, NO_DRYRUN);
    }

    // Set objectListComplete flag for flagging the list is ready to deliver publish status now
    // (means no objects are added anymore to the list!).
    response.objectListComplete = true;

    // Write response to the response.
    PublishUtils.updateResponse(uuid, response);
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param theMNavigationQuery
   * @param theUri
   * @param theResponse
   * @param isPublic
   */
  protected void gather(NavigationQuery theMNavigationQuery, String theUri,
      PublishResponse theResponse, String isPublic) {

    String message = "";

    // Get objects contained in aggregation and add to response.
    Response agg = theMNavigationQuery.listAggregation(theUri);

    for (ResultType res : agg.getResult()) {

      // Check for user abortion at loop start.
      if (theResponse.getPublishStatus().processStatus == ProcessStatusType.ABORTED) {
        // Set abort flag and leave loop.
        this.abort = true;
        break;
      }

      String uri = res.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
      String uriString = "[" + uri + "] ";

      // Check if authorised (or existing).
      if (res.isAuthorized() != null && !res.isAuthorized()) {
        message = uriString + ACCESS_DENIED;
        PublishUtils.setStatusRunningCheckAbort(this.step, message, theResponse, this, NO_DRYRUN);
      } else {
        // NOTE listAggregations does NOT include work objects that are NOT contained anywhere in
        // the edition object! So we have to add them by looking up the metadata of each edition!
        if (res.getObject().getGeneric().getProvided().getFormat().equals(TextGridMimetypes.EDITION)
            && res.getObject().getEdition().getIsEditionOf() != null) {

          // Add work URI and publish status to response.
          String workUri = res.getObject().getEdition().getIsEditionOf();
          PublishObject po = new PublishObject(workUri);
          if (isPublic.equals(PUBLIC)) {
            // TODO Have we to check if the WORK actually is published already?
            po.status = StatusType.ALREADY_PUBLISHED;
          }
          theResponse.addPublishObject(po);
          message = "[" + workUri + "] " + ADDED + " as work URI referenced from edition " + uri
              + " (" + isPublic + ")";
          PublishUtils.setStatusRunningCheckAbort(this.step, message, theResponse, this, NO_DRYRUN);
        }

        // Add URI and publish status to response.
        PublishObject po = new PublishObject(uri);
        if (isPublic.equals(PUBLIC)) {
          po.status = StatusType.ALREADY_PUBLISHED;
        }
        theResponse.addPublishObject(po);
        message = uriString + ADDED + " (" + isPublic + ")";
        PublishUtils.setStatusRunningCheckAbort(this.step, message, theResponse, this, NO_DRYRUN);

        // Start recursion if format is aggregation (edition, collection).
        if (TextGridMimetypes.AGGREGATION_SET
            .contains(res.getObject().getGeneric().getProvided().getFormat())) {
          gather(theMNavigationQuery, uri, theResponse, isPublic);
        }
      }
    }
  }

  // **
  // GETTER AND SETTER
  // **

  /**
   *
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   *
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param tgsearchUrl
   */
  public void setTgsearchUrl(String tgsearchUrl) {
    this.tgsearchUrl = tgsearchUrl;
  }

  /**
   * @param tgsearchUrlPublic
   */
  public void setTgsearchUrlPublic(String tgsearchUrlPublic) {
    this.tgsearchUrlPublic = tgsearchUrlPublic;
  }

}
