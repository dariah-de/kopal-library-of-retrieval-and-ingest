/**
 * This software is copyright (c) 2018 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 **/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.util.HashMap;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import info.textgrid.namespaces.middleware.tgauth.IsPublicRequest;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;

/**
 * <p>
 * Checks for every object, if it is already published.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2018-08-15
 * @since 2011-06-01
 **/

public class CheckIsPublic implements ActionModule {

  // **
  // STATE
  // **

  private Step step;
  private ProcessData processData;
  private boolean dryRun;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Check publicity of single objects.
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
    String sid = (String) customData.get(TGPublishServiceImpl.SESSION_ID);
    String log = (String) customData.get(TGPublishServiceImpl.LOG_PARAMETER);
    boolean dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
    PublishResponse response =
        (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

    // Create Tgextra stub.
    PortTgextra tgauth = TGPublishAbs.getTgauth();

    String message = "Checking for already published objects";
    PublishUtils.setStatusRunning(this.step, message, response, this, false);

    // Loop.
    int amount = 0;
    for (PublishObject o : response.getPublishObjects()) {

      String loguri = "[" + o.uri + "]";

      // Check isPublic.
      IsPublicRequest req = new IsPublicRequest();
      req.setAuth(sid);
      req.setLog(log);
      req.setResource(o.uri);

      boolean isPublic = tgauth.isPublic(req).isResult();
      if (isPublic) {
        // Set status to already published.
        // NOTE Be sure that this status is not overwritten by following modules! It will eventually
        // fail in UpdateTgauth then!
        o.status = StatusType.ALREADY_PUBLISHED;
        amount++;
      }

      message = loguri + " isPublic: " + isPublic;
      PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
    }

    // Complete.
    message = "Check for published objects complete, found " + amount + " already published object"
        + (amount != 1 ? "s" : "");
    PublishUtils.setStatusDone(this.step, message, response, this, dryRun);

    // Finally write response to the response.
    PublishUtils.updateResponse(uri, response);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

}
