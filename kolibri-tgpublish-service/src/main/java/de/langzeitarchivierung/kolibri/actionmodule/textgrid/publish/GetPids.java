/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid._import.RewriteMethod;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Fixity;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import info.textgrid.utils.linkrewriter.ImportMapping;

/**
 * TODOLOG
 * 
 * TODO We are getting PIDs for every object, if not dryRun! We need NOT get PIDs for ALREADY
 * PUBLISHED objects! Due to PID generation in groups of some PIDs we cannot leave PIDs out for
 * certain objects! What a pity! Just be sure we do not UPDATE this objects later (we do!)!
 * 
 **
 * CHANGELOG
 * 
 * 2024-09-02 - Funk - Add tgpid and tgsearch clients from TGPublishAbs.
 * 
 * 2020-05-19 - Funk - Add error handling for missing extend and fixity tags in generated metadata.
 * 
 * 2020-03-03 - Funk - Add "TextGrid" as publisher metadata.
 * 
 * 2020-03-03 - Funk - Implement CHECKSUM and FILESIZE PID metadata, too.
 * 
 * 2017-02-28 - Funk - Set CHECK_PIDS_FIRST default to false. See comment below!
 * 
 * 2016-05-31 - Funk - Removed log param from PID service calls.
 * 
 * 2014-11-15 - Funk - Added fakePids, repeatTimes and repeatEvery parameters.
 * 
 * 2011-01-17 - Funk - First version.
 */

/**
 * <p>
 * Gets a PID (handle) from the GWDG handle service for each TextGrid object in the list.
 * </p>
 * 
 * <p>
 * In DRYRUN mode do nothing, get PIDs otherwise.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-09-02
 * @since 2011-01-17
 */

public class GetPids implements ActionModule {

  // **
  // FINALS
  // *

  private static final String URI_DEVIDER = "...";
  private static final String PID_PREFIX = "hdl:";
  private static final String PUBLISHER = "TextGrid";
  private static final String PID_THINGS_SEPARATION_CHAR = ",";
  // TODO Checking PIDs first only makes sense, if we are using already existing TextGrid URIs
  // AGAIN! Are we doing that? Setting to false for the moment! It is far too slow anyway... :-)
  private static final boolean CHECK_PIDS_FIRST = false;

  // **
  // STATE
  // **

  private Step step;
  private ProcessData processData;
  private String pidResolverPrefix;
  private int amountOfUrisAtOnce = 10;
  private boolean fakePids;
  private boolean dryRun;
  private int repeatTimes = 5;
  private int repeatEvery = 10;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Gets a PID (handle) from the GWDG handle service for each TextGrid object in the list.
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
    String sid = (String) customData.get(TGPublishServiceImpl.SESSION_ID);
    String log = (String) customData.get(TGPublishServiceImpl.LOG_PARAMETER);
    this.dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
    PublishResponse response =
        (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

    // Get the amount of objects to publish.
    int amount = response.getPublishObjects().size();

    String message = "Getting PIDs for " + (amount != 1 ? "all " + amount : "the one and only")
        + " object" + (amount != 1 ? "s" : "") + " to publish";
    PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

    // Create the import mapping to store PIDs in.
    ImportMapping mapping = new ImportMapping();

    try {

      message = "Getting filesizes and checksums from tgsearch";
      PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

      // Assemble URI, filesize and checksum lists.
      List<String> uriList = new ArrayList<String>();
      List<String> filesizeList = new ArrayList<String>();
      List<String> checksumList = new ArrayList<String>();
      for (PublishObject o : response.getPublishObjects()) {
        // Set URI.
        uriList.add(o.uri);
        // Get object's metadata.
        Response searchResponse =
            TGPublishAbs.getTgsearch().infoQuery().setSid(sid).getMetadata(o.uri);
        if (searchResponse.getResult() == null || searchResponse.getResult().size() < 1) {
          message = "URI not existing or access denied [" + o.uri + "]";
          throw new IOException(message);
        }
        ResultType result = searchResponse.getResult().get(0);
        // Check and set filesize.
        BigInteger extent = result.getObject().getGeneric().getGenerated().getExtent();
        String filesize = "";
        if (extent != null) {
          filesize = extent.toString();
          filesizeList.add(filesize);
        } else {
          message =
              "Internal Missing Metadata Error at [" + o.uri
                  + "]: NO <extent> found in <generated> metadata! Please contact your local TextGrid administrator immediately! Or just file a bug report!";
          throw new IOException(message);
        }
        // Check and set checksum.
        List<Fixity> fixityList = result.getObject().getGeneric().getGenerated().getFixity();
        String checksum = "";
        if (fixityList != null && !fixityList.isEmpty()) {
          checksum = fixityList.get(0).getMessageDigestAlgorithm().toLowerCase() + ":"
              + fixityList.get(0).getMessageDigest();
          checksumList.add(checksum);
        } else {
          message = "Internal Missing Metadata Error at [" + o.uri
              + "]: NO <fixity> found in <generated> metadata! Please contact your local TextGrid administrator immediately! Or just file a bug report!";
          throw new IOException(message);
        }
        message = "[" + o.uri + "] filesize=" + filesize + ", checksum=" + checksum;
        PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
      }

      // Get PIDs in steps of theAmount (RUN mode).
      getPids(sid, log, null, uriList, filesizeList, checksumList, mapping, this.amountOfUrisAtOnce,
          this.pidResolverPrefix, URI_DEVIDER);

      message = "Successfully generated persistent identifier" + (amount != 1 ? "s" : "") + " for "
          + amount + " TextGrid URI" + (amount != 1 ? "s" : "");
      PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

    } catch (XPathExpressionException | IOException | SAXException
        | ParserConfigurationException e) {
      message = "Error getting PIDs [" + e.getClass().getName() + "]: " + e.getMessage();
      PublishUtils.addGeneralError(this.step, ErrorType.NOT_SPECIFIED, message, response, this,
          this.dryRun);
      // Write response to the response.
      PublishUtils.updateResponse(uri, response);
      return;
    } catch (Exception e) {
      message = "Error getting PIDs [" + e.getClass().getName() + "]: " + e.getMessage();
      PublishUtils.addGeneralError(this.step, ErrorType.NOT_SPECIFIED, message, response, this,
          this.dryRun);
      // Write response to the response.
      PublishUtils.updateResponse(uri, response);
      return;
    }

    // Set all PIDs to the publish objects.
    if (!this.dryRun) {
      for (PublishObject o : response.getPublishObjects()) {
        // TODO Check if we handle rewrite mapping correctly here!!
        o.pid = PID_PREFIX + mapping.getImportObjectForLocalURI(URI.create(o.uri)).getTextgridUri();

        if (o.status.equals(StatusType.ALREADY_PUBLISHED)) {
          // TODO In fact we ARE setting the generated PID, but we are NOT USING it due to PID
          // generation issue (see TODO above!)!
          message = "Setting NO PID to object with URI " + o.uri
              + " due to already published object (generated PID: " + o.pid + ")!";
        } else {
          message = "Setting PID " + o.pid + " to object with URI " + o.uri;
        }
        PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
      }
    }

    // Log and set status.
    message = "PID generation complete";
    PublishUtils.setStatusDone(this.step, message, response, this, this.dryRun);

    // Write response to the response.
    PublishUtils.updateResponse(uri, response);
  }

  // **
  // INTERNAL METHODS
  // **

  /**
   * <p>
   * Get PIDs for every Textgrid URI and fill import mapping.
   * </p>
   * 
   * /**
   * 
   * @param theRbacSessionId
   * @param theLogParameter
   * @param theWriter
   * @param theUriList
   * @param theFilesizeList
   * @param theChecksumList
   * @param theMapping
   * @param theAmount
   * @param theResolverPrefix
   * @param theUriDevider
   * @throws XPathExpressionException
   * @throws IOException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  private void getPids(final String theRbacSessionId, final String theLogParameter,
      final BufferedWriter theWriter, final List<String> theUriList,
      final List<String> theFilesizeList, List<String> theChecksumList, ImportMapping theMapping,
      final int theAmount, final String theResolverPrefix, final String theUriDevider)
      throws XPathExpressionException, IOException, SAXException, ParserConfigurationException {

    // Get only theAmount URIs at once, until we have all of them.
    List<String> shortUriList = new ArrayList<String>();
    for (int i = 0; i < theUriList.size(); i = i + theAmount) {

      // Check if we have the rest, set amount accordingly.
      int amount = theAmount;
      if (i > theUriList.size() - amount) {
        amount = theUriList.size() - i;
      }

      shortUriList = theUriList.subList(i, i + amount);

      // Assemble a string full of URIs.
      String uriString = list2CSVString(shortUriList, theResolverPrefix);

      // Assemble a string full of file sizes.
      String filesizeString = list2CSVString(theFilesizeList, "");

      // Assemble a string full of checksums.
      String checksumString = list2CSVString(theChecksumList, "");

      PublishUtils.logStatusRunning(this.step, (this.dryRun ? "[DRY RUN] " : "") + "Requesting "
          + shortUriList.size() + (this.fakePids ? " FAKE " : " ") + "PID"
          + (shortUriList.size() != 1 ? "s" : "") + " for " + (i + amount) + "/"
          + theUriList.size() + " TextGrid URI" + (theUriList.size() != 1 ? "s" : "") + ": "
          + (shortUriList.size() == 1 ? shortUriList.get(0)
              : shortUriList.get(0) + theUriDevider + shortUriList.get(shortUriList.size() - 1)),
          this.dryRun);

      // Get a PID for every object in the URI string.
      if (!this.dryRun) {
        long pidRequestStartTime = System.currentTimeMillis();
        String pidString = "";
        int repeat = 1;
        while (pidString.equals("")) {
          try {
            if (this.fakePids) {
              pidString = TGPublishAbs.getTgpid().getFakePids(uriString);
            } else {
              // Use the new TG-pid methods, and do cope with all the new PID metadata! YEAH!
              // Finally! :-)
              pidString = TGPublishAbs.getTgpid().getPids(theRbacSessionId, uriString,
                  filesizeString, checksumString, PUBLISHER, CHECK_PIDS_FIRST);
            }
          } catch (Exception e) {
            repeat++;
            try {
              // Throw error after repeating and always failing.
              if (repeat > this.repeatTimes) {
                throw new IOException("Failed to get PIDs after repeating " + this.repeatTimes
                    + " time" + (this.repeatTimes != 1 ? "s" : "") + "!");
              }

              this.step.setStatus(Status.WARNING,
                  "Error getting PIDs due to " + e.getClass().getName() + ": " + e.getMessage()
                      + "! Try " + repeat + "/" + this.repeatTimes + " in " + this.repeatEvery
                      + " second" + (this.repeatEvery != 1 ? "s" : "") + "...");

              Thread.sleep(this.repeatEvery * 1000);
            } catch (InterruptedException f) {
              // Nothing to catch here :-)
            }
          }
        }

        // Do some logging and measuring.
        long duration = System.currentTimeMillis() - pidRequestStartTime;
        String durationString = KolibriTimestamp.getDurationInHours(duration);
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);
        String average = nf.format((float) duration / (float) amount / 1000f);

        PublishUtils.logStatusRunning(this.step,
            "PID response string retrieved in " + durationString + " (" + average + "s/PID): "
                + (pidString.indexOf(",") == -1 ? pidString
                    : pidString.substring(0, pidString.indexOf(",")) + theUriDevider
                        + pidString.substring(pidString.lastIndexOf(",") + 1)),
            this.dryRun);

        PublishUtils.logStatusRunning(this.step, "PID string: " + pidString, this.dryRun);

        String pairs[] = pidString.split(",");
        // TODO Check if the amount of PIDs is correct!
        for (String pair : pairs) {
          String uriAndPid[] = pair.split("@");
          // We need to have only two and exactly two elements splitted here!
          if (uriAndPid.length == 2) {
            // TODO WHY? Add URI and PID to mapping (purge ".0").
            // String uri = uriAndPid[0].substring(0, uriAndPid[0].lastIndexOf("."));
            String uri = uriAndPid[0].substring(theResolverPrefix.length());
            String pid = uriAndPid[1];
            // FIXME Check if we must put the "hdl:" into the rewrite mapping!!
            theMapping.add(URI.create(pid), URI.create(uri), null, RewriteMethod.XML);

            // Serialize mapping entries.
            if (theWriter != null) {
              theWriter.append(uri + "\t" + pid);
              theWriter.newLine();
            }
          } else {
            // TODO Check if we have exactly two elements after splitting (and trimming).
          }
        }
      }
    }
  }

  /**
   * @param theList
   * @param prefix
   * @return
   */
  private static String list2CSVString(List<String> theList, String prefix) {

    String result = "";

    StringBuffer buffer = new StringBuffer();
    for (String item : theList) {
      buffer.append(prefix + item);
      buffer.append(PID_THINGS_SEPARATION_CHAR);
    }
    buffer.deleteCharAt(buffer.length() - 1);

    result = buffer.toString();

    return result;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param thePidResolverPrefix
   */
  public void setPidResolverPrefix(String thePidResolverPrefix) {
    this.pidResolverPrefix = thePidResolverPrefix;
  }

  /**
   * @param theAmountOfUrisAtOnce
   */
  public void setAmountOfUrisAtOnce(int theAmountOfUrisAtOnce) {
    this.amountOfUrisAtOnce = theAmountOfUrisAtOnce;
  }

  /**
   * @param theFakePids
   */
  public void setFakePids(boolean theFakePids) {
    this.fakePids = theFakePids;
  }

  /**
   * @param theRepeatTimes
   */
  public void setRepeatTimes(int theRepeatTimes) {
    this.repeatTimes = theRepeatTimes;
  }

  /**
   * @param theRepeatEvery
   */
  public void setRepeatEvery(int theRepeatEvery) {
    this.repeatEvery = theRepeatEvery;
  }

}
