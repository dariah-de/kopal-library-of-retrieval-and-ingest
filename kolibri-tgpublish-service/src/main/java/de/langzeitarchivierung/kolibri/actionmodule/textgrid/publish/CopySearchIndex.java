///*******************************************************************************
// * This software is copyright (c) 2015 by
// * 
// * - TextGrid Consortion (http://www.textgrid.de) - DAASI International GmbH (http://www.daasi.de)
// *
// * This is free software. You can redistribute it and/or modify it under the terms described in the
// * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
// * download it from
// *
// * http://www.gnu.org/licenses/lgpl-3.0.txt
// *
// * @copyright TextGrid Consortion (http://www.textgrid.de)
// * @copyright DAASI International GmbH (http://www.daasi.de)
// * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
// * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
// * @author Stefan E. Funk (stefan.e.funk@daasi.de)
// * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
// ******************************************************************************/
//
//package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;
//
//import java.io.IOException;
//import java.net.MalformedURLException;
//import java.util.Arrays;
//import java.util.HashMap;
//import org.apache.http.entity.BufferedHttpEntity;
//import de.langzeitarchivierung.kolibri.ProcessData;
//import de.langzeitarchivierung.kolibri.Step;
//import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
//import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
//import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
//import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
//import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
//import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
//import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
//import info.textgrid.middleware.common.JPairtree;
//import info.textgrid.utils.existclient.ExistClient;
//import info.textgrid.utils.existclient.ExistStatusCodes;
//import info.textgrid.utils.httpclient.TGHttpResponse;
//
///*******************************************************************************
// * <p>
// * Copies the dynamic eXist index to the 2nd (static) Search Index.
// * </p>
// * 
// * @author Ubbo Veentjer, SUB Göttingen
// * @author Stefan E. Funk, DAASI International GmbH
// * @version 2015-01-20
// * @since 2011-02-02
// * @deprecated
// ******************************************************************************/
//
//@Deprecated
//public class CopySearchIndex implements ActionModule {
//
//  // **
//  // STATE
//  // **
//
//  private Step step;
//  private ProcessData processData;
//  private boolean dryRun;
//  private String existReadHost;
//  private String existReadUser;
//  private String existReadPassword;
//  private String existAllWriteHost;
//  private String existAllWriteUser;
//  private String existAllWritePassword;
//  private String existLatestWritePassword;
//  private String existLatestWriteUser;
//  private String existLatestWriteHost;
//  private String uri;
//  private boolean anyError = false;
//  private boolean usePairtreeReadHost = true;
//  private boolean usePairtreeWriteHost = false;
//  private PublishResponse response;
//
//  // **
//  // MANIPULATION
//  // **
//
//  /**
//   * <p>
//   * Just takes the 1st Index and copies all new data to the 2nd index.
//   * </p>
//   * 
//   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
//   */
//  public void go() {
//
//    // Get the data out of the custom data.
//    HashMap<Object, Object> customData = this.processData.getCustomData();
//
//    this.uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
//    this.dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
//    this.response = (PublishResponse) customData
//        .get(TGPublishServiceImpl.PUBLISH_RESPONSE);
//
//    // Start log entry.
//    String message = "Copying eXist index to static";
//    PublishUtils.setStatusRunning(this.step, message, this.response, this,
//        this.dryRun);
//
//    // Get exist.
//    ExistClient existRead = null;
//    ExistClient existAllWrite = null;
//    ExistClient existLatestWrite = null;
//
//    try {
//      // Check hosts.
//      if (this.existReadHost.startsWith("http://")) {
//        message = "Setting eXist readhost to " + this.existReadHost;
//        PublishUtils.setStatusRunning(this.step, message,
//            this.response, this, this.dryRun);
//        existRead = new ExistClient(this.existReadHost,
//            this.existReadUser, this.existReadPassword);
//      } else {
//        finishWithError(ErrorType.NOT_SPECIFIED,
//            "eXist readhost not set correctly");
//      }
//      if (this.existAllWriteHost.startsWith("http://")) {
//        message = "Setting eXist (all) writehost to "
//            + this.existAllWriteHost;
//        PublishUtils.setStatusRunning(this.step, message,
//            this.response, this, this.dryRun);
//        existAllWrite = new ExistClient(this.existAllWriteHost,
//            this.existAllWriteUser, this.existAllWritePassword);
//      } else {
//        finishWithError(ErrorType.NOT_SPECIFIED,
//            "eXist (all) writehost not set correctly");
//      }
//      if (this.existLatestWriteHost == null
//          || this.existLatestWriteHost.equals("")
//          || this.existLatestWriteHost.equals("FALSE")) {
//        message = "eXist (latest) writehost NOT USED";
//        PublishUtils.setStatusRunning(this.step, message,
//            this.response, this, this.dryRun);
//      } else if (this.existLatestWriteHost.startsWith("http://")) {
//        message = "Setting eXist (latest) writehost to "
//            + this.existLatestWriteHost;
//        PublishUtils.setStatusRunning(this.step, message,
//            this.response, this, this.dryRun);
//        existLatestWrite = new ExistClient(this.existLatestWriteHost,
//            this.existLatestWriteUser,
//            this.existLatestWritePassword);
//      } else {
//        finishWithError(ErrorType.NOT_SPECIFIED,
//            "eXist (latest) writehost not set correctly");
//      }
//
//      String[] locArr = {"metadata", "structure/aggregation",
//          "structure/original", "structure/baseline"};
//
//      // Loop.
//      for (PublishObject o : this.response.getPublishObjects()) {
//
//        String logPrefix = "[" + o.uri + "] ";
//
//        // Only if object is not already published!
//        if (!o.status.equals(StatusType.ALREADY_PUBLISHED)) {
//          // Strip the "textgrid:" of the URI.
//          String docnameRead = o.uri.substring(9);
//          String docnameWrite = docnameRead;
//          // Take document name from JPairtree path, if so configured.
//          if (this.usePairtreeReadHost) {
//            docnameRead = new JPairtree(docnameRead)
//                .getPtreePathString() + docnameRead;
//          }
//          if (this.usePairtreeWriteHost) {
//            docnameWrite = new JPairtree(docnameWrite)
//                .getPtreePathString() + docnameWrite;
//          }
//
//          // Loop location array!
//          for (String loc : Arrays.asList(locArr)) {
//            TGHttpResponse readRes = existRead.getDocument(loc,
//                docnameRead);
//
//            message = logPrefix + "Looking for " + docnameRead
//                + " in " + loc;
//            PublishUtils.setStatusRunning(this.step, message,
//                this.response, this, this.dryRun);
//
//            BufferedHttpEntity bufEnt = readRes.getBuffEntity();
//
//            if (readRes.getStatusCode() == ExistStatusCodes.OK) {
//              if (!this.dryRun) {
//                try {
//                  message = logPrefix + "Found and copying "
//                      + loc + "/" + docnameRead;
//                  PublishUtils.setStatusRunning(this.step,
//                      message, this.response, this,
//                      this.dryRun);
//
//                  // Write to existAll.
//                  String allLoc = loc + "/" + docnameWrite;
//                  int writeRes = existAllWrite.putDocument(
//                      allLoc, bufEnt.getContent());
//                  message = logPrefix
//                      + "Uploading "
//                      + allLoc
//                      + " to eXist (all) finished with HTTP status: "
//                      + writeRes;
//                  PublishUtils.setStatusRunning(this.step,
//                      message, this.response, this,
//                      this.dryRun);
//
//                  // Write to existLatest, if configured.
//                  if (!this.existLatestWriteHost
//                      .equals("FALSE")) {
//                    String baseUri = docnameWrite
//                        .split("\\.")[0];
//                    String latestLoc = loc + "/" + baseUri;
//                    writeRes = existLatestWrite
//                        .putDocument(latestLoc,
//                            bufEnt.getContent());
//                    message = logPrefix
//                        + "Uploading "
//                        + latestLoc
//                        + " to eXist (latest) finished with HTTP status: "
//                        + writeRes;
//                    PublishUtils.setStatusRunning(
//                        this.step, message,
//                        this.response, this,
//                        this.dryRun);
//                  }
//
//                } catch (IOException e) {
//                  finishWithError(o, ErrorType.NOT_SPECIFIED,
//                      "IOException getting stream from eXist result");
//                }
//
//              } else {
//                // TODO: could check if existWrite reachable?
//                message = logPrefix + "[DRY RUN] Found " + loc
//                    + "/" + docnameWrite;
//                PublishUtils.setStatusRunning(this.step,
//                    message, this.response, this,
//                    this.dryRun);
//              }
//            } else {
//              message = logPrefix + "eXist responded "
//                  + readRes.getStatusCode()
//                  + ", not copied: " + loc + "/"
//                  + docnameRead;
//              PublishUtils.setStatusRunning(this.step, message,
//                  this.response, this, this.dryRun);
//            }
//
//            readRes.releaseConnection();
//          }
//        }
//
//        // If already published.
//        else {
//          message = logPrefix
//              + "URI is already published and will not be copied";
//          PublishUtils.setStatusRunning(this.step, message,
//              this.response, this, this.dryRun);
//        }
//      }
//    } catch (MalformedURLException e) {
//      finishWithError(ErrorType.NOT_SPECIFIED,
//          "Error creating eXist client");
//    } catch (IOException e) {
//      finishWithError(ErrorType.NOT_SPECIFIED,
//          "Error querying eXist client");
//    }
//
//    // Finally write response to the responseMap.
//    finish();
//  }
//
//  /**
//   * Add error to response and set status of actionmodule to error.
//   * 
//   * @param publishObject
//   * @param type
//   * @param message
//   */
//  protected void setError(PublishObject publishObject, ErrorType type,
//      String message) {
//    this.anyError = true;
//    publishObject.addError(type, message);
//    PublishUtils.setStatusError(this.step, message, this.response, this,
//        this.dryRun);
//  }
//
//  /**
//   * <p>
//   * Add error without publish object.
//   * </p>
//   * 
//   * @param type
//   * @param message
//   */
//  protected void setError(ErrorType type, String message) {
//    this.anyError = true;
//    PublishUtils.setStatusError(this.step, message, this.response, this,
//        this.dryRun);
//  }
//
//  /**
//   * call before exit:
//   * 
//   * - check if there are errors to set module state before returning - update responsemap
//   */
//  private void finish() {
//    if (this.anyError) {
//      PublishUtils.setStatusError(this.step, "Finished with Error",
//          this.response, this, this.dryRun);
//    } else {
//      PublishUtils.setStatusDone(this.step, "Index copy complete",
//          this.response, this, this.dryRun);
//    }
//
//    // Write response to the response.
//    PublishUtils.updateResponse(this.uri, this.response);
//  }
//
//  /**
//   * add error to response and set status of actionmodule to error, have responsemap written
//   * 
//   * @param publishObject
//   * @param type
//   * @param message
//   */
//  private void finishWithError(PublishObject publishObject, ErrorType type,
//      String message) {
//    setError(publishObject, type, message);
//    finish();
//  }
//
//  /**
//   * <p>
//   * Add error without publish object.
//   * </p>
//   * 
//   * @param type
//   * @param message
//   */
//  private void finishWithError(ErrorType type, String message) {
//    setError(type, message);
//    finish();
//  }
//
//  // **
//  // GETTERS AND SETTERS
//  // **
//
//  /*
//   * (non-Javadoc)
//   * 
//   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
//   * (de.langzeitarchivierung.kolibri.ProcessData)
//   */
//  public void setProcessData(ProcessData processData) {
//    this.processData = processData;
//  }
//
//  /*
//   * (non-Javadoc)
//   * 
//   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
//   * langzeitarchivierung.kolibri.Step)
//   */
//  public void setStep(Step step) {
//    this.step = step;
//  }
//
//  /**
//   * @param host
//   */
//  public void setExistReadHost(String host) {
//    this.existReadHost = host;
//  }
//
//  /**
//   * @param user
//   */
//  public void setExistReadUser(String user) {
//    this.existReadUser = user;
//  }
//
//  /**
//   * @param pw
//   */
//  public void setExistReadPassword(String pw) {
//    this.existReadPassword = pw;
//  }
//
//  /**
//   * @param host
//   */
//  public void setExistAllWriteHost(String host) {
//    this.existAllWriteHost = host;
//  }
//
//  /**
//   * @param user
//   */
//  public void setExistAllWriteUser(String user) {
//    this.existAllWriteUser = user;
//  }
//
//  /**
//   * @param pw
//   */
//  public void setExistAllWritePassword(String pw) {
//    this.existAllWritePassword = pw;
//  }
//
//  /**
//   * @param host
//   */
//  public void setExistLatestWriteHost(String host) {
//    this.existLatestWriteHost = host;
//  }
//
//  /**
//   * @param user
//   */
//  public void setExistLatestWriteUser(String user) {
//    this.existLatestWriteUser = user;
//  }
//
//  /**
//   * @param pw
//   */
//  public void setExistLatestWritePassword(String pw) {
//    this.existLatestWritePassword = pw;
//  }
//
//  /**
//   * @param usePairtree
//   */
//  public void setUsePairtreeReadHost(boolean usePairtree) {
//    this.usePairtreeReadHost = usePairtree;
//  }
//
//  /**
//   * @param usePairtree
//   */
//  public void setUsePairtreeWriteHost(boolean usePairtree) {
//    this.usePairtreeWriteHost = usePairtree;
//  }
//
//}
