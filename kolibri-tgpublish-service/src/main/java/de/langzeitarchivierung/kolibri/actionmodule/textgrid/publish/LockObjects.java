/**
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 **/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.util.HashMap;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.TGPublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;

/**
 * <p>
 * Locks all objects to be published before publishing!
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2019-03-19
 * @since 2011-01-19
 **/

public class LockObjects implements ActionModule {

  private final static String ERROR_LOCKING = "Error while locking published objects";

  // STATE (Instance variables)

  private ProcessData processData;
  private Step step;

  // **
  // CREATION
  // **

  public LockObjects() {
    super();
  }

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Locks every URI contained in the PublishObject list.
   * </p>
   * 
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>A filled list of PublishObjects set in the PublishResponse of the customData.</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>All URIs are locked afterwards.</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String sid = (String) customData.get(TGPublishServiceImpl.SESSION_ID);
    String log = (String) customData.get(TGPublishServiceImpl.LOG_PARAMETER);
    boolean dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
    PublishResponse response =
        (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

    // Start log entry.
    int amount = response.getPublishObjects().size();
    String message = "Locking all " + amount + " object" + (amount != 1 ? "s" : "");
    PublishUtils.setStatusRunning(this.step, message, response, this, dryRun);

    // Do lock all objects.
    // NOTE Gather public information in GatherObjectUris() (ONLY FOR LOCKING! Status is set to OK
    // again here! Not a very nice workaround, but it works for the time being!).
    boolean result = true;
    for (PublishObject o : response.getPublishObjects()) {
      try {
        message = "Locking " + o.uri;
        PublishUtils.setStatusRunning(this.step, message, response, this, dryRun);
        if (!o.status.equals(StatusType.ALREADY_PUBLISHED) && !dryRun) {
          result = TGPublishAbs.getTgcrud().lock(sid, log, o.uri);
        }
      } catch (ObjectNotFoundFault e) {
        message = ERROR_LOCKING + ": " + e.getMessage();
        PublishUtils.setStatusError(this.step, message, response, this, dryRun);
        result = false;
      } catch (IoFault e) {
        message = ERROR_LOCKING + ": " + e.getMessage();
        PublishUtils.setStatusError(this.step, message, response, this, dryRun);
        result = false;
      } catch (AuthFault e) {
        message = ERROR_LOCKING + ": " + e.getMessage();
        PublishUtils.setStatusError(this.step, message, response, this, dryRun);
        result = false;
      }
    }

    if (result) {
      // Set module to status DONE and overall process status to RUNNING.
      message = "Successfully locked published objects";
      PublishUtils.setStatusDone(this.step, message, response, this, dryRun);
    } else {
      // Unlock and set module to status ERROR.
      message = "Unlocking all objects again due to locking errors";
      PublishUtils.setStatusRunning(this.step, message, response, this, dryRun);
      if (!dryRun) {
        TGPublishUtils.unlock(sid, log, response);
      }
      PublishUtils.setStatusError(this.step, ERROR_LOCKING, response, this, dryRun);
    }
  }

  // **
  // GET & SET METHODS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

}
