/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettignen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.TGPublishUtils;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Warning;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgauth.FilterBySidRequest;
import info.textgrid.namespaces.middleware.tgauth.FilterResponse;
import info.textgrid.namespaces.middleware.tgauth.IsPublicRequest;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import jakarta.xml.bind.JAXBException;

/**
 * TODOLOG
 *
 * TODO As in processstarter.textgrid.publish.TGPublishWorldReadable: Check why this conditional is
 * necessary. We should not configure this twice! If anyone finds the cause for this double
 * configuration, please fix it and delete the conditionals! (setXxxxRequiredFields)
 *
 **
 * CHANGELOG
 * 
 * 2024-09-02 - Funk - Add tgpid and tgsearch clients from TGPublishAbs.
 * 
 * 2024-03-07 - Funk - Set this.response.objectListComplete = true in finish(), we need to set this
 * for error states, too. If not, we get a false publish response! Fixes #244
 * 
 * 2023-05-11 - Funk - Refactor for new Java search client.
 * 
 * 2019-11-13 - Funk - Add error handling if we have got a work in Edition object, but NOT in
 * TG-search.
 * 
 * 2018-08-13 - Funk - Fixed bug that only publishes (ads) editions and !editions, !works, and
 * !works.
 * 
 * 2017-03-15 - Funk - Moved some checking methods to TGPublishUtils.
 * 
 * 2017-03-01 - Funk -Checking if works really are works, added workRequiredFields.
 * 
 * 2017-02-28 - Funk - Corrected incorrect XPath handling. Now we do check empty <item> objects,
 * too!
 **/

/**
 * <p>
 * Checks if the given URI really is a TextGrid Edition/Collection.
 * </p>
 * 
 * @version 2024-03-07
 * @since 2011-08-15
 **/

public abstract class PublishCheck implements ActionModule {

  // **
  // STATE
  // **

  protected Step step;
  protected ProcessData processData;
  protected String uri;
  protected String sid;
  protected String log;
  protected PublishResponse response;
  protected PublishObject incomingObject;
  protected boolean anyError = false;
  protected List<String> editionRequiredFields = new ArrayList<String>();
  protected List<String> collectionRequiredFields = new ArrayList<String>();
  protected List<String> itemRequiredFields = new ArrayList<String>();
  protected List<String> workRequiredFields = new ArrayList<String>();
  protected Map<String, String> xpathNamespaces = new HashMap<String, String>();
  protected Boolean dryRun = true;
  protected Boolean ignoreWarnings = false;

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Checks recursively if all objects have got all the mandatory metadata.
   * </p>
   * <p>
   * Adds all contained objects from the given collection or edition to the list of objects.
   * </p>
   * 
   * TODO Take object assembly out of this checking class! We should use an extra class for it!
   * 
   * @param theObject
   * @param sid
   * @param response
   */
  protected void checkAggregation(ObjectType theObject, String sid, PublishResponse response) {

    // Get the TextGrid URI.
    String id = theObject.getGeneric().getGenerated().getTextgridUri().getValue();
    String type = theObject.getGeneric().getProvided().getFormat();

    // Test for missing mandatory metadata (EDITIONS and WORKS, if included).
    if (type.equals(TextGridMimetypes.COLLECTION)) {
      checkCollection(theObject);
    }
    // Test for missing mandatory metadata (COLLECTIONS).
    else if (type.equals(TextGridMimetypes.EDITION)) {
      checkEdition(theObject);
    }

    // Get objects contained in aggregation and add to response.
    Response agg = TGPublishAbs.getTgsearch().navigationQuery().setSid(sid).listAggregation(id);

    for (ResultType res : agg.getResult()) {
      String uri = res.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
      String format = res.getObject().getGeneric().getProvided().getFormat();

      PublishObject p = new PublishObject(uri);
      if (res.isAuthorized() != null && !res.isAuthorized()) {
        setError(p, ErrorType.AUTH, "Access denied [" + uri + "]");
        response.addPublishObject(p);
      } else {
        // Item check for ITEMS and AGGREGATIONS only (they do have an <item> tag).
        if (!format.equals(TextGridMimetypes.EDITION)
            && !format.equals(TextGridMimetypes.COLLECTION)
            && !format.equals(TextGridMimetypes.WORK)) {
          checkItem(res.getObject(), p);
        }

        // Add current object to publish response.
        response.addPublishObject(p);

        // Start recursion if aggregation/edition/collection is detected.
        if (TextGridMimetypes.AGGREGATION_SET.contains(format)) {

          PublishUtils.logStatusRunning(this.step,
              "[" + uri + "] " + format + " found, starting recursion", this.dryRun);

          checkAggregation(res.getObject(), sid, response);
        }
      }
    }
  }

  /**
   * <p>
   * Match XPath expression in requiredFields against tgObject, return list of expressions which
   * failed.
   * </p>
   * 
   * @param requiredFields XPath expressions
   * @param tgobject textgrid metadata object
   * @return A list of required missing metadata
   */
  protected List<String> checkRequiredMetadata(final List<String> requiredFields,
      final ObjectType tgobject) {

    List<String> result = new ArrayList<String>();

    try {
      result = TGPublishUtils.checkRequiredMetadata(requiredFields, tgobject, this.xpathNamespaces,
          this.dryRun, this.step);
    } catch (JAXBException e) {
      finishWithError(this.incomingObject, ErrorType.NOT_SPECIFIED, "JAXBException");
    } catch (ParserConfigurationException e) {
      finishWithError(this.incomingObject, ErrorType.NOT_SPECIFIED, "ParserConfigurationException");
    } catch (XPathExpressionException e) {
      finishWithError(this.incomingObject, ErrorType.NOT_SPECIFIED, "XpathExpressionException");
    }

    return result;
  }

  /**
   * @param tgobject
   * @param p
   */
  protected void checkItem(ObjectType tgobject, PublishObject p) {

    // Get item URI and check for <collection> tag.
    String itemUri = tgobject.getGeneric().getGenerated().getTextgridUri().getValue();
    if (tgobject.getItem() == null) {
      setErrorWithoutLogging(this.incomingObject, ErrorType.MISSING_METADATA,
          "[" + itemUri + "] item metadata tag missing");
    }

    // Check if all required metadata fields are set.
    List<String> missing = checkRequiredMetadata(this.itemRequiredFields, tgobject);
    if (missing.size() > 0) {
      for (String miss : missing) {
        setErrorWithoutLogging(p, ErrorType.MISSING_METADATA, "[" + itemUri + "] XPath: " + miss);
      }
    }
  }

  /**
   * <p>
   * Check if user has the right to publish objects. works on objects found in response object. call
   * this method after all found objects are listed in response.
   * </p>
   */
  protected void checkPublishRights() {

    // Get all URIs.
    List<String> uris = new ArrayList<String>();
    for (PublishObject p : this.response.getPublishObjects()) {
      uris.add(p.uri);
    }

    // Check publish rights.
    List<String> allowed = checkPublishRights(this.sid, uris);

    for (PublishObject p : this.response.getPublishObjects()) {
      if (!allowed.contains(p.uri)) {
        setError(p, ErrorType.NO_PUBLISH_RIGHT, "No publish right [" + p.uri + "]");
      }
    }
  }

  /**
   * <p>
   * Add error to response and set status of actionmodule to error, do log.
   * </p>
   * 
   * @param publishObject
   * @param type
   * @param message
   */
  protected void setError(PublishObject publishObject, ErrorType type, String message) {
    setErrorWithoutLogging(publishObject, type, message);
    PublishUtils.setStatusError(this.step, message, this.response, this, this.dryRun);
  }

  /**
   * <p>
   * Add error to response and set status of actionmodule to error, do NOT log.
   * </p>
   * 
   * @param publishObject
   * @param type
   * @param message
   */
  protected void setErrorWithoutLogging(PublishObject publishObject, ErrorType type,
      String message) {
    this.anyError = true;
    publishObject.addError(type, message);
  }

  /**
   * <p>
   * Call before exit:
   * 
   * <ul>
   * <li>Check if there are errors to set module state before returning - update response.</li>
   * </ul>
   * </p>
   */
  protected void finish() {

    if (this.anyError) {
      PublishUtils.setStatusError(this.step, "Finished with Error", this.response, this,
          this.dryRun);
    } else {
      PublishUtils.setStatusDone(this.step,
          "Congratulations! Publishable object state confirmed [" + this.uri + "]", this.response,
          this, this.dryRun);
    }

    // Set objectListComplete flag for flagging the list is ready to deliver publish status now
    // (means no objects are added anymore to the list!).
    this.response.objectListComplete = true;

    // Write response to the response.
    PublishUtils.updateResponse(this.uri, this.response);
  }

  /**
   * <p>
   * Add error to response and set status of actionmodule to error, have response written.
   * </p>
   * 
   * @param publishObject
   * @param type
   * @param message
   */
  protected void finishWithError(PublishObject publishObject, ErrorType type, String message) {
    setError(publishObject, type, message);
    finish();
  }

  /**
   * @return true if object has been published, false if not.
   */
  protected boolean alreadyPublished(final String uri) {
    IsPublicRequest isPublicInput = new IsPublicRequest();
    isPublicInput.setResource(uri);
    return TGPublishAbs.getTgauth().isPublic(isPublicInput).isResult();
  }

  /**
   * @param theResult
   * @return warnings or no warnings? that is here the question!
   */
  protected boolean checkWarnings(ResultType theResult) {

    // Check if warnings are existing in the metadata.
    List<Warning> warningList = theResult.getObject().getGeneric().getGenerated().getWarning();

    if (!this.ignoreWarnings && warningList != null && !warningList.isEmpty()) {
      String warnings = "";
      for (Warning w : warningList) {
        warnings += w.getValue() + " (" + w.getUri() + ")";
      }
      finishWithError(this.incomingObject, ErrorType.METADATA_WARNINGS_EXIST,
          (warningList.size() == 1 ? "A" : warningList.size()) + " warning"
              + (warningList.size() == 1 ? "" : "s") + " do" + (warningList.size() == 1 ? "es" : "")
              + " exist in the object's metadata [" + this.uri + "]: " + warnings);
      return true;
    }

    PublishUtils.setStatusRunning(this.step, "No warnings existing in metadata", this.response,
        this, this.dryRun);

    return false;
  }

  /**
   * <p>
   * Check collection for mandatory metadata.
   * </p>
   * 
   * @param tgobject
   */
  protected void checkCollection(ObjectType tgobject) {

    // Get collection URI and check for <collection> tag.
    String collectionUri = tgobject.getGeneric().getGenerated().getTextgridUri().getValue();
    if (tgobject.getCollection() == null) {
      setErrorWithoutLogging(this.incomingObject, ErrorType.MISSING_METADATA,
          "[" + collectionUri + "] collection metadata tag missing");
    }

    // Check if all collection required metadata fields are set.
    List<String> collectionMissing = checkRequiredMetadata(this.collectionRequiredFields, tgobject);

    if (this.step.getStatus().getType() == Status.ERROR) {
      return;
    } else if (collectionMissing.size() > 0) {
      for (String miss : collectionMissing) {
        setErrorWithoutLogging(this.incomingObject, ErrorType.MISSING_METADATA,
            "[" + collectionUri + "] XPath: " + miss);
      }
    } else if (this.collectionRequiredFields.size() == 0) {
      PublishUtils.logStatusRunning(this.step,
          "[" + collectionUri + "] No XPathes configured for type collection", this.dryRun);
    }
  }

  /**
   * <p>
   * Check edition for mandatory metadata.
   * </p>
   * 
   * @param tgobject
   */
  protected void checkEdition(ObjectType tgobject) {

    // Get edition URI and check for <collection> tag.
    String editionUri = tgobject.getGeneric().getGenerated().getTextgridUri().getValue();
    if (tgobject.getEdition() == null) {
      setErrorWithoutLogging(this.incomingObject, ErrorType.MISSING_METADATA,
          "[" + editionUri + "] edition metadata tag missing");
    }

    // Check if all edition required metadata fields are set.
    List<String> editionMissing = checkRequiredMetadata(this.editionRequiredFields, tgobject);

    if (this.step.getStatus().getType() == Status.ERROR) {
      return;
    } else if (editionMissing.size() > 0) {
      for (String miss : editionMissing) {
        setErrorWithoutLogging(this.incomingObject, ErrorType.MISSING_METADATA,
            "[" + editionUri + "] XPath: " + miss);
      }
    } else if (this.editionRequiredFields.size() == 0) {
      PublishUtils.logStatusRunning(this.step,
          "[" + editionUri + "] No XPathes configured for type edition", this.dryRun);
    }

    // Get work URI from isEditionOf field, handle work, we already checked if we have one.
    if (tgobject.getEdition() != null) {
      String workUri = tgobject.getEdition().getIsEditionOf();
      if (workUri != null && !"".equals(workUri)) {

        PublishUtils.logStatusRunning(this.step,
            "[" + editionUri + "] Work URI found in Edition metadata: " + workUri, this.dryRun);

        if (checkWork(workUri)) {
          PublishUtils.logStatusRunning(this.step,
              "[" + editionUri + "] Work URI also found in TG-search: " + workUri, this.dryRun);
        } else {
          this.anyError = true;
          finishWithError(this.incomingObject, ErrorType.MISSING_METADATA,
              "[" + editionUri + "] Missing work in TG-search (" + workUri
                  + " must be a valid TextGrid URI) " + workUri);
        }
      }
    }
  }

  /**
   * @param workUri
   */
  protected boolean checkWork(final String workUri) {

    // Get TextGrid object of the work.
    List<ResultType> workResults =
        TGPublishAbs.getTgsearch().infoQuery().setSid(this.sid).getMetadata(workUri).getResult();

    ResultType workResult = null;
    if (workResults == null || workResults.isEmpty()) {
      return false;
    }
    workResult = workResults.get(0);

    // Get work revision URI! For we cannot publish a work that has a base URI in the Edition
    // metadata.
    String workRevisionUri = workResult.getTextgridUri();

    // Add work to publishObjects. API is checking, if work already is included.
    PublishObject workObject = new PublishObject(workRevisionUri);
    this.response.addPublishObject(workObject);

    // Check if already published, then no check is needed.
    if (alreadyPublished(workRevisionUri)) {
      PublishUtils.logStatusRunning(this.step,
          "[" + workRevisionUri + "] Work is already published, skipping metadata check",
          this.dryRun);
    } else {
      // Get work type.
      String workFormat = workResult.getObject().getGeneric().getProvided().getFormat();

      // Check if all work required metadata fields are set.
      List<String> workMissing =
          checkRequiredMetadata(this.workRequiredFields, workResult.getObject());

      if (workMissing.size() > 0) {
        for (String miss : workMissing) {
          setErrorWithoutLogging(workObject, ErrorType.MISSING_METADATA,
              "[" + workRevisionUri + "] XPath: " + miss);
        }
      } else if (this.editionRequiredFields.size() == 0) {
        PublishUtils.logStatusRunning(this.step,
            "[" + workRevisionUri + "] No XPathes configured for type work", this.dryRun);
      }

      // Check, is this really IS a work and if required work metadata is existing.
      if (workFormat.equals(TextGridMimetypes.WORK)) {
        PublishUtils.logStatusRunning(this.step,
            "[" + workRevisionUri + "] Type of work checked and approved", this.dryRun);
      } else {
        setError(workObject, ErrorType.MISSING_METADATA, "[" + workRevisionUri
            + "] Type of work must be " + TextGridMimetypes.WORK + " but is " + workFormat);
      }
    }

    return true;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param sid
   * @param uris
   * @param tglog
   * @return the list of allowed TextGrid URIs
   */
  private List<String> checkPublishRights(String sid, List<String> uris) {

    FilterBySidRequest filterBySidInput = new FilterBySidRequest();
    filterBySidInput.setAuth(sid);
    filterBySidInput.setOperation("publish");
    filterBySidInput.getResource().addAll(uris);

    List<String> allowed = null;
    try {
      FilterResponse res = TGPublishAbs.getTgauth().filterBySid(filterBySidInput);
      allowed = res.getResource();
    } catch (AuthenticationFault e) {
      this.step.setStatus(Status.ERROR, "authfault");
      return null;
    }

    return allowed;
  }

  // **
  // GETTER AND SETTER
  // **

  /**
   *
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   *
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param xpathExpression
   */
  public void setEditionRequiredFields(String xpathExpression) {
    if (!this.editionRequiredFields.contains(xpathExpression)) {
      this.editionRequiredFields.add(xpathExpression);
    }
  }

  /**
   * @param xpathExpression
   */
  public void setCollectionRequiredFields(String xpathExpression) {
    if (!this.collectionRequiredFields.contains(xpathExpression)) {
      this.collectionRequiredFields.add(xpathExpression);
    }
  }

  /**
   * @param xpathExpression
   */
  public void setItemRequiredFields(String xpathExpression) {
    if (!this.itemRequiredFields.contains(xpathExpression)) {
      this.itemRequiredFields.add(xpathExpression);
    }
  }

  /**
   * @param xpathExpression
   */
  public void setWorkRequiredFields(String xpathExpression) {
    if (!this.workRequiredFields.contains(xpathExpression)) {
      this.workRequiredFields.add(xpathExpression);
    }
  }

  /**
   * @param namespace
   */
  public void setXpathNamespaces(String namespace) {
    String[] np = namespace.split("=");
    this.xpathNamespaces.put(np[0], np[1]);
  }

}
