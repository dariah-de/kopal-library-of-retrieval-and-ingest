/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.copy;

import java.util.HashMap;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;

/**
 * <p>
 * Just sets the status DONE and marks copying completed.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2023-05-08
 * @since 2012-03-09
 */

public class CopyComplete implements ActionModule {

  // **
  // STATE
  // **

  ProcessData processData;
  Step step;

  // **
  // CREATION
  // **

  public CopyComplete() {
    super();
  }

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>Nothing.</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>Increases the progress in percent.</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    PublishResponse response = (PublishResponse) customData
        .get(TGPublishServiceImpl.PUBLISH_RESPONSE);

    // Set module to status DONE and overall process status to FINISHED.
    String message = "CONGRATULATIONS! OVERALL COPYING PROCESS COMPLETE!!";
    PublishUtils.setFinished(this.step, message, response, this, false);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   *
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   *
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

}
