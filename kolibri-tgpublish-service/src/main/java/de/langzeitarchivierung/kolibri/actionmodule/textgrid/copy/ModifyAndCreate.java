/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.copy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.TGPublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid._import.RewriteMethod;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.ProcessStatusType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.RelationType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter;
import info.textgrid.utils.linkrewriter.ImportMapping;
import jakarta.activation.DataHandler;
import jakarta.mail.util.ByteArrayDataSource;
import jakarta.xml.bind.JAXB;
import jakarta.xml.ws.Holder;

/**
 * TODOLOG
 * 
 * TODO Generalise classes: RenameAndRewrite (kolibri-addon-textgrid-import) publish.ModifyAndUpdate
 * (kolibri-tgpublish-service), copy.ModifyAndCreate (kolibri-tgpublish-service)!
 */

/**
 * <p>
 * Creates URIs for all given PublishObjects and reads the source objects. Modifies all references
 * to be modified, and if necessary the data (i.e. edition and collection files), and finally
 * creates new objects with new URIs in the given project.
 * </p>
 * 
 * @version 2024-06-12
 * @since 2012-03-10
 */

public class ModifyAndCreate implements ActionModule {

  // **
  // FINAL STATIC
  // **

  private static final String NO_PROJECTID = "";
  private static final String URI_DEVIDER = "...";
  private static final String SUCCESS_READING = "TG-crud#READ complete from source ";
  private static final String SUCCESS_CREATING = "TG-crud#CREATE complete from source ";
  private static final String ERROR_READING = "TG-crud#READ failed for source ";
  private static final String ERROR_CREATING = "TG-crud#CREATE failed for source ";
  private static final String MODE_METADATA_REWRITE = "internal:textgrid#metadata";
  private static final String MODE_AGGREGATION_REWRITE = "internal:textgrid#aggregation";
  private static final String MODE_TEI_REWRITE = "internal:tei#tei";
  private static final String MODE_XSD_REWRITE = "internal:schema#xsd";
  private static final String MODE_LINKEDITOR_REWRITE = MODE_TEI_REWRITE;
  private static final int DEFAULT_BUFFER_SIZE = 4096;
  private static final int PROGRESS_UPDATES_PER_MODULE = 1;
  private static final boolean NO_DRYRUN = false;

  // **
  // STATE
  // **

  private Step step;
  private ProcessData processData;
  private int errors = 0;
  private boolean abort = false;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Do modify and create.
   * </p>
   * 
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>A filled list of PublishObjects set in the PublishResponse of the customData.</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>Increases the progress in percent.</li>
   * <li>Creates TextGrid URIs for every object.</li>
   * <li>Reads every object using TG-crud#READ.</li>
   * <li>Rewrites metadata and data using the link-rewriter.</li>
   * <li>Creates a new object for every object using TG-crud#CREATE</li>
   * </ul>
   * </p>
   * 
   * <p>
   * Can be aborted by using the abort() method.
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uuid = (String) customData.get(TGPublishServiceImpl.SESSION_UUID);
    String sid = (String) customData.get(TGPublishServiceImpl.SESSION_ID);
    String log = (String) customData.get(TGPublishServiceImpl.LOG_PARAMETER);
    String projectId = (String) customData.get(TGPublishServiceImpl.PROJECT_ID);
    PublishResponse response =
        (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);
    Boolean newRevision = (Boolean) customData.get(TGPublishServiceImpl.NEW_REVISION);

    // Start log entry.
    int objectAmount = response.getPublishObjects().size();
    String message = "Copying " + objectAmount + " object" + (objectAmount != 1 ? "s" : "")
        + " to project " + projectId;
    PublishUtils.setStatusRunningCheckAbort(this.step, message, response, this, NO_DRYRUN);

    // Get progress: This module has to cover and compute in how many steps we can divide this
    // module. Add read and create progress as single steps, and use creating rewrite map, and
    // rewriting metadata and data, too. So we have five time the amount of objects (use float for
    // computation for small values!).
    float moduleProgress = response.getPublishStatus().progress;
    float stepProgress = (float) PublishUtils.progressPerModule(this.processData)
        / (PROGRESS_UPDATES_PER_MODULE * (float) objectAmount);

    // Get tgcrud stub.
    TGCrudService tgcrudMTOM = TGPublishAbs.getTgcrud();

    // Create URIs for all PublishObjects (only if no new revisions shall be created).
    List<String> uriList = null;
    if (!newRevision) {
      try {
        uriList = tgcrudMTOM.getUri(sid, log, NO_PROJECTID, objectAmount);
      } catch (ObjectNotFoundFault e) {
        this.errors++;
      } catch (IoFault e) {
        this.errors++;
      } catch (AuthFault e) {
        this.errors++;
      }

      // Fail if URI creation fails!
      if (uriList == null || this.errors != 0) {
        message = "Error getting URIs from TG-crud";
        PublishUtils.setStatusError(this.step, message, response, this, NO_DRYRUN);
        return;
      }

      message = "Created " + objectAmount + " URI" + (objectAmount != 1 ? "s" : "")
          + " for new TextGrid object" + (objectAmount != 1 ? "s" : "") + ": " + uriList.get(0)
          + URI_DEVIDER + uriList.get(uriList.size() - 1);
      PublishUtils.setStatusRunningCheckAbort(this.step, message, response, this, NO_DRYRUN);

    } else {
      message = "Creating new revisions, no need for getting URIs from TG-crud";
      PublishUtils.setStatusRunningCheckAbort(this.step, message, response, this, NO_DRYRUN);
    }

    // Go through all objects and create rewrite mapping for the source URI to the destination URI
    // (only if no new revisions shall be created!).
    int c = 0;
    ImportMapping mapping = new ImportMapping();
    if (!newRevision) {
      for (PublishObject o : response.getPublishObjects()) {
        // TODO Shall we rewrite to revision URIs or base URIs?
        String newUri = uriList.get(c++);
        // Only add revision URI if existing.
        String oldUri = "";
        if (o.uri.contains(".")) {
          oldUri = o.uri.substring(0, o.uri.indexOf("."));
          mapping.add(URI.create(newUri), URI.create(oldUri), null, RewriteMethod.XML);
        }
        mapping.add(URI.create(newUri), URI.create(o.uri), null, RewriteMethod.XML);
        message = "[" + o.uri + (oldUri.equals("") ? "" : ", " + oldUri) + "] mapped to " + newUri;
        PublishUtils.setStatusRunningCheckAbort(this.step, message, response, this, NO_DRYRUN);
      }

      message = "Import mapping generated for new files to create (old URI > new URI)";
      PublishUtils.setStatusRunningCheckAbort(this.step, message,
          response, this, NO_DRYRUN);

    } else {
      message = "No import mapping generated, we are creating new revisions for each file";
      PublishUtils.setStatusRunningCheckAbort(this.step, message, response, this, NO_DRYRUN);
    }

    // Go through all objects.
    for (PublishObject o : response.getPublishObjects()) {

      // Set progress per object.
      moduleProgress += stepProgress;
      response.getPublishStatus().progress = (int) moduleProgress;

      // Check for user abortion at loop start.
      if (response.getPublishStatus().processStatus == ProcessStatusType.ABORTED) {
        // Set abort flag and leave loop.
        this.abort = true;
        break;
      }

      // Set string for logging and the new URI from the rewrite mapping.
      String newUri = o.uri;
      if (!newRevision) {
        newUri = mapping.getImportObjectForLocalURI(URI.create(o.uri)).getTextgridUri();
      } else {
        // Use base URI here!
        newUri = o.uri.substring(0, o.uri.indexOf("."));
      }
      String loguri = "[" + newUri + "] ";

      // Create objects to read from and to write to.
      Holder<MetadataContainerType> tgObjectMetadata = new Holder<MetadataContainerType>();
      Holder<DataHandler> tgObjectData = new Holder<DataHandler>();

      // Do #READ.
      String newProjectId = projectId;
      try {
        tgcrudMTOM.read(sid, log, o.uri, tgObjectMetadata, tgObjectData);

        // Set project ID for destination object: Use PID of source object if new revision, use
        // given PID if not!
        if (newRevision) {
          newProjectId =
              tgObjectMetadata.value.getObject().getGeneric().getGenerated().getProject().getId();
        }

        message = loguri + SUCCESS_READING + o.uri;
        PublishUtils.setStatusRunningCheckAbort(this.step, message, response, this, NO_DRYRUN);

      } catch (ObjectNotFoundFault e) {
        message = loguri + ERROR_READING + o.uri + ": "
            + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
        PublishUtils.setStatusRunning(this.step, message, response, this, NO_DRYRUN);
        o.addError(ErrorType.NOT_SPECIFIED, message);
        this.errors++;
      } catch (MetadataParseFault e) {
        message = loguri + ERROR_READING + o.uri + ": "
            + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
        PublishUtils.setStatusRunning(this.step, message, response, this, NO_DRYRUN);
        o.addError(ErrorType.NOT_SPECIFIED, message);
        this.errors++;
      } catch (IoFault e) {
        message = loguri + ERROR_READING + o.uri + ": "
            + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
        PublishUtils.setStatusRunning(this.step, message, response, this, NO_DRYRUN);
        o.addError(ErrorType.NOT_SPECIFIED, message);
        this.errors++;
      } catch (ProtocolNotImplementedFault e) {
        message = loguri + ERROR_READING + o.uri + ": "
            + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
        PublishUtils.setStatusRunning(this.step, message, response, this, NO_DRYRUN);
        o.addError(ErrorType.NOT_SPECIFIED, message);
        this.errors++;
      } catch (AuthFault e) {
        message = loguri + ERROR_READING + o.uri + ": "
            + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
        PublishUtils.setStatusRunning(this.step, message, response, this, NO_DRYRUN);
        o.addError(ErrorType.AUTH, message);
        this.errors++;
      }

      // Add isDerivedFrom relation to the metadata.
      RelationType relations = tgObjectMetadata.value.getObject().getRelations();
      if (relations == null) {
        relations = new RelationType();
      }
      relations.setIsDerivedFrom(o.uri);
      tgObjectMetadata.value.getObject().setRelations(relations);

      // Rewrite metadata (only if no new revisions shall be created).
      // FIXME Why not rewriting if new revision??
      if (!newRevision) {
        tgObjectMetadata.value = rewriteMetadata(o, response, mapping, tgObjectMetadata.value);
      }

      // Get object mimetype
      String objectMimetype =
          tgObjectMetadata.value.getObject().getGeneric().getProvided().getFormat();

      // Rewrite data if necessary, and only if no new revisions shall be created and it's a
      // rewritable mimetype.
      // FIXME Why not rewriting if new revision??

      if (!newRevision && (TextGridMimetypes.AGGREGATION_SET.contains(objectMimetype)
          || TextGridMimetypes.ORIGINAL_SET.contains(objectMimetype)
          || objectMimetype.equals(TextGridMimetypes.LINKEDITOR)
          || objectMimetype.equals(TextGridMimetypes.XSD))) {

        message = loguri + "Rewriting URIs for format " + objectMimetype;
        PublishUtils.setStatusRunningCheckAbort(this.step, message, response, this, NO_DRYRUN);

        tgObjectData.value = rewriteData(o, response, objectMimetype, mapping, tgObjectData.value);

        // If rewriting failed, do retrieve the data again and just copy.
        try {
          if (tgObjectData.value.getInputStream().available() == 0) {
            message = loguri + "Rewriting URIs failed! COPYING ONLY!";
            PublishUtils.setStatusRunning(this.step, message, response, this, NO_DRYRUN);

            // Add error!
            message = "Rewriting URIs failed due to non well-formed XML file!";
            o.addError(ErrorType.NOT_SPECIFIED, message);

            // Read the file again, omit metadata, we already modified them! Just use the data
            // handler retrieved for writing!
            tgcrudMTOM.read(sid, log, o.uri, new Holder<MetadataContainerType>(), tgObjectData);
          } else {
            message = loguri + "URI rewriting complete";
            PublishUtils.setStatusRunningCheckAbort(this.step, message, response, this, NO_DRYRUN);
          }
        } catch (IOException | IoFault | MetadataParseFault | ObjectNotFoundFault | AuthFault
            | ProtocolNotImplementedFault e) {
          message = loguri + ERROR_READING + o.uri + ": " + e.getMessage();
          PublishUtils.setStatusRunning(this.step, message, response, this, NO_DRYRUN);
          o.addError(ErrorType.NOT_SPECIFIED, message);
          this.errors++;
        }
      }

      // DO #WRITE.
      try {

        // Do #CREATE using the new URI (or create new revision).
        tgcrudMTOM.create(sid, log, newUri, (Boolean) newRevision, newProjectId, tgObjectMetadata,
            tgObjectData.value);

        message =
            loguri + SUCCESS_CREATING + o.uri + " (" + response.getPublishStatus().progress + "%)";
        PublishUtils.setStatusRunningCheckAbort(this.step, message, response, this, NO_DRYRUN);

        // Set object status to OK if copying has been completed.
        o.destUri = tgObjectMetadata.value.getObject().getGeneric().getGenerated().getTextgridUri()
            .getValue();
        o.status = StatusType.OK;

      } catch (ObjectNotFoundFault e) {
        message = loguri + ERROR_CREATING + o.uri + ": "
            + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
        PublishUtils.setStatusRunning(this.step, message, response, this, NO_DRYRUN);
        o.addError(ErrorType.NOT_SPECIFIED, message);
        this.errors++;
      } catch (MetadataParseFault e) {
        message = loguri + ERROR_CREATING + o.uri + ": "
            + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
        PublishUtils.setStatusRunning(this.step, message, response, this, NO_DRYRUN);
        o.addError(ErrorType.NOT_SPECIFIED, message);
        this.errors++;
      } catch (IoFault e) {
        message = loguri + ERROR_CREATING + o.uri + ": "
            + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
        PublishUtils.setStatusRunning(this.step, message, response, this, NO_DRYRUN);
        o.addError(ErrorType.NOT_SPECIFIED, message);
        this.errors++;
      } catch (AuthFault e) {
        message = loguri + ERROR_CREATING + o.uri + ": "
            + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
        PublishUtils.setStatusRunning(this.step, message, response, this, NO_DRYRUN);
        o.addError(ErrorType.AUTH, message);
        this.errors++;
      }
    }

    // Only set process status, if not aborted by user.
    if (this.abort) {
      message = "PROCESS ABORTED BY USER";
      PublishUtils.setStatusAborted(this.step, message, response, this, NO_DRYRUN);
    } else if (this.errors != 0 || this.abort) {
      message =
          "Creation of " + this.errors + " object" + (this.errors != 1 ? "s" : "") + " failed";
      PublishUtils.setStatusError(this.step, message, response, this, NO_DRYRUN);
    } else {
      message = "Creation of objects complete";
      PublishUtils.setStatusDoneNoProgressIncrease(this.step, message, response, this, NO_DRYRUN);
    }

    // Write response to the response.
    PublishUtils.updateResponse(uuid, response);
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Rewrites all possible URIs in the data.
   * </p>
   * 
   * @param theObject
   * @param theResponse
   * @param theMimetype
   * @param theMapping
   * @param theData
   * @return
   */
  private DataHandler rewriteData(PublishObject theObject, PublishResponse theResponse,
      String theMimetype, ImportMapping theMapping, DataHandler theData) {

    // Set result to current data.
    // FIXME WHY??
    DataHandler result = theData;

    String loguri = "["
        + theMapping.getImportObjectForLocalURI(URI.create(theObject.uri)).getTextgridUri() + "] ";

    // Rewrite old URIs to new UrRIs in data files.
    try {
      InputStream is = null;

      // Rewrite aggregation data file.
      if (TextGridMimetypes.AGGREGATION_SET.contains(theMimetype)) {
        is = rewriteRoot(theMapping, MODE_AGGREGATION_REWRITE, theData.getInputStream());
      }
      // Rewrite TEI data file.
      else if (TextGridMimetypes.ORIGINAL_SET.contains(theMimetype)) {
        is = rewriteRoot(theMapping, MODE_TEI_REWRITE, theData.getInputStream());
      }
      // Rewrite link editor data files.
      else if (theMimetype.equals(TextGridMimetypes.LINKEDITOR)) {
        is = rewriteRoot(theMapping, MODE_LINKEDITOR_REWRITE, theData.getInputStream());
      }
      // Rewrite XML schema data files.
      else if (theMimetype.equals(TextGridMimetypes.XSD)) {
        is = rewriteRoot(theMapping, MODE_XSD_REWRITE, theData.getInputStream());
      }

      // Set data handler.
      if (is != null) {
        result = new DataHandler(new ByteArrayDataSource(is, TextGridMimetypes.OCTET_STREAM));
      }
    } catch (IOException e) {
      theObject.addError(ErrorType.NOT_SPECIFIED, loguri + e.getMessage());
      this.errors++;
    }

    return result;
  }

  /**
   * <p>
   * Rewrites all possible URIs in the metadata.
   * </p>
   * 
   * @param theObject
   * @param theResponse
   * @param theMapping
   * @param theMetadata
   * @return
   */
  private MetadataContainerType rewriteMetadata(PublishObject theObject,
      PublishResponse theResponse, ImportMapping theMapping, MetadataContainerType theMetadata) {

    // Set result to current data.
    MetadataContainerType result = theMetadata;
    String loguri = "["
        + theMapping.getImportObjectForLocalURI(URI.create(theObject.uri)).getTextgridUri() + "] ";
    String message = loguri + "Rewriting URIs for metadata";
    PublishUtils.setStatusRunningCheckAbort(this.step, message, theResponse, this, NO_DRYRUN);

    // Create InputStream for metadata rewriting from metadata container. No streaming needed
    // because because we only have small metadata files.
    ByteArrayOutputStream metadataStream = new ByteArrayOutputStream();
    JAXB.marshal(theMetadata, metadataStream);
    ByteArrayInputStream metadataInputStream =
        new ByteArrayInputStream(metadataStream.toByteArray());

    // Rewrite metadata.
    try {
      InputStream is = rewriteRoot(theMapping, MODE_METADATA_REWRITE, metadataInputStream);
      result = JAXB.unmarshal(is, MetadataContainerType.class);
    } catch (IOException e) {
      theObject.addError(ErrorType.NOT_SPECIFIED, loguri + e.getMessage());
      this.errors++;
    }

    message = loguri + "URI rewriting complete";
    PublishUtils.setStatusRunningCheckAbort(this.step, message, theResponse, this, NO_DRYRUN);

    return result;
  }

  /**
   * <p>
   * Rewrites data from input streams using an import mapping and a rewrite mode.
   * </p>
   * 
   * @param theMapping
   * @param theMode
   * @param theStream
   * @return
   * @throws IOException
   */
  private static InputStream rewriteRoot(ImportMapping theMapping, String theMode,
      final InputStream theStream) throws IOException {

    // Create piped streams.
    PipedInputStream result = new PipedInputStream(DEFAULT_BUFFER_SIZE);
    final PipedOutputStream out = new PipedOutputStream(result);

    // Create and configure the rewriter.
    final ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(theMapping, false);
    rewriter.recordMissingReferences();
    rewriter.configure(URI.create(theMode));

    // Now do rewrite using the piped streams. Start a new thread that writes to the Pipe
    // (OutputStream > InputStream)...
    Thread thread = new Thread("RewriteOutputInputPipe") {
      @Override
      public void run() {
        try {
          rewriter.rewrite(theStream, out);
        } catch (XMLStreamException e) {
          // DEBUG OUTPUT
          // System.out.println("rewriteRoot(): Error rewriting due to XMLStreamException: " +
          // e.getMessage());
          // DEBUG OUTPUT
        } catch (IOException e) {
          // DEBUG OUTPUT
          // System.out.println("rewriteRoot(): Error rewriting due to IOException: " +
          // e.getMessage());
          // DEBUG OUTPUT
        } finally {
          try {
            out.close();
          } catch (IOException e) {
            // Just ignore.
          }
        }
      }
    };

    thread.start();

    // DEBUG OUTPUT
    // Set<String> set = rewriter.getMissingReferences();
    // if (set.size() > 0) {
    // System.out.println("rewriteRoot(): Detected " + set.size() + " missing reference" +
    // (set.size() != 1 ? "s" : "") + ":");
    // for (String s : set) {
    // System.out.println(s);
    // }
    // }
    // DEBUG OUTPUT

    return result;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData(de.
   * langzeitarchivierung.kolibri.ProcessData)
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

}
