/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import org.elasticsearch.client.RestHighLevelClient;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.publish.util.ElasticSearchUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.TGElasticSearchUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.common.elasticsearch.ESJsonBuilder;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;

/**
 * <p>
 * Copies the dynamic ElasticSearch index to the 2nd (static) ElasticSearch Index.
 * </p>
 * 
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2023-05-05
 * @since 2013-10-02
 */

public class CopyElasticSearchIndex implements ActionModule {

  // **
  // STATIC FINAL
  // **

  private static final String INIT_ERROR = "Error initializing ElasticSearch";

  // **
  // STATE
  // **

  private Step step;
  private ProcessData processData;
  private boolean dryRun;
  private String elasticSearchReadHost;
  private String elasticSearchWriteHost;
  private List<Integer> elasticSearchReadPorts = new ArrayList<Integer>();
  private List<Integer> elasticSearchWritePorts = new ArrayList<Integer>();
  private String uri;
  private boolean anyError = false;
  private PublishResponse response;
  private String indexNameNonpublic;
  private String indexNamePublic;
  private RestHighLevelClient readClient = null;
  private RestHighLevelClient writeClient = null;
  private ESJsonBuilder esJson = null;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Just takes the 1st Index and copies all new data to the 2nd index.
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    this.uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
    this.dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
    this.response = (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

    // Start log entry.
    String message = "Copying dynamic ElasticSearch index to static ElasticSearch index";
    PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);

    // Check hosts.
    if (this.elasticSearchReadHost.startsWith("http://")) {
      message = "Setting ElasticSearch read host to " + this.elasticSearchReadHost;
      PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);
    } else {
      finishWithError(ErrorType.NOT_SPECIFIED, "ElasticSearch read host not set correctly");
    }
    if (this.elasticSearchWriteHost.startsWith("http://")) {
      message = "Setting ElasticSearch write host to " + this.elasticSearchWriteHost;
      PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);
    } else {
      finishWithError(ErrorType.NOT_SPECIFIED, "ElasticSearch write host not set correctly");
    }

    // Initialise the ElasticSearch client.
    try {
      // Get index names and types.
      this.indexNameNonpublic = ElasticSearchUtils.getIndexName(this.elasticSearchReadHost);
      this.indexNamePublic = ElasticSearchUtils.getIndexName(this.elasticSearchWriteHost);
      init();
    } catch (TransformerConfigurationException | MalformedURLException
        | TransformerFactoryConfigurationError | IoFault | SAXException
        | ParserConfigurationException e) {
      finishWithError(ErrorType.NOT_SPECIFIED, INIT_ERROR + ": " + e.getMessage());
      e.printStackTrace();
      return;
    }

    // Loop.
    for (PublishObject o : this.response.getPublishObjects()) {

      String logPrefix = "[" + o.uri + "] ";

      // Only if object is not already published!
      if (!o.status.equals(StatusType.ALREADY_PUBLISHED)) {

        URI uri = URI.create(o.uri);

        // Read from ElasticSearch read host.
        String readRes = null;
        try {
          message = logPrefix + "Looking for " + o.uri + " in dynamic ElasticSearch index";
          PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);

          readRes = new TGElasticSearchUtils().getObject(this.readClient, uri,
              this.indexNameNonpublic);
        } catch (IOException e) {
          finishWithError(ErrorType.NOT_SPECIFIED,
              "Error reading from dynamic ElasticSearch index: " + e.getMessage());
        }

        if (!this.dryRun) {
          try {
            message = logPrefix + "Found and copying " + uri;
            PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);

            // Write to ElasticSearch write host.
            new TGElasticSearchUtils().createObject(this.writeClient, uri, readRes,
                this.indexNamePublic);
          } catch (IOException e) {
            finishWithError(ErrorType.NOT_SPECIFIED,
                "Error writing to static ElasticSearch index: " + e.getMessage());
          }
        } else {
          message = logPrefix + "Found " + uri;
          PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);
        }
      }

      // If already published.
      else {
        message = logPrefix + "URI is already published and will not be copied";
        PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);
      }
    }

    // Finally write response to the responseMap.
    finish();

    // Finally close ElasticSearch connections.
    try {
      this.readClient.close();
      this.writeClient.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /**
   * <p>
   * Add error to response and set status of actionmodule to error.
   * </p>
   * 
   * @param publishObject
   * @param type
   * @param message
   */
  protected void setError(PublishObject publishObject, ErrorType type, String message) {
    this.anyError = true;
    publishObject.addError(type, message);
    PublishUtils.setStatusError(this.step, message, this.response, this, this.dryRun);
  }

  /**
   * <p>
   * Add error without publish object.
   * </p>
   * 
   * @param type
   * @param message
   */
  protected void setError(ErrorType type, String message) {
    this.anyError = true;
    PublishUtils.setStatusError(this.step, message, this.response, this, this.dryRun);
  }

  /**
   * <p>
   * Call before exit: check if there are errors to set module state before returning - update
   * response map.
   * </p>
   */
  private void finish() {
    if (this.anyError) {
      PublishUtils.setStatusError(this.step, "Finished with Error", this.response, this,
          this.dryRun);
    } else {
      PublishUtils.setStatusDone(this.step, "Index copy complete", this.response, this,
          this.dryRun);
    }

    // Write response to the response.
    PublishUtils.updateResponse(this.uri, this.response);
  }

  /**
   * <p>
   * Add error without publish object.
   * </p>
   * 
   * @param type
   * @param message
   */
  private void finishWithError(ErrorType type, String message) {
    setError(type, message);
    finish();
  }

  /**
   * <p>
   * Initialize the ES clients and the ES JSON objects once.
   * </p>
   * 
   * TODO Refactor to static ES client (maybe in TGPublishAbs?), see also
   * /kolibri-tgpublish-service/src/main/java/de/langzeitarchivierung/kolibri/actionmodule/textgrid/publish/ReleaseNearlyPublishedRelation.java.
   * 
   * @throws TransformerConfigurationException
   * @throws TransformerFactoryConfigurationError
   * @throws IoFault
   * @throws MalformedURLException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  private void init()
      throws TransformerConfigurationException, TransformerFactoryConfigurationError, IoFault,
      MalformedURLException, SAXException, ParserConfigurationException {

    // Get read and write clients as singleton.
    if (this.readClient == null) {
      String message = "Creating new ElasticSearch client to read from: " + "[indexname:"
          + this.indexNameNonpublic + "], [clustername:" + this.elasticSearchReadPorts + "]";
      PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);

      this.readClient = ElasticSearchUtils.getElasticSearchClient(this.elasticSearchReadHost,
          this.elasticSearchWritePorts);
    }
    if (this.writeClient == null) {
      String message = "Creating new ElasticSearch client to write to: " + "[indexname:"
          + this.indexNamePublic + "], [clustername:"
          + this.elasticSearchWritePorts + "]";
      PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);

      this.writeClient = ElasticSearchUtils.getElasticSearchClient(this.elasticSearchWriteHost,
          this.elasticSearchWritePorts);
    }

    // Get JSON builder as singleton.
    if (this.esJson == null) {
      String message = "Creating new ElasticSearch builder";
      PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);

      this.esJson = new ESJsonBuilder();
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   *
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   *
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param host
   */
  public void setElasticSearchReadHost(String host) {
    this.elasticSearchReadHost = host;
  }

  /**
   * @param host
   */
  public void setElasticSearchWriteHost(String host) {
    this.elasticSearchWriteHost = host;
  }

  /**
   * @param clusterName
   */
  public void setElasticSearchReadPorts(int port) {
    this.elasticSearchReadPorts.add(port);
  }

  /**
   * @param clusterName
   */
  public void setElasticSearchWritePorts(int port) {
    this.elasticSearchWritePorts.add(port);
  }

}
