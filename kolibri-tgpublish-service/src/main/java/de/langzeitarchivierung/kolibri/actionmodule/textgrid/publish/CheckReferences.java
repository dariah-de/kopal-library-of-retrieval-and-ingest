///**
// * This software is copyright (c) 2018 by
// * 
// * TextGrid Consortium (http://www.textgrid.de)
// * 
// * DAASI International GmbH (http://www.daasi.de)
// *
// * This is free software. You can redistribute it and/or modify it under the terms described in the
// * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
// * download it from
// *
// * http://www.gnu.org/licenses/lgpl-3.0.txt
// *
// * @copyright TextGrid Consortium (http://www.textgrid.de)
// * @copyright DAASI International GmbH (http://www.daasi.de)
// * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
// * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
// * @author Stefan E. Funk (stefan.e.funk@daasi.de)
// * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
// **/
//
//package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.net.MalformedURLException;
//import java.util.HashMap;
//import java.util.List;
//import org.apache.http.entity.mime.HttpMultipartMode;
//import org.apache.http.entity.mime.MultipartEntity;
//import org.apache.http.entity.mime.content.StringBody;
//import de.langzeitarchivierung.kolibri.ProcessData;
//import de.langzeitarchivierung.kolibri.Step;
//import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
//import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
//import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
//import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
//import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
//import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
//import info.textgrid.middleware.tgpublish.api.jaxb.ReferencedUris;
//import info.textgrid.utils.existclient.ExistStatusCodes;
//import info.textgrid.utils.httpclient.TGHttpClient;
//import info.textgrid.utils.httpclient.TGHttpResponse;
//import jakarta.xml.bind.JAXB;
//
///**
// * <p>
// * Checks if there are references in any original encoded XML file, and returns a list of TextGrid
// * URIs per object, that is NOT included in the edition/collection.
// * </p>
// * 
// * @author Stefan E. Funk, DAASI International
// * @version 2018-08-15
// * @since 2011-01-17
// **/
//
//public class CheckReferences implements ActionModule {
//
//  // **
//  // FINALS
//  // **
//
//  private static final char URI_LIST_SEPARATION_CHAR = ',';
//  private static final String EDITION_URI_KEY = "editionuri";
//  private static final String URI_LIST_KEY = "hugo";
//
//  // **
//  // STATE
//  // **
//
//  private Step step;
//  private ProcessData processData;
//  private boolean dryRun;
//  private String xmldbQueryPrefix;
//  private String xmldbUser;
//  private String xmldbPassword;
//
//  // **
//  // MANIPULATION
//  // **
//
//  /**
//   * <p>
//   * Checks all references in all original encoded objects.
//   * </p>
//   * 
//   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
//   */
//  public void go() {
//
//    // Get the data out of the custom data.
//    HashMap<Object, Object> customData = this.processData.getCustomData();
//
//    String uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
//    this.dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
//    Boolean ignoreWarnings = (Boolean) customData.get(TGPublishServiceImpl.IGNORE_WARNINGS);
//    PublishResponse response =
//        (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);
//
//    // Start log entry.
//    String message =
//        "Checking if all referenced objects are existing within the object to publish (ignoreWarnings: "
//            + ignoreWarnings + ")";
//    PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
//
//    try {
//      // Build HTTP multipart message.
//      String uriList = buildUriList(response);
//      MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//      entity.addPart(EDITION_URI_KEY, new StringBody(uri));
//      entity.addPart(URI_LIST_KEY, new StringBody(uriList));
//
//      // Get HTTP client.
//      TGHttpClient httpClient =
//          new TGHttpClient(this.xmldbQueryPrefix, this.xmldbUser, this.xmldbPassword);
//
//      // Get HTTP POST.
//      TGHttpResponse httpResponse = httpClient.post("", entity);
//
//      message = "HTTP POST query params: host=" + this.xmldbQueryPrefix + ", " + EDITION_URI_KEY
//          + "=" + uri + ", " + URI_LIST_KEY + "=" + uriList;
//      PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
//
//      // Evaluate status code.
//      int statusCode = httpResponse.getStatusCode();
//
//      InputStream responseStream = httpResponse.getBuffEntity().getContent();
//      if (statusCode != ExistStatusCodes.OK) {
//        throw new IOException("HTTP GET request failed with status code: " + statusCode);
//      }
//
//      message = "HTTP GET status code: " + statusCode;
//      PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
//
//      // Parse response.
//      PublishResponse references = JAXB.unmarshal(responseStream, PublishResponse.class);
//
//      // Merge lists, if references response is not empty.
//      if (!references.getPublishObjects().isEmpty()) {
//
//        message =
//            "There are URIs referenced in some objects, that are not part of the object to publish";
//
//        if (ignoreWarnings) {
//          // Continue with info message only, if warnings shall be ignored, do NOT merge lists.
//          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
//        } else {
//          // Merge lists only if warnings are not ignored and existing.
//          mergeLists(response, references);
//
//          // Return with error if references are existing.
//          PublishUtils.setStatusError(this.step, message, response, this, this.dryRun);
//          // Write response to the response.
//          PublishUtils.updateResponse(uri, response);
//          return;
//        }
//      }
//    } catch (MalformedURLException e) {
//      message = "XML database URL incorrect: " + e.getMessage();
//      PublishUtils.addGeneralError(this.step, ErrorType.NOT_SPECIFIED, message, response, this,
//          this.dryRun);
//      // Write response to the response.
//      PublishUtils.updateResponse(uri, response);
//      return;
//    } catch (IOException e) {
//      message = "Unable to access XML database: " + e.getMessage();
//      PublishUtils.addGeneralError(this.step, ErrorType.NOT_SPECIFIED, message, response, this,
//          this.dryRun);
//      // Write response to the response.
//      PublishUtils.updateResponse(uri, response);
//      return;
//    }
//
//    // Write response to the response.
//    PublishUtils.updateResponse(uri, response);
//
//    // Everything is OK.
//    message = "Reference check complete: All referenced URIs are part of the object to publish";
//    PublishUtils.setStatusDone(this.step, message, response, this, this.dryRun);
//  }
//
//  // **
//  // INTERNAL METHODS
//  // *
//
//  /**
//   * <p>
//   * Merges the publish response object and the referencedUri response object. Is also used by the
//   * JUnit test.
//   * </p>
//   * 
//   * @param theResponse
//   * @param theReferences
//   */
//  protected void mergeLists(PublishResponse theResponse, PublishResponse theReferences) {
//
//    // Loop over the objects in the referenced list, create URI map.
//    HashMap<String, ReferencedUris> uris = new HashMap<String, ReferencedUris>();
//    for (PublishObject o : theReferences.getPublishObjects()) {
//      uris.put(o.uri, o.getReferencedUris());
//    }
//
//    // Loop over the objects in the response list. Add every
//    // reference list if URIs match.
//    for (PublishObject o : theResponse.getPublishObjects()) {
//      if (uris.keySet().contains(o.uri)) {
//        o.setReferencedUris(uris.get(o.uri));
//      }
//    }
//  }
//
//  /**
//   * <p>
//   * Creates a separated list string from the publish object list.
//   * </p>
//   * 
//   * @param theResponse
//   * @return
//   */
//  private static String buildUriList(PublishResponse theResponse) {
//
//    String response = "";
//
//    // Get object list.
//    List<PublishObject> poList = theResponse.getPublishObjects();
//    for (PublishObject p : poList) {
//      // Add TextGrid URI.
//      response += p.uri + URI_LIST_SEPARATION_CHAR;
//    }
//
//    // Purge last separation char.
//    response = response.substring(0, response.length() - 1);
//
//    return response;
//  }
//
//  // **
//  // GETTERS AND SETTERS
//  // **
//
//  /*
//   * (non-Javadoc)
//   * 
//   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
//   * (de.langzeitarchivierung.kolibri.ProcessData)
//   */
//  public void setProcessData(ProcessData processData) {
//    this.processData = processData;
//  }
//
//  /*
//   * (non-Javadoc)
//   * 
//   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
//   * langzeitarchivierung.kolibri.Step)
//   */
//  public void setStep(Step step) {
//    this.step = step;
//  }
//
//  /**
//   * @param xmldbQueryPrefix
//   */
//  public void setXmldbQueryPrefix(String xmldbQueryPrefix) {
//    this.xmldbQueryPrefix = xmldbQueryPrefix;
//  }
//
//  /**
//   * @param user
//   */
//  public void setXmldbUser(String user) {
//    this.xmldbUser = user;
//  }
//
//  /**
//   * @param pw
//   */
//  public void setXmldbPassword(String pw) {
//    this.xmldbPassword = pw;
//  }
//
//}
