/*******************************************************************************
 * This software is copyright (c) 2015 by
 * 
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.copy;

import java.util.HashMap;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;

/*******************************************************************************
 * <p>
 * Just sets the status DONE and marks copying started.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2015-01-20
 * @since 2012-03-13
 ******************************************************************************/

public class CopyStart implements ActionModule {

	// **
	// STATE
	// **

	ProcessData	processData;
	Step		step;

	// **
	// CREATION
	// **

	public CopyStart() {
		super();
	}

	// **
	// MANIPULATION
	// **

	/**
	 * <p>
	 * <b>NEEDS</b>
	 * <ul>
	 * <li>Nothing.</li>
	 * </ul>
	 * </p>
	 * 
	 * <p>
	 * <b>GIVES</b>
	 * <ul>
	 * <li>Increases the progress in percent.</li>
	 * </ul>
	 * </p>
	 * 
	 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
	 */
	@Override
  public void go() {

		// Get the data out of the custom data.
		HashMap<Object, Object> customData = this.processData.getCustomData();

		String uuid = (String) customData
				.get(TGPublishServiceImpl.SESSION_UUID);
		PublishResponse response = (PublishResponse) customData
				.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

		// Set module to status DONE.
		String message = "COPY PROCESS STARTED!!";
		PublishUtils.setStatusDone(this.step, message, response, this, false);

		// Finally write response to the response.
		PublishUtils.updateResponse(uuid, response);
	}

	// **
	// GET & SET METHODS
	// **

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
	 * (de.langzeitarchivierung.kolibri.ProcessData)
	 */
	@Override
  public void setProcessData(ProcessData processData) {
		this.processData = processData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
	 * langzeitarchivierung.kolibri.Step)
	 */
	@Override
  public void setStep(Step step) {
		this.step = step;
	}

}
