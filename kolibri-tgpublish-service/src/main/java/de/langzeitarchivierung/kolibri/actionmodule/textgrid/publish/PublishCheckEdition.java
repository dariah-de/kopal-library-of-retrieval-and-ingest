/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt GNU)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.util.HashMap;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2024-03-07 - Funk - Set this.response.objectListComplete = true in finish(), we need to set this
 * for error states, too. If not, we get a false publish response! Fixes #244
 * 
 * 2017-02-28 - Funk - Corrected incorrect XPath handling. Now we do check empty <item> objects,
 * too!
 * 
 **/

/**
 * <p>
 * Checks if the given URI really is a TextGrid Edition/Collection. Builds up object list. Checks
 * metadata for validity.
 * </p>
 * 
 * @version 2024-08-23
 * @since 2011-01-11
 **/

public class PublishCheckEdition extends PublishCheck {

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Checks an edition aggregation if it is suitable for being published, this check includes:
   * </p>
   * 
   * <ol>
   * <li>loading the data and metadata for the incoming object</li>
   * <li>collection or edition</li>
   * <li>if edition, is it assigned to a work</li>
   * <li>does the user have rights to publish the object and the items contained</li>
   * </ol>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    this.uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
    this.sid = (String) customData.get(TGPublishServiceImpl.SESSION_ID);
    this.log = (String) customData.get(TGPublishServiceImpl.LOG_PARAMETER);
    this.response = (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);
    this.ignoreWarnings = (Boolean) customData.get(TGPublishServiceImpl.IGNORE_WARNINGS);
    this.dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);

    // Start log entry.
    String message = "Checking if content type publishable [" + this.uri + "]";
    PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);

    // Reference on requested object is hold to change state later if necessary.
    this.incomingObject = new PublishObject(this.uri);
    this.response.addPublishObject(this.incomingObject);

    Response searchResponse =
        TGPublishAbs.getTgsearch().infoQuery().setSid(this.sid).getMetadata(this.uri);
    if (searchResponse.getResult() == null || searchResponse.getResult().size() < 1) {
      finishWithError(this.incomingObject, ErrorType.AUTH,
          "URI not existing or access denied [" + this.uri + "]");
      return;
    }

    ResultType result = searchResponse.getResult().get(0);

    // Check if (root edition or collection) is already published.
    if (alreadyPublished(this.uri)) {
      // Get current publish object, and set status.
      this.response.getPublishObjects().get(0).status = StatusType.ALREADY_PUBLISHED;

      PublishUtils.setStatusError(this.step, "Object has already been published", this.response,
          this, this.dryRun);

      // Update response and return.
      PublishUtils.updateResponse(this.uri, this.response);
      return;
    } else {
      PublishUtils.setStatusRunning(this.step, "Object has not yet been published", this.response,
          this, this.dryRun);
    }

    // Content-Type must be collection or edition!
    String contentType = result.getObject().getGeneric().getProvided().getFormat();

    if (contentType.equals(TextGridMimetypes.EDITION)
        || contentType.equals(TextGridMimetypes.COLLECTION)) {
      checkAggregation(result.getObject(), this.sid, this.response);
    } else {
      finishWithError(this.incomingObject, ErrorType.WRONG_CONTENT_TYPE,
          "Content type of [" + this.uri + "] is not publishable (" + contentType
              + ")! Needs to be " + TextGridMimetypes.EDITION + " or "
              + TextGridMimetypes.COLLECTION + "!");
      return;
    }

    // Check if warnings are existing in the metadata.
    if (checkWarnings(result)) {
      return;
    }

    // Finally check all publish rights (skip in dry-run!)
    if (!this.dryRun) {
      checkPublishRights();
    }

    // Call finish before leaving go().
    finish();
  }

}
