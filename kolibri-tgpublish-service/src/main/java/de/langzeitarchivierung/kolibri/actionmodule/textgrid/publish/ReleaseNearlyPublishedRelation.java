/**
 * This software is copyright (c) 2022 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.script.Script;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.publish.util.ElasticSearchUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.TGElasticSearchUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.common.elasticsearch.ESJsonBuilder;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.utils.sesameclient.SesameClient;

/**
 * <p>
 * Removes the isNearlyPublished relation data from the Sesame and/or ES databases. This class is
 * used for final publishing after sandbox nearlyPublish.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2022-06-13
 * @since 2011-10-10
 */

public class ReleaseNearlyPublishedRelation implements ActionModule {

  // **
  // FINALS
  // **

  private static final String RDF_NAMESPACE = "http://textgrid.info/relation-ns";
  private static final String REL_ISNEARLYPUBLISHED = "#isNearlyPublished";
  private static final String IDX_NEARLYPUBLISHED_KEY = "nearlyPublished";
  private static final String ERROR_CREATING = "Error deleting nearlyPublish flag!";

  // **
  // STATE
  // **

  private Step step;
  private ProcessData processData;
  private boolean dryRun;
  private String sesameHost;
  private String sesameUser;
  private String sesamePassword;
  private String elasticSearchHost;
  private List<Integer> elasticSearchPorts = new ArrayList<Integer>();
  private PublishResponse response;
  private String indexName;
  private String indexType;
  private boolean removeFromRdfDatabase = true;
  private boolean removeFromIdxDatabase = true;
  private RestHighLevelClient elasticSearchClient;
  private ESJsonBuilder esJson;

  // **
  // STATICS
  // **

  private static SesameClient sesameClient;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Just deletes the isNearlyPublished relation data or the given object(s).
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
    this.dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
    this.response = (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

    // Start log entry.
    String message =
        "Releasing NP relation from " + (this.removeFromRdfDatabase ? "RDF database" : "")
            + (this.removeFromIdxDatabase && this.removeFromRdfDatabase ? " and " : "")
            + (this.removeFromIdxDatabase ? "ElasticSearch database" : "");
    PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);

    // Delete here!!
    try {
      // Get Sesame client, if not yet existing.
      if (sesameClient == null && this.removeFromRdfDatabase) {
        sesameClient = new SesameClient(this.sesameHost, this.sesameUser, this.sesamePassword);
      }

      // Get ElasticSearch client.
      if (this.removeFromIdxDatabase) {
        this.indexName = ElasticSearchUtils.getIndexName(this.elasticSearchHost);
        this.indexType = ElasticSearchUtils.getIndexType(this.elasticSearchHost);
        init();
      }

      // Adding URI to PublishObject if only called with root URI.
      // NOTE This is only used with koLibRI TextGrid import module right now! Maybe we can put in a
      // URI list, too?
      if (this.response.getPublishObjects().isEmpty()) {
        this.response.getPublishObjects().add(new PublishObject(uri));
      }

      // Loop every URI in the list.
      for (PublishObject o : this.response.getPublishObjects()) {
        if (!this.dryRun) {

          // Delete from Sesame DB.
          if (this.removeFromRdfDatabase) {

            PublishUtils.setStatusRunning(this.step,
                "Deleting RDF database " + REL_ISNEARLYPUBLISHED + "...", this.response, this,
                this.dryRun);

            int sesameStatus =
                sesameClient.delete(o.uri, RDF_NAMESPACE + REL_ISNEARLYPUBLISHED, "");

            PublishUtils.setStatusRunning(this.step,
                "RDF database " + REL_ISNEARLYPUBLISHED + " deleted, status: " + sesameStatus,
                this.response, this, this.dryRun);
          }

          // Delete from ElasticSearch DB.
          if (this.removeFromIdxDatabase) {
            String uriWithout = new TGElasticSearchUtils().forgetUriPrefix(URI.create(o.uri));

            PublishUtils.setStatusRunning(this.step,
                "Deleting ElasticSearch " + IDX_NEARLYPUBLISHED_KEY + "...", this.response, this,
                this.dryRun);

            UpdateRequest elasticSearchRequest =
                new UpdateRequest(this.indexName, this.indexType, uriWithout);

            Script removeFlagScript =
                new Script("ctx._source.remove('" + IDX_NEARLYPUBLISHED_KEY + "')");

            elasticSearchRequest.script(removeFlagScript);
            UpdateResponse elasticSearchResponse =
                elasticSearchClient.update(elasticSearchRequest, RequestOptions.DEFAULT);

            if (elasticSearchResponse.status() != RestStatus.OK) {
              message = "Failure deleting nearlyPublish flag in ElasticSearch: "
                  + elasticSearchResponse.toString();
              throw new IoFault(message);
            }

            PublishUtils.setStatusRunning(this.step, "ElasticSearch " + IDX_NEARLYPUBLISHED_KEY
                + " deleted, id: " + elasticSearchResponse.getId(), this.response, this,
                this.dryRun);
          }
        } else {
          PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);
        }
      }
    } catch (IOException | TransformerConfigurationException | TransformerFactoryConfigurationError
        | IoFault | SAXException | ParserConfigurationException e) {
      message = ERROR_CREATING + " " + e.getMessage();
      PublishUtils.addGeneralError(this.step, ErrorType.NOT_SPECIFIED, message, this.response, this,
          this.dryRun);
      PublishUtils.updateResponse(uri, this.response);
      return;
    }

    // Finally write response to the response.
    PublishUtils.updateResponse(uri, this.response);

    message = "Release of NP relation"
        + (this.removeFromIdxDatabase && this.removeFromRdfDatabase ? "s" : "") + " complete";
    PublishUtils.setStatusDone(this.step, message, this.response, this, this.dryRun);

    // Finally close ElasticSearch connection.
    try {
      this.elasticSearchClient.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  // **
  // PRIVATE
  // **

  /**
   * <p>
   * Initialise the ES client and the ES JSON objects once.
   * </p>
   * 
   * TODO Refactor to static ES client (maybe in TGPublishAbs?), see also
   * /kolibri-tgpublish-service/src/main/java/de/langzeitarchivierung/kolibri/actionmodule/textgrid/publish/CopyElasticSearchIndex.java.
   * 
   * @throws TransformerConfigurationException
   * @throws TransformerFactoryConfigurationError
   * @throws IoFault
   * @throws MalformedURLException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  private void init()
      throws TransformerConfigurationException, TransformerFactoryConfigurationError, IoFault,
      MalformedURLException, SAXException, ParserConfigurationException {

    if (this.elasticSearchClient == null) {
      this.elasticSearchClient = ElasticSearchUtils.getElasticSearchClient(this.elasticSearchHost,
          this.elasticSearchPorts);

      String message = "Created new ElasticSearch client [" + this.elasticSearchClient.hashCode()
          + "]: " + this.elasticSearchHost + ", " + this.indexName + ", [" + this.indexType + "], ["
          + this.elasticSearchPorts + "]";
      PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);

    } else {
      String message =
          "Using existing ElasticSearch client [" + this.elasticSearchClient.hashCode() + "]";
      PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);
    }

    // Get JSON builder as singleton.
    if (this.esJson == null) {
      this.esJson = new ESJsonBuilder();

      String message = "Created new ElasticSearch builder [" + this.esJson.hashCode() + "]";
      PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);

    } else {
      String message = "Using existing ElasticSearch builder [" + this.esJson.hashCode() + "]";
      PublishUtils.setStatusRunning(this.step, message, this.response, this, this.dryRun);
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param host
   */
  public void setSesameHost(String host) {
    this.sesameHost = host;
  }

  /**
   * @param user
   */
  public void setSesameUser(String user) {
    this.sesameUser = user;
  }

  /**
   * @param pw
   */
  public void setSesamePassword(String pw) {
    this.sesamePassword = pw;
  }

  /**
   * @param host
   */
  public void setElasticSearchHost(String host) {
    this.elasticSearchHost = host;
  }

  /**
   * @param clusterName
   */
  public void setElasticSearchPorts(int port) {
    this.elasticSearchPorts.add(port);
  }

  /**
   * @param flag
   */
  public void setRemoveFromRdfDatabase(boolean flag) {
    this.removeFromRdfDatabase = flag;
  }

  /**
   * @param flag
   */
  public void setRemoveFromIdxDatabase(boolean flag) {
    this.removeFromIdxDatabase = flag;
  }

}
