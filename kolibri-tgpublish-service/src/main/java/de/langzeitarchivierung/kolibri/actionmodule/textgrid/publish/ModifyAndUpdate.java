/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 **/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.publish;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.stream.XMLStreamException;
import org.apache.http.HttpStatus;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.TGPublishUtils;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid._import.RewriteMethod;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Pid;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter;
import info.textgrid.utils.linkrewriter.ImportMapping;
import jakarta.activation.DataHandler;
import jakarta.mail.util.ByteArrayDataSource;
import jakarta.ws.rs.core.Response;
import jakarta.xml.bind.JAXB;
import jakarta.xml.ws.Holder;

/**
 * TODOLOG
 * 
 * TODO Have we to rewrite here metadata, TEI, and other files, too?
 * 
 * TODO Close streams used for rewriting!!
 * 
 * TODO Generalize classes: RenameAndRewrite (kolibri-addon-textgrid-import),
 * publish.ModifyAndUpdate (kolibri-tgpublish-service), copy.ModifyAndCreate
 * (kolibri-tgpublish-service)!
 **
 * CHANGELOG
 * 
 * 2024-09-02 - Funk - Add tgpid and tgsearch clients from TGPublishAbs.
 * 
 * 2024-08-23 - Funk - Fix #255: Add correct filesize and checksum to Handle metadata after
 * TG-crud#UPDATE.
 **/

/**
 * <p>
 * Modifies all references to be modified, updates the TextGrid metadata and, if necessary the data
 * (i.e. edition and collection files), and finally updates all the metadata and data using
 * TG-crud#UPDATEMETADATA or TG-crud#UPDATE.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-09-02
 * @since 2011-01-18
 **/

public class ModifyAndUpdate implements ActionModule {

  // **
  // FINAL STATIC
  // *

  protected static final String MODE_AGGREGATION_REWRITE = "internal:textgrid#aggregation";
  protected static final String MODE_METADATA_REWRITE = "internal:textgrid#metadata";

  private final static String PID_TYPE = "handle";
  private final static String AVAILIBILITY_PUBLIC = "public";
  private final static String PERMISSION_READ = "READ";
  private static final int PROGRESS_UPDATES_PER_MODULE = 1;
  private static final String ERROR = "Error: ";

  // **
  // STATE
  // **

  private Step step;
  private ProcessData processData;
  private boolean dryRun;
  private long issuedDate = System.currentTimeMillis();
  private boolean anyError = false;
  private boolean rewriteAggregationUrisToPids = false;
  private String tgpidSecret;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Do modify and update.
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
    String sid = (String) customData.get(TGPublishServiceImpl.SESSION_ID);
    String log = (String) customData.get(TGPublishServiceImpl.LOG_PARAMETER);
    this.dryRun = (Boolean) customData.get(TGPublishServiceImpl.DRY_RUN);
    PublishResponse response =
        (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

    // Start log entry.
    int objectAmount = response.getPublishObjects().size();
    String message =
        "Modifying and updating " + objectAmount + " object" + (objectAmount != 1 ? "s" : "");
    PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

    // Get progress this module has to cover and compute in how many steps we can divide this
    // module. Add read and create progress as single steps, and use creating rewrite map, and
    // rewriting metadata and data, too. So we have five time the amount of objects (use float for
    // computation for small values!).
    float moduleProgress = response.getPublishStatus().progress;
    float stepProgress = (float) PublishUtils.progressPerModule(this.processData)
        / (PROGRESS_UPDATES_PER_MODULE * (float) objectAmount);

    // Go through all objects and create rewrite mapping for the URI to PID mapping OR the base URI
    // to revision URI mapping.
    ImportMapping mapping = new ImportMapping();
    for (PublishObject o : response.getPublishObjects()) {
      URI objectURI = URI.create(o.uri);
      URI baseURI = URI.create(o.uri.substring(0, uri.indexOf(".")));
      if (this.rewriteAggregationUrisToPids) {
        URI objectPID = URI.create(o.pid);
        mapping.add(objectPID, objectURI, null, RewriteMethod.XML);
        // mapping.add(o.pid, o.uri, null, RewriteMethod.XML);
        mapping.add(objectPID, baseURI, null, RewriteMethod.XML);
        // mapping.add(objectPid, o.uri.substring(0, uri.indexOf(".")), null, RewriteMethod.XML);
      } else {
        mapping.add(objectURI, baseURI, null, RewriteMethod.XML);
        // mapping.add(o.uri, o.uri.substring(0, uri.indexOf(".")), null, RewriteMethod.XML);
      }
    }

    message = "Import mapping generated for rewriting "
        + (this.rewriteAggregationUrisToPids ? "" : "base") + " URIs to "
        + (this.rewriteAggregationUrisToPids ? "PIDs" : "revision URIs");
    PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

    // Go through all objects.
    for (PublishObject o : response.getPublishObjects()) {

      String loguri = "[" + o.uri + "] ";

      // Set progress per object.
      moduleProgress += stepProgress;
      response.getPublishStatus().progress = (int) moduleProgress;

      // Do #READMETADATA.
      MetadataContainerType metadata = null;
      try {
        metadata = TGPublishAbs.getTgcrud().readMetadata(sid, log, o.uri);
      } catch (ObjectNotFoundFault e) {
        message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
        o.addError(ErrorType.NOT_SPECIFIED, message);
        this.anyError = true;
      } catch (MetadataParseFault e) {
        message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
        o.addError(ErrorType.NOT_SPECIFIED, message);
        this.anyError = true;
      } catch (IoFault e) {
        message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
        o.addError(ErrorType.NOT_SPECIFIED, message);
        this.anyError = true;
      } catch (AuthFault e) {
        message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
        o.addError(ErrorType.AUTH, message);
        this.anyError = true;
      }

      // Only proceed, if metadata is not null.
      if (metadata == null) {
        // Just log the error here, setting of the response's data will follow below.
        message = loguri + "Metadata could NOT be read!";
        this.step.setStatus(Status.ERROR, message);
      } else {
        message = loguri + "TG-crud#READMETADATA complete (" + response.getPublishStatus().progress
            + "%)";
        PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

        String mimetype = metadata.getObject().getGeneric().getProvided().getFormat();
        message = loguri + "Detected mimetype " + mimetype;
        PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

        // Check format. Compare to TextGrid mimetypes from TG-crud.
        if (!TextGridMimetypes.AGGREGATION_SET.contains(mimetype)) {

          // Add issued date to metadata.
          try {
            setIssuedDate(metadata, response, loguri);
          } catch (DatatypeConfigurationException e) {
            message = loguri + e.getMessage();
            o.addError(ErrorType.NOT_SPECIFIED, message);
            this.anyError = true;
          }

          // Add PID to metadata.
          addPersistentIdentifier(metadata, response, loguri, o.pid);

          // Add availability = public to the metadata.
          metadata.getObject().getGeneric().getGenerated().setAvailability(AVAILIBILITY_PUBLIC);

          message = loguri + "Availibility set to: " + AVAILIBILITY_PUBLIC;
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

          // Change permissions to READ.
          metadata.getObject().getGeneric().getGenerated().setPermissions(PERMISSION_READ);

          message = loguri + "Permissions set to: " + PERMISSION_READ;
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

          // Do #UPDATEMETADATA (only if not dry run and not already published!).
          if (!this.dryRun && !o.status.equals(StatusType.ALREADY_PUBLISHED)) {
            Holder<MetadataContainerType> tgObjectMetadata =
                new Holder<MetadataContainerType>(metadata);
            try {
              TGPublishAbs.getTgcrud().updateMetadata(sid, log, tgObjectMetadata);
            } catch (ObjectNotFoundFault e) {
              message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
              o.addError(ErrorType.NOT_SPECIFIED, message);
              this.anyError = true;
            } catch (MetadataParseFault e) {
              message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
              o.addError(ErrorType.NOT_SPECIFIED, message);
              this.anyError = true;
            } catch (UpdateConflictFault e) {
              message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
              o.addError(ErrorType.NOT_SPECIFIED, message);
              this.anyError = true;
            } catch (IoFault e) {
              message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
              o.addError(ErrorType.NOT_SPECIFIED, message);
              this.anyError = true;
            } catch (AuthFault e) {
              message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
              o.addError(ErrorType.AUTH, message);
              this.anyError = true;
            }

            message = loguri + "TG-crud#UPDATEMETADATA complete";
            PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
          }
        }

        // Exchange the references in all aggregation data and metadata files from TextGrid, if an
        // aggregation file is processed.
        else {

          // Do #READ.
          Holder<MetadataContainerType> tgObjectMetadata = new Holder<MetadataContainerType>();
          Holder<DataHandler> tgObjectData = new Holder<DataHandler>();
          try {
            TGPublishAbs.getTgcrud().read(sid, log, o.uri, tgObjectMetadata, tgObjectData);
          } catch (ObjectNotFoundFault e) {
            message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
            o.addError(ErrorType.NOT_SPECIFIED, message);
            this.anyError = true;
          } catch (MetadataParseFault e) {
            message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
            o.addError(ErrorType.NOT_SPECIFIED, message);
            this.anyError = true;
          } catch (IoFault e) {
            message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
            o.addError(ErrorType.NOT_SPECIFIED, message);
            this.anyError = true;
          } catch (ProtocolNotImplementedFault e) {
            message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
            o.addError(ErrorType.NOT_SPECIFIED, message);
            this.anyError = true;
          } catch (AuthFault e) {
            message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
            o.addError(ErrorType.AUTH, message);
            this.anyError = true;
          }

          message = loguri + "TG-crud#READ complete";
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

          // Set metadata value from #READ.
          metadata = tgObjectMetadata.value;

          // Add issued date to metadata.
          try {
            setIssuedDate(metadata, response, loguri);
          } catch (DatatypeConfigurationException e) {
            message = loguri + e.getMessage();
            o.addError(ErrorType.NOT_SPECIFIED, message);
            this.anyError = true;
          }

          // Add PID to metadata.
          addPersistentIdentifier(metadata, response, loguri, o.pid);

          // Add availability = public to the metadata.
          metadata.getObject().getGeneric().getGenerated().setAvailability(AVAILIBILITY_PUBLIC);

          message = loguri + "Availibility set to: " + AVAILIBILITY_PUBLIC;
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

          try {
            // Rewrite aggregation data file.
            //
            // URIs to PIDs or base URIs to revision URIs, depending on configuration.
            ConfigurableXMLRewriter dataRewriter = new ConfigurableXMLRewriter(mapping, false);
            dataRewriter.recordMissingReferences();
            dataRewriter.configure(URI.create("internal:textgrid#aggregation"));
            ByteArrayOutputStream dataRewritten = new ByteArrayOutputStream();
            dataRewriter.rewrite(tgObjectData.value.getInputStream(), dataRewritten);

            // Create new DataHandler.
            tgObjectData.value =
                new DataHandler(new ByteArrayDataSource(dataRewritten.toByteArray(),
                    TextGridMimetypes.OCTET_STREAM));

            // Log not rewritten not aggregated URIs.
            Set<String> dataMisref = dataRewriter.getMissingReferences();
            if (!dataMisref.isEmpty()) {
              message = loguri + "Not aggregated data URIs (no rewriting needed): " + dataMisref;
              PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
            }
            message = loguri + "Data successfully rewritten";
            PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

            // Rewrite aggregation metadata file.
            //
            // URIs to PIDs or base URIs to revision URIs, depending on configuration.
            ConfigurableXMLRewriter metadataRewriter = new ConfigurableXMLRewriter(mapping, false);
            metadataRewriter.recordMissingReferences();
            metadataRewriter.configure(URI.create(MODE_METADATA_REWRITE));
            ByteArrayOutputStream metadataRewritten = new ByteArrayOutputStream();

            // Get stream from metadata object and rewrite.
            try (StringWriter metadataWriter = new StringWriter()) {
              JAXB.marshal(metadata, metadataWriter);
              String metadataString = metadataWriter.toString();
              metadataWriter.close();
              InputStream metadataStream =
                  new ByteArrayInputStream(metadataString.getBytes("UTF-8"));
              metadataRewriter.rewrite(metadataStream, metadataRewritten);
            }

            // Create new MetadataHandler.
            tgObjectMetadata.value =
                JAXB.unmarshal(new ByteArrayInputStream(metadataRewritten.toByteArray()),
                    MetadataContainerType.class);

            // Log not rewritten URIs.
            Set<String> metadataMisref = metadataRewriter.getMissingReferences();
            if (!metadataMisref.isEmpty()) {
              message = loguri + "Not rewritten (no rewriting needed): " + metadataMisref;
              PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
            }
            message = loguri + "Metadata successfully rewritten";
            PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

          } catch (IOException | XMLStreamException e) {
            message = loguri + e.getMessage();
            o.addError(ErrorType.NOT_SPECIFIED, message);
            this.anyError = true;
          }

          message =
              loguri + (this.rewriteAggregationUrisToPids ? "" : "Base") + "URIs rewritten to "
                  + (this.rewriteAggregationUrisToPids ? "PIDs" : "revision URIs") + " for format: "
                  + tgObjectMetadata.value.getObject().getGeneric().getProvided().getFormat();
          PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

          // **
          // TGCRUD#UPDATE (only if not dry run and not already published!)
          // **

          if (!this.dryRun && !o.status.equals(StatusType.ALREADY_PUBLISHED)) {
            try {
              TGPublishAbs.getTgcrud().update(sid, log, tgObjectMetadata, tgObjectData.value);
            } catch (ObjectNotFoundFault e) {
              message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
              o.addError(ErrorType.NOT_SPECIFIED, message);
              PublishUtils.setStatusRunning(this.step, ERROR + message, response, this,
                  this.dryRun);
              this.anyError = true;
            } catch (MetadataParseFault e) {
              message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
              o.addError(ErrorType.NOT_SPECIFIED, message);
              PublishUtils.setStatusRunning(this.step, ERROR + message, response, this,
                  this.dryRun);
              this.anyError = true;
            } catch (UpdateConflictFault e) {
              message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
              o.addError(ErrorType.NOT_SPECIFIED, message);
              PublishUtils.setStatusRunning(this.step, ERROR + message, response, this,
                  this.dryRun);
              this.anyError = true;
            } catch (IoFault e) {
              message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
              o.addError(ErrorType.NOT_SPECIFIED, message);
              PublishUtils.setStatusRunning(this.step, ERROR + message, response, this,
                  this.dryRun);
              this.anyError = true;
            } catch (AuthFault e) {
              message = loguri + TGPublishUtils.textgridFaultInfo(e.getFaultInfo());
              o.addError(ErrorType.AUTH, message);
              PublishUtils.setStatusRunning(this.step, ERROR + message, response, this,
                  this.dryRun);
              this.anyError = true;
            }

            message = loguri + "TG-crud#UPDATE complete";
            PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);

            // **
            // Update Handle filesize and checksum metadata of updated object (fixes
            // https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/-/issues/255)
            // **

            // Get filesize and checksum from metadata.
            String filesize = String.valueOf(
                tgObjectMetadata.value.getObject().getGeneric().getGenerated().getExtent());
            String checksum = tgObjectMetadata.value.getObject().getGeneric().getGenerated()
                .getFixity().get(0).getMessageDigest();
            String checksumType = tgObjectMetadata.value.getObject().getGeneric().getGenerated()
                .getFixity().get(0).getMessageDigestAlgorithm();

            // Put metadata in key value list.
            List<String> keyValuePairs = new ArrayList<String>();
            keyValuePairs.add("CHECKSUM," + checksumType + ":" + checksum);
            keyValuePairs.add("FILESIZE," + filesize);

            Response pidServiceResponse = TGPublishAbs.getTgpid().updateTextGridMetadata(
                LTPUtils.omitHdlPrefix(o.pid), keyValuePairs, this.tgpidSecret.trim());

            int responseCode = pidServiceResponse.getStatus();
            String reasonPhrase = pidServiceResponse.getStatusInfo().getReasonPhrase();
            if (responseCode == HttpStatus.SC_NO_CONTENT) {
              message = loguri + "TG-pid#UPDATEMETADATA complete [" + responseCode + " "
                  + reasonPhrase + "]: " + keyValuePairs;
              PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
            }
            // Warn only if PID metadata update didn't work.
            else {
              message = loguri + "WARNING! TG-pid#UPDATEMETADATA failed due to a " + responseCode
                  + " " + reasonPhrase + "! Please check Handle FILESIZE and CHECKSUM metadata!";
              PublishUtils.setStatusRunning(this.step, message, response, this, this.dryRun);
            }
          }
        }
      }
    }

    // Check if errors occurred.
    if (this.anyError)

    {
      // FIXME Log all errors that occurred!
      message = "Update of some objects failed";
      PublishUtils.setStatusError(this.step, message, response, this, this.dryRun);
    } else {
      message = "Update of objects complete";
      PublishUtils.setStatusDoneNoProgressIncrease(this.step, message, response, this, this.dryRun);
    }

    // Write response to the response.
    PublishUtils.updateResponse(uri, response);
  }

  /**
   * <p>
   * Sets the issued date.
   * </p>
   * 
   * @param theMetadata
   * @param theResponse
   * @param theLogUri
   * @throws DatatypeConfigurationException
   */
  private void setIssuedDate(MetadataContainerType theMetadata, PublishResponse theResponse,
      String theLogUri) throws DatatypeConfigurationException {
    GregorianCalendar cal = new GregorianCalendar();
    cal.setTimeInMillis(this.issuedDate);

    theMetadata.getObject().getGeneric().getGenerated()
        .setIssued(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal));

    String message = theLogUri + "Added issued date to metadata: " + this.issuedDate;
    PublishUtils.setStatusRunning(this.step, message, theResponse, this, this.dryRun);
  }

  /**
   * <p>
   * Sets the PID.
   * </p>
   */
  private void addPersistentIdentifier(MetadataContainerType theMetadata,
      PublishResponse theResponse, String theLogUri, String thePid) {

    // Create PID object.
    Pid pid = new Pid();
    pid.setPidType(PID_TYPE);
    pid.setValue(thePid);

    // Set PID only, if not existing yet.
    if (!theMetadata.getObject().getGeneric().getGenerated().getPid().contains(pid)) {
      theMetadata.getObject().getGeneric().getGenerated().getPid().add(pid);

      String message =
          theLogUri + "PID added to metadata: " + (thePid != null ? thePid : "No PID existing yet");
      PublishUtils.setStatusRunning(this.step, message, theResponse, this, this.dryRun);
    } else {
      String message = theLogUri + "PID already existing in metadata: "
          + (thePid != null ? thePid : "No PID existing yet");
      PublishUtils.setStatusRunning(this.step, message, theResponse, this, this.dryRun);
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   *
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   *
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param rewriteAggregationUrisToPids
   */
  public void setRewriteAggregationUrisToPids(boolean rewriteUris) {
    this.rewriteAggregationUrisToPids = rewriteUris;
  }

  /**
   * @param theTgpidSecret
   */
  public void setTgpidSecret(String theTgpidSecret) {
    this.tgpidSecret = theTgpidSecret;
  }

}
