/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.processstarter.textgrid.publish;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.TGPublishServiceVersion;
import de.langzeitarchivierung.kolibri.processstarter.PublishAbs;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;

/**
 * TODOLOG
 * 
 * TODO As in processstarter.textgrid.publish.TGPublishWorldReadable: Check why this conditional is
 * necessary. We should not configure this twice! If anyone finds the cause for this double
 * configuration, please fix it and delete the conditionals! (setMimetypeRegExps)
 * 
 * TODO TextGrid logging maybe can be implemented using the koLibRI logger and extend it somehow!
 * 
 **
 * CHANGELOG
 * 
 * 2012-04-11 - Funk - Added error handling using the PublishResponse.
 * 
 * 2011-09-26 - Funk - Handled response map size.
 * 
 * 2011-01-11 - Funk - Finished process starter with first capabilities.
 * 
 * 2011-01-06 - Funk - Divided the TGPublish webservice into process starter and webservice.
 * 
 * 2011-01-05 - Funk - Created file from Ubbo's version.
 */

/**
 * <p>
 * <b>The TGPublishService WorldReadable process starter implementation class</b>
 * </p>
 * 
 * <p>
 * This process starter is waiting for CustomData entries in a queue, that will be filled by the
 * TG-publish for every publish webservice call.
 * </p>
 * 
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-05-20
 * @since 2011-01-04
 */

public class TGPublishWorldReadable extends TGPublishAbs {

  // **
  // STATICS
  // **

  protected static ArrayBlockingQueue<HashMap<String, Object>> dataQueue;
  protected static int dataQueueCapacity = 10;
  protected static long recheckDataQueue = 10000;
  private static List<String> mimetypeRegExps = new ArrayList<String>();

  // **
  // CONSTRUCTOR
  // **

  /**
   * <p>
   * Constructor creates the data queue, it is ment to be thread-safe.
   * </p>
   */
  public TGPublishWorldReadable() {
    super();
    dataQueue = new ArrayBlockingQueue<HashMap<String, Object>>(dataQueueCapacity);
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#run()
   */
  @Override
  public void run() {

    defaultLogger.log(Level.INFO, "TG-pubish#WORLDREADABLE started ["
        + TGPublishServiceVersion.VERSION + "." + TGPublishServiceVersion.BUILDDATE + "]");

    defaultLogger.log(Level.INFO, "Mimetypes configured: " + mimetypeRegExps);

    // TODO implement re-checking using Listeners!!

    defaultLogger.log(Level.INFO, "Checking #WORLDREADABLE data queue every "
        + KolibriTimestamp.getDurationInHours(recheckDataQueue));

    while (true) {

      defaultLogger.log(Level.FINER,
          "DQS: " + dataQueue.size() + "/" + dataQueueCapacity + ", RMS: "
              + PublishAbs.getResponseMapSize() + "/" + PublishAbs.getResponseMapCapacity());

      // Check data queue size.
      while (!dataQueue.isEmpty()) {

        // Get next customData element.
        HashMap<String, Object> customData = dataQueue.peek();

        // Get the needed data out of the customData.
        String uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
        String policyName = (String) customData.get(TGPublishServiceImpl.POLICY_NAME);
        if (policyName == null || policyName.equals("")) {
          policyName = this.defaultPolicy;
        }
        PublishResponse response =
            (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

        // TODO Must the following be synchronised if using more than one thread??

        // Create process data.
        ProcessData pd = null;
        try {
          pd = new ProcessData(policyName, uri);

          defaultLogger.log(Level.INFO, "Policy set [" + policyName + "]");

          // Add custom data to processData as customData.
          pd.getCustomData().putAll(customData);

          // Add element to process queue.
          this.processQueue.addElement(pd);

          defaultLogger.log(Level.INFO, "Added to process queue [" + uri + "]");

        } catch (Exception e) {
          String message = "Error adding to process queue [" + uri + "]";
          defaultLogger.log(Level.SEVERE, message, e);

          // Set response to error.
          PublishAbs.setErrorResponse(uri, response, this.getClass().getName(),
              ErrorType.NOT_SPECIFIED, message);
          // Leave dataQueue!
          break;
        } finally {
          // Remove element from data queue.
          dataQueue.remove();

          defaultLogger.log(Level.FINER, "Removed from data queue [" + uri + "]");
        }
      }

      // Wait for the next check.
      try {
        Thread.sleep(recheckDataQueue);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param capacity
   */
  public static void setDataQueueCapacity(int capacity) {
    dataQueueCapacity = capacity;
  }

  /**
   * @param recheck
   */
  public static void setRecheckDataQueue(long recheck) {
    recheckDataQueue = recheck;
  }

  /**
   * @return the data queue
   */
  public static ArrayBlockingQueue<HashMap<String, Object>> getDataQueue() {
    return dataQueue;
  }

  /**
   * @return the mimetype regexp list
   */
  public static List<String> getMimetypeRegExps() {
    return mimetypeRegExps;
  }

  /**
   * @param theMimetypeRegExp
   */
  public static void setMimetypeRegExps(String theMimetypeRegExp) {
    if (!mimetypeRegExps.contains(theMimetypeRegExp)) {
      mimetypeRegExps.add(theMimetypeRegExp);
    }
  }

}
