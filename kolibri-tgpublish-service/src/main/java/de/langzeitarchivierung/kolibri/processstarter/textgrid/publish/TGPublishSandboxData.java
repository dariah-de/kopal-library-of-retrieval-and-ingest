/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.processstarter.textgrid.publish;

import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.TGPublishServiceVersion;
import de.langzeitarchivierung.kolibri.processstarter.PublishAbs;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;

/**
 * TODOLOG
 * 
 * TODO TextGrid logging maybe can be implemented using the koLibRI logger and extend it somehow!
 * 
 **
 *
 * CHANGELOG
 * 
 * 2012-04-11 - Funk - Added error handling using the PublishResponse.
 * 
 * 2011-10-10 - Funk - Copied from TGPublish.
 */

/**
 * <p>
 * This process starter is waiting for CustomData entries in a queue, that will be filled by the
 * TG-publish for every publish webservice call.
 * </p>
 * 
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 223-05-05
 * @since 2011-10-10
 */

public class TGPublishSandboxData extends TGPublishAbs {

  // **
  // STATICS
  // **

  protected static ArrayBlockingQueue<HashMap<String, Object>> dataQueue;
  protected static int dataQueueCapacity = 10;
  protected static long recheckDataQueue = 10000;

  // **
  // CONSTRUCTOR
  // **

  /**
   * <p>
   * Constructor creates the data queue, it is ment to be thread-safe.
   * </p>
   */
  public TGPublishSandboxData() {
    super();
    dataQueue = new ArrayBlockingQueue<HashMap<String, Object>>(dataQueueCapacity);
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#run()
   */
  @Override
  public void run() {

    defaultLogger.log(Level.INFO, "TG-publish#SANDBOXDATA started ["
        + TGPublishServiceVersion.VERSION + "." + TGPublishServiceVersion.BUILDDATE + "]");

    // TODO implement re-checking using Listeners!!

    defaultLogger.log(Level.INFO, "Checking #SANDBOXDATA data queue every "
        + KolibriTimestamp.getDurationInHours(recheckDataQueue));

    while (true) {

      defaultLogger.log(Level.FINER,
          "DQS: " + dataQueue.size() + "/" + dataQueueCapacity + ", RMS: "
              + PublishAbs.getResponseMapSize() + "/" + PublishAbs.getResponseMapCapacity());

      // Check data queue size.
      while (!dataQueue.isEmpty()) {

        // Get next customData element.
        HashMap<String, Object> customData = dataQueue.peek();

        // Get the needed data out of the customData.
        String uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
        String policyName = (String) customData.get(TGPublishServiceImpl.POLICY_NAME);
        if (policyName == null || policyName.equals("")) {
          policyName = this.defaultPolicy;
        }
        PublishResponse response =
            (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

        // TODO Must the following be synchronized if using more than one thread??

        // Create process data.
        ProcessData pd = null;
        try {
          pd = new ProcessData(policyName, uri);

          defaultLogger.log(Level.INFO, "Policy set [" + policyName + "]");

          // Add custom data to processData as customData.
          pd.getCustomData().putAll(customData);

          // Add element to process queue.
          this.processQueue.addElement(pd);

          defaultLogger.log(Level.INFO, "Added to process queue [" + uri + "]");

        } catch (Exception e) {
          String message = "Error adding to process queue [" + uri + "]";
          defaultLogger.log(Level.SEVERE, message, e);

          // Set response to error.
          PublishAbs.setErrorResponse(uri, response, this.getClass().getName(),
              ErrorType.NOT_SPECIFIED, message);
          // Leave dataQueue!
          break;
        } finally {
          // Remove element from data queue.
          dataQueue.remove();

          defaultLogger.log(Level.FINER, "Removed from data queue [" + uri + "]");
        }
      }

      // Wait for the next check.
      try {
        Thread.sleep(recheckDataQueue);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param capacity
   */
  public static void setDataQueueCapacity(int capacity) {
    dataQueueCapacity = capacity;
  }

  /**
   * @param recheck
   */
  public static void setRecheckDataQueue(long recheck) {
    recheckDataQueue = recheck;
  }

  /**
   * @return the data queue
   */
  public static ArrayBlockingQueue<HashMap<String, Object>> getDataQueue() {
    return dataQueue;
  }

}
