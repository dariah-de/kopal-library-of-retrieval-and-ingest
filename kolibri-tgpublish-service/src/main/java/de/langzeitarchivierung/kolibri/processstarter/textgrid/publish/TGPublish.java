/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.processstarter.textgrid.publish;

import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.TGPublishServiceVersion;
import de.langzeitarchivierung.kolibri.processstarter.PublishAbs;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import de.langzeitarchivierung.kolibri.webservice.textgrid.publish.TGPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;

/**
 * TODOLOG
 * 
 * TODO TextGrid logging maybe can be implemented using the koLibRI logger and extend it somehow!
 * 
 **
 * CHANGELOG
 * 
 * 2012-04-11 - Funk - Added error handling using the PublishResponse.
 * 
 * 2011-09-26 - Funk - Handled response map size.
 * 
 * 2011-02-16 - Funk - Extracted abstract class for TGPublish.
 * 
 * 2011-01-11 - Funk - Finished process starter with first capabilities.
 * 
 * 2011-01-06 - Funk - Divided the TGPublish webservice into process starter and webservice.
 * 
 * 2011-01-05 - Funk - Created file from Ubbo's version.
 **/

/**
 * <p>
 * <b>The TGPublishService process starter implementation class</b>
 * </p>
 * 
 * <p>
 * This process starter is waiting for CustomData entries in a queue, that will be filled by the
 * TG-publish for every publish webservice call.
 * </p>
 * 
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Stefan E. Funk, DAASI International GmbH * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-05-20
 * @since 2011-01-04
 **/

public class TGPublish extends TGPublishAbs {

  // **
  // STATICS
  // **

  protected static ArrayBlockingQueue<HashMap<String, Object>> dataQueue;
  protected static int dataQueueCapacity = 10;
  protected static long recheckDataQueue = 10000;

  // **
  // CONSTRUCTOR
  // **

  /**
   * <p>
   * Constructor creates the data queue, it is ment to be thread-safe.
   * </p>
   */
  public TGPublish() {
    super();
    dataQueue = new ArrayBlockingQueue<HashMap<String, Object>>(dataQueueCapacity);
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#run()
   */
  @Override
  public void run() {

    defaultLogger.log(Level.INFO, "TG-publish#PUBLISH started [" + TGPublishServiceVersion.VERSION
        + "." + TGPublishServiceVersion.BUILDDATE + "]");

    // TODO implement re-checking using Listeners!!

    defaultLogger.log(Level.INFO, "Checking #PUBLISH data queue every "
        + KolibriTimestamp.getDurationInHours(recheckDataQueue));

    while (true) {

      defaultLogger.log(Level.FINER,
          "DQS: " + dataQueue.size() + "/" + dataQueueCapacity + ", RMS: "
              + PublishAbs.getResponseMapSize() + "/" + PublishAbs.getResponseMapCapacity());

      // Check data queue size.
      while (!dataQueue.isEmpty()) {

        // Get next customData element.
        HashMap<String, Object> customData = dataQueue.peek();

        // Get the needed data out of the customData.
        String uri = (String) customData.get(TGPublishServiceImpl.TEXTGRID_URI);
        String sid = (String) customData.get(TGPublishServiceImpl.SESSION_ID);
        String log = (String) customData.get(TGPublishServiceImpl.LOG_PARAMETER);
        String policyName = (String) customData.get(TGPublishServiceImpl.POLICY_NAME);
        if (policyName == null || policyName.equals("")) {
          policyName = this.defaultPolicy;
        }
        PublishResponse response =
            (PublishResponse) customData.get(TGPublishServiceImpl.PUBLISH_RESPONSE);

        // TODO Must the following be synchronised if using more than one thread?

        // Create process data.
        ProcessData pd = null;
        try {
          pd = new ProcessData(policyName, uri);

          defaultLogger.log(Level.INFO, "Policy set [" + policyName + "]");

          // Add custom data to processData as customData.
          pd.getCustomData().putAll(customData);

          // Lock the root aggregation URI to publish (just to avoid concurrent publishing of the
          // same URI). We lock every object contained in this root aggregation in the action module
          // LockObjects anyway (not yet! :-).

          // NOTE This is only done here in TGPublish! We must unlock in case of an error and in the
          // last action module PublishComplete!

          defaultLogger.log(Level.INFO, "Locking root aggregation [" + uri + "]");

          boolean locked = TGPublishAbs.getTgcrud().lock(sid, log, uri);

          if (locked) {
            defaultLogger.log(Level.INFO, "Root aggregation locking complete [" + uri + "]");
          } else {
            String message =
                "Root aggregation locked by another process, please try again later [" + uri + "]";
            defaultLogger.log(Level.SEVERE, message);

            // Set response to error.
            PublishAbs.setErrorResponse(uri, response, this.getClass().getName(),
                ErrorType.NOT_SPECIFIED, message);
            // Leave dataQueue!
            break;
          }

          // Add element to process queue.
          this.processQueue.addElement(pd);

          defaultLogger.log(Level.INFO, "Added to process queue [" + uri + "]");

        } catch (AuthFault e) {
          String message = "Unable to lock object! Access denied! [" + uri + "]";
          defaultLogger.log(Level.SEVERE, message, e);

          // Set response to error.
          PublishAbs.setErrorResponse(uri, response, this.getClass().getName(), ErrorType.AUTH,
              message);
          // Leave dataQueue!
          break;
        } catch (Exception e) {
          // Unlock eventually locked object.
          try {
            defaultLogger.log(Level.INFO, "Unlocking root aggregation [" + uri + "]");
            TGPublishAbs.getTgcrud().unlock(sid, log, uri);
          } catch (ObjectNotFoundFault | AuthFault | IoFault e1) {
            defaultLogger.log(Level.WARNING, "Unlocking root aggregation [" + uri + "] failed!");
          }

          // Log error.
          String message = "Error adding to process queue due to a " + e.getClass().getSimpleName()
              + " [" + uri + "]";
          defaultLogger.log(Level.SEVERE, message, e);

          // Set response to error.
          PublishAbs.setErrorResponse(uri, response, this.getClass().getName(),
              ErrorType.NOT_SPECIFIED, message + ": " + e.getMessage());
          // Leave dataQueue!
          break;
        } finally {
          // Remove element from data queue.
          dataQueue.remove();

          defaultLogger.log(Level.FINER, "Removed from data queue [" + uri + "]");
        }
      }

      // Wait for the next check.
      try {
        Thread.sleep(recheckDataQueue);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param capacity
   */
  public static void setDataQueueCapacity(int capacity) {
    dataQueueCapacity = capacity;
  }

  /**
   * @param recheck
   */
  public static void setRecheckDataQueue(long recheck) {
    recheckDataQueue = recheck;
  }

  /**
   * @return the data queue
   */
  public static ArrayBlockingQueue<HashMap<String, Object>> getDataQueue() {
    return dataQueue;
  }

}
