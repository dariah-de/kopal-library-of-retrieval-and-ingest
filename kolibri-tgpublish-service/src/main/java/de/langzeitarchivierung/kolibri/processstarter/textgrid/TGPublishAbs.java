/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 **/

package de.langzeitarchivierung.kolibri.processstarter.textgrid;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import de.langzeitarchivierung.kolibri.processstarter.PublishAbs;
import de.langzeitarchivierung.kolibri.util.Configurator;
import info.textgrid.clients.SearchClient;
import info.textgrid.middleware.pid.api.TGPidService;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.Tgextra;
import info.textgrid.namespaces.middleware.tgauth_crud.PortTgextraCrud;
import info.textgrid.namespaces.middleware.tgauth_crud.TgextraCrud;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;
import jakarta.xml.ws.BindingProvider;
import jakarta.xml.ws.soap.SOAPBinding;

/**
 * TODOLOG
 * 
 * TODO TextGrid logging maybe can be implemented using the koLibRI logger and extend it somehow!
 * 
 **
 * CHANGELOG
 * 
 * 2024-09-02 - Funk - Add tgsearch and tgpid clients, add failsafe and creation at null values.
 * 
 * 2014-12-17 - Funk - Removed general things (see PublishAbs).
 * 
 * 2013-03-26 - Funk - Adapted to new tgextra and tgextra-crud stubs.
 * 
 * 2012-04-11 - Funk - Added error handling using the PublishResponse.
 * 
 * 2012-12-13 - Funk - Added tgextra-crud stub to be initialised.
 * 
 * 2011-09-27 - Funk - Using queue now for responses. So we can delete the last result.
 * 
 * 2011-08-29 - Funk - Slightly refactored the tgcrud and tgauth stub creation.
 * 
 * 2011-02-16 - Funk - Extracted abstract class for TGPublish.
 **/

/**
 * <p>
 * <b>The TGPublishService process starter abstract class</b>
 * </p>
 * 
 * <p>
 * This process starter is waiting for CustomData entries in a queue, that will be filled by the
 * TG-publish for every publish webservice call.
 * </p>
 * 
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-09-02
 * @since 2011-01-04
 **/

public abstract class TGPublishAbs extends PublishAbs {

  // **
  // FINALS
  // **

  private static final String INITALISED = "service client initialisation complete: ";
  private static final String FAILED = "service client initialisation failed: ";
  private static final String USING_EXISTING = "service client is already existing: ";

  // **
  // STATICS
  // **

  private static PortTgextra tgauth;
  private static PortTgextraCrud tgauthCrud;
  private static TGCrudService tgcrud;
  private static TGPidService tgpid;
  private static SearchClient tgsearch;

  // **
  // CLASS VARIABLES
  // **

  private String tgauthUrl;
  private String tgauthCrudUrl;
  private String tgcrudUrl;
  private String tgsearchUrl;
  private String tgpidUrl;

  // **
  // CONSTRUCTOR
  // **

  /**
   * <p>
   * Constructor constructs, e.g. service stubs, and creates the response queue. This is ment to be
   * thread-safe.
   */
  public TGPublishAbs() {

    // Configure this class.
    try {
      Configurator.configure(this);
    } catch (InvocationTargetException | IllegalAccessException e) {
      defaultLogger.severe("TG-publishAbs class configuration failed!");
    }

    // Init service clients.
    initTgcrud(this.tgcrudUrl);
    initTgextraService(this.tgauthUrl);
    initTgextraCrudService(this.tgauthCrudUrl);
    initTgsearch(this.tgsearchUrl);
    initTgpid(this.tgpidUrl);

    // Create response map, if not yet created.
    if (publishResponseMap == null) {
      publishResponseMap = new ConcurrentHashMap<String, PublishResponse>(responseMapCapacity);
    }

    // Create start date map, if not yet created.
    if (startDateMap == null) {
      startDateMap = Collections.synchronizedSortedMap(new TreeMap<String, Long>());
    }
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Sets the regular TG-auth stub.
   * </p>
   * 
   * @param theEndpoint
   */
  private static synchronized void initTgextraService(final String theEndpoint) {

    String name = "TG-auth ";

    if (tgauth == null) {
      try {
        URL url = new URL(theEndpoint);

        // Create TG-extra service stubs.
        Tgextra service = new Tgextra();
        tgauth = service.getTgextra();
        BindingProvider bindingProvider = (BindingProvider) tgauth;
        // Disable MTOM.
        SOAPBinding binding = (SOAPBinding) bindingProvider.getBinding();
        binding.setMTOMEnabled(false);

        // Set correct service endpoint.
        Map<String, Object> requestContext = bindingProvider.getRequestContext();
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url.toExternalForm());

        defaultLogger.fine(name + INITALISED + url.toExternalForm());

      } catch (MalformedURLException e) {
        defaultLogger.severe(name + FAILED + theEndpoint);
      }
    } else {
      defaultLogger.info(name + USING_EXISTING + theEndpoint);
    }
  }

  /**
   * <p>
   * Sets the TG-auth_crud stub.
   * </p>
   * 
   * @param theEndpoint
   */
  private static synchronized void initTgextraCrudService(final String theEndpoint) {

    String name = "TG-auth CRUD ";

    if (tgauthCrud == null) {
      try {
        URL url = new URL(theEndpoint);

        // Create TG-extra service stubs.
        TgextraCrud service = new TgextraCrud();
        tgauthCrud = service.getTgextraCrud();
        BindingProvider bindingProvider = (BindingProvider) tgauthCrud;

        // Disable MTOM.
        SOAPBinding binding = (SOAPBinding) bindingProvider.getBinding();
        binding.setMTOMEnabled(false);

        // Set correct service endpoint.
        Map<String, Object> requestContext = bindingProvider.getRequestContext();
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url.toExternalForm());

        defaultLogger.info(name + INITALISED + url.toExternalForm());

      } catch (MalformedURLException e) {
        defaultLogger.severe(name + FAILED + theEndpoint);
      }
    } else {
      defaultLogger.info(name + USING_EXISTING + theEndpoint);
    }
  }

  /**
   * @param theEndpoint
   */
  private static synchronized void initTgcrud(final String theEndpoint) {

    String name = "TG-crud ";

    if (tgcrud == null) {
      try {
        URL url = new URL(theEndpoint);
        String urlString = url.toExternalForm();

        tgcrud = TGCrudClientUtilities.getTgcrud(urlString);

        defaultLogger.info(name + INITALISED + urlString);

      } catch (MalformedURLException e) {
        defaultLogger.severe(name + FAILED + theEndpoint);
      }
    } else {
      defaultLogger.info(name + USING_EXISTING + theEndpoint);
    }
  }

  /**
   * @param theEndpoint
   */
  private static synchronized void initTgsearch(final String theEndpoint) {

    String name = "TG-search ";

    if (tgsearch == null) {
      try {
        URL url = new URL(theEndpoint);
        String urlString = url.toExternalForm();

        tgsearch = new SearchClient(urlString);

        defaultLogger.fine(name + INITALISED + urlString);

      } catch (MalformedURLException e) {
        defaultLogger.severe(name + FAILED + theEndpoint);
      }
    } else {
      defaultLogger.info(name + USING_EXISTING + theEndpoint);
    }
  }

  /**
   * @param theEndpoint
   */
  private static synchronized void initTgpid(final String theEndpoint) {

    String name = "TG-pid ";

    if (tgpid == null) {
      try {
        URL url = new URL(theEndpoint);
        String urlString = url.toExternalForm();

        tgpid = JAXRSClientFactory.create(urlString, TGPidService.class);

        defaultLogger.fine(name + INITALISED + urlString);

      } catch (MalformedURLException e) {
        defaultLogger.severe(name + FAILED + theEndpoint);
      }
    } else {
      defaultLogger.info(name + USING_EXISTING + theEndpoint);
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @return
   */
  public String getTgauthUrl() {
    return this.tgauthUrl;
  }

  /**
   * @param tgauthUrl
   */
  public void setTgauthUrl(String tgauthUrl) {
    this.tgauthUrl = tgauthUrl;
  }

  /**
   * @return
   */
  public String getTgauthCrudUrl() {
    return this.tgauthCrudUrl;
  }

  /**
   * @param tgauthCrudUrl
   */
  public void setTgauthCrudUrl(String tgauthCrudUrl) {
    this.tgauthCrudUrl = tgauthCrudUrl;
  }

  /**
   * @return
   */
  public String getTgcrudUrl() {
    return this.tgcrudUrl;
  }

  /**
   * @param tgcrudUrl
   */
  public void setTgcrudUrl(String tgcrudUrl) {
    this.tgcrudUrl = tgcrudUrl;
  }

  /**
   * @return
   */
  public String getTgsearchUrl() {
    return this.tgsearchUrl;
  }

  /**
   * @param tgsearchUrl
   */
  public void setTgsearchUrl(String tgsearchUrl) {
    this.tgsearchUrl = tgsearchUrl;
  }

  /**
   * @return
   */
  public String getTgpidUrl() {
    return this.tgpidUrl;
  }

  /**
   * @param tgpidUrl
   */
  public void setTgpidUrl(String tgpidUrl) {
    this.tgpidUrl = tgpidUrl;
  }

  /**
   * @return the tgauth service
   */
  public static PortTgextra getTgauth() {
    return tgauth;
  }

  /**
   * @return Configure the TG-auth crud service
   */
  public static PortTgextraCrud getTgauthCrud() {
    return tgauthCrud;
  }

  /**
   * @return Configure the TG-crud service
   */
  public static TGCrudService getTgcrud() {
    return tgcrud;
  }

  /**
   * @return The TG-pid service client
   */
  public static TGPidService getTgpid() {
    return tgpid;
  }

  /**
   * @return The TG-search service client
   */
  public static SearchClient getTgsearch() {
    return tgsearch;
  }

}
