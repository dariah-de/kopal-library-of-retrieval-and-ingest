/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.publish.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TextGridFaultType;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

/**
 * <p>
 * Utility class for TG-publish.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2022-08-19
 * @since 2011-01-17
 */

public class TGPublishUtils extends PublishUtils {

  // **
  // MANIPULATION
  // **

  /**
   * @param theFault The fault type to get info from.
   * @return a TextGrid fault string
   */
  public static String textgridFaultInfo(TextGridFaultType theFault) {
    return theFault.getFaultMessage() + " (" + theFault.getCause() + ")";
  }

  /**
   * <p>
   * Unlock all objects in the PublishResponse PublishObject list.
   * </p>
   * 
   * @param sid The RBAC Session ID.
   * @param log The log parameter.
   * @param theResponse The response to unlock objects from.
   * @return TRUE if everything was successfully unlocked, FALSE if not.
   */
  public static boolean unlock(String sid, String log, PublishResponse theResponse) {

    boolean result = false;

    for (PublishObject o : theResponse.getPublishObjects()) {
      try {
        result = TGPublishAbs.getTgcrud().unlock(sid, log, o.uri);
      } catch (ObjectNotFoundFault | IoFault | AuthFault e) {
        // Ignore and then result is false.
        result = false;
      }
    }

    return result;
  }

  /**
   * <p>
   * Match XPath expression in requiredFields against tgObject, return list of expressions which
   * failed.
   * </p>
   * 
   * @param requiredFields XPath expressions.
   * @param tgobject A TewxtGrid metadata object.
   * @param xpathNamespaces The namespaces of the checked objects.
   * @param dryRun To dry run or to wet run.
   * @param step The step.
   * @return A list of missing required metadata.
   * @throws JAXBException
   * @throws ParserConfigurationException
   * @throws XPathExpressionException
   */
  public static List<String> checkRequiredMetadata(final List<String> requiredFields,
      final ObjectType tgobject, Map<String, String> xpathNamespaces, boolean dryRun, Step step)
      throws JAXBException, ParserConfigurationException, XPathExpressionException {

    List<String> missingFields = new ArrayList<String>();

    JAXBContext jc =
        JAXBContext.newInstance(info.textgrid.namespaces.metadata.core._2010.ObjectType.class);

    Marshaller m = jc.createMarshaller();

    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    Document doc = dbf.newDocumentBuilder().newDocument();
    m.marshal(tgobject, doc);

    XPath xpath = XPathFactory.newInstance().newXPath();
    xpath.setNamespaceContext(new NamespaceContextImpl(xpathNamespaces));

    for (String expression : requiredFields) {
      Boolean res = Boolean.parseBoolean(xpath.evaluate(expression, doc));

      if (res) {
        if (step != null) {
          PublishUtils.logStatusRunning(step,
              "[" + tgobject.getGeneric().getGenerated().getTextgridUri().getValue()
                  + "] XPath value existing for [" + expression + " = " + res + "]",
              dryRun);
        }
      } else {
        missingFields.add(expression);
        if (step != null) {
          PublishUtils.logStatusRunning(step,
              "[" + tgobject.getGeneric().getGenerated().getTextgridUri().getValue()
                  + "] XPath value MISSING for [" + expression + "]",
              dryRun);
        }
      }
    }

    return missingFields;
  }

  /**
   * <p>
   * For applying namespaces to XPath, copied from CXF, org.apache.cxf.jaxrs.ext.xml.XMLSource.
   * </p>
   */
  private static class NamespaceContextImpl implements NamespaceContext {

    private Map<String, String> namespaces;

    /**
     * @param namespaces
     */
    public NamespaceContextImpl(Map<String, String> namespaces) {
      this.namespaces = namespaces;
    }

    /**
     *
     */
    @Override
    public String getNamespaceURI(String prefix) {
      return this.namespaces.get(prefix);
    }

    /**
     *
     */
    @Override
    public String getPrefix(String namespace) {

      for (Map.Entry<String, String> entry : this.namespaces.entrySet()) {
        if (entry.getValue().equals(namespace)) {
          return entry.getKey();
        }
      }

      return null;
    }

    /**
     *
     */
    @Override
    public Iterator<String> getPrefixes(String namespace) {

      String prefix = this.namespaces.get(namespace);
      if (prefix == null) {
        return null;
      }

      return Collections.singletonList(prefix).iterator();
    }
  }

}
