/**
 * This software is copyright (c) 2015 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.publish.util;

import java.net.URI;

/**
 * <p>
 * Util class for ElasticSearch in TG-publish.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2015-01-20
 * @since 2014-03-12
 */

public class TGElasticSearchUtils extends ElasticSearchUtils {

  // **
  // FINALS
  // **

  private static final String URI_PREFIX = "textgrid:";

  // **
  // MANIPULATION
  // **

  /**
   *
   */
  @Override
  public String forgetUriPrefix(URI theUri) {
    return theUri.toASCIIString().replaceFirst(URI_PREFIX, "");
  }

}
