/**
 * This software is copyright (c) 2025 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Stefan E. Funk (funk@sub-uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.webservice.textgrid.publish;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpStatus;
import de.langzeitarchivierung.kolibri.TGPublishServiceVersion;
import de.langzeitarchivierung.kolibri.WorkflowTool;
import de.langzeitarchivierung.kolibri.processstarter.PublishAbs;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.TGPublishAbs;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.copy.TGCopy;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.publish.TGPublish;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.publish.TGPublishSandboxData;
import de.langzeitarchivierung.kolibri.processstarter.textgrid.publish.TGPublishWorldReadable;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import info.textgrid.middleware.tgpublish.api.TGPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.ProcessStatusType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishStatus;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import info.textgrid.middleware.tgpublish.api.jaxb.WorldReadableMimetypes;
import info.textgrid.namespaces.middleware.tgauth.IsPublicRequest;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

/**
 * TODOLOG
 * 
 * TODO TextGrid logging maybe can be implemented using the koLibRI logger, do extend it somehow!?
 * 
 **
 *
 * CHANGELOG
 * 
 * 2025-02-19 - Funk - Adapt PublishWorldReadable to #243. Fixes #264.
 * 
 * 2025-02-17 - Funk - Response URI only for PublishSandboxData and publishWorldReadable. Fixes
 * #263.
 * 
 * 2024-03-08 - Funk - Put new publish response in the response map to avoid getting old responses!
 * (fixes #243)
 * 
 * 2015-01-16 - Funk - Added processStatus to publishStatus object in status and ministatus methods.
 * 
 * 2014-11-25 - Funk - Added concurrency handling for PublishResponse, e.g. if not yet ready filled
 * with objects. Implemented ministatus method.
 * 
 * 2012-04-16 - Funk - Synchronised returning the PublishResponse.
 * 
 * 2012-04-02 - Funk - Added copy method for creating new revisions.
 * 
 * 2012-03-14 - Funk - Added abort service method.
 * 
 * 2012-03-09 - Funk - Added TGCopy service method.
 * 
 * 2011-12-16 - Funk - Added some log entries.
 * 
 * 2011-12-14 - Funk - Refactored config file configuration.
 * 
 * 2011-12-06 - Funk - Updated version string. Now it is the same as the tgcrud string.
 * 
 * 2011-11-18 - Funk - Added config file configurability in Tomcat's init script.
 * 
 * 2011-02-03 - Funk - Added TG-publish#WORLDREADABLE method.
 * 
 * 2011-01-06 - Funk - Divided the TGPublish webservice into process starter and webservice.
 * 
 * 2011-01-05 - Funk - Created file from Ubbo's version.
 */

/**
 * <p>
 * The TGPublishService implementation class. Please configure the WorkflowTool config file using
 * the beans.xml.
 * </p>
 * 
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2024-03-11
 * @since 2011-01-04
 */

public class TGPublishServiceImpl implements TGPublishService {

  // **
  // STATIC FINALS
  // **

  private static final String CONFIG_ERROR_MESSAGE = "Error configuring WorkflowTool";
  private static final String RUN_ERROR_MESSAGE = "Error running WorkflowTool";

  public static final String PUBLISH_RESPONSE = "publishResponse";
  public static final String SESSION_ID = "sessionId";
  public static final String LOG_PARAMETER = "logParameter";
  public static final String PROJECT_ID = "projectId";
  public static final String TEXTGRID_URI = "textgridUri";
  public static final String TEXTGRID_URI_LIST = "textgridUriList";
  public static final String IGNORE_WARNINGS = "ignoreWarnings";
  public static final String DRY_RUN = "dryRun";
  public static final String POLICY_NAME = "policyName";
  public static final String SESSION_UUID = "sessionUuid";
  public static final String NEW_REVISION = "newRevision";

  // **
  // STATICS
  // **

  protected static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");

  // **
  // CONSTRUCTOR
  // **

  /**
   * <p>
   * Starts the koLibRI WorkflowTool thread at service start.
   * </p>
   * 
   * @param theConfigFilename The name of the config file.
   */
  public TGPublishServiceImpl(String theConfigFilename) {

    // Configure TG-publish service from beans.xml.
    if (!new File(theConfigFilename).exists()) {
      defaultLogger.log(Level.SEVERE, "TG-publish config file not found: " + theConfigFilename
          + "! Please configure in beans.xml!");
      return;
    }

    defaultLogger.log(Level.INFO, "Starting TG-publish WorkflowTool thread");

    new StartWorkflowTool(theConfigFilename).start();

    defaultLogger.log(Level.INFO, "TG-publish WorkflowTool thread successfully started");
  }

  /**
   *
   */
  @Override
  @GET
  @Path("/version")
  @Produces("text/plain")
  public Response getVersion() {
    return Response.ok(TGPublishServiceVersion.SERVICENAME + "-" + TGPublishServiceVersion.VERSION
        + "." + TGPublishServiceVersion.BUILDDATE).build();
  }

  /**
   *
   */
  @Override
  @GET
  @Path("/{uri}/publish")
  @Produces("text/plain")
  public Response publish(
      @QueryParam("sid") String sid,
      @QueryParam("log") @DefaultValue("") String log,
      @PathParam("uri") String uri,
      @QueryParam("ignoreWarnings") @DefaultValue("false") boolean ignoreWarnings,
      @QueryParam("dryRun") @DefaultValue("true") boolean dryRun) {

    defaultLogger.log(Level.INFO, "Calling TG-publish service [" + uri + "]");

    // Check for other processes concerning this URI.
    synchronized (this) {
      if (otherProcessesRunning(uri)) {
        String message = "Another publish process is running at the moment for [" + uri
            + "], so please be patient. Thank you!";
        defaultLogger.log(Level.INFO, message);
        return Response.status(HttpStatus.SC_BAD_REQUEST).build();
      }

      // Create a new publish response object.
      PublishResponse response = new PublishResponse(dryRun);

      defaultLogger.log(Level.FINE, "Publish response created");

      response.getPublishStatus().processStatus = ProcessStatusType.QUEUED;

      // Set process status to QUEUED to mark processing right away!
      PublishAbs.updatePublishResponseMap(uri, response);

      defaultLogger.log(Level.FINE, "Process status type set to QUEUED");

      // Create a CustomData object, and set all the necessary parameters from the service call to
      // the customData of the processData element.
      HashMap<String, Object> customData = new HashMap<String, Object>();

      customData.put(PUBLISH_RESPONSE, response);
      customData.put(SESSION_ID, sid);
      customData.put(LOG_PARAMETER, log);
      customData.put(TEXTGRID_URI, uri);
      customData.put(IGNORE_WARNINGS, ignoreWarnings);
      customData.put(DRY_RUN, dryRun);

      defaultLogger.log(Level.INFO,
          "CustomData object created [publishResponse=" + response + ", SID="
              + PublishUtils.purgePasswd(sid) + ", LOG=" + log + ", URI=" + uri
              + ", ignoreWarnings="
              + ignoreWarnings + ", dryRun=" + dryRun + "]");

      // Get the data queue.
      ArrayBlockingQueue<HashMap<String, Object>> dataQueue = TGPublish.getDataQueue();

      defaultLogger.log(Level.INFO, "TGPublishService#publish sucessfully started [" + uri + "]");

      // Add custom data to process starter queue.
      try {
        if (dataQueue != null) {
          dataQueue.put(customData);
        } else {
          throw new IOException();
        }
      } catch (InterruptedException e) {
        defaultLogger.log(Level.SEVERE, "Data could not be added to data queue [" + uri + "]");
        e.printStackTrace();
        return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
      } catch (IOException e) {
        defaultLogger.log(Level.SEVERE, "Please wait for the server to start!");
        e.printStackTrace();
        return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
      }

      defaultLogger.log(Level.INFO, "Data added to data queue [" + uri + "]");
    }

    return Response.ok(uri).build();
  }

  /**
   *
   */
  @Override
  @GET
  @Path("/{uri}/abort")
  @Produces("text/plain")
  public Response abort(@QueryParam("sid") String sid,
      @QueryParam("log") @DefaultValue("") String log,
      @PathParam("uri") String uri) {

    // Get the publish response from the response map for the specified TextGrid URI.
    PublishResponse response = PublishAbs.getPublishResponse(uri);

    // If a response is existing for the given URI, set the ABORT flag.
    if (response != null) {
      defaultLogger.log(Level.INFO, "SENDING ABORT SIGNAL TO PROCESS: " + uri);
      response.getPublishStatus().processStatus = ProcessStatusType.ABORTED;
    } else {
      defaultLogger.log(Level.INFO,
          "Process is not existing, aborting the abort for process: " + uri);
      return Response.status(HttpStatus.SC_BAD_REQUEST).build();
    }

    return Response.ok().build();
  }

  /**
   *
   */
  @Override
  @GET
  @Path("/copy")
  @Produces("text/plain")
  public Response copy(
      @QueryParam("sid") String sid,
      @QueryParam("log") @DefaultValue("") String log,
      @QueryParam("uri") List<String> uriList,
      @QueryParam("projectId") String projectId,
      @QueryParam("newRevision") @DefaultValue("false") boolean newRevision) {

    defaultLogger.log(Level.INFO, "Calling TG-copy service " + uriList);

    // Create an unique session ID for checking responses and complete processing.
    String uuid = "tgcopy:" + UUID.randomUUID().toString();

    // Creating publish response object.
    PublishResponse response = new PublishResponse();

    defaultLogger.log(Level.FINE, "Publish response created [copy response]");

    response.getPublishStatus().processStatus = ProcessStatusType.QUEUED;

    // Set process status to QUEUED to mark processing right away! Use process UUID instead of
    // publish root element for logging here!
    PublishAbs.updatePublishResponseMap(uuid, response);

    // Create a CustomData object, and set all the necessary parameters from the service call to the
    // customData of the processData element.
    HashMap<String, Object> customData = new HashMap<String, Object>();

    customData.put(PUBLISH_RESPONSE, response);
    customData.put(SESSION_ID, sid);
    customData.put(LOG_PARAMETER, log);
    customData.put(PROJECT_ID, projectId);
    customData.put(TEXTGRID_URI_LIST, uriList);
    customData.put(SESSION_UUID, uuid);
    customData.put(NEW_REVISION, newRevision);

    defaultLogger.log(Level.INFO,
        "CustomData object created [publishResponse=" + response + ", SID="
            + PublishUtils.purgePasswd(sid) + ", LOG=" + log + ", URILIST=" + uriList + ", UUID="
            + uuid + "]" + ", PROJECTID=" + projectId + ", NEWREVISION=" + newRevision);

    // Get the data queue.
    ArrayBlockingQueue<HashMap<String, Object>> dataQueue = TGCopy.getDataQueue();

    defaultLogger.log(Level.INFO, "TGPublishService#copy sucessfully started [" + uuid + "]");

    // Add custom data to process starter queue.
    try {
      if (dataQueue != null) {
        dataQueue.put(customData);
      } else {
        throw new IOException();
      }
    } catch (InterruptedException e) {
      defaultLogger.log(Level.SEVERE, "Data could not be added to data queue [" + uuid + "]");
      e.printStackTrace();
      return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
    } catch (IOException e) {
      defaultLogger.log(Level.SEVERE, "Please wait for the server to start!");
      e.printStackTrace();
      return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
    }

    defaultLogger.log(Level.INFO, "Data added to data queue [" + uuid + "]");

    return Response.ok(uuid).build();
  }

  /**
   *
   */
  @Override
  @GET
  @Path("/{uri}/ministatus")
  @Produces("text/xml")
  public PublishResponse getMinistatus(@QueryParam("sid") String sid,
      @QueryParam("log") @DefaultValue("") String log,
      @PathParam("uri") String uri) {

    defaultLogger.log(Level.FINEST, "Calling TG-publish#getMinistatus [" + uri + "]");

    // Get the publish response from the response map for the specified TextGrid URI.
    PublishResponse response =
        getCurrentPublishResponse(uri, PublishAbs.getPublishResponse(uri), alreadyPublished(uri));

    // Create a new response object to respond with.
    PublishResponse ministatusResponse = new PublishResponse();

    // Add needed ministatus to the new response (just leave out all the
    // objects from the publish objects list).
    ministatusResponse.dryRun = response.dryRun;
    ministatusResponse.objectListComplete = response.objectListComplete;
    ministatusResponse.setPublishStatus(response.getPublishStatus());

    // Add only the root object (edition or collection) to object list.
    ministatusResponse.setPublishObjects(getRootObject(response));

    // Finally return the ministatus without object list.
    return ministatusResponse;
  }

  /**
   *
   */
  @Override
  @GET
  @Path("/{uri}/status")
  @Produces("text/xml")
  public PublishResponse getStatus(@QueryParam("sid") String sid,
      @QueryParam("log") @DefaultValue("") String log,
      @PathParam("uri") String uri) {

    defaultLogger.log(Level.FINEST, "Calling TG-publish#getStatus [" + uri
        + "]");

    // Get the publish response from the response map for the specified TextGrid URI.
    return getCurrentPublishResponse(uri, PublishAbs.getPublishResponse(uri),
        alreadyPublished(uri));
  }

  /**
   *
   */
  @Override
  @GET
  @Path("/{uri}/publishWorldReadable")
  @Produces("text/plain")
  public Response publishWorldReadable(
      @QueryParam("sid") String sid,
      @QueryParam("log") @DefaultValue("") String log,
      @PathParam("uri") String uri,
      @QueryParam("ignoreWarnings") @DefaultValue("false") boolean ignoreWarnings,
      @QueryParam("dryRun") @DefaultValue("true") boolean dryRun) {

    defaultLogger.log(Level.INFO, "Calling WorldReadable TG-publish service [" + uri + "]");

    // Check for other processes concerning this URI.
    synchronized (this) {
      if (otherProcessesRunning(uri)) {
        String message = "Another publish process is running at the monent for [" + uri
            + "], so please be patient. Thank you!...";
        defaultLogger.log(Level.INFO, message);
        return Response.status(HttpStatus.SC_BAD_REQUEST).build();
      }
    }

    // Creating publish response object.
    PublishResponse response = new PublishResponse(dryRun);

    defaultLogger.log(Level.FINE, "Publish response created");

    response.getPublishStatus().processStatus = ProcessStatusType.QUEUED;

    // Set process status to QUEUED to mark processing right away! Fixes #264.
    PublishAbs.updatePublishResponseMap(uri, response);

    // Create a CustomData object, and set all the necessary parameters from the service call to the
    // customData of the processData element.
    HashMap<String, Object> customData = new HashMap<String, Object>();

    customData.put(PUBLISH_RESPONSE, response);
    customData.put(SESSION_ID, sid);
    customData.put(LOG_PARAMETER, log);
    customData.put(TEXTGRID_URI, uri);
    customData.put(IGNORE_WARNINGS, ignoreWarnings);
    customData.put(DRY_RUN, dryRun);

    defaultLogger.log(Level.INFO,
        "CustomData object created [publishResponse=" + response + ", SID="
            + PublishUtils.purgePasswd(sid) + ", LOG=" + log + ", URI=" + uri + ", ignoreWarnings="
            + ignoreWarnings + ", dryRun=" + dryRun + "]");
    // Get the data queue.
    ArrayBlockingQueue<HashMap<String, Object>> dataQueue = TGPublishWorldReadable.getDataQueue();

    defaultLogger.log(Level.INFO,
        "TGPublishService#publishWorldReadable sucessfully started [" + uri + "]");

    // Add custom data to process starter queue.
    try {
      if (dataQueue != null) {
        dataQueue.put(customData);
      } else {
        throw new IOException();
      }
    } catch (InterruptedException e) {
      defaultLogger.log(Level.SEVERE, "Data could not be added to data queue [" + uri + "]");
      e.printStackTrace();
      return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
    } catch (IOException e) {
      defaultLogger.log(Level.SEVERE, "Please wait for the server to start!");
      e.printStackTrace();
      return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
    }

    defaultLogger.log(Level.INFO, "Data added to data queue [" + uri + "]");

    return Response.ok(uri).build();
  }

  /**
   *
   */
  @Override
  @GET
  @Path("/listWorldReadables")
  @Produces("text/xml")
  public WorldReadableMimetypes listWorldReadable() {

    defaultLogger.log(Level.INFO, "Calling TG-publish#listWorldReadable");

    // Set the mimetype regexp from ProcessStarter configuration in mai config file.
    WorldReadableMimetypes result = new WorldReadableMimetypes();
    result.setWorldReadableList(TGPublishWorldReadable.getMimetypeRegExps());

    return result;
  }

  /**
   *
   */
  @Override
  @GET
  @Path("/{uri}/publishSandboxData")
  @Produces("text/plain")
  public Response publishSandboxData(@QueryParam("sid") String sid,
      @QueryParam("log") @DefaultValue("") String log,
      @PathParam("uri") String uri) {

    defaultLogger.log(Level.INFO, "Calling SandboxData TG-publish service [" + uri + "]");

    // Check for other processes concerning this URI.
    synchronized (this) {
      if (otherProcessesRunning(uri)) {
        String message = "Another publish process is running at the monent for [" + uri
            + "], so please be patient. Thank you!...";
        defaultLogger.log(Level.INFO, message);
        return Response.status(HttpStatus.SC_BAD_REQUEST).build();
      }
    }

    // Creating publish response object.
    PublishResponse response = new PublishResponse(false);

    // Create a CustomData object, and set all the necessary parameters from the service call to the
    // customData of the processData element.
    HashMap<String, Object> customData = new HashMap<String, Object>();

    customData.put(PUBLISH_RESPONSE, response);
    customData.put(SESSION_ID, sid);
    customData.put(LOG_PARAMETER, log);
    customData.put(TEXTGRID_URI, uri);
    customData.put(IGNORE_WARNINGS, false);
    customData.put(DRY_RUN, false);

    defaultLogger.log(Level.INFO, "CustomData object created [publishResponse=" + response
        + ", SID=" + PublishUtils.purgePasswd(sid) + ", LOG=" + log + ", URI=" + uri + "]");
    // Get the data queue.
    ArrayBlockingQueue<HashMap<String, Object>> dataQueue = TGPublishSandboxData.getDataQueue();

    defaultLogger.log(Level.INFO,
        "TGPublishService#publishSandboxData sucessfully started [" + uri + "]");

    // Add custom data to process starter queue.
    try {
      if (dataQueue != null) {
        dataQueue.put(customData);
      } else {
        throw new IOException();
      }
    } catch (InterruptedException e) {
      defaultLogger.log(Level.SEVERE, "Data could not be added to data queue [" + uri + "]");
      e.printStackTrace();
      return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
    } catch (IOException e) {
      defaultLogger.log(Level.SEVERE, "Please wait for the server to start!");
      e.printStackTrace();
      return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).build();
    }

    defaultLogger.log(Level.INFO, "Data added to data queue [" + uri + "]");

    return Response.ok(uri).build();
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Gets the first object from a PublishReponse (normally the root edition or collection).
   * </p>
   * 
   * @param theResponse The publish response to get root object from.
   * @return A PublishObject list.
   */
  private static List<PublishObject> getRootObject(final PublishResponse theResponse) {

    List<PublishObject> result = new ArrayList<PublishObject>();
    if (!theResponse.getPublishObjects().isEmpty()) {
      result.add(theResponse.getPublishObjects().get(0));
    }

    return result;
  }

  /**
   * <p>
   * Returns the publish response from the current publish process, or a new one if PublishObject
   * has not yet finished building the list (objectListComplete==true) or another new one if no
   * response is existing yet.
   * </p>
   * 
   * @param theUri The URI to get the publish response from.
   * @param theResponse The response value to check.
   * @param alreadyPublished The already published flag.
   * @return A PublishResponse.
   */
  protected static PublishResponse getCurrentPublishResponse(String theUri,
      PublishResponse theResponse, boolean alreadyPublished) {

    PublishResponse result = theResponse;

    // Check for not existing response in response map.
    if (theResponse == null) {
      // Create and set new response.
      if (alreadyPublished) {
        result = createNewStatusResponse(theUri, StatusType.ALREADY_PUBLISHED,
            ProcessStatusType.FINISHED);
      } else {
        result = createNewStatusResponse(theUri, StatusType.NOT_YET_PUBLISHED,
            ProcessStatusType.NOT_QUEUED);
      }
    }

    // Response if response map is NOT null.
    else {
      // Check for objectListComplete flag! We do not want to deliver invalid XML accidentally, so
      // we only deliver the first object with the response as long as the list is not yet complete!
      if (!theResponse.objectListComplete) {
        result = createNewStatusResponseFromOldResponse(theUri, theResponse);
      }
    }

    return result;
  }

  /**
   * @param theUri The URI to be checked.
   * @return TRUE if an other process is already running with this URI's object.
   */
  private static boolean otherProcessesRunning(String theUri) {

    // Check first if this object is being published right now by another process, and abort without
    // updating the response object.
    synchronized (TGPublishServiceImpl.class) {
      // Get the publish response from the response map for the specified TextGrid URI and check for
      // URI in the current URI set.
      PublishResponse response = PublishAbs.getPublishResponse(theUri);
      if (response != null && response.getPublishStatus() != null
          && response.getPublishStatus().processStatus != null
          && response.getPublishStatus().processStatus.equals(ProcessStatusType.RUNNING)) {
        return true;
      }
    }

    return false;
  }

  /**
   * <p>
   * Create a new publish response with the given values.
   * </p>
   * 
   * @param theUri The URI of the object.
   * @param theStatusType The status type of the object.
   * @param theOverallStatus The overall process status.
   * @return A proper publish response.
   */
  private static PublishResponse createNewStatusResponse(String theUri, StatusType theStatusType,
      ProcessStatusType theOverallStatus) {

    PublishResponse result = new PublishResponse();

    // Set overall publish status.
    PublishStatus overallStatus = new PublishStatus();
    overallStatus.activeModule = TGPublishServiceImpl.class.getName();
    overallStatus.processStatus = theOverallStatus;
    result.setPublishStatus(overallStatus);

    // Create new publish object and add object status.
    PublishObject p = new PublishObject(theUri);
    p.status = theStatusType;
    result.addPublishObject(p);

    return result;
  }

  /**
   * <p>
   * Create a new publish response with the given values.
   * </p>
   * 
   * @param theUri The URI of the object.
   * @param theOldResponse The response to base the new response on.
   * @return A proper publish response.
   */
  private static PublishResponse createNewStatusResponseFromOldResponse(String theUri,
      PublishResponse theOldResponse) {

    PublishResponse result = new PublishResponse();

    // Set data from old response.
    result.setPublishStatus(theOldResponse.getPublishStatus());
    result.dryRun = theOldResponse.dryRun;
    result.objectListComplete = theOldResponse.objectListComplete;

    // Omit all until now added objects and only set the root object to not confuse anybody!
    // NOTE We cannot simply set the root object here in the old response, because we are constantly
    // working on the response object, don't we?
    result.setPublishObjects(getRootObject(theOldResponse));

    // Create new publish object.
    // TODO Do we need this here at all?!
    PublishObject p = new PublishObject(theUri);
    result.addPublishObject(p);

    return result;
  }

  /**
   * <p>
   * Check if the object with given URI is already published.
   * </p>
   * 
   * @param theUri
   * @return
   */
  private static boolean alreadyPublished(String theUri) {

    boolean result = false;

    // Check if already published.
    IsPublicRequest isPublicInput = new IsPublicRequest();
    isPublicInput.setResource(theUri);
    result = TGPublishAbs.getTgauth().isPublic(isPublicInput).isResult();

    return result;
  }

  /**
   * <p>
   * Wrapper class for starting WorkflowTool as a thread.
   * </p>
   */

  private class StartWorkflowTool extends Thread {

    private String configFilename;

    /**
     * <p>
     * Constructor.
     * </p>
     */
    public StartWorkflowTool(String theConfigFilename) {
      this.configFilename = theConfigFilename;
    }

    /**
     *
     */
    @Override
    public void run() {

      if (this.configFilename == null || this.configFilename.equals("")) {
        defaultLogger.log(Level.SEVERE,
            "WorkflowTool could not be started, please set config file beans.xml as constructor arg");
        return;
      }

      WorkflowTool woto = new WorkflowTool();

      try {
        woto.configure(this.configFilename);

        defaultLogger.log(Level.INFO, "WorkflowTool configured [" + this.configFilename + "]");

      } catch (Exception e) {
        defaultLogger.log(Level.SEVERE, CONFIG_ERROR_MESSAGE + ": " + e.getMessage());
        e.printStackTrace();
        return;
      }

      try {
        woto.run();
      } catch (Exception e) {
        defaultLogger.log(Level.SEVERE, RUN_ERROR_MESSAGE + ": " + e.getMessage());
        e.printStackTrace();
        return;
      }
    }

  }

}
