XML Schema
----------
The embedded PublishResponse XML schema can be found here:

.. literalinclude:: publishResponse.xml
   :language: xml


publishResponse Root Element
----------------------------
The publishResponse element has got the attribute dryRun, the element publishStatus and a list of publishObject elements.


dryRun
^^^^^^
The attribute dryRun just tells if a publish process is or was running or just a publish test without any consequences.


publishStatus
^^^^^^^^^^^^^
This element tells about the current status of the overall publishing process.

* module: Possibly a list of processed modules (not yet used by TG-publish).
* progress : The overall progress in percent. Each module can implement updating this progress value. Please use it for progress bars or other nice things.
* activeModule: Holds the currently activeModule, that is the qualified name of the corrresponding koLibRI Java ActionModule.
* processStatus: The processStatusdescribes the current overall process status, and can contain the following values:

  * FINISHED: This TG-publish process is finished, either with failure or success
  * RUNNING: The current process is still running, just try again later...
  * FAILED: The TG-publish process has failed. See object list for reasons.
  * NOT_QUEUED: The given TextGrid URI is not yet queued in the TG-publish queue. Either you have not started a publish process with this URI, or the process has not yet begun.


publishObject
^^^^^^^^^^^^^
Each of the objects in the list holds the following data, the root Collection or Edition is alco contained, normally as the first element of the list.

* uri: The URI of the object to publish.
* pid: The generated PID of the object.
* status: This status describes the status of the single object, and can contain:
  
  * OK: Object was processed successfully.
  * WARNING: Object was processed successfully, but there were warnings (can yet be published using the ignoreWarnings flag).
  * ERROR: An error occured while publishing.
  * NOT_YET_PUBLISHED: The object was not yet published (according to TG-auth*).
  * ALREADY_PUBLISHED: The object already is published (according to TG-auth*).

* publishError: Errors occured processing this object.

  * AUTH
  * WRONG_CONTENT_TYPE
  * NO_PUBLISH_RIGHT
  * PID_GENERATION_FAILED
  * MISSING_METADATA
  * ALREADY_PUBLISHED
  * METADATA_WARNINGS_EXIST
  * SERVER_ERROR
  * NOT_SPECIFIED

* referencedUris: List of URIs that are references from within this object, and NOT contained in the current Edition or Collection.
* publishWarning: Warnings occured processing this object.
  
  * CHECK_REFERENCES: Check the referenced URIs.
  * NOT_SPECIFIED: Other warning.
