.. tgpublish documentation master file, created by
   sphinx-quickstart on Thu May 21 16:19:08 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


TG-publish
==========

The TG-publish service shall be used to publish documents and/or TextGrid editions/collections created within the TextGridLab to the public TextGrid Repository. Documents that are published in the TextGrid Repository

* can be publicly searched from within the TextGridLab `TG-search GUI <https://doc.textgrid.de/Search/>`_.

* are publicly available in the TextGrid `Repository Browser <https://textgridrep.org>`_.

* have got an Handle persistent identifier to be able to stay accessible.

* are stored in a secure and robust repository environment that provides retrieval for the long term.

There are mainly three different ways to publish Data:

1. Valid TextGrid Editions or Collections that has already been created in the TextGridLab can be published. To learn more about the mandatory TextGrid Metadata please have a look at the `TextGrid Metadata Schema <https://textgrid.info/namespaces/metadata/core/2010>`_ documentation.

2. Some technical data from the TextGridLab can be published as WorldReadable objects. This objects and their metadata are tested differently for validity, they are not added to the public search index, and furthermore they get no PIDs.

3. Objects that shall be checked for validity after importing can be first published into the TextGrid Sandbox. After importing it is possible to check the objects in the `TextGrid Repository Sandbox Browser <https://textgridrep.org>`_ (please login and enable the sandbox feature in your preferences), and then finally publish them using a special policy of TG-publish, or again delete them if not valid/complete/full of errors. Only the final publishing is covered yet by TG-publish, hence you need write access to the RDF database. Publishing and deletion is yet only possible via the Import Tool External (koLibRI). Also the Sandbox is not yet used from within the TextGridLab.

Furthermore the TG-publish service can be used to copy TextGrid objects from **(a)** the TextGridLab and **(b)** the TextGridRep to own TextGrid projects in the TG-lab.

TG-publish uses the workflow library `koLibRI <http://kopal.langzeitarchivierung.de/index_koLibRI.php.en>`_ (kopal Library of Retrieval and Ingest).


GIT Repository
--------------
The GIT repository can be found here:

	https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest


Version
-------
This page is valid for the current TG-publish Service release version. To check the currently used TG-publish version simply try:

	https://textgridlab.org/1.0/tgpublish/version


koLibRI Documentation
---------------------
To get a more detailed understanding of the koLibRI workflow tool and its modules, you can find the koLibRI documentation here (basic modules only):

	`koLibRI Documentation Version 2.0 <https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/-/tree/main/doc>`_


Installation
------------
In general a complete TG-publish Service installation consists of (or needs):

1. The TG-publish service itself.

2. A second TG-crud instance for public data (TG-crud public).

3. A second TG-search instance for public data including a second IndexDB (ElasticSearch) and RDFDB (Sesame) instance (TG-search public).

4. The public TextGrid Repository Browser (including the TextGrid Sandbox).


Configuring TG-publish
----------------------

.. toctree::
	:maxdepth: 2

	configuration.rst


TextGridLab GUI
---------------
How the TG-publish and TG-copy is used from within the TextGridLab you can read here:

	`TG-publish GUI <https://doc.textgrid.de/Publish/>`_ and `TG-copy GUI <https://doc.textgrid.de/Context-Menu-of-the-Navigator-View/>`_ (Context Menu of the Navigator View)


TG-publish API
--------------

.. toctree::
	:maxdepth: 2

	api.rst


TG-publish Response
-------------------

.. toctree::
	:maxdepth: 2

	publishResponse.rst


Sources
-------

See tgpublish_sources_


Bugtracking
-----------
See tgpublish_bugtracking_


License
-------

See LICENCE_


.. _LICENCE: https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/-/blob/master/COPYING.LESSER.txt
.. _tgpublish_sources: https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest
.. _tgpublish_bugtracking: https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/issues?label_name[]=tg-publish
