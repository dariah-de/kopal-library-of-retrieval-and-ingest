API Overview
------------
====================  ==================================================  ==========================  ========
Method                Request Parameters                                  Response                    Examples
====================  ==================================================  ==========================  ========
copy                  sid, log, uri (repeatable), projectId, newRevision  TG-copy UUID                /tgpublish/copy?sid=SID&log=&uri=textgrid:1234&uri=textgrid:2345&projectId=TGPR&newRevision=FALSE
getStatus             TextGrid URI or TG-copy UUID                        publishResponse XML         /tgpublish/textgrid:1234/status
                                                                                                      /tgpublish/tgcopy:fc1023a3-739e-4727-b4b2-64711650dfc1/status
getVersion            none                                                Version String              /tgpublish/version
listWorldReadables    none                                                worldReadableMimetypes XML  /tgpublish/listWorldReadables
publish               sid, log, ignoreWarnings, dryRun                    HTTP 201 OK                 /tgpublish/textgrid:1234/publish?sid=SID&log=&ignoreWarnings=TRUE&dryRun=FALSE
publishSandboxData    sid, log, ignoreWarnings, dryRun                    HTTP 201 OK                 /tgpublish/textgrid:1234/publishSandboxData?sid=SID&log=&ignoreWarnings=TRUE&dryRun=FALSE
publishWorldReadable  sid, log, ignoreWarnings, dryRun                    HTTP 201 OK                 /tgpublish/textgrid:1234/publishWorldReadable?sid=SID&log=&ignoreWarnings=TRUE&dryRun=FALSE
====================  ==================================================  ==========================  ========


API Methods
-----------


getStatus()
^^^^^^^^^^^
Gets the status of a certain publish process using the specific TextGrid URI or the TG-copy UUID, e.g.

	http://textgridlab.org/1.0/tgpublish/textgrid:1234/status

Optional parameters are

* the RBAC Session ID, and
* the log service parameter.

The TextGrid URI or the TG-copy UUID is given in the RESTful URL path, see example above. You get back the publish response as an XML file, which is described below.


getVersion()
^^^^^^^^^^^^
The current version of the TG-publish installation you can get using

	http://textgridlab.org/1.0/tgpublish/version


listWorldReadables()
^^^^^^^^^^^^^^^^^^^^
The list of TextGrid objects that can be published as single WorldReadable objects you can get using

	http://textgridlab.org/1.0/tgpublish/listWorldReadables


publish()
^^^^^^^^^
Publishes data from the TG-lab directly to the TextGrid Repository, needs a TextGrid URI as resource, and takes the following as input parameters:

* The RBAC session ID (mandatory) [as used in the TG-lab]
* The log parameter (optional – default is "") [as used in the TG-lab, too]
* The ignoreWarnings trigger (default is FALSE) [Any warnings are ignored, if set to TRUE. Objects containing warnings will be published anyhow]
* The dryRun trigger (default is TRUE) [If set to TRUE, nothing will really be published, but just checked]
* The object to publish is given with the URI path parameter as following:

	http://textgridlab.org/1.0/tgpublish/textgrid:1234/publish?sid=MB896JHG&log=&ignoreWarnings=TRUE&dryRun=FALSE

The publish call only returns an HTTP 201 OK, that indicates the service did queue the request and will be processing it as soon as possible. If an HTTP error is returned, the publishing process was NOT started and will not be! You can check your requests outcome by calling status() on *textgrid:1234*:

- Directly after a publish() call, the getStatus() call will shortly be responding QUEUED and then RUNNING some short time after that, meaning the publication is in progress, or
    - After that only FAILED or FINISHED will be repsonded to report either an error or the successfully completed publication process.
- getStatus() delivers NOT_QUEUED, if the process has not been startet yet, and it has not yet been published, or
- getStatus() responds with FINISHED and ALREADY_PUBLISHED, if the collection or edition has already been published.

Please note: No sandbox is involved here!


publishSandboxData()
^^^^^^^^^^^^^^^^^^^^
Really publishes data finally to the TextGridRep that has been published to the sandbox before. At the moment this method is only used by the TG-import tool.


publishWorldReadable()
^^^^^^^^^^^^^^^^^^^^^^
Publishes an object as WorldReadable. The same parameters as in publish() do apply.


copy()
^^^^^^
Copies the objects belonging to the given URIs and all their aggregated objects (from aggregations/editions/collections) to the project belonging to the given project ID. You can copy objects from the TG-lab (non-public repository) and/or the public repository to your own projects, e.g. for further processing.

* The following parameters do apply.
* The RBAC session ID (mandatory) [as used in the TG-lab]
* The log parameter (optional – default is "") [as used in the TG-lab, too]
* The URIs of the objects to copy [This can be URIs of aggregations/collections/editions (in that case all subelements are gathered automatically) or URIs of single objects. This parameter is repeatable, so you can build a list of your favourite objects, and copy them allinto one of your projects.]
* The Project ID [of the project to copy the objects to.]
* newRevision [A boolean parameter to create new revisions from all copied objects (TRUE), or not (FALSE).]

Here an HTTP example of the copy() method:

	http://textgridlab.org/1.0/tgpublish/copy?sid=MB896JHG&log=&uri=textgrid:1234&uri=textgrid:2345&projectId=TGPR&newRevision=FALSE

The copy call returns a session UUID as a string, that **(a)** does indicate the service did queue the request and will be processing it as soon as possible, and that you can **(b)** query via the status() call your copy status. If an HTTP error is returned, the copying process was NOT started and will not be!

To track the provenience of the copied objects, an isDerivedFrom relation is set in the TextGrid metadata of each copied object.


The TG-publish WADL Service Description
---------------------------------------
The WADL file of the TG-publish response you can find as `Service WADL <http://textgridlab.org/1.0/tgpublish/?_wadl>`_.
