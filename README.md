# kopal Library for Retrieval and Ingest

## Service Versions and Releases

### TG-publish Service

- Branch **develop** is deployed on dev.textgridlab.org
- Branch **main** is deployed on textgridlab.org and test.textgridlab.org

### DH-publish Service

- Branch **develop** is deployed on trep.de.dariah.eu
- Branch **main** is deployed on repository.de.dariah.eu and dhrepworkshop.de.dariah.eu

## Releasing a new version

For releasing a new version of TG-publish or DH-publish, please have a look at the [DARIAH-DE Release Management Page](https://projects.academiccloud.de/projects/dariah-de-intern/wiki/dariah-de-release-management#release-mit-gitlab-ci) or see the [Gitlab CI file](.gitlab-ci.yml).

## Badges

[![OpenSSF Best Practices](https://www.bestpractices.dev/projects/8477/badge)](https://www.bestpractices.dev/projects/8477)
[![REUSE status](https://api.reuse.software/badge/gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest)](https://api.reuse.software/info/gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest)
[![Dependency Track Status](https://deps.sub.uni-goettingen.de/api/v1/badge/vulns/project/kopal-library-of-retrieval-and-ingest/develop)](https://deps.sub.uni-goettingen.de/projects/ce3212c3-03ba-43ca-9373-f17ada23d74a)
[![Dependency Track Container Status](https://deps.sub.uni-goettingen.de/api/v1/badge/vulns/project/kopal-library-of-retrieval-and-ingest-container/develop)](https://deps.sub.uni-goettingen.de/projects/ded38718-1843-4dd6-ac9b-f227ab531057)
