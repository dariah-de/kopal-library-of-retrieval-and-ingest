%----------------------------------------------------------------


For further extensions and customizations, the interfaces \texttt{ActionModule} and \texttt{ProcessStarter} become more interesting in particular. A certain familiarity with the structure of koLibRI and the classes of the workflow framework \texttt{de.langzeitarchivierung.kopal} is, however, necessary. At this point, a functional overview shall be provided. Please consult the javadoc API documentation for more detailed information.


%----------------------------------------------------------------

\subsection{The Structure of koLibRI}

%----------------------------------------------------------------

The package structure of koLibRI is lying within \texttt{de.langzeitarchivierung.kopal}. This package contains the central classes of the workflow framework. Underneath lie the following packages. It is possible to create subpackages for institution specific extentions.

\begin{itemize}
	\item \texttt{actionmodule}\\
	Contains the \texttt{ActionModule} interface and its implementing classes.
	\item \texttt{administration}\\
	Contains all classes implementing the search and administration interface of DIAS, see sectiob \ref{administration}, page \pageref{administration}.
	\item \texttt{formats}\\
	Contains the interface \texttt{MetadataFormat} in its implementing class Uof for the \textit{Universal Object Format} of the kopal project.
	\item \texttt{ingest}\\
	Classes for the ingest into DIAS.
	\item \texttt{jhove}\\
	Additional classes for the \textit{JSTOR/Harvard Object Validation Environment}, implemented by the DNB and SUB. 
	\item \texttt{processstarter}\\
	Contains the ProcessStarter interface and its implementing classes.
	\item \texttt{retrieval}\\
	Classes for the access to assets within DIAS.
	\item \texttt{ui}\\
	Classes for user interfaces and event handling.
	\item \texttt{util}\\
	Miscellaneous helper classes, e.g. methods for file handling, HTML- and HTTP-access, the TiffIMP, etc.
	\item \texttt{ws}\\
	The koLibRI Web Service, please see section \ref{webservices}.
\end{itemize}

The workflow framework \texttt{de.langzeitarchivierung.kopal} is consisting of eight classes at the moment:

\begin{itemize}
	\item \texttt{Policy}\\
	Manages a workflow and builds it from ist XML representation.
	\item \texttt{PolicyIterator}\\
	Iterates over the working steps of a workflow tree.
	\item \texttt{ProcessData}\\
	Contains the central information of an asset: Name of the process (e.g. the assets URN), policy, metadata, file list. The run method contains the logic for processing the policy. 
	\item \texttt{ProcessQueue}\\
	A queue of all \texttt{ProcessData} objects to be processed.
	\item \texttt{ProcessThreadPool}\\
	A ThreadPool, that processes ProcessStarter and ProcessData objects.
	\item \texttt{Status}\\
	Contains status value and description for the actual state of the ActionModule. The processing of the workflow is controlled by the status values.
	\item \texttt{Step}\\
	Single working step within the workflow of an asset. Contains -- among others -- functions to load and start action modules and to set their status values.
	\item \texttt{WorkflowTool}\\
	Provides the command-line interface, starts the process starters and is working off the processes. Working steps of a process are only processed if its value is \texttt{Status.TODO} and its predecessor ended with the status \texttt{Status.DONE}.
\end{itemize}

\begin{center}
\begin{figure}[h]
	\includegraphics[width=16cm]{pics/class-koLibRI}
	\caption{Das Klassendiagramm koLibRI}
\end{figure}
\end{center}


%----------------------------------------------------------------
\subsubsection{ProcessStarter}
%----------------------------------------------------------------

For each new SIP, the ProcessStarter has to process the following actions:

\begin{enumerate}
	\item Create a new ProcessData object,
	\item initialize start values of the metadata if necessary (if ActionModules need those for further processing), and
	\item add the ProcessData object to the process queue (\texttt{ProcessQueue.addElement()}).
\end{enumerate}

As long as at least one ProcessStarter is running, or the ProcessQueue still contains elements respectively, koLibRI does not end, and instead waits for new processes or the end of the actual processed one.


%----------------------------------------------------------------
\subsubsection{ActionModules}
%----------------------------------------------------------------

An ActionModul has to

\begin{enumerate}
	\item set its status value to \texttt{Status.RUNNING} upon invocation, and
	\item set ist status value to \texttt{Status.DONE}, when it was finished successfully.
\end{enumerate}

An ActionModule should \dots

\begin{itemize}
	\item \dots	set its status value to \texttt{Status.ERROR} if an error occurs. The error message will be logged seperately and modules with this status are not processed again. It is \emph{not} equal with the throwing of a Java-Exception, that means the module is not interrupted in its processing and could, for example, retry the erroneous action and reset its status at a success. A usual procedure would be the setting of \texttt{Status.ERROR} on a timeout, directly followed by the \texttt{Status.TODO}. The module would then return temporarily to the workflow framework. This way, the error will be logged and the processing will be retried later.
	\item \dots use the staus value \texttt{Status.WARNING}, if an event appears, that should be logged as warning (also in the database), but not lead to the abortion of the modules.
\end{itemize}

An ActionModule can \dots

\begin{itemize}
	\item \dots especially work with and update the metadata and files of the ProcessData object.
	\item \dots store \texttt{customData} in the HashMap of the ProcessData object for other working steps, which has no place in the structure behind UOF.
	\item \dots	access the configuration data.
\end{itemize}

\begin{center}
\begin{figure}
	\includegraphics[width=16cm]{pics/class-action}
	\caption{Klassendiagramm ActionModules}
\end{figure}
\end{center}


%----------------------------------------------------------------

\subsection{Configuring classes}

%----------------------------------------------------------------

The configuration of classes is done through the class \texttt{Configurator}. It gets initialized with the help of a XML file by the method \texttt{setConfigDocument()} and does then set the required configuration values of classes and objects, especially ActionModules and ProcessStarters, over various overridden configure methods. This is possible, because the classes and objects to configure are providing setter methods according to the JavaBeans specification.

If an ActionModule requires the value \texttt{useDatabase} from the configuration file for instance, it just provides a \texttt{setUseDatabase} method, that is called with the according parameter by the Configurator while execution of the program (see also section \ref{installation}).\\

Also possible ist the transfer of configuration values between objects and classes through getter and setter methods. Please see the API documentation for more details.


%----------------------------------------------------------------

\subsection{Metadata formats}

%----------------------------------------------------------------

The metadata specific functionalities and object structures are mostly concentrated in the \texttt{formats} package. The interface \texttt{MetadataFormat} declares the basic functionalities that are required for processing. These are not always specific enough to provide full independency of a concrete metadata format like the UOF.

In the current version of koLibRI, this in particular is valid for mapping the metadata for the database by the class \texttt{MappToHib}, as by the classes \texttt{PrepareMigration} and \texttt{MigrateFiles} to handle migration scenarios.

For the implementation of the interface \texttt{MetadataFormat}, a Java-XML-Binding through XML-Beans \cite{XMLB} was used. The XML schema, according to the UOF, was transferred to Java classes by the XML-Beans scomp compiler. This allows the structured and consistent usage of the schema in Java. In particular, it provides functionalities for writing, reading and validating of according XML files. To integrate individual schemas or metadata formats, according Java classes can be analogous generated through the XML-Beans scomp compiler. The main work for customization is then primary the implementation of the interface MetadataFormat and additional helper methods, customization of some ActionModules and of the class \texttt{MappToHib}, if the database is used.


%----------------------------------------------------------------
