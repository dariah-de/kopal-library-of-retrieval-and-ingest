%----------------------------------------------------------------


To use koLibRI for the ingest of archival packages it is essential to specify the working steps in the configuration file \texttt{policies.xml}\footnote{Which steps are needed is also dependent of the individual archiving requirements. Each institution has to declare these requirements for itself; the process of finding a long-term archiving policy can not be replaced by a technical solution (please see partner project nestor \cite{NES} for further information).}. The formal syntax is defined in the example file \texttt{policies.xsd} within the config directory, the functionality of the example workflows, together with the example configuration files, is explained in section \ref{beispiel-konfigurationen} on page \pageref{beispiel-konfigurationen}.\\

Workflows are seen as trees in the sense of the graph theory, that is processed in the direction from the root to the leafs. Each node is a step, whose child nodes are only processed, if the parent node was processed successfully. The child nodes are processed parallel if applicable, this can be very useful for time consuming tasks (like burning a CD-ROM or the transfer to another archive)\footnote{This functionality is not yet realized}.

An example workflow would process the following six steps in a row:

\begin{enumerate}
	\item
		Copy selective files to a process directory\\
		(ActionModule \texttt{FileCopyBase}),
	\item
		extract descriptive metadata for the files\\
		(ActionModule \texttt{MetadataExtractorDmd}),
	\item
		generate technical metadata for the files
		(ActionModule \texttt{MetadataGenerator}, please see section \ref{jhove} on page \pageref{jhove}),
	\item
		generate metadatafile \texttt{mets.xml}\\
		(ActionModule \texttt{MetsBuilder}),
	\item
		compress all files into an archival package\\
		(ActionModule \texttt{Zip}), and finally
	\item
		ingest the archival package into DIAS\\
		(or another archive system)\\
		(ActionModule \texttt{SubmitDummySipToArchive)}.
\end{enumerate}

Please have a look at the related policy file:
	
\begin{footnotesize}
\begin{verbatim}
<policies>
  <policy name="example">
    <step class="FileCopyBase">
      <step class="MetadataExtractorDmd">
        <step class="MetadataGenerator">
          <config>
            <property>
              <field>showHtmlImages</field>
              <value>true</value>
              <description>...</description>
            </property>
          </config>
          <step class="MetsBuilder">
            <step class="Zip">
              <step class="SubmitDummySipToArchive">
              </step>
            </step>
          </step>
        </step>
      </step>
    </step>
  </policy>
</policies>
\end{verbatim}
\end{footnotesize}

Besides the working steps itself, it has to be defined, how new working steps are being started and which initialization values they get. This is the meaning of the so called \texttt{ProcessStarters}, which are set at the start of the program either through the command line parameter \texttt{-p}, or in the configuration file through the parameter \texttt{defaultProcessStarter}. For basic usage, there are the ProcessStarters \texttt{MonitorHotfolderExample} and \texttt{Mo\-ni\-tor\-Http\-Lo\-ca\-tion\-Ex\-am\-ple}, which generate archival packages from files and folders beneath a given hotfolder or URL. A more special one would be a \texttt{server} (Server and \texttt{Client\-Loa\-der\-Ser\-ver\-Thread}), that makes one machine listening to retrieve archival packages from another machine for further processing. It is also possible to run multiple ProcessStarters at the same time by setting more than one value in the configuration parameter \texttt{de\-fault\-Pro\-cess\-Star\-ter}.\\

%----------------------------------------------------------------
\subsubsection*{Starting a WorkflowTool instance}
%----------------------------------------------------------------

The batch files, which are included within this release, are used to execute an instance of the WorkflowTool after a correct installation. To do this, the batchfiles simply have to be customized with the respective local configurations. Additionally, optional module packages, developed by other institutions can be included and used through the batchfiles (see section \ref{installation} on page \pageref{installation}).\\

\texttt{workflowtool -h} explains the command line options:

\begin{footnotesize}
\begin{verbatim}
usage: WorkflowTool
    -c, --config-file        The config file to use.
    -s, --show-properties    Prints the system properties and continues.
    -p, --process-starter    The process starter module which chooses items for
                               processing.
    -h, --help               Prints this dialog.
\end{verbatim}
\end{footnotesize}

Optional to the execution of the batch file, a WorkflowTool instance can also be started through command \texttt{java -jar kolibri.jar [OPTIONS]}\\

Because the access to DIAS is usually realized through an encrypted connection, the parameter  \texttt{-Djavax.net.ssl.trustStore=KEYSTORE\_LOCATION} will be necessary after the installation of the certificate of the DIAS hosting partner with the Java keytool. The path to the keystore file, as well as the path to the \texttt{known hosts} file, can be set in the configuration file.


%----------------------------------------------------------------

\subsection{Overview over more ActionModules and ProcessStarters}
\label{amundps}

%----------------------------------------------------------------


%----------------------------------------------------------------
\subsubsection{ProcessStarter}
%----------------------------------------------------------------

\begin{description}
	\item[\texttt{Server}] \ \
	The koLibRI instance is listening for network connections and starts a new thread for each request, that executes the necessary actions. This \texttt{ServerThread} has to be set in the configuratiopn file as \texttt{serverClassName}, for example \texttt{Cli\-ent\-Loa\-der\-Ser\-ver\-Thread} (takes archival packages from other koLibRI instances).
		
	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item	serverClassName
		\item	defaultConnectionPort
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{MonitorHotfolderExample}] \ \
	Example ProcessStarter that builds an archival package (Submission Information Package -- SIP) for every subdirectory of a given hotfolder directory. It monitors the directory, stated as \texttt{hotfolderDir} and processes all existing subfolders first. Then it processes all added subfolders. The names of the subfolders are interpreted as persistent identifiers for the SIPs.

	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item	hotfolderDir
		\item readDirectoriesOnly
		\item	startingOffset
		\item	checkingOffset
		\item	addingOffset
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{MonitorHttpLocationExample}] \ \
	Same as MonitorHotfolderExample, but files and directories can be retrieved by an URL. This URL is also monitored and added directories are processed. Directory names are interpreted as persistent identifiers as before.

	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item	urlToMonitor
		\item	readDirectoriesOnly
		\item	startingOffset
		\item	checkingOffset
		\item	addingOffset
	\end{itemize}
\end{description}


%----------------------------------------------------------------
\subsubsection{ActionModule}
%----------------------------------------------------------------

\begin{description}
	\item[\texttt{AdaptHtmlPage}] \ \
	Exchanges links in HTML files. This action module is used by the \texttt{Mi\-gra\-tion\-Ma\-na\-ger} to change filenames to those of migrated and most likely different filesnames, e.g. to keep an HTML page and its linked images useable.

	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item htmlPageFilename
		\item replaceFulltextOnly
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{AddDataToDb}] \ \
	Adds configurable information to the database, if \texttt{useDatabase} is set in the configuration file.

	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item	storeFileData
		\item	storeFileDataTechMd
		\item	storeDc
		\item	storeIds
		\item storeCustomData
		\item customDataEelements
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{AlreadyIngestedChecker}] \ \
	Using the XORed checksumme from the koLibRI database the \texttt{Al\-rea\-dy\-In\-ges\-ted\-Chec\-ker} checks if a SIP already was ingested.
		
	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item	waitUntilNextTry
		\item	errorIfChecksumExists
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{AskDIASifIngested}] \ \
	Tests via DIAS access, if a SIP with a certain external ID already was ingested before.
\end{description}

\begin{description}
	\item[\texttt{CleanPathToContentFiles}] \ \
	Deletes the files and folders in the temporary processing directory.
\end{description}

\begin{description}
	\item[\texttt{CleanUpFiles}] \ \
	Deletes created ZIP files and METS files from the destination directory.
\end{description}

\begin{description}
	\item[\texttt{DiasIngestFeedback}] \ \
	Uses the DIAS ticketing system to request the status of an SIP. All data put in the koLibRI database for this SIP can be removed if an ingest was unsuccessful. Use the config values below to configure what table entries to delete.
		
	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item	removeFileOnError
		\item removeTechmdOnError
		\item removeDublincoreOnError
		\item removeIdentifierOnError
		\item removeVariousOnError
		\item removeDiasOnError
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{Executor}] \ \
	Executor provides the functionality to invoke system based command line tools. The static arguments have to be set in the configuration file.

	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item command
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{FileCopyBase}] \ \
	Files and folders of the object are copied to a temporary processing directory, to work with these copies, without altering the original files. The ActionModul can be extended by own modules, i.e. to change the names of the copied files or ignore certain files for the archival package.
\end{description}

\begin{description}
	\item[\texttt{FileCopyHttpBase}] \ \
	Copies files and folders over HTTP. This allows archiving files from a webserver. For more, see \texttt{FileCopyBase}.
\end{description}

\begin{description}
	\item[\texttt{MetadataExtractorDmd}] \ \
	Includes descriptive Metadata into the archival package. This module has to be customized for individual needs. The given example only shows how descriptive metadata is embedded into a SIP. The source of this metadata is dependant of the structures of the individual institutions. Inherits from \texttt{MetadataExtractorBase}.
\end{description}

\begin{description}
	\item[\texttt{MetadataExtractorDigiprovmd}] \ \
		Includes provenance metadata into the archival package. This module also has to be customized for individual needs. Inherits from \texttt{MetadataExtractorBase}.
\end{description}

\begin{description}
	\item[\texttt{MetadataGenerator}] \ \
		Generates technical metadata for all content files of the archival package with the use of the JHOVE toolkit from the Harvard University Library (please see section \ref{jhove} on page \pageref{jhove} for further information).

	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item	acceptedUnknownFileFormat
		\item	showGifGlobalColorTable
		\item	showPdfPages
		\item	showPdfImages
		\item	showPdfOutlines
		\item	showPdfFonts
		\item	showMixColorMap
		\item	showTiffPhotoshopProperties
		\item	showWaveAESAudioMetadata
		\item	showHtmlLinks
		\item	showHtmlImages
		\item	showIso9660FileStructure
		\item jhoveIsVerbose
		\item errorIfSigmatchDiffers
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{MetsBuilder}] \ \
	Creates a METS file for the archival package out of the gathered informations. Responsible for the creation is the class, stated in \texttt{mdClassName}. For kopal, the implementation \texttt{Uof.java} of the interface \texttt{MetadataFormat} creates a METS file according to the UOF of the kopal project.
\end{description}

\begin{description}
	\item[\texttt{MigrateFiles}] \ \
	Used by the MigrationMagager to convert files in another -- likewise newer -- file format.

	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item migrationToolExecutionCommand
		\item sourceFileName
		\item destFileName
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{PrepareMigration}] \ \
	This module extends the UOF with the provenance metadata needed for a migration.
		
	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item migrateMetadataRecordCreator
		\item provmdComments
		\item provmdPermission
		\item provmdCreator
		\item provmdPurpose
		\item provmdResult
		\item provmdSteps
	\end{itemize}		
\end{description}

\begin{description}
	\item[\texttt{SubmitDummySipToArchive}] \ \
		Example module for demonstration purposes.
\end{description}

\begin{description}
	\item[\texttt{SubmitSipToClientLoader}] \ \
		Submits a SIP to a client loader. This module can be used by more than one asset builder at the same time, that send their archival packages to a central WorkflowTool instance. Please see "Informations about the example configuration" for further information.

	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item	clientLoaderServer
		\item	clientLoaderPort
		\item	submitPolicyName
		\item fileSubmitNotificationTime
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{SubmitSipToDias}] \ \
		Transfers a SIP to the DIAS, used by kopal, and is the implementation of the SIP specification \cite{SIP} for ingests into kopal-DIAS, provided by IBM. Some of the following configuration values are set in the common section of the main config file.

	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item	cmUser
		\item	cmPwd
		\item	ingestPort
		\item	ingestServer
		\item	knownHostsFile
		\item	preloadArea
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{TiffImageMetadataProcessor}] \ \
		Validates all TIFF image files within the archival package with the use of JHOVE first, see section \ref{tiffimp}, page \pageref{tiffimp}.
		
	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item	correctInvalidTiffHeaders
		\item	verbose
		\item	separateLogging
		\item	separateLogfileDir
		\item	jhoveConfigFile
		\item nonVerboseReportingTime
	\end{itemize}
\end{description}


\begin{description}
	\item[\texttt{Unzip}] \ \
	Un-zips a ZIP file into the given directory.

	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item tempDir
		\item sourceFile
		\item	deleteZipOnExit
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{Utf8Controller}] \ \
	With the help of this module, a created METS file, that is always UTF-8 encoded, can be checked for invalid UTF-8 characters and correct them. The source of the used Utf8FilterInputStream is the utf8conditioner by Simeon Warner (\url{simeon@cs.cornell.edu}), which was adapted to JAVA for the use in koLibRI.

	Important configuration values:
	\begin{itemize}
		\setlength{\itemsep}{-1mm}
		\item	filename
		\item	storeOriginalFile
		\item	originalPostfix
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{XorFileChecksums}] \ \
	Generates an XOR checksum from all existing file checksums and puts it into the database. This XOR checksum can be used to check if an identical SIP has already been ingested into the archive.

	Important configuration values:
	\begin{itemize}
		\item	errorIfAlreadyIngested
	\end{itemize}
\end{description}

\begin{description}
	\item[\texttt{Zip}] \ \
	Compresses the content files together with the METS file (mets.xml) into a compact package.

	Important configuration values:
	\begin{itemize}
		\item compressionLevel
	\end{itemize}
\end{description}

Additional -- partly very specialised -- ActionModules and ProcessStarters are currently available by request from the kopal project partner The German National Library (DNB) and the Goettingen State and University Library (SUB).


%----------------------------------------------------------------

\subsection{Get started! -- Information about the example configuration}
\label{beispiel-konfigurationen}

%----------------------------------------------------------------

There are three ways to use the example configuration files. The first is to configure a single machine, so that it creates and ingests archival packages into an archival system itself -- a combination of SIP creation and SIP transfer with one instance of the \texttt{WorkflowTool} class.\\

There is also the possibility to configure multiple machines as so called \texttt{AssetBuilder} and let them create archival packages resulting of different workflows and submit them to another central machine. This central machine, the so called \texttt{ClientLoader}, receives all these archival packages and ingests them into an archival system -- a separation of the SIP creation and the SIP transfer with one instance of the WorkflowTool as ClientLoader, and multiple instances as AssetBuilders.\\

The third way is to use a database for bookkeeping over the ingested and created archival packages. This has currently only been tested further with a single instance of the WorkflowTool in standalone mode (see first point).\\

ATTENTION: The use of the database has not yet been tested for the parallel use of multiple WorkflowTools as standalone AssetBuilders. Usage for multiple independent instances of the Workflowtool in standalone mode is theoretically possible, but must be used at one own's risk. Usage of the database through a ClientLoader is not yet possible.


%----------------------------------------------------------------
\subsubsection{Standalone mode -- Creation and ingest of archival packages with a single instance of the WorkflowTool}
\label{standalone}
%----------------------------------------------------------------

The file \texttt{example\_config\_standalone.xml} is used for a single instance of the class WorkflowTool. To execute this example, all that must be done, is to simply fill all tags enclosed by stars (***) with the right values. These are four path values for the work and temporary directories, a URL or path value depending on the ProcessStarter, and maybe a path for more logfiles. The \texttt{description} tags in the configuration file give a description of the according values.\\

There is the choice out of two ProcessStarters at the moment. \texttt{Mo\-ni\-tor\-Hot\-fol\-der\-Ex\-am\-ple} takes all subfolders of the directory stated in \texttt{hotfolderDir} and further processes all subfolders added later as an archival package to ingest (SIP), by adding the subfolders to the \texttt{ProcessQueue} and processing them. \texttt{MonitorHttpLocationExample} does the same, but checks files and subfolders of an URL stated in \texttt{urlToMonitor}. Of course the stated directories must exist and contain files and folders, of which JHOVE can extract technical metadata, to get meaningful and usable results. Any kind of file structure and files can be used to test this module. See section \ref{jhove} for further information.\\

The policy form the policy configuration file \texttt{policies.xml} that is used here, is \texttt{ex\-am\-ple\_stand\-a\-lone} and executes the following ActionModules for each step. More informations for each module can be obtained through the documentation of the Java classes or the \texttt{description} tags in the configuration file (section \ref{amundps}, page \pageref{amundps}).

\begin{itemize}
	\setlength{\itemsep}{-1mm}
	\item XorFileChecksums
	\item TiffImageMetadataProcessor
	\item MetadataExtractorDmd
	\item MetadataGenerator
	\item MetadataExtractorDigiprovmd
	\item MetsBuilder
	\item Utf8Controller
	\item Zip
	\item AlreadyIngestedChecker
	\item AddDataToDb
	\item SubmitDummySipToArchive
	\item CleanPathToContentFiles
	\item CleanUpFiles
\end{itemize}

The command to run the standalone example from a command line is as follows:

\begin{center}
	\texttt{java -jar kolibri.jar -c config/example\_config\_standalone.xml}
\end{center}

or better (if \texttt{workflowtool.bat} or the \texttt{workflowtool} script has been configured), to set some more, maybe important parameters:

\begin{center}
	\texttt{workflowtool -c config/example\_config\_standalone.xml}
\end{center}


%----------------------------------------------------------------
\subsubsection{ClientLoader/AssetBuilder constellation -- Separation of the creation and ingest of archival packages with two or more instances of the WorkflowTool}
%----------------------------------------------------------------

The file \texttt{example\_config\_clientloader.xml} is used with a single instance of the class WorkflowTool, the file \texttt{example\_config\_assetBuilder.xml} can be used for one or more instances of the WorkflowTool each as AssetBuilder. For the communication between the instances, some more configuration values are needed in the configuration files. The division of work is described below.\\

The AssetBuilders can be started on different machines. This way, each machine can process an individual workflow for the creation of archival packages. For individual workflows, also individual policies and configuration files have to be created. The AssetBuilders in this example process the same  steps as in the last example. Again, both ProcessStarters stated above, can be used. The difference is, that the created SIPs are not directly transferred to an archival system by each AssetBuilder himself, but are submitted to a central ClientLoader first. To do this, the AssetBuilders use the ActionModule \texttt{SubmitSipToClientLoader}. The policy is \texttt{example\_assetBuilder}.\\

A WorkflowTool instance with the ProcessStarter \texttt{Server} is started as ClientLoader. It is then waiting for request on a port, defined by the field defaultConnectionPort in the configuration file.
When the server receives a request from an AssetBuilder, it starts a ClientLoaderServerThread, configured by \texttt{serverClassName}. These Threads are listening for the transfer of a SIP from the requesting AssetBuilder. Multiple SIPs from more than one AssetBuilder can be received at the same time.\\

The policy here is \texttt{example\_clientloader} and contains two ActionModules:

\begin{description}
	\item[\texttt{SubmitDummySipToArchive}] \ \
	Is the same ActionModule as in the above example. The separation of AssetBuilders and ClientLoaders is suggestive if multiple AssetBuilders shall be used, e.g. to process different workflows at the same time and create SIPs from different sources. The ClientLoader gathers all archival packages from all AssetBuilders centrally, and can also apply additional actions on them, e.g. burning CD-ROMs, re-validating the METS files or inform other institutions about newly ingested SIPs.	
\end{description}

\begin{description}
	\item[\texttt{CleanUpFiles}] \ \
	After successfull ingest into the archive system, the archival packages, delivered by the AssetBuilders, are deleted. Commented out in the example for better understanding.
\end{description}

The command to start the AssetBuilder/ClientLoader example on a command line is as follows:

\begin{enumerate}
	\item
		Starting the ClientLoader:\\
		\texttt{java -jar kolibri.jar -c config/example\_config\_clientloader.xml}
	\item
		Starting an AsserBuilder:\\
		\texttt{java -jar kolibri.jar -c config/example\_config\_assetbuilder.xml}
	\item
		Starting more AssetBuilders:\\
		\texttt{java -jar kolibri.jar -c config/example\_config\_assetbuilder.xml}
\end{enumerate}
			
or better (if \texttt{workflowtool.bat} or the \texttt{workflowtool} script has been configured), to set some more, maybe important parameters:
	
\begin{enumerate}
	\item
		Starting the ClientLoader:\\
		\texttt{workflowtool -c config/example\_config\_clientloader.xml}
	\item
		Starting an AsserBuilder:\\
		\texttt{workflowtool -c config/example\_config\_assetbuilder.xml}
	\item
		Starting more AsserBuilder (optionally with different configuration files):\\
		\texttt{workflowtool -c config/example\_config\_assetbuilder.xml}
\end{enumerate}


%----------------------------------------------------------------
\subsubsection{Standalone mode using the database -- Creation and ingest of archival packages with a single instance of the WorkflowTool using the database}
%----------------------------------------------------------------

First a database has to be created, according to the database documentation. If this was successful, the following steps have to be done:

\begin{enumerate}
	\item
	The WorkflowTool has to be configured as described in point \ref{standalone}.
	\item
	The tag \texttt{useDatabase} of the configuration file has to be set to \texttt{TRUE}.
	\item
	All database related values have to be filled with the right contents. These values are marked by \#\#\# in the configuration file.
	\item
	The WorkflowTool can now be started as described in point \ref{standalone}. The database is now updated with useful information during the ingest process. See the database documentation for further information about the use of the database.
\end{enumerate}

If the database shall not be used, all that has to be done, is to set the value of \texttt{useDatabase} to \texttt{FALSE}. The modules that use the database can remain in the policy file, because all database modules check this parameter.


%----------------------------------------------------------------
