%----------------------------------------------------------------

\subsection{Introduction}

%----------------------------------------------------------------

The ability to act as a web service is one of the main enhancements of version 1.0 against the previous version 0.7. As a web service, koLibRI does not have to be called on the command line to work as a batch process. It is started in a servlet container by the web administrator and runs uninterruptedly listening to predefined port for new requests.

Each request is a SOAP \cite{SOAP} message containing besides the user name, password and institution all the parameters needed to process this request. Java class \texttt{WebServiceServer} gets the message and calls the appropriate method in one of the middle layer classes \texttt{In\-gest\-Lay\-er}, \texttt{Ac\-cess\-Lay\-er}, \texttt{WS\-Utils}.

All the services return \texttt{ResponseElemDocument} containing mainly a status code, human readable text for that code, DIAS internal ID of this digital object (where needed), time to wait (if DIAS busy), link to the requested file (if there is file to download), extra info string (if necessary), error Message (if any occured) and a custom XML field if the answer contains complex data. So the client can analyse the results of the request after sending each message.


%----------------------------------------------------------------

\subsection{Installation}

%----------------------------------------------------------------

Installation of koLibRI as a web service is very simple. Just unpack the provided ZIP archive in the Axis2 directory \texttt{/WEB-INF/services} and start Tomcat.

Tree files are all you need: \texttt{kolibri\_web\_services.aar} contains all the necessary program code and libraries to run the web service. The other ones are the configuration and policy files in XML format. They are the same configuration and policy files described in previous chapters. For a real productive environment to ingest, there must be another file containing cryptographic certificates.

Installation is so simple if you already have a running Axis2 \cite{AXIS2}. If not, you have to install Axi2 first. koLibRI is tested with Axis2 version 1.2. Although Axis2 has its own web server and therefore can run without another web server, it is recommended to run it under a \emph{real} server. koLibRI is tested with Tomcat version 5.5.23 \cite{TOM}.


%----------------------------------------------------------------

\subsection{Configuration}

%----------------------------------------------------------------

Operating system, Java SDK, Tomcat and Axis2 should be configured as explained in their corresponding documentation. All koLibRI configuration is done in its own configuration file as described in previous chapters.


%----------------------------------------------------------------

\subsection{Starting up}

%----------------------------------------------------------------

Run Tomcat with the help of \texttt{startup} script under \texttt{\$CATALINA\_HOME/bin} directory. Test the functionality of Tomcat and Axis2 by calling their test pages. Use \texttt{setParameters} method to set the actual parameters if necessary. You can use \texttt{TestWebServices} class as a simple example or write your own code which calls \texttt{WebServiceClient}.

Now you are ready to start koLibRI web service. Call \texttt{initCache} to initialize the cache -- please keep in mind that web service will not function without initializing the cache. After initializing the cache allow Ingest and/or Retrieval by calling \texttt{allowIngestRequests} and/or \texttt{allowRetrievalRequests} respectively. If no error message returned is, web service is ready for productive work.

Please note that the DIAS installation in GWDG checks the IP address to determine if you are allowed to connect DIAS. So don't be suprised that all your connection requests are refused and you get just errors and exceptions. Use koLibRI to generate SIPs in UOF format but do \emph{not} try to upload it to DIAS.

%----------------------------------------------------------------

\subsection{How Ingest works}

%----------------------------------------------------------------

As seen in WSDL file, ingest service needs a policy describing how to handle that digital document and one to six metadata blocks of type \texttt{customXMLType}. The first one is mandatory and contains -- among others -- the \emph{must-have} information Persistent Identifier and location of the files belonging to this document. More information like label, checksums, etc. would be better especially for checking the transfer errors but are not mandatory at the moment.

\texttt{customXMLType} can theoretically transfer any valid XML. This version focuses on LMER simply because the UOF also uses LMER.

The other five optional metadata blocks are for descriptive metadata in different formats. They will be copied unmodified into METS DescMd section. According to DIAS documentation, if descriptive metadata exist the first one must be in Dublin Core \cite{DCORE}.

In this first version of web service ingest is not fully implemented. To generate complete SIPs please use the standalone koLibRI.


%----------------------------------------------------------------

\subsection{How Retrieval works}

%----------------------------------------------------------------

Sequence diagramm \ref{fig:retrieval} shows the mode of operation of retrieval in time. Typically users search relevant information in library database -- which is \emph{not} a part of koLibRI -- and have a list of documents as search result. If they think a specific document in this result list is the right one, they choose this document.

\begin{figure}
	\centering
	\includegraphics[height=20cm]{pics/sequenzdiagramm_retrieval}
	\caption{Sequence diagramm of retrieval including external user interface}
	\label{fig:retrieval}
\end{figure}

In case this document is an electronic one, the user interface sends the URN of the document to koLibRI and requests the metadata.
Web service requests from DIAS the full metadata for that URN. DIAS responds with a link to the zipped \texttt{mets.xml} of that document and its internal DIAS id. Web service generates a unique directory path using this unique internal id and checks there if \texttt{mets.zip} is already downloaded. If yes it saves time and resources and sends a link to the saved copy and does not download it again.

The user interface extracts the \texttt{mets.xml}, processes it and prepares the user with a table of contents for that digital document. User chooses a specific file from the list. The user interface sends the DIAS internal id and file name to web service. Web service looks in its cache and decides if it must request the file from DIAS or if the file is ready in cache.


%----------------------------------------------------------------

\subsection{Organisation of the cache}

%----------------------------------------------------------------

Web service uses a directory to save the downloaded files and metadata. This directory is called cache because of its positive side effect that if web service determines that a digital object already has been downloaded a short time ago, this document needs not to be requested and downloaded from DIAS once more. Especially for tape access in DIAS or downloading large files this cache speeds up things treamendously and at the same time reduces the load on DIAS-System.

\begin{figure}
	\centering
	\includegraphics[height=20cm]{pics/speicher-organisation}
	\caption{Organisation of the directory structure in cache}
	\label{fig:cache}
\end{figure}

Actually the cache is not one single directory but a big tree of directories (see figure \ref{fig:cache}) organised in tree layers and named with digits 0\dots 9. It makes use of the algorithm that the DIAS uses to generate its unique internal id's to determine the location of a file. DIAS genarates for each version of each digital document an unique number in form

\begin{center}
	\texttt{[prefix]:YYYYYMMDDhhmmssSSSnnnn}
\end{center}

where

\begin{itemize}
	\setlength{\itemsep}{-1mm}
	\item{\texttt{YYYYY} stands for year (02007 for this year)}
	\item{\texttt{MM} stands for month (01\dots 12)}
	\item{\texttt{DD} stands for day (01\dots 31)}
	\item{\texttt{hh} stands for hour (00\dots 23)}
	\item{\texttt{mm} stands for minutes (00\dots 59)}
	\item{\texttt{ss} stands for seconds (00\dots 59)}
	\item{\texttt{SSS} stands for milliseconds (000\dots 999)}
	\item{\texttt{nnnn} stands for continious number if more than one assets are ingested in the same millisecond.}
\end{itemize}

Web service takes this id and generates a unique path for that asset so its anytime possible to look in the directory tree to see if that document in that version has been already downloaded. The administrator shall control storage media often to be sure that it is not full. Web service has no automatism to delete the old entries. It offers the \texttt{eraseOldCacheEntries} message to be called if necessary.\\

A more intelligent cache organisation is possible but would be much more complex without a significant win.


%----------------------------------------------------------------

\subsection{Available operations}

%----------------------------------------------------------------

Web service offers many operations:

\begin{itemize}
	\setlength{\itemsep}{-1mm}
	\item{\texttt{getFileFormatList}}
	\item{\texttt{reloadFileFormatList}}
	\item{\texttt{ingestDocument}}
	\item{\texttt{ingestSIP}}
	\item{\texttt{blockNewIngestRequests}}
	\item{\texttt{allowIngestRequests}}
	\item{\texttt{getIngestStatistics}}
	\item{\texttt{resetIngestStatistics}}
	\item{\texttt{getMETS}}
	\item{\texttt{getFile}}
	\item{\texttt{getDIP}}
	\item{\texttt{getDIPExternal}}
	\item{\texttt{getModifiedSince}}
	\item{\texttt{getHistory}}
	\item{\texttt{blockNewRetrievalRequests}}
	\item{\texttt{allowRetrievalRequests}}
	\item{\texttt{getRetrievalStatistics}}
	\item{\texttt{resetRetrievalStatistics}}
	\item{\texttt{initCache}}
	\item{\texttt{eraseOldCacheEntries}}
	\item{\texttt{deleteCache}}
	\item{\texttt{setParameters}}
	\item{\texttt{changePassword}}
\end{itemize}

All needed detailed information is in JavaDoc. So it will not be repeated here. Please note that some of those operations are planned for the next release. So they are not implemented yet but their \emph{skeleton} is ready. Therefore those operations just return

\begin{center}
	\texttt{598 Sorry. This service is not implemented at the moment.}
\end{center}

without causing an error.


%----------------------------------------------------------------

\subsection{Information for programmers}

%----------------------------------------------------------------

Here is a short summary for programmers. For detailed information please read the JavaDoc and see the source code.


%----------------------------------------------------------------

\subsubsection{WSDL}

%----------------------------------------------------------------

Web service in koLibRI is implemented following the \emph{contract first} method. That means first all the types, messages, ports and bindings are defined in Web Services Description Language \cite{WSDL}.
Those definitions of the WSDL file you can find in section \ref{wsdl-code} on page \pageref{wsdl-code}.


%----------------------------------------------------------------
\subsubsection{The generated Java package}
%----------------------------------------------------------------

Using those definitions, in WSDL file the needed java classes are generated with the help of Axis2 code generator plugin for Eclipse. There is also a command line version of code generator in Axis2 distribution. This machine generated code is for humans not easy to understand and therefore not in CVS. Instead in koLibRI just the compiled jar file is used and distributed. If you are interested in those classes you can generate them using the same \texttt{wsdl2java} tool and WSDL file.


%----------------------------------------------------------------
\subsubsection{The Server}
%----------------------------------------------------------------

On the server side, the java class \texttt{WebServiceServer} which implements \texttt{Ko\-lib\-ri\-Web\-Ser\-vice\-Ske\-le\-ton\-In\-ter\-face} from generated jar accepts the SOAP messages and extracts the needed parameters to call the appropriate method in one of the middle layer classes \texttt{In\-gest\-Lay\-er, AccessLayer, WSUtils}.


%----------------------------------------------------------------
\subsubsection{The Client}
%----------------------------------------------------------------

On the client side, the java class \texttt{WebServiceClient} which creates a new instance of \texttt{Ko\-lib\-ri\-Web\-Ser\-vice\-Stub} from generated jar takes the needed parameters and constructs the SOAP messages. It is designed as a helper class to make the communication on client side easier by accepting java data types instead of complex XML constructs.


%----------------------------------------------------------------
\subsubsection{Test Class}
%----------------------------------------------------------------

\texttt{TestWebServices} is on one side a simple example which demonstrates the usage of \texttt{Web\-Ser\-vice\-Cli\-ent} and the communication with web service, on the other side it was necessary to test the functionality of \texttt{Web\-Service\-Server}. It is not for production enviroments. You should implement similar calls to \texttt{Web\-Service\-Server} (with or without using \texttt{Web\-Service\-Client}) in your own system.


%----------------------------------------------------------------
