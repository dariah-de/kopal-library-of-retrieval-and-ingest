%----------------------------------------------------------------

\subsection{Description}

%----------------------------------------------------------------

The MigrationManager is a koLibRI component which manages and executes migrations.
Objects which have to be migrated and those which should be result of the
migration can be described. Depending on those objects and your own requirements
individual migration workflows can be configured and different migration tools
can be used.

The MigrationManager is a prototype and the current functionality is between the
simple and complex scenarios described below. It is not sufficiently tested to be
used for the productive mass use and not all possibilities of DIAS, koLibRI and 
the UOF are implemented. But for exemplary test operations and for 
conception of migration processes the MigrationManager can be regarded as usable.

Migration is in itself a demanding process in the operations of an archive which
can not be isolated from the other processes and plannings of the archive
operations and the available infrastructure. A very simple scenario which can be
implemented without problems with the current version would be:

\begin{itemize}
\item The source file format TIFF and destination file format JPEG2000 are entered
on the command line,
\item the matching objects are looked up on a set of local available METS files, and
\item a simple migration workflow consisting of downloading the affected
archival packages, migration of files with a command line progamm and the new 
building and ingesting of the archival packages is executed.
\end{itemize}

A complex scenario which can not be implemented because of missing supra
institutional infrastructure and know-how, but for which the basic architecture
and interfaces for further development are present:

\begin{itemize}
  \item A koLibRI instance receives a message from a technology watch service 
  or format registry (like GDFR \cite{GDFR} oder PRONOM \cite{PRONOM}) that an
  image format produced by a specific programme and with specific technical
  properties has to be migrated into another format, 
	\item the objects and files with the matching	properties are looked up
	in the DIAS data management,
	\item because the affected files and objects are templates used by other objects,
	some objects have to be migrated and adapted together,
	\item in some files the affected image formats are embedded,
	\item an external service registry is queried for an appropriate migration
	service (e.g. a grid service for the conversion of big data sets), and
	\item the depending files and objects are adapted. 
\end{itemize}
 

%----------------------------------------------------------------

\subsection{Design}

%----------------------------------------------------------------

The MigrationManager itself is a \texttt{ProcessStarter} which consists of
several functional subentities:

\begin{description}
  \item[\texttt{MigrationEventListener}] \ \
  Fetches or receives messages about necessary migrations. This happens currently
  with the command line where the characterisation of source and
  destination objects is specified.
  \item[\texttt{ObjectCharacter}] \ \
  A characterisation of objects like e.g. source and destination file format but
  also more complex properties like producing software or encoding. Because
  there is no standard for the technical description of objects the kopal UOF is
  used as base and single properties are defined by XPath expressions. An object
  characterisation through CQL \cite{CQL} for the DIAS data management is also
  possible.
  \item[\texttt{ObjectCharacterTranslator}] \ \
  Translates object characterisations into queries of a data management system. For
  XPath object characterisations this is possible through queries on METS files
  in the local file system, on the koLibRI database or the DIAS datamanagement.
  Only the DIAS datamanagement is capable of CQL object characterisations at
  the moment. It has to be kept in mind that not every data management does
  support all kinds of queries. Complete in the sense of the kopal UOF is the querying
  of local METS files.
  \item[\texttt{ObjectDependencyAnalyser}] \ \
  Analyses dependencies between objects and combines them. At the moment the
  \texttt{mptr} (METS pointer) of METS files are analysed and all objects, connected
  by METS pointers, are collected. The further processing of dependent object is not
  used at the moment.
  \item[\texttt{MigrationPolicyBuilder}] \ \
  Configures migration processes with the help of templates. Depending on the
  template and the source and destination object characterisation the
  appropriate migration service is chosen.
\end{description}

Besides the configuration possibilities in the common configuration file, two
more configuration files are important for the MigrationManager:

\begin{description}
  \item[\texttt{migrationPolicyTemplates.xml}] \ \
  Within this file (or in a file named according to the common configuration) the
  policy templates according to which the migrations are performed are defined . 
  \item[\texttt{migrationServiceRegistry.xml}] \ \
  The available migration services are listed here as well as defined how they can be used.
\end{description}

The most important action modules for the MigrationManager are:

\begin{description}
  \item[\texttt{PrepareMigration}] \ \
  Adapts the UOF for an migration. This module has to
  be called very early in the migration process or respectively before the file conversion.
  \item[\texttt{MigrateFiles}] \ \
  Executes external programmes for the migration and adapts the metadata into the
  UOF accordingly. Migrations are only possible through execution
  of command line programmes currently.
  \item[\texttt{Executor}] \ \
  Executes external programmes.
  \item[\texttt{AdaptHtmlPage}] \ \
  Adapts links and file references within a HTML page if the name of a file has
  changend through migration.
\end{description}


%----------------------------------------------------------------

\subsection{Usage}

%----------------------------------------------------------------

To perform a migration, several settings have to be configured and the migration
process has to be triggered through a migration event.


%----------------------------------------------------------------
\subsubsection{Configuration}
%----------------------------------------------------------------

\begin{enumerate}
  \item Which data mangement has to or can be used? The corresponding
  \texttt{Obj\-Cha\-rac\-ter\-Trans\-la\-tor} class has to be specified in the
  configuration file as parameter 
  \texttt{Obj\-Chr\-Trans\-la\-tor\-Class\-Name} for the class
  \texttt{pro\-cess\-star\-ter.mi\-gra\-tion\-ma\-na\-ger.Mi\-gra\-tion\-Event\-Sche\-du\-ler}. If
  direct access to DIAS is available
  \texttt{Dias\-CQL\-Obj\-Character\-Translator} can be used for a 
  CQL characterisation or \texttt{Dias\-Obj\-Cha\-rac\-ter\-Translator} can be
  used as XPath characterisation. Alternatively
  \texttt{File\-Obj\-Cha\-rac\-ter\-Trans\-la\-tor} is usable for locally
  available METS files or \texttt{Db\-Obj\-Cha\-rac\-ter\-Trans\-la\-tor}
  for the koLibRI database.
  \item For \texttt{FileObjCharacterTranslator} the \texttt{MetsFilesDir} in which 
  the directories or sub directories with METS files are stored has to be specified.
  \item For the class
  \texttt{pro\-cess\-star\-ter.mi\-gra\-tion\-ma\-na\-ger.Mi\-gra\-tion\-Po\-li\-cy\-Buil\-der}
  the \texttt{mi\-gra\-tion\-Ser\-vi\-ces\-File\-Name} and
  \texttt{migrationPolicyTemplatesFileName} have to be gi\-ven as paths to the
  configuration files which define available migration services and workflows.
  \item The usable migration services have to be defined in the configuration file 
  named \texttt{migrationServicesFileName}. Descriptions and
  examples of the configuration can be found in the file
   \texttt{config/example\_migrationServiceRegistry.xml}. Two aspects are essential:
   It has to be described through which action module and with which parameters a
   service can be called. It also has to be defined which properties the service
   has got, so that it can be chosen for a specific migration. This is possible by
   name, by classifications or by source and destination object characterisations using
   XPath expressions (e.g. source and destination file format).
  \item The templates for migration workflows have to be defined in the configuration
  file given as \texttt{mi\-gra\-tion\-Po\-li\-cy\-Tem\-plates\-File\-Name}. 
  Descriptions and examples of the configuration can be found in the file  
  \texttt{config/example\_mi\-gra\-tion\-Po\-li\-cy\-Tem\-plates.xml}.
  It is also essential here, that application criteria of a migration template
  are defined. The criteria are also expressed as XPath object
  characterisations. After that follows the definition of the policy parts 
  (\texttt{policyFragments}) to use. Fixed policies
  of the \texttt{policies.xml} (e.g. for the invariant parts of the ingest
  process) or criteria for choosing a migration service listed in the service
  registry are listed in order of execution.
\end{enumerate}


%----------------------------------------------------------------
\subsubsection{Executing the Migration}
%----------------------------------------------------------------

If all necessary configurations are done, the migration manager can be started
by invoking koLibRI with the class \texttt{MigrationEventScheduler}. Two parameters
are expected if only a source and destination format shall be used. This is done
by providing the according DIAS format IDs. If four parameters are provided, the
first pair is interpreted as XPATH expression and its expected return value for
the source object characterisation. The second pair respectively for the destination
object characterisation. An object characterisation can be done in longer form by

\begin{center}
	\texttt{/mets/amdSec/techMD/mdWrap/xmldata/format/text()}
\end{center}

(namespaces are left out in this example -- due to better readability -- but however have to be used) 
or in a more practicable form by

\begin{center}
	\texttt{//*[name()='lmerFile:format']}
\end{center}

As a result, GIF (for example) can be defined as

\begin{center}
	\texttt{urn:diasid:fty:kopal:0200507050000000000005}
\end{center}

All objects containing GIF files are selected by this.


%----------------------------------------------------------------

\subsection{Class Descriptions an Extensions}

%----------------------------------------------------------------

\begin{itemize}
	\item \texttt{MigrationEventScheduler}\\
	Implements the interface \texttt{ProcessStarter}, initialises all further
	components with help of the configuration and manages the main process.
	\item \texttt{MigrationEventListener}\\
	Interface for classes that receive events, that require a test for
	the need of a migration. 
	\item \texttt{CommandLineMigrationEventListener}\\
	Implements the interface \texttt{MigrationEventListener}. Interprets commandline
	arguments as events.
	\item \texttt{ObjCharacter}\\
	Object characterisation through XPATH expressions. Enables the usage of the
	\texttt{File\-Obj\-Cha\-rac\-ter\-Trans\-la\-tor},
	\texttt{DbObjCharacterTranslator} and the
	\texttt{Dias\-Obj\-Cha\-rac\-ter\-Trans\-la\-tor}.
	\item \texttt{CQLObjCharacter}\\
	Object characterisation through CQL expressions for the DIAS data management.
	Requeires the usage of the
	\texttt{Dias\-CQL\-Obj\-Cha\-rac\-ter\-Trans\-la\-tor}.  
	\item \texttt{ObjCharacterTranslator}\\
	Interface for the translation between object characterisations into queries of
	according data managements. 
	\item \texttt{FileObjCharacterTranslator}\\
	Queries as quasi-data-management single or multiple METS files. This also qualifies
	it for the mapping of XPATH queries into queries of other data management systems.
	This can be done by querying METS files with the \texttt{FileObjCharacterTranslator} that contain the
	fieldnames of the according data management instead of the regular values of an archive object.
	\item \texttt{DbObjCharacterTranslator}\\
	Queries the koLibRI database as data management, if the objects characterised through
	XPATH exspressions are present. For the mapping it uses the class
	\texttt{File\-Obj\-Cha\-rac\-ter\-Trans\-la\-tor} and the file
	\texttt{MapUofToDB.xml}.
	\item \texttt{DiasObjCharacterTranslator}\\
	Queries the DIAS data management, if the objects characterised through
	XPATH exspressions are present. For the mapping it uses the class
	\texttt{FileObjCharacterTranslator} and the file \texttt{MapUofToDIAS.xml}.  
	\item \texttt{DiasCQLObjCharacterTranslator}\\
	Queries the DIAS data management for objects characterised through CQLObjCharacter.
	\item \texttt{ObjectDependencyAnalysator}\\
	Analyses dependencies between objects and bundles them. Currently, only objects linked
	through METS pointers are analysed.
	\item \texttt{MigrationPolicyBuilder}\\
	Builds and configures migration policies with the help of the classes
  \texttt{Mi\-gra\-tion\-Po\-li\-cy\-Tem\-plate\-Store} and
  \texttt{MigrationServiceRegistry} for given objects and
  object characterisations.  
	\item \texttt{MigrationPolicyTemplateStore}\\
	Class that delivers the migration templates for queries on basis of the according
	configuration file.
	\item \texttt{MigrationServiceRegistry}\\
	Class that delivers descriptions and parameters for chosen migration services
	on basis of the according	configuration file.
\end{itemize}
