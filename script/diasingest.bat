@ECHO OFF
REM Copyright 2005-2007 by Project kopal
REM 
REM http://kopal.langzeitarchivierung.de/
REM 
REM This program is free software; you can redistribute it and/or modify it under
REM the terms of the GNU General Public License as published by the Free Software
REM Foundation; either version 2 of the License, or (at your option) any later
REM version.
REM 
REM This program is distributed in the hope that it will be useful, but WITHOUT
REM ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
REM FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
REM details.
REM
REM You should have received a copy of the GNU General Public License along with
REM this program; if not, write to the Free Software Foundation, Inc., 59 Temple
REM Place, Suite 330, Boston, MA 02111-1307, USA
REM
REM Usage: diasaccess [-hp,--show-properties] [-a,--address] [-f,--file]
REM            [-h,--help] [-n,--port] [-p,--password] [-t,--testdias]
REM            [-u,--user] 
REM
REM where -hp,--show-properties   Print the system properties and continue
REM	      -a,--address            The server address
REM	      -f,--file               The file to submit
REM	      -h,--help               Print this dialog and exit
REM	      -n,--port               The server port
REM	      -p,--password           The CMPassword of the CMUser
REM	      -t,--testdias           Print dias responses
REM	      -u,--user               The submitting CMUser
REM
REM Configuration constants:
REM KOLIBRI_HOME koLibRI installation directory
REM JAVA_HOME  Java JRE directory
REM JAVA       Java interpreter
REM EXTRA_JARS Extra jar files to add to CLASSPATH

SET KOLIBRI_HOME="C:\path_to_your_directory"

SET JAVA_HOME="C:\Programme\java\jre1.5.0_06"
SET JAVA=%JAVA_HOME%\bin\java

SET EXTRA_JARS=

REM NOTE: Nothing below this line should be edited
REM #########################################################################


SET CP=%KOLIBRI_HOME%\kolibri.jar
IF "%EXTRA_JARS%"=="" GOTO FI
  SET CP=%CP%;%EXTRA_JARS%
:FI

REM Retrieve a copy of all command line arguments to pass to the application

SET ARGS=
:WHILE
IF "%1"=="" GOTO LOOP
  SET ARGS=%ARGS% %1
  SHIFT
  GOTO WHILE
:LOOP

REM Set the CLASSPATH and invoke the Java loader
%JAVA% -classpath %CP% -Xmx128m de.langzeitarchivierung.kolibri.ingest.DiasIngest %ARGS%