@ECHO OFF
REM Copyright 2005-2007 by Project kopal
REM 
REM http://kopal.langzeitarchivierung.de/
REM 
REM This program is free software; you can redistribute it and/or modify it under
REM the terms of the GNU General Public License as published by the Free Software
REM Foundation; either version 2 of the License, or (at your option) any later
REM version.
REM 
REM This program is distributed in the hope that it will be useful, but WITHOUT
REM ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
REM FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
REM details.
REM
REM You should have received a copy of the GNU General Public License along with
REM this program; if not, write to the Free Software Foundation, Inc., 59 Temple
REM Place, Suite 330, Boston, MA 02111-1307, USA
REM
REM Usage: diasaccess [-d,--dip-format] [-r,--response-format] [-x,--ext-id]
REM            [-i,--int-id] [-hp,--show-properties] [-t,--request-type]
REM            [-a,--address] [-f,--file] [-f,--file] [-g,--download] [-h,--help]
REM            [-l,--list] [-n,--port] [-p,--print] [-u,--unsecure]
REM
REM where -d,--dip-format         The requested dip format [zip|tar|tar.gz]
REM	  -r,--response-format    The requested response format [xml|html]
REM	  -x,--ext-id             The requested external id
REM	  -i,--int-id             The requested internal id
REM	  -hp,--show-properties   Print the system properties and continue
REM	  -t,--request-type       The type of request [metadata|fullmetadata|asset]
REM	  -a,--address            The server address
REM	  -f,--file               Parse a file as a Dias response and prints it
REM	  -g,--download           If no file is specified the file in the dias 
REM                                 response is downloaded and saved in the current 
REM                                 directory. Else the specified file is downloaded.
REM	  -h,--help               Print this dialog and exit
REM	  -l,--list               Returns a list of all metadata sets for an
REM	                            external id
REM	  -n,--port               The server port
REM	  -p,--print              Prints the dias response
REM	  -u,--unsecure           Use unsecure access to dias
REM
REM Configuration constants:
REM KOLIBRI_HOME koLibRI installation directory
REM JAVA_HOME  Java JRE directory
REM JAVA       Java interpreter
REM EXTRA_JARS Extra jar files to add to CLASSPATH

SET KOLIBRI_HOME="C:\path_to_your_directory"

SET JAVA_HOME="C:\Programme\java\jre1.5.0_06"
SET JAVA=%JAVA_HOME%\bin\java

SET EXTRA_JARS=

REM NOTE: Nothing below this line should be edited
REM #########################################################################


SET CP=%KOLIBRI_HOME%\kolibri.jar
IF "%EXTRA_JARS%"=="" GOTO FI
  SET CP=%CP%;%EXTRA_JARS%
:FI

REM Retrieve a copy of all command line arguments to pass to the application

SET ARGS=
:WHILE
IF "%1"=="" GOTO LOOP
  SET ARGS=%ARGS% %1
  SHIFT
  GOTO WHILE
:LOOP

REM Set the CLASSPATH and invoke the Java loader
%JAVA% -classpath %CP% -Xmx128m de.langzeitarchivierung.kolibri.retrieval.DiasAccess %ARGS%