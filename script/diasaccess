#!/bin/sh

########################################################################
# Copyright 2005-2007 by Project kopal
#
# http://kopal.langzeitarchivierung.de/
# 
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA 02111-1307, USA
#
# Usage: diasaccess [-d,--dip-format] [-r,--response-format] [-x,--ext-id]
#            [-i,--int-id] [-hp,--show-properties] [-t,--request-type]
#            [-a,--address] [-f,--file] [-f,--file] [-g,--download] [-h,--help]
#            [-l,--list] [-n,--port] [-p,--print] [-u,--unsecure]
#
# where -d,--dip-format         The requested dip format [zip|tar|tar.gz]
#	    -r,--response-format    The requested response format [xml|html]
#	    -x,--ext-id             The requested external id
#	    -i,--int-id             The requested internal id
#	    -hp,--show-properties   Print the system properties and continue
#	    -t,--request-type       The type of request [metadata|fullmetadata|asset]
#	    -a,--address            The server address
#	    -f,--file               Parse a file as a Dias response and prints it
#	    -g,--download           If no file is specified the file in the dias 
#                                 response is downloaded and saved in the current 
#                                 directory. Else the specified file is downloaded.
#	    -h,--help               Print this dialog and exit
#	    -l,--list               Returns a list of all metadata sets for an
#	                            external id
#	    -n,--port               The server port
#	    -p,--print              Prints the dias response
#	    -u,--unsecure           Use unsecure access to dias
#
# Configuration constants:

KOLIBRI_HOME=/users/user/projects/kopal

JAVA_HOME=/usr/java      # Java JRE directory
JAVA=$JAVA_HOME/bin/java # Java interpreter

#XTRA_JARS=/users/user/Extensions-Example.jar
EXTRA_JARS=              # Extra .jar files to add to CLASSPATH

# NOTE: Nothing below this line should be edited
########################################################################

CP=${KOLIBRI_HOME}/kolibri.jar:${EXTRA_JARS}

# Retrieve a copy of all command line arguments to pass to the application.

ARGS=""
for ARG do
    ARGS="$ARGS $ARG"
done

# Set the CLASSPATH and invoke the Java loader.
${JAVA} -classpath $CP -Xmx128m de.langzeitarchivierung.kolibri.retrieval.DiasAccess $ARGS