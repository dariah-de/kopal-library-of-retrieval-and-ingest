@ECHO OFF
REM Copyright 2005-2007 by Project kopal
REM 
REM http://kopal.langzeitarchivierung.de/
REM 
REM This program is free software; you can redistribute it and/or modify it under
REM the terms of the GNU General Public License as published by the Free Software
REM Foundation; either version 2 of the License, or (at your option) any later
REM version.
REM 
REM This program is distributed in the hope that it will be useful, but WITHOUT
REM ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
REM FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
REM details.
REM
REM You should have received a copy of the GNU General Public License along with
REM this program; if not, write to the Free Software Foundation, Inc., 59 Temple
REM Place, Suite 330, Boston, MA 02111-1307, USA
REM
REM usage: WorkflowTool
REM     -c, --config-file        The config file to use.
REM     -s, --show-properties    Prints the system properties and continues.
REM     -p, --process-starter    The process starter module which chooses items for processing.
REM     -h, --help               Prints this dialog.
REM
REM Configuration constants:
REM KOLIBRI_HOME koLibRI installation directory
REM JAVA_HOME  Java JRE directory
REM JAVA       Java interpreter
REM EXTRA_JARS Extra jar files to add to CLASSPATH

SET KOLIBRI_HOME="C:\path_to_your_directory"
SET JAVA_HOME="C:\Programme\java\jre1.5.0_06"
SET JAVA=%JAVA_HOME%\bin\java
SET EXTRA_JARS=lib\clibwrapper_jiio.jar;lib\activation.jar;lib\asm.jar;lib\asm-attrs.jar;lib\axiom-attrs.jar;lib\axis2.jar;lib\cglib-2.1.3.jar;lib\commons-beanutils-1.6.1.jar;lib\commons-cli-1.0.jar;lib\commons-codec-1.3.jar;lib\commons-collections3-3.1.jar;lib\commons-digester-1.7.jar;lib\commons-httpclient-3.0-rc3.jar;lib\commons-logging-1.0.4.jar;lib\commons-net-1.4.0.jar;lib\config-beans.jar;lib\db-beans.jar;lib\dom4j-1.6.1.jar;lib\ehcache-1.1.jar;lib\hibernate3.jar;lib\jai_imageio.jar;lib\jdbc2_0-stdext.jar;lib\jhove-1.5.jar;lib\jhove-handler-1.5.jar;lib\jhove-module-1.5.jar;lib\jsch-0.1.23.jar;lib\jsr173_1.0_api.jar;lib\jta.jar;lib\mysql-connector-java-5.0.6-bin.jar;lib\kolibri_ws.jar;lib\saxon8.jar;lib\saxon8-dom.jar;lib\schemas.jar;lib\tar.jar;lib\tidy.jar;lib\xbean.jar;lib\xbean_xpath.jar;lib\oai-schemas.jar;

REM NOTE: Nothing below this line should be edited

REM #########################################################################

SET CP=%KOLIBRI_HOME%\kolibri.jar
IF "%EXTRA_JARS%"=="" GOTO FI
  SET CP=%CP%;%EXTRA_JARS%
:FI

REM Retrieve a copy of all command line arguments to pass to the application

SET ARGS=
:WHILE
IF "%1"=="" GOTO LOOP
  SET ARGS=%ARGS% %1
  SHIFT
  GOTO WHILE
:LOOP

REM Set the CLASSPATH and invoke the Java loader
%JAVA% -classpath %CP% -Xmx512m de.langzeitarchivierung.kolibri.WorkflowTool %ARGS%

REM #########################################################################
