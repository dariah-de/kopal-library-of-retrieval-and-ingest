/**
 * This software is copyright (c) 2020 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware.dhpublish.client;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Properties;
import org.apache.cxf.helpers.IOUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import info.textgrid.middleware.common.DariahStorageClient;
import info.textgrid.middleware.tgpublish.api.jaxb.ProcessStatusType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishStatus;
import jakarta.ws.rs.core.Response;

/**
 * <p>
 * Testing Online Status HTTP.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-12-14
 * @since 2020-12-11
 */

public class TestStatusOnline {

  // Use for (local) testing.
  private static final String PROPERTIES_FILE = "dhpublish.test.trep-de-dariah-eu.properties";

  // Used for testing, if PROPERTIES_FILE is not existing.
  private static final String DEFAULT_PROPERTIES_FILE = "dhpublish.test.properties";
  private static final String TTL_FILE = "geo-browser.ttl";

  private static PublishClient dhpublishClient;
  private static DariahStorageClient storageClient;

  private static String dhpublishEndpoint;
  private static String storageEndpoint;
  private static String oauthToken;
  private static boolean doTest;

  // **
  // PREPARATIONS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    System.out.println("System's default charset: " + Charset.defaultCharset().displayName());

    // Load properties file.
    Properties p = new Properties();
    try {
      p.load(new FileInputStream(getResource(PROPERTIES_FILE)));
    } catch (IOException e) {
      p.load(new FileInputStream(getResource(DEFAULT_PROPERTIES_FILE)));
    }

    System.out.println("Properties file: " + PROPERTIES_FILE);

    // Get properties.
    doTest = Boolean.parseBoolean(p.getProperty("DO_TEST"));

    // Test for testing :-D
    if (doTest) {
      dhpublishEndpoint = p.getProperty("DHPUBLISH_ENDPOINT");
      storageEndpoint = p.getProperty("STORAGE_EDPOINT");
      oauthToken = p.getProperty("OAUTH_TOKEN");

      // Get DH-publish REST endpoint and HTTP client.
      System.out.println("Getting DH-publish HTTP client...");

      dhpublishClient = new PublishClient(dhpublishEndpoint);

      // Get storage client.
      System.out.println("Getting DH-Storage HTTP client...");

      storageClient = new DariahStorageClient().setStorageUri(URI.create(storageEndpoint));
    }
  }

  /**
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {}

  /**
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {}

  /**
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {}

  // **
  // TESTS
  // **

  /**
   * @throws InterruptedException
   * @throws IOException
   * @throws FileNotFoundException
   */
  @Test
  public void testGetStatus() throws InterruptedException, FileNotFoundException, IOException {

    if (doTest) {
      String logID = "kolibriPublishClientTest#" + System.currentTimeMillis();

      System.out.println("Testing status with <" + dhpublishEndpoint + "> ["
          + logID + "]");

      // **
      // Import new collection.
      // **

      String ttl = IOUtils.readStringFromStream(new FileInputStream(getResource(TTL_FILE)));

      System.out.print("Creating new collection from file... ");

      String storageID = storageClient.createFile(new ByteArrayInputStream(ttl.getBytes()),
          "text/plain", oauthToken);

      System.out.println("StorageID: " + storageID + " ... OK");

      ttl = ttl.replaceAll("PLEASE_EXCHANGE_STORAGE_ID", storageID);

      System.out.print("Updating collection... ");

      storageClient.updateFile(storageID, new ByteArrayInputStream(ttl.getBytes()), "text/plain",
          oauthToken, logID);

      System.out.println("OK");

      // **
      // Publish new collection.
      // **

      System.out.print("Publishing collection... ");

      Response importResponse = dhpublishClient.publish(storageID, oauthToken, null, logID);

      int statusCode = importResponse.getStatusInfo().getStatusCode();
      String reasonPhrase = importResponse.getStatusInfo().getReasonPhrase();

      System.out.println(statusCode + " " + reasonPhrase);

      // **
      // Check status repeatedly.
      // **

      PublishResponse publishResponse = dhpublishClient.getStatus(storageID, oauthToken, logID);
      ProcessStatusType statusType = publishResponse.getPublishStatus().processStatus;
      while (statusType == null || !statusType.equals(ProcessStatusType.FAILED)
          && !statusType.equals(ProcessStatusType.FINISHED)) {
        statusType = publishResponse.getPublishStatus().processStatus;
        printStatus(publishResponse);
        Thread.sleep(100);
        publishResponse = dhpublishClient.getStatus(storageID, oauthToken, logID);
      }
    }
  }

  // **
  // PRIVATE
  // **

  /**
   * <p>
   * Loads a resource.
   * </p>
   * 
   * TODO Put together with the method in TGCrudServiceUtilities! Maybe create a first build maven
   * module for utility things.
   * 
   * @param {@link String} The resource to search for.
   * @return {@link File} The resource.
   * @throws IOException
   */
  private static File getResource(String resPart) throws IOException {

    File res;

    // If we have an absolute resPart, just return the file.
    if (resPart.startsWith(File.separator)) {
      return new File(resPart);
    }

    URL url = ClassLoader.getSystemClassLoader().getResource(resPart);
    if (url == null) {
      throw new IOException("Resource '" + resPart + "' not found");
    }
    try {
      res = new File(url.toURI());
    } catch (URISyntaxException ue) {
      res = new File(url.getPath());
    }

    return res;
  }

  /**
   * @param theResponse
   */
  private static void printStatus(PublishResponse theResponse) {

    boolean objectListComplete = theResponse.objectListComplete;
    PublishStatus status = theResponse.getPublishStatus();
    String activeModule = status.activeModule;
    String processStatus = (status.processStatus == null ? "null" : status.processStatus.name());
    int progress = status.progress;

    System.out.println("[" + System.currentTimeMillis() + "]  [status:" + processStatus
        + "]  [progress:" + progress + "%]  [objectListComplete:" + objectListComplete
        + "]  [module:" + activeModule + "]");
  }

}
