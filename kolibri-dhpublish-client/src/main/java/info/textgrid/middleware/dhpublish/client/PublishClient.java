/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * SUB Göttingen (http://www.sub.uni-goettingen.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt GNU)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.middleware.dhpublish.client;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.InfoResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import jakarta.ws.rs.core.Response;

/*******************************************************************************
 * <p>
 * DARIAH Publish Client Class.
 * </p>
 * 
 * @version 2019-05-15
 * @since 2017-11-20
 ******************************************************************************/

public class PublishClient {

  // **
  // CLASS
  // **

  private DHPublishService publish;

  /**
   * <p>
   * Constructor: Initialise new publish client.
   * </p>
   * 
   * @param endpoint HTTP address of the DH-publish endpoint.
   */
  public PublishClient(String endpoint) {
    this.publish = JAXRSClientFactory.create(endpoint + "/", DHPublishService.class);
  }

  /**
   * <p>
   * Constructor: Initialise new publish client.
   * </p>
   * 
   * @param endpoint HTTP address of the DH-publish endpoint.
   * @param timeout
   */
  public PublishClient(final String endpoint, final long timeout) {

    DHPublishService client = JAXRSClientFactory.create(endpoint + "/", DHPublishService.class);

    // Set timeout.
    HTTPConduit conduit = WebClient.getConfig(client).getHttpConduit();
    HTTPClientPolicy policy = new HTTPClientPolicy();
    policy.setReceiveTimeout(timeout);
    conduit.setClient(policy);

    this.publish = client;
  }

  /**
   * <p>
   * Publish a collection.
   * </p>
   * 
   * <p>
   * Status of publishing process can be monitored @see{getStatus}.
   * </p>
   * 
   * <p>
   * Publishing is stopped if errors or warnings found by server. Also look at @see
   * #publishIgnoreWarnings.
   * </p>
   * 
   * @param uri The DARIAH storage ID of the collection to be published.
   * @param storageToken
   * @param seafileToken
   * @param logID
   * @return HTTP response of the DH-publish call.
   */
  public Response publish(String uri, String storageToken, String seafileToken, String logID) {
    return this.publish.publish(uri, false, storageToken, seafileToken, logID);
  }

  /**
   * <p>
   * Only simulates a publish process (dry run)
   * </p>
   * 
   * @param uri The DARIAH storage ID of the collection to be published.
   * @param storageToken
   * @param seafileToken
   * @param logID
   * @return HTTP response of the DH-publish call.
   */
  public Response publishSimulate(String uri, String storageToken, String seafileToken,
      String logID) {
    return this.publish.publish(uri, true, storageToken, seafileToken, logID);
  }

  /**
   * <p>
   * Get publishing status for DARIAH storage ID.
   * </p>
   * 
   * @param uri
   * @param storageToken
   * @param logID
   * @return
   */
  public PublishResponse getStatus(String uri, String storageToken, String logID) {
    return this.publish.getStatus(uri, storageToken, logID);
  }

  /**
   * <p>
   * Get publishing ministatus for DARIAH storage ID.
   * </p>
   * 
   * @param uri The DARIAH storage ID to check status.
   * @param storageToken
   * @param logID
   * @return Response object containing information about processing state.
   */
  public PublishResponse getMinistatus(String uri, String storageToken, String logID) {
    return this.publish.getMinistatus(uri, storageToken, logID);
  }

  /**
   * <p>
   * Get an info status for the DARIAH Publish GUI.
   * </p>
   * 
   * @param uri
   * @param storageToken
   * @param logID
   * @return
   */
  public InfoResponse getInfo(String uri, String storageToken, String logID) {
    return this.publish.getInfo(uri, storageToken, logID);
  }

  /**
   * <p>
   * The DH-publish services' version.
   * </p>
   * 
   * @return
   */
  public String getVersion() {
    return this.publish.getVersion();
  }

}
