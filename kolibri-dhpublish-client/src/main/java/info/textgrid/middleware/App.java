/**
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * SUB Göttingen (http://www.sub.uni-goettingen.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.middleware;

import java.io.IOException;
import info.textgrid.middleware.dhpublish.client.PublishClient;
import info.textgrid.middleware.tgpublish.api.jaxb.InfoResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import jakarta.xml.bind.JAXB;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class App {

  // **
  // FINALS
  // **

  private static String eppn = "StefanFunk@dariah.eu";
  private static String uri = "208802";
  private static String storageToken = "";

  /**
   * @param args
   * @throws IOException
   * @throws InterruptedException
   */
  public static void main(String[] args) throws IOException, InterruptedException {

    System.out.println("creating service...");

    // Create new RTG-publish client.
    // PublishClient pc = new PublishClient("http://localhost:8080/kolibri-dhpublish-service", sid);
    PublishClient client = new PublishClient("http://repository.de.dariah.eu/dhpublish/");

    System.out.println("...ok");

    // DO GET INFO

    InfoResponse info = client.getInfo("urgl:argl:aua", storageToken, "fugifutestitest");

    System.out.println("status: " + info.status);
    System.out.println("uri:    " + info.uri);

    // DO PUBLISH

    System.out.println("uri:    " + uri);
    System.out.println("eppn:   " + eppn);
    System.out.println("token:  " + storageToken);

    // Call publish.
    client.publish(uri, storageToken, "", "");

    // Or use in a browser:
    // http://vm2/1.0/dhpublish/208802/publish?eppn=stefan.funk@textgrid.de

    /*
     * InputStream in = (InputStream) res.getEntity(); byte buffer[] = new byte[ 4000 ];
     * 
     * int len = in.read( buffer, 0, 4000 ); String str = new String( buffer, 0, len );
     * 
     * System.out.println( str );
     */

    System.out.println("========================================");
    for (int i = 1; i <= 10; i++) {
      Thread.sleep(1000);
      PublishResponse response = client.getStatus(uri, storageToken, "fugifutestitest");
      info = client.getInfo(uri, storageToken, "fugifutestitest");
      System.out.println("---- PublishResponse  ----");
      JAXB.marshal(response, System.out);
      System.out.println("---- InfoResponse  ----");
      JAXB.marshal(info, System.out);
      System.out.println();
    }
    System.out.println("========================================");

    // Show status and/or ministatus using the URLs:
    // http://vm2/1.0/dhpublish/208802/status
    // http://vm2/1.0/dhpublish/208802/ministatus
  }

}
