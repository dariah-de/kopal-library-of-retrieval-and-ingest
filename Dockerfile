###
# build
###
FROM maven:3.8.7-eclipse-temurin-17 as builder

ARG JOLOKIA_VERSION="1.3.7" # jolokia as of 1.5.0 requires auth for proxy operation

# download and explode jolokia
RUN curl -XGET "https://repo1.maven.org/maven2/org/jolokia/jolokia-war/${JOLOKIA_VERSION}/jolokia-war-${JOLOKIA_VERSION}.war" --output /jolokia.war
RUN mkdir /jolokia && cd /jolokia && jar -xf ../jolokia.war

COPY . /build
WORKDIR /build

# build and assemble app
RUN --mount=type=cache,target=/root/.m2 mvn clean verify package

###
# assemble image
###
FROM tomcat:10.1-jre17

ENV JAVA_OPTS="-Xmx768m"
ENV CATALINA_OPTS="-Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

COPY Dockerfile /
COPY ./script/docker-entrypoint /entrypoint

COPY --from=builder /jolokia /usr/local/tomcat/webapps/jolokia

# Bringt das hier was? <https://stackoverflow.com/questions/69210431/can-i-use-cxf-with-tomcat-10>
RUN mkdir /webapps
COPY --from=builder /build/kolibri-tgpublish-service/target/tgpublish /webapps/tgpublish
COPY --from=builder /build/kolibri-dhpublish-service/target/dhpublish /webapps/dhpublish

RUN groupadd --system --gid 29900 ULSB \
  && useradd --system --uid 49628 --gid 29900 storage \
  && chown -R storage:ULSB /usr/local/tomcat/webapps/ \
  && chown -R storage:ULSB /webapps/

USER storage
WORKDIR /usr/local/tomcat/

ENTRYPOINT ["/entrypoint"]
