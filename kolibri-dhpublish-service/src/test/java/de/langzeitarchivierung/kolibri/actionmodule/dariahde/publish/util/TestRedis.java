/*******************************************************************************
 * This software is copyright (c) 2017 by
 * 
 *  DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish.util;

import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import org.junit.Test;

import de.langzeitarchivierung.kolibri.publish.util.RedisPool;
import redis.clients.jedis.Jedis;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class TestRedis {

	private static final String	OK		= "...OK";
	private static final String	FAILED	= "...FAILED";

	/**
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	@Test
	public void testCreateRedisServer() throws XMLStreamException, IOException {

		System.out.print("Test creating Redis server... ");

		// Jedis jedis = RedisPool.getInstance().getJedis();

		// jedis.set("key", "value");

	}

}
