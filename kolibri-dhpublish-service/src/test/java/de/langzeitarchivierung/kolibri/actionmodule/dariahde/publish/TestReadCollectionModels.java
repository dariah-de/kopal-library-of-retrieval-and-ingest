/**
 * This software is copyright (c) 2021 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import static org.junit.Assert.assertFalse;
import java.io.IOException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/**
 * <p>
 * Tests reading collection models.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-03-15
 * @since 2017-05-04
 */

public class TestReadCollectionModels {

  // **
  // STATICS
  // **

  private static final String COLLECTION =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:EAEA0-17F8-7EBC-675B-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:37\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0134.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-9386-D2E1-226C-0\n"
          + "        a                dariah:Collection ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:rights        \"free\" ;\n"
          + "        dc:title         \"KOLLEKTION\" ;\n"
          + "        dcterms:hasPart  ( dariahstorage:EAEA0-17F8-7EBC-675B-0 dariahstorage:EAEA0-DB19-ED64-6480-0 dariahstorage:EAEA0-994F-A580-B42A-0 ) .\n"
          + "\n" + "dariahstorage:EAEA0-994F-A580-B42A-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:40\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0135.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-DB19-ED64-6480-0\n"
          + "        a       dariah:Collection .\n";
  private static final String COLLECTION_FAIL =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:EAEA0-17F8-7EBC-675B-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:37\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0134.jpg\" .\n" + "\n"
          + "        a                dariah:Collection ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:rights        \"free\" ;\n"
          + "        dc:title         \"KOLLEKTION\" ;\n"
          + "        dcterms:hasPart  ( dariahstorage:EAEA0-17F8-7EBC-675B-0 dariahstorage:EAEA0-DB19-ED64-6480-0 dariahstorage:EAEA0-994F-A580-B42A-0 ) .\n"
          + "\n" + "dariahstorage:EAEA0-994F-A580-B42A-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:40\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0135.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-DB19-ED64-6480-0\n"
          + "        a       dariah:Collection .\n";
  private static final String COLLECTION_FAIL_WITH_NEWLINE = "@prefix rdf:   \n" +
      "<http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n" +
      "@prefix dcam:  \n" +
      "  <http://purl.org/dc/dcam/> .\n" +
      "@prefix owl:   \n" +
      "  <http://www.w3.org/2002/07/owl#> .\n" +
      "@prefix seafile: \n" +
      "    <https://sftest.de.dariah.eu/files/> .\n" +
      "@prefix xsd:   \n" +
      "    <http://www.w3.org/2001/XMLSchema#> .\n" +
      "@prefix dcterms: \n" +
      "      <http://purl.org/dc/terms/> .\n" +
      "@prefix skos:  \n" +
      "      <http://www.w3.org/2004/02/skos/core#> .\n" +
      "@prefix rdfs:  \n" +
      "        <http://www.w3.org/2000/01/rdf-schema#> .\n" +
      "@prefix tgforms: \n" +
      "          <http://de.dariah.eu/rdf/tgforms/terms#> .\n" +
      "@prefix dariah: \n" +
      "            <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" +
      "@prefix dariahstorage: \n" +
      "            <https://de.dariah.eu/storage/> .\n" +
      "@prefix dc:    \n" +
      "            <http://purl.org/dc/elements/1.1/> .\n" +
      "\n" +
      "dariahstorage:EAEA0-D1DB-387A-E89B-0\n" +
      "        a                dariah:Collection ;\n" +
      "        dc:creator       \"fu\" ;\n" +
      "        dc:rights        \"free\" ;\n" +
      "        dc:title         \"bug #34880\" ;\n" +
      "        dcterms:hasPart  ( dariahstorage:EAEA0-A5CF-69EB-A761-0 ) .\n" +
      "\n" +
      "dariahstorage:EAEA0-A5CF-69EB-A761-0\n" +
      "        a           dariah:DataObject ;\n" +
      "        dc:creator  \"dm\" ;\n" +
      "        dc:format   \"image/gif\" ;\n" +
      "        dc:rights   \"testing only\" ;\n" +
      "        dc:title    \"DRAGON \n DRAGON\" .\n";
  private static final String ANOTHER_COLLECTION_FAIL_WITH_NEWLINE =
      "@prefix rdf:   \n" +
          "<http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n" +
          "@prefix dcam:  \n" +
          "  <http://purl.org/dc/dcam/> .\n" +
          "@prefix owl:   \n" +
          "  <http://www.w3.org/2002/07/owl#> .\n" +
          "@prefix seafile: \n" +
          "    <https://sftest.de.dariah.eu/files/> .\n" +
          "@prefix xsd:   \n" +
          "    <http://www.w3.org/2001/XMLSchema#> .\n" +
          "@prefix dcterms: \n" +
          "      <http://purl.org/dc/terms/> .\n" +
          "@prefix skos:  \n" +
          "      <http://www.w3.org/2004/02/skos/core#> .\n" +
          "@prefix rdfs:  \n" +
          "        <http://www.w3.org/2000/01/rdf-schema#> .\n" +
          "@prefix tgforms: \n" +
          "          <http://de.dariah.eu/rdf/tgforms/terms#> .\n" +
          "@prefix dariah: \n" +
          "            <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" +
          "@prefix dariahstorage: \n" +
          "            <https://de.dariah.eu/storage/> .\n" +
          "@prefix dc:    \n" +
          "            <http://purl.org/dc/elements/1.1/> .\n" +
          "\n" +
          "dariahstorage:EAEA0-D1DB-387A-E89B-0\n" +
          "        a                dariah:Collection ;\n" +
          "        dc:creator       \"fu\" ;\n" +
          "        dc:rights        \"free\" ;\n" +
          "        dc:title         \"bug #34880\" ;\n" +
          "        dcterms:hasPart  ( dariahstorage:EAEA0-A5CF-69EB-A761-0 ) .\n" +
          "\n" +
          "dariahstorage:EAEA0-A5CF-69EB-A761-0\n" +
          "        a           dariah:DataObject ;\n" +
          "          dc:description \"his data collection aims to include all global country rankings related to economic\n"
          +
          "systems, socio-economic development and business environments, including issues of\n" +
          "globalisation, sustainability and equal opportunity, which: • Assign scores in the form of\n"
          +
          "numbers, • Are based on an elaborated methodology which is documented, • Include\n" +
          "countries of the post-Soviet region, • Are published regularly covering a period of several\n"
          +
          "years since the end of the Soviet Union, i.e. since 1992. For all rankings which fulfil the\n"
          +
          "selection criteria the general or total scores for all countries and territories of the post-\n"
          +
          "Soviet region since 1992 (as available) have been included in this data collection. The\n"
          +
          "scores provided by the original source have been copied into this data collection without\n"
          +
          "any changes to the values. Later revisions of earlier data have been incorporated into this\n"
          +
          "dataset.\";\n" +
          "        dc:creator  \"dm\" ;\n" +
          "        dc:format   \"image/gif\" ;\n" +
          "        dc:rights   \"testing only\" ;\n" +
          "        dc:title    \"DRAGON DRAGON\" .\n";
  private static final String OBJECT_FAIL_WITH_NEWLINE =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n" +
          "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" +
          "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" +
          "\n" +
          "<http://hdl.handle.net/21.T11991/0000-001B-4120-5>\n" +
          "        a              dariah:DataObject ;\n" +
          "        dc:creator     \"fu\" ;\n" +
          "        dc:format      \"image/gif\" ;\n" +
          "        dc:identifier  <http://dx.doi.org/10.20375/0000-001B-4120-5> , <http://hdl.handle.net/21.T11991/0000-001B-4120-5> ;\n"
          +
          "        dc:relation    <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11991/0000-001B-411F-8> ;\n"
          +
          "        dc:rights      \"testing only!\" ;\n" +
          "        dc:title       \"DRAGON \n DRAGON\" .\n";
  private static final String SUBCOLLECTION =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:EAEA0-7AAA-2719-2D9C-0\n"
          + "        a       dariah:Collection .\n" + "\n"
          + "dariahstorage:EAEA0-1126-D9C9-7833-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:37\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0134.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-3E8A-97BC-F098-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:43\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0136.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-2985-1103-3CCC-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:55:22\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0143.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-9A4A-D66B-F1E9-0\n"
          + "        a          dariah:DataObject ;\n"
          + "        dc:date    \"2015-06-20T14:55:19\" ;\n"
          + "        dc:format  \"image/jpeg\" ;\n"
          + "        dc:rights  \"free\" ;\n"
          + "        dc:title   \"IMG_0142.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-C7AF-6F18-0F7E-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:40\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0135.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-0ED8-B305-675C-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:55\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0139.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-CCE8-CC65-2F8F-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:27\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0132.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-8AC8-413C-87C0-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:35\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0133.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-D89E-B781-28C4-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:53\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0138.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-DB19-ED64-6480-0\n"
          + "        a                dariah:Collection ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:rights        \"free\" ;\n"
          + "        dc:title         \"UNTERKOLLEKTION\" ;\n"
          + "        dcterms:hasPart  ( dariahstorage:EAEA0-0ED8-B305-675C-0 dariahstorage:EAEA0-CCE8-CC65-2F8F-0 dariahstorage:EAEA0-2985-1103-3CCC-0 dariahstorage:EAEA0-3E8A-97BC-F098-0 dariahstorage:EAEA0-7AAA-2719-2D9C-0 dariahstorage:EAEA0-1126-D9C9-7833-0 dariahstorage:EAEA0-C7AF-6F18-0F7E-0 dariahstorage:EAEA0-9A4A-D66B-F1E9-0 dariahstorage:EAEA0-8AC8-413C-87C0-0 dariahstorage:EAEA0-04B7-994C-7C80-0 dariahstorage:EAEA0-D89E-B781-28C4-0 ) .\n"
          + "\n" + "dariahstorage:EAEA0-04B7-994C-7C80-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:40\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0135.jpg\" .\n";
  private static final String SUBSUBCOLLECTION =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:EAEA0-6816-7A1E-6B82-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:40\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0135.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-05AA-52B2-06DB-0\n"
          + "        a           dariah:DataObject ;\n"
          + "        dc:creator  \"fu\" ;\n"
          + "        dc:date     \"2015-06-20T14:54:37\" ;\n"
          + "        dc:format   \"image/jpeg\" ;\n"
          + "        dc:rights   \"free\" ;\n"
          + "        dc:title    \"IMG_0134.jpg\" .\n" + "\n"
          + "dariahstorage:EAEA0-7AAA-2719-2D9C-0\n"
          + "        a                dariah:Collection ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:rights        \"free\" ;\n"
          + "        dc:title         \"UNTERUNTERKOLLEKTION\" ;\n"
          + "        dcterms:hasPart  ( dariahstorage:EAEA0-6816-7A1E-6B82-0 dariahstorage:EAEA0-05AA-52B2-06DB-0 ) .\n";
  private static final String OWN_STORAGE_ENDPOINT = "https://dariah-cdstar.gwdg.de/dariah/";
  private static final String OK = "...OK";
  private static final String FAILED = "...FAILED";

  // **
  // SET UPS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testReadCollectionModels() throws IOException, ParseException {

    // Create model.
    Model model = ModelFactory.createDefaultModel();

    System.out.println("Testing ReadCollectionModels... ");

    // Add collection.
    Model collection = RDFUtils.readModel(COLLECTION, RDFConstants.TURTLE);
    model.add(collection);

    System.out.println("\t...collection model added");

    // Add subcollection.
    Model subcollection = RDFUtils.readModel(SUBCOLLECTION, RDFConstants.TURTLE);
    model.add(subcollection);

    System.out.println("\t...subcollection model added");

    // Add subsubcollection.
    Model subsubcollection = RDFUtils.readModel(SUBSUBCOLLECTION, RDFConstants.TURTLE);
    model.add(subsubcollection);

    System.out.println("\t...subsubcollection model added");

    // Testing for correct models.
    if (!model.containsAll(collection)) {
      System.out.println(RDFUtils.getTTLFromModel(model));
      System.out.println(FAILED);
      assertFalse("Collection not in overall model!", true);
    }
    if (!model.containsAll(subcollection)) {
      System.out.println(RDFUtils.getTTLFromModel(model));
      System.out.println(FAILED);
      assertFalse("Subcollection not in overall model!", true);
    }
    if (!model.containsAll(subsubcollection)) {
      System.out.println(RDFUtils.getTTLFromModel(model));
      System.out.println(FAILED);
      assertFalse("Subsubcollection not in overall model!", true);
    }

    System.out.println("\tType of dariahstorage:EAEA0-9386-D2E1-226C-0 ends with collection?");
    boolean endsWithCollection =
        RDFUtils.findFirstObject(model, "https://de.dariah.eu/storage/EAEA0-9386-D2E1-226C-0",
            "rdf", "type").endsWith("Collection");
    if (!endsWithCollection) {
      assertFalse("Type doesn't end with 'Collection'!", true);
    }
    System.out.println("\t..." + endsWithCollection);
    System.out.println(OK);
  }

  /**
   * @throws ParseException
   */
  @Test(expected = ParseException.class)
  public void testReadCollectionModelsFAIL() throws ParseException {

    System.out.println("Testing ReadCollectionModelsFAIL... ");

    // Read collection.
    try {
      RDFUtils.readModel(COLLECTION_FAIL, RDFConstants.TURTLE);
    } catch (ParseException e) {
      System.out.println("\tExpected: [" + e.getClass().getName() + "]: " + e.getMessage());
      System.out.println(OK);
      throw e;
    }
  }

  /**
   * @throws ParseException
   */
  @Test(expected = ParseException.class)
  public void testReadCollectionModelsOBJECTFAILWITHNEWLINE() throws ParseException {

    System.out.println("Testing ReadCollectionModelsOBJECTFAILWITHNEWLINE... ");

    // Read model.
    try {
      RDFUtils.readModel(OBJECT_FAIL_WITH_NEWLINE, RDFConstants.TURTLE);
    } catch (ParseException e) {
      System.out.println("Expected: [" + e.getClass().getName() + "]: " + e.getMessage());
      System.out.println(OK);
      throw e;
    }
  }

  /**
   * @throws ParseException
   */
  @Test(expected = ParseException.class)
  public void testReadCollectionModelsCOLLECTIONFAILWITHNEWLINE() throws ParseException {

    System.out.println("Testing ReadCollectionModelsCOLLECTIONFAILWITHNEWLINE... ");

    // Read model.
    try {
      RDFUtils.readModel(COLLECTION_FAIL_WITH_NEWLINE, RDFConstants.TURTLE);
    } catch (ParseException e) {
      System.out.println("Expected: [" + e.getClass().getName() + "]: " + e.getMessage());
      System.out.println(OK);
      throw e;
    }
  }


  /**
   * @throws ParseException
   */
  @Test(expected = ParseException.class)
  public void testReadCollectionModelsANOTHERCOLLECTIONFAILWITHNEWLINE() throws ParseException {

    System.out.println("Testing ReadCollectionModelsANOTHERCOLLECTIONFAILWITHNEWLINE... ");

    // Read model.
    try {
      RDFUtils.readModel(ANOTHER_COLLECTION_FAIL_WITH_NEWLINE, RDFConstants.TURTLE);
    } catch (ParseException e) {
      System.out.println("Expected: [" + e.getClass().getName() + "]: " + e.getMessage());
      System.out.println(OK);
      throw e;
    }
  }

  /**
   * <p>
   * Test getting models from DARIAH-DE OwnStorage, please ignore id testing locally!
   * </p>
   * 
   * @throws ParseException
   * 
   * @throws Exception
   */
  @Ignore
  @Test
  public void testReadCollectionModelsOnline() throws ParseException {

    System.out.print("Testing ReadCollectionModelsOnline... ");

    // Create overall model to compare after reading.
    Model compare = RDFUtils.readModel(COLLECTION, RDFConstants.TURTLE);
    compare.add(RDFUtils.readModel(SUBCOLLECTION, RDFConstants.TURTLE));
    compare.add(RDFUtils.readModel(SUBSUBCOLLECTION, RDFConstants.TURTLE));

    // Create overall model.
    Model model = ModelFactory.createDefaultModel();

    // Settings.
    String rootCollectionUri = "https://de.dariah.eu/storage/EAEA0-9386-D2E1-226C-0";
    PublishResponse pr = new PublishResponse();
    String eppn = "StefanFunk@dariah.eu";
    PublishObject po = new PublishObject();
    String storageToken = "++TOK++";

    // Create overall collection.
    // ReadCollectionModels.readCollectionModels(model,
    // OWN_STORAGE_ENDPOINT,
    // pr, rootCollectionUri, eppn, po, storageToken, "");

    System.out.print("Collection and all subcollections added to overall model.");

    // Testing for correct models.
    if (!model.isIsomorphicWith(compare)) {
      System.out.println(FAILED);
      assertFalse("Models are not equal!!", true);
    }

    System.out.println(OK);
  }

}
