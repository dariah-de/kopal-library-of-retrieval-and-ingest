/*******************************************************************************
 * This software is copyright (c) 2017 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.junit.Test;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class TestRegisterDois {

  private static final String RDF_DATA =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://www.tgforms.de/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:300303  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"Dragon copy.gif\" .\n" + "\n"
          + "dariahstorage:300351  a  dariah:Collection ;\n"
          + "        dc:contributor   \"fu\" ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:description   \"urgl\" ;\n"
          + "        dc:title         \"huhihi\" ;\n"
          + "        dcterms:hasPart  dariahstorage:300304 , dariahstorage:300303 , dariahstorage:300302 , dariahstorage:300352 .\n"
          + "\n" + "dariahstorage:300304  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"Dragon.gif\" .\n" + "\n"
          + "dariahstorage:300352  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"AnimatedArmour.gif\" .\n" + "\n"
          + "dariahstorage:300302  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"BlackFlame.gif\" .\n";
  private static final String RDF_DATA_SUBCOLLECTIONS = "<rdf:RDF\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:owl=\"http://www.w3.org/2002/07/owl#\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\"\n"
      + "    xmlns:tgforms=\"http://de.dariah.eu/rdf/tgforms/terms#\"\n"
      + "    xmlns:dariahstorage=\"https://de.dariah.eu/storage/\"\n"
      + "    xmlns:dcam=\"http://purl.org/dc/dcam/\"\n"
      + "    xmlns:skos=\"http://www.w3.org/2004/02/skos/core#\"\n"
      + "    xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"\n"
      + "    xmlns:seafile=\"https://sftest.de.dariah.eu/files/\"\n"
      + "    xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\" > \n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-9A4A-D66B-F1E9-0\">\n"
      + "    <dc:title>IMG_0142.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:55:19</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-D89E-B781-28C4-0\">\n"
      + "    <dc:title>IMG_0138.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:53</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-05AA-52B2-06DB-0\">\n"
      + "    <dc:title>IMG_0134.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:37</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-3E8A-97BC-F098-0\">\n"
      + "    <dc:title>IMG_0136.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:43</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-0ED8-B305-675C-0\">\n"
      + "    <dc:title>IMG_0139.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:55</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-CCE8-CC65-2F8F-0\">\n"
      + "    <dc:title>IMG_0132.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:27</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-1126-D9C9-7833-0\">\n"
      + "    <dc:title>IMG_0134.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:37</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-7AAA-2719-2D9C-0\">\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-05AA-52B2-06DB-0\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-6816-7A1E-6B82-0\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-139C-E1B0-2B82-0\"/>\n"
      + "    <dc:title>UNTERUNTERKOLLEKTION</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-8BE7-83A6-01ED-0\">\n"
      + "    <dc:title>IMG_0134.jpg</dc:title>\n"
      + "    <dc:rights>ugauga</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:37</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-BD81-30F6-A848-0\">\n"
      + "    <dc:title>IMG_0136.jpg</dc:title>\n"
      + "    <dc:rights>ugl</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:43</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-2985-1103-3CCC-0\">\n"
      + "    <dc:title>IMG_0143.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:55:22</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-C170-16B3-7B7C-0\">\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-9F7E-0639-46BF-0\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-69D0-BA87-9F19-0\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-A2B2-19C2-5063-0\"/>\n"
      + "    <dc:title>SUBSUBSUBSUSBSUSBUBSUSBSUBS</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-8AC8-413C-87C0-0\">\n"
      + "    <dc:title>IMG_0133.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:35</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-139C-E1B0-2B82-0\">\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-8BE7-83A6-01ED-0\"/>\n"
      + "    <dc:title>UNTERUNTERUNTERKOLLEKTION</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-0E7D-B850-1669-0\">\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-C170-16B3-7B7C-0\"/>\n"
      + "    <dc:title>NEUNEUNEUNEUNENUENEUNEUNEUENEUEN</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-19AF-E233-D094-0\">\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-BD81-30F6-A848-0\"/>\n"
      + "    <dc:title>URGL ARGL UNTERUNTERKOLLEKTION</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-994F-A580-B42A-0\">\n"
      + "    <dc:title>IMG_0135.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:40</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-69D0-BA87-9F19-0\">\n"
      + "    <dc:title>IMG_0143.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:55:22</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-9386-D2E1-226C-0\">\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-994F-A580-B42A-0\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-DB19-ED64-6480-0\"/>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>\n"
      + "    <dc:title>KOLLEKTION</dc:title>\n"
      + "    <dcterms:format>text/tg.collection+tg.aggregation+xml</dcterms:format>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <dc:format>text/tg.collection+tg.aggregation+xml</dc:format>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-0E7D-B850-1669-0\"/>\n"
      + "    <dcterms:source>https://dariah-cdstar.gwdg.de/dariah/EAEA0-9386-D2E1-226C-0</dcterms:source>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-17F8-7EBC-675B-0\"/>\n"
      + "    <dc:rights>free</dc:rights>\n" + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-C7AF-6F18-0F7E-0\">\n"
      + "    <dc:title>IMG_0135.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:40</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-DB19-ED64-6480-0\">\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-0ED8-B305-675C-0\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-19AF-E233-D094-0\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-CCE8-CC65-2F8F-0\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-2985-1103-3CCC-0\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-3E8A-97BC-F098-0\"/>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-7AAA-2719-2D9C-0\"/>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:title>UNTERKOLLEKTION</dc:title>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-1126-D9C9-7833-0\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-C7AF-6F18-0F7E-0\"/>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-9A4A-D66B-F1E9-0\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-8AC8-413C-87C0-0\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-04B7-994C-7C80-0\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-D89E-B781-28C4-0\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-9F7E-0639-46BF-0\">\n"
      + "    <dc:title>IMG_0134.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:37</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-17F8-7EBC-675B-0\">\n"
      + "    <dc:title>IMG_0134.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:37</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-6816-7A1E-6B82-0\">\n"
      + "    <dc:title>IMG_0135.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:40</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-04B7-994C-7C80-0\">\n"
      + "    <dc:title>IMG_0135.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:40</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-A2B2-19C2-5063-0\">\n"
      + "    <dc:title>IMG_0133.jpg</dc:title>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:date>2015-06-20T14:54:35</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>";
  private static final String OK = "...OK";
  private static final String FAILED = "...FAILED";

  /**
   * @throws XMLStreamException
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testStoreDoisSingleCollection()
      throws XMLStreamException, IOException, ParseException {

    System.out.print("Storing DOIs to single RDF model... ");

    Model model = RDFUtils.readModel(RDF_DATA, RDFConstants.TURTLE);
    PublishResponse pResponse = createPublishResponse(model);

    int amount = 5;
    List<URI> namespaceDois = new ArrayList<URI>();
    for (int i = 0; i < amount; i++) {
      namespaceDois.add(URI.create("http://dx.doi.org/fu-" + (i + 1)));
    }
    List<URI> dois = new ArrayList<URI>();
    for (int i = 0; i < amount; i++) {
      dois.add(URI.create("doi:fu-" + (i + 1)));
    }

    System.out.println(dois.size() + " DOIs: " + dois);

    // Store DOI to RDF model and PublishResponse.
    int i = 0;
    for (PublishObject o : pResponse.getPublishObjects()) {
      o.pid = RegisterDois.addDoiToModel(model, o.uri, dois.get(i).toString());
      i++;
    }

    // Check model and Publish Response.
    checkModelAndPublishResponse(model, pResponse);
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   * @throws ParseException
   */
  @Test
  public void testStoreDOIsSubcollections() throws XMLStreamException, IOException, ParseException {

    System.out.print("Storing DOIs to subcollection RDF model and PublishResponse... ");

    Model model = RDFUtils.readModel(RDF_DATA_SUBCOLLECTIONS, RDFConstants.RDF_XML);
    PublishResponse pResponse = createPublishResponse(model);

    int amount = 26;
    List<URI> dois = new ArrayList<URI>();
    for (int i = 0; i < amount; i++) {
      dois.add(URI.create("doi:fu-" + (i + 1)));
    }

    System.out.println(dois.size() + " DOIs: " + dois);

    // Store DOI to RDF model and PublishResponse.
    int i = 0;
    for (PublishObject o : pResponse.getPublishObjects()) {
      o.pid = RegisterDois.addDoiToModel(model, o.uri, dois.get(i).toString());
      i++;
    }

    // Check model and Publish Response.
    checkModelAndPublishResponse(model, pResponse);
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param theModel
   * @param theResponse
   * @throws XMLStreamException
   * @throws IOException
   */
  private void checkModelAndPublishResponse(Model theModel, PublishResponse theResponse)
      throws XMLStreamException, IOException {

    // Loop Publish Objects.
    boolean error = false;
    for (PublishObject o : theResponse.getPublishObjects()) {

      String doi = LTPUtils.resolveDoiPid(o.pid);

      System.out.print("  " + o.uri + " = " + o.pid + " = " + doi);

      // Check model's URIs and PIDs.
      String erwartetePid = RDFUtils.findFirstObject(theModel, o.uri, RDFConstants.DC_PREFIX,
          RDFConstants.ELEM_DC_IDENTIFIER);

      if (doi.equals(erwartetePid)) {
        System.out.println(" = " + erwartetePid);
      } else {
        System.out.println(" ≠ " + erwartetePid);
        error = true;
      }
    }

    if (error) {
      System.out.println(FAILED);
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * @param theModel
   * @return
   */
  private PublishResponse createPublishResponse(Model theModel) {

    PublishResponse response = new PublishResponse();

    for (String s : DHPublishUtils.getAllAboutsFromModel(theModel)) {
      PublishObject newObject = new PublishObject(s);
      response.addPublishObject(newObject);
    }

    return response;
  }

}
