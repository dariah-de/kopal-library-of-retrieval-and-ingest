/**
 * This software is copyright (c) 2021 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish.util;

import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.junit.Test;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/**
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-03-12
 * @since 2014-03-12
 */
public class TestDHPublishUtils {

  private static final String RDF_DATA =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://www.tgforms.de/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <http://geobrowser.de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:300303  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"Dragon copy.gif\" .\n" + "\n"
          + "dariahstorage:300351  a  dariah:Collection ;\n"
          + "        dc:contributor   \"fu\" ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:description   \"urgl\" ;\n"
          + "        dc:title         \"huhihi\" ;\n"
          + "        dcterms:hasPart  ( dariahstorage:300304 dariahstorage:300303 dariahstorage:300302 dariahstorage:300352 ) .\n"
          + "\n" + "dariahstorage:300304  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"Dragon.gif\" .\n" + "\n"
          + "dariahstorage:300352  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"AnimatedArmour.gif\" .\n" + "\n"
          + "dariahstorage:300302  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"BlackFlame.gif\" .\n";
  private static final String RDF_DATA_NO_HASPARTS =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://www.tgforms.de/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <http://geobrowser.de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:365001  a  dariah:Collection ;\n"
          + "        dc:title  \"new\" .\n" + "";
  private static final String RDF_PID_DATA =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://www.tgforms.de/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <http://geobrowser.de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:300303  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"Dragon copy.gif\" ;\n"
          + "        dcterms:identifier  <http://hdl.handle.net/fu-4/urgl> .\n"
          + "\n" + "dariahstorage:300351  a  dariah:Collection ;\n"
          + "        dc:contributor   \"fu\" ;\n"
          + "        dc:creator       \"fu\" ;\n"
          + "        dc:description   \"urgl\" ;\n"
          + "        dc:title         \"huhihi\" ;\n"
          + "        dcterms:hasPart ( dariahstorage:300304 dariahstorage:300303 dariahstorage:300302 dariahstorage:300352 ) .\n"
          + "\n" + "dariahstorage:300304  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"Dragon.gif\" ;\n"
          + "        dcterms:identifier  <http://hdl.handle.net/fu-1/urgl> .\n"
          + "\n" + "dariahstorage:300352  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"AnimatedArmour.gif\" ;\n"
          + "        dcterms:identifier  <http://hdl.handle.net/fu-2/urgl> .\n"
          + "\n" + "dariahstorage:300302  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"BlackFlame.gif\" ;\n"
          + "        dcterms:identifier  <http://hdl.handle.net/fu-3/urgl> .\n";
  private static final String EXPECTED_METADATA = "@prefix hdl:   <http://hdl.handle.net/> .\n"
      + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
      + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
      + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
      + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
      + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
      + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
      + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
      + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
      + "@prefix tgforms: <http://www.tgforms.de/terms#> .\n"
      + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
      + "@prefix dariahstorage: <http://geobrowser.de.dariah.eu/storage/> .\n"
      + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
      + "<http://hdl.handle.net/urgl:argl:aua>\n"
      + "        a                   dariah:DataObject ;\n"
      + "        dc:format           \"image/gif\" ;\n"
      + "        dc:title            \"Dragon copy.gif\" ;\n"
      + "        dcterms:identifier  <http://hdl.handle.net/fu-4/urgl> .";
  private static final String TURTLE_PID_DATA = "@prefix dcam:    <http://purl.org/dc/dcam/> .\n"
      + "@prefix owl:     <http://www.w3.org/2002/07/owl#> .\n"
      + "@prefix seafile:  <https://sftest.de.dariah.eu/files/> .\n"
      + "@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .\n"
      + "@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .\n"
      + "@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .\n"
      + "@prefix dariah:  <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
      + "@prefix dariahstorage:  <https://de.dariah.eu/storage/> .\n"
      + "@prefix hdl:     <http://hdl.handle.net/> .\n"
      + "@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
      + "@prefix dcterms:  <http://purl.org/dc/terms/> .\n"
      + "@prefix tgforms:  <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
      + "@prefix dc:      <http://purl.org/dc/elements/1.1/> .\n"
      + "@prefix doi:     <http://dx.doi.org/> .\n" + "\n"
      + "dariahstorage:EAEA0-B19E-E6D8-D512-0\n"
      + "      a       dariah:DataObject ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:date \"2015-06-20T14:54:35\" ;\n"
      + "      dc:format \"image/jpeg\" ;\n"
      + "      dc:identifier <http://dx.doi.org/10.3249/0000-0001-34A0-F> , <http://hdl.handle.net/21.T11998/0000-0001-34A0-F> ;\n"
      + "      dc:rights \"free\" ;\n"
      + "      dc:title \"IMG_0133.jpg\" ;\n"
      + "      dcterms:identifier <http://dx.doi.org/10.3249/0000-0001-34A0-F> , <http://hdl.handle.net/21.T11998/0000-0001-34A0-F> .\n"
      + "\n" + "" + "dariahstorage:EAEA0-59F3-2837-4561-0\n"
      + "      a       dariah:Collection ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:identifier <http://hdl.handle.net/21.T11998/0000-0001-349E-3> , <http://dx.doi.org/10.3249/0000-0001-349E-3> ;\n"
      + "      dc:rights \"free\" ;\n" + "      dc:title \"Der Fugu\" ;\n"
      + "      dcterms:hasPart  ( dariahstorage:EAEA0-B19E-E6D8-D512-0 dariahstorage:EAEA0-C623-258A-EE0F-0 dariahstorage:EAEA0-F0DE-8CF7-10B6-0 ) ;\n"
      + "      dcterms:identifier <http://hdl.handle.net/21.T11998/0000-0001-349E-3> , <http://dx.doi.org/10.3249/0000-0001-349E-3> .\n"
      + "\n" + "dariahstorage:EAEA0-C623-258A-EE0F-0\n"
      + "      a       dariah:DataObject ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:date \"2015-06-20T14:54:27\" ;\n"
      + "      dc:format \"image/jpeg\" ;\n"
      + "      dc:identifier <http://hdl.handle.net/21.T11998/0000-0001-349F-2> , <http://dx.doi.org/10.3249/0000-0001-349F-2> ;\n"
      + "      dc:rights \"free\" ;\n"
      + "      dc:title \"IMG_0132.jpg\" ;\n"
      + "      dcterms:identifier <http://hdl.handle.net/T11998/0000-0001-349F-2> , <http://dx.doi.org/10.3249/0000-0001-349F-2> .\n"
      + "\n" + "dariahstorage:EAEA0-BA55-9615-67CA-0\n"
      + "      a       dariah:DataObject ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:date \"2015-06-20T14:54:40\" ;\n"
      + "      dc:format \"image/jpeg\" ;\n"
      + "      dc:identifier <http://dx.doi.org/10.3249/0000-0001-34A3-C> , <http://hdl.handle.net/21.T11998/0000-0001-34A3-C> ;\n"
      + "      dc:rights \"free\" ;\n"
      + "      dc:title \"IMG_0135.jpg\" ;\n"
      + "      dcterms:identifier <http://dx.doi.org/10.3249/0000-0001-34A3-C> , <http://hdl.handle.net/21.T11998/0000-0001-34A3-C> .\n"
      + "\n" + "dariahstorage:EAEA0-F0DE-8CF7-10B6-0\n"
      + "      a       dariah:Collection ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:identifier <http://dx.doi.org/10.3249/0000-0001-34A1-E> , <http://hdl.handle.net/21.T11998/0000-0001-34A1-E> ;\n"
      + "      dc:rights \"free\" ;\n" + "      dc:title \"FuguSUB\" ;\n"
      + "      dcterms:hasPart  ( dariahstorage:EAEA0-BA55-9615-67CA-0 dariahstorage:EAEA0-3A1F-DEFC-8CFF-0 ) ;\n"
      + "      dcterms:identifier <http://dx.doi.org/10.3249/0000-0001-34A1-E> , <http://hdl.handle.net/21.T11998/0000-0001-34A1-E> .\n"
      + "\n" + "dariahstorage:EAEA0-3A1F-DEFC-8CFF-0\n"
      + "      a       dariah:DataObject ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:date \"2015-06-20T14:54:43\" ;\n"
      + "      dc:format \"image/jpeg\" ;\n"
      + "      dc:identifier <http://dx.doi.org/10.3249/0000-0001-34A2-D> , <http://hdl.handle.net/21.T11998/0000-0001-34A2-D> ;\n"
      + "      dc:rights \"free\" ;\n"
      + "      dc:title \"IMG_0136.jpg\" ;\n"
      + "      dcterms:identifier <http://dx.doi.org/10.3249/0000-0001-34A2-D> , <http://hdl.handle.net/21.T11998/0000-0001-34A2-D> .";
  private static final String COLLECTION_METADATA =
      "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "@prefix hdl:   <http://hdl.handle.net/> .\n"
          + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix doi:   <http://dx.doi.org/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11998/0000-0001-349E-3>\n"
          + "        a                   dariah:Collection ;\n"
          + "        dc:creator          \"fu\" ;\n"
          + "        dc:format           \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dc:identifier       <http://hdl.handle.net/21.T11998/0000-0001-349E-3> , <http://dx.doi.org/10.3249/0000-0001-349E-3> ;\n"
          + "        dc:rights           \"free\" ;\n"
          + "        dc:title            \"Der Fugu\" ;\n"
          + "        dcterms:format      \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dcterms:hasPart     ( dariahstorage:EAEA0-B19E-E6D8-D512-0 dariahstorage:EAEA0-C623-258A-EE0F-0 dariahstorage:EAEA0-F0DE-8CF7-10B6-0 ) ;\n"
          + "        dcterms:identifier  <http://hdl.handle.net/21.T11998/0000-0001-349E-3> , <http://dx.doi.org/10.3249/0000-0001-349E-3> ;\n"
          + "        dcterms:source      \"https://de.dariah.eu/storage/EAEA0-59F3-2837-4561-0\" .";
  private static final String IS_COLLECTION_TURTLE =
      "@prefix dcam:    <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:     <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile:  <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix dariah:  <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage:  <https://de.dariah.eu/storage/> .\n"
          + "@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcterms:  <http://purl.org/dc/terms/> .\n"
          + "@prefix tgforms:  <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
          + "@prefix dc:      <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "\n" + "dariahstorage:EAEA0-69F7-5215-763B-0\n"
          + "      a       dariah:DataObject ;\n"
          + "      dc:creator \"fu\" ;\n"
          + "      dc:date \"2015-06-20T14:54:40\" ;\n"
          + "      dc:format \"image/jpeg\" ;\n"
          + "      dc:relation \"dariah:collection:11022/0000-0006-E116-3\" ;\n"
          + "      dc:rights \"free\" ;\n"
          + "      dc:title \"IMG_0135.jpg\" ;\n"
          + "      dcterms:identifier <http://hdl.handle.net/21.T11998/0000-0006-E117-2> .\n"
          + "dariahstorage:EAEA0-866C-30FD-26DE-0\n"
          + "      a       dariah:DataObject ;\n"
          + "      dc:creator \"fu\" ;\n"
          + "      dc:date \"2015-06-20T14:54:37\" ;\n"
          + "      dc:format \"image/jpeg\" ;\n"
          + "      dc:relation \"dariah:Collection:21.T11998/0000-0006-E116-3\" ;\n"
          + "      dc:rights \"free\" ;\n"
          + "      dc:title \"IMG_0134.jpg\" ;\n"
          + "      dcterms:identifier <http://hdl.handle.net/21.T11998/0000-0006-E118-1> .\n"
          + "\n" + "dariahstorage:EAEA0-3994-688D-93C0-0\n"
          + "      a       dariah:DataObject ;\n"
          + "      dc:creator \"fu\" ;\n"
          + "      dc:date \"2015-06-20T14:55:22\" ;\n"
          + "      dc:format \"image/jpeg\" ;\n"
          + "      dc:relation \"dariah:Collection:21.T11998/0000-0006-E116-3\" ;\n"
          + "      dc:rights \"free\" ;\n"
          + "      dc:title \"IMG_0143.jpg\" ;\n"
          + "      dcterms:identifier <http://hdl.handle.net/21.T11998/0000-0006-E11A-F> .\n"
          + "\n" + "dariahstorage:EAEA0-0606-122A-DE40-0\n"
          + "      a       dariah:Collection ;\n"
          + "      dc:creator \"fu\" ;\n"
          + "      dc:format \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "      dc:identifier <http://hdl.handle.net/21.T11998/0000-0006-E119-0> ;\n"
          + "      dc:relation \"dariah:Collection:21.T11998/0000-0006-E116-3\" ;\n"
          + "      dc:rights \"free\" ;\n" + "      dc:title \"SPINAT\" ;\n"
          + "      dcterms:format \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "      dcterms:hasPart dariahstorage:EAEA0-3994-688D-93C0-0 , dariahstorage:EAEA0-5946-1945-847D-0 ;\n"
          + "      dcterms:source \"https://dariah-cdstar.gwdg.de/dariah/EAEA0-0606-122A-DE40-0\" .\n"
          + "\n" + "dariahstorage:EAEA0-5DA9-2915-DCE4-0\n"
          + "      a       dariah:Collection ;\n"
          + "      dc:creator \"fu\" ;\n"
          + "      dc:format \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "      dc:identifier <http://hdl.handle.net/21.T11998/0000-0006-E116-3> ;\n"
          + "      dc:rights \"free\" ;\n" + "      dc:title \"PORRIDGE\" ;\n"
          + "      dcterms:format \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "      dcterms:hasPart dariahstorage:EAEA0-69F7-5215-763B-0 , dariahstorage:EAEA0-866C-30FD-26DE-0 , dariahstorage:EAEA0-0606-122A-DE40-0 ;\n"
          + "      dcterms:source \"https://dariah-cdstar.gwdg.de/dariah/EAEA0-5DA9-2915-DCE4-0\" .\n"
          + "\n" + "dariahstorage:EAEA0-5946-1945-847D-0\n"
          + "      a       dariah:DataObject ;\n"
          + "      dc:creator \"fu\" ;\n"
          + "      dc:date \"2015-06-20T14:55:19\" ;\n"
          + "      dc:format \"image/jpeg\" ;\n"
          + "      dc:relation \"dariah:Collection:21.T11998/0000-0006-E116-3\" ;\n"
          + "      dc:rights \"free\" ;\n"
          + "      dc:title \"IMG_0142.jpg\" ;\n"
          + "      dcterms:identifier <http://hdl.handle.net/21.T11998/0000-0006-E11B-E> .";
  private static final String IS_COLLECTION = "<rdf:RDF\n"
      + "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\"\n"
      + "    xmlns:hdl=\"http://hdl.handle.net/\" > \n"
      + "  <rdf:Description rdf:about=\"http://hdl.handle.net/11022/0000-0006-E116-3\">\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-69F7-5215-763B-0\"/>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>\n"
      + "    <dc:format>text/vnd.dariah.dhrep.collection+turtle</dc:format>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <dcterms:identifier>http://hdl.handle.net/11022/0000-0006-E116-3</dcterms:identifier>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-866C-30FD-26DE-0\"/>\n"
      + "    <dc:contributor>stefanfunk@dariah.eu</dc:contributor>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-0606-122A-DE40-0\"/>\n"
      + "    <dcterms:source>https://dariah-cdstar.gwdg.de/dariah/EAEA0-5DA9-2915-DCE4-0</dcterms:source>\n"
      + "    <dc:identifier>http://hdl.handle.net/11022/0000-0006-E116-3</dc:identifier>\n"
      + "    <dc:title>PORRIDGE</dc:title>\n"
      + "    <dcterms:format>text/vnd.dariah.dhrep.collection+turtle</dcterms:format>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>\n";
  private static final String IS_COLLECTION_NEW_PIDS = "<rdf:RDF\n"
      + "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\"\n"
      + "    xmlns:hdl=\"http://hdl.handle.net/\" > \n"
      + "  <rdf:Description rdf:about=\"http://hdl.handle.net/21.T11998/0000-0006-E116-3\">\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-69F7-5215-763B-0\"/>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>\n"
      + "    <dc:format>text/vnd.dariah.dhrep.collection+turtle</dc:format>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <dcterms:identifier>http://hdl.handle.net/21.T11998/0000-0006-E116-3</dcterms:identifier>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-866C-30FD-26DE-0\"/>\n"
      + "    <dc:contributor>stefanfunk@dariah.eu</dc:contributor>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-0606-122A-DE40-0\"/>\n"
      + "    <dcterms:source>https://dariah-cdstar.gwdg.de/dariah/EAEA0-5DA9-2915-DCE4-0</dcterms:source>\n"
      + "    <dc:identifier>http://hdl.handle.net/21.T11998/0000-0006-E116-3</dc:identifier>\n"
      + "    <dc:title>PORRIDGE</dc:title>\n"
      + "    <dcterms:format>text/vnd.dariah.dhrep.collection+turtle</dcterms:format>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>\n";
  private static final String IS_NO_COLLECTION = "<rdf:RDF\n"
      + "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\"\n"
      + "    xmlns:hdl=\"http://hdl.handle.net/\" > \n"
      + "  <rdf:Description rdf:about=\"http://hdl.handle.net/11022/0000-0006-E117-2\">\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <dc:date>2015-06-20T14:54:40</dc:date>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:title>IMG_0135.jpg</dc:title>\n"
      + "    <dcterms:identifier>http://hdl.handle.net/11022/0000-0006-E117-2</dcterms:identifier>\n"
      + "    <dc:relation>dariah:collection:11022/0000-0006-E116-3</dc:relation>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>";
  private static final String IS_NO_COLLECTION_NEW_PIDS = "<rdf:RDF\n"
      + "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\"\n"
      + "    xmlns:hdl=\"http://hdl.handle.net/\" > \n"
      + "  <rdf:Description rdf:about=\"http://hdl.handle.net/21.T11998/0000-0006-E117-2\">\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <dc:date>2015-06-20T14:54:40</dc:date>\n"
      + "    <dc:format>image/jpeg</dc:format>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:title>IMG_0135.jpg</dc:title>\n"
      + "    <dcterms:identifier>http://hdl.handle.net/21.T11998/0000-0006-E117-2</dcterms:identifier>\n"
      + "    <dc:relation>dariah:Collection:21.T11998/0000-0006-E116-3</dc:relation>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>";
  private static String TURTLE_METADATA = "@prefix dcam:    <http://purl.org/dc/dcam/> .\n"
      + "@prefix owl:     <http://www.w3.org/2002/07/owl#> .\n"
      + "@prefix seafile:  <https://sftest.de.dariah.eu/files/> .\n"
      + "@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .\n"
      + "@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .\n"
      + "@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .\n"
      + "@prefix dariah:  <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
      + "@prefix dariahstorage:  <https://de.dariah.eu/storage/> .\n"
      + "@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
      + "@prefix dcterms:  <http://purl.org/dc/terms/> .\n"
      + "@prefix tgforms:  <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
      + "@prefix dc:      <http://purl.org/dc/elements/1.1/> .\n" + "\n"
      + "@prefix hdl:      <http://hdl.handle.net/> .\n" + "\n"
      + "dariahstorage:EAEA0-85EB-1ED4-0C1E-0\n"
      + "      a       dariah:DataObject ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:date \"2015-06-20T14:54:37\" ;\n"
      + "      dc:format \"image/jpeg\" ;\n"
      + "      dc:relation \"dariah:Collection:21.T11998/0000-0007-0804-C\" ;\n"
      + "      dc:rights \"free\" ;\n"
      + "      dc:title \"IMG_0134.jpg\" ;\n"
      + "      dcterms:identifier <http://hdl.handle.net/21.T11998/0000-0007-0805-B> .\n"
      + "\n" + "dariahstorage:EAEA0-E67C-E8B2-508F-0\n"
      + "      a       dariah:Collection ;\n"
      + "      dc:contributor \"Mitwirkende\" ;\n"
      + "      dc:contributor \"stefanfunk@dariah.eu\" ;\n"
      + "      dc:coverage \"Coverage: TOTAL\" ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:date \"2017\" , \"2017-07\" ;\n"
      + "      dc:description \"Hier eine Beschreibung\" ;\n"
      + "      dc:format \"text/vnd.dariah.dhrep.collection+turtle\" , \"Ein Format...... extra sozusagen\" ;\n"
      + "      dc:language \"DE\" ;\n"
      + "      dc:publisher \"FU's Verlag\" ;\n"
      + "      dc:relation \"Steht in Beziehung mit einer anderen Kollektion\" ;\n"
      + "      dc:rights \"free\" ;\n"
      + "      dc:source \"Kommt irgendwo her\" , \"Und anderswo her ebenfalls\" ;\n"
      + "      dc:subject \"Dies ist ein Test!\" ;\n"
      + "      dc:title \"Tobis Testkollektion\" ;\n"
      + "      dc:type \"Ein Typ, ein netter :-)\" ;\n"
      + "      dcterms:format \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
      + "      dcterms:hasPart dariahstorage:EAEA0-85EB-1ED4-0C1E-0 ;\n"
      + "      dcterms:identifier <http://hdl.handle.net/21.T11998/0000-0007-0804-C> ;\n"
      + "      dcterms:identifier <http://dx.doi.org/10.3249/0000-0007-0804-C> ;\n"
      + "      dc:identifier <http://hdl.handle.net/21.T11998/0000-0007-0804-C> ;\n"
      + "      dc:identifier <http://dx.doi.org/10.3249/0000-0007-0804-C> ;\n"
      + "      dc:identifier \"ein ID, Yeah!\" ;\n"
      + "      dcterms:source \"https://dariah-cdstar.gwdg.de/dariah/EAEA0-E67C-E8B2-508F-0\" .";
  private static String EXPECTED_CR_METADATA = "<rdf:RDF\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"
      + "  <rdf:Description rdf:about=\"http://dx.doi.org/10.3249/0000-0007-0804-C\">\n"
      + "    <dc:source>Kommt irgendwo her</dc:source>\n"
      + "    <dc:coverage>Coverage: TOTAL</dc:coverage>\n"
      + "    <dc:contributor>stefanfunk@dariah.eu</dc:contributor>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:title>Tobis Testkollektion</dc:title>\n"
      + "    <dc:publisher>FU's Verlag</dc:publisher>\n"
      + "    <dc:type>Ein Typ, ein netter :-)</dc:type>\n"
      + "    <dc:identifier>doi:10.3249/0000-0007-0804-C</dc:identifier>\n"
      + "    <dc:language>DE</dc:language>\n"
      + "    <dc:subject>Dies ist ein Test!</dc:subject>\n"
      + "    <dc:relation>Steht in Beziehung mit einer anderen Kollektion</dc:relation>\n"
      + "    <dc:source>Und anderswo her ebenfalls</dc:source>\n"
      + "    <dc:format>text/vnd.dariah.dhrep.collection+turtle</dc:format>\n"
      + "    <dc:contributor>Mitwirkende</dc:contributor>\n"
      + "    <dc:format>Ein Format...... extra sozusagen</dc:format>\n"
      + "    <dc:identifier>ein ID, Yeah!</dc:identifier>\n"
      + "    <dc:date>2017</dc:date>\n"
      + "    <dc:identifier>hdl:21.T11998/0000-0007-0804-C</dc:identifier>\n"
      + "    <dc:description>Hier eine Beschreibung</dc:description>\n"
      + "    <dc:identifier>https://repository.de.dariah.eu/oaipmh/oai/21.T11998%2F0000-0007-0804-C</dc:identifier>\n"
      + "    <dc:date>2017-07</dc:date>\n"
      + "    <dc:creator>fu</dc:creator>\n" + "  </rdf:Description>\n"
      + "</rdf:RDF>";
  private static String EXPECTED_DC_METADATA_COLLECTION =
      "@prefix dc:      <http://purl.org/dc/elements/1.1/> .\n"
          + "@prefix dariah:  <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "dariahstorage:EAEA0-E67C-E8B2-508F-0\n"
          + "      a       dariah:Collection ;\n"
          + "      dc:contributor \"stefanfunk@dariah.eu\" , \"Mitwirkende\" ;\n"
          + "      dc:coverage \"Coverage: TOTAL\" ;\n"
          + "      dc:creator \"fu\" ;\n"
          + "      dc:date \"2017\" , \"2017-07\" ;\n"
          + "      dc:description \"Hier eine Beschreibung\" ;\n"
          + "      dc:format \"text/vnd.dariah.dhrep.collection+turtle\" , \"Ein Format...... extra sozusagen\" ;\n"
          + "      dc:identifier <http://hdl.handle.net/21.T11998/0000-0007-0804-C> , <http://dx.doi.org/10.3249/0000-0007-0804-C> , \"ein ID, Yeah!\" ;\n"
          + "      dc:language \"DE\" ;\n"
          + "      dc:publisher \"FU's Verlag\" ;\n"
          + "      dc:relation \"Steht in Beziehung mit einer anderen Kollektion\" ;\n"
          + "      dc:rights \"free\" ;\n"
          + "      dc:source \"Kommt irgendwo her\" , \"Und anderswo her ebenfalls\" ;\n"
          + "      dc:subject \"Dies ist ein Test!\" ;\n"
          + "      dc:title \"Tobis Testkollektion\" ;\n"
          + "      dc:type \"Ein Typ, ein netter :-)\" .";
  private static String EXPECTED_DC_METADATA_DATAOBJECT =
      "@prefix dc:      <http://purl.org/dc/elements/1.1/> .\n"
          + "@prefix dariah:  <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
          + "dariahstorage:EAEA0-85EB-1ED4-0C1E-0\n"
          + "      a       dariah:DataObject ;\n"
          + "      dc:creator \"fu\" ;\n"
          + "      dc:date \"2015-06-20T14:54:37\" ;\n"
          + "      dc:format \"image/jpeg\" ;\n"
          + "      dc:relation \"dariah:Collection:21.T11998/0000-0007-0804-C\" ;\n"
          + "      dc:rights \"free\" ;\n"
          + "      dc:title \"IMG_0134.jpg\" .";
  private static final String URGL_NEW_PIDS = "@prefix dcam:    <http://purl.org/dc/dcam/> .\n"
      + "@prefix owl:     <http://www.w3.org/2002/07/owl#> .\n"
      + "@prefix seafile:  <https://sftest.de.dariah.eu/files/> .\n"
      + "@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .\n"
      + "@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .\n"
      + "@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .\n"
      + "@prefix dariah:  <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
      + "@prefix dariahstorage:  <https://de.dariah.eu/storage/> .\n"
      + "@prefix hdl:     <http://hdl.handle.net/> .\n"
      + "@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
      + "@prefix dcterms:  <http://purl.org/dc/terms/> .\n"
      + "@prefix tgforms:  <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
      + "@prefix dc:      <http://purl.org/dc/elements/1.1/> .\n"
      + "@prefix doi:     <http://dx.doi.org/> .\n" + "\n"
      + "dariahstorage:EAEA0-B19E-E6D8-D512-0\n"
      + "      a       dariah:DataObject ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:date \"2015-06-20T14:54:35\" ;\n"
      + "      dc:format \"image/jpeg\" ;\n"
      + "      dc:identifier <http://dx.doi.org/10.3249/0000-0001-34A0-F> , <http://hdl.handle.net/21.T11998/0000-0001-34A0-F> ;\n"
      + "      dc:rights \"free\" ;\n"
      + "      dc:title \"IMG_0133.jpg\" ;\n"
      + "      dcterms:identifier <http://dx.doi.org/10.3249/0000-0001-34A0-F> , <http://hdl.handle.net/21.T11998/0000-0001-34A0-F> .\n"
      + "\n" + "dariahstorage:EAEA0-59F3-2837-4561-0\n"
      + "      a       dariah:Collection ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:identifier <http://hdl.handle.net/21.T11998/0000-0001-349E-3> , <http://dx.doi.org/10.3249/0000-0001-349E-3> ;\n"
      + "      dc:rights \"free\" ;\n" + "      dc:title \"Der Fugu\" ;\n"
      + "      dcterms:hasPart  ( dariahstorage:EAEA0-B19E-E6D8-D512-0 dariahstorage:EAEA0-C623-258A-EE0F-0 dariahstorage:EAEA0-F0DE-8CF7-10B6-0 ) ;\n"
      + "      dcterms:identifier <http://hdl.handle.net/21.T11998/0000-0001-349E-3> , <http://dx.doi.org/10.3249/0000-0001-349E-3> .\n"
      + "\n" + "dariahstorage:EAEA0-C623-258A-EE0F-0\n"
      + "      a       dariah:DataObject ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:date \"2015-06-20T14:54:27\" ;\n"
      + "      dc:format \"image/jpeg\" ;\n"
      + "      dc:identifier <http://hdl.handle.net/21.T11998/0000-0001-349F-2> , <http://dx.doi.org/10.3249/0000-0001-349F-2> ;\n"
      + "      dc:rights \"free\" ;\n"
      + "      dc:title \"IMG_0132.jpg\" ;\n"
      + "      dcterms:identifier <http://hdl.handle.net/21.T11998/0000-0001-349F-2> , <http://dx.doi.org/10.3249/0000-0001-349F-2> .\n"
      + "dariahstorage:EAEA0-BA55-9615-67CA-0\n"
      + "      a       dariah:DataObject ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:date \"2015-06-20T14:54:40\" ;\n"
      + "      dc:format \"image/jpeg\" ;\n"
      + "      dc:identifier <http://dx.doi.org/10.3249/0000-0001-34A3-C> , <http://hdl.handle.net/21.T11998/0000-0001-34A3-C> ;\n"
      + "      dc:rights \"free\" ;\n"
      + "      dc:title \"IMG_0135.jpg\" ;\n"
      + "      dcterms:identifier <http://dx.doi.org/10.3249/0000-0001-34A3-C> , <http://hdl.handle.net/21.T11998/0000-0001-34A3-C> .\n"
      + "\n" + "dariahstorage:EAEA0-F0DE-8CF7-10B6-0\n"
      + "      a       dariah:Collection ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:identifier <http://dx.doi.org/10.3249/0000-0001-34A1-E> , <http://hdl.handle.net/21.T11998/0000-0001-34A1-E> ;\n"
      + "      dc:rights \"free\" ;\n" + "      dc:title \"FuguSUB\" ;\n"
      + "      dcterms:hasPart dariahstorage:EAEA0-BA55-9615-67CA-0 , dariahstorage:EAEA0-3A1F-DEFC-8CFF-0 ;\n"
      + "      dcterms:identifier <http://dx.doi.org/10.3249/0000-0001-34A1-E> , <http://hdl.handle.net/21.T11998/0000-0001-34A1-E> .\n"
      + "\n" + "dariahstorage:EAEA0-3A1F-DEFC-8CFF-0\n"
      + "      a       dariah:DataObject ;\n"
      + "      dc:creator \"fu\" ;\n"
      + "      dc:date \"2015-06-20T14:54:43\" ;\n"
      + "      dc:format \"image/jpeg\" ;\n"
      + "      dc:identifier <http://dx.doi.org/10.3249/0000-0001-34A2-D> , <http://hdl.handle.net/21.T11998/0000-0001-34A2-D> ;\n"
      + "      dc:rights \"free\" ;\n"
      + "      dc:title \"IMG_0136.jpg\" ;\n"
      + "      dcterms:identifier <http://dx.doi.org/10.3249/0000-0001-34A2-D> , <http://hdl.handle.net/21.T11998/0000-0001-34A2-D> .";
  private static final String COLLECTION_DATA_1 = "@prefix hdl:   <http://hdl.handle.net/> .\n"
      + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
      + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
      + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
      + "\n"
      + "<http://hdl.handle.net/21.T11998/0000-0002-0D16-8>  a     dariah:Collection ;\n"
      + "        dcterms:hasPart  ( <http://hdl.handle.net/fu-1/urgl> <http://hdl.handle.net/fu-4/urgl> <http://hdl.handle.net/fu-3/urgl> <http://hdl.handle.net/fu-2/urgl> ) .\n";
  private static final String COLLECTION_DATA_2 = "@prefix hdl:   <http://hdl.handle.net/> .\n"
      + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
      + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
      + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
      + "\n"
      + "<http://hdl.handle.net/21.T11998/0000-0002-0D16-8>  a     dariah:Collection ;\n"
      + "        dcterms:hasPart  ( <http://hdl.handle.net/no:1> <http://hdl.handle.net/no:2> <http://hdl.handle.net/no:3> <http://hdl.handle.net/no:4> <http://hdl.handle.net/no:5> ) .\n";
  private static final String COLLECTION_DATA_3 = "@prefix hdl:   <http://hdl.handle.net/> .\n"
      + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
      + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
      + "\n"
      + "<http://hdl.handle.net/21.T11998/0000-0002-0D16-8>  a     dariah:Collection ;\n"
      + "        dcterms:hasPart  ( <http://hdl.handle.net/21.T11998/0000-0002-XXXX-1> "
      + " <http://hdl.handle.net/21.T11998/0000-0002-XXXX-2> "
      + " <http://hdl.handle.net/21.T11998/0000-0002-XXXX-3> ) .\n";
  private static final String CR_METADATA = "<rdf:RDF\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\"\n"
      + "    xmlns:dariahstorage=\"https://de.dariah.eu/storage/\">\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-B5CC-9C18-ABA5-0\">\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <dc:identifier rdf:resource=\"http://hdl.handle.net/21.T11998/0000-0002-464B-C\"/>\n"
      + "    <dc:identifier rdf:resource=\"http://dx.doi.org/10.20375/0000-0002-464B-C\"/>\n"
      + "    <dc:description>LINK</dc:description>\n"
      + "    <dc:identifier>http://box.dariah.local/oaipmh/oai/21.T11998%2F0000-0002-464B-C</dc:identifier>\n"
      + "    <dc:title>Kollektione!</dc:title>\n"
      + "    <dc:format>text/vnd.dariah.dhrep.collection+turtle</dc:format>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:contributor>StefanFunk@dariah.eu</dc:contributor>\n"
      + "    <dc:date>2017</dc:date>\n" + "  </rdf:Description>\n"
      + "</rdf:RDF>\n";
  private static final String CR_METADATA_EXPECTED = "<rdf:RDF\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\"\n"
      + "    xmlns:dariahstorage=\"https://de.dariah.eu/storage/\">\n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-B5CC-9C18-ABA5-0\">\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <dc:identifier>hdl:21.T11998/0000-0002-464B-C</dc:identifier>\n"
      + "    <dc:identifier>doi:10.20375/0000-0002-464B-C</dc:identifier>\n"
      + "    <dc:description>LINK</dc:description>\n"
      + "    <dc:identifier>http://box.dariah.local/oaipmh/oai/21.T11998%2F0000-0002-464B-C</dc:identifier>\n"
      + "    <dc:title>Kollektione!</dc:title>\n"
      + "    <dc:format>text/vnd.dariah.dhrep.collection+turtle</dc:format>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:contributor>StefanFunk@dariah.eu</dc:contributor>\n"
      + "    <dc:date>2017</dc:date>\n" + "  </rdf:Description>\n"
      + "</rdf:RDF>\n";
  private static final String CR_RESPONSE =
      "<RepositoryResponse xmlns=\"\"><status>OK</status><url>https://dfatest.de.dariah.eu/colreg-ui/drafts/5a1d9097d8ad371a48143293</url><xml>https://dfatest.de.dariah.eu/colreg-ui/oaipmh/?verb=GetRecord&amp;metadataPrefix=dcddm&amp;identifier=5a1d9097d8ad371a48143293</xml><error/></RepositoryResponse>";
  private static final String EXPECTED_CRID = "5a1d9097d8ad371a48143293";
  private static final String OK = "...OK";
  private static final String FAILED = "...FAILED";

  /**
   * @throws XMLStreamException
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testCreateAggregationData() throws XMLStreamException, IOException, ParseException {

    System.out.print("Processing simple collection data creation test... ");

    URI uri1 = URI.create("hdl:no:1");
    URI uri2 = URI.create("hdl:no:2");
    URI uri3 = URI.create("hdl:no:3");
    URI uri4 = URI.create("hdl:no:4");
    URI uri5 = URI.create("hdl:no:5");
    String resource = "hdl:21.T11998/0000-0002-0D16-8";

    List<URI> aggregates = new ArrayList<URI>();
    aggregates.add(uri1);
    aggregates.add(uri2);
    aggregates.add(uri3);
    aggregates.add(uri4);
    aggregates.add(uri5);

    Model collectionData = DHPublishUtils.createCollectionData(aggregates, resource);
    Model expected = RDFUtils.readModel(COLLECTION_DATA_2, RDFConstants.TURTLE);

    if (!expected.isIsomorphicWith(collectionData)) {
      System.out.println(FAILED);
      System.out.println("COLLECTION DATA\n" + RDFUtils.getTTLFromModel(collectionData));
      System.out.println("EXPECTED\n" + RDFUtils.getTTLFromModel(expected));
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * @throws XMLStreamException
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testCreateAggregationDataUsingHandlesWithSlash()
      throws XMLStreamException, IOException, ParseException {

    // NOTE We want to have "hdl:21.T11998/0000-0002-0D16-8" as a resource
    // in the model, but we can only get
    // <http://hdl.handle.net/21.T11998/0000-0002-0D16-8>, because there is
    // a "/" in the Handle! Just add every resource as
    // <http://hdl.handle.net/21.T11998/0000-0002-0D16-8>!

    System.out.print("Processing simple collection data creation test... ");

    URI uri1 = URI.create("hdl:21.T11998/0000-0002-XXXX-1");
    URI uri2 = URI.create("hdl:21.T11998/0000-0002-XXXX-2");
    URI uri3 = URI.create("hdl:21.T11998/0000-0002-XXXX-3");
    String resource = "hdl:21.T11998/0000-0002-0D16-8";

    List<URI> aggregates = new ArrayList<URI>();
    aggregates.add(uri1);
    aggregates.add(uri2);
    aggregates.add(uri3);

    Model collectionData = DHPublishUtils.createCollectionData(aggregates, resource);
    Model expected = RDFUtils.readModel(COLLECTION_DATA_3, RDFConstants.TURTLE);

    if (!expected.isIsomorphicWith(collectionData)) {
      System.out.println(FAILED);
      System.out.println("COLLECTION DATA\n" + RDFUtils.getTTLFromModel(collectionData));
      System.out.println("EXPECTED\n" + RDFUtils.getTTLFromModel(expected));
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testCreateMetadata() throws IOException, ParseException {

    System.out.println("Creating object's metadata from RDF model... ");

    Model overallModel = RDFUtils.readModel(RDF_PID_DATA, RDFConstants.TURTLE);
    String uri = "http://geobrowser.de.dariah.eu/storage/300303";
    String pid = "hdl:urgl:argl:aua";

    Model metadataModel = DHPublishUtils.createMetadata(overallModel, uri, pid);
    Model expectedMetadata = RDFUtils.readModel(EXPECTED_METADATA, RDFConstants.TURTLE);

    if (!metadataModel.isIsomorphicWith(expectedMetadata)) {
      System.out.println(FAILED);
      System.out.println("METADATA:\n" + RDFUtils.getTTLFromModel(metadataModel));
      System.out.println("EXPECTED:\n" + RDFUtils.getTTLFromModel(expectedMetadata));
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   * @throws ParseException
   */
  @Test
  public void testGetHasPartsFromRdfModelOrdered()
      throws XMLStreamException, IOException, ParseException {

    System.out.print("Getting hasParts from ORE RDF data ordered... ");

    String pre = "http://geobrowser.de.dariah.eu/storage/";
    String resource = pre + "300351";
    Model m = RDFUtils.readModel(RDF_DATA, RDFConstants.TURTLE);

    String uri1 = "http://geobrowser.de.dariah.eu/storage/300304";
    String uri2 = "http://geobrowser.de.dariah.eu/storage/300303";
    String uri3 = "http://geobrowser.de.dariah.eu/storage/300302";
    String uri4 = "http://geobrowser.de.dariah.eu/storage/300352";
    List<String> expectedOrderedList = new ArrayList<String>();
    expectedOrderedList.add(uri1);
    expectedOrderedList.add(uri2);
    expectedOrderedList.add(uri3);
    expectedOrderedList.add(uri4);

    List<String> l =
        RDFUtils.getProperties(m, resource, RDFConstants.DCTERMS_PREFIX,
            RDFConstants.ELEM_DCTERMS_HASPART);

    System.out.println(l);

    if (l.size() != 4) {
      System.out.println(FAILED);
      assertTrue(false);
    } else {
      if (l.get(0).equals(uri1) && l.get(1).equals(uri2) && l.get(2).equals(uri3)
          && l.get(3).equals(uri4)) {
        System.out.println(OK);
      } else {
        System.out.println(l);
        System.out.println(expectedOrderedList);
        System.out.println(FAILED);
        assertTrue(false);
      }
    }
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   * @throws ParseException
   */
  @Test
  public void testGetHasPartsWithNoHasPartsFromRdfModel()
      throws XMLStreamException, IOException, ParseException {

    System.out.print("Getting hasParts from ORE RDF data (none existing)... ");

    String pre = "http://geobrowser.de.dariah.eu/storage/";
    String resource = pre + "300351";
    Model m = RDFUtils.readModel(RDF_DATA_NO_HASPARTS, RDFConstants.TURTLE);

    List<String> l = RDFUtils.getProperties(m, resource, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DCTERMS_HASPART);

    System.out.println(l);

    if (!l.isEmpty()) {
      System.out.println(FAILED);
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * @throws XMLStreamException
   * @throws IOException
   * @throws URISyntaxException
   * @throws ParseException
   */
  @Test
  public void testGetPidsFromOverallModel()
      throws XMLStreamException, IOException, URISyntaxException, ParseException {

    System.out.print("Getting PIDs from ORE model... ");

    String resource = "http://geobrowser.de.dariah.eu/storage/300351";
    Model model = RDFUtils.readModel(RDF_PID_DATA, RDFConstants.TURTLE);

    List<URI> pids = new ArrayList<URI>();
    pids.add(URI.create("http://hdl.handle.net/fu-1/urgl"));
    pids.add(URI.create("http://hdl.handle.net/fu-2/urgl"));
    pids.add(URI.create("http://hdl.handle.net/fu-3/urgl"));
    pids.add(URI.create("http://hdl.handle.net/fu-4/urgl"));

    System.out.print(pids + "... ");

    List<URI> modelPids = DHPublishUtils.getHasPartPIDsFromRdfModel(model, resource,
        RDFConstants.DCTERMS_PREFIX, RDFConstants.ELEM_DC_IDENTIFIER);

    System.out.println(modelPids);

    if (modelPids.contains(pids.get(0)) && modelPids.contains(pids.get(1))
        && modelPids.contains(pids.get(2)) && modelPids.contains(pids.get(3))) {
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      System.out.println("RDF_PID_DATA:\n" + RDFUtils.getTTLFromModel(model));
      System.out.println(modelPids);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   * @throws URISyntaxException
   * @throws ParseException
   */
  @Test
  public void testGetCollectionDataFromRdfModelOrdered()
      throws XMLStreamException, IOException, URISyntaxException, ParseException {

    System.out.print("Getting collection data file from RDF model ordered... ");

    String uri = "http://geobrowser.de.dariah.eu/storage/300351";
    Model model = RDFUtils.readModel(RDF_PID_DATA, RDFConstants.TURTLE);
    Model expected = RDFUtils.readModel(COLLECTION_DATA_1, RDFConstants.TURTLE);

    // First get list of PIDs.
    List<URI> pids =
        DHPublishUtils.getHasPartPIDsFromRdfModel(model, uri, RDFConstants.DCTERMS_PREFIX,
            RDFConstants.ELEM_DC_IDENTIFIER);

    System.out.println(pids);

    // Then put together the parts as a new collection data file (dariah:Collection).
    String resource = "hdl:21.T11998/0000-0002-0D16-8";
    Model data = DHPublishUtils.createCollectionData(pids, resource);

    if (!expected.isIsomorphicWith(data)) {
      System.out.println(FAILED);
      System.out.println("COLLECTION DATA\n" + RDFUtils.getTTLFromModel(data));
      System.out.println("EXPECTED\n" + RDFUtils.getTTLFromModel(expected));
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * @throws XMLStreamException
   * @throws IOException
   * @throws URISyntaxException
   * @throws ParseException
   */
  @Test
  public void testCreateCollectionMetadataOneCollection()
      throws XMLStreamException, IOException, URISyntaxException, ParseException {

    System.out.println(
        "Getting collection's metadata from RDF model (new URIs, new PIDs, one collection)... ");

    // Create expected model.
    Model expectedModel = RDFUtils.readModel(COLLECTION_METADATA, RDFConstants.TURTLE);

    // Create collection metadata model.
    Model completeModel = RDFUtils.readModel(TURTLE_PID_DATA, RDFConstants.TURTLE);
    String uri = "hdl:21.T11998/0000-0001-349E-3";
    String source = "https://de.dariah.eu/storage/EAEA0-59F3-2837-4561-0";
    Model collectionMetadata = DHPublishUtils.createCollectionMetadata(completeModel, uri, source);

    // Do compare.
    boolean equal = collectionMetadata.isIsomorphicWith(expectedModel);

    if (equal) {
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      System.out.println("EXPECTED\n" + RDFUtils.getTTLFromModel(expectedModel));
      System.out.println("CREATED\n" + RDFUtils.getTTLFromModel(collectionMetadata));
      assertTrue(false);
    }
  }

  /**
   * @throws XMLStreamException
   * @throws IOException
   * @throws URISyntaxException
   * @throws ParseException
   */
  @Test
  public void testCreateCollectionMetadata()
      throws XMLStreamException, IOException, URISyntaxException, ParseException {

    System.out.println(
        "Getting collection's metadata from RDF model (new URIs, new PIDs, one collection)... ");

    // Create expected model.
    Model expectedModel = RDFUtils.readModel(COLLECTION_METADATA, RDFConstants.TURTLE);

    // Create collection metadata model.
    Model completeModel = RDFUtils.readModel(URGL_NEW_PIDS, RDFConstants.TURTLE);
    String uri = "hdl:21.T11998/0000-0001-349E-3";
    String source = "https://de.dariah.eu/storage/EAEA0-59F3-2837-4561-0";
    Model collectionMetadata = DHPublishUtils.createCollectionMetadata(completeModel, uri, source);

    // Do compare.
    boolean equal = collectionMetadata.isIsomorphicWith(expectedModel);

    if (equal) {
      System.out.println(OK);
    } else {
      System.out.println("EXPECTED\n" + RDFUtils.getTTLFromModel(expectedModel));
      System.out.println("CREATED\n" + RDFUtils.getTTLFromModel(collectionMetadata));
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Checks if we get the correct COLREG_ID value from PID response using JSON.
   * </p>
   */
  @Test
  public void testExtractPidValueColreg() {

    String json =
        "{\"responseCode\":1,\"handle\":\"11022/0000-0000-8487-2\",\"values\":[{\"index\":14,\"type\":\"COLREG_ID\",\"data\":{\"format\":\"string\",\"value\":\"5746ff8a7c8dec05e679dece\"},\"ttl\":86400,\"timestamp\":\"2015-08-18T13:11:56Z\"}]}";
    String toFind = "COLREG_ID";

    System.out.print("Getting COLREG_ID from PID response... ");
    String found = DHPublishUtils.extractPidValue(json, toFind);

    if (!found.equals("5746ff8a7c8dec05e679dece")) {
      System.out.println();
      System.out.println(FAILED);
      assertTrue(false);
    } else {
      System.out.println(found);
      System.out.println(OK);
    }
  }

  /**
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testIsCollectionDariahStorage() throws IOException, ParseException {

    System.out.println("Testing isCollection for about:dariastorage new PIDs... ");

    // Create model.
    Model model = RDFUtils.readModel(IS_COLLECTION_TURTLE, RDFConstants.TURTLE);

    for (String s : DHPublishUtils.getAllAboutsFromModel(model)) {
      boolean isCollection = RDFUtils.isCollection(model, s);
      System.out.println("  " + s + " is collection? " + isCollection);

      if (s.equals("https://de.dariah.eu/storage/EAEA0-0606-122A-DE40-0")
          && isCollection == false) {
        System.out.println(FAILED);
        assertTrue(false);
      }
      if (s.equals("https://de.dariah.eu/storage/EAEA0-5DA9-2915-DCE4-0")
          && isCollection == false) {
        System.out.println(FAILED);
        assertTrue(false);
      }
    }

    System.out.println(OK);
  }

  /**
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testIsCollectionPIDs() throws IOException, ParseException {

    System.out.println("Testing isCollection for about:PID... ");

    // Create COLLECTION model.
    Model model = RDFUtils.readModel(IS_COLLECTION, RDFConstants.RDF_XML);

    for (String s : DHPublishUtils.getAllAboutsFromModel(model)) {
      boolean isCollection = RDFUtils.isCollection(model, s);
      System.out.println("  " + s + " is collection? " + isCollection);

      if (!isCollection) {
        System.out.println(FAILED);
        assertTrue(false);
      } else {
        System.out.println(OK);
      }
    }

    // Create NO COLLECTION model.
    model = RDFUtils.readModel(IS_NO_COLLECTION, RDFConstants.RDF_XML);

    for (String s : DHPublishUtils.getAllAboutsFromModel(model)) {
      boolean isCollection = RDFUtils.isCollection(model, s);
      System.out.println("  " + s + " is collection? " + isCollection);

      if (isCollection) {
        System.out.println(FAILED);
        assertTrue(false);
      } else {
        System.out.println(OK);
      }
    }
  }

  /**
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testIsCollectionPIDsNewPids() throws IOException, ParseException {

    System.out.println("Testing isCollection for about:PID new PIDs... ");

    // Create COLLECTION model.
    Model model = RDFUtils.readModel(IS_COLLECTION_NEW_PIDS, RDFConstants.RDF_XML);

    for (String s : DHPublishUtils.getAllAboutsFromModel(model)) {
      boolean isCollection = RDFUtils.isCollection(model, s);
      System.out.println("  " + s + " is collection? " + isCollection);

      if (!isCollection) {
        System.out.println(FAILED);
        assertTrue(false);
      } else {
        System.out.println(OK);
      }
    }

    // Create NO COLLECTION model.
    model = RDFUtils.readModel(IS_NO_COLLECTION_NEW_PIDS, RDFConstants.RDF_XML);

    for (String s : DHPublishUtils.getAllAboutsFromModel(model)) {
      boolean isCollection = RDFUtils.isCollection(model, s);
      System.out.println("  " + s + " is collection? " + isCollection);

      if (isCollection) {
        System.out.println(FAILED);
        assertTrue(false);
      } else {
        System.out.println(OK);
      }
    }
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   * @throws ParseException
   */
  @Test
  public void testCreateCollectionRegistryDcMetadata()
      throws XMLStreamException, IOException, ParseException {

    System.out.println("Test creating CR metadata new PIDs... ");

    String uri = "https://de.dariah.eu/storage/EAEA0-E67C-E8B2-508F-0";
    String pid = "hdl:21.T11998/0000-0007-0804-C";

    // Create models.
    Model expectedModel = RDFUtils.readModel(EXPECTED_CR_METADATA, RDFConstants.RDF_XML);
    Model model = RDFUtils.readModel(TURTLE_METADATA, RDFConstants.TURTLE);

    // Create result model from model.
    Model resultModel = DHPublishUtils.createCollectionRegistryDcMetadata(model, uri, pid);

    // Do compare.
    // NOTE We do need the rdf:Description tag in the result model for CR calls!
    String resultModelString = RDFUtils.getStringFromModel(resultModel);
    if (resultModel.isIsomorphicWith(expectedModel)
        && resultModelString.contains("rdf:Description")) {
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      System.out.println("EXPECTED\n" + RDFUtils.getStringFromModel(expectedModel));
      System.out.println("CREATED\n" + RDFUtils.getStringFromModel(resultModel));
      System.out.println(
          "DIFFERENCE:\n" + RDFUtils.getStringFromModel(resultModel.difference(expectedModel)));
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   * @throws ParseException
   */
  @Test
  public void testCreateDcMetadataCollection()
      throws XMLStreamException, IOException, ParseException {

    System.out.println("Test creating DC metadata for Collection new PIDs... ");

    String collectionUri = "https://de.dariah.eu/storage/EAEA0-E67C-E8B2-508F-0";

    // Create root model.
    Model model = RDFUtils.readModel(TURTLE_METADATA, RDFConstants.TURTLE);
    // Create expected Collection model.
    Model expectedDcCollectionModel =
        RDFUtils.readModel(EXPECTED_DC_METADATA_COLLECTION, RDFConstants.TURTLE);

    model.write(System.out, RDFConstants.TURTLE);
    expectedDcCollectionModel.write(System.out, RDFConstants.TURTLE);

    // Create result Collection model from model and check.
    Model resultCollectionModel = DHPublishUtils.getDcMetadata(model, collectionUri);

    resultCollectionModel.write(System.out, RDFConstants.TURTLE);

    boolean equalCollection = resultCollectionModel.isIsomorphicWith(expectedDcCollectionModel);
    if (equalCollection) {
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   * @throws ParseException
   */
  @Test
  public void testCreateDcMetadataDataobjectNewPids()
      throws XMLStreamException, IOException, ParseException {

    System.out.println("Test creating DC metadata for DataObject new PIDs... ");

    String dataobjectUri = "https://de.dariah.eu/storage/EAEA0-85EB-1ED4-0C1E-0";

    // Create root model.
    Model model = RDFUtils.readModel(TURTLE_METADATA, RDFConstants.TURTLE);
    // Create expected DataObject model.
    Model expectedDcDataobjectModel =
        RDFUtils.readModel(EXPECTED_DC_METADATA_DATAOBJECT, RDFConstants.TURTLE);

    // Create result DataObject model from model and check.
    Model resultDataobjectModel = DHPublishUtils.getDcMetadata(model, dataobjectUri);

    boolean equalDataobject = resultDataobjectModel.isIsomorphicWith(expectedDcDataobjectModel);
    if (equalDataobject) {
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      System.out.println("CREATED:\n" + RDFUtils.getTTLFromModel(resultDataobjectModel));
      System.out.println("EXPECTED:\n" + RDFUtils.getTTLFromModel(expectedDcDataobjectModel));
      assertTrue(false);
    }
  }

  /**
   * 
   */
  @Test
  public void testOmitPidPrefix() {

    System.out.println("Test omitting HDL prefixes... ");

    String pid1 = "hdl:11022/0000-0007-0805-B";
    String expectedOmitPid1 = "11022/0000-0007-0805-B";
    String omittedPid1 = LTPUtils.omitHdlPrefix(pid1);
    if (expectedOmitPid1.equals(omittedPid1)) {
      System.out.println(OK);
    } else {
      System.out.println(expectedOmitPid1 + " ≠ " + omittedPid1);
      System.out.println(FAILED);
      assertTrue(false);
    }

    String pid2 = "11022/0000-0007-0805-B";
    String expectedOmitPid2 = "11022/0000-0007-0805-B";
    String omittedPid2 = LTPUtils.omitHdlPrefix(pid2);
    if (expectedOmitPid2.equals(omittedPid2)) {
      System.out.println(OK);
    } else {
      System.out.println(expectedOmitPid2 + " ≠ " + omittedPid2);
      System.out.println(FAILED);
      assertTrue(false);
    }

  }

  /**
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testGetAdministrativePidWithPrefix() throws IOException, ParseException {

    System.out.println("Test administrative PID access with HDL prefix... ");

    String dataobjectUri = "https://de.dariah.eu/storage/EAEA0-85EB-1ED4-0C1E-0";
    String expectedPid = "hdl:21.T11998/0000-0007-0805-B";

    // Create root model.
    Model model = RDFUtils.readModel(TURTLE_METADATA, RDFConstants.TURTLE);

    String handle = DHPublishUtils.getAdministrativePid(model, dataobjectUri);

    if (expectedPid.equals(handle)) {
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      System.out.println(expectedPid + " ≠ " + handle);
      System.out.println("MODEL:\n" + RDFUtils.getTTLFromModel(model));
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testGetModelContainingOnlyLiteralsCR() throws IOException, ParseException {

    System.out.println("Test literalising CR metadata... ");

    String uri = "https://de.dariah.eu/storage/EAEA0-B5CC-9C18-ABA5-0";
    List<String> prefixes = new ArrayList<String>();
    prefixes.add(RDFConstants.DC_PREFIX);

    Model crmd = RDFUtils.readModel(CR_METADATA);
    Model crmdExpected = RDFUtils.readModel(CR_METADATA_EXPECTED);

    Model crmdLiteral = RDFUtils.getModelContainingOnlyLiterals(crmd, uri, prefixes, true);

    if (!crmdExpected.isIsomorphicWith(crmdLiteral)) {
      System.out.println(FAILED);
      System.out.println("CREATED:\n" + RDFUtils.getStringFromModel(crmdLiteral));
      System.out.println("EXPECTED:\n" + RDFUtils.getStringFromModel(crmdExpected));
      System.out.println(
          "DIFFERENCE:\n" + RDFUtils.getStringFromModel(crmdExpected.difference(crmdLiteral)));
      assertTrue(false);
    }

    System.out.println(OK);
  }

  /**
   * 
   */
  @Test
  public void testGetCRIDFromCRResponse() {

    System.out.println("Test getting CRID from CR response... ");

    String crid = DHPublishUtils.getCRIDFromCRResponse(CR_RESPONSE);

    if (!crid.equals(EXPECTED_CRID)) {
      System.out.println(FAILED);
      System.out.println("CRID: " + crid);
      System.out.println("EXPECTED: " + EXPECTED_CRID);
      assertTrue(false);
    }

    System.out.println(OK);
  }

}
