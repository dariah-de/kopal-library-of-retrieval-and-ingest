/**
 * This software is copyright (c) 2021 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.junit.Test;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/**
 * <p>
 * Testing CheckCollections class.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-03-10
 * @since 2017-05-04
 */

public class TestCheckCollections {

  private static final String TURTLE_DATA =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
          + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
          + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
          + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
          + "@prefix tgforms: <http://www.tgforms.de/terms#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dariahstorage: <http://geobrowser.de.dariah.eu/storage/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "dariahstorage:379853  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"SwampSlime.gif\" .\n" + "\n"
          + "dariahstorage:379752  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"LordOrder.gif\" .\n" + "\n"
          + "dariahstorage:379851  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"LordOrder.gif\" .\n" + "\n"
          + "dariahstorage:379801  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"WaterElemental.gif\" .\n" + "\n"
          + "dariahstorage:379753  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:title   \"Ruster.gif\" .\n" + "\n"
          + "dariahstorage:379852  a  dariah:DataObject ;\n"
          + "        dc:format  \"image/gif\" ;\n"
          + "        dc:rights  \"urgsimurgsi?\" ;\n"
          + "        dc:title   \"Skeleton.gif\" .\n" + "\n"
          + "dariahstorage:379751  a  dariah:Collection ;\n"
          + "        dc:title         \"urgliwurgli\" ;\n"
          + "        dc:rights         \"arglimargli\" ;\n"
          + "        dcterms:hasPart  dariahstorage:379853 , dariahstorage:379852 , dariahstorage:379851 , dariahstorage:379801 , dariahstorage:379752 , dariahstorage:379753 .\n";
  private static final String TURTLE_DATA_NEW_URIS =
      "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/>.\n"
          + "@prefix dariahstorage: <https://de.dariah.eu/storage/>.\n"
          + "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.\n"
          + "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.\n"
          + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#>.\n"
          + "@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.\n"
          + "@prefix dc: <http://purl.org/dc/elements/1.1/>.\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/>.\n"
          + "@prefix seafile: <https://sftest.de.dariah.eu/files/>.\n"
          + "@prefix owl: <http://www.w3.org/2002/07/owl#>.\n"
          + "@prefix skos: <http://www.w3.org/2004/02/skos/core#>.\n"
          + "@prefix dcam: <http://purl.org/dc/dcam/>.\n" + "\n"
          + "dariahstorage:EAEA0-07C5-8340-37B0-0 dc:title \"URGL ARGL AUA HMPF\";\n"
          + "    dc:creator \"fugu\";\n" + "    dc:rights \"free\";\n"
          + "    dcterms:hasPart dariahstorage:EAEA0-DFE1-1915-CF8D-0, dariahstorage:EAEA0-334C-8D10-1F85-0, dariahstorage:EAEA0-497C-B75E-43B6-0, dariahstorage:EAEA0-22F7-B399-9A0C-0;\n"
          + "    a dariah:Collection.\n"
          + "dariahstorage:EAEA0-DFE1-1915-CF8D-0 dc:title \"Ruster.gif\";\n"
          + "    dc:creator \"fugu\";\n" + "    dc:rights \"free\";\n"
          + "    dc:format \"image/gif\";\n" + "    a dariah:DataObject.\n"
          + "dariahstorage:EAEA0-334C-8D10-1F85-0 dc:title \"Spreu.mp3\", \"Spreu\";\n"
          + "    dc:creator \"INPANIK\";\n" + "    dc:rights \"ccby\";\n"
          + "    dc:format \"audio/mpeg\";\n" + "    a dariah:DataObject.\n"
          + "dariahstorage:EAEA0-497C-B75E-43B6-0 dc:title \"Scorpion.gif\";\n"
          + "    dc:creator \"fugu\";\n" + "    dc:rights \"free\";\n"
          + "    dc:format \"image/gif\";\n" + "    a dariah:DataObject.\n"
          + "dariahstorage:EAEA0-22F7-B399-9A0C-0 dc:title \"Couatl.gif\";\n"
          + "    dc:creator \"fugi\", \"fugu\";\n"
          + "    dc:rights \"free\", \"fugi\";\n"
          + "    dc:format \"image/gif\";\n" + "    a dariah:DataObject.\n";
  private static final String DATE_TIME_RDF = "<rdf:RDF\n"
      + "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\"\n"
      + "    xmlns:hdl=\"http://hdl.handle.net/\" > \n"
      + "  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-E67C-E8B2-508F-0\">\n"
      + "    <dc:date>2017-07.fff</dc:date>\n"
      + "    <dc:date>2017</dc:date>\n"
      + "    <dc:date>2017-04-12</dc:date>\n"
      + "    <dc:date>2017-03-12T00:00:00</dc:date>\n"
      + "    <dc:date>2017-03-12T10:11</dc:date>\n"
      + "    <dc:date>2017-12-32T23:33:01.122</dc:date>\n"
      + "    <dc:date>2017-12-31T23:33:01.122</dc:date>\n"
      + "    <dc:date>morgen :-)</dc:date>\n"
      + "    <dc:publisher>FU's Verlag</dc:publisher>\n"
      + "    <dc:language>DE</dc:language>\n"
      + "    <dc:description>Hier eine Beschreibung</dc:description>\n"
      + "    <dc:format>Ein Format...... extra sozusagen</dc:format>\n"
      + "    <dcterms:source>https://dariah-cdstar.gwdg.de/dariah/EAEA0-E67C-E8B2-508F-0</dcterms:source>\n"
      + "    <dc:identifier>ein ID, Yeah!</dc:identifier>\n"
      + "    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-85EB-1ED4-0C1E-0\"/>\n"
      + "    <dc:rights>free</dc:rights>\n"
      + "    <dc:title>Tobis Testkollektion</dc:title>\n"
      + "    <dc:subject>Dies ist ein Test!</dc:subject>\n"
      + "    <dc:coverage>Coverage: TOTAL</dc:coverage>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>\n"
      + "    <dcterms:format>text/tg.collection+tg.aggregation+xml</dcterms:format>\n"
      + "    <dc:source>Und anderswo her ebenfslla</dc:source>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <dc:format>text/tg.collection+tg.aggregation+xml</dc:format>\n"
      + "    <dc:contributor>Mitwirkende</dc:contributor>\n"
      + "    <dcterms:identifier>11022/0000-0007-2072-4</dcterms:identifier>\n"
      + "    <dc:relation>Steht in Beziehung mit einer anderen Kollektion</dc:relation>\n"
      + "    <dc:type>Ein Typ, ein netter :-)</dc:type>\n"
      + "    <dc:source>Kommt irgendwo her</dc:source>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>";
  private static final String COMPLETE_MODEL = "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
      + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
      + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
      + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
      + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
      + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
      + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
      + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
      + "@prefix hdl:   <http://hdl.handle.net/> .\n"
      + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
      + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
      + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
      + "@prefix doi:   <http://dx.doi.org/> .\n"
      + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
      + "dariahstorage:EAEA0-2FAB-C0C9-FAC3-0\n"
      + "        a                   dariah:DataObject ;\n"
      + "        dc:creator          \"fu\" ;\n"
      + "        dc:date             \"2015-06-20T16:54:35\" ;\n"
      + "        dc:format           \"image/jpeg\" ;\n"
      + "        dc:identifier       \"doi:10.20375/0000-0002-1FA8-F\" , \"hdl:21.T11998/0000-0002-1FA8-F\" ;\n"
      + "        dc:relation         \"dariah:Collection:21.T11998/0000-0002-1FA7-0\" ;\n"
      + "        dc:description      \"<strong>Ho mein lieber! Hier ist ein <a href=\\\"http://ein.link.de\\\">LINK!</a></strong>\" ;\n"
      + "        dc:rights           \"free\" ;\n"
      + "        dc:title            \"IMG_0133.jpg\" ;\n"
      + "        dcterms:identifier  \"doi:10.20375/0000-0002-1FA8-F\" , \"hdl:21.T11998/0000-0002-1FA8-F\" .\n"
      + "\n" + "dariahstorage:EAEA0-2261-6F63-934B-0\n"
      + "        a                   dariah:DataObject ;\n"
      + "        dc:creator          \"fu\" ;\n"
      + "        dc:date             \"2015-06-20T16:54:53\" ;\n"
      + "        dc:format           \"image/jpeg\" ;\n"
      + "        dc:identifier       \"doi:10.20375/0000-0002-1FAA-D\" , \"hdl:21.T11998/0000-0002-1FAA-D\" ;\n"
      + "        dc:relation         \"dariah:Collection:21.T11998/0000-0002-1FA7-0\" ;\n"
      + "        dc:rights           \"free\" ;\n"
      + "        dc:title            \"IMG_0138.jpg\" ;\n"
      + "        dcterms:identifier  \"hdl:21.T11998/0000-0002-1FAA-D\" , \"doi:10.20375/0000-0002-1FAA-D\" .\n"
      + "\n" + "dariahstorage:EAEA0-D291-D132-77AF-0\n"
      + "        a                   dariah:Collection ;\n"
      + "        dc:creator          \"fu\" ;\n"
      + "        dc:format           \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
      + "        dc:identifier       \"hdl:21.T11998/0000-0002-1FA9-E\" , \"doi:10.20375/0000-0002-1FA9-E\" ;\n"
      + "        dc:relation         \"dariah:Collection:21.T11998/0000-0002-1FA7-0\" ;\n"
      + "        dc:rights           \"<a href=\\\"http://urgli.de\\\">free</a>\" ;\n"
      + "        dc:title            \"<strong>subsubsub</strong>\" ;\n"
      + "        dcterms:format      \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
      + "        dcterms:hasPart     dariahstorage:EAEA0-2BB8-278C-60B6-0 , dariahstorage:EAEA0-2261-6F63-934B-0 ;\n"
      + "        dcterms:identifier  \"doi:10.20375/0000-0002-1FA9-E\" , \"hdl:21.T11998/0000-0002-1FA9-E\" ;\n"
      + "        dcterms:source      \"https://cdstar.de.dariah.eu/test/dariah/EAEA0-D291-D132-77AF-0\" .\n"
      + "\n" + "dariahstorage:EAEA0-A154-F401-2282-0\n"
      + "        a                   dariah:DataObject ;\n"
      + "        dc:creator          \"fu\" ;\n"
      + "        dc:date             \"2015-06-20T16:54:37\" ;\n"
      + "        dc:format           \"image/jpeg\" ;\n"
      + "        dc:identifier       \"doi:10.20375/0000-0002-1FAD-A\" , \"hdl:21.T11998/0000-0002-1FAD-A\" ;\n"
      + "        dc:relation         \"dariah:Collection:21.T11998/0000-0002-1FA7-0\" ;\n"
      + "        dc:rights           \"free\" ;\n"
      + "        dc:title            \"IMG_0134.jpg\" ;\n"
      + "        dcterms:identifier  \"doi:10.20375/0000-0002-1FAD-A\" , \"hdl:21.T11998/0000-0002-1FAD-A\" .\n"
      + "\n" + "dariahstorage:EAEA0-B454-E355-D0E2-0\n"
      + "        a                   dariah:DataObject ;\n"
      + "        dc:creator          \"fu\" ;\n"
      + "        dc:date             \"2015-06-20T16:54:40\" ;\n"
      + "        dc:format           \"image/jpeg\" ;\n"
      + "        dc:identifier       \"hdl:21.T11998/0000-0002-1FAC-B\" , \"doi:10.20375/0000-0002-1FAC-B\" ;\n"
      + "        dc:relation         \"dariah:Collection:21.T11998/0000-0002-1FA7-0\" ;\n"
      + "        dc:rights           \"free\" ;\n"
      + "        dc:title            \"IMG_0135.jpg\" ;\n"
      + "        dcterms:identifier  \"doi:10.20375/0000-0002-1FAC-B\" , \"hdl:21.T11998/0000-0002-1FAC-B\" .\n"
      + "\n" + "dariahstorage:EAEA0-B5CC-9C18-ABA5-0\n"
      + "        a                   dariah:Collection ;\n"
      + "        dc:creator          \"fu\" ;\n"
      + "        dc:format           \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
      + "        dc:identifier       \"doi:10.20375/0000-0002-1FA7-0\" , \"hdl:21.T11998/0000-0002-1FA7-0\" ;\n"
      + "        dc:rights           \"free\" ;\n"
      + "        dc:title            \"Kollektione!\" ;\n"
      + "        dcterms:format      \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
      + "        dcterms:hasPart     dariahstorage:EAEA0-2FAB-C0C9-FAC3-0 , dariahstorage:EAEA0-D291-D132-77AF-0 , dariahstorage:EAEA0-B454-E355-D0E2-0 , dariahstorage:EAEA0-A154-F401-2282-0 ;\n"
      + "        dcterms:identifier  \"hdl:21.T11998/0000-0002-1FA7-0\" , \"doi:10.20375/0000-0002-1FA7-0\" ;\n"
      + "        dcterms:source      \"https://cdstar.de.dariah.eu/test/dariah/EAEA0-B5CC-9C18-ABA5-0\" .\n";
  private static final String OK = "\n...OK";
  private static final String FAILED = "\n...FAILED";

  /**
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testCheckRequiredMetadataForCollection() throws IOException, ParseException {

    List<String> missing = new ArrayList<String>();

    System.out.print("Testing required metadata check for collection... ");

    Model model = RDFUtils.readModel(TURTLE_DATA, RDFConstants.TURTLE);
    String resource = "http://geobrowser.de.dariah.eu/storage/379751";

    // Create required item list.
    List<String> requiredFields = new ArrayList<String>();
    requiredFields.add("dc:rights");
    requiredFields.add("dc:title");

    System.out.print("looking for " + requiredFields + " in " + resource + "... ");

    // Loop all the elements required, set error if necessary.
    for (String element : requiredFields) {

      String[] banana = element.split(":");
      String value = RDFUtils.findFirstObject(model, resource, banana[0], banana[1]);

      if (!value.isEmpty()) {
        System.out.print("found " + element + "... ");
      } else {
        missing.add(element);
      }
    }

    if (!missing.isEmpty()) {
      System.out.println("value" + (missing.size() != 1 ? "s" : "") + " missing: " + missing);
      System.out.println(FAILED);
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testCheckRequiredMetadataForCollectionNEWURIS() throws IOException, ParseException {

    List<String> missing = new ArrayList<String>();

    System.out.print("Testing required metadata check for collection NEW URIS... ");

    Model model = RDFUtils.readModel(TURTLE_DATA_NEW_URIS, RDFConstants.TURTLE);
    String resource = "https://de.dariah.eu/storage/EAEA0-07C5-8340-37B0-0";

    // Create required item list.
    List<String> requiredFields = new ArrayList<String>();
    requiredFields.add("dc:rights");
    requiredFields.add("dc:title");

    System.out.print("looking for " + requiredFields + " in " + resource + "... ");

    // Loop all the elements required, set error if necessary.
    for (String element : requiredFields) {

      String[] banana = element.split(":");
      String value = RDFUtils.findFirstObject(model, resource, banana[0], banana[1]);

      if (!value.isEmpty()) {
        System.out.print("found " + element + "... ");
      } else {
        missing.add(element);
      }
    }

    if (!missing.isEmpty()) {
      System.out.println("value" + (missing.size() != 1 ? "s" : "") + " missing: " + missing);
      System.out.println(FAILED);
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testCheckRequiredMetadataForFile() throws IOException, ParseException {

    System.out.print("Testing required metadata check for file... ");

    List<String> expectedMissingMetadata = new ArrayList<String>();
    expectedMissingMetadata.add("Required metadata element dc:testitest is missing!");

    Model model = RDFUtils.readModel(TURTLE_DATA, RDFConstants.TURTLE);
    String resource = "http://geobrowser.de.dariah.eu/storage/379852";
    PublishObject po = new PublishObject(resource);

    // Create required item list.
    List<String> requiredFields = new ArrayList<String>();
    requiredFields.add("dc:rights");
    requiredFields.add("dc:title");
    requiredFields.add("dc:testitest");

    System.out.print("looking for " + requiredFields + " in " + resource + "... ");

    List<String> metadataErrors = CheckCollections.checkRequiredMetadata(model, po, requiredFields);

    System.out.print(metadataErrors + "... ");

    if (!metadataErrors.equals(expectedMissingMetadata)) {
      System.out.println(FAILED);
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testCheckRequiredMetadataForFileNEWURIS() throws IOException, ParseException {

    System.out.print("Testing required metadata check for file NEW URIS... ");

    List<String> expectedMissingMetadata = new ArrayList<String>();
    expectedMissingMetadata.add("Required metadata element dc:testitest is missing!");

    Model model = RDFUtils.readModel(TURTLE_DATA_NEW_URIS, RDFConstants.TURTLE);
    String resource = "https://de.dariah.eu/storage/EAEA0-07C5-8340-37B0-0";
    PublishObject po = new PublishObject(resource);

    // Create required item list.
    List<String> requiredFields = new ArrayList<String>();
    requiredFields.add("dc:rights");
    requiredFields.add("dc:title");
    requiredFields.add("dc:testitest");

    System.out.print("looking for " + requiredFields + " in " + resource + "... ");

    List<String> metadataErrors = CheckCollections.checkRequiredMetadata(model, po, requiredFields);

    System.out.print(metadataErrors + "... ");

    if (!metadataErrors.equals(expectedMissingMetadata)) {
      System.out.println(FAILED);
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testCheckDateFields() throws IOException, ParseException {

    System.out.print("Testing date time fields in metadata model... ");

    List<String> expectedErrors = new ArrayList<String>();
    expectedErrors
        .add("Incorrect <dc:date> entry: morgen :-)" + CheckCollections.PLEASE_PROVIDE_DATE);
    expectedErrors
        .add("Incorrect <dc:date> entry: 2017-12-32T23:33:01.122 [Invalid value 32 for Day field.]"
            + CheckCollections.PLEASE_PROVIDE_DATE);
    expectedErrors
        .add("Incorrect <dc:date> entry: 2017-03-12T10:11" + CheckCollections.PLEASE_PROVIDE_DATE);
    expectedErrors
        .add("Incorrect <dc:date> entry: 2017-07.fff" + CheckCollections.PLEASE_PROVIDE_DATE);

    Model model = RDFUtils.readModel(DATE_TIME_RDF, RDFConstants.RDF_XML);
    PublishObject po = new PublishObject();

    List<String> dateErrors = CheckCollections.checkDateFields(model, po, true);

    if (!expectedErrors.equals(dateErrors)) {
      System.out.println(dateErrors);
      System.out.println(FAILED);
      assertTrue(false);
    } else {
      System.out.println(OK);
    }
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   * @throws ParseException
   */
  @Test
  public void testFindingHtmlDelimetersInDcSimple()
      throws IOException, XMLStreamException, ParseException {

    System.out
        .print("Test finding all HTML content in DC Simple terms... ");

    Model model = RDFUtils.readModel(COMPLETE_MODEL, RDFConstants.TURTLE);
    String uri = "https://de.dariah.eu/storage/EAEA0-D291-D132-77AF-0";

    List<String> urgl = CheckCollections.testDCTags4HTMLTagDelimeters(model, uri);

    if (urgl.contains("dc:rights") && urgl.contains("dc:title")) {
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      System.out.println(urgl);
      assertTrue(false);
    }
  }

}
