/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.processstarter.dariahde.publish;

import java.util.Collections;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import de.langzeitarchivierung.kolibri.DHPublishServiceVersion;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.processstarter.PublishAbs;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import de.langzeitarchivierung.kolibri.webservice.dariahde.publish.DHPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.InfoResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2020-12-03 - Funk - Add QUEUED status to newly created status response here. Fixes #33818.
 * 
 * 2016-07-16 - Funk - Using StartDateMap for caching Info Responses.
 * 
 * 2016-02-12 - Funk - Added InfoResponseMap for InfoResponse caching.
 * 
 * 2014-11-10 - Funk - Created file from TGPublish class.
 */

/**
 * <p>
 * <b>The DHPublishService process starter implementation class</b>
 * </p>
 * 
 * <p>
 * This process starter is waiting for CustomData entries in a queue, that will be filled by the
 * DH-publish for every publish webservice call.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-12-03
 * @since 2014-11-10
 */

public class DHPublish extends DHPublishAbs {

  // **
  // STATICS
  // **

  protected static ArrayBlockingQueue<HashMap<String, Object>> dataQueue;
  protected static volatile ConcurrentHashMap<String, InfoResponse> infoResponseMap;
  protected static volatile SortedMap<String, Long> infoStartDateMap;
  protected static int dataQueueCapacity = 1000;
  protected static long recheckDataQueue = 5000;

  // **
  // CONSTRUCTOR
  // **

  /**
   * <p>
   * Constructor creates the data queue, it is ment to be thread-safe.
   * </p>
   */
  public DHPublish() {

    // Call super class.
    super();

    // Create data queue.
    dataQueue = new ArrayBlockingQueue<HashMap<String, Object>>(dataQueueCapacity);

    // Create start date and info response map, if not yet created...
    if (infoResponseMap == null) {
      infoResponseMap = new ConcurrentHashMap<String, InfoResponse>(responseMapCapacity);

      defaultLogger.fine("InfoResponseMap created");
    }
    if (infoStartDateMap == null) {
      infoStartDateMap = Collections.synchronizedSortedMap(new TreeMap<String, Long>());

      defaultLogger.fine("InfoStartDateMap created");
    }
  }

  /**
   * <p>
   * Adds/updates a key/value pair to the info response map, and adds the start date.
   * </p>
   * 
   * @param theUri
   * @param theInfo
   */
  public synchronized static void updateInfoResponseMap(final String theUri,
      final InfoResponse theInfo) {

    // Check if the capacity of the response map has exceeded already, but only if a new response
    // shall be added. If so, delete oldest entry.
    synchronized (DHPublish.class) {

      if (!infoResponseMap.containsKey(theUri) && infoResponseMap.size() == responseMapCapacity) {
        String uriToDelete = infoStartDateMap.firstKey();

        infoStartDateMap.remove(uriToDelete);
        infoResponseMap.remove(uriToDelete);

        defaultLogger
            .finest("Deleted oldest entry from InfoResponse and StartDate map: " + uriToDelete);
      }

      // After checking map size (and eventually deleting), add or update the object.
      infoResponseMap.put(theUri, theInfo);

      defaultLogger.fine("Added " + theUri + " to InfoResponse map");
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#run()
   */
  @Override
  public void run() {

    defaultLogger.log(Level.INFO, "DH-publish#PUBLISH started [" + DHPublishServiceVersion.VERSION
        + "+" + DHPublishServiceVersion.BUILDDATE + "]");

    // TODO Implement re-checking using Listeners!!

    defaultLogger.log(Level.INFO, "Checking #PUBLISH data queue every "
        + KolibriTimestamp.getDurationInHours(recheckDataQueue));

    while (true) {

      defaultLogger.log(Level.FINER, "DataQueue: " + dataQueue.size() + "/" + dataQueueCapacity
          + ", ResponseMap: " + infoResponseMap.size() + "/" + PublishAbs.getResponseMapCapacity());

      // Check data queue size.
      while (!dataQueue.isEmpty()) {

        // Get next customData element.
        HashMap<String, Object> customData = dataQueue.peek();

        // Get the needed data out of the customData.
        String id = (String) customData.get(DHPublishService.STORAGE_ID);
        String policyName = (String) customData.get(DHPublishServiceImpl.POLICY_NAME);
        if (policyName == null || policyName.equals("")) {
          policyName = this.defaultPolicy;
        }
        PublishResponse response =
            (PublishResponse) customData.get(DHPublishServiceImpl.PUBLISH_RESPONSE);

        // Create process data.
        synchronized (this.processQueue) {
          ProcessData pd = null;
          try {
            pd = new ProcessData(policyName, id);

            defaultLogger.log(Level.INFO, "Policy set [" + policyName + "]");

            // Add custom data to processData as customData.
            pd.getCustomData().putAll(customData);

            defaultLogger.log(Level.INFO, "Root collection used [" + id + "]");

            // Add element to process queue.
            this.processQueue.addElement(pd);

            defaultLogger.log(Level.FINE, "Added element [" + id + "] to process queue");

          } catch (Exception e) {
            String message = "Error adding to process queue [" + id + "]";
            defaultLogger.log(Level.SEVERE, message, e);

            // Print stack trace, just in case...
            e.printStackTrace();

            // Set response to error.
            PublishAbs.setErrorResponse(id, response, this.getClass().getName(),
                ErrorType.NOT_SPECIFIED, message);
            // Leave dataQueue!
            break;
          } finally {
            // Remove element from data queue.
            dataQueue.remove();

            defaultLogger.log(Level.FINER, "Removed from data queue [" + id + "]");
          }
        }
      }

      // Wait for the next check.
      try {
        Thread.sleep(recheckDataQueue);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param capacity
   */
  public static void setDataQueueCapacity(int capacity) {
    dataQueueCapacity = capacity;
  }

  /**
   * @param recheck
   */
  public static void setRecheckDataQueue(long recheck) {
    recheckDataQueue = recheck;
  }

  /**
   * @return the array blocking queue
   */
  public static ArrayBlockingQueue<HashMap<String, Object>> getDataQueue() {
    return dataQueue;
  }

  /**
   * <p>
   * Returns an InfoResponse, if the URI is existing, NULL otherwise.
   * </p>
   * 
   * @param theUri
   * @return the info response
   */
  public static InfoResponse getInfoResponse(String theUri) {
    return infoResponseMap.get(theUri);
  }

}
