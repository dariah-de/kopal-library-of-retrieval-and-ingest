/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.processstarter.dariahde.publish;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import de.langzeitarchivierung.kolibri.processstarter.PublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.RedisPool;
import de.langzeitarchivierung.kolibri.util.Configurator;
import info.textgrid.middleware.pid.api.TGPidService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.DHCrudService;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;

/*******************************************************************************
 * TODOLOG
 * 
 *******************************************************************************
 * CHANGELOG
 * 
 * 2017-09-29 Funk Added timeouts for dhcrud and dhpid clients. 2017-09-28 Funk Added statsd
 * information. 2017-01-18 Funk More tokens added. 2016-05-31 Funk Added pid service secret.
 * 2015-03-23 Funk Added DH-pid service. 2014-12-19 Funk Copied from TGPublishAbs.
 *
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * The TGPublishService process starter abstract class.
 * </p>
 * 
 * <p>
 * This process starter is waiting for CustomData entries in a queue, that will be filled by the
 * DH-publish for every publish webservice call.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2017-10-27
 * @since 2011-01-04
 ******************************************************************************/

public abstract class DHPublishAbs extends PublishAbs {

  // **
  // FINALS
  // **

  private static final String USING_EXISTING = "Using existing HTTP client from ";
  private static final String CREATING_NEW = "Creating new HTTP client from ";
  private static final String NEW_CREATED = "New HTTP client created! Timeout is ";

  // **
  // STATICS
  // **

  private static TGPidService dhpidClient = null;
  private static int dhpidClientTimeout = 60000;
  private static DHCrudService dhcrudClient = null;
  private static int dhcrudClientTimeout = 60000;
  private static Client crClient = null;
  private static int crClientTimeout = 5000;
  private static String crEndpoint = "https://dfatest.de.dariah.eu/colreg-ui/";
  public static String crOaiPath = "colreg/oaipmh";
  private static String crSubmitDraftPath = "colreg/collection/submitDraft/";

  private static String pidSecret;
  private static String ownStorageEndpoint = "https://de.dariah.eu/storage/";
  private static String repositoryOaiEndpoint = "https://repository.de.dariah.eu/oaipmh/oai";
  private static String publishSecret = "";
  private static String statsdEndpoint = "localhost";
  private static int statsdPort = 8125;
  // Reset status after one hour of non-changing infoResponse.
  private static long errorTimeout = 3600000;

  // **
  // CLASS VARIABLES
  // **

  private String dhpidEndpoint = null;
  private String dhcrudEndpoint = null;
  private String redisHostname;
  private int redisPort;
  private int redisMaxParallel = 100;
  // Select database (1) here, (0) is used by Publikator!
  private int redisDatabaseNo = 1;

  // **
  // CONSTRUCTOR
  // **

  /**
   * <p>
   * Constructor constructs, e.g. service stubs, and creates the response queue. This should be
   * thread-safe.
   * </p>
   */
  public DHPublishAbs() {

    // Configure this class.
    try {
      Configurator.configure(this);

      // Create CRUD service client and set timeout.
      if (dhcrudClient == null) {
        defaultLogger.log(Level.FINE, CREATING_NEW + this.dhcrudEndpoint);

        dhcrudClient = JAXRSClientFactory.create(this.getDhcrudEndpoint(), DHCrudService.class);
        HTTPConduit conduit = WebClient.getConfig(dhcrudClient).getHttpConduit();
        HTTPClientPolicy policy = new HTTPClientPolicy();
        policy.setReceiveTimeout(dhcrudClientTimeout);
        conduit.setClient(policy);

        defaultLogger.log(Level.FINE,
            NEW_CREATED + dhcrudClientTimeout + " millis (" + dhcrudClient.getVersion() + ")");

      } else {
        defaultLogger.log(Level.FINE,
            USING_EXISTING + this.dhcrudEndpoint + " (" + dhcrudClient.getVersion() + ")");
      }

      // Create PID service client and set timeout.
      if (dhpidClient == null) {
        defaultLogger.log(Level.FINE, CREATING_NEW + this.dhpidEndpoint);

        dhpidClient = JAXRSClientFactory.create(this.getDhpidEndpoint(), TGPidService.class);
        HTTPConduit conduit = WebClient.getConfig(dhpidClient).getHttpConduit();
        HTTPClientPolicy policy = new HTTPClientPolicy();
        policy.setReceiveTimeout(dhpidClientTimeout);
        conduit.setClient(policy);

        defaultLogger.log(Level.FINE,
            NEW_CREATED + dhpidClientTimeout + " millis (" + dhpidClient.version() + ")");

      } else {
        defaultLogger.log(Level.FINE,
            USING_EXISTING + this.dhpidEndpoint + " (" + dhpidClient.version() + ")");
      }

      // Create CR service client and set timeout.
      if (crClient == null) {
        defaultLogger.log(Level.FINE, "Creating new CR client");

        crClient = ClientBuilder.newClient().property("thread.safe.client", "true")
            .property("http.receive.timeout", crClientTimeout);

        defaultLogger.log(Level.FINE, NEW_CREATED + crClientTimeout + " millis");

      } else {
        defaultLogger.log(Level.FINE, "Using existing CR client");
      }

      // Initialise redis pool.
      defaultLogger.log(Level.FINE, "Initialising REDIS pool[" + this.redisDatabaseNo + "]");

      contextInitialized();

      defaultLogger.log(Level.INFO, "REDIS pool initialisation complete");

    } catch (InvocationTargetException | IllegalAccessException
        | IOException e) {
      e.printStackTrace();
      defaultLogger.log(Level.SEVERE, "Could not configure DH-publish", e);
    }
  }

  /**
   * @throws IOException
   * 
   */
  private void contextInitialized() throws IOException {

    // Start REDIS pool.
    RedisPool.getInstance().connect(this.redisHostname, this.redisPort, this.redisMaxParallel,
        this.redisDatabaseNo);

    String message = "REDIS pool instance initialised (maxParallel=" + this.redisMaxParallel + ")";
    defaultLogger.log(Level.INFO, message);

    // Select REDIS database and test if server is reachable.
    String status = "";
    try {
      status = RedisPool.getInstance().getJedis().select(this.redisDatabaseNo);
    } catch (Exception e) {
      e.printStackTrace();
      message = "Connection to REDIS server " + this.redisHostname + ":" + this.redisPort + " db "
          + this.redisDatabaseNo + " failed";
      throw new IOException(message);
    }

    message = "Connection to REDIS server " + this.redisHostname + ":" + this.redisPort + " db "
        + this.redisDatabaseNo + " started (" + status + ")";
    defaultLogger.log(Level.INFO, message);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param dhcrudEndpoint
   */
  public void setDhcrudEndpoint(String dhcrudEndpoint) {
    this.dhcrudEndpoint = dhcrudEndpoint;
  }

  /**
   * @return the dhcrud endpoint
   */
  public String getDhcrudEndpoint() {
    return this.dhcrudEndpoint;
  }

  /**
   * @return the dhcrud service
   */
  public static DHCrudService getCrudService() {
    return dhcrudClient;
  }

  /**
   * @param dhpidEndpoint
   */
  public void setDhpidEndpoint(String dhpidEndpoint) {
    this.dhpidEndpoint = dhpidEndpoint;
  }

  /**
   * @return the dhpid endpoint
   */
  public String getDhpidEndpoint() {
    return this.dhpidEndpoint;
  }

  /**
   * @return the pid service
   */
  public static TGPidService getPidService() {
    return dhpidClient;
  }

  /**
   * @return the cr client
   */
  public static Client getCRClient() {
    return crClient;
  }

  /**
   * @param theUrl
   */
  public static void setOwnStorageEndpoint(String theUrl) {
    ownStorageEndpoint = theUrl;
  }

  /**
   * @return the OwnStorage endpoint
   */
  public static String getOwnStorageEndpoint() {
    return ownStorageEndpoint;
  }

  /**
   * @param theUrl
   */
  public static void setRepositoryOaiEndpoint(String theUrl) {
    repositoryOaiEndpoint = theUrl;
  }

  /**
   * @return the repository OAI endpoint
   */
  public static String getRepositoryOaiEndpoint() {
    return repositoryOaiEndpoint;
  }

  /**
   * @param theSecret
   */
  public static void setPidSecret(String theSecret) {
    pidSecret = theSecret;
  }

  /**
   * @return the PID secret
   */
  public static String getPidSecret() {
    return pidSecret;
  }

  /**
   * @param theHostname
   */
  public void setRedisHostname(String theHostname) {
    this.redisHostname = theHostname;
  }

  /**
   * @return the Redis hostname
   */
  public String getRedisHostname() {
    return this.redisHostname;
  }

  /**
   * @param thePort
   */
  public void setRedisPort(int thePort) {
    this.redisPort = thePort;
  }

  /**
   * @return the Redis port
   */
  public int getRedisPort() {
    return this.redisPort;
  }

  /**
   * @param theMax
   */
  public void setRedisMaxParallel(int theMax) {
    this.redisMaxParallel = theMax;
  }

  /**
   * @return the maximal redis parallel count
   */
  public int getRedisMaxParallel() {
    return this.redisMaxParallel;
  }

  /**
   * @param theNo
   */
  public void setRedisDatabaseNo(int theNo) {
    this.redisDatabaseNo = theNo;
  }

  /**
   * @return the database no
   */
  public int getRedisDatabaseNo() {
    return this.redisDatabaseNo;
  }

  /**
   * @param publishSecret
   */
  public static void setPublishSecret(String theSecret) {
    publishSecret = theSecret;
  }

  /**
   * @return the publish secret
   */
  public static String getPublishSecret() {
    return publishSecret;
  }

  /**
   * @param statsdEndpoint
   */
  public static void setStatsdEndpoint(String theEndpoint) {
    statsdEndpoint = theEndpoint;
  }

  /**
   * @return the statsd endpoint
   */
  public static String getStatsdEndpoint() {
    return statsdEndpoint;
  }

  /**
   * @param statsdPort
   */
  public static void setStatsdPort(int thePort) {
    statsdPort = thePort;
  }

  /**
   * @return the statsd port
   */
  public static int getStatsdPort() {
    return statsdPort;
  }

  /**
   * @return the dhpid client timeout
   */
  public static int getDhpidClientTimeout() {
    return dhpidClientTimeout;
  }

  /**
   * @param dhpidClientTimeout
   */
  public static void setDhpidClientTimeout(int dhpidClientTimeout) {
    DHPublishAbs.dhpidClientTimeout = dhpidClientTimeout;
  }

  /**
   * @return the dhcrud client timeout
   */
  public static int getDhcrudClientTimeout() {
    return dhcrudClientTimeout;
  }

  /**
   * @param dhcrudClientTimeout
   */
  public static void setDhcrudClientTimeout(int dhcrudClientTimeout) {
    DHPublishAbs.dhcrudClientTimeout = dhcrudClientTimeout;
  }

  /**
   * @return the CR endpoint
   */
  public static String getCrEndpoint() {
    return crEndpoint;
  }

  /**
   * @param crEndpoint
   */
  public static void setCrEndpoint(String crEndpoint) {
    DHPublishAbs.crEndpoint = crEndpoint;
  }

  /**
   * @return the CR OAI path
   */
  public static String getCrOaiPath() {
    return crOaiPath;
  }

  /**
   * @param crOaiPath
   */
  public static void setCrOaiPath(String crOaiPath) {
    DHPublishAbs.crOaiPath = crOaiPath;
  }

  /**
   * @return the CR submit draft path
   */
  public static String getCrSubmitDraftPath() {
    return crSubmitDraftPath;
  }

  /**
   * @param crSubmitDraftPath
   */
  public static void setCrSubmitDraftPath(String crSubmitDraftPath) {
    DHPublishAbs.crSubmitDraftPath = crSubmitDraftPath;
  }

  /**
   * @return the CR client timeout
   */
  public static int getCrClientTimeout() {
    return crClientTimeout;
  }

  /**
   * @param crClientTimeout
   */
  public static void setCrClientTimeout(int crClientTimeout) {
    DHPublishAbs.crClientTimeout = crClientTimeout;
  }

  /**
   * @return the error timeout
   */
  public static long getErrorTimeout() {
    return errorTimeout;
  }

  /**
   * @param errorTimeout
   */
  public static void setErrorTimeout(long timeout) {
    errorTimeout = timeout;
  }

}
