/**
 * This software is copyright (c) 2021 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import org.apache.jena.rdf.model.Model;
import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.dariahde.publish.DHPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.dariahde.publish.DHPublishServiceImpl;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import jakarta.activation.DataHandler;
import jakarta.mail.util.ByteArrayDataSource;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * <p>
 * Create collections from overall RDF data model.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-08-24
 * @since 2014-12-19
 */

public class CreateCollections implements ActionModule {

  // **
  // STATIC FINAL
  // **

  private static final boolean INCREASE_PROGRESS = true;
  private static final boolean IS_ROOT_COLLECTION = true;
  private static final boolean IS_NOT_ROOT_COLLECTION = false;
  private static final String COLLECTION_CREATION_FAILED = "Creating collection failed due to a ";

  // **
  // STATIC
  // **

  private static final StatsDClient statsd = new NonBlockingStatsDClient("dhpublish",
      DHPublishAbs.getStatsdEndpoint(), DHPublishAbs.getStatsdPort());

  // **
  // STATE
  // **

  private ProcessData processData;
  private Step step;
  private boolean dryRun;

  // **
  // MANIPULATION
  // **

  /**
   * 
   * 
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>The overall RDF model including all the PIDs for each object</li>
   * <li>The PublishResponse of the root collection containing all the objects</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>Creates a new collection for every collection object in the overall RDF model</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go() xxxxx
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(DHPublishService.STORAGE_ID);
    String eppn = (String) customData.get(DHPublishServiceImpl.DARIAH_EPPN);
    String collectionLogID = (String) customData.get(DHPublishService.TRANSACTION_ID);
    String storageToken = (String) customData.get(DHPublishService.STORAGE_TOKEN);
    String seafileToken = (String) customData.get(DHPublishService.SEAFILE_TOKEN);
    PublishResponse publishResponse =
        (PublishResponse) customData.get(DHPublishServiceImpl.PUBLISH_RESPONSE);
    Model model = (Model) customData.get(uri);
    this.dryRun = publishResponse.dryRun;

    // Get collection log ID (if existing).
    String coLogIDString = DHPublishUtils.getLogIDString(collectionLogID);

    // Start log entry. Set message also for current task info.
    String message = coLogIDString + "Collections' data and metadata are being created";
    PublishUtils.setStatusRunning(this.step, message, publishResponse, this, message, this.dryRun);

    // Get root collection PID (for dc:relation metadata tag).
    String rootCollectionPid = DHPublishUtils.getAdministrativePid(model,
        uri);

    // Loop over all the collections in the PublishResponse.
    int collectionCount = 0;
    for (PublishObject po : publishResponse.getPublishObjects()) {

      // Generate object specific log IDs.
      String poLogID = DHPublishUtils.createCombinedLogID(collectionLogID);
      String poLogIDString = DHPublishUtils.getLogIDString(poLogID);
      String loguri = poLogIDString + "[" + DHPublishUtils.getStorageId(po.uri) + "] ";

      if (RDFUtils.isCollection(model, po.uri)) {

        collectionCount++;

        // Get PID (administrative Handle) of current object (without PID prefix!).
        String handle = DHPublishUtils.getAdministrativePid(model, po.uri);

        // Check for PID, just to be sure!
        if (handle == null || handle.equals("")) {
          message = loguri
              + "Collection's administrative PID must not be NULL or empty! This is an internal error!";
          PublishUtils.addGeneralError(this.step, ErrorType.NOT_SPECIFIED, message, publishResponse,
              this, INCREASE_PROGRESS, this.dryRun);
          PublishUtils.updateResponse(po.uri, publishResponse);
          return;
        }

        String task = "Collection " + collectionCount + ": found [" + handle + "]";
        message = loguri + task;
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, task, this.dryRun);

        // Enrich collection metadata and get model for collection.
        Model metadata = DHPublishUtils.createCollectionMetadata(model, handle, po.uri);

        task = "Collection " + collectionCount + ": Metadata creation complete";
        message = loguri + task;
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, task, this.dryRun);

        // DEBUG OUTPUT.
        message = loguri + "METADATA:\n" + RDFUtils.getTTLFromModel(metadata);
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);
        // DEBUG OUTPUT.

        // Create data (collection data model) for collection.
        Model data = null;
        try {
          data = createCollection(model, po.uri, handle, publishResponse, poLogID);

          task = "Collection " + collectionCount + ": Data file creation complete";
          message = loguri + task;
          PublishUtils.setStatusRunning(this.step, message, publishResponse, this, task,
              this.dryRun);

        } catch (XMLStreamException | IOException | URISyntaxException e) {
          message = poLogIDString + COLLECTION_CREATION_FAILED + e.getClass().getName() + "]: "
              + e.getMessage();
          PublishUtils.addGeneralError(this.step, ErrorType.NOT_SPECIFIED, message, publishResponse,
              this, INCREASE_PROGRESS, this.dryRun);
          PublishUtils.updateResponse(po.uri, publishResponse);
          return;
        }

        // Store collection data and metadata to DH-crud (PublicStorage).
        if (!this.dryRun) {
          try {
            storeCollection(model, po.uri, handle, publishResponse, seafileToken, storageToken,
                data, metadata, eppn, rootCollectionPid, poLogID);

            task = "Collection " + collectionCount + ": Publishing complete";
            message = loguri + task;
            PublishUtils.setStatusRunning(this.step, message, publishResponse, this, task,
                this.dryRun);

          } catch (IOException | XMLStreamException | URISyntaxException e) {
            message = loguri + COLLECTION_CREATION_FAILED + e.getClass().getName() + ": "
                + e.getMessage();
            PublishUtils.addGeneralError(this.step, ErrorType.NOT_SPECIFIED, message,
                publishResponse, this, INCREASE_PROGRESS, this.dryRun);
            PublishUtils.updateResponse(po.uri, publishResponse);
            return;
          }
        }

        // TODO REMOVE DEBUG OUTPUT.
        message = loguri + "FINAL METADATA:\n" + RDFUtils.getTTLFromModel(model);
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);
        // TODO REMOVE DEBUG OUTPUT.

        task = "Collection " + collectionCount + ": Collection created";
        message = loguri + task;
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, task, this.dryRun);

        // Increase statsd COLLECRTIONS counter!
        statsd.incrementCounter("collections-submitted");

        message = loguri + "Incremented statsd COLLECTIONS counter";
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, message,
            this.dryRun);

      }
    }

    // Set module to status DONE.
    message = coLogIDString + "Collections' data and metadata creation complete";
    PublishUtils.setStatusDone(this.step, message, publishResponse, this, this.dryRun);

    // Finally write publish responses back to custom data.
    PublishUtils.updateResponse(uri, publishResponse);
  }

  // **
  // INTERNAL METHODS
  // **

  /**
   * <p>
   * Create the ORE data file string for a collection.
   * </p>
   * 
   * @param theModel
   * @param theUri
   * @param thePid
   * @param theResponse
   * @param theLogID
   * @return
   * @throws XMLStreamException
   * @throws IOException
   * @throws URISyntaxException
   */
  private Model createCollection(final Model theModel, final String theUri, String thePid,
      PublishResponse theResponse, String theLogID)
      throws XMLStreamException, IOException, URISyntaxException {

    Model result = null;

    // Get all PIDs of parts from the overall RDF model (non-recursively!).
    List<URI> pids = DHPublishUtils.getHasPartPIDsFromRdfModel(theModel, theUri,
        RDFConstants.DCTERMS_PREFIX, RDFConstants.ELEM_DC_IDENTIFIER);

    // Then put together the parts as a new ORE file (DARIAH-DE/TextGrid collection ORE file).
    result = DHPublishUtils.createCollectionData(pids, thePid);

    // TODO REMOVE DEBUG OUTPUT!!
    String message = theLogID + "DATA:\n" + RDFUtils.getTTLFromModel(result);
    PublishUtils.setStatusRunning(this.step, message, theResponse, this, this.dryRun);
    // TODO REMOVE DEBUG OUTPUT!!

    return result;
  }

  /**
   * <p>
   * Store the collection data and metadata to PublicStorage.
   * </p>
   * 
   * @param theModel
   * @param theUri
   * @param thePid
   * @param theResponse
   * @param theSeafileToken
   * @param theStorageToken
   * @param theData
   * @param theMetadata
   * @param theEppn
   * @param theRootCollectionPid
   * @throws IOException
   * @throws XMLStreamException
   * @throws URISyntaxException
   */
  private void storeCollection(final Model theModel, final String theUri, final String thePid,
      PublishResponse theResponse, final String theSeafileToken, final String theStorageToken,
      final Model theData, final Model theMetadata, final String theEppn,
      final String theRootCollectionPid, String theLogID)
      throws IOException, XMLStreamException, URISyntaxException {

    // Create data handlers for collection metadata and metadata string.
    DataHandler binaryMetadata =
        new DataHandler(new ByteArrayDataSource(RDFUtils.getTTLFromModel(theMetadata),
            MediaType.APPLICATION_OCTET_STREAM));
    DataHandler binaryData =
        new DataHandler(new ByteArrayDataSource(RDFUtils.getTTLFromModel(theData),
            MediaType.APPLICATION_OCTET_STREAM));

    // Create collection using DH-crud.
    Response createResponse = DHPublishAbs.getCrudService().createBinary(binaryMetadata, binaryData,
        URI.create(thePid), theStorageToken, theSeafileToken, theLogID);

    int statusCode = createResponse.getStatus();
    String reasonPhrase = createResponse.getStatusInfo().getReasonPhrase();

    if (statusCode != 200) {
      throw new IOException("Error creating data via DH-crud: " + statusCode + " " + reasonPhrase);
    }

    String location = createResponse.getLocation().toString();
    String message = "DH-crud#CREATE complete [" + (location == null ? "NULL" : location) + "]";
    PublishUtils.setStatusRunning(this.step, message, theResponse, this, this.dryRun);

    // Add location to the RDF model for storing collection's PID as dc:relation into each object's
    // metadata, use root collection PID here recursively for all objects!
    storeRootCollectionPIDToRDFModel(theModel, theRootCollectionPid, theUri, IS_ROOT_COLLECTION);

    // Update model on custom data.
    this.processData.getCustomData().put(theUri, theModel);
  }

  /**
   * <p>
   * Store root collection PID to overall model as <dariahstorage:269002> <dc:relation>
   * <dariah:Collection:0000-0000-0000-0XYZ-A>.
   * </p>
   * 
   * @param theModel
   * @param theRootCollectionPid
   * @param theResource
   * @param isRootCollection
   * @throws IOException
   * @throws XMLStreamException
   */
  protected static void storeRootCollectionPIDToRDFModel(Model theModel,
      final String theRootCollectionPid, final String theResource, final boolean isRootCollection)
      throws IOException, XMLStreamException {

    String rootCollectionEntry = DHPublishUtils.getRootCollectionEntry(theRootCollectionPid);

    // Add dc:relation for non-root collection element!
    if (!isRootCollection) {
      RDFUtils.addResourceToModel(theModel, theResource, RDFConstants.DC_PREFIX,
          RDFConstants.ELEM_DC_RELATION, theModel.createResource(rootCollectionEntry));
    }

    // Check for subcollections.
    List<String> hasParts =
        RDFUtils.getProperties(theModel, theResource, RDFConstants.DCTERMS_PREFIX,
            RDFConstants.ELEM_DCTERMS_HASPART);

    // Loop over all parts and add root collection as dc:relation.
    if (!hasParts.isEmpty()) {
      for (String str : hasParts) {
        // Add PID with collection prefix as dc:relation.
        RDFUtils.addResourceToModel(theModel, str, RDFConstants.DC_PREFIX,
            RDFConstants.ELEM_DC_RELATION,
            theModel.createResource(rootCollectionEntry));
        // If part is collection, do recurse...
        if (RDFUtils.isCollection(theModel, str)) {
          storeRootCollectionPIDToRDFModel(theModel, theRootCollectionPid, str,
              IS_NOT_ROOT_COLLECTION);
        }
      }
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

}
