/*******************************************************************************
 * This software is copyright (c) 2017 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import org.apache.jena.rdf.model.Model;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.dariahde.publish.DHPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.dariahde.publish.DHPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/*******************************************************************************
 * <p>
 * Get a PID for every object to store, get PIDs for collections, too.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2017-11-22
 * @since 2014-12-19
 ******************************************************************************/

public class GetPids implements ActionModule {

  // **
  // FINALS
  // **

  private static final String ERROR_GETTING_PIDS = "Error getting PIDs from DH-crud: ";

  // **
  // STATE
  // **

  private ProcessData processData;
  private Step step;
  private boolean dryRun;
  private int howManyAtOnce = 100;
  private String collectionLogID = "";

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>The overall RDF model for reading and then checking the given metadata</li>
   * <li>The PublishResponse of the root collection containing all the objects</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>Handle PIDs are generated for every item in the PublishResponse and are put back into the
   * overall RDF model for further use (both put back into custom data)</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(DHPublishService.STORAGE_ID);
    // String eppn = (String) customData.get(DHPublishService.DARIAH_EPPN);
    String collectionLogID = (String) customData
        .get(DHPublishService.TRANSACTION_ID);
    PublishResponse publishResponse = (PublishResponse) customData
        .get(DHPublishServiceImpl.PUBLISH_RESPONSE);
    this.dryRun = publishResponse.dryRun;
    Model model = (Model) customData.get(uri);

    // Get collection log ID (if existing).
    String coLogID = DHPublishUtils.getLogIDString(collectionLogID);

    // Start log entry. Set message also for current task info. How many
    // PIDs do we need in how many steps and what is the rest?
    int objectAmount = publishResponse.getPublishObjects().size();
    int rest = objectAmount % this.howManyAtOnce;

    String message = coLogID + "Requesting " + objectAmount + " PID"
        + (objectAmount != 1 ? "s" : "") + " in steps of "
        + this.howManyAtOnce + " from DH-crud service [rest is " + rest
        + "]";
    PublishUtils.setStatusRunning(this.step, message, publishResponse, this,
        this.dryRun);

    // Get progress. This module has to cover and compute in how many steps
    // we can divide this module (use float for computation for small
    // values!).
    float moduleProgress = publishResponse.getPublishStatus().progress;
    float decimator = 1;
    if (objectAmount > this.howManyAtOnce) {
      decimator = (float) objectAmount / (float) this.howManyAtOnce;
    }
    float stepProgress = (float) PublishUtils
        .progressPerModule(this.processData) / decimator;

    // Check DH-crud version only in dry-run mode.
    if (this.dryRun) {
      String dhcrudVersion = DHPublishAbs.getCrudService().getVersion();

      message = coLogID + "Just checking DH-crud#VERSION ["
          + dhcrudVersion + "]";
      PublishUtils.setStatusRunning(this.step, message, publishResponse,
          this, this.dryRun);

      // Fail, if DH-crud not accessible!
      if (dhcrudVersion == null || dhcrudVersion.isEmpty()) {
        message = coLogID + ERROR_GETTING_PIDS
            + "DH-crud not available!";
        PublishUtils.addGeneralError(this.step, ErrorType.NOT_SPECIFIED,
            message, publishResponse, this, this.dryRun);
        PublishUtils.updateResponse(uri, publishResponse);
        return;
      }

    } else {

      List<URI> pids = new ArrayList<URI>();
      try {
        // Get PIDs for collection and every single object from DH-crud,
        // build list from response.
        for (int i = 0; i < objectAmount; i = i + this.howManyAtOnce) {
          if (i + this.howManyAtOnce > objectAmount) {

            // Set progress per object.
            moduleProgress += stepProgress;
            PublishUtils.setProgress(publishResponse,
                (int) moduleProgress);

            String task = "Requesting PIDs " + (i + 1) + " to "
                + (i + rest) + " of " + objectAmount;
            message = coLogID + task + " ("
                + publishResponse.getPublishStatus().progress
                + "%)";
            PublishUtils.setStatusRunning(this.step, message,
                publishResponse, this, task, this.dryRun);

            // Get PIDs.
            pids.addAll(
                DHPublishUtils.httpResponseInputStreamToURIList(
                    DHPublishAbs.getCrudService().getPids(
                        rest, this.collectionLogID)));

          } else {

            // Set progress per object.
            moduleProgress += stepProgress;
            PublishUtils.setProgress(publishResponse,
                (int) moduleProgress);

            String task = "Requesting PIDs " + (i + 1) + " to "
                + (i + this.howManyAtOnce) + " of "
                + objectAmount;
            message = coLogID + task + " ("
                + publishResponse.getPublishStatus().progress
                + "%)";
            PublishUtils.setStatusRunning(this.step, message,
                publishResponse, this, task, this.dryRun);

            // Get rest PIDs.
            pids.addAll(
                DHPublishUtils.httpResponseInputStreamToURIList(
                    DHPublishAbs.getCrudService().getPids(
                        this.howManyAtOnce,
                        this.collectionLogID)));
          }
        }

        // Check if we got all PIDs we do need.
        if (objectAmount != pids.size()) {
          message = coLogID + "PID amount mismatch (" + objectAmount
              + " != " + pids.size() + ")";
          throw new IOException(message);
        }

        message = coLogID + "DH-crud#GETPIDS complete (" + pids.size()
            + " PID" + (pids.size() != 1 ? "s" : "") + " created)";
        PublishUtils.setStatusRunning(this.step, message,
            publishResponse, this, this.dryRun);

        message = coLogID + "Storing PIDs to overall RDF model";
        PublishUtils.setStatusRunning(this.step, message,
            publishResponse, this, this.dryRun);

        // Add HDL prefix and namespace to overall model.
        model.setNsPrefix(RDFConstants.HDL_PREFIX, RDFConstants.HDL_NAMESPACE);
        // Put PIDs in overall model.
        addPidsToModel(model, pids, uri);

      } catch (IOException | XMLStreamException | URISyntaxException e) {
        message = coLogID + ERROR_GETTING_PIDS + e.getMessage();
        PublishUtils.addGeneralError(this.step, ErrorType.NOT_SPECIFIED,
            message, publishResponse, this, this.dryRun);
        PublishUtils.updateResponse(uri, publishResponse);
        return;
      }
    }

    // Update model on custom data.
    this.processData.getCustomData().put(uri, model);

    // TODO REMOVE DEBUG OUTPUT!!
    message = coLogID + "FINAL METADATA CONTAINING ALL THE PIDS:\n"
        + RDFUtils.getTTLFromModel(model);
    PublishUtils.setStatusRunning(this.step, message, publishResponse, this,
        this.dryRun);
    // TODO REMOVE DEBUG OUTPUT!!

    // Set module to status DONE.
    message = coLogID + "PID creation complete";
    PublishUtils.setStatusDoneNoProgressIncrease(this.step, message,
        publishResponse, this, this.dryRun);

    // Finally write publish responses back to custom data.
    PublishUtils.updateResponse(uri, publishResponse);
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Store PIDs to overall model as <dariahstorage:269002> <dcterms:identifier>
   * <hdl:0000-0000-0000-0XYZ-A>. We need the PIDs in the overall model for further DH-publish
   * usage!
   * </p>
   * 
   * @param theModel
   * @param thePids
   * @param theResource
   * @throws XMLStreamException
   * @throws IOException
   * @throws URISyntaxException
   */
  protected static void addPidsToModel(Model theModel,
      final List<URI> thePids, final String theResource)
      throws XMLStreamException, IOException, URISyntaxException {

    // Add PID as RDF resource to RDF model (use resource with namespace).
    String pid = LTPUtils.omitHdlPrefix(thePids.remove(0).toString());

    // Add dcterms:identifier for further crud usage. This is our
    // administrative PID used by DH-crud!
    RDFUtils.addResourceToModel(theModel, theResource, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER, theModel.createResource(LTPUtils.resolveHandlePid(pid)));
    // Add dc:identifier for descriptive metadata usage.
    RDFUtils.addResourceToModel(theModel, theResource, RDFConstants.DC_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER, theModel.createResource(LTPUtils.resolveHandlePid(pid)));

    // Check for subcollections.
    List<String> hasParts = RDFUtils.getProperties(theModel, theResource,
        RDFConstants.DCTERMS_PREFIX, RDFConstants.ELEM_DCTERMS_HASPART);

    // Loop over all parts and recurse if existing.
    if (!hasParts.isEmpty()) {
      for (String str : hasParts) {
        addPidsToModel(theModel, thePids, str);
      }
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

}
