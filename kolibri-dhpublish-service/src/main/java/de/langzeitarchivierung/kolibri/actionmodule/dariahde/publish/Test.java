package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import jakarta.activation.DataHandler;
import jakarta.activation.URLDataSource;
import jakarta.mail.util.ByteArrayDataSource;
import jakarta.ws.rs.ProcessingException;
import jakarta.ws.rs.core.Response;

/**
 * @author fugu
 */
public class Test {

  private static final String RDF = "<rdf:RDF\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:skos=\"http://www.w3.org/2004/02/skos/core#\"\n"
      + "    xmlns:seafile=\"https://sftest.de.dariah.eu/files/\"\n"
      + "    xmlns:tgforms=\"http://www.tgforms.de/terms#\"\n"
      + "    xmlns:owl=\"http://www.w3.org/2002/07/owl#\"\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
      + "    xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"\n"
      + "    xmlns:dariahstorage=\"http://geobrowser.de.dariah.eu/storage/\"\n"
      + "    xmlns:dcam=\"http://purl.org/dc/dcam/\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\"\n"
      + "    xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\" > \n"
      + "  <rdf:Description rdf:about=\"http://geobrowser.de.dariah.eu/storage/302552\">\n"
      + "    <dc:relation>dariah:collection:11022/0000-0000-9A1E-2</dc:relation>\n"
      + "    <dcterms:identifier>11022/0000-0000-9A17-9</dcterms:identifier>\n"
      + "    <dc:title>LordChaos.gif</dc:title>\n"
      + "    <dc:format>image/gif</dc:format>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"http://geobrowser.de.dariah.eu/storage/302502\">\n"
      + "    <dc:relation>dariah:collection:11022/0000-0000-9A1E-2</dc:relation>\n"
      + "    <dcterms:identifier>11022/0000-0000-9A1B-5</dcterms:identifier>\n"
      + "    <dc:title>BlackFlame.gif</dc:title>\n"
      + "    <dc:format>image/gif</dc:format>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"http://geobrowser.de.dariah.eu/storage/302553\">\n"
      + "    <dc:relation>dariah:collection:11022/0000-0000-9A1E-2</dc:relation>\n"
      + "    <dcterms:identifier>11022/0000-0000-9A19-7</dcterms:identifier>\n"
      + "    <dc:title>Oitu.gif</dc:title>\n"
      + "    <dc:format>image/gif</dc:format>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"http://geobrowser.de.dariah.eu/storage/302501\">\n"
      + "    <dcterms:hasPart rdf:resource=\"http://geobrowser.de.dariah.eu/storage/302506\"/>\n"
      + "    <dc:identifier>hdl:11022/0000-0000-9A1E-2</dc:identifier>\n"
      + "    <dcterms:hasPart rdf:resource=\"http://geobrowser.de.dariah.eu/storage/302552\"/>\n"
      + "    <dcterms:format>text/tg.collection+tg.aggregation+xml</dcterms:format>\n"
      + "    <dcterms:hasPart rdf:resource=\"http://geobrowser.de.dariah.eu/storage/302505\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"http://geobrowser.de.dariah.eu/storage/302553\"/>\n"
      + "    <dc:format>text/tg.collection+tg.aggregation+xml</dc:format>\n"
      + "    <dcterms:hasPart rdf:resource=\"http://geobrowser.de.dariah.eu/storage/302504\"/>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>\n"
      + "    <dcterms:hasPart rdf:resource=\"http://geobrowser.de.dariah.eu/storage/302502\"/>\n"
      + "    <dc:description>hufugu??</dc:description>\n"
      + "    <dc:title>hugo</dc:title>\n"
      + "    <dc:creator>fu</dc:creator>\n"
      + "    <dcterms:hasPart rdf:resource=\"http://geobrowser.de.dariah.eu/storage/302503\"/>\n"
      + "    <dcterms:identifier>11022/0000-0000-9A15-B</dcterms:identifier>\n"
      + "    <dcterms:hasPart rdf:resource=\"http://geobrowser.de.dariah.eu/storage/302551\"/>\n"
      + "    <dcterms:source>http://geobrowser.de.dariah.eu/storage/302501</dcterms:source>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"http://geobrowser.de.dariah.eu/storage/302551\">\n"
      + "    <dc:relation>dariah:collection:11022/0000-0000-9A1E-2</dc:relation>\n"
      + "    <dcterms:identifier>11022/0000-0000-9A1D-3</dcterms:identifier>\n"
      + "    <dc:title>LordOrder.gif</dc:title>\n"
      + "    <dc:format>image/gif</dc:format>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"http://geobrowser.de.dariah.eu/storage/302503\">\n"
      + "    <dc:relation>dariah:collection:11022/0000-0000-9A1E-2</dc:relation>\n"
      + "    <dcterms:identifier>11022/0000-0000-9A1C-4</dcterms:identifier>\n"
      + "    <dc:title>AnimatedArmour.gif</dc:title>\n"
      + "    <dc:format>image/gif</dc:format>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"http://geobrowser.de.dariah.eu/storage/302506\">\n"
      + "    <dc:relation>dariah:collection:11022/0000-0000-9A1E-2</dc:relation>\n"
      + "    <dcterms:identifier>11022/0000-0000-9A16-A</dcterms:identifier>\n"
      + "    <dc:title>Materializer.gif</dc:title>\n"
      + "    <dc:format>image/gif</dc:format>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"http://geobrowser.de.dariah.eu/storage/302505\">\n"
      + "    <dc:relation>dariah:collection:11022/0000-0000-9A1E-2</dc:relation>\n"
      + "    <dcterms:identifier>11022/0000-0000-9A18-8</dcterms:identifier>\n"
      + "    <dc:title>GreyLord.gif</dc:title>\n"
      + "    <dc:format>image/gif</dc:format>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n"
      + "  <rdf:Description rdf:about=\"http://geobrowser.de.dariah.eu/storage/302504\">\n"
      + "    <dc:relation>dariah:collection:11022/0000-0000-9A1E-2</dc:relation>\n"
      + "    <dcterms:identifier>11022/0000-0000-9A1A-6</dcterms:identifier>\n"
      + "    <dc:title>Ghost.gif</dc:title>\n"
      + "    <dc:format>image/gif</dc:format>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>\n" + "\n";
  private static final String DC_PREFIX = "dc";
  private static final String DCTERMS_PREFIX = "dcterms";
  private static final String DARIAH_PREFIX = "dariah";

  public static void main(String[] args) throws IOException, JSONException {

    String crPath = "colreg/oaipmh";
    String q1 = "verb", q2 = "metadataPrefix", q3 = "identifier";
    String v1 = "GetRecord", v2 = "dcddm", v3 = "5a16da557c8dec05c8ad990d";

    System.out.println(
        "CR query: " + "http://demo2.dariah.eu/" + crPath + "?" + q1
            + "=" + v1 + "&" + q2 + "=" + v2 + "&" + q3 + "=" + v3);

    // Create CR service client and set timeout.
    WebClient crClient = WebClient.create("http://demo2.dariah.eu/", true);
    HTTPConduit conduit = WebClient.getConfig(crClient).getHttpConduit();
    HTTPClientPolicy policy = new HTTPClientPolicy();
    policy.setReceiveTimeout(2);
    conduit.setClient(policy);

    System.out.println("Calling " + "http://demo2.dariah.eu/"
        + "colreg/colreg/collection/submitDraft/");

    try {
      // Get response from HTTP client.
      Response response = crClient
          .path("colreg/colreg/collection/submitDraft/")
          .post("<rdf:RDF\n"
              + " xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
              + " xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"
              + " <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-A6EE-4315-2CCE-0\">\n"
              + "<dc:identifier>http://trep.de.dariah.eu/1.0/oaipmh/oai/21.T11991%2F0000-0005-6CC6-3</dc:identifier>\n"
              + " <dc:creator>fu</dc:creator>\n"
              + " <dc:identifier>doi:10.5072/0000-0005-6CC6-3</dc:identifier>\n"
              + " <dc:title>Die Neue!</dc:title>\n"
              + " <dc:rights>free</dc:rights>\n"
              + " <dc:contributor>StefanFunk@dariah.eu</dc:contributor>\n"
              + " <dc:format>text/vnd.dariah.dhrep.collection+turtle</dc:format>\n"
              + " <dc:identifier>hdl:21.T11991/0000-0005-6CC6-3</dc:identifier>\n"
              + " </rdf:Description>\n" + "</rdf:RDF>");
      int statusCode = response.getStatus();
      String reasonPhrase = response.getStatusInfo().getReasonPhrase();

      System.out.println("CR says: " + statusCode + " " + reasonPhrase);

    } catch (ProcessingException e) {
      System.out.println("URGLI! " + e.getMessage());
    }

    System.exit(0);

    String pidServiceResponse =
        "{\"responseCode\":1,\"handle\":\"11022/0000-0000-8487-2\",\"values\":[{\"index\":14,\"type\":\"COLREG_ID\""
            + ",\"data\":{\"format\":\"string\",\"value\":\"5746ff8a7c8dec05e679dece\"},\"ttl\":86400,\"timestamp\":\"2015-08-18T13:11:56Z\"}]}";

    System.out.println(pidServiceResponse);

    JSONObject jo = new JSONObject(pidServiceResponse);
    System.out.println(jo.names());
    if (jo.has("values")) {
      JSONArray ja = jo.getJSONArray("values");
      System.out.println(ja);
      for (int i = 0; i < ja.length(); i++) {
        System.out.println(ja.getJSONObject(i).names());
        if (ja.getJSONObject(i).has("type") && ja.getJSONObject(i)
            .get("type").equals("COLREG_ID")) {
          System.out.println(ja.getJSONObject(i).getJSONObject("data")
              .getString("value"));
        }
      }
    }

    // Model m = RDFUtils.readModel(RDF);
    // m.write(System.out, "TURTLE");

    // DHCrudService dhcrud = JAXRSClientFactory.create(
    // "http://box:9093/dhcrud", DHCrudService.class);
    //
    // System.out.println("Creating NEW dhcrud: " + dhcrud.getVersion());
    //
    // Response arglResponse = dhcrud.getPids("fugu", 1);
    //
    // System.out.println(DHPublishUtils
    // .httpResponseInputStreamToURIList(arglResponse));

    // test();
  }

  /**
   * @throws IOException
   */
  private static void test() throws IOException {
    String url = "http://geobrowser.de.dariah.eu/storage/208802";

    // Build up JENA RDF object from turtle file.
    Model model = ModelFactory.createDefaultModel();
    model.read(url, "TURTLE");

    // Set used namespaces.
    String dctermsNamespace = model.getNsPrefixURI(DCTERMS_PREFIX);
    String dcNamespace = model.getNsPrefixURI(DC_PREFIX);

    // Sysout the model in TURTLE.
    System.out.println("-- Turtle --");
    model.write(System.out, "TURTLE");

    System.out.println();

    // Sysout the model in RDF XML.
    System.out.println("-- RDF XML --");
    model.write(System.out, "RDF/XML");

    // Get out the collection's title only.
    Resource res = model.getResource(url);
    Property p = model.createProperty(dcNamespace, "title");

    System.out.println();
    System.out.println("-- " + url + " title --");

    StmtIterator iter = res.listProperties(p);
    while (iter.hasNext()) {
      System.out.println(iter.next().getObject());
    }

    // Get out the collection's title only.
    res = model.getResource(url);
    p = model.createProperty(dcNamespace, "format");

    System.out.println();
    System.out.println("-- " + url + " formats --");

    iter = res.listProperties(p);
    while (iter.hasNext()) {
      System.out.println(iter.next().getObject());
    }

    // Get all hasPart resources of the collection.
    res = model.getResource(url);
    p = model.createProperty(dctermsNamespace, "hasPart");

    System.out.println();
    System.out.println("-- " + url + " hasPart --");

    iter = res.listProperties(p);
    List<String> part = new ArrayList<String>();
    while (iter.hasNext()) {
      String pa = iter.next().getObject().toString();
      System.out.println(pa);
      part.add(pa);
    }

    // Get out the collection's metadata.
    System.out.println();
    System.out.println("-- " + url + " DC and DCTERMS metadata --");

    Model m = ModelFactory.createDefaultModel();
    // Set only DARIAH namespace, because namespace is added automatically
    // if used (DARIAH namespace is only used as a value).
    m.setNsPrefix(DARIAH_PREFIX, model.getNsPrefixURI(DARIAH_PREFIX));

    iter = model
        .listStatements(new SimpleSelector(res, null, (RDFNode) null));
    while (iter.hasNext()) {
      Statement s = iter.next();
      // Leave out the hasParts.
      if (!s.getPredicate().getLocalName().equals("hasPart")) {
        m.add(s);
      }
    }

    StringWriter metadataWriter = new StringWriter();
    m.write(metadataWriter);
    String metadata = metadataWriter.toString();

    System.out.println(metadata);

    // Create new model for CR.
    m = ModelFactory.createDefaultModel();

    // Add all the collection's statements to the new model.
    iter = res.listProperties();
    while (iter.hasNext()) {
      Statement s = iter.nextStatement();
      if (s.getPredicate().getLocalName().equals("hasPart")
          || s.getPredicate().getLocalName().equals("creator")
          || s.getPredicate().getLocalName().equals("rights")
          || s.getPredicate().getLocalName().equals("type")) {
        // Leave it out!
      } else if (s.getPredicate().getNameSpace()
          .equals(model.getNsPrefixURI(DCTERMS_PREFIX))) {
        // Change from DCTERMS to DC.
        String localName = s.getPredicate().getLocalName();
        String value = s.getString();
        m.remove(s);
        m.getResource(url).addProperty(
            model.createProperty(dcNamespace, localName), value);
      } else {
        m.add(s);
      }
    }

    // Add more statements.
    m.getResource(url).addProperty(
        model.createProperty(dcNamespace, "description"),
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros. Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est.");
    m.getResource(url).addProperty(
        model.createProperty(dcNamespace, "contributor"),
        "stefan.funk@textgrid.de");
    m.getResource(url).addProperty(
        model.createProperty(dcNamespace, "identifier"),
        "http://oai.de.dariah.eu/123-3434-1221");
    m.getResource(url).addProperty(
        model.createProperty(dcNamespace, "identifier"),
        "hdl:123-3434-1221");
    m.getResource(url).addProperty(
        model.createProperty(dcNamespace, "identifier"),
        "123-3434-1221");

    // Output new model in RDF XML.
    System.out.println();
    System.out.println("-- DARIAH Collection --");

    m.write(System.out);

    // Create DataHandler from String.
    System.out.println();
    System.out.println("-- Create DataHandler from String --");

    String data =
        "<?xml version='1.0' encoding='UTF-8'?><rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:ore=\"http://www.openarchives.org/ore/terms/\"><rdf:Description><ore:aggregates rdf:resource=\"http://geobrowser.de.dariah.eu/storage/208751\"/><ore:aggregates rdf:resource=\"http://geobrowser.de.dariah.eu/storage/208801\"/></rdf:Description></rdf:RDF>";
    DataHandler dh = new DataHandler(new ByteArrayDataSource(data, ""));

    System.out.println(IOUtils.readStringFromStream(dh.getInputStream()));

    // Create DataHandler from URL.
    System.out.println();
    System.out.println("-- Create DataHandler from URL --");

    URL url2 = URI.create(url).toURL();
    dh = new DataHandler(new URLDataSource(url2));

    System.out.println(IOUtils.readStringFromStream(dh.getInputStream()));

    // Get out metadata of one part.
    System.out.println();
    System.out.println("-- Get out metadata of one part only --");

    for (String pa : part) {
      System.out.println("Part: " + pa);

      m = ModelFactory.createDefaultModel();
      m.setNsPrefix(DARIAH_PREFIX, model.getNsPrefixURI(DARIAH_PREFIX));
      res = model.getResource(pa);

      iter = res.listProperties();
      while (iter.hasNext()) {
        m.add(iter.next());
      }

      m.write(System.out);
    }
  }

}
