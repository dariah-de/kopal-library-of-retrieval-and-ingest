/**
 * This software is copyright (c) 2023 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import javax.xml.stream.XMLStreamException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.rdf.model.Model;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.dariahde.publish.DHPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.RedisDB;
import de.langzeitarchivierung.kolibri.webservice.dariahde.publish.DHPublishServiceImpl;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import jakarta.ws.rs.ProcessingException;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 *
 * 2021-08-24 - Funk - Add cr_import_key for CR import.
 *
 * 2021-07-23 - Funk - Checking new CR answers.
 *
 * 2017-07-11 - Funk - Sending ALL DC data to Collection Registry! Tobi will do the mapping :-)
 * Thanks a lot!
 * 
 * 2016-07-28 - Funk - Added timeout to CR calls.
 */

/**
 * <p>
 * Notifies Collection Registry
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2023-05-24
 * @since 2014-12-27
 */

public class NotifyCollectionRegistry implements ActionModule {

  // **
  // STATIC FINAL
  // **

  private static final String CR_URL_STRT = "<url>";
  private static final String CR_URL_END = "</url>";

  // **
  // STATE
  // **

  ProcessData processData;
  Step step;
  private boolean dryRun;
  private String crImportKey;


  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>...</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>...</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(DHPublishService.STORAGE_ID);
    String eppn = (String) customData.get(DHPublishServiceImpl.DARIAH_EPPN);
    String collectionLogID = (String) customData.get(DHPublishService.TRANSACTION_ID);
    PublishResponse publishResponse =
        (PublishResponse) customData.get(DHPublishServiceImpl.PUBLISH_RESPONSE);
    this.dryRun = publishResponse.dryRun;
    Model model = (Model) customData.get(uri);

    // Get collection log ID (if existing).
    String coLogIDString = DHPublishUtils.getLogIDString(collectionLogID);

    // Start log entry. Set message also for current task info.
    String message = coLogIDString
        + "DARIAH-DE Collection Registry collection is being created for [" + eppn + "]";
    PublishUtils.setStatusRunning(this.step, message, publishResponse, this, message, this.dryRun);

    // Get PID.
    String collectionPid = DHPublishUtils.getAdministrativePid(model, uri);

    message = coLogIDString + "Collection's PID is " + collectionPid;
    PublishUtils.setStatusRunning(this.step, message, publishResponse, this, message, this.dryRun);

    try {

      PublishUtils.setStatusRunning(this.step,
          RDFUtils.getStringFromModel(model, RDFConstants.TURTLE), publishResponse, this,
          this.dryRun);

      // Create metadata model out of root collection RDF model, add repository OAI URL as
      // identifier.
      Model crMetadataModel =
          DHPublishUtils.createCollectionRegistryDcMetadata(model, uri, collectionPid);

      // Create string from model.
      String modelString = RDFUtils.getStringFromModel(crMetadataModel);

      PublishUtils.setStatusRunning(this.step, modelString, publishResponse, this, this.dryRun);

      // TODO Check service availability in dryRun mode!

      if (!this.dryRun) {
        String crTarget = DHPublishAbs.getCrEndpoint() + DHPublishAbs.getCrSubmitDraftPath();
        // Add CR import key, if configured.
        message = coLogIDString + "Calling " + crTarget;
        if (this.crImportKey != null && !this.crImportKey.isEmpty()) {
          crTarget += "?key=" + this.crImportKey;
          message += "?key=***";
        }
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);

        // Get response from CR HTTP client.
        Client crClient = DHPublishAbs.getCRClient();
        Response response = crClient.target(crTarget).request()
            .header(DHPublishService.TRANSACTION_ID, collectionLogID)
            .post(Entity.entity(modelString, MediaType.TEXT_XML));

        int statusCode = response.getStatus();
        String reasonPhrase = response.getStatusInfo().getReasonPhrase();

        // 200 OK.
        if (statusCode == Status.OK.getStatusCode()) {

          String rString = IOUtils.readStringFromStream(response.readEntity(InputStream.class));

          message = coLogIDString + "Collection Registry's answer: " + rString;
          PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);

          // Get URL out of response (from <url> tag).
          // TODO Use XML to retrieve response URL, not string processing...
          int urlStrt = rString.indexOf(CR_URL_STRT);
          int urlEnd = rString.indexOf(CR_URL_END);
          String collectionId = "";
          if (urlStrt != -1 && urlEnd != -1) {
            String collectionUrl = rString.substring(urlStrt + CR_URL_STRT.length(), urlEnd);
            collectionId = DHPublishUtils.extractCollectionId(collectionUrl);
            message =
                coLogIDString + "Collection <url> [id=" + collectionId + "]: " + collectionUrl;
            PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);
          }

          // Add COLREG ID to REDIS only! It does NOT belong to Handle PID metadata (at least I
          // would think)!
          if (!collectionId.isEmpty()) {
            RedisDB.addCRID(uri, collectionId);
          } else {
            throw new XMLStreamException(
                "\n\tCollection XML is empty!\n\tWhat should I do?\n\tPlease ask *fu*!\n");
          }

        } else {
          throw new IOException("CR HTTP error: " + statusCode + " " + reasonPhrase);
        }
      }

    } catch (IOException | XMLStreamException | ProcessingException e) {
      message = coLogIDString + "Collection Registry notification failed due to a "
          + e.getClass().getName() + ": " + e.getMessage();
      PublishUtils.addGeneralError(this.step, ErrorType.NOT_SPECIFIED, message, publishResponse,
          this, this.dryRun);
      PublishUtils.updateResponse(uri, publishResponse);
      return;
    }

    // Set module to status DONE.
    message = "Collection Registry notification complete";
    PublishUtils.setStatusDone(this.step, message, publishResponse, this, this.dryRun);

    // Finally write publish responses back to custom data.
    PublishUtils.updateResponse(uri, publishResponse);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param crInputKey
   */
  public void setCrImportKey(String crImportKey) {
    this.crImportKey = crImportKey;
  }

}
