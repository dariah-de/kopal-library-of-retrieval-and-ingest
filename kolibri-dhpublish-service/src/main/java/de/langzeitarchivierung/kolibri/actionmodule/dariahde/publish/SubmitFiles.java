/*******************************************************************************
 * This software is copyright (c) 2018 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import org.apache.jena.rdf.model.Model;
import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.dariahde.publish.DHPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.dariahde.publish.DHPublishServiceImpl;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import jakarta.activation.DataHandler;
import jakarta.mail.util.ByteArrayDataSource;
import jakarta.ws.rs.ProcessingException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * <p>
 * Submits files to the public storage, not collections, files only.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2019-03-29
 * @since 2014-12-19
 */

public class SubmitFiles implements ActionModule {

  // **
  // STATIC FINAL
  // **

  private static final boolean INCREASE_PROGRESS = true;
  private static final String FILE_SUBMISSION_FAILED = "File submission failed due to a ";

  // **
  // STATIC
  // **

  private static final StatsDClient statsd = new NonBlockingStatsDClient("dhpublish",
      DHPublishAbs.getStatsdEndpoint(), DHPublishAbs.getStatsdPort());

  // **
  // STATE
  // **

  ProcessData processData;
  Step step;
  private String ownStorageEndpoint = "https://de.dariah.eu/storage/";
  private boolean dryRun;
  private long dryRunOffset = 1000;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>...</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>...</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(DHPublishService.STORAGE_ID);
    String eppn = (String) customData.get(DHPublishServiceImpl.DARIAH_EPPN);
    String collectionLogID = (String) customData.get(DHPublishService.TRANSACTION_ID);
    String storageToken = (String) customData.get(DHPublishService.STORAGE_TOKEN);
    String seafileToken = (String) customData.get(DHPublishService.SEAFILE_TOKEN);
    PublishResponse publishResponse =
        (PublishResponse) customData.get(DHPublishServiceImpl.PUBLISH_RESPONSE);
    Model model = (Model) customData.get(uri);
    this.dryRun = publishResponse.dryRun;

    // Get collection log ID (if existing).
    String coLogIDString = DHPublishUtils.getLogIDString(collectionLogID);

    // Start log entry. Set message also for current task info.
    int objectAmount = publishResponse.getPublishObjects().size();
    String message = coLogIDString + objectAmount + " file" + (objectAmount != 1 ? "s are" : " is")
        + " beeing submitted for " + eppn;
    PublishUtils.setStatusRunning(this.step, message, publishResponse, this, message, this.dryRun);

    // Get progress. This module has to cover and compute in how many steps we can divide this
    // module (use float for computation for small values!).
    float moduleProgress = publishResponse.getPublishStatus().progress;
    float stepProgress = (float) PublishUtils.progressPerModule(this.processData)
        / (DHPublishUtils.PROGRESS_UPDATES_PER_MODULE * (float) objectAmount);

    // Loop all the publish objects, submit only the files, we already have created the collections
    // in CreateCollections!
    int count = 0;
    for (PublishObject po : publishResponse.getPublishObjects()) {

      // Generate object specific log IDs.
      String poLogID = DHPublishUtils.createCombinedLogID(collectionLogID);
      String poLogIDString = DHPublishUtils.getLogIDString(poLogID);
      String loguri = poLogIDString + "[" + DHPublishUtils.getStorageId(po.uri) + "] ";

      // Set progress per object.
      moduleProgress += stepProgress;
      PublishUtils.setProgress(publishResponse, (int) moduleProgress);

      // Create metadata string from metadata model for creation.
      String handle = DHPublishUtils.getAdministrativePid(model, po.uri);
      Model metadata = DHPublishUtils.createMetadata(model, po.uri, handle);

      // Check for collections in overall model.
      boolean isCollection = RDFUtils.isCollection(model, po.uri);

      // Increase file count and set current task for object.
      count++;
      if (isCollection) {
        String task = "Collection " + count + " of " + objectAmount + " files already created";
        message = loguri + task;
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, task, this.dryRun);
      } else {
        String task = "File " + count + " of " + objectAmount + " is being published";
        message = loguri + task;
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, task, this.dryRun);

        // DEBUG OUTPUT!
        message = loguri + "Metadata for DH-crud creation\n" + RDFUtils.getTTLFromModel(metadata);
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);
        // DEBUG OUTPUT!

        message = loguri + "Metadata taken out of model";
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);

        // Try if URI is correct, if not, ignore and warn!
        String resourceUri = po.uri;
        try {
          // Load data from OwnStorage as configured, if a different OwnStorage endpoint is given in
          // the model.
          if (!resourceUri.startsWith(this.ownStorageEndpoint)) {
            resourceUri = this.ownStorageEndpoint + DHPublishUtils.getStorageId(po.uri);
          }
          URI.create(resourceUri);

          message = loguri + "Data is being downloadad from " + po.uri
              + " (PID used for creation is " + handle + ")";
          PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);

        } catch (IllegalArgumentException e) {
          // TODO Put warning in PublishResponse!
          message = loguri + "Illegal URI detected! File will be ignored!";
          PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);
          continue;
        }

        // Data URI is known, so we can start creating...

        // Only #CREATE if dryRun is set to false.
        if (!this.dryRun) {
          try {
            // Create data handler for collection metadata.
            DataHandler binaryMetadata =
                new DataHandler(new ByteArrayDataSource(RDFUtils.getTTLFromModel(metadata),
                    MediaType.APPLICATION_OCTET_STREAM));

            Response createResponse = DHPublishAbs.getCrudService().create(binaryMetadata,
                URI.create(resourceUri), URI.create(handle), storageToken, seafileToken, poLogID);

            int statusCode = createResponse.getStatus();
            String reasonPhrase = createResponse.getStatusInfo().getReasonPhrase();

            if (statusCode != Status.OK.getStatusCode()) {

              // FIXME Check error handling!

              message = loguri + "Error submitting file! DH-crud says: " + statusCode + " "
                  + reasonPhrase;
              DHPublishUtils.finishWithError(po, this, true, this.step, this.dryRun,
                  publishResponse, message, ErrorType.NOT_SPECIFIED);
              return;
            }

            String location = createResponse.getLocation().toString();
            message =
                loguri + "DH-crud#CREATE complete [" + (location == null ? "NULL" : location) + "]";
            PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);

            // Increase statsd FILES counter!
            statsd.incrementCounter("files-submitted");

            message = loguri + "Incremented statsd FILES counter";
            PublishUtils.setStatusRunning(this.step, message, publishResponse, this, message,
                this.dryRun);

          } catch (IOException | ProcessingException e) {
            message = FILE_SUBMISSION_FAILED + e.getClass().getName() + ": " + e.getMessage();
            PublishUtils.addGeneralError(this.step, ErrorType.NOT_SPECIFIED, message,
                publishResponse, this, INCREASE_PROGRESS, this.dryRun);
            PublishUtils.updateResponse(po.uri, publishResponse);
            return;
          }

        } else {

          String dhcrudVersion = DHPublishAbs.getCrudService()
              .getVersion();

          // Fake the time file is being created :-)
          try {
            Thread.sleep(this.dryRunOffset);
          } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }

          if (dhcrudVersion != null && !"".equals(dhcrudVersion)) {
            message =
                loguri + "DH-crud#CREATE complete (" + publishResponse.getPublishStatus().progress
                    + "%): DH-crud available [" + dhcrudVersion + "]";
          } else {
            // TODO Handle timeout here?
          }
        }
      }
    }

    // Set module to status DONE.
    message = coLogIDString + "Overall data submission complete";
    PublishUtils.setStatusDone(this.step, message, publishResponse, this, this.dryRun);

    // Finally write publish responses back to custom data.
    PublishUtils.updateResponse(uri, publishResponse);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param offset
   */
  public void setDryRunOffset(long offset) {
    this.dryRunOffset = offset;
  }

  /**
   * @param ownStorageEndpoint
   */
  public void setOwnStorageEndpoint(String ownStorageEndpoint) {
    this.ownStorageEndpoint = ownStorageEndpoint;
  }

}
