/*******************************************************************************
 * This software is copyright (c) 2017 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.naming.AuthenticationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.apache.jena.rdf.model.Model;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.dariahde.publish.DHPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.dariahde.publish.DHPublishServiceImpl;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import jakarta.ws.rs.WebApplicationException;

/*******************************************************************************
 * <p>
 * Update Handle metadata.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2017-12-08
 * @since 2014-12-19
 ******************************************************************************/

public class UpdatePidMetadata implements ActionModule {

  // **
  // STATIC FINAL
  // **

  static final boolean INCREASE_PROGRESS = true;

  // **
  // STATE
  // **

  ProcessData processData;
  Step step;
  private boolean dryRun;
  private String pidSecret;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>...</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>...</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(DHPublishService.STORAGE_ID);
    // String eppn = (String)
    // customData.get(DHPublishServiceImpl.DARIAH_EPPN);
    String collectionLogID = (String) customData
        .get(DHPublishService.TRANSACTION_ID);
    PublishResponse publishResponse = (PublishResponse) customData
        .get(DHPublishServiceImpl.PUBLISH_RESPONSE);
    this.dryRun = publishResponse.dryRun;
    Model model = (Model) customData.get(uri);

    // Get collection log ID (if existing).
    String coLogIDString = DHPublishUtils.getLogIDString(collectionLogID);

    // Get PID out of the collection's RDF file, create the RDF model.
    String pid = DHPublishUtils.getAdministrativePid(model, uri);

    // Start log entry. Set message also for current task info.
    String message = coLogIDString
        + "Handle metadata of collection is beeing updated";
    PublishUtils.setStatusRunning(this.step, message, publishResponse, this,
        message, this.dryRun);

    // Data URI is known, so we can start creating.
    if (!this.dryRun) {
      if (!pid.equals("")) {
        List<String> published = new ArrayList<String>();
        published.add("PUBLISHED,true");

        try {
          DHPublishAbs.getPidService().addMetadata(
              LTPUtils.omitHdlPrefix(pid), published,
              this.pidSecret);
        } catch (XPathExpressionException | IOException | SAXException
            | ParserConfigurationException | WebApplicationException
            | AuthenticationException e) {
          message = coLogIDString + "Updating PID metadata failed ["
              + e.getClass().getName() + "]: " + e.getMessage();
          PublishUtils.addGeneralError(this.step,
              ErrorType.NOT_SPECIFIED, message, publishResponse,
              this, INCREASE_PROGRESS, this.dryRun);
          PublishUtils.updateResponse(uri, publishResponse);
          return;
        }

        message = coLogIDString
            + "PUBLISHED key set to TRUE for collection PID " + pid;

      } else {
        message = coLogIDString + "No PID found for collection URI "
            + uri + "!";
      }

      PublishUtils.setStatusRunning(this.step, message, publishResponse,
          this, this.dryRun);
    }

    // Set module to status DONE.
    message = coLogIDString + "Metadata processing complete";
    PublishUtils.setStatusDone(this.step, message, publishResponse, this,
        this.dryRun);

    // Finally write publish responses back to custom data.
    PublishUtils.updateResponse(uri, publishResponse);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param pidSecret
   */
  public void setPidSecret(String pidSecret) {
    this.pidSecret = pidSecret;
  }

}
