/**
 * This software is copyright (c) 2021 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import java.util.HashMap;
import org.apache.jena.rdf.model.Model;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.dariahde.publish.DHPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/**
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-03-17
 * @since 2014-12-19
 */

public class MetadataProcessor implements ActionModule {

  // **
  // STATE
  // **

  ProcessData processData;
  Step step;
  private boolean dryRun;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>...</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>...</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(DHPublishService.STORAGE_ID);
    String eppn = (String) customData.get(DHPublishServiceImpl.DARIAH_EPPN);
    String collectionLogID = (String) customData.get(DHPublishService.TRANSACTION_ID);
    PublishResponse publishResponse =
        (PublishResponse) customData.get(DHPublishServiceImpl.PUBLISH_RESPONSE);
    this.dryRun = publishResponse.dryRun;
    Model model = (Model) customData.get(uri);

    // Get collection log ID (if existing).
    String coLogIDString = DHPublishUtils.getLogIDString(collectionLogID);

    // Start log entry. Set message also for current task info.
    String message = coLogIDString + "Metadata is beeing processed for all objects";
    PublishUtils.setStatusRunning(this.step, message, publishResponse, this, message, this.dryRun);

    // Loop all the parts.
    for (PublishObject o : publishResponse.getPublishObjects()) {

      // Generate object specific log IDs.
      String poLogID = DHPublishUtils.createCombinedLogID(collectionLogID);
      String poLogIDString = DHPublishUtils.getLogIDString(poLogID);
      String loguri = poLogIDString + "[" + DHPublishUtils.getStorageId(o.uri) + "] ";

      // **
      // Add dc:contributor to every object's metadata.
      // **
      // NOTE Existing ePPN is tested in CheckCollections module.
      // **
      message = loguri + "Adding dc:contributor from ePPN " + eppn;
      PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);

      RDFUtils.addLiteralToModel(model, uri, RDFConstants.DC_PREFIX,
          RDFConstants.ELEM_DC_CONTRIBUTOR, eppn);
    }

    // Update overall model on custom data.
    this.processData.getCustomData().put(uri, model);

    // Set module to status DONE.
    message = coLogIDString + "Metadata processing complete";
    PublishUtils.setStatusDone(this.step, message, publishResponse, this, this.dryRun);

    // Finally write publish responses back to custom data.
    PublishUtils.updateResponse(uri, publishResponse);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

}
