/**
 * This software is copyright (c) 2021 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.dariahde.publish.DHPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.common.DariahStorageClient;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/**
 * <p>
 * Gets the overall RDF model out of the various collection RDF files delivered by THE PUBLIKATOR.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-03-15
 * @since 2017-05-04
 */

public class ReadCollectionModels implements ActionModule {

  // **
  // STATE
  // **

  private Step step;
  private ProcessData processData;
  private boolean dryRun;
  private PublishResponse publishResponse;
  private boolean anyError = false;
  private String ownStorageEndpoint = "https://de.dariah.eu/storage/";
  private int collectionCount = 0;
  private String coLogIDString = "";
  private DariahStorageClient storageClient = new DariahStorageClient();
  private String storageToken;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Reads all the collection RDF files into one overall model.
   * </p>
   * 
   * <p>
   * <strong>NEEDS</strong>
   * <ul>
   * <li>An URI of an RDF file containing all the objects and sub collections of a collection</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <strong>GIVES</strong>
   * <ul>
   * <li>Reads all the collection RDF files into one overall model (Model model)</li>
   * <li>Creates the publishResponse containing all the objects (PublishResponse
   * this.publishResponse)</li>
   * <li>Both is put into custom data as hash with the root collection URI as key</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(DHPublishService.STORAGE_ID);
    String eppn = (String) customData.get(DHPublishServiceImpl.DARIAH_EPPN);
    String collectionLogID = (String) customData.get(DHPublishService.TRANSACTION_ID);
    this.storageToken = (String) customData.get(DHPublishService.STORAGE_TOKEN);
    // String seafileToken = (String) customData.get(DHPublishService.SEAFILE_TOKEN);
    this.publishResponse = (PublishResponse) customData.get(DHPublishServiceImpl.PUBLISH_RESPONSE);
    this.dryRun = this.publishResponse.dryRun;

    // Get collection log ID (if existing).
    this.coLogIDString = DHPublishUtils.getLogIDString(collectionLogID);

    // Start log entry. Set message also for current task info.
    String message = this.coLogIDString + "Assembling overall RDF model from collection files";
    PublishUtils.setStatusRunning(this.step, message, this.publishResponse, this, message,
        this.dryRun);

    // Set storage client endpoint.
    this.storageClient = this.storageClient.setStorageUri(URI.create(this.ownStorageEndpoint));

    // Reference on requested object is hold to change state later if necessary.
    PublishObject rootCollectionObject = new PublishObject(uri);
    this.publishResponse.addPublishObject(rootCollectionObject);

    // Create model and set used namespaces.
    Model model = ModelFactory.createDefaultModel();

    // Root URI is a collection! Do read all collection models.
    readCollectionModels(model, uri, eppn, rootCollectionObject);

    // Put overall model in custom data.
    this.processData.getCustomData().put(uri, model);

    // Call finish before leaving go().
    message = this.coLogIDString + "Read " + this.collectionCount + " collection"
        + (this.collectionCount != 1 ? "s" : "") + " from OwnStorage";
    DHPublishUtils.finish(uri, this, this.anyError, this.step, this.dryRun, this.publishResponse,
        message);
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Read given URI from OwnStorage and add to current model.
   * </p>
   * 
   * @param theModel
   * @param theUri
   * @param theEppn
   * @param thePublishObject
   */
  public void readCollectionModels(Model theModel, final String theUri, final String theEppn,
      final PublishObject thePublishObject) {

    // In case of an error, just return.
    if (this.anyError) {
      return;
    }

    this.collectionCount += 1;

    // Load and add to overall RDF object from stream.
    try {
      String message =
          "Reading collection model no#" + this.collectionCount + " with URI " + theUri;
      PublishUtils.setStatusRunning(this.step, message, this.publishResponse, this, message,
          this.dryRun);

      // Read collection's RDF file from DARIAH-DE OwnStorage using given URL and ID.
      String ownStorageObjectString = IOUtils.readStringFromStream(
          this.storageClient.readFile(DHPublishUtils.getStorageId(theUri), this.storageToken));

      message = "Adding collection model no#" + this.collectionCount + " with URI " + theUri
          + " to overall RDF model";
      PublishUtils.setStatusRunning(this.step, message, this.publishResponse, this, message,
          this.dryRun);

      // Add model to overall model.
      Model modelToAdd = RDFUtils.readModel(ownStorageObjectString, RDFConstants.TURTLE);
      theModel.add(modelToAdd);

    } catch (IOException e) {
      this.anyError = true;
      String message = this.coLogIDString + "Error reading collection no#" + this.collectionCount
          + " with URI " + theUri + "! [" + e.getClass().getName() + "] " + e.getMessage();
      DHPublishUtils.setError(thePublishObject, this.step, this.anyError, ErrorType.NOT_SPECIFIED,
          message, DHPublishUtils.DONT_INCREASE_PROGRESS, this.publishResponse, this,
          this.dryRun);
      return;
    } catch (ParseException e) {
      this.anyError = true;
      String message = this.coLogIDString + "Error parsing collection no#" + this.collectionCount
          + " with URI " + theUri + ": [" + e.getClass().getName() + "] " + e.getMessage();
      DHPublishUtils.setError(thePublishObject, this.step, this.anyError, ErrorType.NOT_SPECIFIED,
          message, DHPublishUtils.DONT_INCREASE_PROGRESS, this.publishResponse, this, this.dryRun);
      return;
    }

    // Get objects contained in collection (hasPart).
    Resource res = theModel.getResource(theUri);
    Property prop = theModel.createProperty(theModel.getNsPrefixURI(RDFConstants.DCTERMS_PREFIX),
        RDFConstants.ELEM_DCTERMS_HASPART);

    // Loop all the contained objects, check for containing elements, if none add error.
    StmtIterator iter = res.listProperties(prop);
    if (!iter.hasNext()) {
      this.anyError = true;
      String message = this.coLogIDString + "Collection no#" + this.collectionCount
          + " with URI " + theUri + " has got no files attached!";
      DHPublishUtils.setError(thePublishObject, this.step, this.anyError, ErrorType.NOT_SPECIFIED,
          message, DHPublishUtils.DONT_INCREASE_PROGRESS, this.publishResponse, this, this.dryRun);
      return;
    }

    // If elements are existing, do...
    while (iter.hasNext()) {
      Statement s = iter.next();

      List<String> l = RDFUtils.getRDFList(s.getObject());
      for (String str : l) {
        // ...create publish object for every object,
        PublishObject p = new PublishObject(str);

        // ...check collection metadata, if type is collection.
        if (RDFUtils.isCollection(theModel, p.uri)) {
          readCollectionModels(theModel, p.uri, theEppn, p);
        }

        // Finally add publish object to publish response.
        this.publishResponse.addPublishObject(p);
      }
    }
  }

  // **
  // GETTER AND SETTER
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param ownStorageEndpoint
   */
  public void setOwnStorageEndpoint(String ownStorageEndpoint) {
    this.ownStorageEndpoint = ownStorageEndpoint;
  }

}
