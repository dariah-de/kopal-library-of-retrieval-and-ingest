/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.naming.AuthenticationException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.dariahde.publish.DHPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.RedisDB;
import de.langzeitarchivierung.kolibri.webservice.dariahde.publish.DHPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.common.DariahStorageClient;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/*******************************************************************************
 * <p>
 * Checks if the given URI really is a collection, validates metadata.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2019-04-04
 * @since 2014-12-19
 ******************************************************************************/

public class CheckCollections implements ActionModule {

  // **
  // STATIC FINALS
  // **

  protected static final String PLEASE_PROVIDE_DATE =
      " Please provide date conform to XSD DateTime (https://en.wikipedia.org/wiki/ISO_8601)";

  // **
  // STATE
  // **

  private Step step;
  private ProcessData processData;
  private boolean dryRun;
  private PublishResponse publishResponse;
  private boolean anyError = false;
  private List<String> itemRequiredFields = new ArrayList<String>();
  private List<String> collectionRequiredFields = new ArrayList<String>();
  private Model model;
  private boolean failIfAlreadyPublishedOnPublish = true;
  private boolean checkDateTime = false;
  private String coLogIDString = "";
  private static DariahStorageClient storageClient = new DariahStorageClient();

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Checks an collection if it is suitable for being published. This check includes:
   * </p>
   * 
   * <ol>
   * <li>Check if collections are empty (no objects included)</li>
   * <li>Check if ePPN is existing</li>
   * <li>Check if mandatory metadata is contained</li>
   * <li>Check for correct dc:date field values</li>
   * <lI>Check for HTML content (none allowed)</li>
   * <li>Check existence of liked files</li>
   * </ol>
   * </p>
   * 
   * <p>
   * <strong>NEEDS</strong>
   * <ul>
   * <li>The overall RDF model for reading and then checking the given metadata</li>
   * <li>The PublishResponse of the root collection containing all the objects</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <strong>GIVES</strong>
   * <ul>
   * <li>An updated PublishResponse containing errors or OKs for every object</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(DHPublishService.STORAGE_ID);
    String eppn = (String) customData.get(DHPublishServiceImpl.DARIAH_EPPN);
    String collectionLogID = (String) customData.get(DHPublishService.TRANSACTION_ID);
    String storageToken = (String) customData.get(DHPublishService.STORAGE_TOKEN);
    this.publishResponse = (PublishResponse) customData.get(DHPublishServiceImpl.PUBLISH_RESPONSE);
    this.dryRun = this.publishResponse.dryRun;
    this.model = (Model) this.processData.getCustomData().get(uri);

    // Get collection log ID.
    this.coLogIDString = DHPublishUtils.getLogIDString(collectionLogID);

    // Start log entry. Set message also for current task info.
    String message = this.coLogIDString + "Checking if collection is publishable";
    PublishUtils.setStatusRunning(this.step, message, this.publishResponse, this, message,
        this.dryRun);

    // Reference on requested object is hold to change state later if necessary.
    PublishObject rootCollectionObject = this.publishResponse.getPublishObjects().get(0);

    // Check if ePPN is existing.
    if (eppn == null || eppn.equals("")) {
      message = this.coLogIDString + "No ePPN given as input parameter!";
      DHPublishUtils.finishWithError(rootCollectionObject, this, this.anyError, this.step,
          this.dryRun, this.publishResponse, message, ErrorType.NOT_SPECIFIED);
      return;
    }

    // Get PID and check if root collection already has been published (if PID is existing and PID
    // is set to TRUE in Handle metadata).
    String pid = RedisDB.getHDL(rootCollectionObject.uri);
    try {
      if (DHPublishUtils.checkPublished(pid)) {
        message = this.coLogIDString + "Object has already been published before! PID is " + pid;

        // Fail if configured so...
        if (this.failIfAlreadyPublishedOnPublish) {
          this.anyError = true;
          DHPublishUtils.finishWithError(rootCollectionObject, this, this.anyError, this.step,
              this.dryRun, this.publishResponse, message, ErrorType.ALREADY_PUBLISHED);
          return;
        }
        // ...log only if not.
        else {
          PublishUtils.setStatusRunning(this.step, message + " (continuing due to configuration)",
              this.publishResponse, this, message, this.dryRun);
        }
      }
    } catch (AuthenticationException e) {
      message = this.coLogIDString
          + "Not authenticated on PID service! This error should not have been occurred! We apologise for the inconvenience!";
      this.anyError = true;
      DHPublishUtils.finishWithError(rootCollectionObject, this, this.anyError, this.step,
          this.dryRun, this.publishResponse, message, ErrorType.AUTH);
      return;
    } catch (IOException e) {
      message = this.coLogIDString
          + "Internal service error on PID service! This error should not have been occurred! We apologise for the inconvenience!";
      this.anyError = true;
      DHPublishUtils.finishWithError(rootCollectionObject, this, this.anyError, this.step,
          this.dryRun, this.publishResponse, message, ErrorType.SERVER_ERROR);
      return;
    }

    message =
        this.coLogIDString + "Root collection URI is [" + uri + "], responsible is [" + eppn + "]";
    PublishUtils.setStatusRunning(this.step, message, this.publishResponse, this, this.dryRun);

    // Loop over all the objects in the PublishResponse.
    for (PublishObject p : this.publishResponse.getPublishObjects()) {

      // Only check for mandatory collection metadata, if object really is a collection, check for
      // item metadata otherwise.
      if (RDFUtils.isCollection(this.model, p.uri)) {

        String loguri = "[" + DHPublishUtils.getStorageId(p.uri) + "] ";
        message = this.coLogIDString + loguri + "Checking for mandatory COLLECTION metadata";
        PublishUtils.setStatusRunning(this.step, message, this.publishResponse, this, this.dryRun);

        checkRequiredMetadata(p, this.collectionRequiredFields);

      } else {

        String loguri = "[" + DHPublishUtils.getStorageId(p.uri) + "] ";
        message = this.coLogIDString + loguri + "Checking for mandatory ITEM metadata";
        PublishUtils.setStatusRunning(this.step, message, this.publishResponse, this, this.dryRun);

        checkRequiredMetadata(p, this.itemRequiredFields);
      }

      // Check general things.
      checkDateFields(p);
      check4HTMLContent(p);
      check4ExistingFilesInStorage(p, storageToken);
    }

    // Call finish before leaving go().
    message = this.coLogIDString + "Congratulations! Publishable object state confirmed";
    DHPublishUtils.finish(uri, this, this.anyError, this.step, this.dryRun, this.publishResponse,
        message);
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param theObject
   * @param theRequiredFields
   */
  private void checkRequiredMetadata(PublishObject theObject,
      final List<String> theRequiredFields) {

    List<String> requiredMetadataErrors =
        checkRequiredMetadata(this.model, theObject, theRequiredFields);
    if (!requiredMetadataErrors.isEmpty()) {
      this.anyError = true;
      for (String message : requiredMetadataErrors) {
        DHPublishUtils.setError(theObject, this.step, this.anyError, ErrorType.MISSING_METADATA,
            this.coLogIDString + message, DHPublishUtils.DONT_INCREASE_PROGRESS,
            this.publishResponse, this, this.dryRun);
      }
    }
  }

  /**
   * <p>
   * Check date field errors for ONE object only.
   * </p>
   * 
   * @param theObject
   */
  private void checkDateFields(PublishObject theObject) {

    List<String> dateFieldErrors = checkDateFields(this.model, theObject, this.checkDateTime);

    String storageID = DHPublishUtils.getStorageId(theObject.uri);

    String message = this.coLogIDString + "[" + storageID + "] Checking date filed validity";
    PublishUtils.setStatusRunning(this.step, message, this.publishResponse, this, this.dryRun);

    if (!dateFieldErrors.isEmpty()) {
      this.anyError = true;
      for (String error : dateFieldErrors) {
        DHPublishUtils.setError(theObject, this.step, this.anyError, ErrorType.MISSING_METADATA,
            this.coLogIDString + error, DHPublishUtils.DONT_INCREASE_PROGRESS, this.publishResponse,
            this, this.dryRun);
      }
    }
  }

  /**
   * <p>
   * Check for HTML in DC metadata. It is NOT ALLOWED for the time being!
   * </p>
   * 
   * @param theObject
   */
  private void check4HTMLContent(PublishObject theObject) {

    // Look for HTML content.
    List<String> htmlContent =
        CheckCollections.testDCTags4HTMLTagDelimeters(this.model, theObject.uri);

    String storageID = DHPublishUtils.getStorageId(theObject.uri);

    String message = this.coLogIDString + "[" + storageID + "] Checking for HTML content";
    PublishUtils.setStatusRunning(this.step, message, this.publishResponse, this, this.dryRun);

    // Do check.
    for (String tagitag : htmlContent) {
      this.anyError = true;
      message = this.coLogIDString + "No HTML content allowed in <" + tagitag
          + "> entries! Please use URL encoded content, if necessary!";
      DHPublishUtils.setError(theObject, this.step, this.anyError, ErrorType.MISSING_METADATA,
          message, DHPublishUtils.DONT_INCREASE_PROGRESS, this.publishResponse, this, this.dryRun);
    }
  }

  /**
   * @param theObject
   * @param theToken
   */
  private void check4ExistingFilesInStorage(PublishObject theObject, String theToken) {

    boolean existingAndReadable = false;

    String storageID = DHPublishUtils.getStorageId(theObject.uri);

    String message =
        this.coLogIDString + "[" + storageID + "] Checking storage access and availability";
    PublishUtils.setStatusRunning(this.step, message, this.publishResponse, this, this.dryRun);

    // Check for object's existence in OwnStorage (and read access!).
    DariahStorageClient ownStorageClient =
        storageClient.setStorageUri(URI.create(DHPublishAbs.getOwnStorageEndpoint()));
    try {
      existingAndReadable = ownStorageClient.checkAccess(storageID, theToken, this.coLogIDString,
          DariahStorageClient.CHECKACCESS_OPERATION_READ);

      if (!existingAndReadable) {
        this.anyError = true;
        message =
            this.coLogIDString + "Read access denied from OwnStorage for " + theObject.uri + "!";
        DHPublishUtils.setError(theObject, this.step, this.anyError, ErrorType.AUTH, message,
            DHPublishUtils.DONT_INCREASE_PROGRESS, this.publishResponse, this, this.dryRun);
      }

    } catch (IOException e) {
      this.anyError = true;
      message = this.coLogIDString + "Error accessing OwnStorage for " + theObject.uri + "!";
      DHPublishUtils.setError(theObject, this.step, this.anyError, ErrorType.MISSING_METADATA,
          message, DHPublishUtils.DONT_INCREASE_PROGRESS, this.publishResponse, this, this.dryRun);
    }
  }

  // **
  // PROTECTED
  // **

  /**
   * @param theModel
   * @param theObject
   * @param requiredFields
   * @return A list of required metadata elements, that are NOT existing and mandatory
   */
  protected static List<String> checkRequiredMetadata(final Model theModel, PublishObject theObject,
      final List<String> requiredFields) {

    List<String> result = new ArrayList<String>();

    // Loop all the elements required, set error if necessary.
    for (String element : requiredFields) {
      String[] banana = element.split(":");
      if (banana.length != 2) {
        result.add("Field values configuration failed! Please check value " + element + "!");
      }

      // We only need ONE required element of each tag.
      String value = RDFUtils.findFirstObject(theModel, theObject.uri, banana[0], banana[1]);
      if (value.isEmpty()) {
        result.add("Required metadata element " + element + " is missing!");
      }
    }

    return result;
  }

  /**
   * <p>
   * Checks all the dc:date fields in the model, if configured so.
   * </p>
   * 
   * @param theModel
   * @param theObject
   * @param checkDateTime
   * @return A list of false dc:date fields, empty if correct dates are given.
   */
  protected static List<String> checkDateFields(final Model theModel, PublishObject theObject,
      boolean checkDateTime) {

    List<String> result = new ArrayList<String>();

    if (checkDateTime) {

      // Look for dc:date fields.
      List<String> dcdates =
          RDFUtils.findAllObjects(theModel, theObject.uri, RDFConstants.DC_PREFIX,
              RDFConstants.ELEM_DC_DATE);

      // Do check dc:dates for XSDDateTime validity.
      for (String date : dcdates) {
        try {
          DatatypeFactory dtf = DatatypeFactory.newInstance();
          dtf.newXMLGregorianCalendar(date);
        } catch (IllegalArgumentException | DatatypeConfigurationException e) {
          result.add("Incorrect <dc:date> entry: " + date
              + (e.getMessage().equals(date) ? "" : " [" + e.getMessage() + "]")
              + PLEASE_PROVIDE_DATE);
        }
      }
    }

    return result;
  }

  /**
   * @param theModel
   * @param theResource
   * @return A list of DC tags
   */
  protected static List<String> testDCTags4HTMLTagDelimeters(Model theModel, String theResource) {

    List<String> result = new ArrayList<String>();

    StmtIterator iter = theModel.listStatements();
    while (iter.hasNext()) {
      Statement s = iter.next();
      if (s.getPredicate().getNameSpace().equals(RDFConstants.DC_NAMESPACE)
          && s.getSubject().toString().equals(theResource) && s.getObject().isLiteral()
          && (s.getObject().toString().contains("<") || s.getObject().toString().contains(">"))) {
        result.add("dc:" + s.getPredicate().getLocalName());
      }
    }

    return result;
  }

  // **
  // GETTER AND SETTER
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param xpathExpression
   */
  public void setCollectionRequiredFields(String xpathExpression) {
    if (!this.collectionRequiredFields.contains(xpathExpression)) {
      this.collectionRequiredFields.add(xpathExpression);
    }
  }

  /**
   * @param xpathExpression
   */
  public void setItemRequiredFields(String xpathExpression) {
    if (!this.itemRequiredFields.contains(xpathExpression)) {
      this.itemRequiredFields.add(xpathExpression);
    }
  }

  /**
   * @param failIfAlreadyPublishedOnPublish
   */
  public void setFailIfAlreadyPublishedOnPublish(
      boolean failIfAlreadyPublishedOnPublish) {
    this.failIfAlreadyPublishedOnPublish = failIfAlreadyPublishedOnPublish;
  }

  /**
   * @param checkDateTime
   */
  public void setCheckDateTime(boolean checkDateTime) {
    this.checkDateTime = checkDateTime;
  }

}
