/*******************************************************************************
 * This software is copyright (c) 2018 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import java.util.HashMap;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.webservice.dariahde.publish.DHPublishServiceImpl;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;

/*******************************************************************************
 * <p>
 * Just calls the IIIF service so that IIIF manifest metadata is being created and cached (a call
 * such as:
 * https://trep.de.dariah.eu/1.0/iiif/manifests/hdl:21.T11991%2F0000-0006-17DE-7/manifest.json).
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2018-10-16
 * @since 2018-10-16
 ******************************************************************************/

public class NotifyIIIFMetadataService implements ActionModule {

  // **
  // STATE
  // **

  ProcessData processData;
  Step step;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>Nothing.</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>Increases the progress in percent.</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(DHPublishService.STORAGE_ID);
    String collectionLogID = (String) customData
        .get(DHPublishService.TRANSACTION_ID);
    PublishResponse publishResponse = (PublishResponse) customData
        .get(DHPublishServiceImpl.PUBLISH_RESPONSE);
    boolean dryRun = publishResponse.dryRun;

    // Get collection log ID (if existing).
    String coLogIDString = DHPublishUtils.getLogIDString(collectionLogID);

    // Calling IIF service once.
    String message = coLogIDString + "Calling IIIF manifest metadata service";
    PublishUtils.setStatusRunning(this.step, message, publishResponse, this,
        dryRun);

    // Call.
    // String manifestRequest =
    // "https://trep.de.dariah.eu/1.0/iiif/manifests/hdl:21.T11991%2F0000-0006-17DE-7/manifest.json";

    // CloseableHttpClient client = HttpClientBuilder.create().build();
    // HttpResponse response = client.execute(new HttpGet(manifestRequest));

    // Check response.
    // int statusCode = response.getStatusLine().getStatusCode();
    // String reasonPhrase = response.getStatusLine().getReasonPhrase();
    // String width, height;
    // if (statusCode == Status.OK.getStatusCode()) {


    // Set module to status to DONE.
    message = coLogIDString + "...not waiting for any response";

    PublishUtils.setFinished(this.step, message, publishResponse, this, dryRun);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

}
