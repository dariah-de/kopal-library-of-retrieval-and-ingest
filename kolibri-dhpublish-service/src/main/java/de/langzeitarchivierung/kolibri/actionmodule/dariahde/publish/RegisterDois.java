/**
 * This software is copyright (c) 2022 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import javax.naming.AuthenticationException;
import org.apache.jena.rdf.model.Model;
import com.timgroup.statsd.NonBlockingStatsDClient;
import com.timgroup.statsd.StatsDClient;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.dariahde.publish.DHPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.RedisDB;
import de.langzeitarchivierung.kolibri.webservice.dariahde.publish.DHPublishServiceImpl;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.WebApplicationException;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;

/**
 * <p>
 * Get a DOI for every object to store, get DOIs for collections, too.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2022-08-15
 * @since 2017-05-19
 */

public class RegisterDois implements ActionModule {

  // **
  // STATIC
  // **

  private static final StatsDClient statsd = new NonBlockingStatsDClient(
      "dhpublish", DHPublishAbs.getStatsdEndpoint(),
      DHPublishAbs.getStatsdPort());

  // **
  // STATE
  // **

  private ProcessData processData;
  private Step step;
  private boolean dryRun;
  private String pidSecret;
  private long failsafeDelay = 250;
  private int failsafeMaxRetries = 4;

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>The overall RDF model for reading and then checking the given metadata, including Handle
   * PIDs</li>
   * <li>The PublishResponse of the root collection containing all the objects</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>DOIs are generated for every item in the PublishResponse and are put back into (i) the
   * PublishResponse and (b) the overall RDF model for further use (both put back into custom
   * data)</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(DHPublishService.STORAGE_ID);
    // String eppn = (String) customData.get(DHPublishService.DARIAH_EPPN);
    String collectionLogID = (String) customData.get(DHPublishService.TRANSACTION_ID);
    PublishResponse publishResponse =
        (PublishResponse) customData.get(DHPublishServiceImpl.PUBLISH_RESPONSE);
    this.dryRun = publishResponse.dryRun;
    Model model = (Model) customData.get(uri);

    // Get collection log ID (if existing).
    String coLogIDString = DHPublishUtils.getLogIDString(collectionLogID);

    // Start log entry. Set message also for current task info.
    int objectAmount = publishResponse.getPublishObjects().size();
    String message = coLogIDString + objectAmount + " DOI" + (objectAmount != 1 ? "s are" : " is")
        + " beeing registered";
    PublishUtils.setStatusRunning(this.step, message, publishResponse, this, message, this.dryRun);

    // Get progress. This module has to cover and compute in how many steps we can divide this
    // module (use float for computation for small values!).
    float moduleProgress = publishResponse.getPublishStatus().progress;
    float stepProgress = (float) PublishUtils.progressPerModule(this.processData)
        / (DHPublishUtils.PROGRESS_UPDATES_PER_MODULE * (float) objectAmount);

    // Loop every object.
    int count = 0;
    for (PublishObject po : publishResponse.getPublishObjects()) {

      // Generate object specific log IDs.
      String poLogID = DHPublishUtils.createCombinedLogID(collectionLogID);
      String poLogIDString = DHPublishUtils.getLogIDString(poLogID);
      String loguri = poLogIDString + "[" + DHPublishUtils.getStorageId(po.uri) + "] ";

      // Set progress per object.
      moduleProgress += stepProgress;
      PublishUtils.setProgress(publishResponse, (int) moduleProgress);

      // Get object's DC metadata from overall model as TURTLE.
      String dcMetadata =
          RDFUtils.getStringFromModel(DHPublishUtils.getDcMetadata(model, po.uri),
              RDFConstants.TURTLE);

      message = loguri + "DC METADATA:\n" + dcMetadata;
      PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);

      // Get a DOI for every object.
      count++;
      try {
        String task = "DOI " + count + " of " + objectAmount + " is being registered";
        message = loguri + task;
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, task, this.dryRun);

        // Get PID of current object from dcterms:identifier, that is our administrative PID! (be
        // sure to get handle without PID prefix!).
        String handle = LTPUtils.omitHdlPrefix(DHPublishUtils.getAdministrativePid(model, po.uri));

        message = loguri + "PID registering DOI for: " + handle;
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);

        // Failsafe handling.
        // FIXME Check which exceptions we must handle here! Use HTTP response from PID service!
        RetryPolicy retryPolicy = new RetryPolicy()
            .withDelay(this.failsafeDelay, TimeUnit.MILLISECONDS)
            .withMaxRetries(this.failsafeMaxRetries)
            .retryOn(IOException.class)
            .retryOn(BadRequestException.class);
        TGPidServiceImplWrapper.GetDariahDoi getFailsafeDoi =
            new TGPidServiceImplWrapper().new GetDariahDoi(handle, dcMetadata, this.pidSecret);

        // Get DOI.
        String doi = Failsafe.with(retryPolicy).get(() -> getFailsafeDoi.call());

        message = loguri + "DOI: " + (doi == null ? "null" : doi);
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);

        if (doi == null || "".equals(doi)) {
          throw new AuthenticationException(poLogIDString + "DOI could not be registered");
        }

        // Add DOI namespace to overall model.
        model.setNsPrefix(RDFConstants.DOI_PREFIX, RDFConstants.DOI_NAMESPACE);

        // Add DOI to model.
        po.pid = addDoiToModel(model, po.uri, doi);

        // Add DOI and Handle to internal REDIS DB for mapping URIs to DOIs. We only need to store
        // the collection's DOI and Handle!
        if (uri == po.uri) {
          RedisDB.addDOI(po.uri, LTPUtils.omitDoiPrefix(po.pid));
          RedisDB.addHDL(po.uri, LTPUtils.omitHdlPrefix(handle));
        }

        message = loguri + "DOI registration complete: " + po.pid;
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, message,
            this.dryRun);

        // Increase statsd DOI counter!
        statsd.incrementCounter("dois-registered");

        message = loguri + "Incremented statsd DOI counter";
        PublishUtils.setStatusRunning(this.step, message, publishResponse, this, message,
            this.dryRun);

      } catch (AuthenticationException | IOException | WebApplicationException e) {
        message =
            loguri + "Error creating DOI: " + e.getMessage() + " [" + e.getClass().getName() + "]";
        boolean anyError = true;
        DHPublishUtils.setError(po, this.step, anyError, ErrorType.MISSING_METADATA, message,
            DHPublishUtils.DONT_INCREASE_PROGRESS, publishResponse, this, this.dryRun);
        return;
      }
    }

    // Update model on custom data.
    this.processData.getCustomData().put(uri, model);

    // TODO REMOVE DEBUG OUTPUT!!
    message = coLogIDString + "FINAL METADATA CONTAINING ALL THE DOIS:\n"
        + RDFUtils.getTTLFromModel(model);
    PublishUtils.setStatusRunning(this.step, message, publishResponse, this, this.dryRun);
    // TODO REMOVE DEBUG OUTPUT!!

    // Set module to status DONE.
    message = coLogIDString + "DOI registration complete";
    PublishUtils.setStatusDone(this.step, message, publishResponse, this, this.dryRun);

    // Finally write publish responses back to custom data.
    PublishUtils.updateResponse(uri, publishResponse);
  }

  /**
   * @param theModel
   * @param theUri
   * @param theDoi
   * @return the DOI added to the model
   */
  protected static String addDoiToModel(final Model theModel, final String theUri,
      final String theDoi) {

    // Use DOIs as RDF resource with DOI namespace.
    String doi = LTPUtils.omitDoiPrefix(theDoi);

    // Write back DOI to overall model (add to dc for dc usage, add to dcterms for administrative
    // usage, we want to distinguish user's DOIs and DH-publish created DOIs!).
    RDFUtils.addResourceToModel(theModel, theUri, RDFConstants.DC_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER,
        theModel.createResource(LTPUtils.resolveDoiPid(doi)));
    RDFUtils.addResourceToModel(theModel, theUri, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER, theModel.createResource(LTPUtils.resolveDoiPid(doi)));

    return RDFConstants.DOI_PREFIX + ":" + doi;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param pidSecret
   */
  public void setPidSecret(String pidSecret) {
    this.pidSecret = pidSecret;
  }

  /**
   * @param failsafeDelay
   */
  public void setFailsafeDelay(long failsafeDelay) {
    this.failsafeDelay = failsafeDelay;
  }

  /**
   * @param failsafeMaxRetries
   */
  public void setFailsafeMaxRetries(int failsafeMaxRetries) {
    this.failsafeMaxRetries = failsafeMaxRetries;
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Wrapper class for using failsafe with PID services GetDariahDoi() method.
   * </p>
   * 
   * @see https://github.com/jhalterman/failsafe
   */

  private class TGPidServiceImplWrapper extends Thread {

    /**
     * 
     */
    public TGPidServiceImplWrapper() {}

    /**
     * 
     */
    public class GetDariahDoi implements Callable<String> {
      private String pid;
      private String doiMeta;
      private String auth;

      /**
       * @param pid
       * @param doiMeta
       * @param auth
       * @throws AuthenticationException
       * @throws IOException
       */
      public GetDariahDoi(String pid, String doiMeta, String auth)
          throws AuthenticationException, IOException {
        this.pid = pid;
        this.doiMeta = doiMeta;
        this.auth = auth;
      }

      /*
       * (non-Javadoc)
       * 
       * @see java.util.concurrent.Callable#call()
       */
      @Override
      public String call() throws Exception {
        String result = "";

        try {
          result = DHPublishAbs.getPidService().getDariahDoi(this.pid, this.doiMeta, this.auth);
        } catch (Exception e) {
          throw e;
        }

        return result;
      }
    }

  }

}
