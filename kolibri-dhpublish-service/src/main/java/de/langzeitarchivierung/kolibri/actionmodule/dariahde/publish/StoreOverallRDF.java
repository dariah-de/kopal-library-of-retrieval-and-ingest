/*******************************************************************************
 * This software is copyright (c) 2019 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.dariahde.publish;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.HashMap;
import org.apache.jena.rdf.model.Model;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.RedisDB;
import de.langzeitarchivierung.kolibri.webservice.dariahde.publish.DHPublishServiceImpl;
import info.textgrid.middleware.common.DariahStorageClient;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import jakarta.ws.rs.core.MediaType;

/**
 * <p>
 * Stores a copy of the overall RDF model to DARIAH-DE OwnStorage.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2019-03-29
 * @since 2017-11-17
 */

public class StoreOverallRDF implements ActionModule {

  // **
  // STATE
  // **

  ProcessData processData;
  Step step;
  private String ownStorageEndpoint = "https://de.dariah.eu/storage/";
  private boolean dryRun;
  private DariahStorageClient storageClient = new DariahStorageClient();

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>...</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>...</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Get the data out of the custom data.
    HashMap<Object, Object> customData = this.processData.getCustomData();

    String uri = (String) customData.get(DHPublishService.STORAGE_ID);
    String collectionLogID = (String) customData.get(DHPublishService.TRANSACTION_ID);
    String storageToken = (String) customData.get(DHPublishService.STORAGE_TOKEN);
    // String seafileToken = (String) customData.get(DHPublishService.SEAFILE_TOKEN);
    PublishResponse publishResponse =
        (PublishResponse) customData.get(DHPublishServiceImpl.PUBLISH_RESPONSE);
    Model model = (Model) customData.get(uri);
    this.dryRun = publishResponse.dryRun;

    // Get collection log ID (if existing).
    String coLogIDString = DHPublishUtils.getLogIDString(collectionLogID);

    // Start log entry. Set message also for current task info.
    String message = coLogIDString + "Storing overall RDF model to DARIAH-DE OwnStorage";
    PublishUtils.setStatusRunning(this.step, message, publishResponse, this, message, this.dryRun);

    // Store model to OwnStorage.
    DariahStorageClient ownStorageClient =
        this.storageClient.setStorageUri(URI.create(this.ownStorageEndpoint));

    String id = "";
    try {
      byte modelStream[] = RDFUtils.getTTLFromModel(model).getBytes();
      InputStream is = new ByteArrayInputStream(modelStream);
      id = ownStorageClient.createFile(is, MediaType.TEXT_PLAIN, storageToken);
      is.close();
    } catch (IOException e) {
      message = coLogIDString + "Storing overall RDF model to OwnStorage failed! Storage says: "
          + e.getClass().getName() + ": " + e.getMessage();
      PublishUtils.addGeneralError(this.step, ErrorType.NOT_SPECIFIED, message, publishResponse,
          this, this.dryRun);
      PublishUtils.updateResponse(uri, publishResponse);
      return;
    }

    // Set model OwnStorage ID to REDIS DB.
    RedisDB.addModelID(uri, id);

    // Storing model complete.
    message = coLogIDString + "Overall data submission complete! ID of RDF model is " + id;
    PublishUtils.setStatusDone(this.step, message, publishResponse, this, this.dryRun);

    // Finally write publish responses back to custom data.
    PublishUtils.updateResponse(uri, publishResponse);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setProcessData
   * (de.langzeitarchivierung.kolibri.ProcessData)
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#setStep(de.
   * langzeitarchivierung.kolibri.Step)
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param ownStorageEndpoint
   */
  public void setOwnStorageEndpoint(String ownStorageEndpoint) {
    this.ownStorageEndpoint = ownStorageEndpoint;
  }

}
