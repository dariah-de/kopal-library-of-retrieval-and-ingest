/*******************************************************************************
 * This software is copyright (c) 2017 by
 * 
 *  DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.publish.util;

import java.net.URI;

/*******************************************************************************
 * <p>
 * Utility class for ElasticSearch in DH-publish.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2015-03-30
 * @since 2014-03-12
 ******************************************************************************/

public class DHElasticSearchUtils extends ElasticSearchUtils {

	// **
	// MANIPULATION
	// **

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.langzeitarchivierung.kolibri.publish.util.ElasticSearchUtilsAbs#
	 * forgetUriPrefix(java.net.URI)
	 */
	public String forgetUriPrefix(URI theUri) {
		return DHPublishUtils.getStorageId(theUri.toString());
	}

}
