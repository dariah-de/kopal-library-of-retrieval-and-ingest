package de.langzeitarchivierung.kolibri.publish.util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * - #%L DARIAHDE :: Publikator Standalone %% Copyright (C) 2017 SUB Göttingen
 * (https://www.sub.uni-goettingen.de) %% Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License. #L%
 * 
 * @author Johannes
 */
public class RedisPool {

  // **
  // STATICS
  // **

  private static RedisPool instance;
  private static JedisPool pool;

  // **
  // CLASS
  // **

  private int redisDatabaseNo = 1;

  // **
  // CONSTRUCTOR
  // **

  /**
   * 
   */
  private RedisPool() {}

  /**
   * @return the redis pool instance
   */
  public static final RedisPool getInstance() {

    if (instance == null) {
      RedisPool.instance = new RedisPool();
    }

    return RedisPool.instance;
  }

  /**
   * @param theHost
   * @param thePort
   * @param theMaxParallel
   * @param theRedisDatabaseNo
   */
  public void connect(final String theHost, final int thePort,
      final int theMaxParallel, final int theRedisDatabaseNo) {

    this.redisDatabaseNo = theRedisDatabaseNo;

    JedisPoolConfig poolConfig = new JedisPoolConfig();

    // for a list of options see
    // https://commons.apache.org/proper/commons-dbcp/configuration.html

    // The maximum number of active connections that can be allocated from
    // this pool at the same time, or negative for no limit.
    poolConfig.setMaxTotal(theMaxParallel);
    // Tests whether connection is dead when connection retrieval method is
    // called, should be checked, else the pool might run out of connections
    poolConfig.setTestOnBorrow(true);
    // Tests whether if connection is dead when returning to the pool
    poolConfig.setTestOnReturn(true);
    // Number of connections to Redis idling around
    poolConfig.setMaxIdle(5);
    // Minimum number of idle connections to Redis (always idling)
    poolConfig.setMinIdle(1);
    // Tests if connections are dead during idle periods
    poolConfig.setTestWhileIdle(true);
    // Maximum number of connections to test in each idle check
    poolConfig.setNumTestsPerEvictionRun(10);
    // Idle connection checking period
    poolConfig.setTimeBetweenEvictionRunsMillis(60000);
    // Create the jedisPool
    pool = new JedisPool(poolConfig, theHost, thePort);
  }

  /**
   * 
   */
  public void disconnect() {
    pool.destroy();
  }

  /**
   * @return the jedis client
   */
  public Jedis getJedis() {

    Jedis result = null;

    if (pool != null) {
      result = pool.getResource();
      if (result.getDB() != this.redisDatabaseNo) {
        result.select(this.redisDatabaseNo);
      }
    }

    return result;
  }

}
