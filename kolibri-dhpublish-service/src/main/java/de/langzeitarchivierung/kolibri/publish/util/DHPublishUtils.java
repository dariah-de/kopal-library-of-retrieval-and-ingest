/**
 * This software is copyright (c) 2021 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.publish.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.Adler32;
import javax.naming.AuthenticationException;
import javax.xml.stream.XMLStreamException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.dariahde.publish.DHPublishAbs;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import jakarta.ws.rs.core.Response;

/**
 * <p>
 * Utility class for general DH-publish issues.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-03-12
 * @since 2014-03-12
 */

public class DHPublishUtils {

  // **
  // STATIC FINAL
  // **

  public static final String HTTP = "http://";
  public static final String HTTPS = "https://";

  public static final String ELEM_RDF_UC = "RDF";
  public static final String ELEM_ORE = "ore";
  public static final String ELEM_ORE_DESCRIPTION = "Description";
  public static final String ELEM_AGGREGATES = "aggregates";

  public static final String ORE_NAMESPACE = "http://www.openarchives.org/ore/terms/";

  private static final String CR_ID_SEPARATOR = "/";
  private static final String CR_URL_STRT = "<url>";
  private static final String CR_URL_END = "</url>";

  public static final boolean INCREASE_PROGRESS = true;
  public static final boolean DONT_INCREASE_PROGRESS = false;
  public static final boolean UNTRUSTED = true;

  public static final int PROGRESS_UPDATES_PER_MODULE = 1;

  // **
  // STATICS
  // **

  protected static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Gets the DARIAH storage ID out of the DARIAH Storage location, if location given.
   * </p>
   * 
   * @param theId
   * @return the storage id
   */
  public static String getStorageId(final String theId) {
    if ((theId.startsWith(HTTP) || theId.startsWith(HTTPS)) && theId.lastIndexOf("/") != -1) {
      return theId.substring(theId.lastIndexOf("/") + 1);
    } else {
      return theId;
    }
  }

  /**
   * <p>
   * Gets the PID out of the DH-crud's location URL.
   * </p>
   * 
   * @param theLocation
   * @return the pid
   */
  public static String getPidFromLocation(final String theLocation) {
    return theLocation.substring(
        theLocation.lastIndexOf("/", theLocation.lastIndexOf("/") - 1) + 1);
  }

  /**
   * FIXME Check if we can use this method also in other modules!!
   * 
   * FIXME Generalise with/use dhcrud's RDF utils!!
   * 
   * @param theModel
   * @param theUri
   * @param theNamespace
   * @param theLocalName
   * @return the statement list
   */
  public static List<String> getStatementsFromRdfModel(final Model theModel, final String theUri,
      final String theNamespace, final String theLocalName) {

    List<String> result = new ArrayList<String>();

    // Loop all the statements for URI.
    StmtIterator iter = theModel.listStatements(theModel.getResource(theUri), null, (RDFNode) null);

    // Find statements and add to result list.
    while (iter.hasNext()) {
      Statement s = iter.next();
      if (s.getPredicate().getNameSpace().equals(theNamespace)
          && s.getPredicate().getLocalName().equals(theLocalName)) {
        result.add(s.getString());
      }
    }

    return result;
  }

  /**
   * <p>
   * Creates RDF model from given root model for given URI, only adds needed metadata for the
   * Collection Registry.
   * </p>
   * 
   * @param theModel
   * @param theUri
   * @param thePid
   * @return the model
   * @throws XMLStreamException
   * @throws IOException
   */
  public static Model createCollectionRegistryDcMetadata(final Model theModel, final String theUri,
      String thePid) throws XMLStreamException, IOException {

    // Create new resource name for new model.
    String resourceName = RDFUtils.findFirstObject(theModel, theUri, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER, RDFConstants.DOI_NAMESPACE);

    // At first get all DC metadata for the given URI.
    List<String> prefixes = new ArrayList<String>();
    prefixes.add(RDFConstants.DC_PREFIX);
    Model result =
        RDFUtils.getModelContainingOnlyLiterals(theModel, theUri, prefixes, true, resourceName);

    // URL-encode the "/" for correct CR handling.
    String encodedValue = DHPublishAbs.getRepositoryOaiEndpoint() + "/"
        + URLEncoder.encode(LTPUtils.omitHdlPrefix(thePid.toString()), "UTF-8");
    RDFUtils.addLiteralToModel(result, resourceName, RDFConstants.DC_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER, encodedValue);

    return result;
  }

  /**
   * <p>
   * Creates an aggregation model containing the dcterms:hasparts only as a collection data object.
   * </p>
   * 
   * @param theUris
   * @param theResource
   * @return the resulting model
   * @throws XMLStreamException
   * @throws IOException
   */
  public static Model createCollectionData(List<URI> theUris, String theResource)
      throws XMLStreamException, IOException {

    // Create new model.
    Model result = ModelFactory.createDefaultModel();

    // Add namespaces.
    result.setNsPrefix(RDFConstants.DARIAH_PREFIX, RDFConstants.DARIAH_NAMESPACE);
    result.setNsPrefix(RDFConstants.DARIAHSTORAGE_PREFIX, RDFConstants.DARIAHSTORAGE_NAMESPACE);
    result.setNsPrefix(RDFConstants.DCTERMS_PREFIX, RDFConstants.DCTERMS_NAMESPACE);
    result.setNsPrefix(RDFConstants.HDL_PREFIX, RDFConstants.HDL_NAMESPACE);

    // Add dariah:Collection.
    Resource dariahCollection = result.createResource(RDFConstants.DARIAH_COLLECTION);
    String collectionUri = LTPUtils.resolveHandlePid(theResource);
    result.createResource(collectionUri, dariahCollection);

    // Add dcterms:hasParts.
    RDFNode[] nodeArray = new RDFNode[theUris.size()];
    int count = 0;
    for (URI u : theUris) {
      if (u.toString().startsWith(RDFConstants.HDL_PREFIX)) {
        nodeArray[count] = result.createResource(LTPUtils.resolveHandlePid(u));
      } else {
        nodeArray[count] = result.createResource(u.toString());
      }
      count++;
    }

    RDFUtils.addResourceToModel(result, collectionUri, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DCTERMS_HASPART, result.createList(nodeArray));

    return result;
  }

  /**
   * <p>
   * Creates an URI list from an HTTP response. Response must be an InputStream!
   * </p>
   * 
   * @param theResponse
   * @return the list of URIs
   * @throws IOException
   * @throws URISyntaxException
   */
  public static List<URI> httpResponseInputStreamToURIList(Response theResponse)
      throws IOException, URISyntaxException {

    List<URI> response = new ArrayList<URI>();

    String s = IOUtils.readStringFromStream((InputStream) theResponse.getEntity());
    s = s.replaceAll("[\\[\\]]", "");
    String t[] = s.split(",");
    for (int i = 0; i < t.length; i++) {
      response.add(new URI(t[i].trim()));
    }

    return response;
  }

  /**
   * <p>
   * Creates RDF metadata model from given root model for given URI. Take PID as about attribute
   * instead of URI.
   * </p>
   * 
   * <p>
   * Changes nothing on the overall model!
   * </p>
   * 
   * @param theModel
   * @param theRootUri
   * @param thePid
   * @return the resulting model
   */
  public static Model createMetadata(final Model theModel, final String theRootUri,
      final String thePid) {

    // **
    // FIXME CreateMetadata should get ALL the metadata from the overall model for this resource,
    // and not forget anything! So we can put everything into the metadata that comes from the user!
    // We should only add things we need, if any!
    // **
    // TODO !! I think we already do this here! Please check !!
    // **

    // Create new model for given URIs RDF statements,
    Model result = ModelFactory.createDefaultModel();

    // Set rdf:about to PID.
    Resource r = result.createResource(LTPUtils.resolveHandlePid(thePid));

    // Copy namespaces, add needed, if not yet existing.
    result.setNsPrefixes(theModel.getNsPrefixMap());
    if (!result.getNsPrefixMap().containsKey(RDFConstants.DCTERMS_PREFIX)) {
      result.setNsPrefix(RDFConstants.DCTERMS_PREFIX, RDFConstants.DCTERMS_NAMESPACE);
    }
    if (!result.getNsPrefixMap().containsKey(RDFConstants.DARIAH_PREFIX)) {
      result.setNsPrefix(RDFConstants.DARIAH_PREFIX, RDFConstants.DARIAH_NAMESPACE);
    }
    if (!result.getNsPrefixMap().containsKey(RDFConstants.HDL_PREFIX)) {
      result.setNsPrefix(RDFConstants.HDL_PREFIX, RDFConstants.HDL_NAMESPACE);
    }

    // Loop all the statements for URI in given model.
    StmtIterator iter =
        theModel.listStatements(theModel.getResource(theRootUri), null, (RDFNode) null);
    while (iter.hasNext()) {
      Statement s = iter.next();
      r.addProperty(s.getPredicate(), s.getObject());
    }

    return result;
  }

  /**
   * <p>
   * Creates RDF metadata model for a collection. Adds dc:format, dcterms:format, and dcterms:source
   * for root collections, and prefixes.
   * </p>
   * 
   * <p>
   * Adds some things to the overall model!
   * </p>
   * 
   * @param theModel
   * @param theResource
   * @param theSourceResource
   * @return the resulting model
   */
  public static Model createCollectionMetadata(final Model theModel,
      final String theResource, final String theSourceResource) {

    // **
    // FIXME CreateMetadata should get ALL the metadata from the overall model for this resource,
    // and not forget anything! So we can put everything into the metadata that comes from the user!
    // We should only add things we need, if any!
    // **
    // TODO !! I think we already do this here! Please check !!
    // **

    // First add the dcterms:format to theModel(!), we take the TextGrid Collection format string!
    // Put in a dcterms for administrative metadata and a dc:format for descriptive metadata!
    RDFUtils.addLiteralToModel(theModel, theSourceResource, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_FORMAT, TextGridMimetypes.DARIAH_COLLECTION);
    RDFUtils.addLiteralToModel(theModel, theSourceResource, RDFConstants.DC_PREFIX,
        RDFConstants.ELEM_DC_FORMAT, TextGridMimetypes.DARIAH_COLLECTION);

    // Then add the dcterms:source to theModel(!), we take the DARIAH-DE OwnStorage ID(!) from the
    // initial RDF object and the OwnStorage endpoint(!)! So we can get the PID for the initial URI
    // later from the PID service, just in case the endpoint differs from the storage namespace.
    String sourceUri =
        DHPublishAbs.getOwnStorageEndpoint() + DHPublishUtils.getStorageId(theSourceResource);
    RDFUtils.addToModel(theModel, theSourceResource, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_SOURCE, sourceUri);

    // Now create new model for given URIs RDF statements,
    Model result = ModelFactory.createDefaultModel();

    // Set rdf:about to PID.
    Resource r = result.createResource(LTPUtils.resolveHandlePid(theResource));

    // Copy namespaces, add needed to result model, if not yet existing.
    result.setNsPrefixes(theModel.getNsPrefixMap());
    if (!result.getNsPrefixMap().containsKey(RDFConstants.DC_PREFIX)) {
      result.setNsPrefix(RDFConstants.DC_PREFIX, RDFConstants.DC_NAMESPACE);
    }
    if (!result.getNsPrefixMap().containsKey(RDFConstants.DCTERMS_PREFIX)) {
      result.setNsPrefix(RDFConstants.DCTERMS_PREFIX, RDFConstants.DCTERMS_NAMESPACE);
    }
    if (!result.getNsPrefixMap().containsKey(RDFConstants.DARIAH_PREFIX)) {
      result.setNsPrefix(RDFConstants.DARIAH_PREFIX, RDFConstants.DARIAH_NAMESPACE);
    }
    if (!result.getNsPrefixMap().containsKey(RDFConstants.HDL_PREFIX)) {
      result.setNsPrefix(RDFConstants.HDL_PREFIX, RDFConstants.HDL_NAMESPACE);
    }

    // Loop all the statements for URI, take care of RDF:Collection.
    StmtIterator iter =
        theModel.listStatements(theModel.getResource(theSourceResource), null, (RDFNode) null);
    while (iter.hasNext()) {
      Statement s = iter.next();

      if (s.getObject().isAnon()) {
        // ADD new list.
        List<String> l = RDFUtils.getRDFList(s.getObject());
        RDFNode[] newList = new RDFNode[l.size()];
        int count = 0;
        for (String str : l) {
          newList[count] = result.createResource(str);
          count++;
        }
        r.addProperty(s.getPredicate(), result.createList(newList));
      } else {
        // Add single object.
        r.addProperty(s.getPredicate(), s.getObject());
      }
    }

    return result;
  }

  /**
   * @param theModel
   * @param theResource
   * @param thePrefix
   * @param theElement
   * @return the hasPart list
   * @throws XMLStreamException
   * @throws IOException
   */
  public static List<URI> getHasPartPIDsFromRdfModel(Model theModel, String theResource,
      String thePrefix, String theElement) throws XMLStreamException, IOException {

    List<URI> result = new ArrayList<URI>();

    // Get all hasParts first.
    List<String> hasParts =
        RDFUtils.getProperties(theModel, theResource, RDFConstants.DCTERMS_PREFIX,
            RDFConstants.ELEM_DCTERMS_HASPART);

    // Loop all hasParts URIs and get dcterms:identifier with prefix hdl.
    for (String str : hasParts) {
      result.add(URI.create(
          RDFUtils.findFirstObject(theModel, str, thePrefix, theElement,
              RDFConstants.HDL_NAMESPACE)));
    }

    return result;
  }

  /**
   * <p>
   * Extracts the collection's ID from a collection URL.
   * </p>
   * 
   * @return the collection ID
   */
  public static String extractCollectionId(String theCollectionUrl) {
    return theCollectionUrl
        .substring(theCollectionUrl.lastIndexOf(CR_ID_SEPARATOR) + CR_ID_SEPARATOR.length());
  }

  /**
   * <p>
   * Using Jettison for parsing JSON. We are getting something like this:
   * {"responseCode":1,"handle":"11022/0000-0000-8487-2",
   * "values":[{"index":14,"type":"COLREG_ID","data":{"format":"string",
   * "value":"5746ff8a7c8dec05e679dece"},"ttl":86400,"timestamp": "2015-08-18T13:11:56Z"}]}
   * </p>
   * 
   * @param thePidServiceResponse
   * @param theKey
   * @return the PID value
   */
  public static String extractPidValue(String thePidServiceResponse, String theKey) {

    String result = "";

    if (thePidServiceResponse != null && !"".equals(thePidServiceResponse)) {

      try {
        JSONObject jo = new JSONObject(thePidServiceResponse);
        if (jo.has("values")) {
          JSONArray ja = jo.getJSONArray("values");
          for (int i = 0; i < ja.length(); i++) {
            if (ja.getJSONObject(i).has("type") && ja.getJSONObject(i).get("type").equals(theKey)) {
              result = ja.getJSONObject(i).getJSONObject("data").getString("value");
            }
          }
        }
      } catch (JSONException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    return result;
  }

  /**
   * <p>
   * Call before exit:
   * 
   * <ul>
   * <li>Check if there are errors to set module state before returning - update response.</li>
   * </ul>
   * </p>
   * 
   * @param theUri
   * @param theModule
   * @param anyError
   * @param theStep
   * @param dryRun
   * @param theResponse
   * @param theMessage
   */
  public static void finish(final String theUri, ActionModule theModule, final boolean anyError,
      Step theStep, final boolean dryRun, PublishResponse theResponse, String theMessage) {

    if (anyError) {
      PublishUtils.setStatusError(theStep, "Finished with Error", theResponse, theModule,
          INCREASE_PROGRESS, dryRun);
    } else {
      PublishUtils.setStatusDone(theStep, theMessage, theResponse, theModule, dryRun);
    }

    // Set objectListComplete flag for flagging the list is ready to deliver publish status now
    // (means no objects are added anymore to the list!).
    theResponse.objectListComplete = true;

    // Write response to the response.
    PublishUtils.updateResponse(theUri, theResponse);
  }

  /**
   * <p>
   * Add error to response and set status of action module to error.
   * </p>
   * 
   * @param thePublishObject
   * @param theStep
   * @param anyError
   * @param theType
   * @param theMessage
   * @param increaseProgressOrDontIncreaseProgress
   * @param theResponse
   * @param theModule
   * @param dryRun
   */
  public static void setError(PublishObject thePublishObject, Step theStep, final boolean anyError,
      final ErrorType theType, final String theMessage,
      final boolean increaseProgressOrDontIncreaseProgress, PublishResponse theResponse,
      ActionModule theModule, final boolean dryRun) {

    thePublishObject.addError(theType, theMessage);
    PublishUtils.setStatusError(theStep, theMessage, theResponse, theModule,
        increaseProgressOrDontIncreaseProgress, dryRun);
  }

  /**
   * <p>
   * Just sets the error and calls finish!
   * </p>
   * 
   * @param thePublishObject
   * @param theModule
   * @param anyError
   * @param theStep
   * @param dryRun
   * @param theResponse
   * @param theMessage
   * @param theError
   */
  public static void finishWithError(PublishObject thePublishObject, ActionModule theModule,
      final boolean anyError, Step theStep, final boolean dryRun, PublishResponse theResponse,
      String theMessage, ErrorType theError) {

    DHPublishUtils.setError(thePublishObject, theStep, anyError, theError, theMessage,
        DONT_INCREASE_PROGRESS, theResponse, theModule, dryRun);
    DHPublishUtils.finish(thePublishObject.uri, theModule, anyError, theStep, dryRun, theResponse,
        theMessage);
  }

  /**
   * <p>
   * Get all about attribute from the RDF model.
   * </p>
   * 
   * @param theModel
   * @return the list of all about statements
   */
  public static List<String> getAllAboutsFromModel(Model theModel) {
    return RDFUtils.findAllSubjects(theModel, "",
        RDFConstants.RDF_NAMESPACE + RDFConstants.ELEM_DC_TYPE);
  }

  /**
   * <p>
   * Extracts all DC metadata for a certain subject.
   * </p>
   * 
   * @param theModel
   * @param theUri
   * @return the resulting model
   */
  public static Model getDcMetadata(Model theModel, String theUri) {

    // Create new model for given URIs RDF statements.
    Model result = ModelFactory.createDefaultModel();

    // Add namespaces.
    result.setNsPrefix(RDFConstants.DC_PREFIX, theModel.getNsPrefixURI(RDFConstants.DC_PREFIX));
    result.setNsPrefix(RDFConstants.DARIAH_PREFIX,
        theModel.getNsPrefixURI(RDFConstants.DARIAH_PREFIX));
    result.setNsPrefix(RDFConstants.DARIAHSTORAGE_PREFIX,
        theModel.getNsPrefixURI(RDFConstants.DARIAHSTORAGE_PREFIX));

    // Loop all the statements for URI.
    StmtIterator iter = theModel.listStatements(theModel.getResource(theUri), null, (RDFNode) null);

    // Use all DC metadata from the given model.
    while (iter.hasNext()) {
      Statement s = iter.next();
      if (s.getPredicate().getNameSpace().equals(result.getNsPrefixURI(RDFConstants.DC_PREFIX))) {
        result.add(s);
      }
    }

    // Add DARIAH type dariah:Collection or dariah:DataObject.
    if (RDFUtils.findAllObjects(result, theUri, RDFConstants.DC_PREFIX, RDFConstants.ELEM_DC_FORMAT)
        .contains(TextGridMimetypes.DARIAH_COLLECTION)) {
      result.getResource(theUri).addProperty(theModel.createProperty(RDFConstants.RDF_TYPE),
          theModel.createProperty(RDFConstants.DARIAH_COLLECTION));
    } else {
      result.getResource(theUri).addProperty(theModel.createProperty(RDFConstants.RDF_TYPE),
          theModel.createProperty(RDFConstants.DARIAH_DATAOBJECT));
    }

    return result;
  }

  /**
   * <p>
   * Returns the administrative PID (the Handle) in prefix notation, such as
   * hdl:21.T11998/0000-0007-0805-B.
   * </p>
   * 
   * @param theModel
   * @param theResource
   * @return the administrative PID
   */
  public static String getAdministrativePid(Model theModel, String theResource) {

    String result = "";

    // Look for administrative Handle PID using HDL namespace.
    result = RDFUtils.findFirstObject(theModel, theResource, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER, RDFConstants.HDL_NAMESPACE);

    // Exchange HDL namespace with HDL prefix.
    // FIXME Use RDF Jena methods for doing this! But maybe that just isn't working because of the
    // ugly "/" in the HDL and DOI PIDs!
    result = result.replace(RDFConstants.HDL_NAMESPACE, RDFConstants.HDL_PREFIX + ":");

    return result;
  }

  /**
   * @param theRootCollectionPid
   * @return the root collection entry
   */
  public static String getRootCollectionEntry(String theRootCollectionPid) {
    return RDFConstants.DARIAH_COLLECTION + "/" + LTPUtils.omitHdlPrefix(theRootCollectionPid);
  }

  /**
   * <p>
   * Checks if the object already has been published in the DARIAH Repository.
   * </p>
   * 
   * @param thePid
   * @return is the object published or not published?
   * @throws AuthenticationException
   * @throws IOException
   */
  public static boolean checkPublished(final String thePid)
      throws AuthenticationException, IOException {

    boolean result = false;

    long start = System.currentTimeMillis();

    // Get published value for given PID, if existing.
    if (thePid != null && !"".equals(thePid)) {

      result = DHPublishAbs.getPidService().publishedDariahPid(thePid, DHPublishAbs.getPidSecret());

      defaultLogger.log(Level.FINE, "Called PidService#publishedDariahPid: " + thePid + "=" + result
          + "  -->  " + (System.currentTimeMillis() - start) + "ms");

      // FIXME Catch WebApplicationException as soon as implemented in DH-pid!
    }

    return result;
  }

  /**
   * <p>
   * Adds host to given ID.
   * </p>
   * 
   * @param uri
   * @return The adapted URI.
   */
  public static String adaptUri(final String theUri) {

    String result = theUri;

    // Correct the URI if necessary: Check for missing missing "//".
    if (result.startsWith("http:/") && !result.contains("http://")) {
      result = result.replaceFirst("http:/", "http://");

      defaultLogger.log(Level.FINER, "URI HTTP replacement workaround: " + theUri + " = " + result);
    } else if (result.startsWith("https:/") && !result.contains("https://")) {
      result = result.replaceFirst("https:/", "https://");

      defaultLogger.log(Level.FINER,
          "URI HTTPS replacement workaround: " + theUri + " = " + result);
    }

    // Add OwnStorage or (FIXME: Seafile!) namespace (not endpoint!) to URI, if not already
    // existing.
    if (!theUri.startsWith("http://") || !theUri.startsWith("https://")) {
      result = RDFConstants.DARIAHSTORAGE_NAMESPACE + theUri;

      defaultLogger.log(Level.FINER,
          "URI completed with storage namespace: " + theUri + " = " + result);
    }

    // Finally check if the given URI is a valid URL.
    try {
      result = new URL(result).toExternalForm();
    } catch (MalformedURLException e) {
      // TODO Do we need more error handling here?
      // TODO And do we really need this check, for we only handle collection URIs here?
      defaultLogger.log(Level.WARNING,
          "The given URI " + result + " cannot be URLifed! Please check syntax!");
    }

    return result;
  }

  /**
   * <p>
   * Get URL out of response (from <url> tag).
   * </p>
   * 
   * @param theCRXML
   * @return the CR ID from the CR response
   */
  public static String getCRIDFromCRResponse(String theCRXML) {

    String result = "";

    // TODO Use XML to retrieve response URL, not string processing... I'll do it just tomorrow!!
    int urlStrt = theCRXML.indexOf(CR_URL_STRT);
    int urlEnd = theCRXML.indexOf(CR_URL_END);
    if (urlStrt != -1 && urlEnd != -1) {
      result = DHPublishUtils
          .extractCollectionId(theCRXML.substring(urlStrt + CR_URL_STRT.length(), urlEnd));
    }

    return result;
  }

  /**
   * @param thePlainLogID
   * @return the combined log ID
   */
  public static String createCombinedLogID(String thePlainLogID) {
    return thePlainLogID + "#" + createRandomLogId();
  }

  /**
   * @param theLogID
   * @return the log ID string
   */
  public static String getLogIDString(String theLogID) {
    return "[" + theLogID + "] ";
  }

  /**
   * @param theLogID
   * @return the log ID, a random one, if not yet existing
   */
  public static String getLogID(String theLogID) {
    if (theLogID == null || theLogID.equals("")) {
      return createRandomLogId();
    }
    return theLogID;
  }

  // **
  // PRIVATE
  // **

  /**
   * <p>
   * Used for generating IDs for log messages.
   * </p>
   * 
   * @return a random log ID
   */
  private static String createRandomLogId() {

    Adler32 adler = new Adler32();
    Random rn = new Random();
    adler.update(rn.nextInt());

    return String.valueOf(adler.getValue());
  }

}
