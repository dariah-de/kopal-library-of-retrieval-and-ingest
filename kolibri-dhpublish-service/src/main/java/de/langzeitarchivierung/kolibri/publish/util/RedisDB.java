/*******************************************************************************
 * This software is copyright (c) 2017 by
 * 
 *		DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *		http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.publish.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import redis.clients.jedis.Jedis;

/**
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2017-11-28
 * @since 2017-08-08
 */
public class RedisDB {

	private static Logger		defaultLogger	= Logger
			.getLogger("de.langzeitarchivierung.kolibri");
	private static final String	HDL				= "hdl_";
	private static final String	DOI				= "doi_";
	private static final String	RDF				= "rdf_";
	private static final String	CRID			= "crid_";

	/**
	 * @param theUri
	 * @param thePid
	 */
	public static void addHDL(final String theUri, final String thePid) {
		addValue(theUri, HDL, thePid);
	}

	/**
	 * @param theUri
	 * @return The Handle.
	 */
	public static String getHDL(final String theUri) {
		return getValue(theUri, HDL);
	}

	/**
	 * @param theUri
	 * @param theDoi
	 */
	public static void addDOI(final String theUri, final String theDoi) {
		addValue(theUri, DOI, theDoi);
	}

	/**
	 * @param theUri
	 * @return The DOI.
	 */
	public static String getDOI(final String theUri) {
		return getValue(theUri, DOI);
	}

	/**
	 * @param theUri
	 * @param theModelID
	 */
	public static void addModelID(final String theUri,
			final String theModelID) {
		addValue(theUri, RDF, theModelID);
	}

	/**
	 * @param theUri
	 * @return The RDF model's OwnStorage ID.
	 */
	public static String getModelID(final String theUri) {
		return getValue(theUri, RDF);
	}

	/**
	 * @param theUri
	 * @param theCRID
	 */
	public static void addCRID(final String theUri, final String theCRID) {
		addValue(theUri, CRID, theCRID);
	}

	/**
	 * @param theUri
	 * @return The Collection Registry's ID.
	 */
	public static String getCRID(final String theUri) {
		return getValue(theUri, CRID);
	}

	// **
	// PRIVATE METHODS
	// **

	/**
	 * @param theUri
	 * @param theCRID
	 */
	private static void addValue(final String theUri, final String theKeyPrefix,
			final String theValue) {

		String key = theKeyPrefix + theUri;

		Jedis j = RedisPool.getInstance().getJedis();
		if (j != null) {
			j.set(key, theValue);
		}
		j.close();

		defaultLogger.log(Level.FINEST, "Set REDIS: " + key + "=" + theValue);
	}

	/**
	 * @param theUri
	 * @return The Collection Registry's ID.
	 */
	private static String getValue(final String theUri,
			final String theKeyPrefix) {

		String result = "";

		String key = theKeyPrefix + theUri;

		Jedis j = RedisPool.getInstance().getJedis();
		if (j != null) {
			result = j.get(key);
			result = (result == null ? "" : result);
		}
		j.close();

		defaultLogger.log(Level.FINEST, "Get REDIS: " + key + "=" + result);

		return result;
	}

}
