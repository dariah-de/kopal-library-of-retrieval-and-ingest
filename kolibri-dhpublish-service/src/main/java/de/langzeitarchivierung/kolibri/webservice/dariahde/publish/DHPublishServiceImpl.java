/**
 * This software is copyright (c) 2023 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.webservice.dariahde.publish;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.AuthenticationException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.langzeitarchivierung.kolibri.DHPublishServiceVersion;
import de.langzeitarchivierung.kolibri.WorkflowTool;
import de.langzeitarchivierung.kolibri.processstarter.PublishAbs;
import de.langzeitarchivierung.kolibri.processstarter.dariahde.publish.DHPublish;
import de.langzeitarchivierung.kolibri.processstarter.dariahde.publish.DHPublishAbs;
import de.langzeitarchivierung.kolibri.publish.util.DHPublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.PublishUtils;
import de.langzeitarchivierung.kolibri.publish.util.RedisDB;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import info.textgrid.middleware.common.DariahStorageClient;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.json.AuthInfo;
import info.textgrid.middleware.tgpublish.api.DHPublishService;
import info.textgrid.middleware.tgpublish.api.jaxb.InfoResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.InfoType;
import info.textgrid.middleware.tgpublish.api.jaxb.ProcessStatusType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishStatus;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 *
 * 2023-06-29 - Funk - Add CRID again!
 *
 * 2023-05-24 - Funk - Remove REGISTERED info.
 *
 * 2020-12-14 - Funk - Remove objectListComplete flag conditional from method getPublishResponse().
 * We already have a publish response with process status QUEUED if object list is NOT yet complete.
 *
 * 2020-12-03 - Funk - Add QUEUED status for newly added objects. Makes it possible to check if we
 * have got more concurrently running processes with the same ID. Fixes #34300.
 *
 * 2020-10-19 - Funk - Remove TOKEN EXPIRATION HTTP BAD REQUEST, we can NOT send a warning only, so
 * we MUST accept a valid token!
 * 
 * 2020-10-19 - Funk - Add check for tokens that are valid indefinitely (expiration time is 0).
 * 
 * 2020-06-25 - Funk - Remove special header.
 * 
 * 2019-05-15 - Funk - Add check for token expiration.
 * 
 * 2019-05-14 - Funk - Get ePPN from storage now.
 *
 * 2019-02-26 - Funk - Adapting to storage token using OAuth2.
 * 
 * 2017-12-01 - Funk - Using new storage client.
 * 
 * 2017-11-20 - Funk - Added header logID param.
 * 
 * 2017-11-13 - Funk - Adapted auth headers to newly designed API.
 * 
 * 2017-10-31 - Funk - Added errorTimeout.
 * 
 * 2017-09-07 - Funk - Added PUBLISH_TOKEN for DH-publish API usage.
 * 
 * 2017-07-17 - Funk - PID search gives 404: No PID existing yet, no error!
 * 
 * 2017-06-28 - Funk - Adapted to JAXRS API 2.0.x.
 * 
 * 2017-06-02 - Funk - Added some error handling.
 * 
 * 2017-05-24 - Funk - Refactored PID service calls.
 * 
 * 2017-02-01 - Funk - Added storage NAMESPACE (NOT endpoint!) to URI if not already contained in
 * given URI!
 * 
 * 2017-01-18 - Funk - Implemented OAuth tokens.
 * 
 * 2016-07-28 - Funk - Added timeout to CR calls.
 *
 * 2016-07-21 - Funk - Using adaptUri() again... we do need it!
 * 
 * 2016-07-19 - Funk - Removed adaptation of URIs. Added InfoResponse caching.
 * 
 * 2016-05-02 - Funk - Refactored info response handling.
 * 
 * 2016-03-08 - Funk - Added tasks to info response.
 * 
 * 2016-02-12 - Funk - Improved InfoStatusResponse caching added!
 * 
 * 2015-04-14 - Funk - Caching the PIDs and published values retrieved from the PID service.
 * 
 * 2015-04-02 - Funk - Added REGISTERED request and response.
 * 
 * 2015-03-23 - Funk - Added DH-pid service and method calls.
 * 
 * 2015-03-20 - Funk - Added all the info status types.
 * 
 * 2014-12-16 - Funk - Copied from TGPublishServiceImpl.
 */

/**
 * <p>
 * The DHPublishService implementation class. Please configure the WorkflowTool config file using
 * the beans.xml.
 * </p>
 * 
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2023-06-29
 * @since 2014-12-16
 */

public class DHPublishServiceImpl implements DHPublishService {

  // **
  // STATIC FINALS
  // **

  public static final String PUBLISH_RESPONSE = "publishResponse";
  public static final String INFO_RESPONSE = "infoResponse";
  public static final String POLICY_NAME = "policyName";
  public static final String COLREG_ID = "COLREG_ID";
  public static final String DARIAH_EPPN = "eppn";

  private static final String CONFIG_ERROR_MESSAGE =
      "Error configuring DH-publish service due to a ";
  private static final String RUN_ERROR_MESSAGE = "Error running DH-publish service due to a ";
  private static final String PUBLICH_CHECK_ERROR_MESSAGE = "Published check failed due to a ";
  private static final String NO_PID = null;
  private static final String NOT_AUTHORISED = "NOT AUTHORISED!";
  private static final String STORAGE_SERVICE_ERROR = "ERROR ACCESSING THE STORAGE SERVICE!";
  private static final String NO_EPPN =
      "The user's ePPN coud not be extracted from the storage info call!";
  private static final String TOKEN_EXPIRES_INFO = "Your storage token is about to expire in ";
  private static final String CALLING = "Calling ";

  // **
  // STATICS
  // **

  protected static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");
  private static DariahStorageClient storageClient = new DariahStorageClient();
  private static ObjectMapper jsonObjectMapper = new ObjectMapper();

  // **
  // CLASS
  // **

  private static boolean maintenance = false;
  // For logging only: Set time to 1 hour.
  private static long warnOnTokenValidityLessThan = 1 * 1000 * 60 * 60;

  // **
  // CONSTRUCTOR
  // **

  /**
   * <p>
   * Starts the koLibRI WorkflowTool thread at service start.
   * </p>
   * 
   * @param theConfigFilename
   */
  public DHPublishServiceImpl(String theConfigFilename) {

    // Configure TG-publish service from beans.xml.
    if (!new File(theConfigFilename).exists()) {
      log(Level.SEVERE, "DH-publish config file not found: " + theConfigFilename
          + "! Please configure in beans.xml!");
      return;
    }

    log(Level.INFO, "Starting DH-publish WorkflowTool thread");

    new StartWorkflowTool(theConfigFilename).start();

    log(Level.INFO, "DH-publish WorkflowTool thread successfully started");
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.middleware.tgpublish.api.DHPublishService#getVersion()
   */
  @Override
  public String getVersion() {
    return DHPublishServiceVersion.SERVICENAME + "-" + DHPublishServiceVersion.VERSION + "+"
        + DHPublishServiceVersion.BUILDDATE;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.middleware.tgpublish.api.DHPublishService#publish(java.lang.String, boolean,
   * java.lang.String, java.lang.String, java.lang.String)
   */
  @Override
  public Response publish(final String uri, boolean dryRun, final String storageToken,
      final String seafileToken, final String logID) {

    // Adapt the given URI if needed.
    String collectionUri = DHPublishUtils.adaptUri(uri);

    // Check for null or empty logID.
    String processID = DHPublishUtils.getLogID(logID) + "#" + UUID.randomUUID();

    log(Level.INFO, CALLING + "DH-publish service [" + collectionUri + "]", processID);

    PublishResponse pResponse = null;
    synchronized (this) {
      // Check for other processes concerning this URI.
      boolean otherProcessesRunning = otherProcessesRunning(collectionUri, processID);

      log(Level.FINE, "Are other processes running? " + otherProcessesRunning);

      if (otherProcessesRunning) {
        String message = "Another publish process is running at the moment for [" + collectionUri
            + "]  -->  abort";
        log(Level.SEVERE, message, processID);
        return Response.status(Status.CONFLICT).entity(message).build();
      } else {
        String message = "No other publish process is running at the moment for [" + collectionUri
            + "]  -->  we can proceed";
        log(Level.INFO, message, processID);
      }

      // Create new existing publish response, add dry run and status QUEUED.
      // NOTE: objectListComplete sees that the publish workflow isn't started too early in the
      // process.
      pResponse = PublishAbs.getPublishResponse(collectionUri);
      if (pResponse == null) {
        pResponse = new PublishResponse();
        log(Level.FINE, "Publish response created");
      }
      pResponse.dryRun = dryRun;
      pResponse.getPublishStatus().activeModule = processID;
      pResponse.getPublishStatus().progress = 0;
      pResponse.getPublishStatus().processStatus = ProcessStatusType.QUEUED;

      PublishAbs.updatePublishResponseMap(collectionUri, pResponse);

      log(Level.FINE, "Process status type set to QUEUED");
    }

    // Authenticate.
    try {
      if (!authenticate(storageToken, collectionUri, CAN_PUBLISH, processID)) {
        // FIXME Set status to FAILED here?
        pResponse.getPublishStatus().processStatus = ProcessStatusType.FAILED;
        log(Level.SEVERE, NOT_AUTHORISED, processID);
        return Response.status(Status.UNAUTHORIZED).entity(NOT_AUTHORISED).build();
      }
    } catch (IOException e) {
      // FIXME Set status to FAILED here?
      pResponse.getPublishStatus().processStatus = ProcessStatusType.FAILED;
      log(Level.WARNING, STORAGE_SERVICE_ERROR, processID);
      return Response.status(Status.INTERNAL_SERVER_ERROR)
          .entity(STORAGE_SERVICE_ERROR + ": " + e.getMessage()).build();
    }

    // Get ePPN from storage and check content from token info response.
    String eppn = "";
    long tokenXpiresIn = 0;
    try {
      String tokenInfo = storageClient.authInfo(storageToken, processID);
      eppn = jsonObjectMapper.readValue(tokenInfo, AuthInfo.class).principal.name;
      tokenXpiresIn = jsonObjectMapper.readValue(tokenInfo, AuthInfo.class).expiresIn
          - System.currentTimeMillis();
      if (eppn == null || "".equals(eppn)) {
        log(Level.WARNING, NO_EPPN, processID);
        return Response.status(Status.BAD_REQUEST).entity(NO_EPPN).build();
      }
    } catch (IOException e) {
      String message = STORAGE_SERVICE_ERROR + ": " + e.getMessage();
      log(Level.WARNING, STORAGE_SERVICE_ERROR, processID);
      return Response.status(Status.INTERNAL_SERVER_ERROR).entity(message).build();
    }

    // Only log token expiration (we can not throw a warning using HTTP).
    if (tokenXpiresIn == 0) {
      log(Level.INFO, TOKEN_EXPIRES_INFO + " never never never!", processID);
    } else if (tokenXpiresIn < warnOnTokenValidityLessThan) {
      log(Level.INFO, TOKEN_EXPIRES_INFO + KolibriTimestamp.getDurationHMOnly(tokenXpiresIn),
          processID);
    } else {
      log(Level.FINER, TOKEN_EXPIRES_INFO + KolibriTimestamp.getDurationHMOnly(tokenXpiresIn),
          processID);
    }

    // Check for maintenance flag.
    if (maintenance) {
      String message =
          "The DH-publish service is in maintenance mode at the moment, please be patient and try publishing agein in a few minutes! Thank you very much!";
      log(Level.INFO, message, processID);
      return Response.status(Status.SERVICE_UNAVAILABLE).entity(message).build();
    }

    // Create a CustomData object, and set all the necessary parameters from the service call to the
    // customData of the processData element.
    HashMap<String, Object> customData = new HashMap<String, Object>();

    customData.put(PUBLISH_RESPONSE, pResponse);
    customData.put(DARIAH_EPPN, eppn);
    customData.put(STORAGE_ID, collectionUri);
    customData.put(STORAGE_TOKEN, storageToken);
    customData.put(SEAFILE_TOKEN, seafileToken);
    customData.put(TRANSACTION_ID, processID);

    String message =
        "CustomData object created: [" + PUBLISH_RESPONSE + "=" + pResponse + " | eppn=" + eppn
            + " | uri=" + collectionUri + " | dryRun=" + dryRun + " | logID=" + processID + "]";

    log(Level.INFO, message, processID);

    // Get the data queue.
    ArrayBlockingQueue<HashMap<String, Object>> dataQueue = DHPublish.getDataQueue();

    message = "DHPublishService#publish sucessfully started [" + collectionUri + "]";
    log(Level.INFO, message, processID);

    // Add custom data to process starter queue.
    try {
      if (dataQueue != null) {
        dataQueue.put(customData);
      } else {
        throw new IOException();
      }
    } catch (InterruptedException e) {
      message = "Data could not be added to data queue [" + collectionUri + "] due to a "
          + e.getClass().getName() + ": " + e.getMessage() + " { "
          + Arrays.toString(e.getStackTrace()) + " }";
      log(Level.SEVERE, message, processID);
      return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    } catch (IOException e) {
      message = "Data could not be added to data queue [" + collectionUri + "] due to a "
          + e.getClass().getName() + ": " + e.getMessage() + " { "
          + Arrays.toString(e.getStackTrace()) + " }";
      log(Level.SEVERE, message, processID);
      return Response.status(Status.INTERNAL_SERVER_ERROR).build();
    }

    message = "DH-publish started, data added to data queue [" + collectionUri + "]";
    log(Level.INFO, message, processID);

    return Response.status(Status.OK).build();
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.middleware.tgpublish.api.DHPublishService#getMinistatus(java.lang.String,
   * java.lang.String, java.lang.String)
   */
  @Override
  public PublishResponse getMinistatus(final String uri, final String storageToken,
      final String logID) {

    // Adapt the given URI if needed.
    String collectionUri = DHPublishUtils.adaptUri(uri);

    // Check for null or empty logID.
    String lid = DHPublishUtils.getLogID(logID);

    // Create a new response object to respond with.
    PublishResponse miniResponse = new PublishResponse();

    log(Level.FINE,
        "[" + lid + "] " + CALLING + "DH-publish#getMinistatus [" + collectionUri + "]");

    // Authenticate.
    try {
      if (!authenticate(storageToken, collectionUri, CAN_READ, lid)) {
        log(Level.SEVERE, "[" + lid + "] " + NOT_AUTHORISED);
        return getErrorPublishResponse(miniResponse);
      }
    } catch (IOException e) {
      log(Level.SEVERE, "[" + lid + "] " + STORAGE_SERVICE_ERROR + ": " + e.getMessage());
      return getErrorPublishResponse(miniResponse);
    }

    // Get the current response object (no PID needed here!).
    PublishResponse response = getPublishResponse(collectionUri, NO_PID);

    // Add needed ministatus to the new response (just leave out all the objects from the publish
    // objects list).
    miniResponse.dryRun = response.dryRun;
    miniResponse.objectListComplete = response.objectListComplete;
    miniResponse.setPublishStatus(response.getPublishStatus());

    // Add only the root object (edition or collection) to object list.
    miniResponse.setPublishObjects(whereIsMyRootObject(response));

    // Finally return the ministatus without object list.
    return miniResponse;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.middleware.tgpublish.api.DHPublishService#getInfo(java.lang.String,
   * java.lang.String, java.lang.String)
   */
  @Override
  public InfoResponse getInfo(final String uri, final String storageToken, final String logID) {

    // Adapt the given URI if needed.
    String collectionUri = DHPublishUtils.adaptUri(uri);

    // Check for null or empty logID.
    String lid = DHPublishUtils.getLogID(logID);

    log(Level.FINE, "[" + lid + "] " + CALLING + "DH-publish#getInfo [" + collectionUri + "]");

    // Authenticate.
    try {
      if (!authenticate(storageToken, collectionUri, CAN_READ, lid)) {
        log(Level.SEVERE, "[" + lid + "] " + NOT_AUTHORISED);
        return getErrorInfoResponse(new InfoResponse());
      }
    } catch (IOException e) {
      log(Level.SEVERE, "[" + lid + "] " + STORAGE_SERVICE_ERROR + ": " + e.getMessage());
      return getErrorInfoResponse(new InfoResponse());
    }

    return gatherInformation(collectionUri);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.middleware.tgpublish.api.DHPublishService#getStatus(java.lang.String,
   * java.lang.String, java.lang.String)
   */
  @Override
  public PublishResponse getStatus(final String uri, final String storageToken,
      final String logID) {

    // Adapt the given URI if needed.
    String collectionUri = DHPublishUtils.adaptUri(uri);

    // Check for null or empty logID.
    String lid = DHPublishUtils.getLogID(logID);

    log(Level.FINE, "[" + lid + "] " + CALLING + "DH-publish#getStatus [" + collectionUri + "]");

    // Authenticate.
    try {
      if (!authenticate(storageToken, collectionUri, CAN_READ, lid)) {
        log(Level.SEVERE, "[" + lid + "] " + NOT_AUTHORISED);
        return getErrorPublishResponse(new PublishResponse());
      }
    } catch (IOException e) {
      log(Level.SEVERE, "[" + lid + "] " + STORAGE_SERVICE_ERROR + ": " + e.getMessage());
      return getErrorPublishResponse(new PublishResponse());
    }

    return getPublishResponse(collectionUri, NO_PID);
  }

  /**
   * <p>
   * The given storage token is used for authentication here. We check if the given resource can be
   * accessed with the given right using the given storage token.
   * </p>
   * 
   * @param theStorageToken The storage token.
   * @param theUri The URI of the object.
   * @param theRight The right to be checked.
   * @param theLogID The log ID to be used accessing the storage.
   * @return true if authenticated, false if not
   * @throws IOException
   */
  public static boolean authenticate(final String theStorageToken, final String theUri,
      final String theRight, final String theLogID) throws IOException {

    boolean result = false;

    // Get storage ID from URI.
    String storageID = DHPublishUtils.getStorageId(theUri);

    defaultLogger.log(Level.FINE,
        "[" + theLogID + "] Authenticate for ID " + theUri + " and operation " + theRight);

    // Get OwnStorage client.
    DariahStorageClient ownStorageClient =
        storageClient.setStorageUri(URI.create(DHPublishAbs.getOwnStorageEndpoint()));

    // Check storage client if read access on given resource is granted!
    if (theRight.equals(DHPublishService.CAN_READ)) {
      result = ownStorageClient.checkAccess(storageID, theStorageToken, theLogID,
          DariahStorageClient.CHECKACCESS_OPERATION_READ);
    }
    // Check storage client if write access on given resource is granted!
    else if (theRight.equals(DHPublishService.CAN_WRITE)) {
      result = ownStorageClient.checkAccess(storageID, theStorageToken, theLogID,
          DariahStorageClient.CHECKACCESS_OPERATION_WRITE);
    }
    // Check storage client if publish access on given resource is granted!
    // NOTE We can PUBLISH if we can WRITE!
    else if (theRight.equals(DHPublishService.CAN_PUBLISH)) {
      result = ownStorageClient.checkAccess(storageID, theStorageToken, theLogID,
          DariahStorageClient.CHECKACCESS_OPERATION_WRITE);
    }

    return result;
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Gets the first object from a PublishReponse (normally the root edition or collection).
   * </p>
   * 
   * @param theResponse
   * @return A PublishObject list.
   */
  private static List<PublishObject> whereIsMyRootObject(final PublishResponse theResponse) {

    List<PublishObject> result = new ArrayList<PublishObject>();

    if (!theResponse.getPublishObjects().isEmpty()) {
      result.add(theResponse.getPublishObjects().get(0));
    }

    return result;
  }

  /**
   * <p>
   * Returns the publish response from the current publish process, or build up a new one from
   * scratch if no process is currently running or queued at the moment. If the objectListComplete
   * flag is set, a response already is existing in status QUEUED!
   * </p>
   * 
   * @param theCollectionUri
   * @param thePid
   * @return A PublishResponse.
   */
  private PublishResponse getPublishResponse(final String theCollectionUri, final String thePid) {

    // Get the publish response from the response map for the specified collection URI.
    PublishResponse pResponse = PublishAbs.getPublishResponse(theCollectionUri);

    // If no response is existing for the given URI, return a publish status for the given URI.
    if (pResponse == null) {

      // Create a new response object.
      PublishResponse newPublishResponse = new PublishResponse();

      // Set overall publish status.
      PublishStatus overallStatus = new PublishStatus();
      overallStatus.activeModule = this.getClass().getName();

      // Create new publish object, set PID, and add overall status.
      PublishObject p = new PublishObject(theCollectionUri);
      p.pid = thePid;

      // Check if object has been published.
      boolean alreadyPublished = false;
      try {
        alreadyPublished = DHPublishUtils.checkPublished(thePid);
      } catch (IOException | AuthenticationException e) {

        String message = PUBLICH_CHECK_ERROR_MESSAGE + e.getClass().getName() + ": "
            + e.getMessage() + " { " + Arrays.toString(e.getStackTrace()) + " }";
        log(Level.SEVERE, message);

        // Add error status.
        PublishStatus newStatus = new PublishStatus();
        newStatus.activeModule = "DHPublishService";
        newStatus.processStatus = ProcessStatusType.FAILED;
        newStatus.progress = 0;
        newPublishResponse.setPublishStatus(newStatus);
        newPublishResponse.objectListComplete = false;

        return newPublishResponse;
      }

      if (alreadyPublished) {
        // Add already published status.
        p.status = StatusType.ALREADY_PUBLISHED;
        overallStatus.processStatus = ProcessStatusType.FINISHED;
      } else {
        // Add default status.
        p.status = StatusType.NOT_YET_PUBLISHED;
        overallStatus.processStatus = ProcessStatusType.NOT_QUEUED;
      }

      newPublishResponse.addPublishObject(p);
      newPublishResponse.setPublishStatus(overallStatus);

      return newPublishResponse;
    }

    // ...else respond with the currently existing response (included status QUEUED if
    // !objectListComplete!)
    else {
      return pResponse;
    }
  }

  /**
   * <p>
   * Returns current task if task divider is in module string.
   * </p>
   * 
   * @param theModule
   * @return
   */
  private static String currentTask(String theModule) {
    if (theModule.contains(PublishUtils.MOD_TASK_DIVIDER)) {
      return theModule.substring(theModule.indexOf(PublishUtils.MOD_TASK_DIVIDER) + 1);
    } else {
      return "";
    }
  }

  /**
   * <p>
   * Purges task from module string.
   * </p>
   * 
   * @param theModule
   * @return
   */
  private static String purgeTask(String theModule) {
    if (theModule.contains(PublishUtils.MOD_TASK_DIVIDER)) {
      return theModule.substring(0, theModule.indexOf(PublishUtils.MOD_TASK_DIVIDER));
    } else {
      return theModule;
    }
  }

  /**
   * <p>
   * Sets progress, module, and task to given InfoResponse.
   * </p>
   * 
   * @param theInfoResponse
   * @param theProgress
   * @param theModule
   */
  private static void setInfoThings(InfoResponse theInfoResponse, final int theProgress,
      final String theModule) {
    setInfo(theInfoResponse, theProgress, currentTask(theModule), theModule);
  }

  /**
   * <p>
   * Sets progress, module, and task to given InfoResponse.
   * </p>
   * 
   * @param theInfoResponse
   * @param theProgress
   * @param theTask
   * @param theModule
   */
  private static void setInfo(InfoResponse theInfoResponse, final int theProgress,
      final String theTask, final String theModule) {
    theInfoResponse.progress = theProgress;
    theInfoResponse.task = theTask;
    theInfoResponse.module = purgeTask(theModule);
  }

  /**
   * <p>
   * Gets an InfoResponse. Checks if an object's status already is PUBLISHED or REGISTERED, if it is
   * a DRAFT or if a publish process is already running.
   * </p>
   * 
   * @param uri
   * @return
   */
  private InfoResponse gatherInformation(final String uri) {

    // Get start time for temporal measurement.
    long startTime = System.currentTimeMillis();

    // Create a new InfoResponse object, if none is existing.
    InfoResponse iResponse = DHPublish.getInfoResponse(uri);
    if (iResponse == null) {
      iResponse = new InfoResponse(uri);
    }

    // Always get PID and DOI!
    iResponse.pid = LTPUtils.omitHdlPrefix(RedisDB.getHDL(uri));
    iResponse.doi = LTPUtils.omitDoiPrefix(RedisDB.getDOI(uri));

    // Try to get RDF model OwnStorage ID, write to response, if not empty.
    String rdfID = RedisDB.getModelID(uri);
    if (!rdfID.isEmpty()) {
      iResponse.rdf = rdfID;
    }

    // Get CRID from REDIS.
    String crID = RedisDB.getCRID(iResponse.uri);
    if (crID != null && !crID.isEmpty()) {
      iResponse.crid = crID;
    }

    // Get publish response and status, if a publish process currently is running or already has
    // been run (always returns non-null).
    PublishResponse pResponse = getPublishResponse(uri, iResponse.pid);
    // Publish status of the collection.
    StatusType pubStatus = null;
    if (pResponse.getPublishObjects() != null && !pResponse.getPublishObjects().isEmpty()) {
      pubStatus = pResponse.getPublishObjects().get(0).status;
    }
    // TODO Check if publish status is null here?? And do something about it??

    // Overall publish process status.
    ProcessStatusType procStatus = pResponse.getPublishStatus().processStatus;

    // ******************************************************************************************
    // PUBLISHED
    // ******************************************************************************************
    // Publish process successfully completed. Collection was published in the DARIAH Repository.
    // We assume that a DARIAH object is published, if it's root collection file has got a PID and
    // the PID metadata has got a PUBLISHED=true entry. A publish process MUST NOT currently be
    // running!
    // NOTE The checkPublished() has already be done in getPublishResponse().
    // ******************************************************************************************

    if (procStatus != null && !procStatus.equals(ProcessStatusType.RUNNING)
        && !"".equals(iResponse.pid) && pubStatus.equals(StatusType.ALREADY_PUBLISHED)) {

      log(Level.FINE, "PID & checkPublished  -->  InfoStatus: PUBLISHED");

      iResponse.status = InfoType.PUBLISHED;
    }

    // A publishing process must be currently running, checking publish response...
    else {

      // ******************************************************************************************
      // RUNNING: A publish process is currently running.
      // ******************************************************************************************

      if (ProcessStatusType.RUNNING.equals(procStatus)) {

        // Check for long running tasks in status RUNNING. If it exceeds the resetStatusAfter
        // value, set status to ERROR.
        long noChangesSince = startTime - pResponse.getPublishStatus().getNoChangesSince();
        if (noChangesSince > DHPublishAbs.getErrorTimeout()) {

          log(Level.FINE,
              "LONG RUNNING TASK (" + noChangesSince + "ms > " + DHPublishAbs.getErrorTimeout()
                  + "ms without progress updates) --> InfoStatus: ERROR");

          // Set error status and things.
          iResponse.status = InfoType.ERROR;
          setInfoThings(iResponse, pResponse.getPublishStatus().progress,
              pResponse.getPublishStatus().activeModule);
        }
        // Otherwise set regular things.
        else {

          log(Level.FINE, "ProcessStatus: RUNNING  -->  InfoStatus: RUNNING");

          // Set running status and things.
          iResponse.status = InfoType.RUNNING;
          setInfoThings(iResponse, pResponse.getPublishStatus().progress,
              pResponse.getPublishStatus().activeModule);
        }
      }

      // ******************************************************************************************
      // WARNING
      // ******************************************************************************************
      // Warning occurred during publish process. Please call status/ministatus for more
      // information!
      // ******************************************************************************************

      else if (StatusType.WARNING.equals(pubStatus)
          && (ProcessStatusType.ABORTED.equals(procStatus)
              || ProcessStatusType.ABORTED.equals(procStatus))) {

        log(Level.FINE, "ProcessStatus: WARNING  -->  InfoStatus: WARNING");

        // Set status and things.
        iResponse.status = InfoType.WARNING;
        setInfoThings(iResponse, pResponse.getPublishStatus().progress,
            pResponse.getPublishStatus().activeModule);
      }

      // ******************************************************************************************
      // ERROR
      // ******************************************************************************************
      // Error occurred during publish process. Please call status/ministatus for more
      // information!
      // ******************************************************************************************

      else if (ProcessStatusType.FAILED.equals(procStatus)) {

        log(Level.FINE, "ProcessStatus: FAILED  -->  InfoStatus: ERROR");

        // Set status and things.
        iResponse.status = InfoType.ERROR;
        setInfoThings(iResponse, pResponse.getPublishStatus().progress,
            pResponse.getPublishStatus().activeModule);
      }

      // ******************************************************************************************
      // FINISHED
      // ******************************************************************************************
      // A publish process has finished correctly. If dryRun applies, update info and publish
      // response, and update publish response in custom data.
      // ******************************************************************************************

      else if (ProcessStatusType.FINISHED.equals(procStatus)) {

        if (pResponse.dryRun) {

          log(Level.FINE, "ProcessStatus FINISHED [" + DRY_RUN + "]  -->  InfoStatus: DRAFT");

          iResponse.status = InfoType.DRAFT;
          pResponse.getPublishObjects().get(0).status = StatusType.NOT_YET_PUBLISHED;
          pResponse.getPublishStatus().processStatus = ProcessStatusType.NOT_QUEUED;

          PublishAbs.updatePublishResponseMap(uri, pResponse);

        } else {

          log(Level.FINE, "ProcessStatus FINISHED  -->  InfoStatus: PUBLISHED");

          // Set status.
          iResponse.status = InfoType.PUBLISHED;
        }

        // Set things.
        setInfoThings(iResponse, pResponse.getPublishStatus().progress,
            pResponse.getPublishStatus().activeModule);
      }

      // ******************************************************************************************
      // DRAFT
      // ******************************************************************************************
      // The default value. Not yet published, but (maybe) information available in DH-publish
      // (maybe from a dry run). Please call status/ministatus for more information!
      // ******************************************************************************************

      else {
        log(Level.FINE, "Default: No ProcessStatus does apply  -->  InfoStatus: DRAFT");

        iResponse.status = InfoType.DRAFT;
      }
    }

    log(Level.INFO,
        "[status: " + iResponse.status + " | pid: "
            + (iResponse.pid.equals("") ? "no" : iResponse.pid) + " | doi: "
            + (iResponse.doi.equals("") ? "no" : iResponse.doi) + " | crid: " + iResponse.crid
            + " | rdf: " + iResponse.rdf + " | module: " + iResponse.module + " | task: "
            + iResponse.task + " | pogress: " + iResponse.progress + "%]  -->  "
            + (System.currentTimeMillis() - startTime) + "ms");
    DHPublish.updateInfoResponseMap(uri, iResponse);

    return iResponse;
  }

  /**
   * @param theRepsonse
   * @return
   */
  private PublishResponse getErrorPublishResponse(PublishResponse theRepsonse) {

    PublishResponse result = theRepsonse;

    PublishStatus publishStatus = new PublishStatus();
    publishStatus.activeModule = this.getClass().getName();
    publishStatus.processStatus = ProcessStatusType.FAILED;
    publishStatus.progress = 0;
    result.setPublishStatus(publishStatus);

    return result;
  }

  /**
   * @param theRepsonse
   * @return
   */
  private InfoResponse getErrorInfoResponse(InfoResponse theRepsonse) {

    InfoResponse result = theRepsonse;

    result.module = this.getClass().getName();
    result.progress = 0;
    result.status = InfoType.ERROR;
    result.task = NOT_AUTHORISED;

    return result;
  }

  /**
   * <p>
   * Log using the overall log ID, but without ID :-D
   * </p>
   * 
   * @param theLogLevel
   * @param theMessage
   */
  protected static void log(final Level theLogLevel, final String theMessage) {
    log(theLogLevel, theMessage, null);
  }

  /**
   * <p>
   * Log using the overall log ID.
   * </p>
   * 
   * @param theLogLevel
   * @param theMessage
   * @param theLogID
   */
  private static void log(final Level theLogLevel, final String theMessage, final String theLogID) {
    defaultLogger.log(theLogLevel,
        (theLogID != null ? DHPublishUtils.getLogIDString(theLogID) : "") + theMessage);
  }

  /**
   * <p>
   * Check if we have got another process with this URI in progress. Other processes are running, if
   * there is a publish response for the given URI, and
   * <ul>
   * <li>there is a publish response with publish status, and</li>
   * <li>process status is RUNNING or QUEUED, and</li>
   * <li>process ID is NOT the one of the currently running process.</li>
   * </ul>
   * </p>
   * 
   * @param theCollectionUri
   * @param theProcessID
   * @return True if other processes are running, false if not.
   */
  private static boolean otherProcessesRunning(final String theCollectionUri,
      final String theProcessID) {

    // Check first if this object is being published right now by another process, and abort without
    // updating the response object.
    synchronized (DHPublishServiceImpl.class) {

      log(Level.FINE, "Current collection URI: " + theCollectionUri);
      log(Level.FINE, "Current process ID (active module): " + theProcessID);

      // Get the publish response from the response map for the specified TextGrid URI and check for
      // URI in the current URI set.
      PublishResponse response = PublishAbs.getPublishResponse(theCollectionUri);
      if (response != null && response.getPublishStatus() != null) {

        log(Level.FINE,
            "Response process status: " + response.getPublishStatus().processStatus.toString());
        log(Level.FINE,
            "Response active module (process ID): " + response.getPublishStatus().activeModule);

        if (response.getPublishStatus().processStatus != null
            && (response.getPublishStatus().processStatus.equals(ProcessStatusType.RUNNING)
                || response.getPublishStatus().processStatus.equals(ProcessStatusType.QUEUED))
            && !response.getPublishStatus().activeModule.equals(theProcessID)) {
          return true;
        }
      }
    }

    return false;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param maintenance
   */
  public static void setMaintenance(boolean maintenance) {
    DHPublishServiceImpl.maintenance = maintenance;
  }

  /**
   * @param hours
   */
  public static void setExpirationWarning(long hours) {
    DHPublishServiceImpl.warnOnTokenValidityLessThan = hours;
  }

  // **
  // PRIVATE CLASSES
  // **

  /**
   * <p>
   * Wrapper class for starting WorkflowTool as a thread.
   * </p>
   */

  private class StartWorkflowTool extends Thread {

    private String configFilename;

    /**
     * <p>
     * Constructor.
     * </p>
     */
    public StartWorkflowTool(String theConfigFilename) {
      this.configFilename = theConfigFilename;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {

      if (this.configFilename == null || this.configFilename.equals("")) {
        log(Level.SEVERE,
            "WorkflowTool could not be started, please set config file beans.xml as constructor arg");
        return;
      }

      WorkflowTool woto = new WorkflowTool();

      // Configure koLibRI.
      try {

        log(Level.INFO, "[" + this.configFilename + "] Configuring koLibRI...");

        woto.configure(this.configFilename);

        log(Level.INFO, "[" + this.configFilename + "] koLibRI configuration complete!");

      } catch (SAXException | ParserConfigurationException | IOException | InvocationTargetException
          | IllegalAccessException e) {
        log(Level.SEVERE, CONFIG_ERROR_MESSAGE + e.getClass().getName() + ": " + e.getMessage()
            + " { " + Arrays.toString(e.getStackTrace()) + " }");
        return;
      }

      // Run koLibRI.
      try {

        log(Level.INFO, "[" + this.configFilename + "] Running koLibRI...");

        woto.run();

        log(Level.INFO, "[" + this.configFilename + "] koLibRI run complete!");

      } catch (Exception e) {
        log(Level.SEVERE, RUN_ERROR_MESSAGE + e.getClass().getName() + ": " + e.getMessage() + " { "
            + Arrays.toString(e.getStackTrace()) + " }");
        return;
      }
    }

  }

}
