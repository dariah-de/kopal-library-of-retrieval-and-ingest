/**
 * This software is copyright (c) 2022 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.publish.util;

import static org.junit.Assert.assertFalse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * <p>
 * Test class for TG-publish utils.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2022-08-19
 * @since 2022-08-19
 */

public class TestPublishUtils {

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  /**
   * 
   */
  @Test
  public void testOmitSIDFromMessage() {

    String replacement = "[BLUB]=[BLAH]";

    String tst[] = {
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?sessionId=HJVjhglsdf7sduz",
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?sessionId=HJVjhglsdf7sduz&anotherParam=huhu",
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?sid=HJVjhglsdf7sduz",
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?sid=HJVjhglsdf7sduz&anotherParam=huhu",
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?firstParam=huhu&sessionId=HJVjhglsdf7sduz",
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?firstParam=huhu&sessionId=HJVjhglsdf7sduz&anotherParam=huhu",
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?firstParam=huhu&sid=HJVjhglsdf7sduz",
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?firstParam=huhu&sid=HJVjhglsdf7sduz&anotherParam=huhu"
    };
    String exp[] = {
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?" + replacement,
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?" + replacement
            + "&anotherParam=huhu",
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?" + replacement,
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?" + replacement
            + "&anotherParam=huhu",
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?firstParam=huhu&"
            + replacement,
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?firstParam=huhu&" + replacement
            + "&anotherParam=huhu",
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?firstParam=huhu&"
            + replacement,
        "https://textgridlab.org/1.0/tgcrud/textgrid:1234.0/metadata?firstParam=huhu&" + replacement
            + "&anotherParam=huhu"
    };

    // Check arrays.
    for (int i = 0; i < tst.length; i++) {
      String res = PublishUtils.omitSIDFromMessage(tst[i]);
      if (!res.equals(exp[i])) {
        String message = "NONONO [" + i + "] -->\n(res) " + res + " !=\n(exp) " + exp[i];
        System.out.println(message);
        assertFalse(true);
      }
    }
  }

}
