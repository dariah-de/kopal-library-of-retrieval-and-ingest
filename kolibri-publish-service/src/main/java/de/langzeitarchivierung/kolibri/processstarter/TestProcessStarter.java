/*******************************************************************************
 * This software is copyright (c) 2009 by
 * 
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.processstarter;

import de.langzeitarchivierung.kolibri.ProcessQueue;

public class TestProcessStarter implements ProcessStarter {

	// **
	// STATE
	// **

	protected String		inputDirFromCli;
	protected ProcessQueue	processQueue;

	/**
	 * <p>
	 * Constructor
	 * </p>
	 */
	public TestProcessStarter() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#run()
	 */
	public void run() {
		System.out.println("TestProcessStarter here!");
	}

	// **
	// GETTER AND SETTER
	// **

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#setInput
	 * (java.lang.String)
	 */
	public void setInput(String input) {
		this.inputDirFromCli = input;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#setProcessQueue
	 * (de.langzeitarchivierung.kolibri.ProcessQueue)
	 */
	public void setProcessQueue(ProcessQueue processQueue) {
		this.processQueue = processQueue;
	}

}
