/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.processstarter;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import de.langzeitarchivierung.kolibri.ProcessQueue;
import de.langzeitarchivierung.kolibri.util.Configurator;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.ProcessStatusType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;
import info.textgrid.middleware.tgpublish.api.jaxb.StatusType;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2020-12-03 - Funk - Reformat code.
 */

/**
 * <p>
 * <b>The PublishService process starter abstract class</b>
 * </p>
 * 
 * <p>
 * This process starter is waiting for CustomData entries in a queue, that will be filled by the
 * Publish Service for every publish webservice call.
 * </p>
 * 
 * @author Ubbo Veentjer, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-12-03
 * @since 2011-01-04
 */

public abstract class PublishAbs implements ProcessStarter {

  // **
  // STATICS
  // **

  protected static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");
  protected static volatile ConcurrentHashMap<String, PublishResponse> publishResponseMap;
  protected static int responseMapCapacity = 5;
  protected static volatile SortedMap<String, Long> startDateMap;

  // **
  // CLASS VARIABLES
  // **

  protected ProcessQueue processQueue;
  protected String input;
  protected String defaultPolicy;

  // **
  // CONSTRUCTOR
  // **

  /**
   * <p>
   * Constructor constructs, e.g. service stubs, and creates the response queue. This should be
   * thread-safe.
   */
  public PublishAbs() {

    // Configure this class.
    try {
      Configurator.configure(this);
    } catch (InvocationTargetException e) {
      e.printStackTrace();
      defaultLogger.severe("Could not configure Publish Service");
    } catch (IllegalAccessException e) {
      e.printStackTrace();
      defaultLogger.severe("Could not configure Publish Service");
    }

    // Create start date and response map, if not yet created...
    if (publishResponseMap == null) {
      publishResponseMap = new ConcurrentHashMap<String, PublishResponse>(responseMapCapacity);

      defaultLogger.fine("PublishResponseMap created");
    }
    if (startDateMap == null) {
      startDateMap = Collections.synchronizedSortedMap(new TreeMap<String, Long>());

      defaultLogger.fine("StartDateMap created");
    }
  }

  /**
   * <p>
   * Adds/updates a key/value pair to the response map, and adds the start date.
   * </p>
   * 
   * @param theUri
   * @param theResponse
   */
  public synchronized static void updatePublishResponseMap(String theUri,
      PublishResponse theResponse) {

    // Check if the capacity of the response map has exceeded already, but only if a new response
    // shall be added. If so, delete oldest entry.
    synchronized (PublishAbs.class) {

      if (!publishResponseMap.containsKey(theUri)
          && publishResponseMap.size() == responseMapCapacity) {
        String uriToDelete = startDateMap.firstKey();
        startDateMap.remove(uriToDelete);
        publishResponseMap.remove(uriToDelete);

        defaultLogger.finest("Deleted oldest entry from ResponseMap: " + uriToDelete);
      }

      // After checking map size (and eventually deleting), add or update the object.
      publishResponseMap.put(theUri, theResponse);
      startDateMap.put(theUri, System.currentTimeMillis());

      defaultLogger.fine("Added to ResponseMap: " + theUri);
    }
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Set status failed and set response map. We need to have an error response early in the process
   * starter, so we can report errors to the clients!
   * </p>
   * 
   * @param theUri
   * @param theResponse
   * @param theClassName
   * @param theErrorType
   * @param theErrorMessage
   */
  protected static void setErrorResponse(String theUri, PublishResponse theResponse,
      String theClassName, ErrorType theErrorType, String theErrorMessage) {

    // Create new publish object with root URI and set status to error.
    PublishObject p = new PublishObject(theUri);
    p.addError(theErrorType, theErrorMessage);
    p.status = StatusType.ERROR;
    theResponse.addPublishObject(p);

    // Set publish status and active module.
    theResponse.getPublishStatus().activeModule = theClassName;
    theResponse.getPublishStatus().processStatus = ProcessStatusType.FAILED;

    // Add to publish response.
    PublishAbs.updatePublishResponseMap(theUri, theResponse);
  }

  // /**
  // * <p>
  // * Set status queued and set response map. We need to have a status message early in the process
  // * starter, so we can report the going-on state to the clients!
  // * </p>
  // *
  // * @param theUri
  // * @param theResponse
  // * @param theClassName
  // */
  // protected static void setQueuedResponse(String theUri, PublishResponse theResponse,
  // String theClassName) {
  //
  // // Create new publish object with root URI and set status OK.
  // PublishObject p = new PublishObject(theUri);
  // p.status = StatusType.OK;
  // theResponse.addPublishObject(p);
  //
  // // Set publish status and active module.
  // theResponse.getPublishStatus().activeModule = theClassName;
  // theResponse.getPublishStatus().processStatus = ProcessStatusType.QUEUED;
  // theResponse.getPublishStatus().progress = 0;
  //
  // // Add to publish response.
  // PublishAbs.updatePublishResponseMap(theUri, theResponse);
  // }

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.processstarter.ProcessStarter# setProcessQueue
   * (de.langzeitarchivierung.kolibri.ProcessQueue)
   */
  @Override
  public void setProcessQueue(ProcessQueue processQueue) {
    this.processQueue = processQueue;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#setInput (java.lang.String)
   */
  @Override
  public void setInput(String input) {
    this.input = input;
  }

  /**
   * @param policy
   */
  public void setDefaultPolicy(String policy) {
    this.defaultPolicy = policy;
  }

  /**
   * @param capacity
   */
  public static void setResponseMapCapacity(int capacity) {
    responseMapCapacity = capacity;
  }

  /**
   * @return
   */
  public static int getResponseMapCapacity() {
    return responseMapCapacity;
  }

  /**
   * <p>
   * Returns a PublishResponse, if the URI is existing, NULL otherwise.
   * </p>
   * 
   * @param theUri
   * @return
   */
  public static PublishResponse getPublishResponse(String theUri) {
    return publishResponseMap.get(theUri);
  }

  /**
   * @return
   */
  public static int getResponseMapSize() {
    return publishResponseMap.size();
  }

}
