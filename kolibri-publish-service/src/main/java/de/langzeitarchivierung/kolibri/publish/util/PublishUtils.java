/**
 * This software is copyright (c) 2022 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.publish.util;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.processstarter.PublishAbs;
import info.textgrid.middleware.tgpublish.api.jaxb.ErrorType;
import info.textgrid.middleware.tgpublish.api.jaxb.ProcessStatusType;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishObject;
import info.textgrid.middleware.tgpublish.api.jaxb.PublishResponse;

/**
 * <p>
 * Utility class for TG-publish.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2022-08-19
 * @since 2011-01-17
 */

public class PublishUtils {

  // **
  // STATIC FINAL
  // **

  private static final String DRY_RUN = "[DRY RUN] ";
  private static final String URI_SEPARATOR = "...";
  public static final String MOD_TASK_DIVIDER = "#";

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Sets the status of the given step and fills in data into the publish response, add task to
   * module.
   * </p>
   * 
   * @param theStep
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param theTask
   * @param dryRun
   */
  public static void setStatusRunning(Step theStep, String theMessage, PublishResponse theResponse,
      ActionModule theModule, String theTask, boolean dryRun) {

    // Set the response's module name (and task, if provided).
    theResponse.getPublishStatus().activeModule =
        theModule.getClass().getName() + MOD_TASK_DIVIDER + theTask;

    // Set the response's process status.
    theResponse.getPublishStatus().processStatus = ProcessStatusType.RUNNING;

    // Set the step's status.
    theStep.setStatus(Status.RUNNING, (dryRun ? DRY_RUN : "") + omitSIDFromMessage(theMessage));
  }

  /**
   * <p>
   * Sets the status of the given step and fills in data into the publish response.
   * </p>
   * 
   * @param theStep
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param dryRun
   */
  public static void setStatusRunning(Step theStep, String theMessage, PublishResponse theResponse,
      ActionModule theModule, boolean dryRun) {

    // Set the response's module name only if not starting with own module
    // name.
    if (!theResponse.getPublishStatus().activeModule.startsWith(theModule.getClass().getName())) {
      theResponse.getPublishStatus().activeModule = theModule.getClass().getName();
    }

    // Set the response's process status.
    theResponse.getPublishStatus().processStatus = ProcessStatusType.RUNNING;

    // Set the step's status.
    theStep.setStatus(Status.RUNNING, (dryRun ? DRY_RUN : "") + omitSIDFromMessage(theMessage));
  }

  /**
   * <p>
   * Only logs and uses dryRun flag.
   * </p>
   * 
   * @param theStep
   * @param theMessage
   * @param dryRun
   */
  public static void logStatusRunning(Step theStep, String theMessage, boolean dryRun) {
    theStep.setStatus(Status.RUNNING, (dryRun ? DRY_RUN : "") + theMessage);
  }

  /**
   * <p>
   * Sets the status of the given step and fills in data into the publish response.
   * </p>
   * 
   * <p>
   * NOTE The status is only set to RUNNING, if not already set to ABORT!
   * </p>
   * 
   * FIXME Use ABORT in TG-publish and DH-publish workflows, too??
   * 
   * @param theStep
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param dryRun
   */
  public static void setStatusRunningCheckAbort(Step theStep, String theMessage,
      PublishResponse theResponse, ActionModule theModule, boolean dryRun) {
    if (theResponse.getPublishStatus().processStatus != ProcessStatusType.ABORTED) {
      setStatusRunning(theStep, theMessage, theResponse, theModule, dryRun);
    }
  }

  /**
   * <p>
   * Adds the error to the given publish object, and additionally sets the general error.
   * </p>
   * 
   * @param theStep
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param theObject
   * @param theType
   * @param increaseProgress
   * @param dryRun
   */
  public static void addObjectError(Step theStep, String theMessage, PublishResponse theResponse,
      ActionModule theModule, PublishObject theObject, ErrorType theType, boolean increaseProgress,
      boolean dryRun) {

    // Add the error to the given publish object.
    theObject.addError(theType, theMessage);

    // Set the general error status, and log.
    setStatusError(theStep, theMessage, theResponse, theModule, increaseProgress, dryRun);
  }

  /**
   * <p>
   * General errors are now put into the first object of the list, normally the edition or
   * collection, etc.
   * </p>
   * 
   * @param theStep
   * @param theErrorType
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param increaseProgress
   * @param dryRun
   */
  public static void addGeneralError(Step theStep, ErrorType theErrorType, String theMessage,
      PublishResponse theResponse, ActionModule theModule, boolean increaseProgress,
      boolean dryRun) {

    // Get the first object (if existing).
    PublishObject firstObject = theResponse.getPublishObjects().get(0);
    firstObject.addError(theErrorType, theMessage);

    // Set the general error status, and log.
    setStatusError(theStep, theMessage, theResponse, theModule, increaseProgress, dryRun);
  }

  /**
   * @param theStep
   * @param theErrorType
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param dryRun
   */
  public static void addGeneralError(Step theStep, ErrorType theErrorType, String theMessage,
      PublishResponse theResponse, ActionModule theModule, boolean dryRun) {
    addGeneralError(theStep, theErrorType, theMessage, theResponse, theModule, false, dryRun);
  }

  /**
   * <p>
   * Sets the status of the given step and fills in data into the publish response.
   * </p>
   * 
   * @param theStep
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param increaseProgress
   * @param dryRun
   */
  public static void setStatusError(Step theStep, String theMessage, PublishResponse theResponse,
      ActionModule theModule, boolean increaseProgress, boolean dryRun) {

    // Set the response's module name.
    theResponse.getPublishStatus().activeModule = theModule.getClass().getName();

    // Set the response's process status.
    theResponse.getPublishStatus().processStatus = ProcessStatusType.FAILED;

    // Increase the object's progress (per module).
    if (increaseProgress) {
      PublishUtils.increaseProgress(theResponse, theStep.getProcessData());
    }

    // Set the step's status.
    theStep.setStatus(Status.ERROR, (dryRun ? DRY_RUN : "") + omitSIDFromMessage(theMessage) + " ("
        + theResponse.getPublishStatus().progress + "%)");
  }

  /**
   * @param theStep
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param dryRun
   */
  public static void setStatusError(Step theStep, String theMessage, PublishResponse theResponse,
      ActionModule theModule, boolean dryRun) {
    setStatusError(theStep, theMessage, theResponse, theModule, false, dryRun);
  }

  /**
   * <p>
   * Sets the status of the given step and fills in data into the publish response.
   * </p>
   * 
   * @param theStep
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param increaseProgress
   * @param dryRun
   */
  public static void setStatusAborted(Step theStep, String theMessage, PublishResponse theResponse,
      ActionModule theModule, boolean increaseProgress, boolean dryRun) {

    // Set the response's module name.
    theResponse.getPublishStatus().activeModule = theModule.getClass().getName();

    // Set the response's process status.
    theResponse.getPublishStatus().processStatus = ProcessStatusType.ABORTED;

    // Increase the object's progress (per module).
    if (increaseProgress) {
      PublishUtils.increaseProgress(theResponse, theStep.getProcessData());
    }

    // Set the step's status.
    theStep.setStatus(Status.CANCELED, (dryRun ? DRY_RUN : "") + omitSIDFromMessage(theMessage));
  }

  /**
   * @param theStep
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param dryRun
   */
  public static void setStatusAborted(Step theStep, String theMessage, PublishResponse theResponse,
      ActionModule theModule, boolean dryRun) {
    setStatusAborted(theStep, theMessage, theResponse, theModule, false, dryRun);
  }

  /**
   * <p>
   * Sets the status of the given step and fills in data into the publish response. Increases the
   * module progress. Use only if module is NOT increasing its progress itself!
   * </p>
   * 
   * @param theStep
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param dryRun
   */
  public static void setStatusDone(Step theStep, String theMessage, PublishResponse theResponse,
      ActionModule theModule, boolean dryRun) {
    setStatusDone(theStep, theMessage, theResponse, theModule, true, dryRun);
  }

  /**
   * <p>
   * Sets the status of the given step and fills in data into the publish response. Does NOT
   * increases the module progress. Use only if module IS increasing its progress itself!
   * </p>
   * 
   * @param theStep
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param dryRun
   */
  public static void setStatusDoneNoProgressIncrease(Step theStep, String theMessage,
      PublishResponse theResponse, ActionModule theModule, boolean dryRun) {
    setStatusDone(theStep, theMessage, theResponse, theModule, false, dryRun);
  }

  /**
   * <p>
   * Sets the status of the given step to DONE and overall process state to FINISHED.
   * </p>
   * 
   * @param theStep
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param dryRun
   */
  public static void setFinished(Step theStep, final String theMessage, PublishResponse theResponse,
      final ActionModule theModule, final boolean dryRun) {

    // Set the response's module name.
    theResponse.getPublishStatus().activeModule = theModule.getClass().getName();

    // Set the response's process status.
    theResponse.getPublishStatus().processStatus = ProcessStatusType.FINISHED;

    // Increase the object's progress to 100% (because the process will be finished here, and to
    // avoid rounding errors).
    setProgress(theResponse, 100);

    // Set the step's status.
    theStep.setStatus(Status.DONE, (dryRun ? DRY_RUN : "") + omitSIDFromMessage(theMessage) + " ("
        + theResponse.getPublishStatus().progress + "%)");
  }

  /**
   * <p>
   * Adds items to the response map.
   * </p>
   * 
   * @param theUri
   * @param theResponse
   */
  public static void updateResponse(String theUri, PublishResponse theResponse) {
    PublishAbs.updatePublishResponseMap(theUri, theResponse);
  }

  /**
   * <p>
   * Set the response's status. Set noChangesSince also!
   * </p>
   * 
   * @param theResponse
   * @param theProgress
   */
  public static void setProgress(PublishResponse theResponse, int theProgress) {
    theResponse.getPublishStatus().progress = theProgress;
    theResponse.getPublishStatus().updateNoChangesSince();
  }

  /**
   * <p>
   * Get progress covered by module and set progress.
   * </p>
   * 
   * @param theResponse
   * @param theProcessData
   */
  public static void increaseProgress(PublishResponse theResponse, ProcessData theProcessData) {
    setProgress(theResponse,
        theResponse.getPublishStatus().progress + progressPerModule(theProcessData));
  }

  /**
   * <p>
   * Compute progress in percent the module has to cover, just count steps (modules) and divide.
   * </p>
   * 
   * @param theProcessData
   * @return progress count in %
   */
  public static int progressPerModule(ProcessData theProcessData) {

    int result = 0;

    Iterator<Step> iter = theProcessData.getPolicy().getPolicyIterator();
    while (iter.hasNext()) {
      iter.next();
      result++;
    }

    if (result != 0) {
      result = 100 / result;
    }

    return result;
  }

  /**
   * <p>
   * Returns a purged password string if not null and length is > 10, empty string otherwise.
   * </p>
   * 
   * @param thePassword
   * @return
   */
  public static String purgePasswd(String thePassword) {

    String result = "";

    if (thePassword != null && thePassword.length() > 10) {
      result = thePassword.substring(0, 3) + URI_SEPARATOR
          + thePassword.substring(thePassword.length() - 3);
    }

    return result;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Sets the status of the given step and fills in data into the publish response.
   * </p>
   * 
   * @param theStep
   * @param theMessage
   * @param theResponse
   * @param theModule
   * @param increaseProgress
   * @param dryRun
   */
  protected static void setStatusDone(Step theStep, String theMessage, PublishResponse theResponse,
      ActionModule theModule, boolean increaseProgress, boolean dryRun) {

    // Set the response's module name.
    theResponse.getPublishStatus().activeModule = theModule.getClass().getName();

    // Set the response's process status.
    theResponse.getPublishStatus().processStatus = ProcessStatusType.RUNNING;

    // Increase the object's progress (per module).
    if (increaseProgress) {
      PublishUtils.increaseProgress(theResponse, theStep.getProcessData());
    }

    // Set the step's status.
    theStep.setStatus(Status.DONE, (dryRun ? DRY_RUN : "") + omitSIDFromMessage(theMessage) + " ("
        + theResponse.getPublishStatus().progress + "%)");
  }

  /**
   * <p>
   * Omit the session ID from incoming messages, assume we have SID in an URL like "&sid="
   * (TG-search) or "&sessionId=" (TG-crud).
   * </p>
   * 
   * @param theMessage
   * @param theReplacement
   * @return
   */
  protected static String omitSIDFromMessage(final String theMessage) {

    String result = "";

    String regexp = "(sid=|sessionId=)(\\w*)";
    Pattern pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE);

    Matcher matcher = pattern.matcher(theMessage);
    result = matcher.replaceAll("[BLUB]=[BLAH]");

    return result;
  }

}
