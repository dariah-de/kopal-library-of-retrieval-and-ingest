/*******************************************************************************
 * This software is copyright (c) 2015 by
 * 
 *  - TextGrid Consortion (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.publish.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpHost;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

/*******************************************************************************
 * <p>
 * Util class for ElasticSearch in TG-publish.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2015-01-20
 * @since 2014-03-12
 ******************************************************************************/

public abstract class ElasticSearchUtils {

	// **
	// MANIPULATION
	// **

	/**
	 * <p>
	 * Creates and returns an ElasticSearch client.
	 * </p>
	 * 
	 * @return
	 * @throws MalformedURLException
	 */
	public static RestHighLevelClient getElasticSearchClient(String theHost,
	    List<Integer> ports) throws MalformedURLException {

        String host = new URL(theHost).getHost();

	    List<HttpHost> hosts = new ArrayList<HttpHost>();
	    for(int port : ports) {
	      hosts.add(new HttpHost(host, port, "http"));
	    }

	    return new RestHighLevelClient(
	        RestClient.builder(
	            hosts.toArray(new HttpHost[hosts.size()])));
	}

	/**
	 * <p>
	 * Creates an object in the ES database.
	 * </p>
	 * 
	 * @param theClient
	 * @param theUri
	 * @param theJson
	 * @param theIndex
	 * @return
	 * @throws IOException
	 */
	public IndexResponse createObject(RestHighLevelClient theClient, URI theUri,
			String theJson, String theIndex) throws IOException {
	    IndexRequest indexRequest =
	        new IndexRequest().index(theIndex)
	        .id(forgetUriPrefix(theUri)).source(theJson, XContentType.JSON);
	    return theClient.index(indexRequest, RequestOptions.DEFAULT);
	}

	/**
	 * <p>
	 * Reads from the ElasticSearch database.
	 * </p>
	 * 
	 * @param theClient
	 * @param theUri
	 * @param theName
	 * @return A JSON string containing all information ElasticSearch holds for
	 *         that ID.
	 * @throws IOException
	 */
	public String getObject(RestHighLevelClient theClient, URI theUri,
			String theName) throws IOException {
	    GetRequest getRequest = new GetRequest(theName, forgetUriPrefix(theUri));
        GetResponse response = theClient.get(getRequest, RequestOptions.DEFAULT);
        return response.getSourceAsString();
	}

	/**
	 * <p>
	 * Get index name from hostname (MUST be NOT NULL after INIT taken from URL
	 * path as: /textgrid-nonpublic/metadata/[indexName]/!
	 * </p>
	 * 
	 * @param theHost
	 * @return
	 * @throws MalformedURLException
	 */
	public static String getIndexName(String theHost)
			throws MalformedURLException {

		String index[] = new URL(theHost).getPath().split("/");
		return index[1];
	}

	/**
	 * <p>
	 * Get index type from hostname (MUST be NOT NULL after INIT taken from URL
	 * path as: /textgrid-nonpublic/metadata/[indexName]/!
	 * </p>
	 * 
	 * @param theHost
	 * @return
	 * @throws MalformedURLException
	 * @deprecated
	 */
	@Deprecated
	public static String getIndexType(String theHost)
			throws MalformedURLException {
		String index[] = new URL(theHost).getPath().split("/");
		return index[2];
	}

	/**
	 * <p>
	 * Returns a ES usable URI for ES cannot use any URI, e.g. ":" and "/" is
	 * not allowed! Please configure as ES is used!
	 * </p>
	 * 
	 * @param theUri
	 * @return The URI without e.g. the prefix and ":" as a String.
	 */
	public abstract String forgetUriPrefix(URI theUri);

}
