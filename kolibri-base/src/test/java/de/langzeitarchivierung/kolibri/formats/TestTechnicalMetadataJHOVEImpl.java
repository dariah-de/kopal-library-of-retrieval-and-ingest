package de.langzeitarchivierung.kolibri.formats;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import de.langzeitarchivierung.kolibri.util.FileUtils;

/**
 * <p>
 * JUnit test class for testing the TechnicalMetadata JHOVE implementation.
 * </p>
 * 
 * @author Stefan E. Funk
 */

public class TestTechnicalMetadataJHOVEImpl {

  // **
  // STATIC
  // **

  private static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");
  private static String jhoveOutputFile = "./src/test/resources/jhoveOutput.xml";

  /**
   * <p>
   * Test getting data from the JHOVE technical metadata string.
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testGettingDataFromJHOVEMetadataString() throws IOException {

    // Set expected values.
    String expectedType = "JHOVE";
    String expectedFormat = "PDF";
    String expectedMimetype = "application/pdf";
    String expectedVersion = "1.4";
    long expectedSize = 13051;

    // Get JHOVE XML string.
    String jhoveMetadataString = FileUtils.readFile(new File(jhoveOutputFile));

    defaultLogger.info("JHOVE metadata file location: " + jhoveOutputFile);
    defaultLogger.info("JHOVE metadata file content: \n" + jhoveMetadataString);

    // Create a technical metadata JHOVE object.
    TechnicalMetadataJHOVEImpl jhoveTechMd = new TechnicalMetadataJHOVEImpl(jhoveMetadataString);

    // Output all information from the JHOVE object.
    defaultLogger.info("Data from JHOVE metadata object:");
    defaultLogger.info("\tType:     " + jhoveTechMd.getType());
    defaultLogger.info("\tFormat:   " + jhoveTechMd.getFormat());
    defaultLogger.info("\tMimetype: " + jhoveTechMd.getMimetype());
    defaultLogger.info("\tVersion:  " + jhoveTechMd.getVersion());
    defaultLogger.info("\tSize:     " + jhoveTechMd.getSize());

    // Check values.
    if (!expectedType.equals(jhoveTechMd.getType())
        || !expectedFormat.equals(jhoveTechMd.getFormat())
        || !expectedVersion.equals(jhoveTechMd.getVersion())
        || !expectedMimetype.equals(jhoveTechMd.getMimetype())
        || expectedSize != jhoveTechMd.getSize()) {
      assertTrue(false);
    }
  }

}
