/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri;

import static org.junit.Assert.assertFalse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.Test;
import org.xml.sax.SAXException;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2019-12-03 - Funk - Set test to not ignore :-) Please configure to really ingest into koala!
 * 
 * 2019-11-18 - Funk - Copied from TestWorfklowTool.
 */

/**
 * <p>
 * JUnit test class for testing the WorkflowTool' connection to the DIAS (respective the koala LTE
 * system). Builds a simple SIP from test data and submits it to the DIAS/koala.
 * </p>
 * 
 * @author Stefan E. Funk
 * @version 2019-12-03
 * @since 2019-11-18
 */

public class TestUploadToDIAS {

  // **
  // STATIC
  // **

  private static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");

  /**
   * <p>
   * Test WorkflowTool here.
   * </p>
   * 
   * @throws NoSuchAlgorithmException
   * @throws KeyStoreException
   * 
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   */
  @Test
  public void testUploadToDIAS() throws NoSuchAlgorithmException, KeyStoreException {

    // Get koLibRI config file.
    String configFilename = "./src/test/resources/config/config__TestUploadToDIAS.xml";
    // Note Please insert username, password, and SSL config file pathes to config file, and get a
    // koala test account first :-) You also have to comment in the class SubmitSipToDias in
    // policies.xml!
    // String configFilename = "./src/test/resources/config/config__TestUploadToDIAS__PRIVATE.xml";

    // Create the WorkflowTool.
    WorkflowTool woto = new WorkflowTool();

    // Configure and run the WorkflowTool.
    try {
      woto.configure(configFilename);
      woto.run();

      boolean error = !woto.getErrorQueue().isEmpty();
      if (error) {
        Iterator<ProcessData> i = woto.getErrorQueue().getProcessIterator();
        while (i.hasNext()) {
          System.out.println("  ##  failed process name:" + i.next().processName);
        }
        assertFalse("Error queue is not empty, but should be!", error);
      }

    } catch (Exception e) {
      assertFalse(e.getMessage(), true);
    } finally {
      File logfile = new File(woto.getLogfileName());
      boolean logfileDeleted = logfile.delete();

      if (!logfileDeleted) {
        defaultLogger.warning("Logfile could not be deleted: " + logfile.getAbsolutePath());
      }
    }
  }

}
