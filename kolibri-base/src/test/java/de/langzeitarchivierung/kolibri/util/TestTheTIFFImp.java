package de.langzeitarchivierung.kolibri.util;

import static org.junit.Assert.assertFalse;
import java.io.File;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * TODOLOG
 * 
 **
 *
 * CHANGELOG
 * 
 * 2010-09-17 - Funk - First version.
 */

/**
 * <p>
 * JUnit test class for testing the TIFFImp.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2019-11-20
 * @since 2010-09-17
 */
@Ignore
public class TestTheTIFFImp {

  // **
  // STATIC
  // **

  private static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");
  private static String tiffFilename =
      "./src/test/resources/folders/hotfolder/data-mi/mi-test-sip/MonkeyIsland.tiff";

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {}

  /**
   * <p>
   * Test getting the header metadata.
   * </p>
   * 
   * @throws Exception
   */
  @Test
  public void testGettingHeaderMetadata() throws Exception {

    // Get image and config file.
    File tiffImage = new File(tiffFilename);
    File jhoveConf = new File("../config/jhove.conf");

    defaultLogger.info("TIFF Image file:   " + tiffImage.getAbsolutePath());
    defaultLogger.info("JHOVE config file: " + jhoveConf.getAbsolutePath());

    // Create a TiffIMP instance and set input file.
    TiffIMP imp = new TiffIMP(TiffIMP.initializeTheJhove(jhoveConf), tiffImage);

    defaultLogger.info("TiffIMP instance created and configured");

    // Validate TIFF image.
    imp.setValidateTiffImage(true);

    defaultLogger.info("Validating image...");

    assertFalse("Validation failed!", !imp.validate());

    String status = imp.getStatusString();

    assertFalse("Not well-formed and valid!", !status.equals("well-formed and valid"));

    defaultLogger.info("Validating complete");
  }

}
