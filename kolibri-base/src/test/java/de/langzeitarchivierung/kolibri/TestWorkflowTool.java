/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri;

import static org.junit.Assert.assertFalse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2011-08-17 Funk First version.
 */

/**
 * <p>
 * JUnit test class for testing the WorkflowTool. Builds a simple SIP from test data.
 * </p>
 * 
 * @author Stefan E. Funk
 * @version 2020-02-20
 * @since 2011-08-17
 */

public class TestWorkflowTool {

  // **
  // STATIC
  // **

  private static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {}

  /**
   * <p>
   * Test WorkflowTool here.
   * </p>
   * 
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   */
  @Test
  public void testWorkflowTool() {

    // Get koLibRI config file.
    String configFilename = "./src/test/resources/config/config__TestWorkflowTool.xml";

    // Create the WorkflowTool.
    WorkflowTool woto = new WorkflowTool();

    // Configure and run the WorkflowTool.
    try {
      woto.configure(configFilename);
      woto.run();

      boolean error = !woto.getErrorQueue().isEmpty();
      if (error) {
        Iterator<ProcessData> i = woto.getErrorQueue().getProcessIterator();
        while (i.hasNext()) {
          System.out.println("  ##  failed process name:" + i.next().processName);
        }
        assertFalse("Error queue is not empty, but should be!", error);
      }

    } catch (Exception e) {
      assertFalse(e.getMessage(), true);
    } finally {
      File logfile = new File(woto.getLogfileName());
      boolean logfileDeleted = logfile.delete();

      if (!logfileDeleted) {
        defaultLogger.warning("Logfile could not be deleted: " + logfile.getAbsolutePath());
      }
    }
  }

}
