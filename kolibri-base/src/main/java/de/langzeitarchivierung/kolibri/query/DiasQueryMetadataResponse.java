/******************************************************************************
 * de.langzeitarchivierung.kolibri.query / DiasQueryMetadataResponse.java
 * 
 * Copyright 2011 by Project DP4lib
 * 
 * http://dp4lib.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 *	2011-08-17	Funk	First version copied from
 *						DiasIngestMetadataResponse.java.
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.query;

import java.io.Serializable;

/*******************************************************************************
 * <p>
 * A metadata subfield of the DIAS response to search queries as a bean. To
 * understand the data fields read the Metadata Search Interface Specification.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2011-08-17
 * @since 2011-08-17
 ******************************************************************************/

public class DiasQueryMetadataResponse implements Serializable {

	// CONSTANTS ***************************************************************

	private static final long	serialVersionUID	= 5650347467993121164L;

	// FIXME
}
