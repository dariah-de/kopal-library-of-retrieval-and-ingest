/**
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Funk, SUB Goettingen
 */

package de.langzeitarchivierung.kolibri.ingest;

import java.io.Serializable;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2007-07-10 - koLibRI version 1.0
 */

/**
 * <p>
 * A metadata subfield of the DIAS response to ingest requests as a bean. To understand the data
 * fields read the SIP Interface Specification.
 * </p>
 * 
 * @version 2019-11-18
 * @since 2007-02-15
 */

public class DiasIngestMetadataResponse implements Serializable {

  // **
  // CONSTANTS
  // **

  private static final long serialVersionUID = 1L;

  public static final String STATE_UPLOADING = "uploading";
  public static final String STATE_LOADING = "loading";
  public static final String STATE_DONE = "done";
  public static final String STATE_ERROR = "error";
  public static final String STATE_FATAL_ERROR = "fatalError";

  // **
  // STATE (Instance variables)
  // **

  private String sipIngestTicketNumber = DiasIngestResponse.NOT_IN_DIAS_RESPONSE;
  private String ftpPassword = DiasIngestResponse.NOT_IN_DIAS_RESPONSE;
  private String sipFilename = DiasIngestResponse.NOT_IN_DIAS_RESPONSE;
  private String sipPreloadarea = DiasIngestResponse.NOT_IN_DIAS_RESPONSE;
  private String ftpServer = DiasIngestResponse.NOT_IN_DIAS_RESPONSE;
  private String ftpUser = DiasIngestResponse.NOT_IN_DIAS_RESPONSE;
  private String startTimestamp = DiasIngestResponse.NOT_IN_DIAS_RESPONSE;
  private String sourceId = DiasIngestResponse.NOT_IN_DIAS_RESPONSE;
  private String state = DiasIngestResponse.NOT_IN_DIAS_RESPONSE;
  private String substate = DiasIngestResponse.NOT_IN_DIAS_RESPONSE;
  private String supplementaryInformation = DiasIngestResponse.NOT_IN_DIAS_RESPONSE;
  private String timestamp = DiasIngestResponse.NOT_IN_DIAS_RESPONSE;
  private String alert = DiasIngestResponse.NOT_IN_DIAS_RESPONSE;

  // **
  // GET & SET METHODS
  // **

  /**
   * @return
   */
  public String getFtpPassword() {
    return this.ftpPassword;
  }

  /**
   * @param ftpPassword
   */
  public void setFtpPassword(String ftpPassword) {
    this.ftpPassword = ftpPassword;
  }

  /**
   * @return
   */
  public String getFtpServer() {
    return this.ftpServer;
  }

  /**
   * @param ftpServer
   */
  public void setFtpServer(String ftpServer) {
    this.ftpServer = ftpServer;
  }

  /**
   * @return
   */
  public String getFtpUser() {
    return this.ftpUser;
  }

  /**
   * @param ftpUser
   */
  public void setFtpUser(String ftpUser) {
    this.ftpUser = ftpUser;
  }

  /**
   * @return
   */
  public String getSipIngestTicketNumber() {
    return this.sipIngestTicketNumber;
  }

  /**
   * @param sipIngestTicketNumber
   */
  public void setSipIngestTicketNumber(String sipIngestTicketNumber) {
    this.sipIngestTicketNumber = sipIngestTicketNumber;
  }

  /**
   * @return
   */
  public String getSipPreloadarea() {
    return this.sipPreloadarea;
  }

  /**
   * @param sipPreloadarea
   */
  public void setSipPreloadarea(String sipPreloadarea) {
    this.sipPreloadarea = sipPreloadarea;
  }

  /**
   * @return
   */
  public String getSipFilename() {
    return this.sipFilename;
  }

  /**
   * @param sipFilename The sipFilename to set.
   */
  public void setSipFilename(String sipFilename) {
    this.sipFilename = sipFilename;
  }

  /**
   * @return Returns the sourceId.
   */
  public String getSourceId() {
    return this.sourceId;
  }

  /**
   * @param sourceId The sourceId to set.
   */
  public void setSourceId(String sourceId) {
    this.sourceId = sourceId;
  }

  /**
   * @return Returns the startTimestamp.
   */
  public String getStartTimestamp() {
    return this.startTimestamp;
  }

  /**
   * @param startTimestamp
   */
  public void setStartTimestamp(String startTimestamp) {
    this.startTimestamp = startTimestamp;
  }

  /**
   * @return Returns the state.
   */
  public String getState() {
    return this.state;
  }

  /**
   * @param state The state to set.
   */
  public void setState(String state) {
    this.state = state;
  }

  /**
   * @return Returns the substate.
   */
  public String getSubstate() {
    return this.substate;
  }

  /**
   * @param substate The substate to set.
   */
  public void setSubstate(String substate) {
    this.substate = substate;
  }

  /**
   * @return Returns the supplementaryInformation.
   */
  public String getSupplementaryInformation() {
    return this.supplementaryInformation;
  }

  /**
   * @param supplementaryInformation The supplementaryInformation to set.
   */
  public void setSupplementaryInformation(String supplementaryInformation) {
    this.supplementaryInformation = supplementaryInformation;
  }

  /**
   * @return Returns the timestamp.
   */
  public String getTimestamp() {
    return this.timestamp;
  }

  /**
   * @param timestamp The timestamp to set.
   */
  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  /**
   * @return Returns the alert.
   */
  public String getAlert() {
    return this.alert;
  }

  /**
   * @param alert The alert to set.
   */
  public void setAlert(String alert) {
    this.alert = alert;
  }

  // **
  // QUERIES (Queries - no change to object's state)
  // **

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    String result = "";

    result += "\tsipIngestTicketNumber: " + this.sipIngestTicketNumber;
    result += "\n\tftpPassword: " + this.ftpPassword;
    result += "\n\tsipPreloadarea: " + this.sipPreloadarea;
    result += "\n\tftpServer: " + this.ftpServer;
    result += "\n\tftpUser: " + this.ftpUser;
    result += "\n\tstartTimestamp: " + this.startTimestamp;
    result += "\n\tsourceId: " + this.sourceId;
    result += "\n\tstate: " + this.state;
    result += "\n\tsubstate: " + this.substate;
    result += "\n\tsupplementaryInformation: " + this.supplementaryInformation;
    result += "\n\ttimestamp: " + this.timestamp;
    result += "\n\talert: " + this.alert;

    return result;
  }

}
