/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Ludwig
 * @author Stefan E. Funk, SUB Göttingen
 * @author Kadir Karaca Koçer, German National Library
 */

package de.langzeitarchivierung.kolibri.ingest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.digester3.Digester;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import de.langzeitarchivierung.kolibri.util.FormatInfo;
import de.langzeitarchivierung.kolibri.util.KolibriXMLParser;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.Response;

/**
 * TODOLOG
 * 
 * TODO Use digester to return response beans?
 * 
 * TODO Improve error handling!
 * 
 **
 * CHANGELOG
 * 
 * 2024-02-16 - Funk - Adapt to new HTTP client due to Java17 update.
 * 
 * 2019-11-26 - Funk - Adapted to GWDG koala service.
 * 
 * 2007-07-10 - koLibRI version 1.0
 */

/**
 * <p>
 * Class for ingesting SIPs to DIAS according to the SIP Interface Specification 2.7 (including
 * ticketing system).
 * </p>
 * 
 * @version 2024-02-16
 * @since 2006-02-27
 */

public class DiasIngest {

  // **
  // CONSTANTS
  // **

  private static final int INPUT_BUFFER_LENGTH = 1024 * 1024;

  // URLs.
  private static final String ACCESS_MANAGER_COMMAND_URL = "/AccessManager/AccessManager?cmd=";
  private static final String INGEST_STATUS_COMMAND_URL = "/IngestStatus/Ingest?";
  private static final String SOURCEID_URL = "&destId=amgr&sourceId=";
  private static final String USER_URL = "&CMUser=";
  private static final String PASSWORD_URL = "&CMPasswd=";
  private static final String SIP_NAME_URL = "&SipName=";
  private static final String TICKET_ID_URL = "&TicketId=";

  // SFTP settings.
  private static final String SFTP_CHANNEL = "sftp";
  private static final int SFTP_TIMEOUT = 10000;
  private static final int SFTP_PORT = 22;

  // Request types.
  private static final String REQUEST_FILE_TYPES_COMMAND = "filetypes";
  private static final String REQUEST_STORAGE_RULES_COMMAND = "storagerules";
  private static final String REQUEST_SIP_TRANSPORT_PARAMETERS_COMMAND = "FTPInfo";

  // **
  // STATE (Instance variables)
  // **

  private String keyStoreFile = "";
  private String sourceId = "";
  private String cmUser = "";
  private String cmPwd = "";
  private String knownHostsFile = "";
  private String ingestServer = null;

  private int ingestPort = 443;

  private boolean strictHostKeyChecking = true;
  private boolean useHttps = true;

  // **
  // CLASS VARIABLES (Static variables)
  // **

  private static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");

  // Resources.
  private Client diasClient;

  // FTP transport parameters as returned from DIAS.
  private static String ftpUser = "";
  private static String sipPreloadarea = "";
  private static String ftpServer = "";
  private static String ftpPassword = "";

  // **
  // CREATION (Constructors, factory methods, static/inst init)
  // **

  /**
   * 
   */
  public DiasIngest() {
    this.diasClient = ClientBuilder.newClient();
  }

  /**
   * <p>
   * Constructor with the server to connect.
   * </p>
   * 
   * @param serverReqAddress host name or ip address
   * @param serverReqPort the port number
   */
  public DiasIngest(String serverReqAddress, int serverReqPort) {
    this.ingestServer = serverReqAddress;
    this.ingestPort = serverReqPort;
    this.diasClient = ClientBuilder.newClient();
  }

  // **
  // MANIPULATION (Manipulation - what the object does)
  // **

  /**
   * <p>
   * Retrieves the list of supported file types from DIAS and returns it as xml.
   * </p>
   * 
   * @return A NodeList with nodes as specified in the LmerSIP Interface Specification IFI_SIP002A
   * @throws IOException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public NodeList requestFileTypesXml()
      throws ParserConfigurationException, SAXException, IOException {
    String diasResponse = requestFileTypes();
    Document fileTypesDocument = KolibriXMLParser.getDocumentFromString(diasResponse);
    return fileTypesDocument.getElementsByTagName("Item");
  }

  /**
   * <p>
   * Requests from DIAS the supported file types.
   * </p>
   * 
   * @return XML as String with the supported fileTypes
   * @throws IOException
   */
  public String requestFileTypes() throws IOException {
    return retrieveDataFromDIAS(
        ACCESS_MANAGER_COMMAND_URL + REQUEST_FILE_TYPES_COMMAND + SOURCEID_URL + this.sourceId);
  }

  /**
   * <p>
   * Retrieves the list of supported storage rules from DIAS and returns it as XML.
   * </p>
   * 
   * @return A NodeList with nodes as specified in the LmerSIP Interface Specification IFI_SIP011A
   * @throws IOException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public NodeList requestStorageRulesXml()
      throws ParserConfigurationException, SAXException, IOException {
    String diasResponse = requestStorageRules();
    Document fileTypesDocument = KolibriXMLParser.getDocumentFromString(diasResponse);
    return fileTypesDocument.getElementsByTagName("Item");
  }

  /**
   * <p>
   * Requests the list of supported storage rules from DIAS.
   * </p>
   * 
   * @return XML as string with the supported storage rules
   * @throws IOException
   */
  private String requestStorageRules() throws IOException {
    return retrieveDataFromDIAS(
        ACCESS_MANAGER_COMMAND_URL + REQUEST_STORAGE_RULES_COMMAND + SOURCEID_URL + this.sourceId);
  }

  /**
   * <p>
   * Initializes/updates variables with FTP transport parameters from DIAS if we haven't initialized
   * them already.
   * </p>
   * 
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws IOException
   */
  public void initializeFtpParameters()
      throws IOException, ParserConfigurationException, SAXException {
    if (ftpServer.equals("")) {
      forceInitializeFtpParameters();
    }
  }

  /**
   * <p>
   * Requests the FTP transport parameters from DIAS.
   * </p>
   * 
   * @return XML as string with the FTP transport parameters
   * @throws IOException
   */
  public String requestSipTransportParameters() throws IOException {
    return retrieveDataFromDIAS(
        ACCESS_MANAGER_COMMAND_URL + REQUEST_SIP_TRANSPORT_PARAMETERS_COMMAND + SOURCEID_URL
            + this.sourceId + USER_URL + this.cmUser + PASSWORD_URL + this.cmPwd);
  }

  /**
   * <p>
   * Retrieves the list of FTP transport parameters from DIAS and returns it as XML.
   * </p>
   * 
   * @return A NodeList with nodes as specified in the LmerSIP Interface Specification IFI_SIP021A
   * @throws IOException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public NodeList requestSipTransportParametersXml()
      throws IOException, ParserConfigurationException, SAXException {

    Document fileTypesDocument;
    String diasResponse = requestSipTransportParameters();

    if (!diasResponse.equals("")) {
      fileTypesDocument = KolibriXMLParser.getDocumentFromString(diasResponse);
    } else {
      throw new IOException("XML response from DIAS is empty, please check the request parameters");
    }

    return fileTypesDocument.getElementsByTagName("FTPInfoItem");
  }

  /**
   * <p>
   * Transfers asset to an FTP server specified with address, port, user and password in config file
   * and not as returned from DIAS.
   * </p>
   * 
   * @param serverDir The directory where to save the asset. If null the sipPreloadarea as set with
   *        initializeFtpParameters() is used. If initializeFtpParameters() hasn't been called the
   *        current directory (usually the home dir) is used.
   * @param asset
   * @throws IOException
   */
  public void submitFileToFtp(File asset, String serverDir) throws IOException {

    FTPClient ftp = new FTPClient();
    FileInputStream fis = null;

    try {

      defaultLogger.log(Level.FINE, "Connecting to : " + this.ingestServer + this.ingestPort);

      ftp.connect(this.ingestServer, this.ingestPort);

      defaultLogger.log(Level.FINE,
          "Successfully connected to : " + this.ingestServer + this.ingestPort);

      if (!FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
        ftp.disconnect();
        throw new IOException(
            "Server refused FTP connection. System message: " + ftp.getReplyCode());
      }

      // Try login, if it fails try again with new FTP parameters.
      if (!ftp.login(this.cmUser, this.cmPwd)) {
        throw new IOException(
            "Server refused FTP user and password. System message: " + ftp.getReplyCode());
      }

      ftp.enterLocalPassiveMode();
      ftp.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);

      if (serverDir != null) {
        // Try changing to work dir.
        if (!ftp.changeWorkingDirectory(serverDir)) {
          throw new IOException("Changing to directory " + serverDir + " failed. System message: "
              + ftp.getReplyCode());
        }
      } else if (!sipPreloadarea.equals("")) {
        // Try changing to work dir.
        if (!ftp.changeWorkingDirectory(sipPreloadarea)) {
          throw new IOException("Changing to directory " + serverDir + " failed. System message: "
              + ftp.getReplyCode());
        }
      }

      fis = new FileInputStream(asset);
      String filename = asset.getName() + ".tmp";
      if (!ftp.storeFile(filename, fis)) {
        throw new IOException("Uploading file failed: " + ftp.getReplyCode());
      }

      if (!ftp.rename(filename, asset.getName())) {
        throw new IOException("Renaming asset failed! " + ftp.getReplyCode());
      }

      ftp.logout();

    } finally {
      if (ftp.isConnected()) {
        ftp.disconnect();
      }
      if (fis != null) {
        fis.close();
      }
    }
  }

  /**
   * <p>
   * Returns a ticket number from DIAS for a given SIP ingest name as XML.
   * </p>
   * 
   * @return
   * @throws IOException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public NodeList requestSipIngestXml(String _sipName)
      throws ParserConfigurationException, SAXException, IOException {

    Document sipTicketNumberDocument;

    String diasResponse = requestSipIngest(_sipName);

    if (!diasResponse.equals("")) {
      sipTicketNumberDocument = KolibriXMLParser.getDocumentFromString(diasResponse);
    } else {
      throw new IOException("XML response from DIAS is empty, please check the request parameters");
    }

    return sipTicketNumberDocument.getElementsByTagName("METADATABLOCK");
  }


  /**
   * <p>
   * Returns a ticket number from DIAS for a given SIP ingest name.
   * </p>
   * 
   * @return
   * @throws IOException
   */
  public String requestSipIngest(String _sipName) throws IOException {
    return retrieveDataFromDIAS(INGEST_STATUS_COMMAND_URL + USER_URL + this.cmUser + PASSWORD_URL
        + this.cmPwd + SIP_NAME_URL + _sipName);
  }

  /**
   * <p>
   * Returns a ticket number from DIAS for a given SIP ingest name as XML.
   * </p>
   * 
   * @return
   * @throws IOException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public NodeList requestSipIngestStatusXml(String _theTicket)
      throws ParserConfigurationException, SAXException, IOException {

    Document sipIngestStatusDocument;
    String diasResponse = requestSipIngestStatus(_theTicket);

    if (!diasResponse.equals("")) {
      sipIngestStatusDocument = KolibriXMLParser.getDocumentFromString(diasResponse);
    } else {
      throw new IOException(
          "XML response from DIAS is empty, please check your request parameters");
    }

    return sipIngestStatusDocument.getElementsByTagName("METADATABLOCK");
  }

  /**
   * <p>
   * Returns an ingest status from DIAS for a given SIP ingest name.
   * </p>
   * 
   * @return
   * @throws IOException
   */
  public String requestSipIngestStatus(String _theTicket) throws IOException {
    return retrieveDataFromDIAS(INGEST_STATUS_COMMAND_URL + TICKET_ID_URL + _theTicket);
  }

  /**
   * <p>
   * Parses the ingest response of the DIAS. Uses Jakarta Commons digester.
   * </p>
   * 
   * @param diasResponse A DIAS response as a NodeList taken from a requestSomethingXml() method.
   * @return A DiasIngestResponse object filled with useful information.
   * @throws SAXException
   * @throws IOException
   */
  public static DiasIngestResponse parseDiasResponse(Reader diasResponse)
      throws IOException, SAXException {

    DiasIngestResponse result = null;

    Digester responseParser = new Digester();

    // Validating requires inline DTD in DIAS response.
    responseParser.setValidating(true);

    // Create a DiasIngestResponse object for the DIAS response and map the properties.
    responseParser.addObjectCreate("MetadataBlock", DiasIngestResponse.class);
    responseParser.addSetProperties("MetadataBlock", "type", "responseType");
    responseParser.addSetProperties("MetadataBlock", "version", "responseVersion");
    // Note: It is necessary to repeat the property in lower case.
    responseParser.addBeanPropertySetter("MetadataBlock/Description", "description");

    // Create a DiasIngestMetadataResponse object for the corresponding part in the response and map
    // the properties.
    responseParser.addObjectCreate("MetadataBlock/Metadata",
        "de.langzeitarchivierung.kolibri.ingest.DiasIngestMetadataResponse");
    responseParser.addBeanPropertySetter("MetadataBlock/Metadata/SIP_Filename", "sipFilename");
    responseParser.addBeanPropertySetter("MetadataBlock/Metadata/SIPIngestTicketNumber",
        "sipIngestTicketNumber");
    responseParser.addBeanPropertySetter("MetadataBlock/Metadata/SIP.preloadarea",
        "sipPreloadarea");
    responseParser.addBeanPropertySetter("MetadataBlock/Metadata/FTP_Password", "ftpPassword");
    responseParser.addBeanPropertySetter("MetadataBlock/Metadata/FTP_Server", "ftpServer");
    responseParser.addBeanPropertySetter("MetadataBlock/Metadata/FTP_User", "ftpUser");
    responseParser.addBeanPropertySetter("MetadataBlock/Metadata/StartTimestamp",
        "startTimestamp");
    responseParser.addBeanPropertySetter("MetadataBlock/Metadata/SourceID", "sourceId");
    responseParser.addBeanPropertySetter("MetadataBlock/Metadata/State", "state");
    responseParser.addBeanPropertySetter("MetadataBlock/Metadata/Substate", "substate");
    responseParser.addBeanPropertySetter("MetadataBlock/Metadata/SupplementaryInformation",
        "supplementaryInformation");
    responseParser.addBeanPropertySetter("MetadataBlock/Metadata/Timestamp", "timestamp");
    responseParser.addBeanPropertySetter("MetadataBlock/Metadata/Alert", "alert");

    // Advance to next block.
    responseParser.addSetNext("MetadataBlock/Metadata", "addMetadata");

    defaultLogger.log(Level.FINER, "Parsing DIAS response...");

    result = (DiasIngestResponse) responseParser.parse(diasResponse);

    defaultLogger.log(Level.FINER, "Setting FTP parameters...");

    // Set FTP transport parameters.
    ftpUser = result.getMetadata().get(0).getFtpUser();
    sipPreloadarea = result.getMetadata().get(0).getSipPreloadarea();
    ftpServer = result.getMetadata().get(0).getFtpServer();
    ftpPassword = result.getMetadata().get(0).getFtpPassword();

    return result;
  }

  /**
   * <p>
   * Parses all attribute information out of a DIAS format response and sets all affected attributes
   * in the returned DiasIngestResponse object using the apache.commons.digester.
   * </p>
   * 
   * @param formatResponse
   * @return
   * @throws SAXException
   * @throws IOException
   */
  public static ArrayList<FormatInfo> parseDiasFormatResponse(String formatResponse)
      throws SAXException, IOException {

    ArrayList<FormatInfo> result = new ArrayList<FormatInfo>();

    // Use the commons.digester for XML parsing.
    Digester formatParser = new Digester();
    formatParser.setValidating(true);
    formatParser.addObjectCreate("FILETYPES", ArrayList.class);
    formatParser.addObjectCreate("FILETYPES/Item", FormatInfo.class);
    formatParser.addSetProperties("FILETYPES/Item");
    formatParser.addSetNext("FILETYPES/Item", "add");

    StringReader sr = new StringReader(formatResponse);
    result = (ArrayList<FormatInfo>) formatParser.parse(sr);

    return result;
  }

  /**
   * <p>
   * Tests if the specified file exists in the specified directory. Useful for checking the error
   * area.
   * </p>
   * 
   * @param asset the filename to check
   * @param serverDir the directory in which to look for the file.
   * @return true if the file exists, otherwise false
   * @throws JSchException
   * @throws FileNotFoundException
   */
  public boolean existsFile(File asset, String serverDir)
      throws FileNotFoundException, JSchException {

    ChannelSftp sftpChannel = null;
    Session sftpSession = null;

    try {
      JSch jsch = new JSch();
      jsch.setKnownHosts(new FileInputStream(this.knownHostsFile));

      sftpSession = jsch.getSession(ftpUser, ftpServer);
      sftpSession.setPassword(ftpPassword);

      if (!this.strictHostKeyChecking) {
        // This is insecure!
        Hashtable<String, String> config = new Hashtable<String, String>();
        config.put("StrictHostKeyChecking", "no");
        sftpSession.setConfig(config);
      }

      sftpSession.connect();

      sftpChannel = (ChannelSftp) sftpSession.openChannel("sftp");
      sftpChannel.connect();

      sftpChannel.cd(serverDir);

      sftpChannel.get(asset.getName());
    } catch (SftpException e) {
      // TODO Check if this the correct error code. It is unknown because Jsch has no code
      // documentation :-(
      if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
        return false;
      }

    } finally {
      if (sftpChannel != null && sftpChannel.isConnected()) {
        sftpChannel.disconnect();
      }
      if (sftpSession != null && sftpSession.isConnected()) {
        sftpSession.disconnect();
      }
    }

    return true;
  }

  /**
   * <p>
   * Tests if the specified file name exists in the specified directory. Useful for checking the
   * error area.
   * </p>
   * 
   * @param asset the filename to check
   * @param serverDir the directory in which to look for the file.
   * @return true if the file exists, otherwise false
   * @throws JSchException
   * @throws FileNotFoundException
   * @throws SftpException
   */
  public boolean existsFile(String asset, String serverDir)
      throws FileNotFoundException, JSchException, SftpException {

    JSch jsch = new JSch();
    Session sftpSession = null;
    ChannelSftp sftpChannel = null;
    java.util.Vector fileList = null;
    boolean exists = false;

    try {
      jsch.setKnownHosts(new FileInputStream(this.knownHostsFile));

      sftpSession = jsch.getSession(ftpUser, ftpServer);
      sftpSession.setPassword(ftpPassword);
      if (!this.strictHostKeyChecking) {
        // This is insecure!
        Hashtable<String, String> config = new Hashtable<String, String>();
        config.put("StrictHostKeyChecking", "no");
        sftpSession.setConfig(config);
      }
      sftpSession.connect();

      if (sftpSession != null) {
        sftpChannel = (ChannelSftp) sftpSession.openChannel("sftp");
        sftpChannel.connect();
        fileList = sftpChannel.ls(serverDir);
        com.jcraft.jsch.ChannelSftp.LsEntry le = null;
        for (int i = 0; i < fileList.size(); i++) {
          le = (com.jcraft.jsch.ChannelSftp.LsEntry) fileList.get(i);
          if (asset.equalsIgnoreCase(le.getFilename())) {
            exists = true;
            break;
          }
        }
      }

    } finally {
      // TODO close() or exit()?
      if (sftpChannel != null && sftpChannel.isConnected())
        sftpChannel.exit();
      if (sftpSession != null && sftpSession.isConnected())
        sftpSession.disconnect();
    }

    return exists;
  }

  /**
   * <p>
   * Transfers a file to DIAS via SFTP.
   * </p>
   * 
   * @param asset The File to transfer.
   * @throws Exception
   */
  public void submitFileToDias(File asset) throws Exception {

    ChannelSftp sftpChannel = null;
    Session sftpSession = null;
    BufferedInputStream bis = null;

    try {
      JSch jsch = new JSch();

      // Only use known hosts file if existing!
      File knownHosts = new File(this.knownHostsFile);
      if (knownHosts.exists()) {
        jsch.setKnownHosts(new FileInputStream(knownHosts));
      } else {
        defaultLogger.warning("No known hosts file set or not found: " + this.knownHostsFile);
      }

      defaultLogger.fine("Get SFTP session for " + ftpUser + "@" + ftpServer + ":" + SFTP_PORT);

      // FIME Do we need to set the port?
      sftpSession = jsch.getSession(ftpUser, ftpServer, SFTP_PORT);
      sftpSession.setPassword(ftpPassword);

      if (!this.strictHostKeyChecking) {
        // This is insecure!
        Hashtable<String, String> config = new Hashtable<String, String>();
        config.put("StrictHostKeyChecking", "no");
        sftpSession.setConfig(config);
      }

      defaultLogger.fine("Connecting with timeout " + SFTP_TIMEOUT);
      sftpSession.connect(SFTP_TIMEOUT);

      defaultLogger.finest("Opening channel " + SFTP_CHANNEL);
      sftpChannel = (ChannelSftp) sftpSession.openChannel(SFTP_CHANNEL);

      defaultLogger.finest("Connecting to channel " + SFTP_CHANNEL);
      sftpChannel.connect();

      defaultLogger.fine("CDing to preload area: " + sipPreloadarea);
      sftpChannel.cd(sipPreloadarea);

      bis = new BufferedInputStream(new FileInputStream(asset));
      String filename = asset.getName() + ".tmp";

      defaultLogger.fine("Uploading asset: " + filename);
      sftpChannel.put(bis, filename);
      bis.close();

      defaultLogger.finest("Renaming asset to: " + asset.getName());
      sftpChannel.rename(asset.getName() + ".tmp", asset.getName());

      defaultLogger.finest("Exiting channel " + SFTP_CHANNEL);
      sftpChannel.exit();

    } catch (JSchException | SftpException | FileNotFoundException e) {
      defaultLogger.severe(
          "Error submitting SIP to DIAS. " + e.getClass().getName() + ": " + e.getMessage());
      throw e;
    } finally {
      if (sftpChannel != null && sftpChannel.isConnected()) {
        sftpChannel.disconnect();
        defaultLogger.fine("Channel disconnected");
      }
      if (sftpSession != null && sftpSession.isConnected()) {
        sftpSession.disconnect();
        defaultLogger.fine("Session disconnected");
      }
      if (bis != null) {
        bis.close();
      }
    }
  }

  /**
   * <p>
   * Transfers a input stream to DIAS via SFTP.
   * </p>
   * 
   * @param asset The input stream to transfer.
   * @param serverDir The directory where to save the asset. If null the sipPreloadarea as set with
   *        initializeFtpParameters() is used. If null and initializeFtpParameters() hasn't been
   *        called the current directory (usually the home dir) is used.
   * @throws JSchException
   * @throws SftpException
   * @throws IOException
   */
  public void submitInputStreamToDias(InputStream in, String name, String serverDir)
      throws JSchException, SftpException, IOException {

    ChannelSftp sftpChannel = null;
    Session sftpSession = null;
    BufferedInputStream fis = new BufferedInputStream(in);

    try {
      JSch jsch = new JSch();

      jsch.setKnownHosts(this.knownHostsFile);

      sftpSession = jsch.getSession(ftpUser, ftpServer);
      sftpSession.setPassword(ftpPassword);
      if (!this.strictHostKeyChecking) {
        // NOTE This is insecure! Use only for local testing!
        Hashtable<String, String> config = new Hashtable<String, String>();
        config.put("StrictHostKeyChecking", "no");
        sftpSession.setConfig(config);
      }
      sftpSession.connect();

      sftpChannel = (ChannelSftp) sftpSession.openChannel("sftp");
      sftpChannel.connect();

      if (serverDir != null) {
        sftpChannel.cd(serverDir);
      } else if (!sipPreloadarea.equals("")) {
        sftpChannel.cd(sipPreloadarea);
      }

      String filename = name + ".tmp";
      sftpChannel.put(fis, filename);
      sftpChannel.rename(filename, name);

    } finally {
      if (sftpChannel != null && sftpChannel.isConnected()) {
        sftpChannel.disconnect();
      }
      if (sftpSession != null && sftpSession.isConnected()) {
        sftpSession.disconnect();
      }
      if (fis != null) {
        fis.close();
      }
    }
  }

  // **
  // INTERNAL (Internal - implementation details, local classes, ...)
  // **

  /**
   * <p>
   * Initializes/updates variables with FTP transport parameters from DIAS.
   * </p>
   * 
   * TODO Check if this method should be synchronized. Or doesn't it matter if the values are
   * initialized two times?
   * 
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws IOException
   * 
   */
  private void forceInitializeFtpParameters()
      throws IOException, ParserConfigurationException, SAXException {

    NodeList ftpParameters = requestSipTransportParametersXml();

    // NOTE: DIAS returns only one FTP parameter set although more sets are possible according to
    // the specification.
    Element ftpParameter = (Element) ftpParameters.item(0);

    ftpUser = ftpParameter.getAttribute("FTP_User");
    sipPreloadarea = ftpParameter.getAttribute("SIP.Preloadarea");
    ftpServer = ftpParameter.getAttribute("FTP_Server");
    ftpPassword = ftpParameter.getAttribute("FTP_Password");
  }

  /**
   * <p>
   * Gets the data of the specified URL from the server. If setUseHttps(false) has been called, HTTP
   * is used, else (or if setUseHttps(true) has been called) HTTPS is used.
   * </p>
   * 
   * @param url The path of the data to retrieve (without serverAddress and serverRequestPort but
   *        with leading slash)
   * @return The retrieved data as a string
   * @throws IOException
   */
  private String retrieveDataFromDIAS(String url) throws IOException {

    int start;
    int end;

    // Check the protocol.
    String protocol = "https";
    if (!this.useHttps) {
      protocol = "http";
    }

    // Assemble the complete URL.
    String completeUrl = protocol + "://" + this.ingestServer + ":" + this.ingestPort + url;
    String urlToLog = completeUrl;

    // Test if some password information is included.
    if (completeUrl.indexOf(PASSWORD_URL) != -1) {
      start = completeUrl.indexOf(PASSWORD_URL) + PASSWORD_URL.length();
      if ((end = completeUrl.indexOf("&", start)) == -1) {
        end = completeUrl.length();
      }
      urlToLog = completeUrl.substring(0, start) + "***" + completeUrl.substring(end);
    }

    // Finally log the password clean URL.
    defaultLogger.log(Level.FINE, "DIAS request URL: " + urlToLog);

    // Start retrieving data from DIAS.
    Response response = this.diasClient.target(completeUrl).request().get();
    // GetMethod diasRequest = new GetMethod(completeUrl);

    try {
      StringBuffer result = new StringBuffer();
      InputStream responseStream = new BufferedInputStream(response.readEntity(InputStream.class));
      byte[] buffer = new byte[INPUT_BUFFER_LENGTH];

      int read = responseStream.read(buffer);
      while (read > 0) {
        result.append(new String(buffer, 0, read));
        read = responseStream.read(buffer);
      }

      return result.toString();

    } finally {
      response.close();
    }
  }

  // **
  // GET & SET METHODS
  // **

  /**
   * @return
   */
  public String getCmPwd() {
    return this.cmPwd;
  }

  /**
   * @param password
   */
  public void setCmPwd(String password) {
    this.cmPwd = password;
  }

  /**
   * @return
   */
  public String getSourceId() {
    return this.sourceId;
  }

  /**
   * @param sourceId
   */
  public void setSourceId(String sourceId) {
    this.sourceId = sourceId;
  }

  /**
   * @return
   */
  public String getCmUser() {
    return this.cmUser;
  }

  /**
   * @param user
   */
  public void setCmUser(String user) {
    this.cmUser = user;
  }

  /**
   * @return
   */
  public int getIngestPort() {
    return this.ingestPort;
  }

  /**
   * @param port
   */
  public void setIngestPort(int port) {
    this.ingestPort = port;
  }

  /**
   * @return
   */
  public String getIngestServer() {
    return this.ingestServer;
  }

  /**
   * @param serverAddress
   */
  public void setIngestServer(String serverAddress) {
    this.ingestServer = serverAddress;
  }

  /**
   * @return
   */
  public boolean isUseHttps() {
    return this.useHttps;
  }

  /**
   * @param useHttps
   */
  public void setUseHttps(boolean useHttps) {
    this.useHttps = useHttps;
  }

  /**
   * @return
   */
  public String getKnownHostsFile() {
    return this.knownHostsFile;
  }

  /**
   * @param knownHostsFile
   */
  public void setKnownHostsFile(String knownHostsFile) {
    this.knownHostsFile = knownHostsFile;
  }

  /**
   * @param strictHostKeyChecking
   */
  public void setStrictHostKeyChecking(boolean strictHostKeyChecking) {
    this.strictHostKeyChecking = strictHostKeyChecking;
  }

  /**
   * @return
   */
  public String getKeyStoreFile() {
    return this.keyStoreFile;
  }

  /**
   * @param keyStoreFile
   */
  public void setKeyStoreFile(String keyStoreFile) {
    System.setProperty("javax.net.ssl.trustStore", keyStoreFile);
    this.keyStoreFile = keyStoreFile;
  }

}
