/**
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk, SUB Göttingen
 */

package de.langzeitarchivierung.kolibri.ingest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2007-07-10 - koLibRI version 1.0
 */

/**
 * <p>
 * The DIAS response to ingest requests as a bean. To understand the data fields read the SIP
 * Interface Specification.
 * </p>
 * 
 * @version 2019-11-26
 * @since 2007-03-08
 */

public class DiasIngestResponse implements Serializable {

  // **
  // CONSTANTS
  // **

  private static final long serialVersionUID = 1L;

  public static final String NOT_IN_DIAS_RESPONSE = "NOT IN DIAS RESPONSE";
  public static final String RESPONSE_TYPE_BUSY = "busy";
  public static final String RESPONSE_TYPE_ERROR = "error";
  public static final String RESPONSE_VERSION = "1.0";

  // **
  // STATE (Instance variables)
  // **

  private String responseType;
  private String responseVersion;
  // Between the DESCRIPTION tag.
  private String description;
  // Between the METADATA tag.
  private List<DiasIngestMetadataResponse> metadata;

  // **
  // CREATION (Constructors, factory methods, static/inst init)
  // **

  /**
   * 
   */
  public DiasIngestResponse() {
    super();
    this.metadata = new ArrayList<DiasIngestMetadataResponse>();
  }

  // **
  // GET & SET METHODS
  // **

  /**
   * @return
   */
  public String getDescription() {
    return this.description;
  }

  /**
   * @param description
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return
   */
  public List<DiasIngestMetadataResponse> getMetadata() {
    return this.metadata;
  }

  /**
   * @param md
   */
  public void addMetadata(DiasIngestMetadataResponse md) {
    this.metadata.add(md);
  }

  /**
   * @return
   */
  public String getResponseVersion() {
    return this.responseVersion;
  }

  /**
   * @param responseVersion
   */
  public void setResponseVersion(String responseVersion) {
    this.responseVersion = responseVersion;
  }

  /**
   * @return
   */
  public String getResponseType() {
    return this.responseType;
  }

  /**
   * @param responseType
   */
  public void setResponseType(String responseType) {
    this.responseType = responseType;
  }

  // **
  // QUERIES (Queries - no change to object's state)
  // **

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  public String toString() {

    String result = "";

    result += "responseType: " + this.responseType;
    result += "\nresponseVersion: " + this.responseVersion;
    result += "\ndescription: " + this.description;

    int j = 0;
    for (Iterator<DiasIngestMetadataResponse> i = this.metadata.iterator(); i.hasNext(); j++) {
      DiasIngestMetadataResponse element = i.next();
      result += "\nmetadatablock #" + j + ":\n" + element;
    }

    return result;
  }

}
