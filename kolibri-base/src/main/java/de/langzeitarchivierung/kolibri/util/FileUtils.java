/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / FileUtils.java
 *  
 * Copyright 2005-2007 by Project kopal
 * Copyright 2009-2011 by Project dp4lib
 * 
 * http://kopal.langzeitarchivierung.de/
 * http://dp4lib.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 *******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 
 *	2011-08-18	Funk	Added getBytesFromFile() from kolibri-diagnose-service.
 *	2009-08-31	Funk	Added NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT.
 *	2007-08-21	Funk	Added some unzip methods.
 *	2007-07-10	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.collections.ListUtils;

/*******************************************************************************
 * <p>
 * Some useful file load and store routines.
 * </p>
 * 
 * @author Stefan Funk, SUB Göttingen
 * @version 2016-06-08
 * @since 2006-02-25
 ******************************************************************************/

public class FileUtils {

	// CLASS VARIABLES (Static variables) **************************************

	// TODO Use URIs here?
	public static String	NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT	= "_";

	// MANIPULATION (Manipulation - what the object does) **********************

	/***************************************************************************
	 * <p>
	 * Returns the content of a text file as a String.
	 * </p>
	 * 
	 * @param file
	 *            File to read
	 * @param encoding
	 *            Encoding of the text. Default is "UTF-8" if called with null.
	 * @return Contents of the file as a String
	 * @version 20060225
	 * @since 20060225
	 **************************************************************************/
	public static String readFile(java.io.File file, String encoding) {
		// Default is "UTF-8" if called with null or empty string.
		if (encoding == null || encoding.equals("")) {
			encoding = "UTF-8";
		}

		String text = "";
		java.io.FileInputStream in = null;

		try {
			in = new java.io.FileInputStream(file);
			int fl = (int) file.length();
			byte buffer[] = new byte[fl];
			int len = in.read(buffer, 0, fl);
			// Read all the text file with the given encoding.
			if (encoding.equals("none")) {
				text = new String(buffer, 0, len);
			} else {
				text = new String(buffer, 0, len, encoding);
			}
		} catch (java.io.IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null)
					in.close();
			} catch (java.io.IOException e) {
				e.printStackTrace();
			}
		}

		return text;
	}

	/***************************************************************************
	 * <p>
	 * Opens the given text file with the given encoding and saves it as an
	 * other file with another encoding. We use it mainly to convert german
	 * "ISO-8859-1" encoding to "UTF-8" but many other combinations are
	 * possible.
	 * </p>
	 * 
	 * @param source
	 *            File to read
	 * @param encodingSource
	 *            Encoding of the source text.
	 * @param destination
	 *            Destination file to write into
	 * @param encodingDestination
	 *            Encoding of the destination text.
	 * @throws IOException
	 * @version 20070414
	 * @since 20070414
	 **************************************************************************/
	public static void convertFile(File source, String encodingSource,
			File destination, String encodingDestination) throws IOException {
		Reader input = null;
		Writer output = null;

		try {
			// Define Buffers to make it faster
			input = new BufferedReader(new InputStreamReader(
					new FileInputStream(source), encodingSource));
			output = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(destination), encodingDestination));
			int i = 0;
			while ((i = input.read()) != -1) {
				output.write(i);
			}
		} finally {
			if (input != null)
				input.close();
			if (output != null)
				output.close();
		}
	}

	/***************************************************************************
	 * <p>
	 * Simply reads a file with UTF-8.
	 * </p>
	 * 
	 * @param file
	 * @return The file as text.
	 **************************************************************************/
	public static String readFile(java.io.File file) {
		return readFile(file, "UTF-8");
	}

	/***************************************************************************
	 * <p>
	 * Stores a string into a file.
	 * </p>
	 * 
	 * @param file
	 * @param encoding
	 * @param text
	 * @return boolean - true if the store operation was successful
	 **************************************************************************/
	public static boolean storeFile(File file, String encoding, String text) {
		// Default is "UTF-8" if called with null or empty string.
		if (encoding == null || encoding.equals("")) {
			encoding = "UTF-8";
		}

		java.io.FileOutputStream os = null;
		try {
			os = new java.io.FileOutputStream(file);
			byte buffer[];
			if (encoding.equals("none")) {
				buffer = text.getBytes();
			} else {
				buffer = text.getBytes(encoding);
			}
			os.write(buffer);
		} catch (java.io.IOException e) {
			return false;
		} finally {
			try {
				if (os != null)
					os.close();
			} catch (java.io.IOException e) {
				e.printStackTrace();
			}
		}

		return true;
	}

	/***************************************************************************
	 * <p>
	 * Simply stores a file with "UTF-8" encoding.
	 * </p>
	 * 
	 * @param file
	 * @param text
	 * @return boolean - true if the store operation was successful
	 **************************************************************************/
	public static boolean storeFile(File file, String text) {
		return storeFile(file, "UTF-8", text);
	}

	/***************************************************************************
	 * <p>
	 * Converts all non letter or digit characters of the asset identifier to
	 * underscores for general filename compliance.
	 * </p>
	 * 
	 * @param idString
	 * @return the converted identifier
	 **************************************************************************/
	public static String getFilenamePersistentIdentifier(String idString)
			throws Exception {
		if (idString != null) {
			StringBuffer result = new StringBuffer();
			for (int i = 0; i < idString.length(); i++) {
				if (Character.isLetterOrDigit(idString.charAt(i))) {
					result.append(idString.charAt(i));
				} else {
					result.append(NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT);
				}
			}

			return result.toString();
		}

		throw new Exception(
				"The object's persistent identifier is needed to generate a filename persistent identifier");
	}

	/***************************************************************************
	 * <p>
	 * Recursive deletion of the given directory.
	 * </p>
	 * 
	 * @param dir
	 *            The directory or file to delete
	 **************************************************************************/
	public static void deleteDir(File dir) {
		if (dir.isDirectory()) {
			File dirEntries[] = dir.listFiles();
			for (int i = 0; i < dirEntries.length; i++) {
				if (dirEntries[i].isDirectory()) {
					deleteDir(dirEntries[i]);
				} else {
					dirEntries[i].delete();
				}
			}
			dir.delete();
		}
	}

	/***************************************************************************
	 * <p>
	 * Recursive deletion of the Files within the given directory.
	 * </p>
	 * 
	 * @param dir
	 *            The directory or file to delete
	 **************************************************************************/
	public static void deleteFiles(File dir) {
		if (dir.isDirectory()) {
			File dirEntries[] = dir.listFiles();
			for (int i = 0; i < dirEntries.length; i++) {
				if (dirEntries[i].isDirectory()) {
					deleteDir(dirEntries[i]);
				} else {
					dirEntries[i].delete();
				}
			}
		}
	}

	/***************************************************************************
	 * <p>
	 * Computes the size of a given file or directory.
	 * </p>
	 * 
	 * @param currentFile
	 * @return The file size of course :) What else?
	 **************************************************************************/
	public static long getCompleteFileSize(File currentFile) {
		long completeFileLength = 0;

		// If it is a file, directly return it's size.
		if (currentFile.isFile()) {
			completeFileLength = currentFile.length();
		} else {
			// Otherwise get the file list and go through all directories and
			// add their lengths.
			File fileList[] = currentFile.listFiles();

			for (int i = 0; i < fileList.length; i++) {
				if (fileList[i].isDirectory()) {
					completeFileLength += getCompleteFileSize(fileList[i]);
				} else {
					completeFileLength += fileList[i].length();
				}
			}
		}

		return completeFileLength;
	}

	/***************************************************************************
	 * <p>
	 * Returns the amount of free space on disk.
	 * </p>
	 * 
	 * @param dir
	 *            The directory or file to ask how much free space available
	 * @return The amount of free space in this directory
	 **************************************************************************/
	public static long getFreeSpace(java.io.File dir) {
		long fs = 0;
		// TODO: implement the algorithm

		return fs;
	}

	/***************************************************************************
	 * <p>
	 * Untar a tar archive with destination as string.
	 * </p>
	 * 
	 * TODO Put in unpack/tar class.
	 * 
	 * @param tarName
	 *            Name of the Archive
	 * @param destDir
	 *            Destination directory
	 * @return Status code -> 0: Success, 1: destination Directory does not
	 *         exist
	 **************************************************************************/
	public static int untar(String tarName, String destDir) {
		// No errors.
		int status = 0;

		java.io.File f = new java.io.File(destDir);
		boolean success = f.mkdirs();
		if (success) {
			status = untar(new java.io.File(tarName), f);
		} else {
			status = 1;
		}

		return status;
	}

	/***************************************************************************
	 * <p>
	 * Untar a tar archive with destination as file object.
	 * </p>
	 * 
	 * TODO Put in unpack/tar class.
	 * 
	 * @param arcFile
	 *            The Archive to untar
	 * @param destDir
	 *            Destination directory
	 * @return Status code -> 0: Success, 1: destination Directory do not exists
	 **************************************************************************/
	public static int untar(java.io.File arcFile, java.io.File destDir) {
		// No errors.
		int status = 0;

		try {
			// Create the destination directory if it does not exist.
			destDir.mkdirs();

			System.out.println("Reading the archive " + "file: "
					+ arcFile.getName());

			java.io.InputStream is = new java.io.FileInputStream(arcFile);

			// Use the tar-Library. Its GPL :-)
			System.out.println("Buffering the contents");

			// com.ice.tar.TarArchive archive = new com.ice.tar.TarArchive(is,
			// BUFFER_SIZE);
			com.ice.tar.TarArchive archive = new com.ice.tar.TarArchive(is);

			System.out.println("Extracting to: " + destDir.getName());

			// Extract the content.
			archive.extractContents(destDir);
			// Close the archive.
			archive.closeArchive();

			System.out.println("TAR archive successfully extracted.");

		} catch (FileNotFoundException e) {
			status = 2;
			e.printStackTrace();
		} catch (IOException e) {
			status = 3;
			e.printStackTrace();
		}

		return status;
	}

	/***************************************************************************
	 * <p>
	 * Goes through all the ZIP file entries and stores all of them including
	 * all subdirectories.
	 * </p>
	 * 
	 * TODO Put in pack/zip class.
	 * 
	 * @param zipStream
	 * @param destFolder
	 * @throws Exception
	 **************************************************************************/
	public static void storeZipContent(File zipFile, File destFolder)
			throws Exception {
		ZipInputStream zipStream = new ZipInputStream(new BufferedInputStream(
				new FileInputStream(zipFile)));

		// Go through all the ZIP entries.
		ZipEntry metsEntry = zipStream.getNextEntry();

		while (metsEntry != null) {
			if (metsEntry.isDirectory()) {
				File newDir = new File(destFolder, metsEntry.getName());
				newDir.mkdir();
			} else {
				storeZipFile(zipStream,
						new File(destFolder, metsEntry.getName()));
			}

			metsEntry = zipStream.getNextEntry();
		}

		zipStream.close();
	}

	/**
	 * <p>
	 * Returns the contents of the file in a byte array.
	 * </p>
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		// Get the size of the file
		long length = file.length();

		// You cannot create an array using a long type. It needs to be an int
		// type. Before converting to an int type, check to ensure that file is
		// not larger than Integer.MAX_VALUE.
		if (length > Integer.MAX_VALUE) {
			// File is too large
			// TODO What then??
		}

		// Create the byte array to hold the data
		byte[] bytes = new byte[(int) length];

		// Read in the bytes
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length
				&& (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		// Close the input stream.
		is.close();

		// Ensure all the bytes have been read in
		if (offset < bytes.length) {
			throw new IOException("Could not completely read file "
					+ file.getName());
		}

		// Return bytes.
		return bytes;
	}

	// INTERNAL (Internal - implementation details, local classes, ...) ****

	/***************************************************************************
	 * <p>
	 * Stores a single file from the ZIP.
	 * </p>
	 * 
	 * TODO Put in pack/zip class.
	 * 
	 * @param zipStream
	 * @param destinationFile
	 * @throws Exception
	 **************************************************************************/
	private static void storeZipFile(ZipInputStream zipStream,
			File destinationFile) throws Exception {
		BufferedOutputStream tempMetsStream = null;
		destinationFile.getParentFile().mkdirs();
		try {
			tempMetsStream = new BufferedOutputStream(new FileOutputStream(
					destinationFile));

			// Decompress now.
			final int BUFFER_LENGTH = 256 * 1024;
			byte[] buffer = new byte[BUFFER_LENGTH];
			int bytesRead = zipStream.read(buffer);
			while (bytesRead > 0) {
				tempMetsStream.write(buffer, 0, bytesRead);
				bytesRead = zipStream.read(buffer);
			}

			tempMetsStream.flush();
		} catch (Exception e) {
			throw new Exception("Extraction failed. System message: "
					+ e.getMessage());
		} finally {
			if (tempMetsStream != null) {
				try {
					tempMetsStream.close();
				} catch (Exception closeE) {
					throw new Exception("Error closing stream: "
							+ closeE.getMessage());
				}
			}
		}
	}

	/**
	 * <p>
	 * Lists files recursively.
	 * </p>
	 * 
	 * @param source
	 * @return
	 */
	public static List<File> listFilesRecursivly(File source) {

		File fileArray[] = source.listFiles();
		List<File> fileList = new ArrayList<File>();
		fileList = (List<File>) Arrays.asList(fileArray);

		for (Iterator<File> i = fileList.iterator(); i.hasNext();) {
			File file = i.next();
			if (file.isDirectory()) {
				fileList = ListUtils.union(fileList, listFilesRecursivly(file));
			}
		}
		return fileList;
	}

}
