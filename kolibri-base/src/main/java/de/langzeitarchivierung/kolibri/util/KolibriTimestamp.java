/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / KolibriTimestamp.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * 
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 	2016-10-11	Funk	Fixed bug in HM-only-view.
 * 	2016-08-16	Funk	Added duration with seconds only.
 * 	2011-10-07	Funk	Added duration method with millis.
 *	2009-08-31	Funk	Added NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT.
 *	2007-07-10	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/*******************************************************************************
 * <p>
 * Class for generating timestamps.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @author Jens Ludwig, SUB Göttingen
 * @version 2016-10-11
 ******************************************************************************/

public class KolibriTimestamp {

	// CLASS VARIABLES (Static variables) **************************************

	// TODO Use URIs here?
	public static String	NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT	= "_";

	/***************************************************************************
	 * <p>
	 * W3C compliant timestamp: "yyyy-MM-dd'T'HH:mm:ss"
	 * </p>
	 * 
	 * @return "yyyy-MM-dd'T'HH:mm:ss"
	 **************************************************************************/
	public static String getTimestamp() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss");
		SimpleDateFormat timezone = new SimpleDateFormat("Z");
		GregorianCalendar timeStamp = new GregorianCalendar();
		return dateFormat.format(timeStamp.getTime())
				+ timezone.format(timeStamp.getTime()).substring(0, 3) + ":"
				+ timezone.format(timeStamp.getTime()).substring(3);
	}

	/***************************************************************************
	 * <p>
	 * Timestamp without time zone: "yyyy-MM-dd'T'HH:mm:ss"
	 * </p>
	 * 
	 * @param milliseconds
	 * @return "yyyy-MM-dd'T'HH:mm:ss"
	 **************************************************************************/
	public static String getTimestamp(long milliseconds) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss");
		GregorianCalendar timeStamp = new GregorianCalendar();
		timeStamp.setTimeInMillis(milliseconds);
		return dateFormat.format(timeStamp.getTime());
	}

	/***************************************************************************
	 * <p>
	 * Parses a date string as delivered by DIAS and returns its representation
	 * in milliseconds since the begin of the epoch.
	 * </p>
	 * 
	 * @param diasDate
	 *            A date string as delivered by DIAS
	 * @return timestamp in milliseconds
	 * @throws ParseException
	 **************************************************************************/
	public static long parseTimestamp(String diasDate) throws ParseException {
		SimpleDateFormat dateReader = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss.SSS");
		return dateReader.parse(diasDate).getTime();
	}

	/***************************************************************************
	 * <p>
	 * Timestamp without colons.
	 * </p>
	 * 
	 * @return "yyyy-MM-dd'T'HH_mm_ss"
	 **************************************************************************/
	public static String getFilenameTimestamp() {
		String result = "";

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy"
				+ NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT + "MM"
				+ NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT + "dd'T'HH"
				+ NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT + "mm"
				+ NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT + "ss");
		SimpleDateFormat timezone = new SimpleDateFormat("Z");
		GregorianCalendar timeStamp = new GregorianCalendar();

		result = dateFormat.format(timeStamp.getTime())
				+ NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT
				+ timezone.format(timeStamp.getTime()).substring(1, 3)
				+ NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT
				+ timezone.format(timeStamp.getTime()).substring(3);

		return result;
	}

	/***************************************************************************
	 * <p>
	 * Replace all occurrences of non ASCII letters or digits with "_".
	 * </p>
	 * 
	 * @param source
	 * @return the modified string
	 **************************************************************************/
	public static String makeFilenameCompatible(String source) {
		StringBuffer result = new StringBuffer();
		for (int i = 0; i < source.length(); i++) {
			if (!Character.isLetterOrDigit(source.charAt(i))) {
				result.append(source.charAt(i));
			} else {
				result.append("_");
			}
		}
		return result.toString();
	}

	/***************************************************************************
	 * <p>
	 * W3C compliant timestamp with milliseconds: "yyyy-MM-dd'T'HH:mm:ss.S"
	 * </p>
	 * 
	 * @return "yyyy-MM-dd'T'HH:mm:ss.SSS"
	 **************************************************************************/
	public static String getTimestampWithMs() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss.SSS");
		SimpleDateFormat timezone = new SimpleDateFormat("Z");
		GregorianCalendar timeStamp = new GregorianCalendar();
		return dateFormat.format(timeStamp.getTime())
				+ timezone.format(timeStamp.getTime()).substring(0, 3) + ":"
				+ timezone.format(timeStamp.getTime()).substring(3);
	}

	/***************************************************************************
	 * <p>
	 * W3C compliant timestamp with milliseconds: "yyyy-MM-dd'T'HH:mm:ss.S"
	 * </p>
	 * 
	 * @param milliseconds
	 *            - the milliseconds since 1970 for which a timestamp will be
	 *            computed
	 * @return "yyyy-MM-dd'T'HH:mm:ss.SSS"
	 **************************************************************************/
	public static String getTimestampWithMs(long milliseconds) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss.SSS");
		SimpleDateFormat timezone = new SimpleDateFormat("Z");
		GregorianCalendar timeStamp = new GregorianCalendar();
		timeStamp.setTimeInMillis(milliseconds);
		return dateFormat.format(timeStamp.getTime())
				+ timezone.format(timeStamp.getTime()).substring(0, 3) + ":"
				+ timezone.format(timeStamp.getTime()).substring(3);
	}

	/***************************************************************************
	 * <p>
	 * Timestamp with milliseconds without colons.
	 * </p>
	 * 
	 * @return "yyyy-MM-dd'T'HH_mm_ss.SSS"
	 **************************************************************************/
	public static String getFilenameTimestampWithMs() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH_mm_ss_SSS");
		SimpleDateFormat timezone = new SimpleDateFormat("Z");
		GregorianCalendar timeStamp = new GregorianCalendar();
		return dateFormat.format(timeStamp.getTime())
				+ timezone.format(timeStamp.getTime()).substring(0, 3) + "_"
				+ timezone.format(timeStamp.getTime()).substring(3);
	}

	/***************************************************************************
	 * <p>
	 * Return the duration without milliseconds per default and use some default
	 * unit strings.
	 * </p>
	 * 
	 * @param duration
	 * @return
	 **************************************************************************/
	public static String getDurationInHours(long duration) {
		return getDurationInHours(duration, false, "less than a", "s", "hour",
				"minute", "second", "millisecond");
	}

	/***************************************************************************
	 * <p>
	 * Return the duration with milliseconds per default and use some default
	 * unit strings.
	 * </p>
	 * 
	 * @param duration
	 * @return
	 **************************************************************************/
	public static String getDurationWithMillis(long duration) {
		return getDurationInHours(duration, true, "less than a", "s", "hour",
				"minute", "second", "millisecond");
	}

	/**
	 * <p>
	 * Gets you the duration in hours and minutes only.
	 * </p>
	 * 
	 * @param duration
	 * @return
	 */
	public static String getDurationHMOnly(long duration) {
		return getDurationInHours(duration, false, false, "less than a", "s",
				"hour", "minute", "second", "millisecond");
	}

	/**
	 * <p>
	 * Gets you the duration in hours, minutes, and seconds only.
	 * </p>
	 * 
	 * @param duration
	 * @return
	 */
	public static String getDurationHMSOnly(long duration) {
		return getDurationInHours(duration, false, true, "less than a", "s",
				"hour", "minute", "second", "millisecond");
	}

	/**
	 * <p>
	 * Gets you the duration with seconds and configurable millis.
	 * </p>
	 * 
	 * @param duration
	 * @param showMils
	 * @param lessThanExpression
	 * @param plural
	 * @param disp_hor
	 * @param disp_min
	 * @param disp_sec
	 * @param disp_mil
	 * @return
	 */
	public static String getDurationInHours(long duration, boolean showMils,
			String lessThanExpression, String plural, String disp_hor,
			String disp_min, String disp_sec, String disp_mil) {
		return getDurationInHours(duration, true, showMils, lessThanExpression,
				plural, disp_hor, disp_min, disp_sec, disp_mil);
	}

	/***************************************************************************
	 * <p>
	 * Interpret and format a long as a duration. Useful for a performance check
	 * with two System.currentTimeMillis().
	 * </p>
	 * 
	 * @param duration
	 *            The time to display the duration for in milliseconds.
	 * @param showSecs
	 *            Show the seconds or hide them.
	 * @param showMils
	 *            Show the milliseconds or hide them.
	 * @param disp_hor
	 *            The hour display string.
	 * @param disp_min
	 *            The minute display string.
	 * @param disp_sec
	 *            The second display string.
	 * @param disp_mil
	 *            The millisecond display string.
	 * @return String The duration in hours.
	 */
	public static String getDurationInHours(long duration, boolean showMils,
			boolean showSecs, String lessThanExpression, String plural,
			String disp_hor, String disp_min, String disp_sec, String disp_mil) {
		String result = "";
		DecimalFormat df = new DecimalFormat("0");

		long hors = (duration / 3600000); // get hours
		long rest = (duration % 3600000); // get rest

		long mins = (rest / 60000); // get minutes
		rest = (rest % 60000); // get rest

		long secs = (rest / 1000); // get seconds
		rest = (rest % 1000); // get rest

		long mils = rest;

		if (hors >= 1) {
			result += df.format(hors) + " " + disp_hor
					+ (hors > 1 ? plural + " " : " ");
		}
		if (mins >= 1) {
			result += df.format(mins) + " " + disp_min
					+ (mins > 1 ? plural + " " : " ");
		}
		if (secs >= 1 && showSecs) {
			result += df.format(secs) + " " + disp_sec
					+ (secs > 1 ? plural + " " : " ");
		}
		if (mils >= 1 && showMils) {
			result += df.format(mils) + " " + disp_mil
					+ (mils > 1 ? plural + " " : " ");
		}

		if (result.equals("")) {
			if (showMils) {
				result = lessThanExpression + " " + disp_mil;
			} else if (showSecs) {
				result = lessThanExpression + " " + disp_sec;
			} else {
				result = lessThanExpression + " " + disp_min;
			}
		}

		return result.trim();
	}

}
