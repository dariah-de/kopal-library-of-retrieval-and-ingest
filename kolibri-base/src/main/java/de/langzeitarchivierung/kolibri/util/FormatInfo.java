/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / FormatInfo.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 *******************************************************************************
 *
 *
 * CHANGELOG:
 *	2007-07-10	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

import java.io.Serializable;

/*******************************************************************************
 * <p>
 * A datatype for format specific information.
 * </p>
 * 
 * @author Matthias Neubauer, German National Library
 * @version 2007-01-25
 * @since 2005-08-25
 * @see de.langzeitarchivierung.kolibri.util.FormatRegistry
 ******************************************************************************/

public class FormatInfo implements Serializable {

	// CLASS VARIABLES (Static variables) ******************************

	private static final long	serialVersionUID			= 1L;

	// STATE (Instance variables) *****************************************

	private String				mimeType					= "";
	private String				fileType					= "";
	private String				fileTypeVersion				= "";
	private String				fileTypeCategory			= "";
	private String				fileTypeID					= "";
	private String				fileTypeStatus				= "";
	private String				fileTypeViewerApplication	= "";
	private String				fileExtension				= "";

	/***************************************************************************
	 * @see java.lang.Object#toString()
	 **************************************************************************/
	@Override
	public String toString() {
		String result = "";

		result = "fileTypeCategory: " + this.fileTypeCategory;
		result += "\nfileType: " + this.fileType;
		result += "\nfileTypeID: " + this.fileTypeID;
		result += "\nfileTypeStatus: " + this.fileTypeStatus;
		result += "\nfleTypeVersion: " + this.fileTypeViewerApplication;
		result += "\nfileTypeViewerApplication: "
				+ this.fileTypeViewerApplication;
		result += "\nmimeType: " + this.mimeType;

		return result;
	}

	// GET & SET METHODS **********************************************

	/***************************************************************************
	 * <p>
	 * Get the category of this format.
	 * </p>
	 * 
	 * @return The media category of this format
	 **************************************************************************/
	public String getFileTypeCategory() {
		return this.fileTypeCategory;
	}

	/***************************************************************************
	 * <p>
	 * Get the name of this format.
	 * </p>
	 * 
	 * @return The JHOVE name of this format
	 **************************************************************************/
	public String getFileType() {
		return this.fileType;
	}

	/***************************************************************************
	 * <p>
	 * Get the mimetype of this format.
	 * </p>
	 * 
	 * @return The mimetype of this format
	 **************************************************************************/
	public String getMimeType() {

		System.out.println("getMimeType()");

		return this.mimeType;
	}

	/***************************************************************************
	 * <p>
	 * Get the default viewer application of this format.
	 * </p>
	 * 
	 * @return The name of an application, able to view the file
	 **************************************************************************/
	public String getFileTypeViewerApplication() {
		return this.fileTypeViewerApplication;
	}

	/***************************************************************************
	 * <p>
	 * Get the version of this format.
	 * </p>
	 * 
	 * @return The version of the specific format
	 **************************************************************************/
	public String getFileTypeVersion() {
		return this.fileTypeVersion;
	}

	/***************************************************************************
	 * <p>
	 * Get the state of this format.
	 * </p>
	 * 
	 * @return The state of the specific format-version. Either "Active" or
	 *         "Obsolete"
	 **************************************************************************/
	public String getFileTypeStatus() {
		return this.fileTypeStatus;
	}

	/***************************************************************************
	 * <p>
	 * Get the unique ID of this format.
	 * </p>
	 * 
	 * @return The unique id of the specific format version
	 **************************************************************************/
	public String getFileTypeID() {
		return this.fileTypeID;
	}

	/***************************************************************************
	 * <p>
	 * Get the default file extension of this format.
	 * </p>
	 * 
	 * @return The standard file extension of this type
	 **************************************************************************/
	public String getFileExtension() {
		return this.fileExtension;
	}

	/***************************************************************************
	 * <p>
	 * Set the category of this format.
	 * </p>
	 * 
	 * @param fileTypeCategory
	 *            The category of this format
	 **************************************************************************/
	public void setFileTypeCategory(String fileTypeCategory) {
		this.fileTypeCategory = fileTypeCategory;
	}

	/***************************************************************************
	 * <p>
	 * Set the default viewer application of this format.
	 * </p>
	 * 
	 * @param fileTypeViewerApplication
	 *            The default viewer application
	 **************************************************************************/
	public void setFileTypeViewerApplication(String fileTypeViewerApplication) {
		this.fileTypeViewerApplication = fileTypeViewerApplication;
	}

	/***************************************************************************
	 * <p>
	 * Set the default file extension of this format.
	 * </p>
	 * 
	 * @param fileExtension
	 *            The default file extension
	 **************************************************************************/
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	/***************************************************************************
	 * <p>
	 * Set the name of this format.
	 * </p>
	 * 
	 * @param fileType
	 *            The formats name
	 **************************************************************************/
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/***************************************************************************
	 * <p>
	 * Set the unique ID of this format.
	 * </p>
	 * 
	 * @param fileTypeID
	 *            The unique ID for this format
	 **************************************************************************/
	public void setFileTypeID(String fileTypeID) {
		this.fileTypeID = fileTypeID;
	}

	/***************************************************************************
	 * <p>
	 * Set the mimetype of this format.
	 * </p>
	 * 
	 * @param mimeType
	 *            The mimetype of this format
	 **************************************************************************/
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	/***************************************************************************
	 * <p>
	 * Set the state of this format.
	 * </p>
	 * 
	 * @param fileTypeStatus
	 *            The State of this format, either "Active" or "Obsolete"
	 **************************************************************************/
	public void setFileTypeStatus(String fileTypeStatus) {
		this.fileTypeStatus = fileTypeStatus;
	}

	/***************************************************************************
	 * <p>
	 * Set the version of this format.
	 * </p>
	 * 
	 * @param fileTypeVersion
	 *            The version number
	 **************************************************************************/
	public void setFileTypeVersion(String fileTypeVersion) {
		this.fileTypeVersion = fileTypeVersion;
	}

}
