/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / HTMLUtils.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 *******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

/*******************************************************************************
 * Some useful routines such as read, parse, search,... to handle with HTML and
 * pseudo SGML/XML files.
 * 
 * @author Kadir Karaca Koçer, German National Library
 * @version 20061204
 * @since 20060225
 ******************************************************************************/

public class HTMLUtils
{

	// MANIPULATION (Manipulation - what the object does) **********************

	/***************************************************************************
	 * Main class for testing purposes.
	 * 
	 * @param args
	 **************************************************************************/
	public static void main(String args[]) {
		String blubb = "--&amp;--&Uuml;--&circ;--&auml;--&#x3C;--"
				+ "&#60;--&#161;--&#xA1;--&#164;--&#xA4;";
		System.out.println(blubb);

		blubb = convertTagsToUnicode(blubb);
		System.out.println(blubb);
	}

	/***************************************************************************
	 * Returns the value between two HTML/pseudo SGML/XML Tags. Use it if the
	 * text is not a valid XML and therefore can not be parsed with SAX/DOM/JDOM
	 * whatsever. (Note that only the first occurance is returned)
	 * 
	 * @author Kadir Karaca Koçer, German National Library
	 * @version 20051018
	 * @param text Text to parse
	 * @param tag Tag to look for
	 * @return The value of tag -- or ""
	 * @see java.lang.String
	 * @since 20051018
	 **************************************************************************/
	public static String getFirstTagValue(String text, String tag) {
		String value = "";
		String[] tmpStrArray1 = text.split("<" + tag + ">");
		if (tmpStrArray1.length > 1) {
			String[] tmpStrArray2 = tmpStrArray1[1].split("</" + tag + ">");
			value = tmpStrArray2[0];
		}
		return value;
	}

	/***************************************************************************
	 * Searches a string in HTML meta tags. (Note that only the first occurance
	 * is returned)
	 * 
	 * @author Kadir Karaca Koçer, German National Library
	 * @version 20050921
	 * @return The filtered string
	 * @param tokens Texts to search within
	 * @param key Texphrase to search for
	 * @since 20050906
	 **************************************************************************/
	public static String getFirstMetaTagContent(String[] tokens, String key) {
		String result = "";
		String tmpStr;
		for (int i = 0; i < tokens.length; i++) {
			tmpStr = tokens[i];
			String[] fields = tmpStr.split("\"");
			// Must be minimum 4 Fields AND the first must be the HTML tag "META
			// NAME=" AND the second one must match with the key.
			if ((fields.length > 3)
					&& (fields[0].trim().compareToIgnoreCase("META NAME=") == 0)
					&& (fields[1].trim().compareToIgnoreCase(key) == 0)) {
				// now we must search the attribute CONTENT=
				for (int j = 2; j < fields.length; j++) {
					System.out.println("\t \t processing field: "
							+ fields[j].trim());
					if (fields[j].trim().compareToIgnoreCase("CONTENT=") == 0) {

						// Found it. The next one is the result :-)
						result = fields[j + 1].trim();
						break;
					} // if
				} // for
			} // if
		} // for
		System.out.println("\tgetMetaTag found: " + result);
		return result;
	} // getMetaTag

	/***************************************************************************
	 * Method to convert characters in a string from HTML to UTF-8
	 * representation. Thanks to selfhtml.org.
	 * 
	 * TODO Do we need to convert the &amp; &gt; &lt; and &quot; entities??
	 * 
	 * @author Kadir Karaca Koçer, German National Library
	 * @version 20061213
	 * @return The filtered string
	 * @param text Text to filter
	 * @since 20050906
	 **************************************************************************/
	public static String convertTagsToUnicode(String text) {
		// Codes for ISO 8859-1, greek letters, mathematical symbols, technical
		// symbols, arrows, other symbols, enhanced latin, interpunction and
		// diacritic symbols.
		String htmlNames[] = { "nbsp", "iexcl", "cent", "pound", "curren",
				"yen", "brvbar", "sect", "uml", "copy", "ordf", "laquo", "not",
				"shy", "reg", "macr", "deg", "plusmn", "sup2", "sup3", "acute",
				"micro", "para", "middot", "cedil", "sup1", "ordm", "raquo",
				"frac14", "frac12", "frac34", "iquest", "Agrave", "Aacute",
				"Acirc", "Atilde", "Auml", "Aring", "AElig", "Ccedil",
				"Egrave", "Eacute", "Ecirc", "Euml", "Igrave", "Iacute",
				"Icirc", "Iuml", "ETH", "Ntilde", "Ograve", "Oacute", "Ocirc",
				"Otilde", "Ouml", "times", "Oslash", "Ugrave", "Uacute",
				"Ucirc", "Uuml", "Yacute", "THORN", "szlig", "agrave",
				"aacute", "acirc", "atilde", "auml", "aring", "aelig",
				"ccedil", "egrave", "eacute", "ecirc", "euml", "igrave",
				"iacute", "icirc", "iuml", "eth", "ntilde", "ograve", "oacute",
				"ocirc", "otilde", "ouml", "divide", "oslash", "ugrave",
				"uacute", "ucirc", "uuml", "yacute", "thorn", "yuml",

				"Alpha", "alpha", "Beta", "beta", "Gamma", "gamma", "Delta",
				"delta", "Epsilon", "epsilon", "Zeta", "zeta", "Eta", "eta",
				"Theta", "theta", "Iota", "iota", "Kappa", "kappa", "Lambda",
				"lambda", "Mu", "mu", "Nu", "nu", "Xi", "xi", "Omicron",
				"omicron", "Pi", "pi", "Rho", "rho", "Sigma", "sigmaf",
				"sigma", "Tau", "tau", "Upsilon", "upsilon", "Phi", "phi",
				"Chi", "chi", "Psi", "psi", "Omega", "omega", "thetasym",
				"upsih", "piv",

				"forall", "part", "exist", "empty", "nabla", "isin", "notin",
				"ni", "prod", "sum", "minus", "lowast", "radic", "prop",
				"infin", "ang", "and", "or", "cap", "cup", "int", "there4",
				"sim", "cong", "asymp", "ne", "equiv", "le", "ge", "sub",
				"sup", "nsub", "sube", "supe", "oplus", "otimes", "perp",
				"sdot", "loz",

				"lceil", "rceil", "lfloor", "rfloor", "lang", "rang",

				"larr", "uarr", "rarr", "darr", "harr", "crarr", "lArr",
				"uArr", "rArr", "dArr", "hArr",

				"bull", "prime", "Prime", "oline", "frasl", "weierp", "image",
				"real", "trade", "euro", "alefsym", "spades", "clubs",
				"hearts", "diams",

				"OElig", "oelig", "Scaron", "scaron", "Yuml", "fnof",

				"ensp", "emsp", "thinsp", "zwnj", "zwj", "lrm", "rlm", "ndash",
				"mdash", "lsquo", "rsquo", "sbquo", "ldquo", "rdquo", "bdquo",
				"dagger", "Dagger", "hellip", "permil", "lsaquo", "rsaquo",

				"circ", "tilde" };

		int unicodeNr[] = { 160, 161, 162, 163, 164, 165, 166, 167, 168, 169,
				170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181,
				182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193,
				194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205,
				206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217,
				218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229,
				230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241,
				242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253,
				254, 255,

				913, 945, 914, 946, 915, 947, 916, 948, 917, 949, 918, 950,
				919, 951, 920, 952, 921, 953, 922, 954, 923, 955, 924, 956,
				925, 957, 926, 958, 927, 959, 928, 960, 929, 961, 931, 962,
				963, 932, 964, 933, 965, 934, 966, 935, 967, 936, 968, 937,
				969, 977, 978, 982,

				8704, 8706, 8707, 8709, 8711, 8712, 8713, 8715, 8719, 8721,
				8722, 8727, 8730, 8733, 8734, 8736, 8743, 8744, 8745, 8746,
				8747, 8756, 8764, 8773, 8776, 8800, 8801, 8804, 8805, 8834,
				8835, 8836, 8838, 8839, 8853, 8855, 8869, 8901, 9674,

				8968, 8969, 8970, 8971, 9001, 9002,

				8592, 8593, 8594, 8595, 8596, 8629, 8656, 8657, 8658, 8659,
				8660,

				8226, 8242, 8243, 8254, 8260, 8472, 8465, 8476, 8482, 8364,
				8501, 9824, 9827, 9829, 9830,

				338, 339, 352, 353, 376, 402,

				8194, 8195, 8201, 8204, 8205, 8206, 8207, 8211, 8212, 8216,
				8217, 8218, 8220, 8221, 8222, 8224, 8225, 8230, 8240, 8249,
				8250,

				710, 732 };

		// Convert all entities to UTF-8 characters.
		for (int i = 0; i < htmlNames.length; i++) {
			Character utf8 = (char) unicodeNr[i];

			// Replace the HTML notation &text;
			text = text.replaceAll("&" + htmlNames[i] + ";", Character
					.toString(utf8.charValue()));

			// Replace the Unicode notations &#dec; and &#hex;
			text = text.replaceAll("&#" + unicodeNr[i] + ";", Character
					.toString(utf8.charValue()));
			text = text.replaceAll("&#x"
					+ Integer.toHexString(unicodeNr[i]).toUpperCase() + ";",
					Character.toString(utf8.charValue()));
		}

		return text;
	}

	/***************************************************************************
	 * The umlauts to change in the TIFF EXIF metadata headers.
	 * 
	 * TODO Anyway, are we allowed to transform any chars from the TIFF header
	 * information? Maybe not, maybe take it out here and check TIFF header
	 * umlauts in an extra action module if wanted? Or in the TIFFIMP?
	 * 
	 * TODO Check these charset problems we have with the JHOVE. How can we get
	 * UTF8?
	 * 
	 * @param xmlData The xmlData String with its ISO-8859-1 encoded umlauts in
	 *            a very queer format.
	 * 
	 * @return The xmlData string with valid UTF-8 umlauts.
	 **************************************************************************/
	public static String correctUmlautsInMixSections(String xmlData) {
		// The ISO-8859-1 character encodings:
		char iso_char_ae = 0xFFE4; // Hex: FFE4 - Dec: 65508
		char iso_char_oe = 0xFFF6; // Hex: FFF6 - Dec: 65526
		char iso_char_ue = 0xFFFC; // Hex: FFFC - Dec: 65532
		char iso_char_Ae = 0xFFC4; // Hex: FFC4 - Dec: 65476
		char iso_char_Oe = 0xFFD6; // Hex: FFD6 - Dec: 65494
		char iso_char_Ue = 0xFFDC; // Hex: FFDC - Dec: 65500
		char iso_char_sz = 0xFFDF; // Hex: FFDF - Dec: 65503

		// The "y.
		char iso_char_y = 0xFFFF; // Hex: FFFF - Dec: 65535

		// The UTF-8 character encodings:
		char utf8_char_ae = '\u00E4'; // Hex: E4 - Dec: 228
		char utf8_char_oe = '\u00F6'; // Hex: F6 - Dec: 246
		char utf8_char_ue = '\u00FC'; // Hex: FC - Dec: 252
		char utf8_char_Ae = '\u00C4'; // Hex: C4 - Dec: 196
		char utf8_char_Oe = '\u00D6'; // Hex: D6 - Dec: 214
		char utf8_char_Ue = '\u00DC'; // Hex: DC - Dec: 220
		char utf8_char_sz = '\u00DF'; // Hex: DF - Dec: 223

		// The "y.
		char utf8_char_y = '\u00FF'; // Hex: FF - Dec: 255

		// Replace'em all!
		xmlData = xmlData.replace(iso_char_ae, utf8_char_ae);
		xmlData = xmlData.replace(iso_char_oe, utf8_char_oe);
		xmlData = xmlData.replace(iso_char_ue, utf8_char_ue);
		xmlData = xmlData.replace(iso_char_Ae, utf8_char_Ae);
		xmlData = xmlData.replace(iso_char_Oe, utf8_char_Oe);
		xmlData = xmlData.replace(iso_char_Ue, utf8_char_Ue);
		xmlData = xmlData.replace(iso_char_sz, utf8_char_sz);
		xmlData = xmlData.replace(iso_char_y, utf8_char_y);

		return xmlData;
	}

	/***************************************************************************
	 * Converts arabic numbers to roman counterparts. Good for page numbers.
	 * 
	 * TODO What if someone needs a roman number greater than 30?
	 * 
	 * @author Karaca Koçer
	 * @param number String representation of the number to convert
	 * @param uppercase Upper- or lowercase (xxiv or XXIV)
	 * @return String representation of the converted number
	 **************************************************************************/
	public static String getRomanNumeral(String number, boolean uppercase) {
		String[] arabic = { "01", "02", "03", "04", "05", "06", "07", "08",
				"09", "10", "11", "12", "13", "14", "15", "16", "17", "18",
				"19", "20", "21", "22", "23", "24", "25", "26", "27", "28",
				"29", "30" };

		String[] roman = { "i", "ii", "iii", "iv", "v", "vi", "vii", "viii",
				"ix", "x", "xi", "xii", "xiii", "xiv", "xv", "xvi", "xvii",
				"xviii", "xix", "xx", "xxi", "xxii", "xxiii", "xxiv", "xxv",
				"xxvi", "xxvii", "xxviii", "xxix", "xxx" };

		String result = "";

		for (int i = 0; i < arabic.length; i++) {
			if (number.equals(arabic[i])) {
				if (uppercase) {
					result = roman[i].toUpperCase();
				}
				else {
					result = roman[i];
				}
				return result;
			}
		}
		return result;
	}

}
