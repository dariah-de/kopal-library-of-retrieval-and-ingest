/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / Configurator.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 20.09.2007	Funk	Just added some comments.
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*******************************************************************************
 * Manages the configuration of objects. At first it should be initialized with
 * setConfigDocument().
 ******************************************************************************/

public class Configurator {
	/***************************************************************************
	 * The part of the class package name which can be omitted for saving
	 * characters.
	 **************************************************************************/
	private static final String									PACKAGES					= "de.langzeitarchivierung.kolibri.";

	/***************************************************************************
	 * The tags for global, module or policy specific configuration values.
	 **************************************************************************/
	public static final String									COMMON_CONFIG_TAG			= "common";

	/***************************************************************************
	 * The tags for global, module or policy specific configuration values.
	 **************************************************************************/
	public static final String									MODULE_CONFIG_TAG			= "class";

	/***************************************************************************
	 * The tags for global, module or policy specific configuration values.
	 **************************************************************************/
	public static final String									POLICY_CONFIG_TAG			= "config";

	/***************************************************************************
	 * Attribute to specify the class name of module specific configuration
	 * values.
	 **************************************************************************/
	public static final String									CONFIG_CLASS_NAME_ATTRIBUTE	= "name";

	/***************************************************************************
	 * Tags for setting a config value.
	 **************************************************************************/
	public static final String									PROPERTY_TAG				= "property";

	/***************************************************************************
	 * Tags for setting a config value.
	 **************************************************************************/
	public static final String									FIELD_TAG					= "field";

	/***************************************************************************
	 * Tags for setting a config value.
	 **************************************************************************/
	public static final String									VALUE_TAG					= "value";

	/***************************************************************************
	 * Logging...
	 **************************************************************************/
	private static Logger										defaultLogger				= Logger
			.getLogger("de.langzeitarchivierung.kolibri");

	/***************************************************************************
	 * maps property (String) -> value (Object)
	 **************************************************************************/
	private static Hashtable<String, Object>					globalConfig;

	/***************************************************************************
	 * maps module class name (String) -> hashtable (maps property (String) ->
	 * value (Object))
	 * 
	 * The config values are always strings because we retrieve them from the
	 * config file.
	 **************************************************************************/
	private static Hashtable<String, Hashtable<String, Object>>	moduleConfig;

	/***************************************************************************
	 * this custom class extends ArrayList to distinguish between an ArrayList
	 * as config value and as an container for multiple config values
	 **************************************************************************/
	private static class MultipleConfigValues extends ArrayList<String> {
		static final long serialVersionUID = 1;
	}

	/***************************************************************************
	 * Initializes the Configurator class with the values of an xml config file.
	 * 
	 * @param configuration
	 *            the xml document of the configuration file
	 **************************************************************************/
	public static void setConfigDocument(Document configuration) {
		// initialize global config table
		NodeList commonList = configuration
				.getElementsByTagName(COMMON_CONFIG_TAG);
		// only one common element is allowed, so we can take index 0
		globalConfig = iniTable((Element) commonList.item(0));

		// initialize modul config table
		moduleConfig = new Hashtable<String, Hashtable<String, Object>>();
		NodeList moduleList = configuration
				.getElementsByTagName(MODULE_CONFIG_TAG);
		for (int i = 0; i < moduleList.getLength(); i++) {
			Element moduleElement = (Element) moduleList.item(i);
			// we want to ignore the case of class names so we make it
			// always lower case
			moduleConfig.put(moduleElement
					.getAttribute(CONFIG_CLASS_NAME_ATTRIBUTE).toLowerCase(),
					iniTable(moduleElement));
		}
	}

	/***************************************************************************
	 * Creates and initializes a table with configuration values.
	 * 
	 * @param parent
	 *            parent the parent element of the config properties
	 * @return the hashtable with the configuration values
	 **************************************************************************/
	private static Hashtable<String, Object> iniTable(Element parent) {
		Hashtable<String, Object> result = new Hashtable<String, Object>();

		NodeList properties = parent.getElementsByTagName(PROPERTY_TAG);
		for (int j = 0; j < properties.getLength(); j++) {
			String field = null;
			MultipleConfigValues values = new MultipleConfigValues();
			NodeList fieldsAndValues = properties.item(j).getChildNodes();

			for (int k = 0; k < fieldsAndValues.getLength(); k++) {
				if (fieldsAndValues.item(k).getNodeName().equals(FIELD_TAG)) {
					field = fieldsAndValues.item(k).getFirstChild()
							.getNodeValue();
				}
				if (fieldsAndValues.item(k).getNodeName().equals(VALUE_TAG)) {
					Node valueNode = fieldsAndValues.item(k).getFirstChild();
					String valueValue = "";
					// If the value of the value tag is empty, take an empty
					// string as value.
					if (valueNode != null) {
						valueValue = valueNode.getNodeValue().trim();
					}
					values.add(valueValue);
				}
			}
			if (field != null && !values.isEmpty()) {
				// we want to ignore the case of names so we make it
				// always lower case
				field = field.toLowerCase();

				// if we have only one config value we only put the first one in
				if (values.size() == 1) {
					result.put(field, values.get(0));
				} else {
					result.put(field, values);
				}
			}

		}
		return result;
	}

	/***************************************************************************
	 * Returns the config value of a module.
	 * 
	 * @param module
	 *            the class for which the config value is requested
	 * @param field
	 *            the name of the requested config parameter, not case sensitive
	 * @return the config value or null if not present.
	 **************************************************************************/
	public static Object getValue(Class module, String field) {
		if (moduleConfig.containsKey(module.getName().toLowerCase())) {
			return ((Hashtable<String, Object>) moduleConfig
					.get(module.getName().toLowerCase()))
							.get(field.toLowerCase());
			// or for its short form
		} else if (module.getName().toLowerCase().startsWith(PACKAGES)
				&& moduleConfig.containsKey(module.getName().toLowerCase()
						.substring(PACKAGES.length()))) {
			return ((Hashtable<String, Object>) moduleConfig.get(module
					.getName().toLowerCase().substring(PACKAGES.length())))
							.get(field.toLowerCase());
		}
		return null;

	}

	/***************************************************************************
	 * Returns a global config value.
	 * 
	 * @param field
	 *            the name of the requested config parameter, not case sensitive
	 * @return the config value or null if not present.
	 **************************************************************************/
	public static Object getValue(String field) {
		return globalConfig.get(field.toLowerCase());
	}

	/***************************************************************************
	 * Configures an object or class. All setters are set with their
	 * corresponding config parameters. Setters are set multiple times if
	 * multiple values for this property exist in the config file or policy.
	 * 
	 * Priority: 1. Configure parameters with specific values for the object and
	 * its superclasses 2. configure remaining parameters with global values
	 * 
	 * @param objectToConfigure
	 *            The object or class which should be configured.
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws NoSuchMethodException
	 **************************************************************************/
	public static void configure(Object objectToConfigure)
			throws InvocationTargetException, IllegalAccessException {
		configure(objectToConfigure, null, null);
	}

	/***************************************************************************
	 * Configures an object or class with the config file values and the getters
	 * of the source objects. All setters are set with their corresponding
	 * config parameters. Setters are set multiple times if multiple values for
	 * this property exist in the config file or policy.
	 * 
	 * Priority: 1. Configure parameters with specific values for the object and
	 * its superclasses 2. configure remaining parameters with global values 3.
	 * configure remaining parameters with values of source object
	 * 
	 * @param objectToConfigure
	 *            The object or class which should be configured.
	 * @param sourceObject
	 *            The source object those values should be used
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws NoSuchMethodException
	 **************************************************************************/
	public static void configure(Object objectToConfigure, Object sourceObject)
			throws InvocationTargetException, IllegalAccessException {
		configure(objectToConfigure, null, sourceObject);
	}

	/***************************************************************************
	 * Configures an object with the config file values, the getters of the
	 * source object and the policy specific values. All setters are set with
	 * their corresponding config parameters. Setters are set multiple times if
	 * multiple values for this property exist in the config file or policy.
	 * 
	 * Priority: 1. Configure with policy specific values. 2. Configure
	 * remaining parameters with specific values for the object and its
	 * superclasses 3. configure remaining parameters with global values 4.
	 * configure remaining parameters with values of source object
	 * 
	 * @param objectToConfigure
	 *            The object which should be configured.
	 * @param policyStep
	 *            The policy node with additional config values.
	 * @param sourceObject
	 *            The source object those values should be used.
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 * @throws NoSuchMethodException
	 **************************************************************************/
	public static void configure(Object objectToConfigure, Node policyStep,
			Object sourceObject)
			throws InvocationTargetException, IllegalAccessException {

		// We need to distinguish between the configuration of a static class
		// and a normal bean because otherwise the superclass configuration
		// fails
		Class classToConfigure = null;
		if (objectToConfigure instanceof Class) {
			classToConfigure = (Class) objectToConfigure;
		} else {
			classToConfigure = objectToConfigure.getClass();
		}
		String objectClassName = classToConfigure.getName();

		Method methodsToCheck[] = classToConfigure.getMethods();

		List<Method> notConfiguredMethods = new ArrayList<Method>();

		// get methods to configure by checking all methods for setters
		for (int i = 0; i < methodsToCheck.length; i++) {
			if (methodsToCheck[i].getName().startsWith("set")) {
				// System.out.println(methodsToCheck[i].getName().substring(3));
				notConfiguredMethods.add(methodsToCheck[i]);
			}
		}

		// configure bean with policy specific values
		if (policyStep != null) {
			configureMethods(objectToConfigure, notConfiguredMethods,
					getPropertyTable(policyStep));
		}

		// configure with module specific values for itself and its superclasses
		for (; classToConfigure != null; classToConfigure = classToConfigure
				.getSuperclass()) {
			// System.out.println(classToConfigure.getName());
			// configure if we have config values for this class
			if (moduleConfig
					.containsKey(classToConfigure.getName().toLowerCase())) {
				configureMethods(objectToConfigure, notConfiguredMethods,
						moduleConfig
								.get(classToConfigure.getName().toLowerCase()));
				// or for its short form
			} else if (classToConfigure.getName().toLowerCase()
					.startsWith(PACKAGES)
					&& moduleConfig.containsKey(classToConfigure.getName()
							.toLowerCase().substring(PACKAGES.length()))) {
				configureMethods(objectToConfigure, notConfiguredMethods,
						moduleConfig.get(classToConfigure.getName()
								.toLowerCase().substring(PACKAGES.length())));
			}
		}

		// configure with global values
		configureMethods(objectToConfigure, notConfiguredMethods, globalConfig);

		// configure destination set methods with the values of the
		// source get methods
		if (sourceObject != null) {
			configureMethods(objectToConfigure, notConfiguredMethods,
					getPropertyTable(sourceObject));
		}

		// if there are unconfigured methods and it is not the main package
		// TODO explain why we shouldn't log it if it is in the main package
		if (!notConfiguredMethods.isEmpty()
				&& objectClassName.split("\\.").length > 4) {
			String missingMethods = "";
			for (Iterator<Method> iter = notConfiguredMethods.iterator(); iter
					.hasNext();) {
				missingMethods += iter.next().getName() + " ";
			}
			defaultLogger.log(Level.FINE,
					"Not everything in class " + objectClassName
							+ " has been configured. Missing are: "
							+ missingMethods);
		}

	}

	/***************************************************************************
	 * Configures Methods on an object or class with the specified config data.
	 * 
	 * @param objectToConfigure
	 *            The object which should be configured.
	 * @param notConfiguredMethods
	 *            The methods which should be configured.
	 * @param configData
	 *            The table with the config data.
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 **************************************************************************/
	private static void configureMethods(Object objectToConfigure,
			List<Method> notConfiguredMethods,
			Hashtable<String, Object> configData)
			throws InvocationTargetException, IllegalAccessException {
		List<Method> configuredMethods = new ArrayList<Method>();

		String objectClassName = null;
		if (objectToConfigure instanceof Class) {
			objectClassName = ((Class) objectToConfigure).getName();
		} else {
			objectClassName = objectToConfigure.getClass().getName();
		}

		for (Iterator<Method> methodIter = notConfiguredMethods
				.iterator(); methodIter.hasNext();) {
			Method methodToInvoke = methodIter.next();
			String propertyName = methodToInvoke.getName().substring(3);
			if (configData.containsKey(propertyName.toLowerCase())) {

				// Secure, that passwords are NOT logged here...
				String propertyValue = configData
						.get(propertyName.toLowerCase()).toString();

				if (propertyName.toLowerCase().contains("pass")) {
					propertyValue = "***";
				}

				defaultLogger.log(Level.FINEST,
						"Setting " + objectClassName + "'s property "
								+ propertyName + " to '" + propertyValue + "'");
				Object parameter = configData.get(propertyName.toLowerCase());

				if (parameter instanceof MultipleConfigValues) {
					MultipleConfigValues confValues = (MultipleConfigValues) parameter;
					for (Iterator<String> parameterIter = confValues
							.iterator(); parameterIter.hasNext();) {
						configureMethod(objectToConfigure, methodToInvoke,
								parameterIter.next());
					}
				} else {
					configureMethod(objectToConfigure, methodToInvoke,
							parameter);
				}

				configuredMethods.add(methodToInvoke);
			}
		}

		notConfiguredMethods.removeAll(configuredMethods);
	}

	/***************************************************************************
	 * Configures a single method on an object or class with the specified
	 * parameter
	 * 
	 * @param objectToConfigure
	 *            The object which should be configured.
	 * @param methodToInvoke
	 *            The method to call.
	 * @param parameter
	 *            The parameter which should be set
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 **************************************************************************/
	private static void configureMethod(Object objectToConfigure,
			Method methodToInvoke, Object parameter)
			throws IllegalAccessException, InvocationTargetException {
		Class parameterType = methodToInvoke.getParameterTypes()[0];
		// if we have to cast the config data to the parametertype
		if (parameter.getClass() != parameterType) {
			if (parameterType == Boolean.TYPE) {
				parameter = Boolean.valueOf((String) parameter);
			} else if (parameterType == Integer.TYPE) {
				parameter = Integer.valueOf((String) parameter);
			} else if (parameterType == Long.TYPE) {
				parameter = Long.valueOf((String) parameter);
			} else if (parameterType == Double.TYPE) {
				parameter = Double.valueOf((String) parameter);
			} else if (parameterType == Float.TYPE) {
				parameter = Float.valueOf((String) parameter);
			} else if (parameterType == Character.TYPE) {
				parameter = Character.valueOf(((String) parameter).charAt(0));
			}
		}
		methodToInvoke.invoke(objectToConfigure, parameter);
	}

	/***************************************************************************
	 * Tries to create and return a hashtable with configuration values of the
	 * policy step.
	 * 
	 * @param policyStep
	 *            The policy node with config parameters
	 * @return the hashtable filled with config values
	 **************************************************************************/
	private static Hashtable<String, Object> getPropertyTable(Node policyStep) {
		Hashtable<String, Object> result = new Hashtable<String, Object>();

		// get the first config sub node and uses its config properties
		NodeList subNodes = policyStep.getChildNodes();
		for (int i = 0; i < subNodes.getLength(); i++) {
			// System.out.println(subNodes.item(i).getNodeName());
			if (subNodes.item(i).getNodeName().equals(POLICY_CONFIG_TAG)) {
				result = iniTable((Element) subNodes.item(i));
				break;
			}
		}

		// System.out.println("policy config table: " + result);
		return result;
	}

	/***************************************************************************
	 * Tries to create and return a hashtable with configuration values of a
	 * source object. Each get method is interpreted as a config parameter with
	 * the name sourceObject.getNAME() and the value is the returned object.
	 * 
	 * @param sourceObject
	 *            the object from which to create a configuration table
	 * @return the hashtable with the config parameters
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 **************************************************************************/
	private static Hashtable<String, Object> getPropertyTable(
			Object sourceObject)
			throws IllegalAccessException, InvocationTargetException {
		Hashtable<String, Object> result = new Hashtable<String, Object>();

		Method beanMethods[] = sourceObject.getClass().getMethods();
		for (int i = 0; i < beanMethods.length; i++) {
			// we need to check that the getMethod takes no parameters
			// otherwise getProperty() will fail
			if (beanMethods[i].getName().startsWith("get")
					&& beanMethods[i].getParameterTypes().length == 0) {
				String propertyName = beanMethods[i].getName().substring(3);

				// DEBUG OUTPUT
				// propertyName = propertyName.substring(0, 1).toLowerCase()
				// + propertyName.substring(1);
				// System.out
				// .println("sourceObject: "
				// + sourceObject.getClass().getName()
				// + " Property: " + propertyName + " Method: "
				// + beanMethods[i].getName());
				// DEBUG OUTPUT

				Object parameters[] = {};
				Object returnValue = beanMethods[i].invoke(sourceObject,
						parameters);
				// we can only put values not equal null into the hashtable
				if (returnValue != null) {
					result.put(propertyName.toLowerCase(), returnValue);
				}
			}
		}

		// and additionally add the sourceObject itself
		String classNameParts[] = sourceObject.getClass().getName()
				.split("\\.");
		result.put(classNameParts[classNameParts.length - 1].toLowerCase(),
				sourceObject);

		return result;
	}

}
