/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / KolibriSeparateLogFormatter.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * 
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

import java.util.logging.LogRecord;

/*******************************************************************************
 * The KolibriConsoleLogFormatter formats the kopal console log.
 * 
 * @author Funk
 * @version 20061204
 * @since 20060125
 ******************************************************************************/

public class KolibriSeparateLogFormatter extends java.util.logging.Formatter
{

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * Default constructor
	 **************************************************************************/
	public KolibriSeparateLogFormatter() {
		super();
	}

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * @see java.util.logging.Formatter#format(java.util.logging.LogRecord)
	 **************************************************************************/
	@Override
	public String format(LogRecord record) {
		return KolibriTimestamp.getTimestamp(record.getMillis()) + "\t"
				+ "Status:" + record.getLevel() + "\t" + record.getMessage()
				+ "\n";
	}

}
