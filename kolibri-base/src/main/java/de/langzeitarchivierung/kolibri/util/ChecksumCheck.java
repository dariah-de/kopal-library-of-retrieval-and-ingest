package de.langzeitarchivierung.kolibri.util;

/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / ChecksumCheck.java
 *  
 * Copyright 2009-2011 by Project DP4lib 
 * 
 * http://dp4lib.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * ******************************************************************************
 *
 *
 * CHANGELOG:
 * 11.06.2010	koLibRI version 1.2
 ******************************************************************************/

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;


/*******************************************************************************
 * @author Stefan Hein, German National Library
 * @version 20101029
 * @since 20100611
********************************************************************************/ 
public class ChecksumCheck {

	// STATE (Instance variables) *****************************************
	
	HashMap<Object, Object> result;
	String calcChs;
	
	// CREATION (Constructors, factory methods, static/inst init) **********
	
	/***************************************************************************
	 * Default Constructor
	 **************************************************************************/
	public ChecksumCheck() {
		
		result = new HashMap<Object, Object>();
	}
	
	/***************************************************************************
	 * calculates a MD5 checksum for a given FileInputStream and compare this
	 * with the given checksum
	 * 
	 * @param file - the given file
	 * @param refChs - reference checksum
	 * 
	 * @throws Exception
	 **************************************************************************/
	public HashMap<Object, Object> checkMD5(FileInputStream file, String refChs) 
		throws IOException, NoSuchAlgorithmException {
		
		DataInputStream in = new DataInputStream(file);
    	
		calcChs = ChecksumGenerator.getMD5(in);
    	
    	result.put("result", refChs.equals(calcChs));
    	result.put("calcChs", calcChs);
    	
    	in.close();
    	return result;
		
	}
	
	/***************************************************************************
	 * calculates a SHA1 checksum for a given FileInputStream and compare this
	 * with the given checksum
	 * 
	 * @param file - the given file
	 * @param refChs - reference checksum
	 * 
	 * @throws Exception
	 **************************************************************************/
	public HashMap<Object, Object> checkSHA1(FileInputStream file, String refChs) 
		throws IOException, NoSuchAlgorithmException {
			
			DataInputStream in = new DataInputStream(file);
	    	calcChs = ChecksumGenerator.getSHA1(in);
	    	
	    	result.put("result", refChs.equals(calcChs));
	    	result.put("calcChs", calcChs);
	    	
	    	in.close();
	    	return result;
	}
	
}
