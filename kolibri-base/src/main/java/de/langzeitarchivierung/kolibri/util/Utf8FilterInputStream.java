/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionmodule.sub / Utf8FilterInputStream.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 *
 *
 *******************************************************************************
 *
 * 
 * UTF-8 checker and conditioner
 * Copyright (C) 2001-2005 Simeon Warner - simeon@cs.cornell.edu
 *
 * utf8conditioner is supplied under the GNU Public License and comes 
 * with ABSOLUTELY NO WARRANTY; see COPYING for more details.
 *
 * Designed for use with Open Archives Initiative (OAI, 
 * see: http://www.openarchives.org/) harvesting software. Aims to 
 * `fix' bad codes in UTF-8 encoded XML so that XML parsers will
 * be able to parse it (albeit with some corruption introduced by
 * substitution of dummy characters in place of illegal codes).
 *
 * I assume that the most likely cause of errors is inclusion of non-UTF-8
 * bytes from various 8-bit character sets. Thus, any attempt to read a
 * sequence of continuation bytes that finds an invalid byte will result
 * in the termination of the attempt to read the multi-byte character. Each
 * valid single byte will be written out, invalid single bytes will be 
 * replaced with a dummy character. The hope is that this will avoid an 
 * error having run-on effects which might interfere with XML markup.
 * (Option -m changes this behaviour to replace an invalid multi-byte
 * with a single dummy character.)
 *
 * Bugs:
 * - no protection against overflow of byte, character and line counters
 *   (unsigned long int). Will result in incorrect output messages.
 *
 * [CVS: $Id: Utf8FilterInputStream.java,v 1.10 2007/07/11 10:19:07 funk Exp $]
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

import java.io.BufferedInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

/*******************************************************************************
 * This filterInputStream takes a given inputStream and reads every byte of it
 * and returns either a valid unicode character or an array of int (actually:
 * bytes) as a representation of it.
 * 
 * Ported from the original C code, see license and original comments from
 * Simeon Warner in the above comment block. The getopt software was taken out
 * because we don't need it for koLibRI.
 * 
 * Invalid UTF-8 characters (as invalid codes, faulty XML entities, ...) are
 * substituted by a substitute character (default is '?').
 * 
 * If there is a fatal error (as e.g. EOF coming too soon) an I/O exception is
 * thrown.
 * 
 * Occurring errors are put in an error list, so this filter input stream can
 * also be used as an UTF-8 validity check only.
 * 
 * TODO Still to check for better-to-port-to-JAVA-things (e.g. use JAVA methods
 * for character checking)
 * 
 * TODO Still to test and compare with the original UTF8 conditioner, which I
 * had no time to compile with my windows system.
 * 
 * @author Funk
 * @version 2007-02-08
 * @since 2006-08-21
 ******************************************************************************/

public class Utf8FilterInputStream extends FilterInputStream {

	// CONSTANTS ***************************************************************

	private final int			MAX_BAD_CHAR			= 100;
	private final int			MAX_BYTES				= 100;

	// STATE (Instance variables) **********************************************

	private byte				substituteCharacter		= '?';
	private ArrayList<String>	errorList				= new ArrayList<String>();
	private InputStream			is;

	private long				bytenum					= 0;
	private long				charnum					= 0;
	private long				linenum					= 1;

	private boolean				checkXml1_0Chars		= true;
	private boolean				checkXml1_1Chars		= false;
	private boolean				checkXml1_1Restricted	= false;
	private boolean				checkOverlong			= true;

	private int					_byte[]					= new int[this.MAX_BYTES];
	private int					unicode					= 0;

	// CREATION (Constructors, factory methods, static/inst init) **************

	/***************************************************************************
	 * Constructor.
	 * 
	 * @param _in
	 **************************************************************************/
	public Utf8FilterInputStream(BufferedInputStream _in) {
		super(_in);
		this.is = _in;
	}

	// MANIPULATION (Manipulation - what the object does) **********************

	/***************************************************************************
	 * mark() is not needed in this class. There will be no need to go back!
	 * 
	 * @see java.io.InputStream#markSupported()
	 **************************************************************************/
	@Override
	public boolean markSupported() {
		return false;
	}

	/***************************************************************************
	 * Checks the given stream for unicode conformance. To get all chars please
	 * use like this:
	 * 
	 * <code>
	 * String input_filename = "JEPP";
	 * int someChar;
	 * 
	 * Utf8FilterInputStream is = new Utf8FilterInputStream(
	 * 		new BufferedInputStream(new FileInputStream(input_filename)));
	 * 	 
	 * while ((someChar = is.readNextUtf8Char()) != -1) {
	 * 		// Do something with someChar, e.g. assembling a valid UTF-8 string.
	 * }
	 * </code>
	 * 
	 * Please note: Writing the unicode characters from above back to a file
	 * will fail if saved as integer values, because only the first byte of the
	 * int is written!!
	 * 
	 * @return The next valid unicode character or the substitution character as
	 *         an int. Useful for building up a string of valid unicode chars.
	 * @throws IOException
	 **************************************************************************/
	public int readNextUtf8Char() throws IOException {
		if (readNext()) {
			return this.unicode;
		}

		return -1;
	}

	/***************************************************************************
	 * Checks the given stream for unicode conformance. To get all bytes and
	 * write it back to a file please use like this:
	 * 
	 * <code>
	 * String input_filename = "JEPP.xml";
	 * String output_filename = "JUPP.xml";
	 * int outputArray[];
	 * 
	 * Utf8FilterInputStream is = new Utf8FilterInputStream(
	 * 		new BufferedInputStream(new FileInputStream(input_filename)));
	 * OutputStream os = new FileOutputStream(output_filename);
	 * 
	 * for (int i = 0; i < outputArray.length; i++) {
	 * 		os.write(outputArray[i]);
	 * }
	 * </code>
	 * 
	 * Please note: You get back an array of int, and write each int value as a
	 * single byte to the output stream.
	 * 
	 * @return The next valid unicode character or the substitution character as
	 *         a list of int (hm... some sort of bytes, I think :). Useful for
	 *         writing back the int values (as bytes) to an output stream.
	 * @throws IOException
	 **************************************************************************/
	public int[] readNextByteArray() throws IOException {
		int arrayLen;

		if (readNext()) {
			// Test how many bytes are used in the global _byte.
			for (arrayLen = 0; arrayLen < this.MAX_BYTES; arrayLen++) {
				if (this._byte[arrayLen] == 0) {
					break;
				}
			}

			// Initialize the byte array to return with exactly this length,
			// fill and then return it.
			int byteArray[] = new int[arrayLen];

			for (int i = 0; i < arrayLen; i++) {
				byteArray[i] = this._byte[i];
			}

			return byteArray;
		}

		return null;
	}

	/***************************************************************************
	 * Reads one or more bytes from the input stream and checks the byte(s) read
	 * for UTF-8 conformance. The unicode character read from those bytes is
	 * stored as an int in "unicode" and as a list of bytes in "byteList".
	 * 
	 * @return True if more bytes are following, false if the stream is at it's
	 *         end.
	 * @throws IOException
	 **************************************************************************/
	private boolean readNext() throws IOException {
		int charRead = 0;
		int j, k;
		int contBytes;
		int _unicode;
		char byteStr[] = new char[this.MAX_BYTES + 1];
		String errorString = "";

		// Check entities if any XML checks are on.
		boolean checkEntities = (this.checkXml1_0Chars || this.checkXml1_1Chars);

		// Variables for bad characters option.
		int badChar = 0;
		int badChars[] = new int[this.MAX_BAD_CHAR];
		int highestCharInNBytes[] = new int[6];

		// Terminator.
		badChars[badChar] = 0;
		highestCharInNBytes[0] = 0x7F;
		highestCharInNBytes[1] = 0x7FF;
		highestCharInNBytes[2] = 0xFFFF;
		highestCharInNBytes[3] = 0x1FFFFF;
		highestCharInNBytes[4] = 0x3FFFFFF;
		highestCharInNBytes[5] = 0x7FFFFFFF;

		// Clear the byte array.
		for (int i = 0; i < this.MAX_BYTES; i++) {
			this._byte[i] = 0;
		}

		// Go through input code (character) by code and check for correct
		// use of UTF-8 continuation bytes, check for unicode character
		// validity.
		if ((charRead = this.is.read()) != -1) {
			this.bytenum++;
			this.charnum++;
			if (charRead == '\n') {
				this.linenum++;
			}

			_unicode = charRead;
			if ((charRead & 0x80) == 0) {
				// ORIGINAL COMMENT: One byte char 0000 0000-0000 007F 0xxxxxxx
				contBytes = 0;
			} else if ((charRead & 0xE0) == 0xC0) {
				// ORIGINAL COMMENT: 0000 0080-0000 07FF 110xxxxx 10xxxxxx
				contBytes = 1;
				_unicode = (charRead & 0x1F);
			} else if ((charRead & 0xF0) == 0xE0) {
				// ORIGINAL COMMENT: 0000 0800-0000 FFFF 1110xxxx 10xxxxxx
				// 10xxxxxx
				contBytes = 2;
				_unicode = (charRead & 0x0F);
			} else if ((charRead & 0xF8) == 0xF0) {
				// ORIGINAL COMMENT: 0001 0000-001F FFFF 11110xxx 10xxxxxx
				// 10xxxxxx 10xxxxxx
				contBytes = 3;
				_unicode = (charRead & 0x07);
			} else if ((charRead & 0xFC) == 0xF8) {
				// ORIGINAL COMMENT: 0020 0000-03FF FFFF 111110xx 10xxxxxx
				// 10xxxxxx 10xxxxxx 10xxxxxx
				contBytes = 4;
				_unicode = (charRead & 0x03);
			} else if ((charRead & 0xFE) == 0xFC) {
				// ORIGINAL COMMENT: 0400 0000-7FFF FFFF 1111110x 10xxxxxx ...
				// 10xxxxxx
				contBytes = 5;
				_unicode = (charRead & 0x01);
			} else {
				errorString += "Illegal byte: 0x"
						+ Integer.toHexString(charRead) + "\n";
				contBytes = 0;
			}
			this._byte[0] = charRead;

			// Check all continuing bytes.
			for (j = 1; j <= contBytes; j++) {
				this.is.mark(1);
				if ((charRead = this.is.read()) != -1) {
					this.bytenum++;
					this._byte[j] = charRead;

					if ((charRead & 0xC0) != 0x80) {
						// ORIGINAL COMMENT: Doesn't match 10xxxxxx
						String message = "Byte " + (j + 1)
								+ " isn't continuation:";
						for (k = 0; k <= j; k++) {
							message += " 0x"
									+ Integer.toHexString(this._byte[k]);
						}
						errorString += message + ", restart at 0x"
								+ Integer.toHexString(charRead);
						this.is.reset();
						this.bytenum--;
						this._byte[j] = 0;
						break;
					}
					_unicode = (_unicode << 6) + (charRead & 0x3F);
				} else {
					errorString += "Premature EOF at byte " + this.bytenum
							+ ", should be byte " + j + " of code";
					// IO error here??
					break;
				}

			}

			// ORIGINAL COMMENT: Check for overlong encodings if no error
			// already.
			if (errorString.equals("") && this.checkOverlong && contBytes > 0
					&& (_unicode <= highestCharInNBytes[contBytes - 1])) {
				errorString += "Illegal overlong encoding of 0x"
						+ Integer.toHexString(_unicode);
			}

			// ORIGINAL COMMENT:
			// Attempt to read numeric character reference or entity reference
			// if we have an ampersand (&) start character, e.g. &#123; for
			// decimal, &#xABC; for hex
			//
			// http://www.w3.org/TR/2000/WD-xml-2e-20000814#dt-charref
			//
			// [66] CharRef ::= '&#' [0-9]+ ';' | '&#x' [0-9a-fA-F]+ ';'
			//
			// Well-formedness constraint: Legal Character
			//
			// Characters referred to using character references must match the
			// production for Char.
			//
			// If the character reference begins with "&#x ", the digits and
			// letters up to the terminating ; provide a hexadecimal
			// representation of the character's code point in ISO/IEC 10646. If
			// it begins just with "&#", the digits up to the terminating ;
			// provide a decimal representation of the character's code point.
			if (checkEntities && (this._byte[0] == '&')) {
				for (j = 1; (j < this.MAX_BYTES && this._byte[j - 1] != ';'); j++) {
					this.is.mark(1);
					if ((charRead = (byte) this.is.read()) == -1) {
						this._byte[j] = ';';
						errorString += "EOF in entity reference, "
								+ "terminated to read " + byteStr.length;
						// IO error here??
					} else if (charRead < 32) {
						this.is.reset();
						this._byte[j] = ';';
						errorString += "Character < 32 in entity reference, "
								+ "terminated to read " + byteStr.length;
						// IO error here??
					} else {
						this.bytenum++;
						if ((charRead < '0' || charRead > '9')
								&& (charRead < 'a' || charRead > 'z')
								&& (charRead < 'A' || charRead > 'Z')
								&& charRead != '#' && charRead != ';') {
							// ORIGINAL COMMENT:
							// There are a vast number of characters allowed in
							// a general XML entity reference (see
							// http://www.w3.org/TR/2000/WD-xml-2e-20000814
							// #NT-EntityRef).
							// Here I allow a reduced set of characters
							// sufficient to allow parsing of numeric character
							// references and the 5 XML entities.
							errorString += "Bad character in entity reference, "
									+ "got 0x" + Integer.toHexString(charRead);

							errorString = errorString.trim()
									+ ", substituted by '"
									+ this.substituteCharacter
									+ "' ("
									+ Integer
											.toHexString(this.substituteCharacter)
									+ ")'\n";
							_unicode = this.substituteCharacter;
						}
						this._byte[j] = charRead;
					}
				}
				contBytes = (j - 1);
				if (this._byte[contBytes] == ';') {
					if (this._byte[1] == '#') {
						if ((_unicode = parseNumericCharacterReference(this._byte)) == 0) {
							errorString += "Bad numeric character reference: "
									+ Arrays.toString(this._byte);
						}
					} else if (!validXMLEntity(this._byte)) {
						errorString += "Illegal XML entity reference: "
								+ Arrays.toString(this._byte);
					}
				} else {
					// ORIGINAL COMMENT:
					// There is no limit on the length of an entity, it is
					// defined via:
					//
					// http://www.w3.org/TR/2000/WD-xml-2e-20000814#NT-EntityRef
					//
					// [68] EntityRef ::= '&' Name ';'
					// [5] Name ::= (Letter | '_' | ':') ( NameChar)*
					// [4] NameChar ::= Letter | Digit | '.' | '-' | '_' | ':' |
					// CombiningChar | Extender
					// ...
					// However, here we add a local constraint of maximum length
					// MAX_BYTES which is more than sufficient to allow numeric
					// character references and the 5 XML entities.
					// [Simeon/2005-10-25]
					errorString += "Entity reference too long (local"
							+ " constraint) or not terminated, " + "adding ';'";
					this._byte[++contBytes] = ';';
				}
			}

			// ORIGINAL COMMENT: Check for illegal Unicode chars.
			if (errorString.equals("") && !validUTF8Char(_unicode)) {
				errorString += "Illegal UTF-8 code: 0x"
						+ Integer.toHexString(_unicode);
			}

			if (errorString.equals("")) {
				if (this.checkXml1_0Chars && !validXML1_0Char(_unicode)) {
					errorString += "Code not allowed in XML1.0: 0x"
							+ Integer.toHexString(_unicode);
				} else if (this.checkXml1_1Chars && !validXML1_1Char(_unicode)) {
					errorString += "Code not allowed in XML1.1: 0x"
							+ Integer.toHexString(_unicode);
				} else {
					for (k = 0; badChars[k] != 0; k++) {
						if (_unicode == badChars[k]) {
							errorString += "Bad code: 0x"
									+ Integer.toHexString(_unicode);
							break;
						}
					}
				}
			}

			// If an error did occur, put the substitution char into the byte
			// list and set unicode to the substitution character.
			if (!errorString.equals("")) {
				this.unicode = this.substituteCharacter;
				this._byte[0] = this.substituteCharacter;
				this._byte[1] = 0;
				j = 1;
			} else {
				// ORIGINAL COMMENT: Finally check for restricted chars that we
				// do a NCR substitution for.
				if (this.checkXml1_1Restricted
						&& restrictedXML1_1Char(_unicode)) {
					errorString += "Code restricted in XML1.1: 0x"
							+ Integer.toHexString(_unicode);

					errorString = errorString.trim() + " substituted NCR '"
							+ "&#x" + _unicode + "'";
					this._byte[0] = this.substituteCharacter;
					this._byte[1] = 0;
				}
			}

			if (!errorString.equals("")) {
				// Empty the error string (make place for new errors :)
				this.errorList.add("Line " + this.linenum + ", char "
						+ this.charnum + ", byte " + this.bytenum + "[0x"
						+ Long.toHexString(this.bytenum) + "]: "
						+ errorString.trim());
				errorString = "";
			}
			contBytes = j - 1;

			return true;
		}

		return false;
	}

	// GET & SET METHODS *******************************************************

	/***************************************************************************
	 * The substitute character must consist of ONE byte only.
	 * 
	 * @param _substituteCharacter
	 **************************************************************************/
	public void setSubstituteCharacter(char _substituteCharacter) {
		this.substituteCharacter = (byte) _substituteCharacter;
	}

	/***************************************************************************
	 * The number of errors occured in this stream.
	 * 
	 * @return Number of errors.
	 **************************************************************************/
	public ArrayList getErrorList() {
		return this.errorList;
	}

	/***************************************************************************
	 * Set if all the XML 1.0 characters shall be tested.
	 * 
	 * @param _checkXml1_0Chars
	 **************************************************************************/
	public void setCheckXml1_0Chars(boolean _checkXml1_0Chars) {
		this.checkXml1_0Chars = _checkXml1_0Chars;
	}

	/***************************************************************************
	 * Set if all the XML 1.1 characters shall be tested.
	 * 
	 * @param _checkXml1_1Chars
	 **************************************************************************/
	public void setCheckXml1_1Chars(boolean _checkXml1_1Chars) {
		this.checkXml1_1Chars = _checkXml1_1Chars;
	}

	/***************************************************************************
	 * Set if all the XML 1.1 restricted characters shall be tested.
	 * 
	 * @param _checkXml1_1Restricted
	 **************************************************************************/
	public void setCheckXml1_1Restricted(boolean _checkXml1_1Restricted) {
		this.checkXml1_1Restricted = _checkXml1_1Restricted;

	}

	/***************************************************************************
	 * Set if overlong characters shall be tested.
	 * 
	 * @param _checkOverlong
	 **************************************************************************/
	public void setCheckOverlong(boolean _checkOverlong) {
		this.checkOverlong = _checkOverlong;
	}

	// INTERNAL (Internal - implementation details, local classes, ...) ********

	/***************************************************************************
	 * ORIGINAL COMMENT: Returns true unless the character is one of a small set
	 * of codes that do not represent legal characters in Unicode.
	 * 
	 * From Unicode 3.2
	 * (http://www.unicode.org/unicode/reports/tr28/#3_1_conformance)
	 * UxD800..UxDFFF are ill-formed
	 * 
	 * UTF-8 is defined by http://www.ietf.org/rfc/rfc3629.txt (and obsoletes
	 * http://www.ietf.org/rfc/rfc2279.txt which, in turn obsoletes
	 * http://www.ietf.org/rfc/rfc2044.txt )
	 * 
	 * Ux10FFFF is highest legal UTF-8 code RFC2279 permitted codes greater than
	 * Ux10FFFF but RFC3629 does not.
	 * 
	 * @param ch
	 * @return boolean
	 **************************************************************************/
	private boolean validUTF8Char(int ch) {
		boolean result = false;

		if ((ch < 0xD800 || ch > 0xDFFF) && ch <= 0x10FFFF) {
			result = true;
		}

		return result;
	}

	/***************************************************************************
	 * ORIGINAL COMMENT: From http://www.w3.org/TR/2000/REC-xml-20001006 (sec
	 * 2.2, extracted 16July2001)
	 * 
	 * Legal characters are tab, carriage return, line feed, and the legal
	 * characters of Unicode and ISO/IEC 10646. The versions of these standards
	 * cited in A.1 Normative References were current at the time this document
	 * was prepared. New characters may be added to these standards by
	 * amendments or new editions. Consequently, XML processors must accept any
	 * character in the range specified for Char. The use of "compatibility
	 * characters", as defined in section 6.8 of [Unicode] (see also D21 in
	 * section 3.6 of [Unicode3]), is discouraged.]
	 * 
	 * Character Range Char ::= #x9 | #xA | #xD | [#x20-#xD7FF] |
	 * [#xE000-#xFFFD] | [#x10000-#x10FFFF] [end excerpt]
	 * 
	 * @param ch
	 * @return boolean
	 **************************************************************************/
	private boolean validXML1_0Char(int ch) {
		boolean result = false;

		if (ch == 0x09 || ch == 0x0A || ch == 0x0D
				|| (ch >= 0x20 && ch <= 0xD7FF)
				|| (ch >= 0xE000 && ch <= 0xFFFD)
				|| (ch >= 0x10000 && ch <= 0x10FFFF)) {
			result = true;
		}

		return result;
	}

	/***************************************************************************
	 * ORIGINAL COMMENT: From http://www.w3.org/TR/xml11/#charsets (sec 2.2,
	 * extracted 23Dec2003)
	 * 
	 * Char ::= [#x1-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]
	 * RestrictedChar ::= [#x1-#x8] | [#xB-#xC] | [#xE-#x1F] | [#x7F-#x84] |
	 * [#x86-#xBF]
	 * 
	 * Spec doesn't seem to say what one should do about RestricterChar.
	 * However, http://www.w3.org/International/questions/qa-controls.html says:
	 * 
	 * In XML 1.1 (which is still in Candidate Recommendation stage), if you
	 * need to represent a control code explicitly the simplest alternative is
	 * to use an NCR (numeric character reference). For example, the control
	 * code ESC (Escape) U+001B would be represented by either the &#x1B;
	 * (hexadecimal) or &#27; (decimal) Numeric Character References.
	 * 
	 * @param ch
	 * @return boolean
	 **************************************************************************/
	private boolean validXML1_1Char(int ch) {
		boolean result = false;

		if ((ch >= 0x1 && ch <= 0xD7FF) || (ch >= 0xE000 && ch <= 0xFFFD)
				|| (ch >= 0x10000 && ch <= 0x10FFFF)) {
			result = true;
		}

		return result;
	}

	/***************************************************************************
	 * @param ch
	 * @return boolean
	 **************************************************************************/
	private boolean restrictedXML1_1Char(int ch) {
		boolean result = false;

		if ((ch >= 0x1 && ch <= 0x8) || (ch >= 0xB && ch <= 0xC)
				|| (ch >= 0xE && ch <= 0x1F) || (ch >= 0x7F && ch <= 0x84)
				|| (ch >= 0x86 && ch <= 0xBF)) {
			result = true;
		}

		return result;
	}

	/***************************************************************************
	 * ORIGINAL COMMENT: Parse a numeric character reference in either decimal
	 * or hex notation. Expects b[] to start with codes for &# and to be
	 * terminated with ;
	 * 
	 * Returns: unicode code point on success 0 on failure
	 * 
	 * Note that #x0 is not a valid XML Char and so can safely be used as the
	 * failure return value. See
	 * http://www.w3.org/TR/2000/WD-xml-2e-20000814#sec-references
	 * 
	 * @param b
	 * @return boolean
	 **************************************************************************/
	private int parseNumericCharacterReference(int b[]) {
		int _unicode = 0;

		if (b[2] == 'x') {
			// hex
			for (int j = 3; b[j] != ';'; j++) {
				_unicode *= 16;
				if (b[j] >= '0' && b[j] <= '9') {
					_unicode += b[j] - '0';
				} else if (b[j] >= 'a' && b[j] <= 'f') {
					_unicode += b[j] - 'a' + 10;
				} else if (b[j] >= 'A' && b[j] <= 'F') {
					_unicode += b[j] - 'A' + 10;
				} else {
					return 0;
				}
			}
		} else {
			// decimal
			for (int j = 2; b[j] != ';'; j++) {
				_unicode *= 10;
				if (b[j] >= '0' && b[j] <= '9') {
					_unicode += b[j] - '0';
				} else {
					return 0;
				}
			}
		}

		return _unicode;
	}

	/***************************************************************************
	 * ORIGINAL COMMENT: Returns true if the entity passed in is one of the 5
	 * pre-defined valid non-numerical entities allowed in XML: &amp; &apos;
	 * &quot; &gt; &lt; Returns false otherwise
	 * 
	 * @param b
	 * @return boolean
	 **************************************************************************/
	private boolean validXMLEntity(int b[]) {
		boolean result = false;

		if (b[0] == '&'
				&& ((b[1] == 'a' && b[2] == 'm' && b[3] == 'p' && b[4] == ';')
						|| (b[1] == 'a' && b[2] == 'p' && b[3] == 'o'
								&& b[4] == 's' && b[5] == ';')
						|| (b[1] == 'q' && b[2] == 'u' && b[3] == 'o'
								&& b[4] == 't' && b[5] == ';')
						|| (b[1] == 'g' && b[2] == 't' && b[3] == ';') || (b[1] == 'l'
						&& b[2] == 't' && b[3] == ';'))) {
			result = true;
		}

		return result;
	}

}
