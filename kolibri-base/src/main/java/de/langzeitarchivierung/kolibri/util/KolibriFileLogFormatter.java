/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / KolibriFileLogFormatter.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * 
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

import java.util.logging.LogRecord;

/*******************************************************************************
 * The KolibriFileLogFormatter formats the kopal file log.
 * 
 * @author Funk
 * @version 20060126
 * @since 20060125
 ******************************************************************************/

public class KolibriFileLogFormatter extends java.util.logging.Formatter
{

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * Default constructor
	 **************************************************************************/
	public KolibriFileLogFormatter() {
		super();
	}

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * @see java.util.logging.Formatter#format(java.util.logging.LogRecord)
	 **************************************************************************/
	@Override
	public String format(LogRecord record) {
		String result = "";
		String message = "";

		// Remove all "\"", \n and \t, replace them for "'", " " and " " for
		// .TSV compatibility (Tab Separated Value)
		message = record.getMessage().replaceAll("\"", "'");
		message = record.getMessage().replaceAll("\t", "    ");
		message = record.getMessage().replaceAll("\n", " ");

		result += KolibriTimestamp.getTimestampWithMs(record.getMillis()) + "\t"
				+ record.getMillis() + "\t" + "[#" + record.getThreadID()
				+ "]\t" + record.getLevel() + "\t"
				+ record.getSourceClassName() + "."
				+ record.getSourceMethodName() + "()\t" + message + "\n";

		return result;
	}

}
