/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / FormatMapper.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 *******************************************************************************
 * 
 * 
 * CHANGELOG:
 *	2010-09-17	Funk	Parametrized List parameter.
 *	2008-02-08	Funk	Added logging entry for missing dias to jhove mapping
 * 						file.
 * 2007-07-10	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/*******************************************************************************
 * <p>
 * A class that maps a format registry in DIAS format to the names and versions
 * used by JHOVE from HUL.
 * </p>
 * 
 * @author Matthias Neubauer, German National Library
 * @version 2016-06-08
 * @since 2006-01-02
 * @see de.langzeitarchivierung.kolibri.util.FormatRegistry
 ******************************************************************************/

public class FormatMapper {

	// CLASS VARIABLES (Static variables) **************************************

	private static Logger	defaultLogger	= Logger.getLogger("de.langzeitarchivierung.kolibri");

	// MANIPULATION (Manipulation - what the object does) **********************

	/**
	 * <p>
	 * A method that maps one format registry to another.
	 * </p>
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @see de.langzeitarchivierung.kolibri.util.FormatRegistry
	 * @param formatreg
	 *            - The ArrayList that contains the format registry to be mapped
	 * @param mappingFile
	 *            - Path to the xml format mapping file
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	public void map(java.util.ArrayList<FormatInfo> formatreg,
			String mappingFile) throws IOException,
			ParserConfigurationException, SAXException {

		String xmlstring = "";
		java.io.File xmlfile = new java.io.File(mappingFile);

		if (xmlfile.exists()) {
			// Use the backup file.
			xmlstring = FileUtils.readFile(xmlfile);
		} else {
			String message = "No format mapping file found";
			defaultLogger.log(Level.SEVERE, message, xmlfile);
			throw new IOException(message);
		}

		// Get the single "Item" Nodes.
		org.w3c.dom.Document doc = KolibriXMLParser
				.getDocumentFromString(xmlstring);

		org.w3c.dom.NodeList formats = doc.getElementsByTagName("map");

		FormatInfo tempform = new FormatInfo();

		// Go through the List...
		for (int i = 0; i < formatreg.size(); i++) {
			tempform = formatreg.get(i);
			int numberOfItems = formats.getLength();

			// Go through the List of entries an create a FormtatInfo for each.
			for (int j = 0; j < numberOfItems; j++) {
				String oldname = KolibriXMLParser
						.getDirectSubNodes(formats.item(j), "oldname").item(0)
						.getFirstChild().getNodeValue();

				if (tempform.getFileType().equals(oldname)) {
					String newname = KolibriXMLParser
							.getDirectSubNodes(formats.item(j), "newname")
							.item(0).getFirstChild().getNodeValue();
					String version = "";
					boolean addoldversion = false;

					try {
						version = KolibriXMLParser
								.getDirectSubNodes(formats.item(j),
										"newversion").item(0).getFirstChild()
								.getNodeValue();
						if (KolibriXMLParser
								.getDirectSubNodes(formats.item(j),
										"newversion").item(0).getAttributes()
								.getNamedItem("addoldversion").getNodeValue()
								.equals("true")) {
							addoldversion = true;
						}
					} catch (java.lang.ArrayIndexOutOfBoundsException e) {
						// Fall through.
					}

					tempform.setFileType(newname);

					if (!"".equals(version)) {
						if (addoldversion) {
							version += tempform.getFileTypeVersion();
						}
						tempform.setFileTypeVersion(version);
					}

					break;
				}
			}
		}
	}

}
