/**
 * Copyright 2005-2020 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 */

/**
 * TODOLOG
 **
 * CHANGELOG
 *
 * 2011-08-31 - Funk - Added timestamp to method logging string.
 *
 * 2011-08-11 - Funk - Minor changes for method logging.
 *
 * 2007-07-10 - koLibRI version 1.0
 */

package de.langzeitarchivierung.kolibri.util;

import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * <p>
 * The KolibriConsoleLogFormatter formats the kopal console log.
 * </p>
 * 
 * @author Funk
 * @version 2020-02-24
 * @since 2006-01-25
 */

public class KolibriConsoleLogFormatter extends java.util.logging.Formatter {

  // **
  // CLASS VARIABLES (Static variables)
  // **

  private int oldThreadId = 0;
  private String oldClassName = null;

  // **
  // CREATION (Constructors, factory methods, static/inst init)
  // **

  /**
   * <p>
   * Default constructor
   * </p>
   */
  public KolibriConsoleLogFormatter() {
    super();
  }

  // **
  // MANIPULATION (Manipulation - what the object does)
  // **

  /*
   * (non-Javadoc)
   * 
   * @see java.util.logging.Formatter#format(java.util.logging.LogRecord)
   */
  @Override
  public String format(LogRecord record) {

    String result = "";

    String className = record.getSourceClassName();
    String methodName = record.getSourceMethodName();
    int threadId = record.getThreadID();
    String recordMessage = record.getMessage();
    long recordDate = record.getMillis();

    if (this.oldClassName == null) {
      this.oldClassName = "";
    }

    // Show thread ID and class Name if different from last ID.
    if (this.oldThreadId != threadId) {
      result += "\n[#" + threadId + "]\t" + className + "." + methodName + "() ["
          + KolibriTimestamp.getTimestampWithMs(recordDate) + "]\n";
    }
    // Only show the class name if thread ID has not changed since last log entry.
    else if (!this.oldClassName.equals(className)) {
      result += "\n\t" + className + (methodName.equals("") ? "" : "." + methodName + "()") + "\n";
    }

    // Set the old values.
    this.oldThreadId = threadId;
    this.oldClassName = className;

    // If the log level is equal to WARNING or equal to SEVERE, add it to the log message if not
    // occurring in an action module.
    Level level = record.getLevel();
    String levelMessage = "";

    if (!className.equals("de.langzeitarchivierung.kopal.ui.LoggingStatusListener")) {
      // Put some spaces behind the level.
      for (int i = 0; i < (9 - level.toString().length()); i++) {
        levelMessage += " ";
      }

      levelMessage = "[" + level + "]" + levelMessage;
    }

    result += "\t" + levelMessage + recordMessage + "\n";

    return result;
  }

}
