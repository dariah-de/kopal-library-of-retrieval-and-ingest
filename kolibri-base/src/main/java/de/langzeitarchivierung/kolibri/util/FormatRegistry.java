/**
 * Copyright 2005-2024 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * http://dp4lib.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 * 
 **
 * CHANGELOG
 * 
 * 2011-08-22 - Hausner - Added simple method for getting DIAS IDs by format and version.
 * 
 * 2011-08-19 - Funk - Added some todo notes.
 * 
 * 2007-07-10 - koLibRI version 1.0
 */

package de.langzeitarchivierung.kolibri.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import de.ddb.lmeRfile.LmerFile;
import de.langzeitarchivierung.kolibri.ingest.DiasIngest;

/**
 * <p>
 * Encapsulating class for a table of format specific data.
 * </p>
 * 
 * @author Matthias Neubauer, German National Library
 * @see de.langzeitarchivierung.kolibri.actionmodule.MetadataGenerator
 */

public class FormatRegistry {

  // CLASS VARIABLES (Static variables)

  private static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");
  private static String formatRegistryBackupFile = "";

  // TODO Get the unknown filetype ID from the DIAS?
  public static String UNKNOWN_FILETYPE = "urn:diasid:fty:kopal:0000000000000000000000";

  // STATE (Instance variables)

  private String workingDir = "";
  private String formatMapFileLocation = "";
  private ArrayList<FormatInfo> formatRegistry = new ArrayList<FormatInfo>();
  private FormatMapper formatMapper = new FormatMapper();
  private boolean useBackupFileOnly = false;

  /**
   * <p>
   * Default constructor.
   * </p>
   * 
   * @throws IOException
   * @throws ParserConfigurationException
   */
  public FormatRegistry() throws IOException, ParserConfigurationException {

    super();

    try {
      Configurator.configure(this);
    } catch (Exception e) {
      e.printStackTrace();
      defaultLogger.log(Level.SEVERE, "Error configuring class " + this.getClass().getName()
          + ". System message: " + e.getMessage());
    }

    defaultLogger.log(Level.INFO, "Initializing Format Registry");

    try {
      this.initializeXMLFormatRegistryData();
    } catch (SAXException e) {
      String message =
          "Error parsing the DIAS format registry backup file. No DIAS file types available at all";
      defaultLogger.log(Level.SEVERE, message, new File(formatRegistryBackupFile));
      throw new IOException(message);
    } catch (IOException e) {
      String message =
          "Error accessing the format registry backup file. No DIAS file types available at all";
      defaultLogger.log(Level.SEVERE, message, new File(formatRegistryBackupFile));
      throw new IOException(message);
    }
  }

  /**
   * <p>
   * Default constructor.
   * </p>
   * 
   * @throws IOException
   * @throws ParserConfigurationException
   */
  public FormatRegistry(String workingDir) throws IOException,
      ParserConfigurationException {

    super();

    defaultLogger.log(Level.INFO, "Setting Working Directory:" + workingDir);

    this.workingDir = workingDir;

    try {
      Configurator.configure(this);
    } catch (Exception e) {
      e.printStackTrace();
      defaultLogger.log(Level.SEVERE, "Error configuring class " + this.getClass().getName()
          + ". System message: " + e.getMessage());
    }

    defaultLogger.log(Level.INFO, "Initializing Format Registry");

    try {
      this.initializeXMLFormatRegistryData();
    } catch (SAXException e) {
      String message =
          "Error parsing the DIAS format registry backup file. No DIAS file types available at all";
      defaultLogger.log(Level.SEVERE, message, new File(formatRegistryBackupFile));
      throw new IOException(message);
    } catch (IOException e) {
      String message =
          "Error accessing the format registry backup file. No DIAS file types available at all";
      defaultLogger.log(Level.SEVERE, message, new File(formatRegistryBackupFile));
      throw new IOException(message);
    }

  }

  /**
   * @param diasFormatId
   * @return
   */
  public FormatInfo getFormatInfo(String diasFormatId) {

    for (FormatInfo fInfo : this.formatRegistry) {
      if (fInfo.getFileTypeID().equalsIgnoreCase(diasFormatId)) {
        return fInfo;
      }
    }

    return null;
  }

  // MANIPULATION (Manipulation - what the object does)

  /**
   * <p>
   * Searches the format registry list for the specific entry of a format's name and its version.
   * </p>
   * 
   * @param formatName The formats name specified by the technical metadata implementation (e.g.
   *        JHOVE, FITS)
   * @param version The formats version specified by the technical metadata implementation (e.g.
   *        JHOVE, FITS)
   * @param lmerfile LmerFile object which takes the infos
   */
  public void getFormat(String formatName, String version, LmerFile lmerfile) {

    // Go through the list...
    for (int i = 0; i < this.formatRegistry.size(); i++) {
      FormatInfo tempform = this.formatRegistry.get(i);

      // If the format has no version specified, do not compare versions.
      if (version == null) {

        // Compare the format name from the technical metadata implementation (e.g. JHOVE, FITS)
        // with the actual entry.
        if (tempform.getFileType().equals(formatName)) {

          // If it equals, set the specific LmerFile fields and return.
          // TODO We take the first applicable format ID here, right?
          // That makes maybe sense, if the technical metadata implementation (e.g. JHOVE, FITS)
          // only extracts the format string, but no version. For queries on the format it makes NO
          // SENSE! Please use the newly implemented getFormats() method!
          lmerfile.getFormatArray(0).setStringValue(tempform.getFileTypeID());

          if (!tempform.getFileTypeCategory().equals("")) {
            // TODO Check if this works.
            lmerfile.setCategory(LmerFile.Category.Enum.forString(tempform.getFileTypeCategory()));
          }
          if (!tempform.getFileTypeViewerApplication().equals("")) {
            lmerfile.setViewerApplication(tempform.getFileTypeViewerApplication());
          }

          lmerfile.getFormatArray(0).setREGISTRYNAME("DIAS");

          return;
        }
      } else {
        // Compare the formats name and version from the technical metadata implementation with the
        // actual entry.
        if (tempform.getFileType().equals(formatName)
            && tempform.getFileTypeVersion().equals(version)) {

          // If they equal, set the specific LmerFile fields and return.
          lmerfile.getFormatArray(0).setStringValue(tempform.getFileTypeID());

          if (!tempform.getFileTypeCategory().equals("")) {
            // TODO check if this works
            lmerfile.setCategory(LmerFile.Category.Enum.forString(tempform.getFileTypeCategory()));
          }
          if (!tempform.getFileTypeViewerApplication().equals("")) {
            lmerfile.setViewerApplication(tempform.getFileTypeViewerApplication());
          }

          lmerfile.getFormatArray(0).setREGISTRYNAME("DIAS");

          return;
        }
      }
    }

    // Fall-through: Set the format name to the DIAS "Unknown File Type".
    lmerfile.getFormatArray(0).setStringValue(UNKNOWN_FILETYPE);
    lmerfile.getFormatArray(0).setREGISTRYNAME("DIAS");
    // Set category to BINARY in case of an unknown file format.
    lmerfile.setCategory(LmerFile.Category.BINARY);
  }

  /**
   * <p>
   * Searches the format registry list for the specific entry of a formats name and its version. If
   * a format corresponding to the given formatName and version is found, its fileTypeID is returned
   * as the only entry of the List. If there is no version given, all fileTypeIDs corresponding to
   * the given formatName will be returned.
   * </p>
   * 
   * @param formatName The formats name specified by the technical metadata implementation (e.g.
   *        JHOVE, FITS)
   * @param version The formats version specified by the technical metadata implementation (e.g.
   *        JHOVE, FITS)
   */
  public List<String> getFormats(String formatName, String version) {

    List<String> formats = new ArrayList<String>();

    // Go through the list...
    for (int i = 0; i < this.formatRegistry.size(); i++) {
      FormatInfo tempform = this.formatRegistry.get(i);

      // Compare the format name from the technical metadata implementation (e.g. JHOVE, FITS) with
      // the actual entry.
      if (tempform.getFileType().equals(formatName)) {

        // If the format has no version specified, do not compare versions.
        if (version == null) {
          formats.add(tempform.getFileTypeID());
        } else {
          // Compare the format version from the technical metadata implementation (e.g. JHOVE,
          // FITS) with the actual entry.
          if (tempform.getFileTypeVersion().equals(version)) {
            formats.add(tempform.getFileTypeID());
            break;
          }
        }
      }
    }

    return formats;
  }

  /**
   * <p>
   * Method to initialize the format registry by requesting the file types list from DIAS or using a
   * local backup file.
   * </p>
   * 
   * @throws IOException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public void initializeXMLFormatRegistryData() throws SAXException,
      IOException, ParserConfigurationException {

    String xmlstring = "";

    File xmlfile = null;

    if (this.workingDir != null && this.workingDir.length() > 0) {
      xmlfile = new File(this.workingDir + formatRegistryBackupFile);
    } else {
      xmlfile = new File(formatRegistryBackupFile);
    }

    defaultLogger.log(Level.INFO, "format registry location: " + xmlfile.getAbsolutePath());

    if (!this.useBackupFileOnly) {
      // Retrieve the FormatRegistry from DIAS.
      DiasIngest diasing = new DiasIngest();

      // Configure the DiasIngest object and get the file type response from DIAS. If not
      // successful, use the backup file.
      try {
        Configurator.configure(diasing);
        defaultLogger.log(Level.FINE, "Requesting format registry file from DIAS");
        xmlstring = diasing.requestFileTypes();

        // Get the ArrayList with format information from the DiasIngest.
        this.formatRegistry = DiasIngest.parseDiasFormatResponse(xmlstring);

        // Store the retrieved information locally.
        if (!xmlstring.equals("")) {
          defaultLogger.log(Level.FINE, "Storing format registry file");
          FileUtils.storeFile(xmlfile, xmlstring);
        }
      } catch (SAXException e) {
        defaultLogger.log(Level.WARNING,
            "Parsing the DIAS format registry file failed. Please check the response");
        this.useBackupFileOnly = true;
      } catch (Exception e) {
        defaultLogger.log(Level.WARNING, "Request for DIAS format registry file failed");
        this.useBackupFileOnly = true;
      }
    }

    if (this.useBackupFileOnly) {
      if (xmlfile.exists()) {
        // Use the backup file.
        defaultLogger.log(Level.INFO,
            "Using local format registry backup file: " + xmlfile.getAbsolutePath());
        xmlstring = FileUtils.readFile(xmlfile);

        // Parse the backup file.
        this.formatRegistry = DiasIngest.parseDiasFormatResponse(xmlstring);
      } else {
        String message = "No backup file for format registry found! Formats can not be identified";
        defaultLogger.log(Level.SEVERE, message, xmlfile);
        throw new IOException(message);
      }
    }

    defaultLogger.log(Level.FINE, "Loaded " + this.formatRegistry.size() + " format descriptions");
    defaultLogger.log(Level.FINE, "Unknown File Type ID is '" + UNKNOWN_FILETYPE + "'");

    // Map the formats from the the technical metadata implementation (e.g. JHOVE, FITS) to the
    // DIAS.
    if (this.workingDir != null && this.workingDir.length() > 0) {
      this.formatMapper.map(this.formatRegistry, this.workingDir + this.formatMapFileLocation);
    } else {
      this.formatMapper.map(this.formatRegistry, this.formatMapFileLocation);
    }
  }

  // GET & SET METHODS

  /**
   * @param formatRegistryBackupFile The formatRegistryBackupFile to set.
   */
  public static void setFormatRegistryBackupFile(String formatRegistryBackupFile) {
    FormatRegistry.formatRegistryBackupFile = formatRegistryBackupFile;
  }

  /**
   * @param formatMapFileLocation The formatMapFileLocation to set.
   */
  public void setFormatMapFileLocation(String formatMapFileLocation) {
    this.formatMapFileLocation = formatMapFileLocation;
  }

  /**
   * @param useBackupFile The useBackupFile to set.
   */
  public void setUseBackupFileOnly(boolean useBackupFileOnly) {
    this.useBackupFileOnly = useBackupFileOnly;
  }

  /**
   * @param workingDir
   */
  public void setWorkingDir(String workingDir) {
    this.workingDir = workingDir;
  }

}
