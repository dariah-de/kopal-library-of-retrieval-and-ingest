/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / ChecksumGenerator.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.zip.DataFormatException;

/*******************************************************************************
 * Class for MD5, SHA-1 and XORed checksum calculations.
 * 
 * @author Funk, Ludwig, Neubauer
 * @version 2016-06-08
 ******************************************************************************/

public class ChecksumGenerator {

	// CONSTANTS **********************************************************

	/** Checksum type MD5 */
	public static final String	CHECKSUM_TYPE_MD5		= "MD5";
	/** Checksum type SHA-1 */
	public static final String	CHECKSUM_TYPE_SHA1		= "SHA-1";
	/** The XOR of SHA-1 file checksums */
	public static final String	XOR_OF_FILE_CHECKSUMS	= "xor of sha1 file checksums";

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * Main method for testing purposes.
	 * 
	 * @param args
	 **************************************************************************/
	public static void main(String args[]) {
		String urn[] = new String[14];
		urn[0] = "urn:nbn:de:1111-200606299";
		urn[1] = "urn:nbn:de:1111-2005051906";
		urn[2] = "urn:nbn:de:1111-2004092415";
		urn[3] = "urn:nbn:de:1111-2004092415";
		urn[4] = "urn:nbn:de:1111-20040721146";
		urn[5] = "urn:nbn:de:1111-20040721153";
		urn[6] = "urn:nbn:de:gbv:7-webdoc-49";
		urn[7] = "urn:nbn:de:gbv:089-3321752945";
		urn[8] = "urn:nbn:ch:bel-9478";
		urn[9] = "urn:nbn:ch:bel-9039";
		urn[10] = "urn:nbn:se:uu:diva-3475";
		urn[11] = "URN:NBN:no-3132";
		urn[12] = "urn:nbn:hu-3006";
		urn[13] = "urn:nbn:fi:tkk-004781";

		for (int i = 0; i < urn.length; i++) {
			if (urn[i] != null) {
				System.out.println("orig URN: " + urn[i]);
				System.out.println("urn ohne: "
						+ urn[i].substring(0, (urn[i].length() - 1)));
				System.out.println("fugu chs: "
						+ getUrnChecksum(urn[i].substring(0,
								(urn[i].length() - 1))));
			}
		}

		// return;

		String currentUrn;
		int count[] = new int[10];
		for (int i = 0; i < count.length; i++) {
			count[i] = 0;
		}
		int krams;

		for (int i = 0; i < 1000000; i++) {
			currentUrn = "urn:nbn:de:1111-" + i;
			krams = Integer.parseInt(getUrnChecksum(currentUrn));
			count[krams]++;
		}

		for (int i = 0; i < count.length; i++) {
			System.out.println(i + "  " + count[i]);
		}

		// return;

		String a = "0bb31baca4e83653977c1e59b1905284491c86a4";
		String b = "737b7a359d271747ea4ae241ca0e00fceb7c91c0";
		System.out.println("String a     : " + a);
		System.out.println("String b     : " + b);
		try {
			System.out.println("xored Strings: " + xorHexStrings(a, b));
			System.out.println("is ((a xor b) xor b) == a?: "
					+ a.equals(xorHexStrings(xorHexStrings(a, b), b)));
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	/***************************************************************************
	 * Calculates a MD5 checksum for a given String.
	 * 
	 * @return the checksum string
	 * @param message
	 *            - The InputStream to calculate checksum for
	 * @exception NoSuchAlgorithmException
	 * @exception IOException
	 **************************************************************************/
	public static String getMD5(InputStream message)
			throws NoSuchAlgorithmException, IOException {
		return digest(message, CHECKSUM_TYPE_MD5);
	}

	/***************************************************************************
	 * Calculates a MD5 checksum for a given String.
	 * 
	 * @return the checksum string
	 * @param message
	 *            - The string to calculate checksum for
	 * @exception NoSuchAlgorithmException
	 * @exception IOException
	 **************************************************************************/
	public static String getMD5(String message)
			throws NoSuchAlgorithmException, IOException {
		return digest(new ByteArrayInputStream(message.getBytes()),
				CHECKSUM_TYPE_MD5);
	}

	/***************************************************************************
	 * Calculates a SHA-1 checksum for a given String.
	 * 
	 * @return the checksum string
	 * @param message
	 *            - The InputStream to calculate checksum for
	 * @exception NoSuchAlgorithmException
	 * @exception IOException
	 **************************************************************************/
	public static String getSHA1(InputStream message)
			throws NoSuchAlgorithmException, IOException {
		return digest(message, CHECKSUM_TYPE_SHA1);
	}

	/***************************************************************************
	 * Calculates a SHA-1 checksum for a given String.
	 * 
	 * @return the checksum string
	 * @param message
	 *            - The string to calculate checksum for
	 * @exception NoSuchAlgorithmException
	 * @exception IOException
	 **************************************************************************/
	public static String getSHA1(String message)
			throws NoSuchAlgorithmException, IOException {
		return digest(new ByteArrayInputStream(message.getBytes()),
				CHECKSUM_TYPE_SHA1);
	}

	/***************************************************************************
	 * Returns the XORed result of two hex strings with the same length.
	 * 
	 * @param a
	 *            hex string
	 * @param b
	 *            hex string
	 * @return String the xored result
	 * @exception DataFormatException
	 *                if the strings have not the same length
	 **************************************************************************/
	public static String xorHexStrings(String a, String b)
			throws DataFormatException {

		if (a == null || b == null) {
			throw new DataFormatException("Strings are not allowed to be NULL "
					+ "for checksum calculation");
		}

		if (a.length() != b.length()) {
			throw new DataFormatException("Strings have not the same length: "
					+ a.length() + " vs " + b.length());
		}

		StringBuffer result = new StringBuffer();

		// XOR each character.
		for (int i = 0; i < a.length(); i++) {
			result.append(Integer.toHexString(Integer.valueOf(
					String.valueOf(a.charAt(i)), 16).intValue()
					^ Integer.valueOf(String.valueOf(b.charAt(i)), 16)
							.intValue()));
		}

		return result.toString();
	}

	/***************************************************************************
	 * Returns the SHA1-checksum of a file or if it is a direcory the XORed
	 * SHA1-checksums of all files beneath this directory and its
	 * subdirectories.
	 * 
	 * @param dirsOrFiles
	 *            an array of file objects which are directories or files
	 * @return the xored file checksum or null if no files exist
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws DataFormatException
	 **************************************************************************/
	public static String getXorFileChecksums(File[] dirsOrFiles)
			throws IOException, NoSuchAlgorithmException, DataFormatException {
		String xorFileChecksum = null;

		if (dirsOrFiles != null) {
			for (int i = 0; i < dirsOrFiles.length; i++) {
				if (!dirsOrFiles[i].exists()) {
					throw new FileNotFoundException(
							dirsOrFiles[i].getAbsolutePath()
									+ " does not exist");
				}

				if (dirsOrFiles[i].isFile()) {
					// The first file checksum should not be XORed but just set.
					if (xorFileChecksum == null) {
						xorFileChecksum = ChecksumGenerator
								.getSHA1(new FileInputStream(dirsOrFiles[i]));
					} else {
						xorFileChecksum = ChecksumGenerator.xorHexStrings(
								xorFileChecksum,
								getXorFileChecksums(dirsOrFiles[i]));
					}
				} else if (dirsOrFiles[i].isDirectory()
						&& dirsOrFiles[i].listFiles().length > 0) {
					// If no previous checksum exists, first calculate the dir's
					// checksum.
					if (xorFileChecksum == null) {
						xorFileChecksum = getXorFileChecksums(dirsOrFiles[i]
								.listFiles());
					} else {
						xorFileChecksum = ChecksumGenerator
								.xorHexStrings(xorFileChecksum,
										getXorFileChecksums(dirsOrFiles[i]
												.listFiles()));
					}
				}
			}
		}

		return xorFileChecksum;
	}

	/***************************************************************************
	 * Returns the SHA1-checksum of a file or if it is a directory the XORed
	 * SHA1-checksums of all files beneath this directory and its
	 * subdirectories.
	 * 
	 * @param dirOrFile
	 *            file or directory
	 * @return the xored file checksum or null if no files exist
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws DataFormatException
	 **************************************************************************/
	public static String getXorFileChecksums(File dirOrFile)
			throws IOException, NoSuchAlgorithmException, DataFormatException {
		String xorFileChecksum = null;
		if (!dirOrFile.exists()) {
			throw new FileNotFoundException(dirOrFile.getAbsolutePath()
					+ " does not exist");
		}

		if (dirOrFile != null) {
			if (dirOrFile.isFile()) {
				return ChecksumGenerator
						.getSHA1(new FileInputStream(dirOrFile));
			} else if (dirOrFile.isDirectory()
					&& dirOrFile.listFiles().length > 0) {
				xorFileChecksum = getXorFileChecksums(dirOrFile.listFiles());
			}
		}

		return xorFileChecksum;
	}

	/***************************************************************************
	 * Computes the checksum needed for submitting URNs to the German Nationam
	 * Library (DNB) according to http://www.persistent-identifier.de/?link=316.
	 * 
	 * @param identifier
	 * @return Returns the URN checksum only.
	 * @author Funk
	 * @version 20061016
	 **************************************************************************/
	public static String getUrnChecksum(String identifier) {
		HashMap<Character, Integer> hm = new HashMap<Character, Integer>();
		String token = "";
		int checksum = 0;

		// Build up the HashMap to get the character <--> number mapping.
		char trans[] = { '^', '0', '1', '2', '3', '4', '5', '6', '7', '8', '^',
				'U', 'R', 'N', 'B', 'D', 'E', ':', 'A', 'C', '^', 'F', 'G',
				'H', 'I', 'J', 'L', 'M', 'O', 'P', '^', 'Q', 'S', 'T', 'V',
				'W', 'X', 'Y', 'Z', '-', '^', '9' };

		for (int i = 0; i < trans.length; i++) {
			hm.put((char) trans[i], i);
		}

		// Build up the number string out of the identifier string.
		for (int i = 0; i < identifier.length(); i++) {
			if (hm.containsKey((char) identifier.toUpperCase().charAt(i))) {
				token += hm.get((char) identifier.toUpperCase().charAt(i));
			}
		}

		// Add sums according to specs.
		for (int i = 0; i < token.length(); i++) {
			checksum += (i + 1) * Character.getNumericValue(token.charAt(i));
		}

		// Get the last digit from token and divide.
		int lastDigit = Character
				.getNumericValue(token.charAt(token.length() - 1));

		// Return the checksum.
		return String.valueOf((checksum / lastDigit) % 10);
	}

	// INTERNAL (Internal - implementation details, local classes, ...) ******

	/***************************************************************************
	 * Produces a checksum.
	 * 
	 * See Appendix A of Java Cryptography Architecture API Specification &
	 * Reference.
	 * 
	 * @param message
	 *            the message to compute
	 * @param algo
	 *            an checksum algorithm available in java.security
	 * @return checksum the result
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 **************************************************************************/
	private static String digest(InputStream message, String algo)
			throws NoSuchAlgorithmException, IOException {
		BufferedInputStream bufMessage = new BufferedInputStream(message);
		MessageDigest checksumAlgo = MessageDigest.getInstance(algo);

		final int BUFFER_LENGTH = 1048576;
		byte[] buffer = new byte[BUFFER_LENGTH];

		// Read complete message and use it as source for the algorithm.
		int bytesRead = bufMessage.read(buffer);
		while (bytesRead > 0) {
			checksumAlgo.update(buffer, 0, bytesRead);
			bytesRead = bufMessage.read(buffer);
		}

		// Produce checksum from input.
		byte[] digest = checksumAlgo.digest();

		// Convert Bytes to String with leading zeros.
		StringBuffer result = new StringBuffer();
		for (int i = 0; i < digest.length; i++) {
			String s = Integer.toHexString(digest[i] & 0xFF);
			if (s.length() == 1) {
				result.append("0").append(s);
			} else {
				result.append(s);
			}
		}

		return result.toString();
	}

}
