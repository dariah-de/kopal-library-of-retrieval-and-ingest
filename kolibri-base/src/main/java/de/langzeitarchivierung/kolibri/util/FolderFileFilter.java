/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionmodule.textgrid / FolderFileFilter.java
 *  
 * Copyright 2011 by Project TextGrid
 * 
 * http://textgrid.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 	2011-05-23	Funk	First version.
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

import java.io.File;
import java.io.FileFilter;

/*******************************************************************************
 * <p>
 * File filter class to filter everything but folders :-)
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2011-05-27
 * @since 2011-05-23
 ******************************************************************************/

public class FolderFileFilter implements FileFilter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.io.FileFilter#accept(java.io.File)
	 */
	
	public boolean accept(File theFile) {
		if (theFile.isDirectory()) {
			return true;
		}

		return false;
	}
}
