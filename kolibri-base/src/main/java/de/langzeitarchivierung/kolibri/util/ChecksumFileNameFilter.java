package de.langzeitarchivierung.kolibri.util;

import java.io.File;
import java.io.FilenameFilter;

/*******************************************************************************
 * filter files which begins with the same filename given in class 
 * variable filename
 *  
 * 
 * @author Stefan Hein, German National Library
 * @version 20101008
 * @since 20101008
 ******************************************************************************/
public class ChecksumFileNameFilter implements FilenameFilter {

	private String filename; 

    public ChecksumFileNameFilter(String name) {
	    this.filename = name;
	}

	public boolean accept(File f, String name) {
		return name.startsWith(filename); 
    }
}

