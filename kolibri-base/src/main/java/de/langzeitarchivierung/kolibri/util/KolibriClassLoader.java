/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / KolibriClassLoader.java
 *  
 * Copyright 2005-2009 by Project kopal
 * Copyright 2010-2011 by Project DP4lib
 * 
 * http://kopal.langzeitarchivierung.de
 * http://dp4lib.langzeitarchivierung.de
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 * 
 * 
 * CHANGELOG
 * 
 * 	08.01.2011	Funk	Changed class loader to system class loader.
 * 	10.07.2007	koLibRI version 1.0
 * 
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;

/*******************************************************************************
 * <p>
 * Helper methods for loading classes, not a real class loader.
 * </p>
 * 
 * 
 * @author Jens Ludwig, SUB Göttingen
 * @version 2011-01-08
 * @since 2006-01-23
 ******************************************************************************/

public class KolibriClassLoader {

	// CONSTANTS **********************************************************

	private static final String	MODUL_PACKAGES				= "de.langzeitarchivierung.kolibri.actionmodule";
	private static final String	PROCESS_STARTER_PACKAGES	= "de.langzeitarchivierung.kolibri.processstarter";
	private static final String	FORMAT_PACKAGES				= "de.langzeitarchivierung.kolibri.formats";

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * <p>
	 * Gets the class for the class name of an actionModule.
	 * </p>
	 * 
	 * @param actionModuleName
	 *            The name of the class
	 * @return the class
	 * @throws MalformedURLException
	 * @throws ClassNotFoundException
	 **************************************************************************/
	public static Class getActionModule(String actionModuleName)
			throws MalformedURLException, ClassNotFoundException {
		return getClass(actionModuleName, MODUL_PACKAGES);
	}

	/***************************************************************************
	 * <p>
	 * Gets the class for the class name of a ProcessStarter.
	 * </p>
	 * 
	 * @param processStarterName
	 *            The name of the class
	 * @return the class
	 * @throws MalformedURLException
	 * @throws ClassNotFoundException
	 **************************************************************************/
	public static Class getProcessStarter(String processStarterName)
			throws MalformedURLException, ClassNotFoundException {
		return getClass(processStarterName, PROCESS_STARTER_PACKAGES);
	}

	/***************************************************************************
	 * <p>
	 * Gets the class for the class name of a MetadataFormat.
	 * </p>
	 * 
	 * @param formatName
	 *            The name of the class
	 * @return the class
	 * @throws MalformedURLException
	 * @throws ClassNotFoundException
	 **************************************************************************/
	public static Class getFormat(String formatName)
			throws MalformedURLException, ClassNotFoundException {
		return getClass(formatName, FORMAT_PACKAGES);
	}

	/***************************************************************************
	 * <p>
	 * Gets the class for the class name.
	 * </p>
	 * 
	 * TODO Use better class loading here!!
	 * 
	 * @param className
	 *            The name of the class
	 * @param parentPackage
	 *            The package where we should for the class
	 * @return The class
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 **************************************************************************/
	private static Class getClass(String className, String parentPackage)
			throws MalformedURLException, ClassNotFoundException {

		// If short form is used, add the missing package name.
		if (!className.startsWith(parentPackage)) {
			className = parentPackage + "." + className;
		}

		// Get class with this classes class loader.
		return KolibriClassLoader.class.getClassLoader().loadClass(className);
	}

	/***************************************************************************
	 * <p>
	 * Returns an instance of the specified class with the specified parameters.
	 * It chooses the constructor with the correct number of arguments.
	 * </p>
	 * 
	 * TODO But what happens if we have more than one constructor with the same
	 * amount of parameters??
	 * 
	 * @param newClass
	 *            The class to get an instance of.
	 * @param parameters
	 *            The parameters of type String to be given to the constructor.
	 * @return Instance of the class. An explicit cast is necessary after this
	 *         method.
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws ClassNotFoundException
	 **************************************************************************/
	public static Object getInstanceOfClass(Class newClass, Object[] parameters)
			throws InstantiationException, IllegalAccessException,
			InvocationTargetException, ClassNotFoundException {

		// Get available constructors from class, if not null.
		if (newClass != null) {
			Constructor[] constructors = newClass.getConstructors();

			// Check all available constructors for the first constructor that
			// has the correct number of arguments.
			if (parameters.length == 0) {
				return newClass.newInstance();
			}
			for (int i = 0; i < constructors.length; i++) {
				if (constructors[i]
						.getParameterTypes().length == parameters.length) {
					return constructors[i].newInstance(parameters);
				}
			}
		} else {
			throw new ClassNotFoundException();
		}

		// We haven't found the appropriate constructor for our parameters. That
		// means the config file is not valid.
		throw new IllegalArgumentException();
	}

	/***************************************************************************
	 * <p>
	 * Returns an instance of the specified class without parameters.
	 * </p>
	 * 
	 * @param newClass
	 *            The class to get an instance of.
	 * @return Instance of the class. An explicit cast is necessary after this
	 *         method.
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws ClassNotFoundException
	 **************************************************************************/
	public static Object getInstanceOfClass(Class newClass)
			throws InstantiationException, IllegalAccessException,
			InvocationTargetException, ClassNotFoundException {
		return getInstanceOfClass(newClass, new Object[0]);
	}

}
