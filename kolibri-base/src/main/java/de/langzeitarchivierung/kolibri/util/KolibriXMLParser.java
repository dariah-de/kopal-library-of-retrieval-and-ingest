/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / KolibriXMLParser.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * 
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.logging.Logger;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/*******************************************************************************
 * Helper class to parse a XML file.
 * 
 * @author Kadir Karaca Koçer, German National Library
 * @version 20070205
 * @see de.langzeitarchivierung.kolibri.actionmodule.MetadataExtractorBase
 * @since 20050811
 ******************************************************************************/

public class KolibriXMLParser {

	protected static Logger	defaultLogger	= Logger
													.getLogger("de.langzeitarchivierung.kolibri");

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * Returns the DOM representation of a given XML file.
	 * 
	 * @version 20050811
	 * @param file
	 *            File to parse
	 * @return DOM representation of the file
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @see org.w3c.dom.Document
	 * @see javax.xml.parsers.DocumentBuilderFactory
	 * @see javax.xml.parsers.DocumentBuilder
	 **************************************************************************/
	public static Document getDocument(String file) throws SAXException,
			ParserConfigurationException, IOException {
		DocumentBuilderFactory dbf = null;
		DocumentBuilder db = null;
		Document doc = null;

		dbf = DocumentBuilderFactory.newInstance();
		db = dbf.newDocumentBuilder();
		db.setErrorHandler(null);

		// Parse the file.
		doc = db.parse(file);

		return doc;
	}

	/***************************************************************************
	 * Returns the DOM representation of a given XML string.
	 * 
	 * @author Jens Ludwig, SUB
	 * @version 20050901
	 * @param string
	 *            String to parse
	 * @return DOM representation of the string
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 * @see org.w3c.dom.Document
	 * @see org.xml.sax.InputSource
	 * @see javax.xml.parsers.DocumentBuilderFactory
	 * @see javax.xml.parsers.DocumentBuilder
	 * @since 20050901
	 **************************************************************************/
	public static Document getDocumentFromString(String string)
			throws ParserConfigurationException, SAXException, IOException {
		InputSource inputSource = new InputSource(new StringReader(string));
		DocumentBuilderFactory dbf = null;
		DocumentBuilder db = null;
		Document doc = null;

		dbf = DocumentBuilderFactory.newInstance();

		// Needed because not all dias responses have an inline DTD.
		dbf.setValidating(false);
		db = dbf.newDocumentBuilder();

		// Needed because not all DIAS responses have an inline DTD and
		// because XML parsers still need the DTD although validation is
		// turned off.
		db.setEntityResolver(new EntityResolver() {
			public InputSource resolveEntity(String publicId, String systemId) {
				return new InputSource(new ByteArrayInputStream(new byte[0]));
			}
		});
		// Parse the string.
		doc = db.parse(inputSource);

		return doc;
	}

	/***************************************************************************
	 * Stores a DOM Document to a file. At the moment stores only in charset
	 * encoding UTF-8 and XHTML.
	 * 
	 * TODO Make that configurable!
	 * 
	 * TODO Or much better: Store as Read!
	 * 
	 * @param doc
	 * @param file
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 **************************************************************************/
	public static void storeDocument(Document doc, File file) {
		Source source = new DOMSource(doc);
		Result result = new StreamResult(file);

		Transformer transformer;
		try {
			transformer = TransformerFactory.newInstance().newTransformer();

			// Set some transformer properties. We store ALL HTML files in XHTML
			// and with charset UTF-8!
			// TODO Use the attributes of the given HTML file!
			transformer.setOutputProperty("method", "xml");
			transformer.setOutputProperty("encoding", "UTF-8");
			transformer.setOutputProperty("indent", "yes");
			transformer.setOutputProperty("omit-xml-declaration", "no");

			transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/***************************************************************************
	 * Returns a NodeList which contains only the direct sub nodes with the
	 * specified name.
	 * 
	 * @author Jens Ludwig, SUB
	 * @version 20050902
	 * @param parentNode
	 *            The sub nodes of this node are examined
	 * @param queriedNodeName
	 *            The name of the nodes you want
	 * @return NodeList contains all nodes with the specified name
	 * @see org.w3c.dom.Document
	 * @see org.w3c.dom.Element
	 * @see org.w3c.dom.NodeList
	 **************************************************************************/
	public static NodeList getDirectSubNodes(Node parentNode,
			String queriedNodeName) {
		return new TypedNodeList(parentNode, queriedNodeName);
	}

	/***************************************************************************
	 * Returns the elementvalue for the given input param elementName.
	 * 
	 * @param nodelist
	 *            List to search
	 * @return Value of the element
	 * @see org.w3c.dom.Document
	 * @see org.w3c.dom.Element
	 * @see org.w3c.dom.NodeList
	 **************************************************************************/
	public static String getFirstValue(NodeList nodelist) {
		String value = "";

		try {
			if (null != nodelist) {
				Element elem = (Element) nodelist.item(0);
				if (null != elem) {
					Node namechild = elem.getFirstChild();
					if (null != namechild)
						value = namechild.getNodeValue();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return value;
	}

	/***************************************************************************
	 * Returns the elementvalue for the given input param elementName.
	 * 
	 * @param doc
	 *            Document to search
	 * @param elementName
	 *            The name of the searched element
	 * @return Value of the element
	 * @see org.w3c.dom.Document
	 * @see org.w3c.dom.Element
	 * @see org.w3c.dom.NodeList
	 **************************************************************************/
	public static String getFirstValue(Document doc, String elementName) {
		String value = "";

		try {
			NodeList nodelist = doc.getElementsByTagName(elementName);
			if (null != nodelist) {
				Element elem = (Element) nodelist.item(0);
				if (null != elem) {
					org.w3c.dom.Node namechild = elem.getFirstChild();
					if (null != namechild)
						value = namechild.getNodeValue();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return value;
	}

	/***************************************************************************
	 * TODO Test this validation aginst a given XML schema! Doesn't work yet!
	 * 
	 * @param xmlData
	 * @param schemaURL
	 **************************************************************************/
	public static void validate(final String xmlData, final String schemaURL) {
		try {
			InputStream schema = new ByteArrayInputStream(schemaURL.getBytes());
			InputStream in = new ByteArrayInputStream(xmlData.getBytes());

			// FIXME Is XML_NS_URI correct? Was W3C_XML_SCHEMA_NS_URI before!
			SchemaFactory sf = SchemaFactory
					.newInstance(XMLConstants.XML_NS_URI);
			Schema theSchema = sf.newSchema(new SAXSource(new InputSource(
					schema)));

			Validator validator = theSchema.newValidator();
			validator.validate(new SAXSource(new InputSource(in)));

			System.out.println("Schema compiled and valid.");
		} catch (Exception e) {
			System.out.println("Error validating. System message: "
					+ e.getMessage());
		}
	}

	/***************************************************************************
	 * Contains the sub nodes of a specified type. The main difference to
	 * getElementsByTagName() is that it only looks for the direct sub nodes.
	 * 
	 * @author Ludwig
	 * @version 20060224
	 * @since 20060224
	 **************************************************************************/

	static class TypedNodeList implements NodeList {

		// STATE (Instance variables) *****************************************

		private Node[]	nodes	= null;

		// CREATION (Constructors, factory methods, static/inst init) **********

		/***********************************************************************
		 * Constructor.
		 * 
		 * @param node
		 *            The node whose childs should be tested.
		 * @param type
		 *            The nodeName of the sub nodes which will be returned.
		 **********************************************************************/

		TypedNodeList(Node node, String type) {
			super();
			int numberOfNodes = 0;

			if (!node.hasChildNodes()) {
				this.nodes = new Node[0];
			} else {
				NodeList nodeList = node.getChildNodes();
				// For each child node.
				for (int i = 0; i < nodeList.getLength(); i++) {
					// If it is of the specified type add one to the length of
					// the array.
					if (nodeList.item(i).getNodeName().equals(type)) {
						numberOfNodes++;
					}
				}

				this.nodes = new Node[numberOfNodes];
				int nodeToWrite = 0;

				for (int i = 0; i < nodeList.getLength(); i++) {
					// If it is of the specified type add it to the array.
					if (nodeList.item(i).getNodeName().equals(type)) {
						this.nodes[nodeToWrite] = nodeList.item(i);
						nodeToWrite++;
					}
				}
			}
		}

		// GET & SET METHODS **************************************************

		/***********************************************************************
		 * @see org.w3c.dom.NodeList#getLength()
		 **********************************************************************/
		public int getLength() {
			return this.nodes.length;
		}

		// QUERIES (Queries - no change to object's state) ********************

		/***********************************************************************
		 * @see org.w3c.dom.NodeList#item(int)
		 **********************************************************************/
		public Node item(int arg0) {
			return this.nodes[arg0];
		}
	}

}
