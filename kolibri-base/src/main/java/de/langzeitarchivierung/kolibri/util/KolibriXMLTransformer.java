/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / KolibriXMLTransformer.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * 
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.util;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/*******************************************************************************
 * Helper class with methods for xml transformation.
 * 
 * @author Ludwig
 * @version 20060227
 ******************************************************************************/
public class KolibriXMLTransformer {

	// CLASS VARIABLES (Static variables) **********************************

	private static String				data_character_encoding	= "UTF-8";
	private static TransformerFactory	tFac;
	static {
		tFac = TransformerFactory.newInstance();
	}

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * Transforms input XML-Tree to output stream.
	 * 
	 * @param input
	 *            The DOMSource of the XML-Tree to transform
	 * @param output
	 *            The StreamResult of the transformation *
	 * @throws TransformerException
	 **************************************************************************/
	public static void outputXMLTree(DOMSource input, StreamResult output)
			throws TransformerException {
		Transformer transformer = KolibriXMLTransformer.tFac.newTransformer();
		transformer.setOutputProperty(OutputKeys.ENCODING,
				data_character_encoding);
		transformer.transform(input, output);
	}

	/***************************************************************************
	 * Transforms input XML-Tree to output stream.
	 * 
	 * @param input
	 *            The DOMSource of the XML-Tree to transform
	 * @param output
	 *            The StreamResult of the transformation
	 * @param transformerSource
	 *            The transformer to use
	 * @throws TransformerException
	 **************************************************************************/
	public static void outputXMLTree(DOMSource input, StreamResult output,
			Source transformerSource) throws TransformerException {
		// tFac.setAttribute("debug", new Boolean(true));
		Transformer transformer = KolibriXMLTransformer.tFac
				.newTransformer(transformerSource);
		transformer.setOutputProperty(OutputKeys.ENCODING,
				data_character_encoding);
		transformer.transform(input, output);
	}

	/***************************************************************************
	 * @return Returns the data_character_encoding.
	 **************************************************************************/

	public static String getData_character_encoding() {
		return data_character_encoding;
	}

	/***************************************************************************
	 * @param encoding
	 *            The data_character_encoding to set.
	 **************************************************************************/

	public static void setData_character_encoding(String encoding) {
		KolibriXMLTransformer.data_character_encoding = encoding;
	}

}
