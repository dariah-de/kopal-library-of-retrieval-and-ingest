/*******************************************************************************
 * de.langzeitarchivierung.kolibri.util / UnpackUtils.java
 * 
 * Copyright 2005-2009 by Project kopal
 * Copyright 2010 by Project DP4lib
 * 
 * http://kopal.langzeitarchivierung.de
 * http://dp4lib.langzeitarchivierung.de
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * ******************************************************************************/
package de.langzeitarchivierung.kolibri.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/*******************************************************************************
 * <p>
 * Some usefull routines to use to unpack tar or zip.
 * </p>
 * 
 * <p>
 * PLEASE NOTE: All the index directory methods are implemented to run on an
 * APACHE index directory!
 * </p>
 * 
 * @author Virginie werb
 * @version 20110718
 * @since 20110722
 ******************************************************************************/
public class UnpackUtils {
	/**
	 * Extract ZIP file to destination folder.
	 * 
	 * @param file
	 *            ZIP file to extract
	 * @param destination
	 *            destination directory
	 */
	public static void extractZIP( File  unpackFile, File destFolder) throws IOException, ZipException {
		if (isValid(unpackFile)) {

			ZipInputStream in = null;
			OutputStream out = null;
			System.out.println("zip is valid");
				
			// Open the ZIP file
			in = new ZipInputStream(new FileInputStream(unpackFile));
			// Get the first entry
			ZipEntry entry = null;

			while ((entry = in.getNextEntry()) != null) {
				String outFilename = entry.getName();

				// Open the output file
				if (entry.isDirectory()) {
					new File(destFolder, outFilename).mkdirs();
				}
				else {
					out = new FileOutputStream(new File(destFolder,outFilename));

					// Transfer bytes from the ZIP file to the output file
					byte[] buf = new byte[1024];
					int len;

					while ((len = in.read(buf)) > 0) {
						out.write(buf, 0, len);
					}
					System.out.println("transfer fertig");
	
					// Close the stream
					out.close();
				}
			}

			// Close the stream
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
		else {
			System.out.println("zip is NOT valid");
			throw new IOException();
		}
	}

	/**
	 * Extract TAR file to destination folder.
	 * 
	 * @param file
	 *            TAR file to extract
	 * @param destination
	 *            destination directory
	 */

	public static void extractTAR(File unpackFile, File destFolder) throws IOException {
	
		try {
			untar(unpackFile, destFolder);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * verify if a file is a valid ZIP File
	 * 
	 * @param file
	 *            ZIP file to verify
	 * 
	 *           
	 */

	private static boolean isValid(File file) {
		
		ZipFile zipfile = null;
		try {
			zipfile = new ZipFile(file);
			return true;
		}
		catch (ZipException e) {
			return false;
		}
		catch (IOException e) {
			return false;
		}
		finally {
			try {
				if (zipfile != null) {
					zipfile.close();
					zipfile = null;
				}
			}
			catch (IOException e) {
			}
		}
	}

	/***************************************************************************
	 * Untar a tar archive with destination as file object. 
	 * @param arcFile
	 *            The Archive to untar
	 * @param destDir
	 *            Destination directory
	 * @return Status code -> 0: Success, 1: destination Directory do not exists
	 **************************************************************************/
	public static void untar(java.io.File arcFile, java.io.File destDir)
			throws IOException {

		try {
			// Create the destination directory if it does not exist.
			destDir.mkdirs();
			// System.out.println("Reading the archive " + "file: "
			// + arcFile.getName());
			java.io.InputStream is = new java.io.FileInputStream(arcFile);

			// Use the tar-Library. Its GPL :-)
			// System.out.println("Buffering the contents");
			// com.ice.tar.TarArchive archive = new com.ice.tar.TarArchive(is,
			// BUFFER_SIZE);
			com.ice.tar.TarArchive archive = new com.ice.tar.TarArchive(is);
			// System.out.println("Extracting to: " + destDir.getName());
			// Extract the content.
			archive.extractContents(destDir);
			// Close the archive.
			archive.closeArchive();
			// System.out.println("TAR archive successfully extracted.");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
