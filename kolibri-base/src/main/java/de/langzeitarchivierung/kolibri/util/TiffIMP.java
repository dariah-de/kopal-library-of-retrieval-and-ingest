/**
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Stefan Hein, DNB
 */

package de.langzeitarchivierung.kolibri.util;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageInputStream;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import com.sun.media.imageio.plugins.tiff.TIFFDirectory;
import com.sun.media.imageio.plugins.tiff.TIFFField;
import com.sun.media.imageio.plugins.tiff.TIFFTag;
import edu.harvard.hul.ois.jhove.App;
import edu.harvard.hul.ois.jhove.JhoveBase;
import edu.harvard.hul.ois.jhove.JhoveException;
import edu.harvard.hul.ois.jhove.Message;
import edu.harvard.hul.ois.jhove.Module;
import edu.harvard.hul.ois.jhove.OutputHandler;
import edu.harvard.hul.ois.jhove.RepInfo;
import edu.harvard.hul.ois.jhove.handler.XmlHandler;

/**
 * TODOLOG
 * 
 * TODO Find a better solution for the initialization of the TiffIMP: Make the constructor working
 * without sending a JhoveBase object.
 * 
 * TODO Rewrite only the TIFF header metadata without rewriting the whole image.
 * 
 * TODO Add the path to the jhove config file to the cli.
 * 
 * TODO Get single TIFF tag values by tag name and by tag number.
 * 
 * TODO Make it possible to remove unknown TIFF tags.
 * 
 * TODO Don't use the commons.cli, the word wrapping is not nice at all.
 * 
 * TODO Check only the TIFF header metadata? (maybe without using Jhove).
 * 
 * TODO Check and repair single TIFF tags.
 * 
 * TODO Only correct the TIFF-ASCII fields if necessary.
 * 
 * TODO TIFF_ASCII verbose --> also use HEX values.
 * 
 * TODO TIFF_ASCII verbose --> use formatter.
 **
 * CHANGELOG
 *
 * 2016-05-10 - Funk Corrected Sonar issue.
 * 
 * 2011-08-17 - Funk - Added try/catch block for TIFF_LONG println.
 * 
 * 2010-09-17 - Funk - Version strings changed, partly adapted to Java 1.5., removed Exceptions,
 * added IOExceptions. 2010-05-25 Hein Adapted class to Jhove Version 1.5.
 * 
 * 2008-07-18 - Funk - Added .dissolve() to the image reader and writer, minor changes.
 * 
 * 2007-07-01 - koLibRI version 1.0
 */

/**
 * <p>
 * The TIFF Image Metadata Processor
 * </p>
 * 
 * <p>
 * This class is useful to validate TIFF images and process their header metadata. Use it to show,
 * validate and repair the TIFF header metadata.
 * </p>
 * 
 * @version 2019-11-20
 * @since 2007-01-18
 */

public class TiffIMP {

  // **
  // CONSTANTS
  // **

  public static final String VERSION = "The TIFF Image Metadata Processor v0.4.3";
  public static final String USAGE = "tiffimp [options]";

  private static final String JHOVE_NAME = "tiffimp";
  private static final String JHOVE_RELEASE = "1.5";
  private static final int[] JHOVE_DATE = {2010, 5, 25};
  private static final String JHOVE_USAGE =
      "Jhove is used here for TIFF image validation and metadata extraction only";
  private static final String JHOVE_RIGHTS = "(c)2010 by project DP4lib";
  private static final App JHOVE_APP =
      new App(TiffIMP.JHOVE_NAME, TiffIMP.JHOVE_RELEASE, TiffIMP.JHOVE_DATE, TiffIMP.JHOVE_USAGE,
          TiffIMP.JHOVE_RIGHTS);
  private static final String JHOVE_LOG_LEVEL = "NONE";

  // **
  // CLASS VARIABLES (Static variables)
  // **

  private static CommandLine commandLine = null;

  // **
  // STATE (Instance variables)
  // **

  private JhoveBase jhoveBase;
  private RepInfo repInfo;
  private IIOMetadata headerMetadata;
  private TIFFDirectory tiffDirectory;
  private RenderedImage imageData;
  private File inputFile;
  private File outputFile;
  private List<String> jhoveMessages = new ArrayList<String>();
  private String statusString = "";
  private boolean wellFormed = false;
  private boolean valid = false;
  private boolean quiet = false;
  private boolean verbose = false;
  private boolean showHeaderMetadata = false;
  private boolean rewriteTiffHeader = false;
  private boolean rewriteIfNotValid = false;
  private boolean jhoveXmlOutput = false;
  private boolean validateTiffImage = false;

  // **
  // CREATION (Constructors, factory methods, static/inst init)
  // **

  /**
   * 
   */
  public TiffIMP() {
    super();
  }

  /**
   * <p>
   * Please envoke the TiffIMP with the following parameters, if you need to use the Jhove:
   * </p>
   * 
   * <p>
   * For each TiffIMP instance that processes a bunch of TIFF images one after the other, please
   * invoke: <code>JhoveBase jb = TiffIMP.initializeTheJhove(jhoveConfigFile);</code>
   * </p>
   * 
   * <p>
   * Then create a new TiffIMP object for each image, submitting the JhoveBase:
   * <code>TiffIMP tiffImp = new TiffIMP(jb);</code>
   * </p>
   * 
   * @param jhoveBase
   */
  public TiffIMP(JhoveBase jhoveBase) {
    super();
    this.jhoveBase = jhoveBase;
  }

  /**
   * <p>
   * Constructor using a file only. Loads needed information at creation time. Use if you do not
   * need the Jhove.
   * </p>
   * 
   * @param file
   * @throws IOException
   */
  public TiffIMP(File file) throws IOException {
    super();
    this.inputFile = file;
    this.load();
  }

  /**
   * <p>
   * Constructor using the Jhove and a file. Loads needed information at creation time.
   * </p>
   * 
   * @param jhoveBase
   * @param file
   * @throws IOException
   */
  public TiffIMP(JhoveBase jhoveBase, File file) throws IOException {
    super();
    this.jhoveBase = jhoveBase;
    this.inputFile = file;
    this.load();
  }

  // **
  // MANIPULATION (Manipulation - what the object does)
  // **

  /**
   * <p>
   * The main class reads the command line and sets some attributes to work with later on.
   * </p>
   * 
   * @param args
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {
    // Measure time.
    long startTime = System.currentTimeMillis();

    // At first check the CLI options and load the file.
    TiffIMP tiffImp = checkCliOptions(args);

    try {
      // Print out version information and filename.
      if (!tiffImp.quiet) {
        System.out.println(VERSION);
        System.out.println();
        System.out.println(tiffImp.inputFile.getAbsolutePath());
      }

      // Show the header metadata and exit.
      if (tiffImp.showHeaderMetadata) {
        if (!tiffImp.quiet) {
          System.out.println("\nSHOW HEADER METADATA");
          printLongLine(tiffImp);
        }
        tiffImp.showHeaderMetadata();
      }

      // Validate the TIFF image and exit.
      else if (tiffImp.validateTiffImage) {
        if (!tiffImp.quiet) {
          System.out.println("\nVALIDATE TIFF IMAGE");
          printLongLine(tiffImp);
        }
        tiffImp.validate();
      }

      // Get the Jhove XML output and exit.
      else if (tiffImp.jhoveXmlOutput) {
        if (!tiffImp.quiet) {
          System.out.println("\nJHOVE XML OUTPUT");
          printLongLine(tiffImp);
        }
        tiffImp.jhoveXmlOutput();
      }

      // Rewrite the TIFF header and exit.
      else if (tiffImp.rewriteTiffHeader) {
        if (!tiffImp.quiet) {
          System.out.println("\nREWRITE HEADER METADATA");
          printLongLine(tiffImp);
        }
        tiffImp.correctHeaderMetadata();
        tiffImp.store();
        System.out.println("Stored " + tiffImp.outputFile.getAbsolutePath());
      }

      // Rewrite the TIFF header if the image is not valid according to
      // the Jhove.
      else if (tiffImp.rewriteIfNotValid) {
        if (!tiffImp.quiet) {
          System.out.println("\nREWRITE HEADER METADATA");
          printLongLine(tiffImp);
        }
        if (!tiffImp.validate()) {
          tiffImp.correctHeaderMetadata();
          tiffImp.store();
          System.out.println("Image stored: " + tiffImp.outputFile.getAbsolutePath());
        }
      }
    } catch (Exception e) {
      System.err.println("?UNKNOWN  ERROR\nREADY.\nSystem message: " + e.getMessage());
      e.printStackTrace();
      System.exit(1);
    }

    if (!tiffImp.quiet) {
      printLongLine(tiffImp);
      System.out.println("READY.");
    }

    // Print out duration.
    if (!tiffImp.quiet) {
      System.out.println("\nDuration: "
          + KolibriTimestamp.getDurationInHours(System.currentTimeMillis() - startTime));
    }
  }

  /**
   * <p>
   * Loads the TIFF image and prints out all the existing TIFF header metadata.
   * </p>
   * 
   * @throws FileNotFoundException
   */
  public void showHeaderMetadata() {
    TIFFField tiffFieldArray[];
    TIFFField tiffField;
    String valueTab = "";

    // Get the TIFFField array.
    tiffFieldArray = this.tiffDirectory.getTIFFFields();

    // Walk throught all TIFF fields.
    for (int i = 0; i < tiffFieldArray.length; i++) {
      tiffField = tiffFieldArray[i];

      // Output tag type, number and name, prepare the value output.
      System.out.println(tiffField.getTag().getNumber() + ": " + tiffField.getTag().getName() + " ("
          + TIFFField.getTypeName(tiffField.getType()) + ")");
      valueTab = "";
      for (int k = 0; k < Integer.toString(tiffField.getTag().getNumber()).length(); k++) {
        valueTab += " ";
      }
      valueTab += "  ";

      // Walk throught all TIFF field values and print them out.
      for (int j = 0; j < tiffField.getCount(); j++) {
        System.out.print(valueTab);

        switch (tiffField.getType()) {
          case TIFFTag.TIFF_BYTE:
            System.out.println(tiffField.getAsInt(j));
            break;
          case TIFFTag.TIFF_ASCII:
            String result = tiffField.getAsString(j);
            System.out.println(result);

            // Print all character values. Filter the codes 10 and
            // 13.
            if (this.verbose) {
              System.out.print(valueTab);
              for (int k = 0; k < result.length(); k++) {
                if (result.charAt(k) == 13) {
                  System.out.print("(C13 13) ");
                } else if (result.charAt(k) == 10) {
                  System.out.print("(C10 10) ");
                } else {
                  System.out.print("[" + result.charAt(k) + " " + (int) result.charAt(k) + "] ");
                }
              }
              System.out.println();
            }
            break;
          case TIFFTag.TIFF_SHORT:
            System.out.println(tiffField.getAsInt(j));
            break;
          case TIFFTag.TIFF_LONG:
            try {
              System.out.println(tiffField.getAsLong(j));
            } catch (Exception e) {
              System.out.println("TIFF_LONG throws exception: " + e.getMessage());
            }
            break;
          case TIFFTag.TIFF_RATIONAL:
            long[] rational = tiffField.getAsRational(j);
            System.out.println(rational[0] + "/" + rational[1]);
            break;
          case TIFFTag.TIFF_FLOAT:
            System.out.println(tiffField.getAsFloat(j));
            break;
          case TIFFTag.TIFF_DOUBLE:
            System.out.println(tiffField.getAsDouble(j));
            break;
          case TIFFTag.TIFF_UNDEFINED:
            System.out.println("TIFF_UNDEFINED");
            break;
          default:
            System.out.println("?TAG TYPE DOES NOT EXIST  " + "ERROR\nREADY.\n");
        }
      }
    }
  }

  /**
   * <p>
   * Rewrites the TIFF image header metadata.
   * </p>
   * 
   * @return The amount of corrected tags.
   */
  public int correctHeaderMetadata() {
    int tagsCorrected = 0;
    TIFFField tiffField;
    TIFFField correctedTiffField;
    String tagName;
    int tagNumber;
    ArrayList<TIFFField> tagsToAdd = new ArrayList<TIFFField>();

    // Get the TIFFField array.
    TIFFField fields[] = this.tiffDirectory.getTIFFFields();

    for (int i = 0; i < fields.length; i++) {
      tiffField = fields[i];

      // Get field name and number.
      tagName = tiffField.getTag().getName();
      tagNumber = tiffField.getTag().getNumber();

      // TODO Do take care of ALL values, not just the first!!

      // Look into TIFF tags 297 (PageNumber), correct if only one value of the two values needed is
      // given here.
      if (tagNumber == 297 && tiffField.getType() == TIFFTag.TIFF_SHORT
          && tiffField.getCount() < 2) {
        char[] value = tiffField.getAsChars();

        // Create a new TIFF field with the old name and number.
        char[] neededValue = {value[0], 0};
        TIFFTag newTag = new TIFFTag(tagName, tagNumber, (1 << TIFFTag.TIFF_SHORT));
        correctedTiffField = new TIFFField(newTag, TIFFTag.TIFF_SHORT, 2, neededValue);

        // Put the new TIFF field into the list of tags to add.
        tagsToAdd.add(correctedTiffField);

        // Do some logging.
        if (!this.quiet) {
          System.out.println("Tag " + tagNumber
              + " corrected: Added missing total number of pages value and set to 0.");
        }
        tagsCorrected++;
      }

      // Look into TIFF tag 305 (Software), add some processing information and correct the maybe
      // wrong word-alignedness of the tag by rewriting it later.
      if (tagNumber == 305 && tiffField.getType() == TIFFTag.TIFF_ASCII) {
        // Get the content of the TIFF field and trim it.
        String value = tiffField.getAsString(0).trim();

        // Add some processing data to the tag.
        value += " [corrected by " + VERSION + " using com.sun.media.imageio.plugins.tiff.*]";

        // Create a new TIFF field with the old name and number.
        String[] _value = {value};
        TIFFTag newTag = new TIFFTag(tagName, tagNumber, (1 << TIFFTag.TIFF_ASCII));
        correctedTiffField = new TIFFField(newTag, TIFFTag.TIFF_ASCII, 1, _value);

        // Put the new TIFF field into the list of tags to add.
        tagsToAdd.add(correctedTiffField);

        // Do some logging.
        if (!this.quiet) {
          System.out.println("Tag " + tagNumber
              + " newly written: Word-alignedness corrected if necesarry, added some software information.");
        }
        tagsCorrected++;
      }
    }

    // Remove the old TIFF fields and add the newly created ones.
    if (!tagsToAdd.isEmpty()) {
      for (TIFFField field : tagsToAdd) {
        this.tiffDirectory.removeTIFFField(field.getTag().getNumber());
        this.tiffDirectory.addTIFFField(field);
      }
    }

    return tagsCorrected;
  }

  /**
   * <p>
   * Get the Jhove output as XML.
   * </p>
   * 
   * @param file The file name to examine.
   * @throws Exception
   */
  public void jhoveXmlOutput() throws Exception {
    try {
      // Make some definitions.
      String[] imageFileArray = {this.inputFile.getAbsolutePath()};
      Module module = this.jhoveBase.getModule("TIFF-hul");
      OutputHandler about = this.jhoveBase.getHandler("XML");
      OutputHandler handler = this.jhoveBase.getHandler("XML");

      // Envoke the Jhove dispatcher to get the XML output. (app, module, about, handler,
      // outputFile, imageFileArray).
      this.jhoveBase.dispatch(JHOVE_APP, module, about, handler, null, imageFileArray);
    } catch (Exception e) {
      throw new Exception("Error dispatching the JhoveBase object. Please report to sickbay!");
    }
  }

  /**
   * <p>
   * Validates a TIFF image and stores the RepInfo object.
   * </p>
   * 
   * @return
   * @throws Exception
   */
  public boolean validate() throws Exception {
    this.repInfo = new RepInfo(this.inputFile.getAbsolutePath());
    Module module = this.jhoveBase.getModule("TIFF-hul");

    if (module == null) {
      throw new Exception("The TIFF-hul module could not be found");
    }

    XmlHandler xmlHandler = new XmlHandler();

    // If the XML handler can process the file...
    if (xmlHandler.okToProcess(this.inputFile.getAbsolutePath())) {
      // ...read the date of the last modification.
      this.repInfo.setLastModified(new Date(this.inputFile.lastModified()));

      // Invoke the TIFF-hul module, of course it can validate!
      if (module.hasFeature("edu.harvard.hul.ois.jhove.canValidate")) {
        try {
          this.jhoveBase.processFile(JHOVE_APP, module, true, this.inputFile, this.repInfo);
        } catch (Exception e) {
          throw new Exception("The module " + module + " is not capable of validation");
        }
      }
    }

    // Set the statusString value of this TiffImp.
    if (this.repInfo.getWellFormed() == RepInfo.TRUE) {
      this.statusString = "well-formed";
    } else {
      this.statusString = "not well-formed";
    }
    if (this.repInfo.getValid() == RepInfo.TRUE) {
      this.statusString += " and valid";

    } else {
      this.statusString += " and not valid";
    }

    // Print out Jhove validity information.
    if (!this.quiet) {
      this.printValidationInformation();
    }

    // Return false if not well-formed or not valid...
    if (this.repInfo.getWellFormed() == RepInfo.FALSE || this.repInfo.getValid() == RepInfo.FALSE) {
      if (!this.quiet && !this.rewriteIfNotValid) {
        System.out.println("\nTry the -r flag! Engage!");
      }

      return false;
    }

    // ...and return TRUE otherwise.
    return true;
  }

  /**
   * <p>
   * Stores a given TIFF image including it's technical metadata.
   * </p>
   * 
   * @throws IOException
   */
  public void store() throws IOException {
    ImageWriter imageWriter = null;

    // Get the core TIFF writer.
    Iterator<ImageWriter> writerIterator = ImageIO.getImageWritersByFormatName("tiff");

    // Look for a com.sun.imageio image writer
    while (writerIterator.hasNext()) {
      imageWriter = writerIterator.next();
      if (imageWriter.getClass().getName().startsWith("com.sun.imageio")) {
        // Break on finding the core provider.
        break;
      }
    }

    if (imageWriter == null) {
      throw new IOException("No com.sun.imageio ImageWriter found");
    }

    // Set the output stream to the file to store.
    imageWriter.setOutput(new FileImageOutputStream(this.outputFile));

    // Create an image from the rendered image and the newly created metadata.
    IIOImage newImage = new IIOImage(this.imageData, null, this.headerMetadata);

    // Write the image to the temp file.
    imageWriter.write(null, newImage, null);

    // Free the resource.
    imageWriter.dispose();
  }

  /**************************************************************************
   * @throws IOException
   */
  public void load() throws IOException {
    // Get the image reader.
    ImageReader imageReader = getImageReader(this.inputFile);

    // Set some things.
    this.headerMetadata = imageReader.getImageMetadata(0);
    this.tiffDirectory = TIFFDirectory.createFromMetadata(this.headerMetadata);
    this.imageData = imageReader.readAll(0, null).getRenderedImage();

    // Free the resource.
    imageReader.dispose();
  }

  // **
  // INTERNAL (Internal - implementation details, local classes, ...)
  // **

  /**
   * <p>
   * Just prints out some validation information.
   * </p>
   */
  private void printValidationInformation() {
    String format = this.repInfo.getFormat();
    String version = this.repInfo.getVersion();

    System.out.println("The file is " + this.statusString + ".");
    System.out.println("File format is " + (format != null ? format : "")
        + " and format version is " + (version != null ? version : "unknown") + ".");

    // Finally print all the Jhove messages.
    if (this.repInfo.getMessage().size() > 0) {
      System.out.println("Jhove messages:");
    }

    for (Object messageObject : this.repInfo.getMessage()) {
      Message m = (Message) messageObject;

      System.out.println("\t" + (m.getOffset() != -1 ? "<" + m.getOffset() + "> " : "")
          + m.getClass().getSimpleName() + " -> " + m.getMessage()
          + (m.getSubMessage() != null ? " -> " + m.getSubMessage() : ""));
    }
  }

  /**
   * <p>
   * Gets a TIFF image reader from a TIFF file.
   * </p>
   * 
   * @param file
   * @return A valid image reader for the given file.
   * @throws IOException
   */
  private static ImageReader getImageReader(File file) throws IOException {
    ImageInputStream inputStream = ImageIO.createImageInputStream(file);
    Iterator<ImageReader> readerIterator = ImageIO.getImageReaders(inputStream);
    ImageReader imageReader = null;

    // Check for ImageReaders.
    if (readerIterator == null || !readerIterator.hasNext()) {
      throw new IOException("No ImageReaders found");
    }

    // Look for a com.sun.media image reader.
    while (readerIterator.hasNext()
        && (imageReader == null || !imageReader.getClass().getName().startsWith("com.sun.media"))) {
      imageReader = readerIterator.next();
    }

    if (imageReader == null) {
      throw new IOException("No com.sun.media ImageReader found");
    }

    // Set the reader's input stream.
    imageReader.setInput(inputStream);

    return imageReader;
  }

  /**
   * <p>
   * Checks the command line options and sets some attributes to use later on.
   * </p>
   * 
   * @param args
   * @return
   * @throws IOException
   */
  private static TiffIMP checkCliOptions(String[] args) throws IOException {
    // Create a new TiffImp object.
    TiffIMP tiffImp =
        new TiffIMP(TiffIMP.initializeTheJhove(new File(JhoveBase.getConfigFileFromProperties())));

    // At first let us parse the command line options.
    CommandLineParser cliParser = new PosixParser();
    Options cliOptions = cliOptions();

    // If something goes wrong concerning the CLAs, complain and exit!
    try {
      commandLine = cliParser.parse(cliOptions, args);
    } catch (ParseException pe) {
      System.out.println("?SYNTAX  ERROR.\nREADY.\nSystem Message: " + pe + "\n");
      new HelpFormatter().printHelp(USAGE + " ", cliOptions);
      System.exit(1);
    }

    // Check for the required TIFF image file argument and get the filename.
    if (commandLine.getOptionValue('i') != null) {
      tiffImp.inputFile = new File(commandLine.getOptionValue('i'));

      if (tiffImp.inputFile.exists()) {
        tiffImp.load();
      } else {
        System.out.println("?FILE NOT FOUND  ERROR\nREADY.\n");
        System.exit(1);
      }
    }

    // Check command line options and set values.
    if (commandLine.hasOption('h')) {
      new HelpFormatter().printHelp(USAGE + " ", cliOptions);
      System.exit(0);
    }
    if (commandLine.hasOption('v')) {
      tiffImp.verbose = true;
      tiffImp.quiet = false;
    }
    if (commandLine.hasOption('q')) {
      tiffImp.quiet = true;
      tiffImp.verbose = false;
    }

    if (commandLine.hasOption('o')) {
      tiffImp.outputFile = new File(commandLine.getOptionValue('o'));
    } else {
      if (tiffImp.inputFile != null) {
        // It's string magic (hihi :-)
        tiffImp.outputFile = new File(tiffImp.inputFile.getAbsolutePath().substring(0,
            tiffImp.inputFile.getAbsolutePath().indexOf('.')) + ".COR"
            + tiffImp.inputFile.getAbsolutePath()
                .substring(tiffImp.inputFile.getAbsolutePath().indexOf('.')));
      }
    }

    if (commandLine.hasOption('f')) {
      tiffImp.outputFile = tiffImp.inputFile;
    }

    if (commandLine.hasOption('c')) {
      tiffImp.validateTiffImage = true;
    } else if (commandLine.hasOption('j')) {
      tiffImp.jhoveXmlOutput = true;
    } else if (commandLine.hasOption('r')) {
      tiffImp.rewriteTiffHeader = true;
    } else if (commandLine.hasOption('n')) {
      tiffImp.rewriteIfNotValid = true;
    } else {
      tiffImp.showHeaderMetadata = true;
    }

    // Finally test, if an input file is given. Complain if not.
    if (tiffImp.inputFile == null) {
      System.out.println("?NEED AN INPUT FILE  ERROR\n.READY.\n");
      new HelpFormatter().printHelp(USAGE + " ", cliOptions);
      System.exit(1);
    }

    return tiffImp;
  }

  /**
   * <p>
   * The command line interface options are created here.
   * </p>
   * 
   * @return The command line interface options.
   */
  private static Options cliOptions() {
    Options cliOptions = new Options();

    // Defines the CLI options using the following arguments: Short option string, long option
    // string, has arguments boolean, description string.
    Option _check = new Option("c", "validate-tiff-image", false,
        "Validates the TIFF image according to TIFF specification compliance using the Jhove (see http://hul.harvard.edu/jhove).");
    cliOptions.addOption(_check);

    Option _force = new Option("f", "force-overwrite", false,
        "Forces the input image file to be overwritten if header metadata shall be corrected.");
    cliOptions.addOption(_force);

    Option _help = new Option("h", "help", false, "Very surprising: A help message :-)");
    cliOptions.addOption(_help);

    Option _get_jhove_xml_output = new Option("j", "jhove-xml-output", false,
        "Outputs the Jhove XML output using the TIFF-hul module.");
    cliOptions.addOption(_get_jhove_xml_output);

    Option _rewriteIfNotValid = new Option("n", "rewrite-if-not-valid", false,
        "Only rewrites the TIFF header if the image file is not wellformed or not valid according to the Jhove.");
    cliOptions.addOption(_rewriteIfNotValid);

    Option _output = new Option("o", "output-filename", false,
        "The filename of the corrected TIFF image. Default is _cor.tif");
    _output.setArgs(1);
    _output.setArgName("file");
    cliOptions.addOption(_output);

    Option _input =
        new Option("i", "input-filename", false, "The filename of the TIFF image to process");
    _input.setArgs(1);
    _input.setArgName("file");
    cliOptions.addOption(_input);

    Option _quiet = new Option("q", "quiet", false, "Run tiffimp in quiet mode.");
    cliOptions.addOption(_quiet);

    Option _rewrite = new Option("r", "rewrite-tiff-header", false,
        "The TIFF header metadata will be rewritten. Please notice that only the following tasks are able to be repaired yet: (a) Word-alignedness of all TIFF ASCII tags. (b) PageNumber 297 0x0129 set to the valid TIF _SHORT count of 2 bytes.");
    cliOptions.addOption(_rewrite);

    Option _show = new Option("s", "show-header-metadata", false,
        "Outputs the TIFF image header metadata tags and their content to standard out. This is the default option.");
    cliOptions.addOption(_show);

    Option _verbose = new Option("v", "verbose", false, "Run tiffimp in verbose mode.");
    cliOptions.addOption(_verbose);

    return cliOptions;
  }

  /**
   * <p>
   * Initialize the Jhove engine.
   * </p>
   * 
   * @return Returns a JhoveBase object.
   * @throws IOException
   * @throws Exception
   */
  public static JhoveBase initializeTheJhove(File configFile)
      throws IOException {
    // Test if the Jhove config file is set and existing.
    if (configFile == null || !configFile.exists()) {
      throw new IOException("The Jhove config file is not set or does not exist");
    }

    String encoding = null;
    String tempDir = null;
    String saxClass;
    int bufferSize = -1;

    String logLevel = TiffIMP.JHOVE_LOG_LEVEL;
    boolean checksum = false;
    boolean showRaw = false;
    boolean signature = false;

    // Retrieve the configuration file.
    saxClass = JhoveBase.getSaxClassFromProperties();

    JhoveBase jb = null;

    try {
      jb = new JhoveBase();
      jb.setLogLevel(logLevel);
      jb.init(configFile.getAbsolutePath(), saxClass);

      if (encoding == null) {
        encoding = jb.getEncoding();
      }
      if (tempDir == null) {
        tempDir = jb.getTempDirectory();
      }
      if (bufferSize < 0) {
        bufferSize = jb.getBufferSize();
      }

      // Invoke the Jhove engine.
      jb.setEncoding(encoding);
      jb.setTempDirectory(tempDir);
      jb.setBufferSize(bufferSize);
      jb.setChecksumFlag(checksum);
      jb.setShowRawFlag(showRaw);
      jb.setSignatureFlag(signature);
    } catch (JhoveException e) {
      throw new IOException("Error initializing the Jhove");
    }

    return jb;
  }

  /**
   * <p>
   * Don't mention it.
   * </p>
   * 
   * @param the_long
   */
  private static void printLongLine(TiffIMP tiffimp) {
    for (int i = 0; i < tiffimp.getInputFile().getAbsolutePath().length(); i++) {
      System.out.print("-");
    }

    System.out.println();
  }

  // **
  // GET & SET METHODS
  // **

  /**
   * @return Returns the headerMetadata.
   */
  public IIOMetadata getHeaderMetadata() {
    return this.headerMetadata;
  }

  /**
   * @param headerMetadata The headerMetadata to set.
   */
  public void setHeaderMetadata(IIOMetadata headerMetadata) {
    this.headerMetadata = headerMetadata;
  }

  /**
   * @return Returns the imageData.
   */
  public RenderedImage getImageData() {
    return this.imageData;
  }

  /**
   * @param imageData The imageData to set.
   */
  public void setImageData(RenderedImage imageData) {
    this.imageData = imageData;
  }

  /**
   * @return Returns the inputFilename.
   */
  public File getInputFile() {
    return this.inputFile;
  }

  /**
   * @param inputFile The inputFile to set.
   */
  public void setInputFile(File inputFile) {
    this.inputFile = inputFile;
  }

  /**
   * @return Returns the outputFilename.
   */
  public File getOutputFile() {
    return this.outputFile;
  }

  /**
   * @param outputFilename The outputFilename to set.
   */
  public void setOutputFile(File outputFile) {
    this.outputFile = outputFile;
  }

  /**
   * @param quiet The quiet to set.
   */
  public void setQuiet(boolean quiet) {
    this.quiet = quiet;
  }

  /**
   * @param rewriteTiffHeader The rewriteTiffHeader to set.
   */
  public void setRewriteTiffHeader(boolean rewriteTiffHeader) {
    this.rewriteTiffHeader = rewriteTiffHeader;
  }

  /**
   * @param showHeaderMetadata The showHeaderMetadata to set.
   */
  public void setShowHeaderMetadata(boolean showHeaderMetadata) {
    this.showHeaderMetadata = showHeaderMetadata;
  }

  /**
   * @return Returns the tiffDirectory.
   */
  public TIFFDirectory getTiffDirectory() {
    return this.tiffDirectory;
  }

  /**
   * @param tiffDirectory The tiffDirectory to set.
   */
  public void setTiffDirectory(TIFFDirectory tiffDirectory) {
    this.tiffDirectory = tiffDirectory;
  }

  /**
   * @param validateTiffImage The validateTiffImage to set.
   */
  public void setValidateTiffImage(boolean validateTiffImage) {
    this.validateTiffImage = validateTiffImage;
  }

  /**
   * @param verbose The verbose to set.
   */
  public void setVerbose(boolean verbose) {
    this.verbose = verbose;
  }

  /**
   * @param jhoveXmlOutput The jhoveXmlOutput to set.
   */
  public void setJhoveXmlOutput(boolean jhoveXmlOutput) {
    this.jhoveXmlOutput = jhoveXmlOutput;
  }

  /**
   * @return Returns the jhoveMessages.
   */
  public List<String> getJhoveMessages() {
    return this.jhoveMessages;
  }

  /**
   * @return Returns the repInfo.
   */
  public RepInfo getRepInfo() {
    return this.repInfo;
  }

  /**
   * @return Returns the valid.
   */
  public boolean isValid() {
    return this.valid;
  }

  /**
   * @return Returns the wellFormed.
   */
  public boolean isWellFormed() {
    return this.wellFormed;
  }

  /**
   * @return Returns the statusString.
   */
  public String getStatusString() {
    return this.statusString;
  }

}
