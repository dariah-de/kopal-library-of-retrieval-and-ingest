/**
 * 
 * Copyright 2005-2020 by Project kopal(http://kopal.langzeitarchivierung.de)
 * 
 * Copyright 2010 by Project DP4lib (http://dp4lib.langzeitarchivierung.de)
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 */

/**
 * CHANGELOG
 * 
 * 2010-11-12 - Funk - Added database reference.
 * 
 * 2007-07-10 - koLibRI version 1.0
 */

package de.langzeitarchivierung.kolibri;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import de.langzeitarchivierung.kolibri.util.Configurator;

/**
 * <p>
 * Contains the SIPs.
 * </p>
 * 
 * TODO The ListDataListener methods are unused at the moment.
 * 
 * @author Ludwig
 * @version 2023-04-27
 * @since 2006-02-02
 */

public class ProcessQueue {

  // **
  // STATE (Instance variables)
  // **

  private static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");

  private boolean awaitingMoreProcesses = true;
  private BlockingQueue<ProcessData> pQueue;
  private List<ListDataListener> listDataListeners;

  // **
  // CREATION (Constructors, factory methods, static/inst init)
  // **

  /**
   * <p>
   * Constructor.
   * </p>
   */
  public ProcessQueue() {
    super();
    this.pQueue = new LinkedBlockingQueue<ProcessData>();
    this.listDataListeners = new ArrayList<ListDataListener>();
  }

  // **
  // GET & SET METHODS
  // **

  /**
   * <p>
   * Gets an iterator for the process list.
   * </p>
   * 
   * @return an iterator for the process list.
   */
  public Iterator<ProcessData> getProcessIterator() {
    return this.pQueue.iterator();
  }

  /**
   * <p>
   * Adds an element without the option of adding it to the database. Useful for adding an element
   * again which was already in the queue but did not finish.
   * </p>
   * 
   * @param pData The process to add.
   */
  public void addOldElement(ProcessData pData) {
    try {
      this.pQueue.put(pData);
    } catch (Exception e) {
      defaultLogger.log(Level.WARNING,
          "Error adding process: " + pData.getProcessName() + " Reason: " + e.getMessage());
    }
  }

  /**
   * <p>
   * Adds an element to the queue, setting its owner and source server to the default values.
   * </p>
   * 
   * @param pData The process to add.
   * @throws Exception
   */
  public void addElement(ProcessData pData) throws Exception {
    try {
      this.addElement(pData,
          (String) Configurator.getValue("DefaultOwner"),
          (String) Configurator.getValue("DefaultServer"));
    } catch (Exception e) {
      throw new Exception(e.getMessage());
    }
  }

  /**
   * <p>
   * Adds an element to the queue, setting its owner and source server to the specified values.
   * </p>
   * 
   * @param pData The process to add.
   * @param owner The owner identification of this process
   * @param server The server identification of this process
   * @throws Exception
   */
  public void addElement(ProcessData pData, String owner, String server)
      throws Exception {
    try {
      // if (getUseDatabase()) {
      // addInputInDb(pData, owner, server);
      // }
      this.pQueue.put(pData);
    } catch (Exception e) {
      throw new Exception(e.getMessage());
    }
  }

  /**
   * @return ProcessData - The next process data element or null if there are no more elements
   * @throws InterruptedException
   */
  protected ProcessData takeElement() throws InterruptedException {
    return this.pQueue.poll(1, TimeUnit.SECONDS);
  }

  // **
  // MANIPULATION (Manipulation - what the object does) ******************
  // **

  /**
   * <p>
   * Adds object which implements the listDataListener interface to notify list.
   * </p>
   * 
   * @param ldl listDataListener to be added
   * @see javax.swing.event.ListDataListener
   */
  public void addListDataListeners(ListDataListener ldl) {
    if (!this.listDataListeners.contains(ldl)) {
      this.listDataListeners.add(ldl);
    }
  }

  /**
   * <p>
   * Removes object which implement the listDataListener interface to notify list.
   * </p>
   * 
   * TODO Which access modifier? public (= action modules can call it) or package?
   * 
   * @param ldl listDataListener to be removed
   * @return true if removing succeeded, false otherwise
   * @see javax.swing.event.ListDataListener
   */
  public boolean removeListDataListeners(ListDataListener ldl) {
    return this.listDataListeners.remove(ldl);
  }

  /**
   * <p>
   * Notifies all registered listeners about the event
   * </p>
   * 
   * <p>
   * Access modifier needs to be public so that action modules can call it.
   * </p>
   * 
   * TODO 3 methods instead of 1? for each event type one?
   * 
   * @param lde the ListDataEvent to be passed to the listeners
   * @see javax.swing.event.ListDataListener
   * @see javax.swing.event.ListDataEvent
   */
  public void fireListDataEvent(ListDataEvent lde) {
    ListIterator<ListDataListener> listenerIterator = this.listDataListeners.listIterator();
    while (listenerIterator.hasNext()) {
      switch (lde.getType()) {
        case ListDataEvent.CONTENTS_CHANGED:
          ((ListDataListener) listenerIterator.next()).contentsChanged(lde);
          break;
        case ListDataEvent.INTERVAL_ADDED:
          ((ListDataListener) listenerIterator.next()).intervalAdded(lde);
          break;
        case ListDataEvent.INTERVAL_REMOVED:
          ((ListDataListener) listenerIterator.next()).intervalRemoved(lde);
          break;
        default:
          // TODO throw new exception
          break;
      }
    }
  }

  /**
   * <p>
   * Checks if there are not finished steps in any element build policy.
   * </p>
   * 
   * @return True if there are unfinished steps, false if everything is finished.
   * @see Status
   */
  public boolean hasUndone() {
    // TODO Synchronize this?
    synchronized (ProcessQueue.class) {
      Iterator<ProcessData> processIterator = this.getProcessIterator();
      Policy policy;

      while (processIterator.hasNext()) {
        policy = ((ProcessData) processIterator.next()).getPolicy();
        if (policy.hasOtherStatusThan(Status.DONE)) {
          return true;
        }
      }

      return false;
    }
  }

  /**
   * @return boolean - true if this queue contains no more elements
   */
  public boolean isEmpty() {
    return this.pQueue.isEmpty();
  }

  /**
   * <p>
   * Checks if there are steps with the status todo in any process build policy which can be reached
   * with only DONE or RUNNING steps. That are the steps which can be reached without further user
   * interaction.
   * </p>
   * 
   * @return True if there are reachable todo steps, false otherwise.
   * @see Status
   */
  public boolean hasReachableTodo() {
    synchronized (ProcessQueue.class) {
      // TODO synchronize this?
      Iterator<ProcessData> processIterator = this.getProcessIterator();
      Policy policy;

      while (processIterator.hasNext()) {
        policy = ((ProcessData) processIterator.next()).getPolicy();
        if (policy.hasStatusWithParent(Status.TODO, Status.DONE)) {
          return true;
        }
      }

      return false;
    }
  }

  /**
   * TODO Check that this allows re-adding failed/rescheduled processes.
   */
  public void shutdownProcessQueue() {
    this.awaitingMoreProcesses = false;
  }

  /**
   * @return Returns the awaitsMoreProcesses.
   */
  public boolean isAwaitingMoreProcesses() {
    return this.awaitingMoreProcesses;
  }

}
