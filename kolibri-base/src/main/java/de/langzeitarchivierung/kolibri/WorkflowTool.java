/**
 * Copyright
 * 
 * 2005-2009 by kopal (http://kopal.langzeitarchivierung.de)
 * 
 * 2010-2011 by DP4lib (http://dp4lib.langzeitarchivierung.de)
 * 
 * 2011-2015 by TextGrid (https://textgrid.de)
 * 
 * 2015-2023 by DARIAH-DE (https://de.dariah.eu)
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 * 
 **/

package de.langzeitarchivierung.kolibri;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.formats.MetadataFormat;
import de.langzeitarchivierung.kolibri.processstarter.ProcessStarter;
import de.langzeitarchivierung.kolibri.ui.TestListDataListener;
import de.langzeitarchivierung.kolibri.util.Configurator;
import de.langzeitarchivierung.kolibri.util.KolibriClassLoader;
import de.langzeitarchivierung.kolibri.util.KolibriConsoleLogFormatter;
import de.langzeitarchivierung.kolibri.util.KolibriFileLogFormatter;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import de.langzeitarchivierung.kolibri.util.KolibriXMLParser;

/**
 * <p>
 * WorkflowTool is the main class of the koLibRI workflow framework. It can be used to e.g. commit
 * SIPs to the Client Loader and to commit SIPs to DIAS or another archive, either split up into two
 * tools (Workflow Tool, Client Loader) or as a standalone tool.
 * </p>
 * 
 * <p>
 * NOTE: Please use the databases only with a single instance of the WorkflowTool, because a
 * concurrent usage of the database is not yet tested!
 * </p>
 * 
 * @author Jens Ludwig, SUB Göttingen
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2023-04-27
 * @since 2005-06-23
 **/

public class WorkflowTool {

  // **
  // CONSTANTS
  // **

  private static final String DEFAULT_CONFIG_FILE = "../config/config.xml";
  public static final int NUMBER_OF_TEST_LISTENERS = 1;
  public static final String KOLIBRI_VERSION = KolibriBaseVersion.VERSION;
  public static final String PROCESSSTARTER_ERROR_STRING =
      "Could not load and initialize process starters. Please check the commandline flag -p or the entry DefaultProcessStarter in the main config file! ";

  // **
  // CLASS VARIABLES (Static variables)
  // **

  public static CommandLine cLine = null;

  // **
  // STATE (Instance variables)
  // **

  private Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");
  private FileHandler fileLogHandler;
  private ConsoleHandler consoleLogHandler;
  private String logfileDir;
  private String logfileName = "kolibri.log";
  private String configfileName = DEFAULT_CONFIG_FILE;
  private String logLevel;
  private ArrayList<String> processStarters = new ArrayList<String>();
  private Level lLevel;
  // The maximum number of parallel threads per ProcessStarter.
  public int maxNumberOfThreads = 1;
  private ProcessThreadPool pBoss = null;
  private ProcessQueue processQueue;
  private ProcessQueue errorQueue;
  private long programStartTime;
  private String defaultCharset = "UTF-8";

  /**
   * <p>
   * Default constructor.
   * </p>
   */
  public WorkflowTool() {
    super();
  }

  // **
  // MANIPULATION (Manipulation - what the object does)
  // **

  /**
   * <p>
   * The Main method reads the command line (and additionally the config file), starts a process
   * starter to collect list elements to process and starts processing the list.
   * </p>
   * 
   * @param args The used command line arguments.
   */
  public static void main(String[] args) {

    // Command line parser parses the command line.
    CommandLineParser cliParser = new PosixParser();
    Options cliOptions = cliOptions();

    try {
      cLine = cliParser.parse(cliOptions, args);
    } catch (ParseException e) {
      System.out.println("koLibRI parse error: " + e);
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("WorkflowTool", cliOptions);
      System.exit(1);
    }

    // Print help informations.
    if (cLine.hasOption("h")) {
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("WorkflowTool", cliOptions);
      System.exit(0);
    }

    // Print system properties.
    if (cLine.hasOption("s")) {
      Properties p = System.getProperties();
      for (Enumeration<?> e = p.propertyNames(); e.hasMoreElements();) {
        Object po = e.nextElement();
        System.out.println("Name: " + po + " Value: " + p.getProperty((String) po));
      }
    }

    WorkflowTool woto = new WorkflowTool();

    // Use config file data from the command line or if not specified the default config file to
    // gather general configuration information.
    try {
      String configFileString = DEFAULT_CONFIG_FILE;
      if (cLine.hasOption("c")) {
        configFileString = cLine.getOptionValue("c");
      }

      woto.configure(configFileString);

    } catch (Exception e) {
      System.err.println("koLibRI configuration error: " + e.getMessage());
      System.exit(1);
    }

    try {
      woto.run();
    } catch (Exception e) {
      System.err.println("koLibRi error: " + e.getMessage());
      e.printStackTrace();
      System.exit(2);
    }
  }

  /**
   * <p>
   * The run() method runs the WorkflowTool.
   * </p>
   * 
   * @throws Exception
   */
  public void run() throws Exception {

    this.programStartTime = System.currentTimeMillis();

    this.startLogging();

    // Show config file location.
    this.defaultLogger.log(Level.INFO,
        "Config file: " + new File(this.configfileName).getCanonicalPath());

    // Set default charset to config file value.
    System.setProperty("file.encoding", this.defaultCharset);

    this.defaultLogger.log(Level.INFO, "Default charset: " + System.getProperty("file.encoding"));
    this.defaultLogger.log(Level.INFO,
        "Java VM charset: " + Charset.defaultCharset().displayName());
    this.defaultLogger.log(Level.INFO, "Root directory: " + new File(".").getAbsolutePath());
    this.defaultLogger.log(Level.INFO,
        "koLibRI version " + KolibriBaseVersion.VERSION + " \"" + KolibriBaseVersion.BUILDNAME
            + "\" (" + KolibriBaseVersion.BUILDDATE + ") started at "
            + KolibriTimestamp.getTimestampWithMs(this.programStartTime) + " ("
            + this.programStartTime + " millis)");

    this.createProcessQueue();
    this.addListListeners();
    this.startProcessStarter();
    this.process();

    // TODO Remove log handlers, does it make sense??
    // Plus --> Nothing is logged multiple times anymore during JUnit tests!
    // Minus --> Logging stops after a while?!
    // this.defaultLogger.log(Level.INFO, "*removing log handlers*");
    // this.defaultLogger.removeHandler(this.consoleLogHandler);
    // this.defaultLogger.removeHandler(this.fileLogHandler);

    this.defaultLogger.log(Level.INFO, "*stop*");
  }

  /**
   * <p>
   * Defines the available command line options.
   * </p>
   * 
   * @return the options
   */
  private static Options cliOptions() {

    Options cliOptions = new Options();
    cliOptions.addOption("p", "process-starter", true,
        "The process starter module which chooses items for processing.");
    cliOptions.addOption("c", "config-file", true, "The config file to use.");
    cliOptions.addOption("h", "help", false, "Prints this dialog");
    cliOptions.addOption("s", "show-properties", false,
        "Prints the system properties and continues.");

    return cliOptions;
  }

  /**
   * <p>
   * Processes the process list.
   * </p>
   * 
   * @throws Exception
   */
  private void process() throws Exception {

    this.defaultLogger.log(Level.INFO, "Process queue processing in progress");

    try {
      // As long as we will receive more tasks we execute them and wait for new ones.
      ProcessData processData = null;
      while (this.processQueue.isAwaitingMoreProcesses() || !this.processQueue.isEmpty()) {

        processData = this.processQueue.takeElement();

        if (processData != null) {
          this.defaultLogger.log(Level.FINE, "Scheduling process: " + processData.getProcessName());

          synchronized (processData) {
            this.pBoss.execute(processData);
          }

          this.defaultLogger.log(Level.FINE, "Waiting for next process");
        }
      }

      // All processes are finished, so we shutdown our work.
      this.pBoss.shutdown();

      this.defaultLogger.log(Level.FINER, "Shutting down ProcessThreadPool");

      this.pBoss.awaitTermination(this.pBoss.getTaskCount() * 1000, TimeUnit.SECONDS);

      this.defaultLogger.log(Level.FINER, "ProcessThreadPool shutdown complete");

    } catch (

    InterruptedException e) {
      e.printStackTrace();
      this.defaultLogger.log(Level.SEVERE,
          "Error shutting down ProcessThreadPool: " + e.getMessage());
    }

    // If there are no more processes to be delivered by the process starter the program exits and
    // displays the total time elapsed and writes it to the logfile.
    long totalTimeElapsed = System.currentTimeMillis() - this.programStartTime;

    this.defaultLogger.log(Level.INFO, "The process queue has been processed. Total time elapsed: "
        + KolibriTimestamp.getDurationInHours(totalTimeElapsed));

    // If error queue is not empty, tell me!
    String logfileLocation = new File(this.logfileName).getCanonicalPath();

    String failedProcesses = "[";
    Iterator<ProcessData> i = this.errorQueue.getProcessIterator();
    while (i.hasNext()) {
      ProcessData pd = i.next();
      failedProcesses += pd.processName + " ";
    }
    failedProcesses = failedProcesses.trim() + "]";

    if (!this.errorQueue.isEmpty()) {
      this.defaultLogger.log(Level.SEVERE,
          "Not everything has finished correctly! Failed processes: " + failedProcesses
              + ". Please check the logfile: " + logfileLocation);
    } else {
      this.defaultLogger.log(Level.INFO,
          "Everything has been done! This process was logged to file: " + logfileLocation);
    }
  }

  // **
  // INTERNAL (Internal - implementation details, local classes, ...)
  // **

  /**
   * <p>
   * Initialise the thread pool. Use the amount of ProcessStarters plus the max numbers of threads
   * from the configuration file per ProcessStarter, e.g. for three ProcessStarters and two threads
   * per ProcessStarter there will be nine threads allowed.
   * </p>
   */
  private void initializeThreadPool(int amountOfProcessStarters) {

    this.pBoss = new ProcessThreadPool(
        amountOfProcessStarters + (amountOfProcessStarters * this.maxNumberOfThreads));
    this.pBoss.setProcessQueue(this.processQueue);
    this.pBoss.setErrorQueue(this.errorQueue);

    this.defaultLogger.log(Level.FINE,
        "Number of ProcessStarters: " + amountOfProcessStarters
            + ", number of threads/ProcessStarter: "
            + (amountOfProcessStarters * this.maxNumberOfThreads) + ", number of threads: "
            + (amountOfProcessStarters + (amountOfProcessStarters * this.maxNumberOfThreads)));
  }

  /**
   * <p>
   * Start the process starters.
   * </p>
   * 
   * @throws MalformedURLException
   * @throws InvocationTargetException
   * @throws ClassNotFoundException
   * @throws InstantiationException
   * @throws IllegalAccessException
   */
  private void startProcessStarter() throws MalformedURLException, InvocationTargetException,
      ClassNotFoundException, InstantiationException, IllegalAccessException {

    ArrayList<ProcessStarter> pStarters = new ArrayList<ProcessStarter>();

    try {
      Class<?> pStarterClass = null;

      // Use the process starter specified in command line.
      if (cLine != null && cLine.hasOption("p")) {
        pStarterClass = KolibriClassLoader.getProcessStarter(cLine.getOptionValue("p"));
        pStarters.add((ProcessStarter) KolibriClassLoader.getInstanceOfClass(pStarterClass));
      }

      // Or use the default process starters from the config file.
      else {
        for (Iterator<String> pIter = this.processStarters.iterator(); pIter.hasNext();) {
          pStarterClass = KolibriClassLoader.getProcessStarter(pIter.next());
          pStarters.add((ProcessStarter) KolibriClassLoader.getInstanceOfClass(pStarterClass));
        }
      }
    } catch (MalformedURLException | ClassNotFoundException | InstantiationException
        | IllegalAccessException | InvocationTargetException e) {
      this.defaultLogger.log(Level.SEVERE,
          PROCESSSTARTER_ERROR_STRING + this.processStarters + ". System message: " + e);
      throw e;
    }

    // Initialise the thread pool.
    initializeThreadPool(pStarters.size());

    // Configure and start all processStarters.
    for (ProcessStarter pStarter : pStarters) {
      // Configure the pStarter.
      Configurator.configure(pStarter, this);
      // Use command line for input of process starter.
      if (cLine != null) {
        String[] remainingArgs = cLine.getArgs();
        for (int i = 0; i < remainingArgs.length; i++) {
          pStarter.setInput(remainingArgs[i]);
        }
      }

      this.pBoss.execute(pStarter);
      this.defaultLogger.log(Level.INFO,
          "ProcessStarter " + pStarter.getClass().getName() + " started");
    }
  }

  /**
   * <p>
   * Defines and starts the logging infrastructure.
   * </p>
   * 
   * @throws IOException
   */
  private void startLogging() throws IOException {

    // At the very beginning turn off the parent loggers console output.
    this.defaultLogger.setUseParentHandlers(false);

    // Create logfile name
    this.logfileName = this.logfileDir + File.separatorChar + this.logfileName;

    // Set the file and the console log handlers and assign them to our defaultLogger.
    try {
      this.fileLogHandler = new FileHandler(this.logfileName, true);
    } catch (IOException e) {
      System.out.println(
          "Could not create logfile '" + this.logfileName + "'! System message: " + e.getMessage());
      throw e;
    }

    // Check the log levels in the config file and set it. Use Level.INFO if there is no correct
    // level given.
    try {
      this.lLevel = Level.parse(this.logLevel);
    } catch (Exception e) {
      this.lLevel = Level.parse("INFO");
    }

    // Set the default logger's level.
    this.defaultLogger.setLevel(this.lLevel);

    // Configure the console log formatter and the file log formatter to a file handler and add them
    // to the defaultLogger.
    this.fileLogHandler.setFormatter(new KolibriFileLogFormatter());
    this.fileLogHandler.setLevel(this.lLevel);
    this.defaultLogger.addHandler(this.fileLogHandler);

    this.consoleLogHandler = new ConsoleHandler();
    this.consoleLogHandler.setFormatter(new KolibriConsoleLogFormatter());
    this.consoleLogHandler.setLevel(this.lLevel);
    this.defaultLogger.addHandler(this.consoleLogHandler);

    this.defaultLogger.log(Level.INFO,
        "koLibRI logging service up and running. Log level set to " + this.lLevel.toString()
            + ". Writing to file: " + new File(this.logfileName).getCanonicalPath());
  }

  /**
   * <p>
   * Creates a new process queue.
   * </p>
   * 
   * @throws InvocationTargetException
   * @throws IllegalAccessException
   */
  private void createProcessQueue() throws InvocationTargetException, IllegalAccessException {

    this.processQueue = new ProcessQueue();
    this.errorQueue = new ProcessQueue();

    try {
      Configurator.configure(this.processQueue);
      Configurator.configure(this.errorQueue);
    } catch (InvocationTargetException | IllegalAccessException e) {
      this.defaultLogger.log(Level.SEVERE, "Configuration invalid!");
      throw e;
    }

    this.defaultLogger.log(Level.FINE, "Process queue and error queue have been created");
  }

  /**
   * <p>
   * Adds simple console output listeners for debugging purposes to the process list.
   * </p>
   */
  private void addListListeners() {

    for (int listeners = 0; listeners < NUMBER_OF_TEST_LISTENERS; listeners++) {
      this.processQueue.addListDataListeners(new TestListDataListener(listeners));
    }

    this.defaultLogger.log(Level.FINE, "List listeners have been added properly");
  }

  /**
   * @param theConfigFile
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws IOException
   * @throws InvocationTargetException
   * @throws IllegalAccessException
   */
  public void configure(String theConfigFile) throws SAXException, ParserConfigurationException,
      IOException, InvocationTargetException, IllegalAccessException {

    this.configfileName = theConfigFile;

    // Set configuration file.
    Configurator.setConfigDocument(KolibriXMLParser.getDocument(this.configfileName));

    // Configure the WorkflowTool class and the default metadata format.
    Configurator.configure(this);
    Configurator.configure(MetadataFormat.Factory.class);
  }

  // **
  // GET & SET METHODS
  // **

  /**
   * @param defaultProcessStarter The defaultProcessStarter to set.
   */
  public void setDefaultProcessStarter(String defaultProcessStarter) {
    this.processStarters.add(defaultProcessStarter);
  }

  /**
   * @param logfileDir The logfileDir to set.
   */
  public void setLogfileDir(String logfileDir) {
    this.logfileDir = logfileDir;
  }

  /**
   * @param logLevel The logLevel to set.
   */
  public void setLogLevel(String logLevel) {
    this.logLevel = logLevel;
  }

  /**
   * @return Returns the processQueue.
   */
  public ProcessQueue getProcessQueue() {
    return this.processQueue;
  }

  /**
   * @return Returns the errorQueue.
   */
  public ProcessQueue getErrorQueue() {
    return this.errorQueue;
  }

  /**
   * @param maxNumberOfThreads The maxNumberOfThreads to set.
   */
  public void setMaxNumberOfThreads(int maxNumberOfThreads) {
    this.maxNumberOfThreads = maxNumberOfThreads;
  }

  /**
   * @return
   */
  public String getLogfileName() {
    return this.logfileName;
  }

  /**
   * @param defaultCharset
   */
  public void setDefaultCharset(String defaultCharset) {
    this.defaultCharset = defaultCharset;
  }

  /**
   * @return
   */
  public String getDefaultCharset() {
    return this.defaultCharset;
  }

}
