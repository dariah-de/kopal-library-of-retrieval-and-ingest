/**
 * Copyright 2005-2020 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 */

/**
 * TODOLOG
 **
 * CHANGELOG
 * 
 * 2020-02-18 - Funk - DOne some formatting.
 * 
 * 2008-04-01 - Funk - Take the action name from the XML file again if taken from the class gives
 * back a null.
 * 
 * 2007-07-10 - koLibRI version 1.0
 */

package de.langzeitarchivierung.kolibri;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import org.w3c.dom.Node;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.ui.InputRequestEvent;
import de.langzeitarchivierung.kolibri.ui.InputRequestListener;
import de.langzeitarchivierung.kolibri.ui.StatusChangedEvent;
import de.langzeitarchivierung.kolibri.ui.StatusListener;
import de.langzeitarchivierung.kolibri.util.Configurator;
import de.langzeitarchivierung.kolibri.util.KolibriClassLoader;

/**
 * <p>
 * Node of a policy tree. Contains the action to be performed and the infrastructure for the action
 * models.
 * </p>
 * 
 * <p>
 * TODO Check if the collections in this class have to be synchronised.
 * </p>
 * 
 * @author Ludwig
 * @version 2007-07-10
 * @since 2006-02-02
 */

public class Step {

  // **
  // STATE (Instance variables)
  // **

  private Status status;
  private ActionModule action;

  // Data for lazy loading and initialisation of action modules.
  private boolean firstTime = true;
  private Node configPolicyNode;
  private ProcessData processData;

  // List of child nodes in the policy tree.
  private List<Step> childs;

  // Parent node in the policy tree.
  private Step parent;

  private List<StatusListener> statusListeners;
  private InputRequestListener irl;

  // **
  // CREATION (Constructors, factory methods, static/inst init)
  // **

  /**
   * <p>
   * Constructor.
   * </p>
   * 
   * @param ch
   * @param node
   */
  public Step(List<Step> ch, Node node) {
    super();
    this.childs = ch;
    this.configPolicyNode = node;
    this.parent = null;
    this.status = new Status(Status.TODO, "Has to be done");
    this.statusListeners = new ArrayList<StatusListener>();
  }

  /**
   * <p>
   * Constructor.
   * </p>
   * 
   * @param ch
   * @param par
   * @param node
   */
  public Step(List<Step> ch, Node node, Step par) {
    super();
    this.childs = ch;
    this.parent = par;
    this.configPolicyNode = node;
    this.status = new Status(Status.TODO, "Has to be done");
    this.statusListeners = new ArrayList<StatusListener>();
  }

  // **
  // MANIPULATION (Manipulation - what the object does) ******************
  // **

  /***************************************************************************
   * <p>
   * Runs the action module.
   * </p>
   **************************************************************************/
  public void run() {
    // Create and initialise action module only the first time it is needed.
    if (this.firstTime) {
      try {
        initializeActionModule();
      } catch (Exception e) {
        e.printStackTrace();
        setStatus(Status.ERROR, "Fatal error while loading action " + "module: " + e.getMessage());
        return;
      }

      // If action module has been successful initialized prevent it from happening again.
      this.firstTime = false;
      setStatus(Status.RUNNING, "Initialized ActionModule " + getActionName());
    }

    this.action.go();
  }

  /**
   * <p>
   * Adds object which implements the statusListener interface to the notify list.
   * </p>
   * 
   * @param sl the statusListener to add
   */
  public void addStatusListener(StatusListener sl) {
    if (!this.statusListeners.contains(sl)) {
      this.statusListeners.add(sl);
    }
  }

  /**
   * <p>
   * Adds object which implements the StatusListener interface to the notify list.
   * </p>
   * 
   * @param sl the statusListener to remove
   */
  public void removeStatusListener(StatusListener sl) {
    this.statusListeners.remove(sl);
  }

  /**
   * <p>
   * Notifies all statusListeners about a changed status.
   * </p>
   * 
   * @param oldStatus
   */
  private void fireStatusEvent(int oldStatus) {
    ListIterator<StatusListener> listenerIterator = this.statusListeners.listIterator();

    StatusChangedEvent eventToFire = new StatusChangedEvent(this, oldStatus);
    while (listenerIterator.hasNext()) {
      ((StatusListener) listenerIterator.next()).update(eventToFire);
    }
  }

  /**
   * <p>
   * Remember specified inputRequestListener to be notified if input is needed. A previous
   * inputRequestListener will be overwritten.
   * </p>
   * 
   * <p>
   * NOTE It seems that only one inputRequestListener makes sense.
   * </p>
   * 
   * @param ireql The inputRequestListener to set
   */
  public void setInputRequestListener(InputRequestListener ireql) {
    this.irl = ireql;
  }

  /**
   * <p>
   * Removes the specified inputRequestListener.
   * </p>
   * 
   * @param listener The inputRequestListener to be removed
   */
  public void removeInputRequestListener(InputRequestListener listener) {
    if (this.irl == listener) {
      this.irl = null;
    }
  }

  /**
   * <p>
   * Notifies the inputRequestListeners about an input request.
   * </p>
   * 
   * @param input The inputRequestListener puts his input into this object. If necessary a modifier
   *        for the inputRequestListener can be passed with this object. TODO Check this
   *        possibilities
   */
  public void fireInputRequestEvent(Object input) {
    if (this.irl != null) {
      this.irl.getInput(new InputRequestEvent(input));
    }
  }

  /**
   * <p>
   * Checks if every step in the path from this step to root has the specified status.
   * </p>
   * 
   * @param pathStatus Must be one of the constants of class Status
   * @return true if every step in the path from node to root has the specified status or this step
   *         or it's parent is the dummy root, false otherwise
   */
  public boolean pathHasStatus(int pathStatus) {
    if (this.getStatus().getDescription().equals(Policy.DUMMY_ROOT_STRING)
        || this.getParent().getStatus().getDescription().equals(Policy.DUMMY_ROOT_STRING)) {
      return true;
    }

    Step parentNode = this.getParent();

    while (!parentNode.getStatus().getDescription().equals(Policy.DUMMY_ROOT_STRING)) {
      if (parentNode.getStatus().getType() != pathStatus) {
        return false;
      }
      parentNode = parentNode.getParent();
    }

    return true;
  }

  // **
  // INTERNAL (Internal - implementation details, local classes, ...)
  // **

  /**
   * <p>
   * Loads the actionModule class from disc and initialises it.
   * </p>
   * 
   * @exception Exception
   */
  private void initializeActionModule() throws Exception {
    this.action = loadClassFromDisc(getClassNameFromXMLString(this.configPolicyNode));
    Configurator.configure(this.action, this.configPolicyNode, this);
  }

  /**
   * <p>
   * Small helper method. Returns the class name under the given node.
   * </p>
   * 
   * @param node where to look for the class name
   * @return the class name under the given node
   */
  private static String getClassNameFromXMLString(Node node) {
    return node.getAttributes().getNamedItem(Policy.CLASS_TAG).getNodeValue();
  }

  /**
   * <p>
   * Loads the specified class from disc.
   * </p>
   * 
   * <p>
   * TODO configurable path
   * </p>
   * 
   * @param classToLoad well, the class name to load
   * @return the loaded actionModul instance
   * @throws Exception
   */
  private static ActionModule loadClassFromDisc(String classToLoad) throws Exception {
    Class<ActionModule> newClass = KolibriClassLoader.getActionModule(classToLoad);
    return (ActionModule) KolibriClassLoader.getInstanceOfClass(newClass);
  }

  /**
   * <p>
   * Destructs this step and prepares for garbage collection.
   * </p>
   * 
   * <p>
   * NOTE This makes the tree invalid because this step is not removed from its parents child list.
   * </p>
   */
  protected void destructStep() {
    if (this.childs != null) {
      this.childs.clear();
      this.childs = null;
    }
    this.action = null;
  }

  // **
  // GET & SET METHODS
  // **

  /**
   * @return Returns iterator over child nodes if they exist, otherwise null.
   */
  public ListIterator<Step> getChildIterator() {
    if (this.childs != null) {
      return this.childs.listIterator();
    }

    return null;
  }

  /**
   * @return Returns the parent or null if not parent exists.
   */
  public Step getParent() {
    return this.parent;
  }

  /**
   * @return Returns the status of this node.
   */
  public Status getStatus() {
    return this.status;
  }

  /**
   * @param type The type to set
   * @param description The description to set
   * @see de.langzeitarchivierung.kolibri.Status
   */
  public void setStatus(int type, String description) {
    File f = null;
    int oldStat = this.status.getType();
    this.status.setType(type);
    this.status.setDescription(description);
    if (this.processData != null && this.processData.getPathToContentFiles() != null) {
      f = new File(this.processData.getPathToContentFiles());
    }
    this.status.setCurrentFile(f);
    fireStatusEvent(oldStat);

    // We want to proceed as normal if we informed the listeners about the warning.
    if (type == Status.WARNING) {
      this.status.setType(Status.RUNNING);
    }
  }

  /**
   * @param type The type to set
   * @param description The description to set
   * @param currentFile The file to log
   * @see de.langzeitarchivierung.kolibri.Status
   */
  public void setStatus(int type, String description, File currentFile) {
    int oldStat = this.status.getType();
    this.status.setType(type);
    this.status.setDescription(description);
    this.status.setCurrentFile(currentFile);
    fireStatusEvent(oldStat);

    // We want to proceed as normal if we informed the listeners about the warning.
    if (type == Status.WARNING) {
      this.status.setType(Status.RUNNING);
    }
  }

  /**
   * @return Returns the action module name.
   */
  public String getActionName() {
    String result;

    // Take class name from the XML policy file.
    if (this.action.getClass() == null) {
      result = getClassNameFromXMLString(this.configPolicyNode);
    } else {
      // Take the class name from the action object.
      result = this.action.getClass().getName();
    }

    return result;
  }

  /**
   * @return Returns the processData.
   */
  public ProcessData getProcessData() {
    return this.processData;
  }

  /**
   * @param processData The processData to set.
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   * @param status The status to set.
   */
  public void setStatus(Status status) {
    this.status = status;
  }

}
