/**
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 * 
 **
 *
 * CHANGELOG
 * 
 * 10.07.2007 - koLibRI version 1.0
 */

package de.langzeitarchivierung.kolibri.ui;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * TestListDataListener.
 * 
 * TODO The UI package is work in progress.
 * 
 * @author Ludwig
 * @version 20060123
 */

public class TestListDataListener implements ListDataListener {

  private static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");

  // Number to distinguish different listeners.
  int number;

  /**
   * @param number the number
   */
  public TestListDataListener(int number) {
    super();
    this.number = number;
  }

  /**
   *
   */
  @Override
  public void contentsChanged(ListDataEvent arg0) {
    System.out.println(" " + this.number + ": ListDataEvent: Contents changed, source: "
        + arg0.getSource() + " index: " + arg0.getIndex0());
    defaultLogger.log(Level.INFO, this.number + ": ListDataEvent: Contents changed, source: "
        + arg0.getSource() + " index: " + arg0.getIndex0());
  }

  /**
   *
   */
  @Override
  public void intervalAdded(ListDataEvent arg0) {
    System.out
        .println(" " + this.number + ": ListDataEvent: Interval added, source: " + arg0.getSource()
            + " lower index: " + arg0.getIndex0() + " upper index : " + arg0.getIndex1());
    defaultLogger.log(Level.INFO,
        this.number + ": ListDataEvent: Interval added, source: " + arg0.getSource()
            + " lower index: " + arg0.getIndex0() + " upper index : " + arg0.getIndex1());
  }

  /**
   *
   */
  @Override
  public void intervalRemoved(ListDataEvent arg0) {
    System.out.println(
        " " + this.number + ": ListDataEvent: Interval removed, source: " + arg0.getSource()
            + " lower index: " + arg0.getIndex0() + " upper index : " + arg0.getIndex1());
    defaultLogger.log(Level.INFO,
        this.number + ": ListDataEvent: Interval removed, source: " + arg0.getSource()
            + " lower index: " + arg0.getIndex0() + " upper index : " + arg0.getIndex1());
  }

}
