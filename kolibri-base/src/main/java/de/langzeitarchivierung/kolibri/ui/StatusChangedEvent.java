/*******************************************************************************
 * de.langzeitarchivierung.kolibri.ui / StatusChangedEvent.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * 
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.ui;

import java.util.EventObject;

/*******************************************************************************
 * Event class that informs about status changes of an action moduls.
 * 
 * TODO The UI package is work in progress.
 * 
 * @author Ludwig
 * @version 20060214
 ******************************************************************************/

public class StatusChangedEvent extends EventObject
{

	// CREATION (Constructors, factory methods, static/inst init) **********
	
	private static final long	serialVersionUID	= 1L;
	private int					oldStatus;

    /***********************************************************************
     * Constructor.
     *
     * @param arg The step whose status changed
     * @param _oldStatus the previous status type
     ***********************************************************************/
	public StatusChangedEvent(Object arg, int _oldStatus) {
		super(arg);
		this.oldStatus = _oldStatus;
	}

    /***********************************************************************
     * Returns the previos status type.
     * 
     * @return the previous status type
     ***********************************************************************/
    public int getOldStatus(){
        return this.oldStatus;
    }

}
