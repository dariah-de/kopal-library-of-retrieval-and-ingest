/*******************************************************************************
 * de.langzeitarchivierung.kolibri.ui / StatusListener.java
 *  
 * Copyright 2005-2007 by Project kopal 
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.ui;

import java.util.EventListener;

/*******************************************************************************
 * Interface for classes that inform about status changes of action modules.
 * 
 * TODO The UI package is work in progress.
 * 
 * @author Ludwig
 * @since 20060123
 * @version 20070425
 ******************************************************************************/

public interface StatusListener extends EventListener
{

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * The method called when a statusChangedEvent was fired.
	 * 
	 * @param sc The status change event *
	 **************************************************************************/
	void update(StatusChangedEvent sc);

}
