/*******************************************************************************
 * de.langzeitarchivierung.kolibri.ui / TestInputListener.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 *******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.ui;

/*******************************************************************************
 * TestInputListener.
 * 
 * TODO The UI package is work in progress.
 * 
 * @since 20050627
 * @author Ludwig
 * @version 20060123
 ******************************************************************************/

public class TestInputListener implements InputRequestListener
{

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * Default Constructor.
	 **************************************************************************/
	public TestInputListener() {
		super();
	}

	// MANIPULATION (Manipulation - what the object does) ******************

	/**
	 * @see de.langzeitarchivierung.kolibri.ui
	 * 				.InputRequestListener#getInput(
	 * 						de.langzeitarchivierung.kolibri.ui.InputRequestEvent)
	 */
	public void getInput(InputRequestEvent ir) {

		System.out.print("InputRequestEvent, source: " + ir.getSource()
				+ " type: ");
		switch (ir.requestedInput) {
			case InputRequestEvent.DMD:
				System.out.print("DMD");
				ir.input = "testDmdString";
				break;
			case InputRequestEvent.ID:
				System.out.print("ID");
				ir.input = "testIdString";
				break;
			case InputRequestEvent.FILES:
				System.out.print("FILES");
				// TODO perhaps type File instead of String is more appropiate?
				ir.input = "testFileString";
				break;
			default:
				System.out.print("UNKNOWN TYPE " + ir.requestedInput);
				ir.input = "testUnknownString";
				break;
		}
	}

}
