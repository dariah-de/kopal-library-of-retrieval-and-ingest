/*******************************************************************************
 * de.langzeitarchivierung.kolibri.ui / InputRequestEvent.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * 
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.ui;

import java.util.EventObject;

/*******************************************************************************
 * Event class that requests user input for action moduls.
 * 
 * TODO The ui package is work in progress.
 * 
 * @author Ludwig
 * @since 20060623
 * @version 20061218
 ******************************************************************************/

public class InputRequestEvent extends EventObject
{

	// CONSTANTS **********************************************************

	private static final long	serialVersionUID	= 1L;

	// TODO Choose sane constants, these are for testing.
	/**DMD constant*/
	public static final int		DMD					= 0;
	/**ID constant*/
	public static final int		ID					= 1;
	/**FILES constant*/
	public static final int		FILES				= 2;

	// STATE (Instance variables) *****************************************

	/** Access modifiers?*/
	public int					requestedInput;
	/** Access modifiers?*/
	public Object				input;

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * Constructor.
	 * 
	 * @param _input The inputRequestListener puts his input into this object.
	 *            If necessary a modifier for the inputRequestListener can be
	 *            passed with this object.
	 * @since 0.1
	 * 
	 **************************************************************************/
	public InputRequestEvent(Object _input) {
		super(_input);

		// Extract requested input type from parameter.
		if ((_input != null) && (_input instanceof Integer)) {
			this.requestedInput = ((Integer) _input).intValue();
		}

		this.input = _input;
	}

}
