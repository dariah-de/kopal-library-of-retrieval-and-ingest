/*******************************************************************************
 * de.langzeitarchivierung.kolibri.ui / LoggingStatusListener.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.ui;

import java.util.logging.Level;
import java.util.logging.Logger;

import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;

/*******************************************************************************
 * The LoggingStatusListener is updated if the status of a ProcessQueue element
 * changes. The status' description from the actionmodule is written to the
 * defaultLogger.
 * 
 * @author Ludwig
 * @version 20070131
 * @since 20050627
 ******************************************************************************/

public class LoggingStatusListener implements StatusListener
{

	// CLASS VARIABLES (Static variables) *********************************

	private static Logger	defaultLogger	= Logger
													.getLogger("de.langzeitarchivierung.kolibri");

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * Constructor
	 **************************************************************************/
	public LoggingStatusListener() {
		super();
	}

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * @see de.langzeitarchivierung.kolibri.ui .StatusListener#update(
	 *      de.langzeitarchivierung.kolibri.ui.StatusChangedEvent)
	 **************************************************************************/
	public void update(StatusChangedEvent sc) {
		Level logLevel;
		String moduleName = ((Step) sc.getSource()).getActionName();
		String processName = ((Step) sc.getSource()).getProcessData()
				.getProcessName();
		Status status = ((Step) sc.getSource()).getStatus();
		String statusText = "<" + status.getStatusText() + ">";
		int statusType = ((Step) sc.getSource()).getStatus().getType();
		String statusDescription = ((Step) sc.getSource()).getStatus()
				.getDescription();

		// All status changes are logged here, but first get the log level from
		// the status types.
		switch (statusType) {
			case Status.ERROR:
				logLevel = Level.SEVERE;
				break;
			case Status.WARNING:
				logLevel = Level.WARNING;
				break;
			default:
				logLevel = Level.INFO;
		}

		// Get the status text and do a leftalign.
		while (statusText.length() < 11) {
			statusText += " ";
		}

		// Finally log the result.
		defaultLogger.logp(logLevel, moduleName, "", statusText + processName
				+ "  -->  " + statusDescription);
	}

}
