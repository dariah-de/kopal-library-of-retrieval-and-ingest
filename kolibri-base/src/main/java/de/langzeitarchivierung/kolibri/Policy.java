/**
 * Copyright 2005-2009 by Project kopal (http://kopal.langzeitarchivierung.de)
 * 
 * Copyright 2010-2011 by Project DP4lib (http://dp4lib.langzeitarchivierung.de)
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 * 
 **
 * 
 * CHANGELOG
 * 
 * 13.02.2008 - Funk - Added some "y" or "ies" ;-)
 * 
 * 10.07.2007 - koLibRI version 1.0
 */

package de.langzeitarchivierung.kolibri;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
// import de.langzeitarchivierung.kolibri.ui.DBMessageListener;
import de.langzeitarchivierung.kolibri.ui.InputRequestListener;
import de.langzeitarchivierung.kolibri.ui.LoggingStatusListener;
import de.langzeitarchivierung.kolibri.ui.StatusChangedEvent;
import de.langzeitarchivierung.kolibri.ui.StatusListener;
import de.langzeitarchivierung.kolibri.ui.TestInputListener;
import de.langzeitarchivierung.kolibri.util.Configurator;
import de.langzeitarchivierung.kolibri.util.KolibriXMLParser;
import de.langzeitarchivierung.kolibri.util.KolibriXMLTransformer;

/******
 * <p>
 * Represents a policy e.g. a build policy or an ingest policy. Can build a tree from its
 * representation.
 * </p>
 * 
 * <p>
 * Keeps up to date status lists of steps and returns steps with a specific status and path or
 * parent.
 * </p>
 * 
 * TODO Use SAX or Jakarta-digester if we have enough time.
 * 
 * @see <a href="config/config.xml">Definition of policies in config file</a>
 * @author Jens Ludwig, SUB Göttingen
 * @version 2011-01-08
 * @since 2006-01-19
 */

public class Policy implements StatusListener {

  // CONSTANTS **********************************************************

  /** The xml tags and attributes for the policy file */
  public static final String POLICY_TAG = "policy";
  /** The xml tags and attributes for the policy file */
  public static final String POLICY_NAME_TAG = "name";
  /** The xml tags and attributes for the policy file */
  public static final String STEP_TAG = "step";
  /** The xml tags and attributes for the policy file */
  public static final String CLASS_TAG = "class";
  /** The xml tags and attributes for the policy file */
  public static final String TEMPLATE_TAG = "template";
  public static final String SINGLE_TEMPLATE_TAG = "<"
      + TEMPLATE_TAG + "/>";

  // STATE (Instance variables) *******************************************

  /** Dummy values */
  public static final String DUMMY_ROOT_STRING = "Dummy root element";
  public static final Node DUMMY_ROOT_NODE = null;

  private static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");
  private static HashMap<String, String> policies;

  // Representation of the policy tree.
  private String tree;

  // Dummy root element of the policy tree.
  private Step root;

  // Constructor parameters for new steps.
  private ProcessData processData;

  // Keeps for each status type a list of nodes.
  private LinkedList<Step>[] statusLists;

  // The index position of the step with the [Status.TYPE] delivered with
  // next().
  private int[] nextStatusStep;

  // CREATION ***************************************************************

  /**
   * <p>
   * Constructor.
   * </p>
   * 
   * @param policyName - The name of the policy
   * @throws Exception
   */
  public Policy(String policyName) throws Exception {

    super();

    policies = Policy.loadPolicy((String) Configurator.getValue("policyFile"));

    if (!policies.containsKey(policyName)) {
      throw new Exception("No policy with name " + policyName);
    }

    // Lists for keeping track of node status.
    this.nextStatusStep = new int[Status.NUMBER_OF_STATUS_TYPES];
    this.statusLists = new LinkedList[Status.NUMBER_OF_STATUS_TYPES];

    for (int i = 0; i < Status.NUMBER_OF_STATUS_TYPES; i++) {
      this.statusLists[i] = new LinkedList<Step>();
      this.nextStatusStep[i] = 0;
    }

    this.tree = (String) policies.get(policyName);
  }

  /**
   * <p>
   * Constructor for initalising a custom policy. Useful for example for migrations.
   * </p>
   * 
   * TODO Well, the policyTree is of the string buffer type because String is already in use. Thats
   * not really nice.
   * 
   * @param policyTree - the policy representation
   * @throws Exception
   */
  public Policy(StringBuffer policyTree) throws Exception {

    super();

    // Lists for keeping track of node status.
    this.nextStatusStep = new int[Status.NUMBER_OF_STATUS_TYPES];
    this.statusLists = new LinkedList[Status.NUMBER_OF_STATUS_TYPES];

    for (int i = 0; i < Status.NUMBER_OF_STATUS_TYPES; i++) {
      this.statusLists[i] = new LinkedList<Step>();
      this.nextStatusStep[i] = 0;
    }

    this.tree = policyTree.toString();
  }

  // GET & SET METHODS **************************************************

  /**
   * <p>
   * Returns the xml string representation of the policy tree.
   * </p>
   * 
   * @return Returns the xml string representation of the policy tree.
   */
  public String getPolicyRepresentaion() {
    return this.tree;
  }

  /**
   * <p>
   * Returns a new policy iterator if a tree has been build.
   * </p>
   * 
   * @return a new policy iterator if a tree has been build otherwise null
   */
  public PolicyIterator getPolicyIterator() {

    if (this.root != null) {
      return new PolicyIterator(this.root);
    }

    return null;
  }

  /**
   * <p>
   * Adds to every step of the policy tree a StatusListener.
   * </p>
   * 
   * @param sListener the StatusListener to add
   */
  public void addStatusListener(StatusListener sListener) {

    PolicyIterator stepIterator = this.getPolicyIterator();
    while ((stepIterator != null) && (stepIterator.hasNext())) {
      ((Step) stepIterator.next()).addStatusListener(sListener);
    }
  }

  /**
   * <p>
   * Sets the InputRequestListener of every step of the policy tree.
   * </p>
   * 
   * @param irListener the InputRequestListener to set
   */
  public void setInputRequestListener(InputRequestListener irListener) {

    PolicyIterator stepIterator = this.getPolicyIterator();
    while ((stepIterator != null) && (stepIterator.hasNext())) {
      ((Step) stepIterator.next()).setInputRequestListener(irListener);
    }
  }

  /**
   * <p>
   * Returns the list of steps with the specified status type.
   * </p>
   * 
   * @param statusType one of the public constants of Status.java
   * @return the requested list of steps
   */
  public LinkedList<Step> getStatusList(int statusType) {
    return this.statusLists[statusType];
  }

  /**
   * <p>
   * Tests if the policy has a node with the specified status.
   * </p>
   * 
   * @param statusTypeToCheck one of the public constants of Status.java
   * @return True if the policy tree contains the specified status, false otherwise
   */
  public boolean hasStatus(int statusTypeToCheck) {

    if (this.statusLists[statusTypeToCheck].isEmpty()) {
      return false;
    }

    return true;
  }

  /**
   * <p>
   * Tests if a step with the specified status (e.g. Status.TODO) and path (e.g. Status.DONE)
   * exists.
   * </p>
   * 
   * @param statusTypeToCheck the status of the requested step
   * @param pathStatus the status of the path
   * @return true if step exists, false otherwise
   */
  public boolean hasStatusWithPath(int statusTypeToCheck, int pathStatus) {

    for (int i = 0; i < this.statusLists[statusTypeToCheck].size(); i++) {
      Step statusStep = this.statusLists[statusTypeToCheck].get(i);
      if (statusStep.pathHasStatus(pathStatus)) {
        return true;
      }
    }

    return false;
  }

  /**
   * <p>
   * Tests if the policy tree has a step with a status which equals not the specified status.
   * </p>
   * 
   * @param statusTypeToCheck
   * @return true if there is a step with an other status, false otherwise
   */
  public boolean hasOtherStatusThan(int statusTypeToCheck) {

    for (int i = 0; i < Status.NUMBER_OF_STATUS_TYPES; i++) {
      if (i != statusTypeToCheck) {
        if (hasStatus(i)) {
          return true;
        }
      }
    }

    return false;
  }

  /**
   * <p>
   * Resets the index of the next step checked in nextStatusStepWithPath() or
   * nextStatusStepWithParent().
   * </p>
   * 
   * @param statusTypeToCheck the status type of the steps to reset
   */
  public void resetNextStatusStep(int statusTypeToCheck) {
    this.nextStatusStep[statusTypeToCheck] = 0;
  }

  /**
   * <p>
   * Returns the next step with the specified status (e.g. Status.TODO) and path (e.g. Status.DONE)
   * which has been unchecked. Note that this method can return no step although hasStatusWithPath
   * returns true. This is the case if the step has been already checked and resetNextStatusStep()
   * hasn't been called.
   * </p>
   * 
   * @param statusTypeToCheck the status of the requested step
   * @param pathStatus the status of the path
   * @return the requested step of null if no such unchecked step exists
   */
  public Step nextStatusStepWithPath(int statusTypeToCheck, int pathStatus) {

    defaultLogger.log(Level.FINEST, "MAX: " + this.statusLists[statusTypeToCheck].size() + " NOW: "
        + this.nextStatusStep[statusTypeToCheck]);

    // Begin checking steps with previously unchecked steps.
    for (int i = this.nextStatusStep[statusTypeToCheck]; i < this.statusLists[statusTypeToCheck]
        .size(); i++) {
      Step statusStep = this.statusLists[statusTypeToCheck].get(i);

      if (statusStep.pathHasStatus(pathStatus)) {
        // Remember which step should be checked the next time.
        this.nextStatusStep[statusTypeToCheck] = i + 1;
        return statusStep;
      }
    }

    return null;
  }

  /**
   * <p>
   * Tests if a step with the specified status (e.g. Status.TODO) and parent status (e.g.
   * Status.DONE) exists.
   * </p>
   * 
   * @param statusTypeToCheck the status of the requested step
   * @param parentStatus the status of the parent
   * @return true if step exists, false otherwise
   */
  public boolean hasStatusWithParent(int statusTypeToCheck,
      int parentStatus) {

    for (int i = 0; i < this.statusLists[statusTypeToCheck].size(); i++) {
      Step statusStep = this.statusLists[statusTypeToCheck].get(i);
      if (statusStep.getParent().getStatus().getType() == parentStatus) {
        return true;
      }
    }

    return false;
  }

  // MANIPULATION (Manipulation - what the object does) ******************

  /**
   * <p>
   * Builds a new policy tree from its representation and initialises the modules.
   * </p>
   * 
   * @return Step
   * @exception Exception
   */
  public Step buildTree() throws Exception {

    // Get parsable source from tree representation.
    Document xmlTreeRepresentation = KolibriXMLParser.getDocumentFromString(this.tree);

    // Build tree recursively, begin with xml root element.
    this.root = buildTreeNode(xmlTreeRepresentation.getDocumentElement());

    // TODO This has to be made configurable.
    this.addStatusListener(new LoggingStatusListener());
    this.setInputRequestListener(new TestInputListener());

    return this.root;
  }

  /**
   * <p>
   * Debug method. Parses string representation of policy tree and prints it to screen.
   * </p>
   */
  public void printPolicyTree() {

    try {
      // Get source from tree representation.
      Document document = KolibriXMLParser.getDocumentFromString(this.tree);
      // Print tree, begin with root element.
      KolibriXMLTransformer.outputXMLTree(new DOMSource(document.getDocumentElement()),
          new StreamResult(System.out));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * @see de.langzeitarchivierung.kolibri.ui
   *      .StatusListener#update(de.langzeitarchivierung.kolibri.ui.StatusChangedEvent)
   */
  @Override
  public void update(StatusChangedEvent sc) {

    Step changedStep = (Step) sc.getSource();
    int previousStatusType = sc.getOldStatus();
    int newStatusType = changedStep.getStatus().getType();

    // If old and new status don't differ, nothing has to be done.
    if (previousStatusType == newStatusType) {
      return;
    }

    // Remove the changed step from its old list and update index.
    if (this.nextStatusStep[previousStatusType] > this.statusLists[previousStatusType]
        .indexOf(changedStep)) {
      this.nextStatusStep[previousStatusType]--;
    }
    this.statusLists[previousStatusType].remove(changedStep);

    // Add the changed step to the new list and update index.
    this.statusLists[newStatusType].addFirst(changedStep);
    this.nextStatusStep[newStatusType]++;
  }

  /**
   * <p>
   * Prints the statusLists for debugging.
   * </p>
   */
  public void printLists() {

    System.out.println("status lists:");

    for (int i = 0; i < Status.NUMBER_OF_STATUS_TYPES; i++) {
      System.out.println("    " + Status.getStatusText(i) + " has " + this.statusLists[i].size()
          + " with unchecked step #" + this.nextStatusStep[i]);
      for (int j = 0; j < this.statusLists[i].size(); j++) {
        System.out.println("      " + j + " is " + (this.statusLists[i].get(j)).getActionName());
      }
    }
  }

  // INTERNAL (Internal - implementation details, local classes, ...)

  /**
   * <p>
   * Builds the sub tree of the specified representation node and initialises the modules. Builds
   * the dummy root.
   * </p>
   * 
   * <p>
   * Method strips the root xml node from the policy representation and looks for the first child
   * element that is a step node. So buildTreeNode(Node, Step) gets (hopefully) a Node which is
   * already a step node.
   * </p>
   * 
   * TODO Check that the root element returned from getDocumentElement() never returns already a
   * step node because this would be ignored.
   * 
   * @param node The root representation node of the sub tree to construct. This needs NOT to be
   *        already a step node!
   * @return Step which is the root of the policy tree, null if there is no step node among the
   *         childs
   * @exception Exception
   */
  private Step buildTreeNode(Node node) throws Exception {

    Step dummyRoot = null;
    NodeList stepNodes = KolibriXMLParser.getDirectSubNodes(node, Policy.STEP_TAG);

    if (stepNodes.getLength() != 0) {
      // Build dummy root element because a policy can have more than one
      // node on its highest level make new child list for root, children
      // are added later.
      ArrayList<Step> childList = new ArrayList<Step>();

      // Root step has no action and no parent.
      dummyRoot = new Step(childList, DUMMY_ROOT_NODE, null);

      // Its status is done from the beginning so that it's a dummy.
      dummyRoot.setStatus(Status.DONE, DUMMY_ROOT_STRING);

      // For each step node...
      for (int i = 0; i < stepNodes.getLength(); i++) {
        // ...build policy tree with this child after dummyRoot.
        childList.add(buildTreeNode((stepNodes.item(i)), dummyRoot));
      }
    }

    return dummyRoot;
  }

  /**
   * <p>
   * Recursive function that builds the sub tree of the specified representation node and
   * initialises the modules.
   * </p>
   * 
   * @param node The representation node of the subtree to construct. This has to be already a step
   *        node!
   * @param parent The parent node in the policy tree
   * @return Step Returns the step which is the root of the sub tree
   * @exception Exception
   */
  private Step buildTreeNode(Node node, Step parent) throws Exception {

    Step newStep;
    NodeList stepNodes = KolibriXMLParser.getDirectSubNodes(node, Policy.STEP_TAG);

    // Only if step children are present.
    if (stepNodes.getLength() != 0) {
      // Make new child list for new Step, children are added later.
      ArrayList<Step> childList = new ArrayList<Step>();

      // We must make the new step now because the dear children have to
      // know their parents.
      newStep = new Step(childList, node, parent);

      // Add step to its status list and listen for changes we have to do
      // this before the recursion for the right list order.
      newStep.addStatusListener(this);
      this.statusLists[Status.TODO].add(newStep);

      // For each step child node...
      for (int i = 0; i < stepNodes.getLength(); i++) {
        // ...build the child node and add it to newSteps child list.
        ListIterator<Step> iter = newStep.getChildIterator();
        iter.add(buildTreeNode(stepNodes.item(i), newStep));
      }
    }

    // If we have no step child nodes...
    else {
      // ...we need no child list in newStep and no recursion.
      newStep = new Step(null, node, parent);

      // Add step to its status list and listen for changes we have to do this before the recursion
      // for the right list order.
      newStep.addStatusListener(this);
      this.statusLists[Status.TODO].add(newStep);
    }

    Configurator.configure(newStep, node, this);

    return newStep;
  }

  /**
   * <p>
   * Destructs the whole policy tree and tries to call the garbage collector.
   * </p>
   */
  protected void destructTree() {
    if (this.root != null) {
      destructSubTree(this.root);
    }
  }

  /**
   * <p>
   * Destructs a sub tree.
   * </p>
   * 
   * @param s Destructs its sub tree and itself
   */
  private void destructSubTree(Step s) {

    Iterator<Step> childIterator = s.getChildIterator();
    while (childIterator != null && childIterator.hasNext()) {
      destructSubTree(childIterator.next());
    }
    s.destructStep();
  }

  /**
   * <p>
   * Returns the policies of the requested type.
   * </p>
   * 
   * @param configFileName
   * @return A hash map which maps the policy name to its xml definition
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws TransformerException
   * @since 0.1
   */
  private static HashMap<String, String> loadPolicy(String configFileName)
      throws SAXException, ParserConfigurationException, IOException,
      TransformerException {

    HashMap<String, String> result = new HashMap<String, String>();
    String singlePolicyString = "";
    String policyName = "";
    String policyNames = "";

    // Get XML parser.
    Document document = KolibriXMLParser.getDocument(configFileName);
    NodeList policyNodes = document.getElementsByTagName(POLICY_TAG);

    // For every policy...
    for (int i = 0; i < policyNodes.getLength(); i++) {
      // ...put it with its name to the build policies.

      // Try writing xml tree into resultString.
      //
      // Use a transformer to get a string representation of an XML node. "policyNodes.item
      // (i).toString ())" didn't work with Java 5, worked only with Java 1.4.
      StringWriter resultString = new StringWriter();

      KolibriXMLTransformer.outputXMLTree(new DOMSource(policyNodes.item(i)),
          new StreamResult(resultString));
      singlePolicyString = resultString.toString();

      // Get the policies' name and put the name and the policy string
      // into the hash map.
      policyName = ((Element) policyNodes.item(i)).getAttribute(Policy.POLICY_NAME_TAG);
      result.put(policyName, singlePolicyString);

      // Just for logging output.
      policyNames += (policyNames.equals("") ? "" : ", ") + policyName;
    }

    defaultLogger.log(Level.INFO, "Policy configuration file '" + configFileName + "' processed");
    defaultLogger.log(Level.INFO,
        result.size() + " polic" + (result.size() == 1 ? "y" : "ies") + " found: " + policyNames);

    return result;
  }

  /**
   * <p>
   * Inserts a new policyFragment in an unfinished policy and returns the resulting policyFragment
   * for further editing.
   * </p>
   * 
   * @param oldPolicyFragment
   * @param newPolicyFragment
   * @return
   */
  public static StringBuffer buildPolicyString(StringBuffer oldPolicyFragment,
      StringBuffer newPolicyFragment) {

    if (oldPolicyFragment == null) {
      oldPolicyFragment = new StringBuffer();
    }

    // Remove the XML fragment headers if present.
    if (newPolicyFragment.toString().startsWith("<?xml")) {
      newPolicyFragment = new StringBuffer(newPolicyFragment.substring(
          newPolicyFragment.indexOf("<" + STEP_TAG + " " + CLASS_TAG),
          newPolicyFragment.lastIndexOf("</" + STEP_TAG + ">") + ("</" + STEP_TAG + ">").length()));
    }

    if (oldPolicyFragment.length() == 0) {
      oldPolicyFragment.append(newPolicyFragment);
    } else {
      oldPolicyFragment.replace(oldPolicyFragment.indexOf(SINGLE_TEMPLATE_TAG),
          oldPolicyFragment.indexOf(SINGLE_TEMPLATE_TAG) + SINGLE_TEMPLATE_TAG.length(),
          newPolicyFragment.toString());
    }

    return oldPolicyFragment;
  }

  /**
   * <p>
   * Inserts a policy in an unfinished policy and returns the resulting policyFragment for further
   * editing.
   * </p>
   * 
   * @param oldPolicyFragment
   * @param policyName
   * @return
   * @throws Exception
   */
  public static StringBuffer buildPolicyString(StringBuffer oldPolicyFragment,
      String policyName) throws Exception {

    if (oldPolicyFragment == null) {
      oldPolicyFragment = new StringBuffer();
    }

    if (!exists(policyName)) {
      throw new Exception("No policy with name " + policyName);
    }
    StringBuffer policyFragment = new StringBuffer((String) policies.get(policyName));

    if (oldPolicyFragment.length() == 0) {
      oldPolicyFragment.append(policyFragment);
    } else {
      oldPolicyFragment = buildPolicyString(oldPolicyFragment, policyFragment);
    }

    return oldPolicyFragment;
  }

  /**
   * @param policyName The policyName to check.
   * @return Returns TRUE if the policyName exists.
   * @throws TransformerException
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   */
  public static boolean exists(String policyName)
      throws SAXException, ParserConfigurationException, IOException, TransformerException {

    if (policies == null) {
      policies = Policy.loadPolicy((String) Configurator.getValue("policyFile"));
    }

    return policies.containsKey(policyName);
  }

  // GET & SET METHODS **************************************************

  /**
   * @return Returns the processData.
   */
  public ProcessData getProcessData() {
    return this.processData;
  }

  /**
   * @param processData The processData to set.
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

}
