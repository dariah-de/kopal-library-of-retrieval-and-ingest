/**
 * Copyright 2005-2020 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 */

/**
 * TODOLOG
 ** 
 * CHANGELOG
 */

package de.langzeitarchivierung.kolibri;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import de.langzeitarchivierung.kolibri.processstarter.ProcessStarter;

/**
 * <p>
 * A pool of threads which execute ProcessStarters and ProcessData objects. If such an object is
 * finished appropriate measures are taken (e.g. rescheduling of failed objects, serializing them,
 * etc).
 * </p>
 * 
 * @author ludwig
 * @version 2020-02-19
 * @since 2006-05-19
 */

public class ProcessThreadPool extends ThreadPoolExecutor {

  // **
  // STATE (Instance variables)
  // **

  ProcessQueue pQueue;
  ProcessQueue errorQueue;

  int numberOfProcessStarters = 0;

  // **
  // CREATION (Constructors, factory methods, static/inst init)
  // **

  /**
   * @param corePoolSize
   */
  public ProcessThreadPool(int corePoolSize) {
    super(corePoolSize, corePoolSize, 10, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
  }

  // **
  // INTERNAL (Internal - implementation details, local classes, ...)
  // **

  /*
   * (non-Javadoc)
   * 
   * @see java.util.concurrent.ThreadPoolExecutor#execute(java.lang.Runnable)
   */
  @Override
  public void execute(Runnable command) {
    // To keep track how much processStarters are running and allow program exit if they all are
    // finished.
    if (command instanceof ProcessStarter) {
      this.numberOfProcessStarters++;
    }
    super.execute(command);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.util.concurrent.ThreadPoolExecutor#afterExecute(java.lang.Runnable,
   * java.lang.Throwable)
   */
  @Override
  protected void afterExecute(Runnable r, Throwable t) {
    super.afterExecute(r, t);

    // To keep track how much processStarters are running and allow program exit if they all are
    // finished.
    if (r instanceof ProcessStarter) {
      this.numberOfProcessStarters--;
      if (this.numberOfProcessStarters == 0) {
        this.pQueue.shutdownProcessQueue();
      }
      return;
    } else if (r instanceof ProcessData) {
      ProcessData lastProcess = (ProcessData) r;

      // If everything has finished we just throw the processData away.
      if (!lastProcess.getPolicy().hasOtherStatusThan(Status.DONE)) {
        return;
      }

      // We have not finished: We have either another reachable todo or not.
      else if (lastProcess.getPolicy().hasStatusWithParent(Status.TODO, Status.DONE)) {
        // We have a todo status with parent done --> proceed!
        this.pQueue.addOldElement(lastProcess);
      }

      // We have got an error or a cancelled todo.
      else if (lastProcess.getPolicy().hasStatus(Status.ERROR)
          || lastProcess.getPolicy().hasStatus(Status.CANCELED)) {
        try {
          this.errorQueue.addElement(lastProcess);
        } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }

      // What to do else?
      else {
        // TODO
      }
    }
  }

  // **
  // GET & SET METHODS
  // **

  /**
   * @param pQueue The pQueue to set.
   */
  public void setProcessQueue(ProcessQueue pQueue) {
    this.pQueue = pQueue;
  }

  /**
   * @param errorQueue The errorQueue to set.
   */
  public void setErrorQueue(ProcessQueue errorQueue) {
    this.errorQueue = errorQueue;
  }

}
