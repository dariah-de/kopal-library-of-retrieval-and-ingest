/*******************************************************************************
 * de.langzeitarchivierung.kolibri.formats / FormatConverter.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.formats;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.purl.dc.elements.x11.DcSetDocument;
import org.purl.dc.elements.x11.DcSetDocument.DcSet;

/*******************************************************************************
 * @author ludwig
 ******************************************************************************/

public class FormatConverter
{

	/***************************************************************************
	 * @param dcXml
	 * @return
	 * @throws XmlException
	 **************************************************************************/
	public static DcSet getDc(String dcXml) throws XmlException {
		XmlOptions opts = new XmlOptions();
		// Tricky! Because we get only an XML fragment of a dcSet we have to
		// deceive the Factory parser.
		opts.setLoadReplaceDocumentElement(new QName(
				"http://purl.org/dc/elements/1.1/", "dcSet", "dc"));
		return DcSetDocument.Factory.parse(dcXml).getDcSet();
	}

	// public static XmlObject getXmlObject(Class xmlBeanClass, String xml){
	// xmlBeanClass.get
	// }
	//    
	// public LmerFile getLmerFileCopy(String techMdId) throws XmlException{
	// return LmerFile.Factory.parse(getTechMdXmlData(techMdId).toString());
	// }
	//    
	// public Jhove getJhoveTechMd(File contentFile) throws XmlException,
	// FileNotFoundException{
	// return getJhoveTechMd(getFileTechMdId(contentFile));
	// }
	//
	// public Jhove getJhoveTechMd(String techMdId) throws XmlException{
	// return
	// JhoveDocument.Factory.parse(getLmerFileCopy(techMdId).getXmlDataArray(0).toString()).getJhove();
	// }
	//
	// /***********************************************************************
	// * Gets a COPY of the LmerFile of the specified file.
	// *
	// * @param contentFile The file for which we want the lmerFile.
	// * @return a COPY of the LmerFile or null if the contentFile does not
	// * exist.
	// ***********************************************************************/
	// public LmerFile getLmerFileCopy(File contentFile) throws
	// FileNotFoundException,
	// XmlException{
	// String techMdId = getFileTechMdId(contentFile);
	// if (techMdId != null){
	// return LmerFile.Factory.parse(getTechMdXmlData(techMdId).toString());
	// }
	// return null;
	// }

}
