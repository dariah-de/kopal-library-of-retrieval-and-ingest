/*******************************************************************************
 * de.langzeitarchivierung.kolibri.formats / UofExtended.java
 *  
 * Copyright 2005-2007 by Project kopal
 * Copyright 2011 by Project DP4lib
 * 
 * http://kopal.langzeitarchivierung.de/
 * http://dp4lib.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 *******************************************************************************/

package de.langzeitarchivierung.kolibri.formats;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.zip.DataFormatException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import de.ddb.lmeRfile.LmerFile;
import de.ddb.lmeRobject.LmerObject;
import gov.loc.mets.AmdSecType;
import gov.loc.mets.FileType;
import gov.loc.mets.MdSecType;
import gov.loc.mets.MdSecType.MdWrap;

/*
 * TODO Check if this class should be merged with uof.java!
 */

/**
 * @author hein
 */

public class UofExtended extends Uof {

	private static final long	serialVersionUID		= 1L;

	public static final String	FILE_LOCAT_HREF_PREFIX	= "file://./content";

	/**
	 * @param workingDir
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public UofExtended(String workingDir)
			throws IOException, ParserConfigurationException {

		super(workingDir);

	}

	/**
	 * <p>
	 * Rewrites all file data, that could have been altered within a file
	 * migration. If you already have the content file's file data in the METS
	 * metadata object, use it to update some file data for migrational use.
	 * </p>
	 * 
	 * @param path
	 * @param createDate
	 * @param sha1
	 * @param techMd
	 * @throws Exception
	 */
	public void updateFileData(String path, Calendar createDate, String sha1,
			TechnicalMetadata techMd) throws Exception {

		checkTechMdSize(techMd);

		String fileId = getFileId(path);

		// Update the file data in the file sec.
		List<FileType> fileList = this.uof.getFileSec().getFileGrpArray(0)
				.getFileList();

		for (FileType file : fileList) {
			if (file.getID().equals(fileId)) {
				file = updateFileSecFileData(path, createDate, sha1, file,
						techMd);
			}
		}

		// Update the file data in the techMd.
		String techMdId = getFileTechMdId(path);
		LmerFile lmerFile = getLmerFileCopy(techMdId);

		// Update the lmerFile data.
		lmerFile = updateLmerFileData(lmerFile, techMd);

		// Set the lmerFile data.
		List<gov.loc.mets.MdSecType> techMdList = this.uof.getAmdSecArray(0)
				.getTechMDList();
		for (gov.loc.mets.MdSecType tmd : techMdList) {
			if (tmd.getID().equals(techMdId)) {
				tmd.getMdWrap().getXmlData().set(lmerFile);
			}
		}
	}

	/**
	 * @param path
	 * @param createDate
	 * @param sha1
	 * @param techMd
	 * @throws Exception
	 */
	public void addFile(String path, Calendar createDate, String sha1,
			TechnicalMetadata techMd) throws Exception {

		checkTechMdSize(techMd);

		// Assemble the filename.
		// String fileNameToAdd = contentFile.toURI().toString()
		// .substring(this.basePathUri.toString().length() - 6);

		String fileNameToAdd = path.substring(path.lastIndexOf("/"));

		// Is this file already in the METS?
		List<FileType> files = this.uof.getFileSec().getFileGrpArray(0)
				.getFileList();
		for (FileType file : files) {
			String fileNameInFileSec = file.getFLocatArray(0).getHref()
					.substring(FILE_LOCAT_HREF_PREFIX.length());
			if (fileNameToAdd.equals(fileNameInFileSec)) {
				throw new Exception("Duplicate file in the METS");
			}
		}

		// Create a new lmer file object and update some lmerFile information.
		LmerFile lmerFile = LmerFile.Factory.newInstance();
		lmerFile.addNewFormat();
		lmerFile = updateLmerFileData(lmerFile, techMd);

		// Add new tech md and mdwrap.
		List<AmdSecType> amdSecs = this.uof.getAmdSecList();
		MdSecType tMd = amdSecs.get(0).addNewTechMD();
		String techMdId = techMdPrefix + prefixSeparator + this.fileCounter;
		tMd.setID(techMdId);

		MdWrap fileMdWrap = tMd.addNewMdWrap();
		fileMdWrap.setID(techMdId + prefixSeparator + mdWrapPostfix);
		fileMdWrap.setMIMETYPE("text/xml");
		fileMdWrap.setMDTYPE(MdWrap.MDTYPE.OTHER);
		fileMdWrap.setOTHERMDTYPE("lmerFile");
		// fileMdWrap.setLABEL("lmerFile");
		fileMdWrap.addNewXmlData();

		// Add lmer file sec.
		fileMdWrap.getXmlData().set(lmerFile);

		// Add file entry.
		FileType fileEntry = this.uof.getFileSec().getFileGrpArray(0)
				.addNewFile();

		// Create and set file ID.
		String fileId = "FILE" + prefixSeparator + this.fileCounter;
		fileEntry.setID(fileId);

		// Create and set the ADMIDs.
		List<String> admIds = new ArrayList<String>();
		admIds.add(techMdId);
		fileEntry.setADMID(admIds);

		// Add and set the file location.
		FileType.FLocat fileLocat = fileEntry.addNewFLocat();
		fileLocat.setLOCTYPE(FileType.FLocat.LOCTYPE.URL);
		fileLocat.setType("simple");

		// Update the other file data.
		fileEntry = updateFileSecFileData(path, createDate, sha1, fileEntry,
				techMd);

		// Add filepointer to struct map.
		getFirstAssetDiv().addNewFptr().setFILEID(fileId);

		// Update the lmer object section.
		this.fileCounter++;
		LmerObject lobj = getLmerObjectCopy();
		lobj.setNumberOfFiles(new BigInteger(String.valueOf(this.fileCounter)));
		setLmerObject(lobj);
	}

	/**
	 * <p>
	 * Updates the file data of one file in the file group. Used from both
	 * addFile() and updateFileData().
	 * </p>
	 * 
	 * @param path
	 * @param createDate
	 * @param sha1
	 * @param file
	 * @param techMd
	 * @return
	 * @throws Exception
	 */
	private FileType updateFileSecFileData(String path, Calendar createDate,
			String sha1, FileType file, TechnicalMetadata techMd)
			throws Exception {

		// Calendar createDate = Calendar.getInstance();
		// createDate.setTimeInMillis(contentFile.lastModified());

		// XmlCursor xmlCursor = techMd.getXmlCursor();

		file.setCREATED(createDate);
		file.setSIZE(techMd.getContentFileSize());
		file.setCHECKSUM(sha1);
		file.setCHECKSUMTYPE(FileType.CHECKSUMTYPE.SHA_1);
		file.setMIMETYPE(techMd.getMimetype());
		file.getFLocatArray(0).setHref(FILE_LOCAT_HREF_PREFIX + path);

		return file;
	}

	/**
	 * <p>
	 * Returns for a specified content file the referenced admId (in UOF it's
	 * called techMdId) in the file section. This works only if contentFile has
	 * the same absolute path as basePathUri + fileSecEntry.
	 * </p>
	 * 
	 * @param path
	 * @return
	 * @throws FileNotFoundException
	 */
	public String getFileTechMdId(String path) throws FileNotFoundException {
		List<FileType> fileList = this.uof.getFileSec().getFileGrpArray(0)
				.getFileList();

		// Go through all files.
		for (FileType file : fileList) {
			String absoluteFileSecPath = file.getFLocatArray(0).getHref()
					.substring(FILE_LOCAT_HREF_PREFIX.length());

			// It the two pathes are equal, return the TECHMD identifier (the
			// LAST identifier in the list of IDs!!).
			if (path.equals(absoluteFileSecPath)) {
				return (String) file.getADMID().get(file.getADMID().size() - 1);
			}
		}

		throw new FileNotFoundException("File not found in UOF");
	}

	/**
	 * <p>
	 * Returns for a specified content file the file id in the file section.
	 * This works only if contentFile has the same absolute path as basePathUri
	 * + fileSecEntry.
	 * </p>
	 * 
	 * @param path
	 * @return
	 * @throws FileNotFoundException
	 */
	public String getFileId(String path) throws FileNotFoundException {
		List<FileType> fileList = this.uof.getFileSec().getFileGrpArray(0)
				.getFileList();

		for (FileType file : fileList) {
			String absoluteFileSecPath = file.getFLocatArray(0).getHref()
					.substring(FILE_LOCAT_HREF_PREFIX.length());

			if (path.equals(absoluteFileSecPath)) {
				return file.getID();
			}
		}

		throw new FileNotFoundException("File not found in UOF");
	}

	/**
	 * @param out
	 * @throws IOException
	 * @throws DataFormatException
	 * @throws XmlException
	 */
	public void saveMetadataFormat(OutputStream out)
			throws IOException, DataFormatException, XmlException {

		XmlOptions savingOpts = new XmlOptions();

		savingOpts.setSaveAggressiveNamespaces();

		savingOpts.setSavePrettyPrint();

		savingOpts.setSaveOuter();

		updateUof();

		this.uof.save(out, savingOpts);
	}

}
