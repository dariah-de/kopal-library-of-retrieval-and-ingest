/*******************************************************************************
 * de.langzeitarchivierung.kolibri.formats / TechnicalMetadataJhoveImpl.java
 *  
 * Copyright 2011 by Project DP4lib
 * 
 * http://dp4lib.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 *******************************************************************************
 *
 *
 * CHANGELOG:
 * 
 *	2011-07-19	Funk	First version.
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.formats;

/*******************************************************************************
 * <p>
 * The JHOVE implementation of the TechnicalMetadata interface to include JHOVE
 * technical metadata into the UOF.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2011-08-22
 * @since 2011-08-19
 ******************************************************************************/

public abstract class TechnicalMetadataAbs implements TechnicalMetadata {

	// **
	// CLASS
	// **

	protected String	type;
	protected String	format;
	protected String	mimetype;
	protected String	version;
	protected String	value;
	protected long		size;
	protected long		contentFileSize;

	// **
	// CONSTRUCTOR
	// **

	/**
	 * <p>
	 * Constructor constructs.
	 * </p>
	 * 
	 * @param theMetadataString
	 */
	public TechnicalMetadataAbs(String theMetadataString) {
		this.value = theMetadataString;
		this.size = theMetadataString.getBytes().length;
		// Only type, format, version, and mimetype must be set in the
		// implementations itself.
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.formats.TechnicalMetadata#getFormat()
	 */
	@Override
	public String getFormat() {
		return this.format;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.formats.TechnicalMetadata#getVersion()
	 */
	@Override
	public String getVersion() {
		return this.version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.formats.TechnicalMetadata#getMimetype()
	 */
	public String getMimetype() {
		return this.mimetype;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.langzeitarchivierung.kolibri.formats.TechnicalMetadata#getValue()
	 */
	@Override
	public String getStringValue() {
		return this.value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.formats.TechnicalMetadata#getMetadataType
	 * ()
	 */
	@Override
	public String getType() {
		return this.type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.langzeitarchivierung.kolibri.formats.TechnicalMetadata#getSize()
	 */
	@Override
	public long getSize() {
		return this.size;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.formats.TechnicalMetadata#getContentFileSize
	 * ()
	 */
	public long getContentFileSize() {
		return this.contentFileSize;
	}

}
