/*******************************************************************************
 * de.langzeitarchivierung.kolibri.formats / TechnicalMetadata.java
 *  
 * Copyright 2011 by Project DP4lib
 * 
 * http://dp4lib.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 *	2011-08-19	Funk	First version.
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.formats;

import org.apache.xmlbeans.XmlCursor;

/*******************************************************************************
 * <p>
 * This interface generalises the technical metadata of a content file. So UOF
 * must cope with at least JHOVE and FITS technical metadata at the moment.
 * Please add implementations as you wish.
 * </p>
 * 
 * @author Stefan E. Funk
 * @version 2011-08-19
 * @since 2011-08-19
 ******************************************************************************/

public interface TechnicalMetadata {

	/**
	 * <p>
	 * Delivers the file format of the file, this technical metadata belongs to.
	 * </p>
	 * 
	 * @return
	 */
	public String getFormat();

	/**
	 * <p>
	 * Delivers the file format version of the file, this technical metadata
	 * belongs to.
	 * </p>
	 * 
	 * @return
	 */
	public String getVersion();

	/**
	 * <p>
	 * Delivers the mimetype of the file, this technical metadata belongs to.
	 * </p>
	 * 
	 * @return
	 */
	public String getMimetype();

	/**
	 * <p>
	 * Delivers the technical metadata as XML string (to be independent of XML
	 * formats).
	 * </p>
	 * 
	 * @return
	 */
	public String getStringValue();

	/**
	 * <p>
	 * Delivers the technical metadata as XML cursor.
	 * </p>
	 * 
	 * @return
	 */
	public XmlCursor getXmlCursor();

	/**
	 * <p>
	 * Delivers the technical metadata type, e.g. JHOVE or FITS..
	 * </p>
	 * 
	 * @return
	 */
	public String getType();

	/**
	 * <p>
	 * Delivers the file size of the technical metadata XML string in bytes.
	 * </p>
	 * 
	 * @return
	 */
	public long getSize();

	/**
	 * <p>
	 * Delivers the file size of the content file in bytes.
	 * </p>
	 * 
	 * @return
	 */
	public long getContentFileSize();

}
