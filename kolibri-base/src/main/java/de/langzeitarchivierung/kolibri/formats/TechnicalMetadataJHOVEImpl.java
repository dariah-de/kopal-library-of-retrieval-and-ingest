/**
 * de.langzeitarchivierung.kolibri.formats / TechnicalMetadataJhoveImpl.java
 * 
 * Copyright 2024 by Project DP4lib
 * 
 * http://dp4lib.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 * 
 **
 *
 * CHANGELOG
 * 
 * 2011-07-19 - Funk - First version.
 */

package de.langzeitarchivierung.kolibri.formats;

import java.io.IOException;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import edu.harvard.hul.ois.xml.ns.jhove.JhoveDocument;
import edu.harvard.hul.ois.xml.ns.jhove.JhoveDocument.Jhove;

/**
 * <p>
 * The JHOVE implementation of the TechnicalMetadata interface to include JHOVE technical metadata
 * into the UOF.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */

public class TechnicalMetadataJHOVEImpl extends TechnicalMetadataAbs {

  // **
  // FINALS
  // **

  public static final String METADATA_TYPE = "JHOVE";

  // **
  // CLASS
  // **

  private Jhove jhoveInfo;

  // **
  // CONSTRUCTOR
  // **

  /**
   * <p>
   * Constructor constructs.
   * </p>
   * 
   * @param theMetadataString
   * @throws IOException
   */
  public TechnicalMetadataJHOVEImpl(String theMetadataString) throws IOException {

    // First call superclass to be sure everything besides the format and version is correctly set.
    super(theMetadataString);

    // Set type from final String.
    this.type = METADATA_TYPE;

    // Now set format, version, and mimetype from the JHOVE metadata string.
    try {
      JhoveDocument jd = JhoveDocument.Factory.parse(theMetadataString);

      this.jhoveInfo = jd.getJhove();
      this.format = this.jhoveInfo.getRepInfoArray(0).getFormat();
      this.mimetype = this.jhoveInfo.getRepInfoArray(0).getMimeType();
      this.version = this.jhoveInfo.getRepInfoArray(0).getVersion();
      this.contentFileSize = this.jhoveInfo.getRepInfoArray(0).getSize().longValue();
    } catch (XmlException e) {
      throw new IOException(e);
    }
  }

  /**
   *
   */
  @Override
  public XmlCursor getXmlCursor() {
    return this.jhoveInfo.newCursor();
  }

}
