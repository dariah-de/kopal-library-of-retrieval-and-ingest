/*******************************************************************************
 * de.langzeitarchivierung.kolibri.formats / MetadataFormat.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 	2011-08-22	Funk	Adapted to the new TechnicalMetadata object.
 *	2007-07-10	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.formats;

import java.io.File;
import java.net.MalformedURLException;
import java.util.List;

import de.langzeitarchivierung.kolibri.util.KolibriClassLoader;

/*******************************************************************************
 * <p>
 * The Interface for the metadata creation. Please implement any metadata format
 * you want to create with koLibRI! As used by kopal and as an example the
 * Universal Object Format (UOF) already is implemented.
 * </p>
 * 
 * @author ludwig
 ******************************************************************************/

public interface MetadataFormat {

	/***************************************************************************
	 * <p>
	 * Returns the identifier of the object.
	 * </p>
	 * 
	 * @return the object identifier
	 * @throws Exception
	 **************************************************************************/
	public String getObjectId() throws Exception;

	/***************************************************************************
	 * <p>
	 * Sets the object identifier.
	 * </p>
	 * 
	 * @param id
	 *            The string which should identify the object.
	 * @throws Exception
	 **************************************************************************/
	public void setObjectId(String id) throws Exception;

	/***************************************************************************
	 * <p>
	 * Adds descriptive metadata to the metadata object.
	 * </p>
	 * 
	 * @param dmd
	 *            The descriptive metadata.
	 * @param mdType
	 *            The name of the descriptive metadata type, e.g. dc for dublin
	 *            core.
	 * @throws Exception
	 **************************************************************************/
	public void addDescMd(String dmd, String mdType) throws Exception;

	/***************************************************************************
	 * <p>
	 * Returns stored descriptive metadata.
	 * </p>
	 * 
	 * @return
	 **************************************************************************/
	public List<String> getDescMd() throws Exception;

	/***************************************************************************
	 * <p>
	 * Adds a file and its technical metadata to the metadata object.
	 * </p>
	 * 
	 * @param contentFile
	 *            The file to add.
	 * @param techMd
	 *            The technical metadata to add.
	 * @throws Exception
	 *             If something goes wrong while accessing the file or metadata
	 **************************************************************************/
	public void addFile(File contentFile, TechnicalMetadata techMd)
			throws Exception;

	/***************************************************************************
	 * <p>
	 * Get all files in the metadata.
	 * </p>
	 * 
	 * @return All files in the metadata with absolute path
	 * @throws Exception
	 **************************************************************************/
	public File[] getFiles() throws Exception;

	/***************************************************************************
	 * <p>
	 * Returns the metadata of a file in the metadata object.
	 * </p>
	 * 
	 * @param contentFile
	 *            The content file for which the metadata should be returned
	 * @return the complete metadata for a file TODO returns null if content
	 *         file is not found?
	 * @throws Exception
	 **************************************************************************/
	public String getFileMd(File contentFile) throws Exception;

	/***************************************************************************
	 * <p>
	 * Returns only the technical metadata of a file in the metadata object.
	 * </p>
	 * 
	 * @param contentFile
	 *            The content file for which the technical metadata should be
	 *            returned TODO returns null if content file is not found?
	 * @return the technical metadata for a file
	 * @throws Exception
	 **************************************************************************/
	public String getTechMd(File contentFile) throws Exception;

	/***************************************************************************
	 * <p>
	 * Adds provenance metadata for the whole object.
	 * </p>
	 * 
	 * TODO Change provMd to String?
	 * 
	 * @param provMd
	 * @throws Exception
	 **************************************************************************/
	public void addProvMd(Object provMd) throws Exception;

	/***************************************************************************
	 * <p>
	 * Adds provenance metadata for a given file.
	 * </p>
	 * 
	 * TODO Change provMd to String?
	 * 
	 * @param contentFile
	 * @param provMd
	 * @throws Exception
	 **************************************************************************/
	public void addProvMd(File contentFile, Object provMd) throws Exception;

	/***************************************************************************
	 * <p>
	 * Returns stored provenance metadata for the given file.
	 * </p>
	 * 
	 * @param contentFile
	 * @return
	 * @throws Exception
	 **************************************************************************/
	public List<String> getProvMd(File contentFile) throws Exception;

	/***************************************************************************
	 * <p>
	 * Returns the objects provenance metadata.
	 * </p>
	 * 
	 * @return
	 * @throws Exception
	 **************************************************************************/
	public List<String> getProvMd() throws Exception;

	// TODO These have to be added later:
	//
	// /***************************************************************************
	// *
	// * @param data
	// * @param contentFileToReference
	// **************************************************************************/
	// public void addCustomData(String data, File contentFileToReference);
	//
	// /***************************************************************************
	// *
	// * @param contentFile
	// * @return
	// **************************************************************************/
	// public String getCustomData(File contentFile);

	/***************************************************************************
	 * <p>
	 * This sets the metadata object to the values of the specified file.
	 * </p>
	 * 
	 * @param templateFile
	 *            The file to use as template
	 * @throws Exception
	 *             If an error occurs while accessing and parsing the template
	 *             file.
	 **************************************************************************/
	public void useTemplate(File templateFile) throws Exception;

	/***************************************************************************
	 * <p>
	 * This writes the metadata to the specified file.
	 * </p>
	 * 
	 * @param outputFile
	 *            Where
	 * @throws Exception
	 *             If something goes wrong while writing.
	 **************************************************************************/
	public void saveMetadataFormat(File outputFile) throws Exception;

	/***************************************************************************
	 * <p>
	 * This returns the actual implementation which may offer more
	 * functionality.
	 * </p>
	 * 
	 * If you use this function you may risk inconsistency depending on your
	 * implementation.
	 * 
	 * @return The object which implements the interface
	 **************************************************************************/
	public Object getMetadataImplementation();

	// INTERNAL (Internal - implementation details, local classes, ...) ******

	/***************************************************************************
	 * <p>
	 * This creates an instance of the metadata format. You have to set the
	 * format and a template first or you have to specify them as parameters of
	 * newInstance.
	 * </p>
	 * 
	 * @author Ludwig
	 **************************************************************************/
	static class Factory {

		// CLASS VARIABLES (Static variables) **********************************

		static Class	metadataClass	= null;
		static File		templateFile	= null;

		/***********************************************************************
		 * <p>
		 * Creates a metadata object of the default class and template.
		 * </p>
		 * 
		 * @return A default instance of MetadataFormat
		 * @throws Exception
		 *             If something is wrong with loading the default class or
		 *             template.
		 **********************************************************************/
		public static MetadataFormat newInstance() throws Exception {
			MetadataFormat md = (MetadataFormat) KolibriClassLoader
					.getInstanceOfClass(metadataClass);
			md.useTemplate(templateFile);

			return md;
		}

		/***********************************************************************
		 * <p>
		 * Creates a metadata object of the specified class and template.
		 * </p>
		 * 
		 * @param mdClassName
		 *            The metadata format class name to load. It has to be the
		 *            full class name with package path. TODO allow short form
		 * @param mdTemplateFile
		 *            file with the default metadata structure and values.
		 * @return An instance of the specified MetadataFormat
		 * @throws Exception
		 *             If something is wrong with loading the default class or
		 *             template.
		 **********************************************************************/
		public static MetadataFormat newInstance(String mdClassName,
				String mdTemplateFile) throws Exception {
			MetadataFormat md = (MetadataFormat) KolibriClassLoader
					.getInstanceOfClass(KolibriClassLoader
							.getFormat(mdClassName));
			md.useTemplate(new File(mdTemplateFile));

			return md;
		}

		/***********************************************************************
		 * <p>
		 * Creates a metadata object of the specified class and template.
		 * </p>
		 * 
		 * @param mdClassName
		 *            The metadata format class name to load. It has to be the
		 *            full class name with package path. TODO allow short form
		 * @param mdTemplate
		 *            file with the default metadata structure and values.
		 * @return An instance of the specified MetadataFormat
		 * @throws Exception
		 *             If something is wrong with loading the default class or
		 *             template.
		 **********************************************************************/
		public static MetadataFormat newInstance(String mdClassName,
				File mdTemplate) throws Exception {
			MetadataFormat md = (MetadataFormat) KolibriClassLoader
					.getInstanceOfClass(KolibriClassLoader
							.getFormat(mdClassName));
			md.useTemplate(mdTemplate);

			return md;
		}

		/***********************************************************************
		 * <p>
		 * Sets the className of the implementing metadata format.
		 * </p>
		 * 
		 * @param mdClassName
		 *            The metadata format class name to load. It has to be the
		 *            full class name with package path. TODO allow short form
		 * @throws MalformedURLException
		 * @throws ClassNotFoundException
		 **********************************************************************/
		public static void setMdClassName(String mdClassName)
				throws MalformedURLException, ClassNotFoundException {
			metadataClass = KolibriClassLoader.getFormat(mdClassName);
		}

		/***********************************************************************
		 * <p>
		 * Sets the file with the default metadata structure and values.
		 * </p>
		 * 
		 * @param mdTemplateFile
		 *            file with the default metadata structure and values.
		 **********************************************************************/
		public static void setMdTemplateFile(String mdTemplateFile) {
			templateFile = new File(mdTemplateFile);
		}
	}

}
