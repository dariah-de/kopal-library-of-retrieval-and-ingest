/**
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Ludwig
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.formats;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.DataFormatException;
import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.purl.dc.elements.x11.DcSetDocument;
import org.purl.dc.elements.x11.DcSetDocument.DcSet;
import de.ddb.lmeRfile.LmerFile;
import de.ddb.lmeRobject.LmerObject;
import de.ddb.lmeRobject.LmerObject.TransferChecksum;
import de.ddb.lmeRprocess.LmerProcess;
import de.langzeitarchivierung.kolibri.util.ChecksumGenerator;
import de.langzeitarchivierung.kolibri.util.Configurator;
import de.langzeitarchivierung.kolibri.util.FormatRegistry;
import edu.harvard.hul.ois.xml.ns.jhove.JhoveDocument;
import edu.harvard.hul.ois.xml.ns.jhove.JhoveDocument.Jhove;
import gov.loc.mets.AmdSecType;
import gov.loc.mets.DivType;
import gov.loc.mets.FileType;
import gov.loc.mets.MdSecType;
import gov.loc.mets.MdSecType.MdRef.MDTYPE;
import gov.loc.mets.MdSecType.MdWrap;
import gov.loc.mets.MetsDocument;
import gov.loc.mets.MetsType;
import gov.loc.mets.StructMapType;

/**
 * TODOLOG
 * 
 * TODO Remove JHOVE dependencies, change to TechnicalMetadata object.
 * 
 **
 * CHANGELOG
 * 
 * 2019-12-02 - Funk - reformatted due to SUB FE Java formatting rules.
 * 
 * 2011-09-29 - Funk - Added format, mimetype, and version maps to store the entries of the
 * technical metadata objects. Used to store format, mimetype, and version to the koLibRI DB.
 * 
 * 2011-07-22 - Funk - Adapted to the new TechnicalMetadata object, now we support all technical
 * metadata types that are XML conform, and do implement the TechnicalMetadata interface, e.g.
 * JHOVE, and FITS input. Changed some XMLBeans arrays into list as well.
 * 
 * 2008-01-24 - Funk - Fixed some warnings concerning typed Lists and Iterators.
 * 
 * 2008-01-24 - Funk - Adapted the AddProvMd code to the incident #31 solution: No provenance
 * history for files in the file's ADMID list!
 * 
 * 2007-07-10 koLibRI version 1.0
 */

/**
 * <p>
 * The kopal Universal Object Format (UOF).
 * </p>
 * 
 * <p>
 * To get more information please go to <a href=
 * "http://kopal.langzeitarchivierung.de/index_objektspezifikation.php.en">http://kopal.langzeitarchivierung.de/index_objektspezifikation.php.en</a>
 * </p>
 * 
 * <p>
 * CODE SNIPPET if you have to create a METS doc:
 * </p>
 * 
 * <code>
 * MetsDocument mets = MetsDocument.Factory.newInstance();
 * uof = mets.addNewMets();
 * XmlCursor cursor = mets.newCursor();
 * 
 * if (cursor.toFirstChild()) {
 * 		System.out.println("Setting attributes");
 *     	cursor.setAttributeText(
 *      	new QName("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation"),
 *       	"http://www.loc.gov/METS/ http://www.loc.gov/standards/mets/mets.xsd " +
 *       	"http://www.ddb.de/LMERfile " +
 *       	"http://www.ddb.de/standards/lmer/lmer-file.xsd " +
 *       	"http://www.ddb.de/LMERobject " +
 *       	"http://www.ddb.de/standards/lmer/lmer-object.xsd " +
 *       	"http://www.ddb.de/LMERprocess " +
 *       	"http://www.ddb.de/standards/lmer/lmer-process.xsd " +
 *       	"http://purl.org/dc/elements/1.1/ " +
 *       	"http://dublincore.org/schemas/xmls/qdc/2003/04/02/dc.xsd"
 *    	);
 * }
 *  
 * cursor.dispose();
 * </code>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2019-12-02
 */

public class Uof implements MetadataFormat, Serializable {

  // **
  // CONSTANTS
  // **

  private static final long serialVersionUID = 1L;
  public static final String FILE_LOCAT_HREF_PREFIX = "file://./";

  // **
  // CLASS VARIABLES (Static variables)
  // **

  protected static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");

  // Limits for DIAS
  public static int diasDcDescriptionLimit = 1024;
  public static int diasDcIdentifierLimit = 255;
  public static int diasDcTypeLimit = 100;

  // Prefixes used in the METS file
  private static String dmdPrefix = "DMD";
  protected static String techMdPrefix = "TECHMD";
  private static String digiprovMdPrefix = "DIGIPROVMD";
  protected static String mdWrapPostfix = "MDWRAP";
  private static String lmerProcessObjectID = "DIGIPROVMD-OBJECT";

  // prefixSeparator can be the empty string
  protected static String prefixSeparator = "-";
  protected static FormatRegistry fRegistry;

  // **
  // STATE (Instance variables)
  // **

  private int dmdCounter = 0;
  protected int fileCounter = 0;

  // The first part of the content file path which is omitted in the METS file. The default path is
  // not very useful and should be changed.
  protected URI basePathUri = new File(System.getProperty("user.dir")).toURI();
  private boolean noWhiteSpacesInTechMd = false;
  private boolean checkTechMdSize = true;
  private long maxTechMDLength = 262144;

  // Format, mimetype and version from TechnicalMetadata.
  // TODO Refactor later!
  private HashMap<String, String> formatMap = new HashMap<String, String>();
  private HashMap<String, String> mimetypeMap = new HashMap<String, String>();
  private HashMap<String, String> versionMap = new HashMap<String, String>();

  // This contains the complete XmlBeans metadata classes.
  protected MetsType uof;

  // **
  // CREATION
  // **

  /**
   * <p>
   * Constructor. Object configures itself.
   * </p>
   * 
   * @throws IOException
   * @throws ParserConfigurationException
   */
  public Uof() throws IOException, ParserConfigurationException {

    super();

    fRegistry = new FormatRegistry();

    try {
      Configurator.configure(this);
    } catch (Exception e) {
      e.printStackTrace();
      defaultLogger.log(Level.SEVERE, "Error configuring class " + this.getClass().getName()
          + ". System message: " + e.getMessage());
    }
  }

  /**
   * <p>
   * Constructor. Object configures itself.
   * </p>
   * 
   * @throws ParserConfigurationException
   * @throws IOException
   */
  public Uof(String workingDir) throws IOException, ParserConfigurationException {

    super();

    fRegistry = new FormatRegistry(workingDir);

    try {
      Configurator.configure(this);
    } catch (Exception e) {
      e.printStackTrace();
      defaultLogger.log(Level.SEVERE, "Error configuring class " + this.getClass().getName()
          + ". System message: " + e.getMessage());
    }
  }

  // **
  // MANIPULATION
  // **

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#getObjectId()
   */
  public String getObjectId() throws XmlException {
    return getLmerObjectCopy().getPersistentIdentifier();
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#setObjectId(java .lang.String)
   */
  public void setObjectId(String id) throws XmlException, DataFormatException {
    LmerObject lobj = getLmerObjectCopy();
    lobj.setPersistentIdentifier(id);
    setLmerObject(lobj);
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#addDmd(java.lang .String,
   * java.lang.String)
   */
  public void addDescMd(String dmd, String mdType) throws XmlException {

    MdWrap dmdwrap = getNewDmdWrap();
    dmdwrap.setMIMETYPE("text/xml");
    MdWrap.MDTYPE.Enum metsMdTypes = MdWrap.MDTYPE.OTHER;

    if (mdType.equalsIgnoreCase("dc")) {
      metsMdTypes = MdWrap.MDTYPE.DC;
    } else if (mdType.equalsIgnoreCase("ddi")) {
      metsMdTypes = MdWrap.MDTYPE.DDI;
    } else if (mdType.equalsIgnoreCase("ead")) {
      metsMdTypes = MdWrap.MDTYPE.EAD;
    } else if (mdType.equalsIgnoreCase("fdgc")) {
      metsMdTypes = MdWrap.MDTYPE.FGDC;
    } else if (mdType.equalsIgnoreCase("lc_av")) {
      metsMdTypes = MdWrap.MDTYPE.LC_AV;
    } else if (mdType.equalsIgnoreCase("lom")) {
      metsMdTypes = MdWrap.MDTYPE.LOM;
    } else if (mdType.equalsIgnoreCase("marc")) {
      metsMdTypes = MdWrap.MDTYPE.MARC;
    } else if (mdType.equalsIgnoreCase("mods")) {
      metsMdTypes = MdWrap.MDTYPE.MODS;
    } else if (mdType.equalsIgnoreCase("nisoimg")) {
      metsMdTypes = MdWrap.MDTYPE.NISOIMG;
    } else if (mdType.equalsIgnoreCase("teihdr")) {
      metsMdTypes = MdWrap.MDTYPE.TEIHDR;
    } else if (mdType.equalsIgnoreCase("vra")) {
      metsMdTypes = MdWrap.MDTYPE.VRA;
    } else {
      // MdType is "other", add the MdType now.
      dmdwrap.setOTHERMDTYPE(mdType);
    }

    dmdwrap.setMDTYPE(metsMdTypes);

    XmlCursor dmdCursor = dmdwrap.addNewXmlData().newCursor();
    dmdCursor.toEndToken();

    XmlCursor sourceCursor = null;

    // Ensure the DC descriptive metadata is not too long.
    if (metsMdTypes == MdWrap.MDTYPE.DC) {
      XmlOptions opts = new XmlOptions();
      // Tricky! Because we get only an XML fragment of a dcSet we have to deceive the factory
      // parser.
      opts.setLoadReplaceDocumentElement(
          new QName("http://purl.org/dc/elements/1.1/", "dcSet", "dc"));
      DcSet dcDmd = this.getCorrectedFieldLength(DcSetDocument.Factory.parse(dmd, opts).getDcSet());
      sourceCursor = dcDmd.newCursor();
    } else {
      sourceCursor = XmlObject.Factory.parse(dmd).newCursor();
    }

    sourceCursor.toFirstContentToken();

    while (sourceCursor.toNextSibling()) {
      sourceCursor.copyXml(dmdCursor);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#addFile(java.io. File,
   * java.lang.String)
   */
  public void addFile(File contentFile, TechnicalMetadata techMd) throws Exception {

    checkTechMdSize(techMd);

    // Check the base path.
    if (!contentFile.toURI().toString().startsWith(this.basePathUri.toString())) {
      throw new Exception("File is not in the base path " + this.basePathUri.toString());
    }

    // Assemble the filename.
    String fileNameToAdd =
        contentFile.toURI().toString().substring(this.basePathUri.toString().length() - 6);

    // Is this file already in the METS?
    List<FileType> files = this.uof.getFileSec().getFileGrpArray(0).getFileList();
    for (FileType file : files) {
      String fileNameInFileSec =
          file.getFLocatArray(0).getHref().substring(FILE_LOCAT_HREF_PREFIX.length());
      if (fileNameToAdd.equals(fileNameInFileSec)) {
        throw new Exception("Duplicate file in the METS");
      }
    }

    // Create a new LMER file object and update some lmerFile information.
    LmerFile lmerFile = LmerFile.Factory.newInstance();
    lmerFile.addNewFormat();
    lmerFile = updateLmerFileData(lmerFile, techMd);

    // Add new TECHMD and MDWRAP.
    List<AmdSecType> amdSecs = this.uof.getAmdSecList();
    MdSecType tMd = amdSecs.get(0).addNewTechMD();
    String techMdId = techMdPrefix + prefixSeparator + this.fileCounter;
    tMd.setID(techMdId);

    MdWrap fileMdWrap = tMd.addNewMdWrap();
    fileMdWrap.setID(techMdId + prefixSeparator + mdWrapPostfix);
    fileMdWrap.setMIMETYPE("text/xml");
    fileMdWrap.setMDTYPE(MdWrap.MDTYPE.OTHER);
    fileMdWrap.setOTHERMDTYPE("lmerFile");
    // fileMdWrap.setLABEL("lmerFile");
    fileMdWrap.addNewXmlData();

    // Add LMER file section.
    fileMdWrap.getXmlData().set(lmerFile);

    // Add file entry.
    FileType fileEntry = this.uof.getFileSec().getFileGrpArray(0).addNewFile();

    // Create and set file ID.
    String fileId = "FILE" + prefixSeparator + this.fileCounter;
    fileEntry.setID(fileId);

    // Create and set the ADMIDs.
    List<String> admIds = new ArrayList<String>();
    admIds.add(techMdId);
    fileEntry.setADMID(admIds);

    // Add and set the file location.
    FileType.FLocat fileLocat = fileEntry.addNewFLocat();
    fileLocat.setLOCTYPE(FileType.FLocat.LOCTYPE.URL);
    fileLocat.setType("simple");

    // Update the other file data.
    fileEntry = updateFileSecFileData(contentFile, fileEntry, techMd);

    // Add file pointer to structural map.
    getFirstAssetDiv().addNewFptr().setFILEID(fileId);

    // Update the LMER object section.
    this.fileCounter++;
    LmerObject lobj = getLmerObjectCopy();
    lobj.setNumberOfFiles(new BigInteger(String.valueOf(this.fileCounter)));
    setLmerObject(lobj);
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#getFiles()
   */
  public File[] getFiles() throws URISyntaxException {

    FileType files[] = this.uof.getFileSec().getFileGrpArray(0).getFileArray();
    File result[] = new File[files.length];

    for (int i = 0; i < files.length; i++) {
      String fileNameInFileSec =
          files[i].getFLocatArray(0).getHref().substring(FILE_LOCAT_HREF_PREFIX.length());
      result[i] = new File(new URI(this.basePathUri.toString() + fileNameInFileSec));
    }

    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#getFileMd(java.io .File)
   */
  public String getFileMd(File contentFile) throws Exception {

    List<FileType> fileList = this.uof.getFileSec().getFileGrpArray(0).getFileList();

    for (FileType file : fileList) {
      String absoluteFileSecPath = this.basePathUri
          + file.getFLocatArray(0).getHref().substring(FILE_LOCAT_HREF_PREFIX.length());
      if (contentFile.toURI().toString().equals(absoluteFileSecPath)) {
        return file.toString();
      }
    }

    throw new FileNotFoundException("File not found in UOF");
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#getTechMd(java.io .File)
   */
  public String getTechMd(File contentFile) throws FileNotFoundException, XmlException {
    return getLmerFileCopy(getFileTechMdId(contentFile)).getXmlDataArray(0).toString();
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#addMigration(java .lang.String)
   */
  public void addProvMd(Object provMd) throws Exception {

    String digiprovMdId = "";

    // Cast the java object to LmerProcess.
    LmerProcess lmerProcessFragment = (LmerProcess) provMd;

    // Get the list of ADMIDs from the file group.
    List<String> admIdList = this.uof.getFileSec().getFileGrpArray(0).getADMID();

    // Check the old one and use the amount of IDs as new counter. Use the list size as new counter:
    // list size - 1 (TECHMD ID entry) + 1 (new DIGIPROV entry) - 1 (we computer scientists start
    // counting at 0 :-).
    if (admIdList.size() > 0) {
      digiprovMdId = lmerProcessObjectID + prefixSeparator + (admIdList.size() - 1);
    }

    // Add the file into the DIGIPROV section.
    this.addDigiprovSec(lmerProcessFragment, digiprovMdId);

    // Set the DIGIPROV ID in the file group's ADMID. At first add the newly created migration ID,
    // afterwards add all other IDs with the TechMdId at the end.

    // TODO NOT TESTED YET!!!!! BUT Should work after all! :-)

    // Create the new list here.
    List<String> newAdmIdList = new ArrayList<String>();
    // Add the new AMDID.
    newAdmIdList.add(digiprovMdId);
    // Add all the existing entries.
    newAdmIdList.addAll(this.uof.getFileSec().getFileGrpArray(0).getADMID());
    // Finally set the new list here.
    this.uof.getFileSec().getFileGrpArray(0).setADMID(newAdmIdList);
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#addMigration(java .io.File,
   * java.lang.String)
   */
  public void addProvMd(File contentFile, Object provMd) throws Exception {

    String digiprovMdId = "";

    // Cast the java object to LmerProcess.
    LmerProcess lmerProcessFragment = (LmerProcess) provMd;

    // Get the FILEID of the current file.
    String fileId = getFileId(contentFile);

    // Get the ADMID from the last existing migration.
    List<String> admIdList = getFileMdMets(contentFile).getADMID();

    // Check the old one and use the amount of IDs as new counter. Use the list size as new counter:
    // list size - 1 (TECHMD ID entry) + 1 (new digiprov entry) - 1 (we computer scientists start
    // counting at 0 :-).
    if (admIdList.size() > 0) {
      digiprovMdId =
          digiprovMdPrefix + prefixSeparator + fileId + prefixSeparator + (admIdList.size() - 1);
    }

    // Add the file into the digiprov section.
    this.addDigiprovSec(lmerProcessFragment, digiprovMdId);

    // Add the digiprov ID in the file's admid. We take the first entry (as it must be the reference
    // to the lmerProcess section), and set the
    // value to the new ADMID.
    // NOTE We just exchange the reference to the lmerProcess section here, because in the UOF only
    // one ADMID reference per file is allowed! This is different to the object's provenance
    // history, see addProvMd(Object provMd)!

    // Get the current list.
    List<String> currentAdmIdList = getFileMdMets(contentFile).getADMID();
    // Finally set the new ID in exchange for the old one here.
    currentAdmIdList.set(0, digiprovMdId);
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#getProvMd(java.io .File)
   */
  public List<String> getProvMd(File contentFile) throws Exception {

    List<String> result = new ArrayList<String>();

    // Get the list of the file's ADMIDs.
    List<String> admIdList = getFileMdMets(contentFile).getADMID();
    List<LmerProcess> lmerProcessList = this.getLmerProcessCopy(admIdList);

    Iterator<LmerProcess> iter = lmerProcessList.iterator();
    while (iter.hasNext()) {
      result.add((iter.next()).toString());
    }

    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#getProvMd()
   */
  public List<String> getProvMd() throws Exception {

    List<String> result = new ArrayList<String>();

    // Get the list of ADMIDs from the file group.
    List<String> admIdList = this.uof.getFileSec().getFileGrpArray(0).getADMID();
    List<LmerProcess> lmerProcessList = this.getLmerProcessCopy(admIdList);

    Iterator<LmerProcess> iter = lmerProcessList.iterator();
    while (iter.hasNext()) {
      result.add((iter.next()).toString());
    }

    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#getDescMd()
   */
  public List<String> getDescMd() throws Exception {

    List<String> result = new ArrayList<String>();

    // Get the list of DMDIDs from the structural map.
    List<String> dmdIdList = this.uof.getStructMapArray(0).getDiv().getDMDID();
    List<MdSecType> dmdSecList = this.getDmdSecCopy(dmdIdList);

    Iterator<MdSecType> iter = dmdSecList.iterator();
    while (iter.hasNext()) {
      result.add((iter.next()).toString());
    }

    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#useTemplate(java .io.File)
   */
  public void useTemplate(File templateFile) throws Exception {
    MetsDocument xml = MetsDocument.Factory.parse(templateFile);
    this.uof = xml.getMets();
    defaultLogger.log(Level.FINEST, "METS template file " + "sucessfully loaded");
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.formats.MetadataFormat#saveMetadataFormat (java.io.File)
   */
  public void saveMetadataFormat(File outputFile)
      throws IOException, DataFormatException, XmlException {

    XmlOptions savingOpts = new XmlOptions();

    // NOTE This prefixes already are in the template file:
    // HashMap suggestedPrefixes = new HashMap();
    // suggestedPrefixes.put("http://www.loc.gov/METS/", "");
    // suggestedPrefixes.put("http://purl.org/dc/elements/1.1/", "dc");
    // suggestedPrefixes.put("http://www.w3.org/2001/XMLSchema-instance", "xsi");
    // suggestedPrefixes.put("http://www.ddb.de/LMERfile", "lmerFile");
    // suggestedPrefixes.put("http://www.ddb.de/LMERobject", "lmerObject");
    // suggestedPrefixes.put("http://www.ddb.de/LMERprocess", "lmerProcess");
    // suggestedPrefixes.put("http://www.w3.org/1999/xlink", "xlink");
    // opts.setSaveSuggestedPrefixes(suggestedPrefixes);

    savingOpts.setSaveAggressiveNamespaces();

    // savingOpts.setSavePrettyPrint();
    savingOpts.setSaveOuter();

    updateUof();

    this.uof.save(outputFile, savingOpts);
  }

  /*
   * (non-Javadoc)
   * 
   * @seede.langzeitarchivierung.kolibri.formats.MetadataFormat# getMetadataImplementation()
   */
  public Object getMetadataImplementation() {
    return this.uof;
  }

  // **
  // MANIPULATION (Manipulation - further methods)
  // **

  /**
   * <p>
   * Rewrites all file data, that could have been altered within a file migration. If you already
   * have the content file's file data in the METS metadata object, use it to update some file data
   * for migration use.
   * </p>
   * 
   * @param contentFile
   * @param techMd
   * @throws Exception
   */
  public void updateFileData(File contentFile, TechnicalMetadata techMd) throws Exception {

    checkTechMdSize(techMd);

    String fileId = getFileId(contentFile);

    // Update the file data in the file section.
    List<gov.loc.mets.FileType> fileList = this.uof.getFileSec().getFileGrpArray(0).getFileList();

    for (gov.loc.mets.FileType file : fileList) {
      if (file.getID().equals(fileId)) {
        file = updateFileSecFileData(contentFile, file, techMd);
      }
    }

    // Update the file data in the techMd.
    String techMdId = getFileTechMdId(contentFile);
    LmerFile lmerFile = getLmerFileCopy(techMdId);

    // Update the lmerFile data.
    lmerFile = updateLmerFileData(lmerFile, techMd);

    // Set the lmerFile data.
    List<gov.loc.mets.MdSecType> techMdList = this.uof.getAmdSecArray(0).getTechMDList();
    for (gov.loc.mets.MdSecType tmd : techMdList) {
      if (tmd.getID().equals(techMdId)) {
        tmd.getMdWrap().getXmlData().set(lmerFile);
      }
    }
  }

  /**
   * <p>
   * LMER specific add-method for "linked-to" fields.
   * </p>
   * 
   * @param contentFile - The content file
   * @param dependencies - The list of dependencies
   * @throws FileNotFoundException
   * @throws Exception
   */
  public void addLmerFileLinkedTo(File contentFile, List<File> dependencies)
      throws FileNotFoundException, Exception {

    LmerFile lFile = getLmerFileCopy(contentFile);

    for (int i = 0; i < dependencies.size(); i++) {
      lFile.addLinkedTo(getFileId(dependencies.get(i)));
    }

    setLmerFile(lFile, getFileTechMdId(contentFile));
  }

  /**
   * <p>
   * Gets a COPY of the LmerObject. Please do not forget to set the lmerObject again afterwards.
   * </p>
   * 
   * @return LmerObject - A COPY of the LmerObject.
   * @throws XmlException
   */
  public LmerObject getLmerObjectCopy() throws XmlException {
    return LmerObject.Factory.parse(getTechMdXmlData(getObjectTechMdId()).toString());
  }

  /**
   * <p>
   * Sets the LmerObject and recalculates the transfer checksum. The transfer checksum is the XORed
   * file checksums.
   * </p>
   * 
   * @param lObj the LmerObject to set.
   * @throws XmlException
   * @throws DataFormatException
   */
  public void setLmerObject(LmerObject lObj) throws XmlException, DataFormatException {
    // Update the checksums.
    getTechMdXmlData(getObjectTechMdId()).set(recalculateObjectChecksum(lObj));
  }

  /**
   * <p>
   * Gets a COPY of the LmerFile of the specified file.
   * </p>
   * 
   * @param contentFile The file for which we want the lmerFile.
   * @return LmerFile - A COPY of the LmerFile or null if the contentFile does not exist.
   * @throws FileNotFoundException
   * @throws XmlException
   */
  public LmerFile getLmerFileCopy(File contentFile) throws FileNotFoundException, XmlException {

    String techMdId = getFileTechMdId(contentFile);

    if (techMdId != null) {
      return LmerFile.Factory.parse(getTechMdXmlData(techMdId).toString());
    }

    return null;
  }

  /**
   * <p>
   * Gets a COPY of the LmerFile of the specified techMdId.
   * </p>
   * 
   * @param techMdId The techMdId for which we want the lmerFile.
   * @return LmerFile - A COPY of the LmerFile or null if the techMdId does not exist.
   * @throws XmlException
   */
  public LmerFile getLmerFileCopy(String techMdId) throws XmlException {
    return LmerFile.Factory.parse(getTechMdXmlData(techMdId).toString());
  }

  /**
   * <p>
   * Gets a copy of all the descriptive metadata objects from the dmdSec of the specified identifier
   * list.
   * </p>
   * 
   * @param dmdIdList
   * @return List<MdSecType> - A List containing all DMD sections.
   * @throws Exception
   */
  public List<MdSecType> getDmdSecCopy(List<String> dmdIdList) throws Exception {

    // Create a list and add all the descriptive metadata blocks concerning the given DMDID.
    ArrayList<MdSecType> result = new ArrayList<MdSecType>();

    // Iterate over all the DMDIDs and add the descriptive metadata sections to the result list.
    Iterator<String> iter = dmdIdList.iterator();
    while (iter.hasNext()) {
      String dmdId = iter.next();
      MdSecType dmdSec = getDmdSecCopy(dmdId);
      if (dmdSec != null) {
        result.add(dmdSec);
      }
    }

    return result;
  }

  /**
   * <p>
   * Gets a copy of the descriptive metadata object of the specified ID.
   * </p>
   * 
   * @param dmdId
   * @return MdSecType - A COPY of the specified DMD section.
   * @throws Exception
   */
  public MdSecType getDmdSecCopy(String dmdId) throws Exception {

    // Get the array of DMD sections.
    List<MdSecType> dmdList = this.uof.getDmdSecList();

    // Go through all the DMD sections and return the one that fits to the given ID.
    for (MdSecType dmd : dmdList) {
      if (dmd.getID().equals(dmdId)) {
        return (MdSecType) XmlObject.Factory.parse(dmd.getMdWrap().getXmlData().toString());
      }
    }

    return null;
  }

  /**
   * <p>
   * Gets a copy of all the LmerProcess objects of the specified identifier list.
   * </p>
   * 
   * @param admIdList
   * @return List<LmerProcess> - A list of all provenance metadata sections specified by the given
   *         ADMID list.
   * @throws Exception
   */
  public List<LmerProcess> getLmerProcessCopy(List<String> admIdList) throws Exception {

    // Create a list and add all the provenance metadata blocks concerning the given ADMID, starting
    // with the first (the last migration metadata).
    ArrayList<LmerProcess> result = new ArrayList<LmerProcess>();

    // Iterate over all the ADMIDs and add the provenance metadata sections to the result list. We
    // can iterate over ALL the IDs, because the getLmerProcessCopy() method only iterates over the
    // digiprovMd sections.
    Iterator<String> iter = admIdList.iterator();
    while (iter.hasNext()) {
      String admId = iter.next();
      LmerProcess lmerProcess = getLmerProcessCopy(admId);
      if (lmerProcess != null) {
        result.add(lmerProcess);
      }
    }

    return result;
  }

  /**
   * <p>
   * Gets a copy of the LmerProcess object of the specified digiprovMdId.
   * </p>
   * 
   * @param digiprovMdId
   * @return LmerProcess - A COPY of the provenance metadata section specified by the digiprovMdId.
   */
  public LmerProcess getLmerProcessCopy(String digiprovMdId) throws Exception {

    // Get the array of DIGIPROV sections.
    List<MdSecType> digiprovList = this.uof.getAmdSecArray(0).getDigiprovMDList();

    // Go throught all the digiprov sections and return the one that fits to the given ID.
    for (MdSecType digiprov : digiprovList) {
      if (digiprov.getID().equals(digiprovMdId)) {
        return LmerProcess.Factory.parse(digiprov.getMdWrap().getXmlData().toString());
      }
    }

    return null;
  }

  /**
   * <p>
   * Returns the Jhove technical metadata for a content file.
   * </p>
   * 
   * @param contentFile The file you want the jhove data for.
   * 
   * @return Jhove - The jhove object of the given content file.
   * @throws XmlException if the technical metadata for the content file can not be parsed.
   * @throws FileNotFoundException if the contentFile is not in the mets.
   */
  public Jhove getJhoveTechMd(File contentFile) throws XmlException, FileNotFoundException {
    return getJhoveTechMd(getFileTechMdId(contentFile));
  }

  /**
   * <p>
   * Returns the jhove technical metadata for a techMdId in the mets.
   * </p>
   * 
   * @param techMdId The ID of the technical Metadata.
   * @return Jhove - The jhove object of the given ID.
   * @throws XmlException if the techMdId does not exist
   */
  public Jhove getJhoveTechMd(String techMdId) throws XmlException {
    return ((JhoveDocument) XmlObject.Factory.parse(getLmerFileCopy(techMdId).getXmlDataArray(0).toString()))
        .getJhove();
  }

  /**
   * <p>
   * Returns a copy of the first dublin core descriptive metadata section in the mets.
   * </p>
   * 
   * @return DcSet - The dcSet object of the first dublin core descriptive metadata section in the
   *         mets or null if none exists.
   * @throws XmlException
   */
  public DcSet getDcCopy() throws XmlException {

    List<MdSecType> dmdList = this.uof.getDmdSecList();

    for (MdSecType dmd : dmdList) {
      if (dmd.getMdWrap().getMDTYPE().toString().equals(MDTYPE.DC.toString())) {
        XmlOptions opts = new XmlOptions();
        // Tricky! Because we get only an XML fragment of a dcSet we have to deceive the Factory
        // parser.
        opts.setLoadReplaceDocumentElement(
            new QName("http://purl.org/dc/elements/1.1/", "dcSet", "dc"));
        return DcSetDocument.Factory.parse(dmd.getMdWrap().getXmlData().toString(), opts)
            .getDcSet();
      }
    }

    return null;
  }

  /**
   * <p>
   * Set the lmerFile block under the amdSec.techMD.mdWrap.XmlData hierarchy with the given
   * techMdID.
   * </p>
   * 
   * @param lFile
   * @param techMdId
   * @throws Exception
   */
  public void setLmerFile(LmerFile lFile, String techMdId) throws Exception {
    getTechMdXmlData(techMdId).set(lFile);
    // Update checksums.
    setLmerObject(getLmerObjectCopy());
  }

  /**
   * <p>
   * Returns for the object the referenced admId for the technical metadata in the file section
   * (techMd).
   * </p>
   * 
   * @return String - The object's ID which references the techMd section if set, empty string
   *         otherwise.
   */
  public String getObjectTechMdId() {

    String result = "";

    // Get the list of ADMIDs from the file group.
    List<String> admIdList = this.uof.getFileSec().getFileGrpArray(0).getADMID();

    // Return the TECHMD identifier (the LAST identifier in the list of IDs!!), if existing.
    if (admIdList.size() > 0) {
      result = admIdList.get(admIdList.size() - 1);
    }

    return result;
  }

  /**
   * <p>
   * Returns for a specified content file the referenced admId (in UOF it's called techMdId) in the
   * file section. This works only if contentFile has the same absolute path as basePathUri +
   * fileSecEntry.
   * </p>
   * 
   * @param contentFile
   * @return String
   * @throws FileNotFoundException
   */
  public String getFileTechMdId(File contentFile) throws FileNotFoundException {

    List<FileType> fileList = this.uof.getFileSec().getFileGrpArray(0).getFileList();

    // Go throught all files.
    for (FileType file : fileList) {
      String absoluteFileSecPath = this.basePathUri
          + file.getFLocatArray(0).getHref().substring(FILE_LOCAT_HREF_PREFIX.length());

      // It the two pathes are equal, return the TECHMD identifier (the LAST identifier in the list
      // of IDs!!).
      if (contentFile.toURI().toString().equals(absoluteFileSecPath)) {
        return (String) file.getADMID().get(file.getADMID().size() - 1);
      }
    }

    throw new FileNotFoundException("File not found in UOF");
  }

  /**
   * <p>
   * Returns for a specified content file the file metadata from the file section. This works only
   * if contentFile has the same absolute path as basePathUri + fileSecEntry.
   * </p>
   * 
   * @param contentFile
   * @return FileType
   * @throws Exception
   */
  public FileType getFileMdMets(File contentFile) throws Exception {

    List<FileType> fileList = this.uof.getFileSec().getFileGrpArray(0).getFileList();

    for (FileType file : fileList) {
      String absoluteFileSecPath = this.basePathUri
          + file.getFLocatArray(0).getHref().substring(FILE_LOCAT_HREF_PREFIX.length());
      if (contentFile.toURI().toString().equals(absoluteFileSecPath)) {
        return file;
      }
    }

    throw new FileNotFoundException("File not found in UOF");
  }

  /**
   * <p>
   * Returns for a specified content file the referenced digiprovId from the file section. This
   * works only if contentFile has the same absolute path as basePathUri + fileSecEntry.
   * </p>
   * 
   * @param contentFile
   * @return String
   * @throws FileNotFoundException
   */
  public List<String> getFileProvMdIdList(File contentFile) throws FileNotFoundException {

    List<FileType> fileList = this.uof.getFileSec().getFileGrpArray(0).getFileList();

    // Go through all files.
    for (FileType file : fileList) {
      String absoluteFileSecPath = this.basePathUri
          + file.getFLocatArray(0).getHref().substring(FILE_LOCAT_HREF_PREFIX.length());

      // It the two pathes are equal, return the list of the DIGIPROVMD identifier (all except the
      // last).
      if (contentFile.toURI().toString().equals(absoluteFileSecPath)) {
        return file.getADMID().subList(0, file.getADMID().size() - 1);
      }
    }

    throw new FileNotFoundException("File not found in UOF");
  }

  /**
   * <p>
   * Returns true if the file has migration data (a reference to an lmerProcess section from the
   * file section), false otherwise.
   * </p>
   * 
   * @param contentFile
   * @return boolean - true or false.
   * @throws FileNotFoundException
   */
  public boolean hasMigrationFileData(File contentFile) throws FileNotFoundException {

    // Return true if the digiprovMdIdList is not empty for this file.
    if (!getFileProvMdIdList(contentFile).isEmpty()) {
      return true;
    }

    return false;
  }

  /**
   * <p>
   * Returns for a specified content file the file id in the file section. This works only if
   * contentFile has the same absolute path as basePathUri + fileSecEntry.
   * </p>
   * 
   * @param contentFile
   * @return String
   * @throws FileNotFoundException
   */
  public String getFileId(File contentFile) throws FileNotFoundException {

    List<FileType> fileList = this.uof.getFileSec().getFileGrpArray(0).getFileList();

    for (FileType file : fileList) {
      String absoluteFileSecPath = this.basePathUri
          + file.getFLocatArray(0).getHref().substring(FILE_LOCAT_HREF_PREFIX.length());

      if (contentFile.toURI().toString().equals(absoluteFileSecPath)) {
        return file.getID();
      }
    }

    throw new FileNotFoundException("File not found in UOF");
  }

  // **
  // INTERNAL (Internal - implementation details, local classes, ...)
  // **

  /**
   * <p>
   * Just checks if the size of the technical metadata string exceeds the given permitted size.
   * </p>
   * 
   * @param techMd
   * @throws Exception
   */
  protected void checkTechMdSize(TechnicalMetadata techMd) throws Exception {
    if (this.checkTechMdSize && techMd.getSize() > this.maxTechMDLength) {
      throw new Exception(
          "Technical metadata size exceeds DIAS limit of " + this.maxTechMDLength + " bytes");
    }
  }

  /**
   * <p>
   * Corrects the length of the dublin core metadata fields if any of them is too long.
   * </p>
   * 
   * @param dc The dcSet to correct
   * @return DcSet - The corrected dcSet
   */
  private DcSet getCorrectedFieldLength(DcSet dc) {

    dc.setContributorArray(getCorrectedArray(dc.getContributorArray(), diasDcTypeLimit));
    dc.setTitleArray(getCorrectedArray(dc.getTitleArray(), diasDcIdentifierLimit));
    dc.setCreatorArray(getCorrectedArray(dc.getCreatorArray(), diasDcTypeLimit));
    dc.setSubjectArray(getCorrectedArray(dc.getSubjectArray(), diasDcIdentifierLimit));
    dc.setDescriptionArray(getCorrectedArray(dc.getDescriptionArray(), diasDcDescriptionLimit));
    dc.setPublisherArray(getCorrectedArray(dc.getPublisherArray(), diasDcTypeLimit));
    dc.setDateArray(getCorrectedArray(dc.getDateArray(), diasDcTypeLimit));
    dc.setTypeArray(getCorrectedArray(dc.getTypeArray(), diasDcTypeLimit));
    dc.setFormatArray(getCorrectedArray(dc.getFormatArray(), diasDcTypeLimit));
    dc.setIdentifierArray(getCorrectedArray(dc.getIdentifierArray(), diasDcIdentifierLimit));
    dc.setSourceArray(getCorrectedArray(dc.getSourceArray(), diasDcIdentifierLimit));
    dc.setLanguageArray(getCorrectedArray(dc.getLanguageArray(), diasDcTypeLimit));
    dc.setRelationArray(getCorrectedArray(dc.getRelationArray(), diasDcIdentifierLimit));
    dc.setCoverageArray(getCorrectedArray(dc.getCoverageArray(), diasDcTypeLimit));
    dc.setRightsArray(getCorrectedArray(dc.getRightsArray(), diasDcIdentifierLimit));

    return dc;
  }

  /**
   * <p>
   * Divides the given string array. The result strings will not use more memory than
   * maxLengthInBytes bytes. We now divide in strings of a certain BYTE amount instead of a certain
   * CHAR amount.
   * </p>
   * 
   * @param toCorrect
   * @param maxLengthInBytes
   * @return String[] - The corrected Array.
   */
  private String[] getCorrectedArray(String toCorrect[], int maxLengthInBytes) {

    ArrayList<String> correctedStrings = new ArrayList<String>();
    int amountOfCharacters = maxLengthInBytes;

    for (int i = 0; i < toCorrect.length; i++) {
      try {
        while (toCorrect[i].getBytes("UTF-8").length > maxLengthInBytes) {
          // Look for the correct position to divide the string. We must find the amount of
          // characters, which fits into maxLengthInBytes UTF-8 bytes.
          while (toCorrect[i].substring(0, amountOfCharacters)
              .getBytes("UTF-8").length > maxLengthInBytes) {
            amountOfCharacters--;
          }
          // Divide the string at position of the computed character value.
          correctedStrings.add(toCorrect[i].substring(0, amountOfCharacters));
          toCorrect[i] = toCorrect[i].substring(amountOfCharacters);
        }
      } catch (UnsupportedEncodingException e) {
        // Doesn't occur here, because we always chose the correct encoding :-)
        e.printStackTrace();
      }
      correctedStrings.add(toCorrect[i]);
    }

    // Create a new string array and fill it.
    String result[] = new String[correctedStrings.size()];

    Iterator<String> strIter = correctedStrings.iterator();
    for (int i = 0; i < result.length; i++) {
      result[i] = strIter.next();
    }

    return result;
  }

  /**
   * <p>
   * Adds a DIGIPROV metadata section to the UOF.
   * </p>
   * 
   * @param digiprovInfo - The lmerProcess data to add.
   * @param digiprovMdId - The DIGIPTOV metadata ID of the lkmerProcessData to set.
   */
  private void addDigiprovSec(LmerProcess digiprovInfo, String digiprovMdId) {

    // Add new DIGIPROVMD section to the AMDSEC.
    List<AmdSecType> amdSecs = this.uof.getAmdSecList();
    MdSecType digiprovMd = amdSecs.get(0).addNewDigiprovMD();
    digiprovMd.setID(digiprovMdId);

    // Create a new MDWRAP section.
    MdWrap digiprovMdWrap = digiprovMd.addNewMdWrap();
    digiprovMdWrap.setID(digiprovMdId + prefixSeparator + mdWrapPostfix);
    digiprovMdWrap.setMIMETYPE("text/xml");
    digiprovMdWrap.setMDTYPE(MdWrap.MDTYPE.OTHER);
    digiprovMdWrap.setOTHERMDTYPE("lmerProcess");
    // digiprovMdWrap.setLABEL("lmerProcess");

    // Add the provenance metadata into the XMLDATA.
    digiprovMdWrap.addNewXmlData();
    digiprovMdWrap.getXmlData().set(digiprovInfo);
  }

  /**
   * Updates the Metadata with correct creation dates.
   * 
   * @throws XmlException
   * @throws DataFormatException
   */
  protected void updateUof() throws XmlException, DataFormatException {
    this.uof.getMetsHdr().setCREATEDATE(Calendar.getInstance());
    LmerObject lObj = getLmerObjectCopy();
    lObj.setMasterCreationDate(Calendar.getInstance());
    lObj.setMetadataCreationDate(Calendar.getInstance());
    setLmerObject(lObj);
  }

  /**
   * <p>
   * Returns an lmerObject with the updated XOR file checksums. This calculation of the object
   * checksum uses the already calculated checksum in the lmerFile sections.
   * </p>
   * 
   * @param lObj The lmerObject for which the checksum has to be calculated.
   * @return LmerObject - The updated lmerObject.
   * @throws DataFormatException If something goes wrong while calculatin the checksums.
   */
  private LmerObject recalculateObjectChecksum(LmerObject lObj) throws DataFormatException {

    TransferChecksum xorFileChecksum = null;

    if (lObj.sizeOfTransferChecksumArray() == 0) {
      xorFileChecksum = lObj.addNewTransferChecksum();
    } else {
      xorFileChecksum = TransferChecksum.Factory.newInstance();
    }

    xorFileChecksum.setCHECKSUMTYPE(ChecksumGenerator.XOR_OF_FILE_CHECKSUMS);

    List<FileType> fileEntries = this.uof.getFileSec().getFileGrpArray(0).getFileList();

    // The first transfer checksum is the checksum of the first existing file.
    if (fileEntries.size() > 0) {
      xorFileChecksum.setStringValue(fileEntries.get(0).getCHECKSUM());
    }

    // Later transfer checksum are xored with the checksums of files.
    for (int i = 1; i < fileEntries.size(); i++) {
      xorFileChecksum.setStringValue(ChecksumGenerator
          .xorHexStrings(xorFileChecksum.getStringValue(), fileEntries.get(i).getCHECKSUM()));
    }

    lObj.setTransferChecksumArray(0, xorFileChecksum);
    return lObj;
  }

  /**
   * <p>
   * Creates and references a new DMD sec and DMD wrap.
   * </p>
   * 
   * @return MdWrap - The new DMD wrap of the new DMD sec.
   */
  private MdWrap getNewDmdWrap() {

    String dmdId = dmdPrefix + prefixSeparator + this.dmdCounter;
    MdSecType dmdSec = this.uof.addNewDmdSec();
    dmdSec.setID(dmdId);

    // Adds the id of the DMDSEC in the DIV section.
    List<String> dmdList = new ArrayList<String>();
    dmdList.add(dmdId);
    DivType assetDiv = getFirstAssetDiv();
    if (assetDiv.getDMDID() != null) {
      // TODO: Make schemas.jar Java 1.5 compliant
      dmdList.addAll(assetDiv.getDMDID());
    }
    assetDiv.setDMDID(dmdList);

    this.dmdCounter++;
    return dmdSec.addNewMdWrap();
  }

  /**
   * <p>
   * Returns the XmlData-block in the amdSec.techMD.mdWrap.XmlData hierarchy for the given techMD
   * ID.
   * </p>
   * 
   * @param id
   * @return XmlData
   * @throws XmlException
   */
  private MdSecType.MdWrap.XmlData getTechMdXmlData(String id) throws XmlException {

    List<AmdSecType> amdSecs = this.uof.getAmdSecList();

    for (AmdSecType amd : amdSecs) {
      List<MdSecType> mdSecs = amd.getTechMDList();

      for (MdSecType md : mdSecs) {
        if (md.getID().equals(id)) {
          md.getMdWrap().getXmlData();
          return md.getMdWrap().getXmlData();
        }
      }
    }

    throw new XmlException("mdWrap xmlData block with ID " + id + " not found");
  }

  /**
   * <p>
   * Returns the first DIV section of type asset or null if none exist.
   * </p>
   * 
   * @return DivType
   */
  protected DivType getFirstAssetDiv() {

    List<StructMapType> sms = this.uof.getStructMapList();

    for (StructMapType sm : sms) {
      if (sm.getDiv().getTYPE().equalsIgnoreCase("ASSET")) {
        return sm.getDiv();
      }
    }

    return null;
  }

  /**
   * <p>
   * Update the lmer File data. Used from both addFile() and updateFileData().
   * </p>
   * 
   * @param lmerFile
   * @param techMd
   * @return
   */
  protected LmerFile updateLmerFileData(LmerFile lmerFile, TechnicalMetadata techMd) {

    // Get data from technical metadata.
    String tmdformat = techMd.getFormat();
    String tmdmimetype = techMd.getMimetype();
    String tmdversion = techMd.getVersion();
    // We only need format as mandatory data.
    if (tmdformat == null) {
      tmdformat = "not specified";
    }

    // Add DIAS file URI to the LMER.
    fRegistry.getFormat(tmdformat, tmdversion, lmerFile);
    // Optional LMER file data:
    // lmerFile.setCategory(Category.BINARY);
    // lmerFile.setFormatInfos("testFormatInfo");
    // lmerFile.setCreatorApplication("");
    // lmerFile.setViewerApplication("");
    // lmerFile.setLinkedTo("testLinkedTo");
    // lmerFile.setComments("Comments2");
    //
    // These are already in mets so we don't use them:
    // lmerFile.setFileIdentifier("");
    // lmerFile.setPath("");
    // lmerFile.setName("");
    // lmerFile.setSize(new BigInteger("0"));
    // lmerFile.setFileDateTime(Calendar.getInstance());
    // FileChecksum fileCheck = FileChecksum.Factory.newInstance();
    // fileCheck.setCHECKSUMTYPE("SHA-1");
    // fileCheck.setStringValue("qwertz");
    // lmerFile.setFileChecksum(fileCheck);
    // lmerFile.setMimeType("text/xml");

    // Add technical metadata to LMER file.
    lmerFile.addNewXmlData();
    lmerFile.getXmlDataArray(0).setMDTYPE(techMd.getType());
    XmlCursor xmlDataCursor = lmerFile.getXmlDataArray(0).newCursor();
    xmlDataCursor.toEndToken();
    techMd.getXmlCursor().copyXml(xmlDataCursor);

    // Add format and version from technical metadata to appropriate hash maps (use DIAS format URI
    // as key).
    // TODO The UOF only has a map of existing formats PER FORMAT, not per file. If you want to have
    // maps on file basis, please use the fileId as key!
    String key = lmerFile.getFormatList().get(0).getStringValue();

    // Only add format, if DIAS format URI is of a known file format.
    if (key.equals(FormatRegistry.UNKNOWN_FILETYPE)) {
      this.formatMap.put(key, "unknown");
      this.mimetypeMap.put(key, "unknown");
      this.versionMap.put(key, "unknown");
    } else {
      this.formatMap.put(key, tmdformat);
      this.mimetypeMap.put(key, tmdmimetype);
      this.versionMap.put(key, tmdversion);
    }

    return lmerFile;
  }

  /**
   * <p>
   * Updates the file data of one file in the file group. Used from both addFile() and
   * updateFileData().
   * </p>
   * 
   * @param contentFile
   * @param file
   * @param techMd
   * @return
   * @throws Exception
   */
  private gov.loc.mets.FileType updateFileSecFileData(File contentFile, gov.loc.mets.FileType file,
      TechnicalMetadata techMd) throws Exception {

    Calendar createDate = Calendar.getInstance();
    createDate.setTimeInMillis(contentFile.lastModified());

    file.setCREATED(createDate);
    file.setSIZE(contentFile.length());
    file.setCHECKSUM(ChecksumGenerator.getSHA1(new FileInputStream(contentFile)));
    file.setCHECKSUMTYPE(FileType.CHECKSUMTYPE.SHA_1);
    file.setMIMETYPE(techMd.getMimetype());
    file.getFLocatArray(0).setHref(FILE_LOCAT_HREF_PREFIX
        + contentFile.toURI().toString().substring(this.basePathUri.toString().length()));

    return file;
  }

  // **
  // GET & SET METHODS
  // **

  /**
   * <p>
   * Sets the base path which is omitted in the FILE LOCAT entry.
   * </p>
   * 
   * @param basePath
   */
  public void setBasePath(File basePath) {
    this.basePathUri = basePath.toURI();
  }

  /**
   * @return The base path URI
   */
  public URI getBasePath() {
    return this.basePathUri;
  }

  /**
   * @param dmdPrefix The dmdPrefix to set.
   */
  public static void setDmdPrefix(String dmdPrefix) {
    Uof.dmdPrefix = dmdPrefix;
  }

  /**
   * @param mdWrapPostfix The mdWrapPostfix to set.
   */
  public static void setMdWrapPostfix(String mdWrapPostfix) {
    Uof.mdWrapPostfix = mdWrapPostfix;
  }

  /**
   * @param techMdPrefix The techMdPrefix to set.
   */
  public static void setTechMdPrefix(String techMdPrefix) {
    Uof.techMdPrefix = techMdPrefix;
  }

  /**
   * @return The noWhiteSpacesInTechMd
   */
  public boolean isNoWhiteSpacesInTechMd() {
    return this.noWhiteSpacesInTechMd;
  }

  /**
   * @param noWhiteSpacesInTechMd The noWhiteSpInTechMd to set.
   */
  public void setNoWhiteSpacesInTechMd(boolean noWhiteSpacesInTechMd) {
    this.noWhiteSpacesInTechMd = noWhiteSpacesInTechMd;
  }

  /**
   * @param checkTechMdSize
   */
  public void setCheckTechMdSize(boolean checkTechMdSize) {
    this.checkTechMdSize = checkTechMdSize;
  }

  /**
   * @return The maxTechMDLength
   */
  public long getMaxTechMDLength() {
    return this.maxTechMDLength;
  }

  /**
   * @param maxTechMDLength The noWhiteSpInTechMd to set.
   */
  public void setMaxTechMDLength(long maxTechMDLength) {
    this.maxTechMDLength = maxTechMDLength;
  }

  /**
   * @param diasDcDescriptionLimit
   */
  public static void setDiasDcDescriptionLimit(int diasDcDescriptionLimit) {
    Uof.diasDcDescriptionLimit = diasDcDescriptionLimit;
  }

  /**
   * @param diasDcIdentifierLimit
   */
  public static void setDiasDcIdentifierLimit(int diasDcIdentifierLimit) {
    Uof.diasDcIdentifierLimit = diasDcIdentifierLimit;
  }

  /**
   * @param diasDcTypeLimit
   */
  public static void setDiasDcTypeLimit(int diasDcTypeLimit) {
    Uof.diasDcTypeLimit = diasDcTypeLimit;
  }

  /**
   * @param theDiasFormatId
   * @return
   */
  public String getTechMDFormat(String theDiasFormatId) {
    return this.formatMap.get(theDiasFormatId);
  }

  /**
   * @param theDiasFormatId
   * @return
   */
  public String getTechMDMimetype(String theDiasFormatId) {
    return this.mimetypeMap.get(theDiasFormatId);
  }

  /**
   * @param theDiasFormatId
   * @return
   */
  public String getTechMDVersion(String theDiasFormatId) {
    return this.versionMap.get(theDiasFormatId);
  }

  /**
   * @return
   */
  public static String getLmerProcessObjectID() {
    return lmerProcessObjectID;
  }

  /**
   * @param lmerProcessObjectID
   */
  public static void setLmerProcessObjectID(String lmerProcessObjectID) {
    Uof.lmerProcessObjectID = lmerProcessObjectID;
  }

}
