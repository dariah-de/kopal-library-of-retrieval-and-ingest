/**
 * Copyright 2005-2020 by project kopal
 *
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 */

/**
 * TODOLOG
 **
 * CHANGELOG
 * 
 * 2020-02-18 - Funk - Done some formatting.
 * 
 * 2012-03-09 - Funk Removed some "_"s in method parameters.
 * 
 * 2007-07-10 - koLibRI version 1.0
 */

package de.langzeitarchivierung.kolibri;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import de.langzeitarchivierung.kolibri.formats.MetadataFormat;
import de.langzeitarchivierung.kolibri.util.Configurator;

/**
 * <p>
 * Contains all data for running a process.
 * </p>
 * 
 * @author Ludwig
 * @version 2020-02-18
 * @since 2006-02-02
 */

public class ProcessData implements Runnable {

  // **
  // STATIC FINALS
  // **

  public static final String DATA_ALREADY_CLEARED = "DATA_ALREADY_CLEARED";
  public static final String OLD_OBJECT_VERSION = "OLD_OBJECT_VERSION";

  // **
  // CLASS VARIABLES (Static variables)
  // **

  protected static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");

  // **
  // STATE (Instance variables)
  // **

  // The human readable identifier for the new process.
  protected String processName;
  // The policy.
  protected Policy policy;
  // The root dir of the sip content files.
  private String pathToContentFiles;
  // List of all the files.
  private List<File> fileList = null;
  // Creation date of this object in milliseconds from System.currentTimeMillis().
  private long startDate;
  // Location of the SIP.
  private File sipLocation;
  // Container for the metadata, file locations, etc.
  private MetadataFormat metadata = null;
  // Action modules can put data here for exchange if there is no other place.
  protected HashMap<Object, Object> customData = null;
  private HashMap<Object, Object> customIds = null;
  // Reference to the database used.
  // private HibernateUtil database;

  // **
  // CREATION (Constructors, factory methods, static/inst init)
  // **

  // /**
  // * <p>
  // * Constructor. Uses the default metadata format.
  // * </p>
  // *
  // * @param database The database to write to.
  // * @param pName Human understandable identifier for the new process
  // * @param policyName The name of the policy to execute if the run method is called.
  // * @throws Exception
  // */
  // public ProcessData(HibernateUtil database, String policyName, String pName)
  // throws Exception {
  // super();
  // this.startDate = System.currentTimeMillis();
  // this.fileList = new ArrayList<File>();
  // this.processName = pName;
  // this.policy = new Policy(policyName);
  // Configurator.configure(this);
  // Configurator.configure(this.policy, this);
  // this.policy.buildTree();
  // this.database = database;
  // }

  /**
   * <p>
   * Constructor. Uses the default metadata format.
   * </p>
   * 
   * @param pName Human understandable identifier for the new process
   * @param policyName The name of the policy to execute if the run method is called.
   * @throws Exception
   */
  public ProcessData(String policyName, String pName) throws Exception {
    super();
    this.startDate = System.currentTimeMillis();
    this.fileList = new ArrayList<File>();
    this.processName = pName;
    this.policy = new Policy(policyName);
    Configurator.configure(this);
    Configurator.configure(this.policy, this);
    this.policy.buildTree();
  }

  /**
   * <p>
   * Constructor. Uses the default metadata format.
   * </p>
   * 
   * @param pName Human understandable identifier for the new process
   * @param pol The policy to execute if the run method is called.
   * @throws Exception
   */
  public ProcessData(Policy pol, String pName) throws Exception {
    super();
    this.startDate = System.currentTimeMillis();
    this.fileList = new ArrayList<File>();
    this.processName = pName;
    this.policy = pol;
    Configurator.configure(this);
    Configurator.configure(this.policy, this);
    this.policy.buildTree();
  }

  /**
   * <p>
   * Constructor.
   * </p>
   * 
   * @param pName Human understandable identifier for the new process
   * @param policyName The name of the policy to execute if the run method is called.
   * @param md A custom MetadataFormat object.
   * @throws Exception
   */
  public ProcessData(MetadataFormat md, String policyName, String pName) throws Exception {
    super();
    this.startDate = System.currentTimeMillis();
    this.fileList = new ArrayList<File>();
    this.processName = pName;
    this.metadata = md;
    this.policy = new Policy(policyName);
    Configurator.configure(this);
    Configurator.configure(this.policy, this);
    this.policy.buildTree();
  }

  // **
  // MANIPULATION (Manipulation - what the object does)
  // **

  /**
   * <p>
   * Runs the defined policy for this ProcessData object.
   * </p>
   * 
   * @see java.lang.Runnable#run()
   */
  public void run() {
    // As long as we have a reachable todo...
    while (this.policy.hasStatusWithParent(Status.TODO, Status.DONE)) {
      // ...we check each todo node of the policy once.
      this.policy.resetNextStatusStep(Status.TODO);
      Step nextStep = this.policy.nextStatusStepWithPath(Status.TODO, Status.DONE);
      // If there is a node with status "todo" and a parent with status "done", run the
      // actionmodule.
      while (nextStep != null) {
        nextStep.run();
        nextStep = this.policy.nextStatusStepWithPath(Status.TODO, Status.DONE);
      }
    }

    // In case of an error or a canceled module log this information and ask for user interaction!
    if (this.policy.hasStatus(Status.ERROR)) {
      defaultLogger.log(Level.SEVERE, "ERROR: Could not finish processing element '"
          + this.getProcessName() + "'. User interaction is needed!");
    } else if (this.policy.hasStatus(Status.CANCELED)) {
      defaultLogger.log(Level.SEVERE, "CANCELED: Could not finish processing element '"
          + this.getProcessName() + "'. User interaction is needed!");
    }

    // Destroy data structure if everything is done.
    if (!this.policy.hasOtherStatusThan(Status.DONE)) {
      this.clearProcessData();
    }
  }

  // **
  // INTERNAL (Internal - implementation details, local classes, ...)
  // **

  /**
   * <p>
   * Clears the process data tree.
   * </p>
   */
  protected void clearProcessData() {
    this.policy.destructTree();
    this.setMetadata(null);
    this.getCustomData().put(ProcessData.DATA_ALREADY_CLEARED, true);
  }

  /**
   * <p>
   * Adds a file or a directory (recursively) to the list of files.
   * </p>
   * 
   * @param toAdd The file or directory to add.
   */
  private void addDirToFileList(File toAdd) {
    if (toAdd.isDirectory()) {
      String dirEntries[] = toAdd.list();
      for (int i = 0; i < dirEntries.length; i++) {
        addDirToFileList(new File(toAdd, dirEntries[i]));
      }
    } else if (toAdd.isFile()) {
      this.fileList.add(toAdd);
    }
  }

  // **
  // GET & SET METHODS
  // **

  /**
   * @return Returns the metadata object
   * @throws Exception
   */
  public MetadataFormat getMetadata() throws Exception {
    if (this.metadata == null) {
      this.metadata = MetadataFormat.Factory.newInstance();
    }
    return this.metadata;
  }

  /**
   * @return Returns the customData.
   */
  public HashMap<Object, Object> getCustomData() {
    if (this.customData == null) {
      this.customData = new HashMap<Object, Object>();
    }
    return this.customData;
  }

  /**
   * @return Returns the processName.
   */
  public String getProcessName() {
    return this.processName;
  }

  /**
   * @return Returns the policy.
   */
  public Policy getPolicy() {
    return this.policy;
  }

  /**
   * @param policy The policy to set.
   */
  public void setPolicy(Policy policy) {
    this.policy = policy;
  }

  /**
   * @return Returns the pathToContentFiles.
   */
  public String getPathToContentFiles() {
    return this.pathToContentFiles;
  }

  /**
   * Clears the file list, sets the path to all content files and adds all files in it.
   * 
   * @param pathToContentFiles The pathToContentFiles to set.
   */
  public void setPathToContentFiles(String pathToContentFiles) {
    this.pathToContentFiles = pathToContentFiles;
    this.fileList.clear();
    addDirToFileList(new File(this.pathToContentFiles));
  }

  /**
   * Sets the path to all content files without adding all files in it to the file list. This is
   * important if the first given path to content files refers to a directory with a collection of
   * different digital objects that have to be processed individually by action modules first. E.g.
   * special file copy parameters to drop certain files, etc.
   * 
   * @param pathToContentFiles The pathToContentFiles to set.
   */
  public void setPathToContentFilesWithoutFileListing(String pathToContentFiles) {
    this.pathToContentFiles = pathToContentFiles;
  }

  /**
   * @return Returns the sipLocation.
   */
  public File getSipLocation() {
    return this.sipLocation;
  }

  /**
   * @param sipLocation The sipLocation to set.
   */
  public void setSipLocation(File sipLocation) {
    this.sipLocation = sipLocation;
  }

  /**
   * @return Returns the fileList.
   */
  public List<File> getFileList() {
    return this.fileList;
  }

  /**
   * @param metadata The metadata to set.
   */
  public void setMetadata(MetadataFormat metadata) {
    this.metadata = metadata;
  }

  /**
   * @return Returns the customIds.
   */
  public HashMap<Object, Object> getCustomIds() {
    if (this.customIds == null) {
      this.customIds = new HashMap<Object, Object>();
    }
    return this.customIds;
  }

  /**
   * @return the startDate
   */
  public long getStartDate() {
    return this.startDate;
  }

  /**
   * @param startDate
   */
  public void setStartDate(long startDate) {
    this.startDate = startDate;
  }

  // /**
  // * @return
  // */
  // public HibernateUtil getDatabase() {
  // return this.database;
  // }

}
