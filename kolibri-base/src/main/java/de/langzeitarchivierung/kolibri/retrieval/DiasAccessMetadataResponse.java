/**********************************************************************
 * de.langzeitarchivierung.kolibri.retrieval / DiasAccessMetadataResponse.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.retrieval;

import java.io.Serializable;

/*******************************************************************************
 * A metadata subfield of the DIAS response to access requests as a bean. To
 * understand the data fields read the DIP Interface Specification.
 * 
 * @author Ludwig
 * @version 20070308
 * @since 20060118
 ******************************************************************************/

public class DiasAccessMetadataResponse implements Serializable
{

	// CONSTANTS **********************************************************

	private static final long	serialVersionUID	= 1L;

	// STATE (Instance variables) *****************************************

	// Between the METADATABLOCK tag.
	// Between the METADATA tag.
	private String				internalAssetId;
	private String				externalAssetId;
	private String				assetVersion;
	private String				assetDescription;
	private String				aipIsDeleted;
	private String				aipDeletionTimestamp;
	private String				assetCreationTimestamp;
	private String				assetIngestTimestamp;
	private String				oldObjectIdentifier	= DiasAccessResponse.NOT_IN_DIAS_RESPONSE;
	private String				oldVersion			= DiasAccessResponse.NOT_IN_DIAS_RESPONSE;
	private String				size				= DiasAccessResponse.NOT_IN_DIAS_RESPONSE;
	private String				dipRetrieveWaitTime	= DiasAccessResponse.NOT_IN_DIAS_RESPONSE;
	private String				timeUnit;

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * Default constructor.
	 **************************************************************************/
	public DiasAccessMetadataResponse() {
		super();
	}

	// GET & SET METHODS **************************************************

	/***************************************************************************
	 * Gets the aipDeletionTimestamp.
	 * 
	 * @return Returns the aipDeletionTimestamp.
	 **************************************************************************/
	public String getAipDeletionTimestamp() {
		return this.aipDeletionTimestamp;
	}

	/***************************************************************************
	 * Sets the aipDeletionTimestamp.
	 * 
	 * @param _aipDeletionTimestamp The aipDeletionTimestamp to set.
	 **************************************************************************/
	public void setAipDeletionTimestamp(String _aipDeletionTimestamp) {
		this.aipDeletionTimestamp = _aipDeletionTimestamp;
	}

	/***************************************************************************
	 * Gets the aipIsDeleted.
	 * 
	 * @return Returns the aipIsDeleted.
	 **************************************************************************/
	public String getAipIsDeleted() {
		return this.aipIsDeleted;
	}

	/***************************************************************************
	 * Sets the aipIsDeleted.
	 * 
	 * @param _aipIsDeleted The aipIsDeleted to set.
	 **************************************************************************/
	public void setAipIsDeleted(String _aipIsDeleted) {
		this.aipIsDeleted = _aipIsDeleted;
	}

	/***************************************************************************
	 * Gets the assetCreationTimestamp.
	 * 
	 * @return Returns the assetCreationTimestamp.
	 **************************************************************************/
	public String getAssetCreationTimestamp() {
		return this.assetCreationTimestamp;
	}

	/***************************************************************************
	 * Sets the assetCreationTimestamp.
	 * 
	 * @param _assetCreationTimestamp The assetCreationTimestamp to set.
	 **************************************************************************/
	public void setAssetCreationTimestamp(String _assetCreationTimestamp) {
		this.assetCreationTimestamp = _assetCreationTimestamp;
	}

	/***************************************************************************
	 * Gets the assetDescription.
	 * 
	 * @return Returns the assetDescription.
	 **************************************************************************/
	public String getAssetDescription() {
		return this.assetDescription;
	}

	/***************************************************************************
	 * Sets the assetDescription.
	 * 
	 * @param _assetDescription The assetDescription to set.
	 **************************************************************************/
	public void setAssetDescription(String _assetDescription) {
		this.assetDescription = _assetDescription;
	}

	/***************************************************************************
	 * Gets the assetIngestTimestamp.
	 * 
	 * @return Returns the assetIngestTimestamp.
	 **************************************************************************/
	public String getAssetIngestTimestamp() {
		return this.assetIngestTimestamp;
	}

	/***************************************************************************
	 * Sets the assetIngestTimestamp.
	 * 
	 * @param _assetIngestTimestamp The assetIngestTimestamp to set.
	 **************************************************************************/
	public void setAssetIngestTimestamp(String _assetIngestTimestamp) {
		this.assetIngestTimestamp = _assetIngestTimestamp;
	}

	/***************************************************************************
	 * Gets the assetVersion.
	 * 
	 * @return Returns the assetVersion.
	 **************************************************************************/
	public String getAssetVersion() {
		return this.assetVersion;
	}

	/***************************************************************************
	 * Sets the assetVersion.
	 * 
	 * @param _assetVersion The assetVersion to set.
	 **************************************************************************/
	public void setAssetVersion(String _assetVersion) {
		this.assetVersion = _assetVersion;
	}

	/***************************************************************************
	 * Gets the dipRetrieveWaitTime.
	 * 
	 * @return Returns the dipRetrieveWaitTime.
	 **************************************************************************/
	public String getDipRetrieveWaitTime() {
		return this.dipRetrieveWaitTime;
	}

	/***************************************************************************
	 * Sets the dipRetrieveWaitTime.
	 * 
	 * @param _dipRetrieveWaitTime The dipRetrieveWaitTime to set.
	 **************************************************************************/
	public void setDipRetrieveWaitTime(String _dipRetrieveWaitTime) {
		this.dipRetrieveWaitTime = _dipRetrieveWaitTime;
	}

	/***************************************************************************
	 * Gets the externalAssetId.
	 * 
	 * @return Returns the externalAssetId.
	 **************************************************************************/
	public String getExternalAssetId() {
		return this.externalAssetId;
	}

	/***************************************************************************
	 * Sets the externalAssetId.
	 * 
	 * @param _externalAssetId The externalAssetId to set.
	 **************************************************************************/
	public void setExternalAssetId(String _externalAssetId) {
		this.externalAssetId = _externalAssetId;
	}

	/***************************************************************************
	 * Gets the internalAssetId.
	 * 
	 * @return Returns the internalAssetId.
	 **************************************************************************/
	public String getInternalAssetId() {
		return this.internalAssetId;
	}

	/***************************************************************************
	 * Sets the internalAssetId.
	 * 
	 * @param _internalAssetId The internalAssetId to set.
	 **************************************************************************/

	public void setInternalAssetId(String _internalAssetId) {
		this.internalAssetId = _internalAssetId;
	}

	/***************************************************************************
	 * Gets the oldObjectIdentifier.
	 * 
	 * @return Returns the oldObjectIdentifier.
	 **************************************************************************/

	public String getOldObjectIdentifier() {
		return this.oldObjectIdentifier;
	}

	/***************************************************************************
	 * Sets the oldObjectIdentifier.
	 * 
	 * @param _oldObjectIdentifier The oldObjectIdentifier to set.
	 **************************************************************************/

	public void setOldObjectIdentifier(String _oldObjectIdentifier) {
		this.oldObjectIdentifier = _oldObjectIdentifier;
	}

	/***************************************************************************
	 * Gets the oldVersion.
	 * 
	 * @return Returns the oldVersion.
	 **************************************************************************/
	public String getOldVersion() {
		return this.oldVersion;
	}

	/***************************************************************************
	 * Sets the oldVersion.
	 * 
	 * @param _oldVersion The oldVersion to set.
	 **************************************************************************/
	public void setOldVersion(String _oldVersion) {
		this.oldVersion = _oldVersion;
	}

	/***************************************************************************
	 * Gets the size.
	 * 
	 * @return Returns the size.
	 **************************************************************************/
	public String getSize() {
		return this.size;
	}

	/***************************************************************************
	 * Sets the size.
	 * 
	 * @param _size The size to set.
	 **************************************************************************/
	public void setSize(String _size) {
		this.size = _size;
	}

	/***************************************************************************
	 * @return Returns the timeUnit.
	 * @author Stefan Funk, SUB Goettingen, 08.03.2007
	 **************************************************************************/
	public String getTimeUnit() {
		return this.timeUnit;
	}

	/***************************************************************************
	 * @param timeUnit The timeUnit to set.
	 * @author Stefan Funk, SUB Goettingen, 08.03.2007
	 **************************************************************************/
	public void setTimeUnit(String timeUnit) {
		this.timeUnit = timeUnit;
	}

	// QUERIES (Queries - no change to object's state) ********************

	/***************************************************************************
	 * @see java.lang.Object#toString()
	 **************************************************************************/
	@Override
	public String toString() {
		String result = "";

		result += "\tinternalAssetId: " + this.internalAssetId;
		result += "\n\texternalAssetId: " + this.externalAssetId;
		result += "\n\tassetVersion: " + this.assetVersion;
		result += "\n\tassetDescription: " + this.assetDescription;
		result += "\n\taipIsDeleted: " + this.aipIsDeleted;
		result += "\n\taipDeletionTimestamp: " + this.aipDeletionTimestamp;
		result += "\n\tassetCreationTimestamp: " + this.assetCreationTimestamp;
		result += "\n\tassetIngestTimestamp: " + this.assetIngestTimestamp;
		result += "\n\toldObjectIdentifier: " + this.oldObjectIdentifier;
		result += "\n\toldVersion: " + this.oldVersion;
		result += "\n\tsize: " + this.size;
		result += "\n\tdipRetrieveWaitTime: " + this.dipRetrieveWaitTime;
		result += "\n\ttimeUnit: " + this.timeUnit;

		return result;
	}

}
