/**********************************************************************
 * Copyright 2005-2024 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 ******************************************************************************
 * 
 * CHANGELOG
 *
 * 16.02.2024 - Funk - Adapted to new HTTP client.
 * 
 * 24.01.2008 - Funk - Corrected some output lines.
 * 
 * 10.07.2007 - koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.retrieval;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.digester3.Digester;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.Response;

/*******************************************************************************
 * Methods for accessing DIAS.
 * 
 * TODO Introduce convenience methods.
 * 
 * TODO After testing dias remove the request methods that do not have dipformat as parameter.
 * 
 * @author Ludwig
 * @version 2016-06-08
 * @since 2006-01-18
 ******************************************************************************/

public class DiasAccess {

  // CONSTANTS **********************************************************

  private static final int INPUT_BUFFER_LENGTH = 1024 * 1024;

  /** Constant for DIP format zip */
  public static final String DIP_FORMAT_ZIP = "zip";
  /** Constant for DIP format tar */
  public static final String DIP_FORMAT_TAR = "tar";
  /** Constant for DIP format tar.gz */
  public static final String DIP_FORMAT_TAR_GZ = "tar.gz";
  private static final String[] VALID_DIP_FORMATS = {
      DIP_FORMAT_ZIP, DIP_FORMAT_TAR, DIP_FORMAT_TAR_GZ};

  /** Constant for RESPONSE format xml */
  public static final String RESPONSE_FORMAT_XML = "xml";
  /** Constant for RESPONSE format html */
  public static final String RESPONSE_FORMAT_HTML = "html";
  private static final String[] VALID_RESPONSE_FORMATS = {
      RESPONSE_FORMAT_XML, RESPONSE_FORMAT_HTML};

  /** Constant for REQUEST type metadata */
  public static final String REQUEST_TYPE_METADATA = "metadata";
  /** Constant for REQUEST type fullmetadata */
  public static final String REQUEST_TYPE_FULLMETADATA = "fullmetadata";
  /** Constant for REQUEST type asset */
  public static final String REQUEST_TYPE_ASSET = "asset";
  private static final String[] VALID_REQUEST_TYPES = {
      REQUEST_TYPE_METADATA, REQUEST_TYPE_ASSET,
      REQUEST_TYPE_FULLMETADATA};

  private static final String BASE_URL = "/dias/Retriever?";
  private static final String EXT_ID_URL = "ExternalAssetID=";
  private static final String INT_ID_URL = "InternalAssetID=";
  private static final String DIP_FORMAT_URL = "&DipFmt=";
  private static final String LIST_FORMAT_URL = "&List=";
  private static final String RESPONSE_FORMAT_URL = "&RespFmt=";
  private static final String REQUEST_TYPE_URL = "&Request=";

  // Request types.
  private static final String REQUEST_METADATA = "Metadata";
  private static final String REQUEST_FULL_METADATA = "FullMetadata";
  private static final String REQUEST_ASSET = "Asset";

  // STATE (Instance variables) *****************************************

  private String accessServer = null;
  private int accessPort = 80;
  private String keyStoreFile;
  private boolean useHttps = true;
  private Client diasClient;

  // CLASS VARIABLES (Static variables) **********************************

  private static Logger defaultLogger = Logger.getLogger("de.langzeitarchivierung.kolibri");

  // CREATION (Constructors, factory methods, static/inst init) **********

  /***************************************************************************
   * Constructor.
   **************************************************************************/
  public DiasAccess() {
    this.diasClient = ClientBuilder.newClient();
  }

  /***************************************************************************
   * Constructor.
   * 
   * @param server The server address
   **************************************************************************/
  public DiasAccess(String server) {
    this.accessServer = server;
    this.diasClient = ClientBuilder.newClient();
  }

  /***************************************************************************
   * Constructor.
   * 
   * @param server The server address
   * @param p The port number
   **************************************************************************/
  public DiasAccess(String server, int p) {
    this.accessServer = server;
    this.accessPort = p;
    this.diasClient = ClientBuilder.newClient();
  }

  // MANIPULATION (Manipulation - what the object does) ******************

  /***************************************************************************
   * Configures the command line options. Using Jakarata Commons CLI.
   * 
   * TODO The static methods are accessed as described in the Jakarata Commons Cli documentation.
   * Use them in a clean way later.
   * 
   * @return the command line options object.
   **************************************************************************/
  public static Options cliOptions() {
    Options cliOptions = new Options();

    // Identifier options.
    cliOptions.addOption("x", "ext-id", true, "The requested external id");
    cliOptions.addOption("i", "int-id", true, "The requested internal id");

    // Request type options.
    cliOptions.addOption("d", "dip-format", true, "The requested dip "
        + "format [zip|tar|tar.gz]");
    cliOptions.addOption("r", "response-format", true, "The requested "
        + "response format [xml|html]");
    cliOptions.addOption("t", "request-type", true, "The type of request "
        + "[metadata|fullmetadata|asset]");
    cliOptions.addOption("l", "list", false, "Returns a list of all "
        + "metadata sets for an external id");

    // Connection options.
    cliOptions.addOption("a", "address", true, "The server address");
    cliOptions.addOption("n", "port", true, "The server port");
    cliOptions.addOption("u", "unsecure", false, "Use unsecure access "
        + "to dias");

    // General modifier options.
    cliOptions.addOption("hp", "show-properties", false, "Print the "
        + "system properties and continue");

    // Commands modifier.
    cliOptions.addOption("p", "print", false, "Prints the dias response");
    Option downloadOption = new Option("g", "download", true, "If no "
        + "file is specified the file specified in the dias response "
        + "is downloaded and saved in the current directory. Else the "
        + "specified file is downloaded.");
    downloadOption.setOptionalArg(true);
    cliOptions.addOption(downloadOption);

    cliOptions.addOption("f", "file", true, "Parse a file as a Dias"
        + " response and prints it");
    cliOptions.addOption("h", "help", false, "Print this dialog and exit");
    return cliOptions;
  }

  /***************************************************************************
   * Main method for testing purposes.
   * 
   * @param args the command line.
   **************************************************************************/
  public static void main(String[] args) {
    // Parse the command line options.
    CommandLineParser cliParser = new DefaultParser();
    Options cliOptions = cliOptions();
    CommandLine cLine = null;

    try {
      cLine = cliParser.parse(cliOptions, args);
    } catch (ParseException e) {
      e.printStackTrace();
      System.err.println("Commandline and its arguments not correct!");
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("DiasAccess", cliOptions);
      System.exit(1);
    }

    // Print system properties and continue.
    if (cLine.hasOption("hp")) {
      Properties p = System.getProperties();
      for (Enumeration e = p.propertyNames(); e.hasMoreElements();) {
        Object po = e.nextElement();
        System.out.println("Name:" + po + " Value:"
            + p.getProperty((String) po));
      }
    }

    // Print help dialog.
    if (cLine.hasOption("h")) {
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("DiasAccess", cliOptions);
      System.exit(0);
    }

    // For testing purposes: parse a file as response.
    if (cLine.hasOption("f")) {
      File responseFile = new File(cLine.getOptionValue("f"));
      Reader source = null;

      try {
        source = new BufferedReader(new FileReader(responseFile));
      } catch (FileNotFoundException fe) {
        System.err.println("File " + cLine.getOptionValue("f")
            + "not found");
        System.exit(1);
      }
      DiasAccessResponse diasResponse = parseDiasResponse(source);
      System.out.println(diasResponse);
      System.exit(0);
    }

    // Checks for correct command line.
    // TODO Check if the CLI package has options for this tests.
    if ((cLine.hasOption("x") && cLine.hasOption("i"))
        || (!cLine.hasOption("x") && !cLine.hasOption("i"))) {
      System.err.println("You must use either an internal or an "
          + "external id!");
      System.exit(1);
    }

    String id = null;
    if (cLine.hasOption("x")) {
      id = cLine.getOptionValue("x");
    } else {
      id = cLine.getOptionValue("i");
    }

    String dipFormat = "zip";
    if (cLine.hasOption("d")) {
      if (!isValidDipFormat(cLine.getOptionValue("d"))) {
        System.err.println(cLine.getOptionValue("d") + " is not a "
            + "valid dip format!");
        System.exit(1);
      }
      dipFormat = cLine.getOptionValue("d");
    }

    String responseFormat = "xml";
    if (cLine.hasOption("r")) {
      if (!isValidResponseFormat(cLine.getOptionValue("r"))) {
        System.err.println(cLine.getOptionValue("r") + " is not a "
            + "valid response format!");
        System.exit(1);
      }
      responseFormat = cLine.getOptionValue("r");
    }

    String requestType = "metadata";
    if (cLine.hasOption("t")) {
      if (!isValidRequestType(cLine.getOptionValue("t"))) {
        System.err.println(cLine.getOptionValue("t") + " is not a "
            + "valid request type!");
        System.exit(1);
      }
      requestType = cLine.getOptionValue("t");
    }

    DiasAccess diasAccessor = new DiasAccess();
    if (cLine.hasOption("a")) {
      // TODO Check command line option.
      diasAccessor.setAccessServer(cLine.getOptionValue("a"));
    }
    if (cLine.hasOption("n")) {
      // TODO Check command line option.
      diasAccessor.setAccessPort(Integer.parseInt(cLine
          .getOptionValue("n")));
    }
    if (cLine.hasOption("u")) {
      diasAccessor.setUseHttps(false);
    }
    boolean list = false;
    if (cLine.hasOption("l")) {
      list = true;
    }

    // long requestStartTime = System.currentTimeMillis();

    // Ccommands.
    String diasStringResponse = null;
    try {
      if (requestType.equals("fullmetadata")) {
        if (cLine.hasOption("x")) {// by external id
          // Note: We could just use requestFullMdByExtId with 3
          // parameters because dipFormat is set to the correct
          // default and dias should produce the correct result. But
          // it would always test only the long url of UOF.ac.MQ6A and
          // not UOF.ac.MQ4A.
          if (cLine.hasOption("d")) {
            // With specified dip format.
            diasStringResponse = diasAccessor.requestFullMdByExtId(
                id, responseFormat, dipFormat);
          } else {
            diasStringResponse = diasAccessor.requestFullMdByExtId(
                id, responseFormat);
          }
        } else {
          // By internal ID.
          // Note: We could just use requestFullMdByIntId with 3
          // parameters because dipFormat is set to the correct
          // default and dias should produce the correct result. But
          // it would always test only the long url of UOF.ac.MQ6 and
          // not UOF.ac.MQ4.
          if (cLine.hasOption("d")) {
            // With specified dip format.
            diasStringResponse = diasAccessor.requestFullMdByIntId(
                id, responseFormat, dipFormat);
          } else {
            diasStringResponse = diasAccessor.requestFullMdByIntId(
                id, responseFormat);
          }
        }
      } else if (requestType.equals("asset")) {
        if (cLine.hasOption("x")) {
          // By external ID.
          // Note: Asset request by external id are not directly
          // supported by dias.
          diasStringResponse = diasAccessor.requestShortMdByExtId(id,
              responseFormat, false);
          DiasAccessResponse diasResponse = parseDiasResponse(new StringReader(
              diasStringResponse));
          // Note: In this case we can **NOT** just use requestAsset
          // with 3 parameters because if no dipFormat is specified
          // dias unpacks the asset in a separate directory.
          if (cLine.hasOption("d")) {
            // With specified dip format.
            diasStringResponse = diasAccessor.requestAsset(
                diasResponse.getInternalAssetId(),
                responseFormat, dipFormat);
          } else {
            diasStringResponse = diasAccessor.requestAsset(
                diasResponse.getInternalAssetId(),
                responseFormat);
          }

        } else {
          // By internal ID.
          // Note: In this case we can **NOT** just use requestAsset
          // with 3 parameters because if no dipFormat is specified
          // dias unpacks the asset in a separate directory.
          if (cLine.hasOption("d")) {// with specified dip format
            diasStringResponse = diasAccessor.requestAsset(id,
                responseFormat, dipFormat);
          } else {
            diasStringResponse = diasAccessor.requestAsset(id,
                responseFormat);
          }
        }

      } else {
        // The default request type: short "metadata".
        if (cLine.hasOption("x")) {// by external id
          diasStringResponse = diasAccessor.requestShortMdByExtId(id,
              responseFormat, list);
        } else {
          // By internal ID.
          diasStringResponse = diasAccessor.requestShortMdByIntId(id,
              responseFormat);
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
      System.err.println("IOException while requesting information "
          + "from dias!");
      System.exit(2);
    }

    // System.out.println(diasStringResponse.length()/1024 +
    // " KB response received from Dias in " +
    // (KolibriTimestamp.getDurationInHours(
    // System.currentTimeMillis() - requestStartTime)) +
    // " = " +
    // ((diasStringResponse.length()/1024 )/
    // ((System.currentTimeMillis() -
    // requestStartTime)/1000)) +
    // " KB/sec");

    if (cLine.hasOption("p")) {
      System.out.println(diasStringResponse);
    }

    // We can't download or parse the answer if it is in html, so we exit.
    if (responseFormat.equals("html")) {
      System.exit(0);
    }

    DiasAccessResponse diasResponse = parseDiasResponse(new StringReader(
        diasStringResponse));
    // Download files.
    if (cLine.hasOption("g") && !requestType.equals("metadata")
        && diasResponse.getResponseType().equals("dip")) {

      long downloadStartTime = System.currentTimeMillis();
      File downloadedFile = null;

      // Download the specified command line file.
      if (requestType.equals("asset") && !cLine.hasOption("d")) {
        String fileToDownload = cLine.getOptionValue("g");
        if (fileToDownload != null) {
          try {
            downloadedFile = diasAccessor.downloadDiasFile(
                diasResponse, fileToDownload);
          } catch (IOException e) {
            e.printStackTrace();
            System.err.println("IOException while downloading "
                + "the file " + diasResponse.getUri()
                + fileToDownload + " from dias: "
                + e.getMessage());
          }
        } else {
          System.err.println("You need to specifiy a file if you "
              + "request an asset and do not specifiy a "
              + "dip format. Printing response instead:");
          System.out.println(diasResponse);
          System.exit(1);
        }
      } else {
        // Download the package mentioned in the diasResponse.
        try {
          downloadedFile = diasAccessor
              .downloadDiasPackage(diasResponse);
        } catch (IOException e) {
          e.printStackTrace();
          System.err
              .println("IOException while downloading the file "
                  + diasResponse.getUri() + "from dias");
        }
      }
      long duration = (System.currentTimeMillis() - downloadStartTime) / 1000;
      if (duration < 1) {
        duration = 1;
      }

      System.out.println((downloadedFile != null ? downloadedFile
          .length() : 0)
          / 1024
          + " KB asset received from Dias in "
          + KolibriTimestamp.getDurationInHours(duration)
          + " = "
          + ((downloadedFile.length() / 1024) / (duration))
          + " KB/sec");

    } else {
      // The default: print the response.
      System.out.println(diasResponse);
    }

    System.exit(0);
  }

  /***************************************************************************
   * Downloads the (relativ to the dias directory in the uri field of diasResponse) specified file
   * relativ to the current directory. E.g. if in the asset the source is "content/test/help.xml"
   * you will find help.xml it relativ to your directory in content/test.
   * 
   * @param diasResponse
   * @param source
   * @return File
   * @exception IOException
   **************************************************************************/
  public File downloadDiasFile(DiasAccessResponse diasResponse, String source)
      throws IOException {
    // TODO check that File.separator + source.toString() works and
    // does not result in a double fileSeparator
    File newFile = new File(source);
    String newDir = null;

    // if source has no specified parent dir in its path
    if (newFile.getParent() == null) {
      newDir = System.getProperty("user.dir") + File.separator;
    } else {
      newDir = System.getProperty("user.dir") + File.separator
          + newFile.getParent();
    }

    return downloadDiasFile(diasResponse, source, newDir);
  }

  /***************************************************************************
   * Downloads the specified file (relativ to the dias directory in the uri field of diasResponse)
   * to the destination directory. Uses the source filename as destination filename.
   * 
   * @param diasResponse
   * @param source
   * @param destinationDir
   * @return File
   * @exception IOException
   **************************************************************************/
  public File downloadDiasFile(DiasAccessResponse diasResponse,
      String source, String destinationDir) throws IOException {
    return downloadDiasFile(diasResponse, source, destinationDir, source);
  }

  /***************************************************************************
   * Downloads the specified file (relativ to the dias directory in the uri field of diasResponse)
   * to the destination directory. Uses a destination filename.
   * 
   * @param diasResponse
   * @param source
   * @param destinationDir
   * @param destinationFilename
   * @return Downloaded file.
   * @throws IOException
   **************************************************************************/
  public File downloadDiasFile(DiasAccessResponse diasResponse,
      String source, String destinationDir, String destinationFilename)
      throws IOException {
    if (diasResponse.getUri().equals(
        DiasAccessResponse.NOT_IN_DIAS_RESPONSE)) {
      throw new IOException("No file specified in dias response");
    }

    // Create parent dirs if not present.
    File newDir = new File(destinationDir);
    newDir.mkdirs();

    // Output file in destination dir with the destination file name.
    File newFile = new File(newDir, (new File(destinationFilename).getName()));

    Response response = this.diasClient.target(diasResponse.getUri() + source).request().get();
    // GetMethod diasConnection = new GetMethod(diasResponse.getUri() + source);
    InputStream diasInputStream = new BufferedInputStream(response.readEntity(InputStream.class));

    // TODO The following can be done in a very much simpler way!
    OutputStream localOutputStream = new BufferedOutputStream(new FileOutputStream(newFile));

    // Reading and saving the file.
    try {
      byte[] buffer = new byte[INPUT_BUFFER_LENGTH];

      int read = diasInputStream.read(buffer);
      while (read > 0) {
        localOutputStream.write(buffer, 0, read);
        read = diasInputStream.read(buffer);
      }
      localOutputStream.flush();
    } finally {
      // Clean up.
      if (diasInputStream != null) {
        diasInputStream.close();
      }
      if (localOutputStream != null) {
        localOutputStream.close();
      }
      response.close();
    }

    return newFile;
  }

  /***************************************************************************
   * Downloads the package in the diasResponse to the current directory.
   * 
   * TODO check that it works on unix *and* ms
   * 
   * @param diasResponse
   * @return File
   * @throws IOException
   **************************************************************************/
  public File downloadDiasPackage(DiasAccessResponse diasResponse)
      throws IOException {
    return downloadDiasPackage(diasResponse, System.getProperty("user.dir"));
  }

  /***************************************************************************
   * Downloads the package in the diasResponse to the current directory into a given directory.
   * 
   * TODO Check that it works on unix *and* ms!
   * 
   * @param diasResponse
   * @param destinationDir
   * @return File
   * @throws IOException
   **************************************************************************/
  public File downloadDiasPackage(DiasAccessResponse diasResponse,
      File destinationDir) throws IOException {
    return downloadDiasPackage(diasResponse,
        destinationDir.getAbsolutePath());
  }

  /***************************************************************************
   * Downloads the package in the diasResponse to the destination directory with the destination
   * fiilename.
   * 
   * @param diasResponse
   * @param destinationDir
   * @return File
   * @throws IOException
   **************************************************************************/
  public File downloadDiasPackage(DiasAccessResponse diasResponse,
      String destinationDir, String destinationFile) throws IOException {
    String uri = diasResponse.getUri();
    String uriParts[] = uri.split("/");
    String fileName = uriParts[uriParts.length - 1];
    String pathName = uri.substring(0, uri.lastIndexOf(fileName));
    diasResponse.setUri(pathName);

    return downloadDiasFile(diasResponse, fileName, destinationDir,
        destinationFile);
  }

  /***************************************************************************
   * Downloads the package in the diasResponse to the destination directory with the source
   * filename.
   * 
   * @param diasResponse
   * @param destinationDir
   * @return
   * @throws IOException
   **************************************************************************/
  public File downloadDiasPackage(DiasAccessResponse diasResponse,
      String destinationDir) throws IOException {
    String uri = diasResponse.getUri();
    String uriParts[] = uri.split("/");
    String fileName = uriParts[uriParts.length - 1];
    String pathName = uri.substring(0, uri.lastIndexOf(fileName));
    diasResponse.setUri(pathName);

    return downloadDiasFile(diasResponse, fileName, destinationDir,
        fileName);
  }

  /***************************************************************************
   * Downloads the package in the diasResponse to the current directory into a given directory.
   * 
   * TODO Check that it works on unix *and* ms!
   * 
   * @param diasResponse
   * @param destinationDir
   * @return File
   * @throws IOException
   **************************************************************************/
  public File downloadDiasMets(DiasAccessResponse diasResponse,
      String destinationDir) throws IOException {
    return downloadDiasPackage(diasResponse, destinationDir);
  }

  /***************************************************************************
   * Parses the access response of the DIAS. Uses parseDiasResponse(Reader diasResponse).
   * 
   * @param diasResponse
   **************************************************************************/
  public static DiasAccessResponse parseDiasResponse(String diasResponse) {
    return DiasAccess.parseDiasResponse(new StringReader(diasResponse));
  }

  /***************************************************************************
   * Parses the access response of the DIAS. Uses Jakarta Commons digester.
   * 
   * @param diasResponse
   * @return a DiasAccessResponse object which contains the values of the response.
   **************************************************************************/
  public static DiasAccessResponse parseDiasResponse(Reader diasAccessResponse) {
    Digester responseParser = new Digester();

    // Debugging code for digester:
    //
    // SimpleLog logger = new SimpleLog("bla");
    // responseParser.setLogger(logger);
    // logger.setLevel(SimpleLog.LOG_LEVEL_DEBUG);
    // System.out.println("Logger:" + logger + " Debug: "
    // + logger.isDebugEnabled());

    // TODO Strange bugs are the reason for setting the class loader!
    // CHECK IF WE CAN DO WITHOUT IT!
    // System.out.println(System.getProperty("java.system.class.loader"));
    // responseParser.setClassLoader(ClassLoader.getSystemClassLoader());
    // System.out.println(responseParser.getClassLoader().getClass());
    // Validating requires inline DTD in DIAS response.
    responseParser.setValidating(true);

    // Create a DiasAccessResponse object for the DIAS response and map the
    // properties.
    responseParser.addObjectCreate("DIASMETSRESPONSE",
        DiasAccessResponse.class);
    responseParser.addSetProperties("DIASMETSRESPONSE", "type",
        "responseType");
    // Note: It is necessary to repeat the property in lower case.
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DESCRIPTION",
        "description");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/REQUEST/"
        + "EXTERNALASSETID", "externalAssetId");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/REQUEST/"
        + "INTERNALASSETID", "internalAssetId");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/REQUEST/LIST",
        "list");

    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/ASSETBLOCK/"
        + "ASSET/INTERNALASSETID", "internalAssetId");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/ASSETBLOCK/"
        + "ASSET/URI", "uri");

    // Create a DiasAccessMetadataResponse object for the corresponding part
    // in the response and map the properties.
    responseParser.addObjectCreate("DIASMETSRESPONSE/DIP/METADATABLOCK/"
        + "METADATA",
        "de.langzeitarchivierung." + "kolibri.retrieval."
            + "DiasAccessMetadataResponse");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/"
        + "METADATABLOCK/METADATA/" + "INTERNALASSETID",
        "internalAssetId");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/"
        + "METADATABLOCK/METADATA/" + "EXTERNALASSETID",
        "externalAssetId");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/"
        + "METADATABLOCK/METADATA/" + "ASSETVERSION", "assetVersion");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/"
        + "METADATABLOCK/METADATA/" + "ASSETDESCRIPTION",
        "assetDescription");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/"
        + "METADATABLOCK/METADATA/" + "AIPISDELETED", "aipIsDeleted");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/"
        + "METADATABLOCK/METADATA/" + "AIPDELETIONTIMESTAMP",
        "aipDeletionTimestamp");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/"
        + "METADATABLOCK/METADATA/" + "ASSETCREATIONTIMESTAMP",
        "assetCreationTimestamp");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/"
        + "METADATABLOCK/METADATA/" + "ASSETINGESTTIMESTAMP",
        "assetIngestTimestamp");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/"
        + "METADATABLOCK/METADATA/" + "OLDOBJECTIDENTIFIER",
        "oldObjectIdentifier");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/"
        + "METADATABLOCK/METADATA/" + "OLDVERSION", "oldVersion");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/"
        + "METADATABLOCK/METADATA/SIZE", "size");
    responseParser.addBeanPropertySetter("DIASMETSRESPONSE/DIP/"
        + "METADATABLOCK/METADATA/" + "DIPRETRIEVEWAITTIME",
        "dipRetrieveWaitTime");
    // TODO --> TEST!!!
    responseParser.addSetProperties("DIASMETSRESPONSE/DIP/"
        + "METADATABLOCK/METADATA/" + "DIPRETRIEVEWAITTIME", "unit",
        "timeUnit");

    // Advance to next block.
    responseParser.addSetNext(
        "DIASMETSRESPONSE/DIP/METADATABLOCK/METADATA", "addMetadata");

    try {
      return (DiasAccessResponse) responseParser
          .parse(diasAccessResponse);
    } catch (SAXException e) {
      e.printStackTrace();
      System.err.println("DIAS response corrupt");
    } catch (IOException ie) {
      ie.printStackTrace();
      System.err.println("Could not read source for DIAS response");
    }

    return null;
  }

  /***************************************************************************
   * Requests only the "essential" metadata from Dias.
   * 
   * @param externalAssetID The external asset id.
   * @param responseFormat "xml" or "html"
   * @param list If true metadata is returned for all Assets in the collection of Assets with the
   *        specified externalAssetID
   * @return The DIAS response as String
   * @throws IOException thrown if responseFormat is unknown or the connection to DIAS failed
   **************************************************************************/
  public String requestShortMdByExtId(String externalAssetID,
      String responseFormat, boolean list) throws IOException {
    if (!isValidResponseFormat(responseFormat)) {
      throw new IOException("Unknown requested response format: "
          + responseFormat);
    }

    String requestUrl = BASE_URL + EXT_ID_URL + externalAssetID
        + RESPONSE_FORMAT_URL + responseFormat + REQUEST_TYPE_URL
        + REQUEST_METADATA;

    if (list) {
      requestUrl += LIST_FORMAT_URL + "true";
    }

    return retrieveDataFromDIAS(requestUrl);

  }

  /***************************************************************************
   * Requests only the "essential" metadata from Dias.
   * 
   * @param internalAssetID The internal asset id.
   * @param responseFormat "xml" or "html"
   * @return The DIAS response as String
   * @throws IOException thrown if responseFormat is unknown or the connection to DIAS failed
   **************************************************************************/
  public String requestShortMdByIntId(String internalAssetID,
      String responseFormat) throws IOException {
    if (!isValidResponseFormat(responseFormat)) {
      throw new IOException("Unknown requested response format: "
          + responseFormat);
    }
    return retrieveDataFromDIAS(BASE_URL + INT_ID_URL + internalAssetID
        + RESPONSE_FORMAT_URL + responseFormat + REQUEST_TYPE_URL
        + REQUEST_METADATA);
  }

  /***************************************************************************
   * Requests the full metadata from Dias.
   * 
   * @param externalAssetID The external asset id.
   * @param responseFormat "xml" or "html"
   * @return The DIAS response as String
   * @throws IOException thrown if responseFormat is unknown or the connection to DIAS failed
   **************************************************************************/
  public String requestFullMdByExtId(String externalAssetID,
      String responseFormat) throws IOException {
    if (!isValidResponseFormat(responseFormat)) {
      throw new IOException("Unknown requested response format: "
          + responseFormat);
    }
    return retrieveDataFromDIAS(BASE_URL + EXT_ID_URL + externalAssetID
        + RESPONSE_FORMAT_URL + responseFormat + REQUEST_TYPE_URL
        + REQUEST_FULL_METADATA);
  }

  /***************************************************************************
   * Requests the full metadata from Dias.
   * 
   * @param externalAssetID The external asset id.
   * @param responseFormat "xml" or "html"
   * @param dipFormat "zip", "tar" or "tar.gz"
   * @return The DIAS response as String
   * @throws IOException thrown if responseFormat or dipFormat is unknown or the connection to DIAS
   *         failed
   **************************************************************************/
  public String requestFullMdByExtId(String externalAssetID,
      String responseFormat, String dipFormat) throws IOException {
    if (!isValidResponseFormat(responseFormat)) {
      throw new IOException("Unknown requested response format: "
          + responseFormat);
    }
    if (!isValidDipFormat(dipFormat)) {
      throw new IOException("Unknown requested dip format: " + dipFormat);
    }
    return retrieveDataFromDIAS(BASE_URL + EXT_ID_URL + externalAssetID
        + RESPONSE_FORMAT_URL + responseFormat + REQUEST_TYPE_URL
        + REQUEST_FULL_METADATA + DIP_FORMAT_URL + dipFormat);
  }

  /***************************************************************************
   * Requests the full metadata from Dias.
   * 
   * @param internalAssetID The internal asset id.
   * @param responseFormat "xml" or "html"
   * @return The DIAS response as String
   * @throws IOException thrown if responseFormat is unknown or the connection to DIAS failed
   **************************************************************************/
  public String requestFullMdByIntId(String internalAssetID,
      String responseFormat) throws IOException {
    if (!isValidResponseFormat(responseFormat)) {
      throw new IOException("Unknown requested response format: "
          + responseFormat);
    }

    return retrieveDataFromDIAS(BASE_URL + INT_ID_URL + internalAssetID
        + RESPONSE_FORMAT_URL + responseFormat + REQUEST_TYPE_URL
        + REQUEST_FULL_METADATA);
  }

  /***************************************************************************
   * Requests the full metadata from Dias.
   * 
   * @param internalAssetID The internal asset id.
   * @param responseFormat "xml" or "html"
   * @param dipFormat "zip", "tar" or "tar.gz"
   * @return The DIAS response as String
   * @throws IOException thrown if responseFormat or dipFormat is unknown or the connection to DIAS
   *         failed
   **************************************************************************/
  public String requestFullMdByIntId(String internalAssetID,
      String responseFormat, String dipFormat) throws IOException {
    if (!isValidResponseFormat(responseFormat)) {
      throw new IOException("Unknown requested response format: "
          + responseFormat);
    }
    if (!isValidDipFormat(dipFormat)) {
      throw new IOException("Unknown requested dip format: " + dipFormat);
    }
    return retrieveDataFromDIAS(BASE_URL + INT_ID_URL + internalAssetID
        + RESPONSE_FORMAT_URL + responseFormat + REQUEST_TYPE_URL
        + REQUEST_FULL_METADATA + DIP_FORMAT_URL + dipFormat);
  }

  /***************************************************************************
   * Requests Asset in packed format from Dias.
   * 
   * @param internalAssetID The internal asset id.
   * @param responseFormat "xml" or "html"
   * @param dipFormat "zip", "tar" or "tar.gz"
   * @return The DIAS response as String
   * @throws IOException thrown if responseFormat or dipFormat is unknown or the connection to DIAS
   *         failed
   **************************************************************************/
  public String requestAsset(String internalAssetID, String responseFormat,
      String dipFormat) throws IOException {
    if (!isValidResponseFormat(responseFormat)) {
      throw new IOException("Unknown requested response format: "
          + responseFormat);
    }
    if (!isValidDipFormat(dipFormat)) {
      throw new IOException("Unknown requested dip format: " + dipFormat);
    }

    return retrieveDataFromDIAS(BASE_URL + INT_ID_URL + internalAssetID
        + RESPONSE_FORMAT_URL + responseFormat + REQUEST_TYPE_URL
        + REQUEST_ASSET + DIP_FORMAT_URL + dipFormat);
  }

  /***************************************************************************
   * Requests an unpacked Asset from Dias.
   * 
   * @param internalAssetID The internal asset id.
   * @param responseFormat "xml" or "html"
   * @return The DIAS response as String
   * @throws IOException thrown if responseFormat is unknown or the connection to DIAS failed
   **************************************************************************/
  public String requestAsset(String internalAssetID, String responseFormat)
      throws IOException {
    if (!isValidResponseFormat(responseFormat)) {
      throw new IOException("Unknown requested response format: "
          + responseFormat);
    }

    return retrieveDataFromDIAS(BASE_URL + INT_ID_URL + internalAssetID
        + RESPONSE_FORMAT_URL + responseFormat + REQUEST_TYPE_URL
        + REQUEST_ASSET);
  }

  // INTERNAL (Internal - implementation details, local classes, ...) ****

  /***************************************************************************
   * Determines if the specified String is a valid response format.
   * 
   * @param responseFormat the string to test
   * @return true if the parameter is a valid response format, false otherwise
   **************************************************************************/
  private static boolean isValidResponseFormat(String responseFormat) {
    for (int i = 0; i < VALID_RESPONSE_FORMATS.length; i++) {
      if (responseFormat.equals(VALID_RESPONSE_FORMATS[i])) {
        return true;
      }
    }

    return false;
  }

  /***************************************************************************
   * Determines if the specified String is a valid request type.
   * 
   * @param requestType the string to test
   * @return true if the parameter is a valid request type, false otherwise
   **************************************************************************/
  private static boolean isValidRequestType(String requestType) {
    for (int i = 0; i < VALID_REQUEST_TYPES.length; i++) {
      if (requestType.equals(VALID_REQUEST_TYPES[i])) {
        return true;
      }
    }

    return false;
  }

  /***************************************************************************
   * Determines if the specified String is a valid dip format.
   * 
   * @param dipFormat the string to test
   * @return true if the parameter is a valid dip format, false otherwise
   **************************************************************************/
  private static boolean isValidDipFormat(String dipFormat) {
    for (int i = 0; i < VALID_DIP_FORMATS.length; i++) {
      if (dipFormat.equals(VALID_DIP_FORMATS[i])) {
        return true;
      }
    }

    return false;
  }

  /***************************************************************************
   * Gets the data of the specified url from the server. If setUseHttps(false) has been called, http
   * is used, else (or if setUseHttps(true) has been called) https is used.
   * 
   * @param url The path of the data to retrieve (without serveraddress and port but with leading
   *        slash)
   * @return The retrieved data as a string
   * @throws IOException thrown if connection failed
   **************************************************************************/
  private String retrieveDataFromDIAS(String url) throws IOException {
    // Check protocol.
    String protocol = "https";
    if (!this.useHttps) {
      protocol = "http";
    }

    // Assemble the URL and log it.
    String completeUrl = protocol + "://" + this.accessServer + ":"
        + this.accessPort + url;
    defaultLogger.log(Level.FINE, "DIAS request URL: " + completeUrl);

    // Start retrieving data from DIAS.
    Response response = this.diasClient.target(completeUrl).request().get();
    // GetMethod diasRequest = new GetMethod(completeUrl);

    try {
      StringBuffer result = new StringBuffer();
      InputStream responseStream = new BufferedInputStream(response.readEntity(InputStream.class));

      byte[] buffer = new byte[INPUT_BUFFER_LENGTH];

      int read = responseStream.read(buffer);
      while (read > 0) {
        result.append(new String(buffer, 0, read));
        read = responseStream.read(buffer);
      }

      return result.toString();
    } finally {
      response.close();
    }
  }

  // GET & SET METHODS **************************************************

  /***************************************************************************
   * @return Returns the useHttps.
   **************************************************************************/
  public boolean isUseHttps() {
    return this.useHttps;
  }

  /***************************************************************************
   * @param _useHttps The useHttps to set.
   **************************************************************************/
  public void setUseHttps(boolean _useHttps) {
    this.useHttps = _useHttps;
  }

  /***************************************************************************
   * @return Returns the port.
   **************************************************************************/
  public int getAccessPort() {
    return this.accessPort;
  }

  /***************************************************************************
   * @param port The port to set.
   **************************************************************************/
  public void setAccessPort(int port) {
    this.accessPort = port;
  }

  /***************************************************************************
   * @return Returns the serverAddress.
   **************************************************************************/
  public String getAccessServer() {
    return this.accessServer;
  }

  /***************************************************************************
   * @param serverAddress The serverAddress to set.
   **************************************************************************/
  public void setAccessServer(String serverAddress) {
    this.accessServer = serverAddress;
  }

  /***************************************************************************
   * @return Returns the keyStoreFile.
   **************************************************************************/
  public String getKeyStoreFile() {
    return this.keyStoreFile;
  }

  /***************************************************************************
   * @param _keyStoreFile The keyStoreFile to set.
   **************************************************************************/
  public void setKeyStoreFile(String _keyStoreFile) {
    System.setProperty("javax.net.ssl.trustStore", _keyStoreFile);
    this.keyStoreFile = _keyStoreFile;
  }

}
