/**********************************************************************
 * de.langzeitarchivierung.kolibri.retrieval / DiasAccessResponse.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 *******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.retrieval;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/*******************************************************************************
 * The DIAS response to access requests as a bean. To understand the data fields
 * read the DIP Interface Specification.
 * 
 * @author Ludwig
 * @version 20070308
 * @since 20060118
 ******************************************************************************/

public class DiasAccessResponse implements Serializable
{

	// CONSTANTS **********************************************************

	private static final long					serialVersionUID		= 1L;

	public static final String					NOT_IN_DIAS_RESPONSE	= "NOT IN DIAS RESPONSE";
	public static final String					RESPONSE_TYPE_BUSY		= "busy";
	public static final String					RESPONSE_TYPE_ERROR		= "error";
	public static final String					RESPONSE_VERSION		= "1.0";
	public static final String					RESPONSE_TYPE_DIP		= "dip";

	// STATE (Instance variables) *****************************************

	private String								responseType;
	private String								responseVersion;

	// Between the DESCRIPTION tag:
	private String								description;

	// Between the REQUEST tag:
	// InternalAssetId may also appear at this place instead of externalAssetId.
	// InternalAssetId or externalAssetId has to be set.
	private String								externalAssetId			= NOT_IN_DIAS_RESPONSE;
	private String								list					= NOT_IN_DIAS_RESPONSE;

	// Between the DIP tag:

	// Between the ASSETBLOCK tag:
	private String								internalAssetId			= NOT_IN_DIAS_RESPONSE;
	private String								uri						= NOT_IN_DIAS_RESPONSE;
	private List<DiasAccessMetadataResponse>	metadata;

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * Constructor.
	 **************************************************************************/
	public DiasAccessResponse() {
		super();
		this.metadata = new ArrayList<DiasAccessMetadataResponse>();
	}

	// GET & SET METHODS **************************************************

	/***************************************************************************
	 * Gets the description.
	 * 
	 * @return Returns the description.
	 **************************************************************************/
	public String getDescription() {
		return this.description;
	}

	/***************************************************************************
	 * Sets the description.
	 * 
	 * @param _description The description to set.
	 **************************************************************************/
	public void setDescription(String _description) {
		this.description = _description;
	}

	/***************************************************************************
	 * Gets the externalAssetId.
	 * 
	 * @return Returns the externalAssetId.
	 **************************************************************************/
	public String getExternalAssetId() {
		return this.externalAssetId;
	}

	/***************************************************************************
	 * Sets the externalAssetId.
	 * 
	 * @param _externalAssetId The externalAssetId to set.
	 **************************************************************************/
	public void setExternalAssetId(String _externalAssetId) {
		this.externalAssetId = _externalAssetId;
	}

	/***************************************************************************
	 * Gets the internalAssetId.
	 * 
	 * @return Returns the internalAssetId.
	 **************************************************************************/
	public String getInternalAssetId() {
		return this.internalAssetId;
	}

	/***************************************************************************
	 * Sets the internalAssetId.
	 * 
	 * @param _internalAssetId The internalAssetId to set.
	 **************************************************************************/
	public void setInternalAssetId(String _internalAssetId) {
		this.internalAssetId = _internalAssetId;
	}

	/***************************************************************************
	 * Gets the list data field.
	 * 
	 * @return Returns the list.
	 **************************************************************************/
	public String getList() {
		return this.list;
	}

	/***************************************************************************
	 * Sets the list data field.
	 * 
	 * @param _list The list to set.
	 **************************************************************************/
	public void setList(String _list) {
		this.list = _list;
	}

	/***************************************************************************
	 * Gets the list of DiasAccessMetadata objects.
	 * 
	 * @return Returns the metadata.
	 **************************************************************************/
	public List<DiasAccessMetadataResponse> getMetadata() {
		return this.metadata;
	}

	/***************************************************************************
	 * Adds a DiasAccessMetadata object to the metadata list.
	 * 
	 * @param md the metadata object to add.
	 **************************************************************************/
	public void addMetadata(DiasAccessMetadataResponse md) {
		this.metadata.add(md);
	}

	/***************************************************************************
	 * Gets the responseType.
	 * 
	 * @return Returns the responseType.
	 **************************************************************************/
	public String getResponseType() {
		return this.responseType;
	}

	/***************************************************************************
	 * Sets the responseType.
	 * 
	 * @param _responseType The responseType to set.
	 **************************************************************************/
	public void setResponseType(String _responseType) {
		this.responseType = _responseType;
	}

	/***************************************************************************
	 * Gets the responseVersion.
	 * 
	 * @return Returns the responseVersion.
	 **************************************************************************/
	public String getResponseVersion() {
		return this.responseVersion;
	}

	/***************************************************************************
	 * Sets the responseVersion.
	 * 
	 * @param _responseVersion The responseVersion to set.
	 **************************************************************************/
	public void setResponseVersion(String _responseVersion) {
		this.responseVersion = _responseVersion;
	}

	/***************************************************************************
	 * Gets the URI.
	 * 
	 * @return Returns the uri.
	 **************************************************************************/
	public String getUri() {
		return this.uri;
	}

	/***************************************************************************
	 * Sets the URI
	 * 
	 * @param _uri The uri to set.
	 **************************************************************************/
	public void setUri(String _uri) {
		this.uri = _uri;
	}

	// QUERIES (Queries - no change to object's state) ********************

	/***************************************************************************
	 * @see java.lang.Object#toString()
	 **************************************************************************/
	@Override
	public String toString() {
		String result = "";

		result += "responseType: " + this.responseType;
		result += "\nresponseVersion: " + this.responseVersion;
		result += "\ndescription: " + this.description;
		result += "\nexternalAssetId: " + this.externalAssetId;
		result += "\nlist: " + this.list;
		result += "\ninternalAssetId: " + this.internalAssetId;
		result += "\nuri: " + this.uri;

		int j = 0;
		for (Iterator i = this.metadata.iterator(); i.hasNext(); j++) {
			DiasAccessMetadataResponse element = (DiasAccessMetadataResponse) i
					.next();
			result += "\nmetadatablock #" + j + ":\n" + element;
		}

		return result;
	}

}
