/**
 * Copyright 2005-2020 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 */

/**
 * TODOLOG
 **
 * CHANGELOG
 *
 * 2007-07-10 - koLibRI version 1.0
 */

package de.langzeitarchivierung.kolibri;

import java.io.File;

/**
 * <p>
 * Contains the status value and its description of a step.
 * </p>
 * 
 * @author Ludwig
 * @version 2012-03-15
 * @since 2005-06-23
 */

public class Status {

  // **
  // CONSTANTS
  // **

  // How many status types are defined? Important for e.g. Policy.java.
  public static final int NUMBER_OF_STATUS_TYPES = 6;
  // Status type value that this action has to be done.
  public static final int TODO = 0;
  // Status type value that this action has been done.
  public static final int DONE = 1;
  // Status type value that this action is running.
  public static final int RUNNING = 2;
  // Status type value that this action failed.
  // TODO: Introduce different kinds of errors.
  public static final int ERROR = 3;
  // Status type value that this action has been cancelled.
  public static final int CANCELED = 4;
  // Status type value that this action is still running, but doesn't work as
  public static final int WARNING = 5;

  // **
  // STATE (Instance variables)
  // **

  private int type;
  private String description;
  private File currentFile;

  // **
  // CREATION (Constructors, factory methods, static/inst init)
  // **

  /**
   * <p>
   * Constructor.
   * </p>
   * 
   * @param t The status type. See the constant fields in this class.
   * @param descr An additional description of the status that makes sense as output to the user.
   */
  Status(int t, String descr) {
    super();
    this.type = t;
    this.description = descr;
    this.currentFile = null;
  }

  // **
  // GET & SET METHODS **************************************************
  // **

  /**
   * @return Returns the description.
   **************************************************************************/
  public String getDescription() {
    return this.description;
  }

  /**
   * @return Returns the type.
   **************************************************************************/
  public int getType() {
    return this.type;
  }

  /**
   * <p>
   * Returns a description of the current status.
   * </p>
   * 
   * @return meaning of the status type value
   **************************************************************************/
  public String getStatusText() {
    return getStatusText(this.type);
  }

  /**
   * <p>
   * Returns a description of the status types.
   * </p>
   * 
   * @return meaning of the status type values
   * @param statusType
   **************************************************************************/
  public static String getStatusText(int statusType) {
    switch (statusType) {
      case Status.TODO:
        return "TODO";
      case Status.DONE:
        return "DONE";
      case Status.CANCELED:
        return "CANCELED";
      case Status.ERROR:
        return "ERROR";
      case Status.RUNNING:
        return "RUNNING";
      case Status.WARNING:
        return "WARNING";
      default:
        return "UNDEFINED";
    }
  }

  /**
   * @param description The description to set.
   **************************************************************************/
  protected void setDescription(String description) {
    this.description = description;
  }

  /**
   * @param type The type to set.
   **************************************************************************/
  protected void setType(int type) {
    this.type = type;
  }

  /**
   * @return Returns the currentFilename.
   */
  public File getCurrentFile() {
    return this.currentFile;
  }

  /**
   * @param currentFile The current file to set.
   */
  public void setCurrentFile(File currentFile) {
    this.currentFile = currentFile;
  }

}
