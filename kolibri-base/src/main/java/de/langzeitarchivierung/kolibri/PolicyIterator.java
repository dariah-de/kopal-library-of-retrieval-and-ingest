/**********************************************************************
 * de.langzeitarchivierung.kolibri / PolicyIterator.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/*******************************************************************************
 * <p>
 * Iterator class for policies. Implements methods for convenient iteration.
 * </p>
 * 
 * TODO Check if it is sensible to use breadth-first instead of depth-first
 * (different iniLists())
 * 
 * @author Ludwig
 * @see de.langzeitarchivierung.kolibri.Policy
 * @see de.langzeitarchivierung.kolibri.Step
 * @see de.langzeitarchivierung.kolibri.Status
 * @since 20060131
 * @version 20070710
 ******************************************************************************/

public class PolicyIterator implements Iterator<Step> {

	// STATE (Instance variables) *****************************************

	// list of all steps except the dummy root
	private ArrayList<Step>	stepList;
	// keeps track of the index for the next Step returned by next()
	private int				currentStep;

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * <p>
	 * Constructor
	 * </p>
	 * 
	 * @param root
	 *            The constructor will iterate over the (sub) tree with this
	 *            root node
	 **************************************************************************/
	public PolicyIterator(Step root) {
		super();
		this.currentStep = 0;
		this.stepList = new ArrayList<Step>();

		// root is not added by this iniLists() call
		iniLists(root);
	}

	/***************************************************************************
	 * <p>
	 * Initializes the interator list with all child nodes of the specified
	 * node.
	 * </p>
	 * 
	 * @param node
	 **************************************************************************/
	private void iniLists(Step node) {
		ListIterator<Step> nodeIterator = node.getChildIterator();
		Step nodeToAdd;
		while (nodeIterator != null && nodeIterator.hasNext()) {
			nodeToAdd = (Step) nodeIterator.next();
			this.stepList.add(nodeToAdd);
			iniLists(nodeToAdd);
		}
	}

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * <p>
	 * NOT IMPLEMENTED. If you want to destruct a whole tree you can use
	 * Policy.destructTree().
	 * </p>
	 * 
	 * @exception UnsupportedOperationException
	 * @see java.util.Iterator#remove()
	 **************************************************************************/
	public void remove() {
		throw new UnsupportedOperationException();
	}

	/***************************************************************************
	 * <p>
	 * Checks if there is another unvisited element in the policy tree.
	 * </p>
	 * 
	 * @return true if there is another unvisited element in the policy tree,
	 *         false otherwise
	 * @see java.util.Iterator#hasNext()
	 **************************************************************************/
	public boolean hasNext() {
		if (this.stepList.size() > this.currentStep) {
			return true;
		}
		return false;
	}

	/***************************************************************************
	 * <p>
	 * Visits the next unvisited step in the policy tree in depth-first order.
	 * </p>
	 * 
	 * @return the next unvisited step
	 * @throws NoSuchElementException
	 *             if no unvisited step exists
	 * 
	 * @see java.util.Iterator#next()
	 **************************************************************************/
	public Step next() {
		if (hasNext()) {
			return this.stepList.get(this.currentStep++);
		}
		throw new NoSuchElementException();
	}

}
