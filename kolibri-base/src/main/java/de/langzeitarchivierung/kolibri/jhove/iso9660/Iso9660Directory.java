/*******************************************************************************
 * de.langzeitarchivierung.kolibri.jhove.iso9660 / Iso9660Directory.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.jhove.iso9660;

/*******************************************************************************
 * Representation of an Iso9660 Directory Descriptor
 * 
 * @author Matthias Neubauer, German National Library
 * @version 20051209
 * @see de.langzeitarchivierung.kolibri.jhove.iso9660.Iso9660File
 * @since 20051123
 ******************************************************************************/

public class Iso9660Directory extends Iso9660File
{

	// CLASS VARIABLES (Static variables) **********************************

	protected java.util.ArrayList	fileList		= new java.util.ArrayList();
	protected long					extentPosition	= 0;
	protected int					parentDirNumber	= 0;
	protected int					dirNr			= 0;

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * Default constructor
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051209
	 * @since 20051209
	 * 
	 **************************************************************************/
	public Iso9660Directory() {
		super();
	}

	/***************************************************************************
	 * Constructor that sets all information fields
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051209
	 * @since 20051209
	 * @param dL - The data length
	 * @param rDT - The recording date and time
	 * @param fUS - The file unit size
	 * @param iGS - The interleave gap size
	 * @param vSN - The volume sequence number
	 * @param fI - The file identifier
	 * 
	 **************************************************************************/
	public Iso9660Directory(long dL, String rDT, long fUS, long iGS, long vSN,
			String fI) {
		super(dL, rDT, fUS, iGS, vSN, fI);
	}

	// GET & SET METHODS **************************************************

	/***************************************************************************
	 * Get-method for the data length
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @return long - The file list
	 **************************************************************************/
	public java.util.ArrayList getFileList() {
		return this.fileList;
	}

	/***************************************************************************
	 * Add a Iso9660File to the file list
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @param file - The file to put into the list
	 **************************************************************************/
	public void addToFileList(Iso9660File file) {
		this.fileList.add(file);
	}

	/***************************************************************************
	 * Retrieve a Iso9660File from the file list
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @param i - Index of the file to get from the list
	 * @return Iso9660File - The asked file
	 **************************************************************************/
	public Iso9660File getFileFromFileList(int i) {
		return (Iso9660File) this.fileList.get(i);
	}

	/***************************************************************************
	 * Retrieve the length of the file list
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @return int - The length of the file list
	 **************************************************************************/
	public int getLengthOfFileList() {
		return this.fileList.size();
	}

	/***************************************************************************
	 * Retrieve the position of this directories extent
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20060620
	 * @since 20060620
	 * @return long - The position of the extent
	 **************************************************************************/
	public long getExtentPosition() {
		return this.extentPosition;
	}

	/***************************************************************************
	 * Retrieve the position of this directories extent
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20060620
	 * @since 20060620
	 * @return long - The position of the extent
	 **************************************************************************/
	public int getParentDirNumber() {
		return this.parentDirNumber;
	}

	/***************************************************************************
	 * Retrieve the position of this directories extent
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20060620
	 * @since 20060620
	 * @return long - The directory number of this object
	 **************************************************************************/
	public int getDirNumber() {
		return this.dirNr;
	}

	/***************************************************************************
	 * Set the position of this directories extent
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20060620
	 * @since 20060620
	 * @param extPos - The position of the extent
	 **************************************************************************/
	public void setExtentPosition(long extPos) {
		this.extentPosition = extPos;
	}

	/***************************************************************************
	 * Set the position of this directories extent
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20060620
	 * @since 20060620
	 * @param parDirNum - The position of the extent
	 **************************************************************************/
	public void setParentDirNumber(int parDirNum) {
		this.parentDirNumber = parDirNum;
	}

	/***************************************************************************
	 * Set the position of this directories extent
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20060620
	 * @since 20060620
	 * @param DirNum - The position of the extent
	 **************************************************************************/
	public void setDirNumber(int DirNum) {
		this.dirNr = DirNum;
	}

}
