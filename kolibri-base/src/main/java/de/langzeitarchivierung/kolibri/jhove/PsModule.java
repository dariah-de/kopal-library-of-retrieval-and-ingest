/*******************************************************************************
 * de.langzeitarchivierung.kolibri.jhove / PsModule.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 * 
 * CHANGELOG:
 * 	2016-05-10	Funk	Fixed some Sonar issues.
 *	2007-07-10	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.jhove;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import edu.harvard.hul.ois.jhove.Agent;
import edu.harvard.hul.ois.jhove.AgentType;
import edu.harvard.hul.ois.jhove.ChecksumInputStream;
import edu.harvard.hul.ois.jhove.Checksummer;
import edu.harvard.hul.ois.jhove.Document;
import edu.harvard.hul.ois.jhove.DocumentType;
import edu.harvard.hul.ois.jhove.ErrorMessage;
import edu.harvard.hul.ois.jhove.ExternalSignature;
import edu.harvard.hul.ois.jhove.Identifier;
import edu.harvard.hul.ois.jhove.IdentifierType;
import edu.harvard.hul.ois.jhove.InfoMessage;
import edu.harvard.hul.ois.jhove.ModuleBase;
import edu.harvard.hul.ois.jhove.Property;
import edu.harvard.hul.ois.jhove.PropertyArity;
import edu.harvard.hul.ois.jhove.PropertyType;
import edu.harvard.hul.ois.jhove.RepInfo;
import edu.harvard.hul.ois.jhove.SignatureType;
import edu.harvard.hul.ois.jhove.SignatureUseType;

/*******************************************************************************
 * Jhove module for identification and validation of postscript files.
 * 
 * TODO In executeGhostscript() cmd_array für Linux etc.
 * 
 * @author Sabine Liess, SUB Göttingen
 * @version 2007-07-10
 ******************************************************************************/

public class PsModule extends ModuleBase {

	// CLASS VARIABLES (Static variables) **********************************

	private static final String						NAME								= "PS-sub";
	private static final String						RELEASE								= "0.3";

	private static final int[]						DATE								= {
			2007, 1, 27																};
	private static final String[]					FORMAT								= {
			"PS", "Postscript application"												};
	private static final String						COVERAGE							= "Postscript up to Level 3, "
																								+ "PostScript Level 1 (1, 1.0): 1984, "
																								+ "PostScript Level 2 (2.0, 2.1): 1991, "
																								+ "PostScript 3 (3, 3.0): 1998.";
	private static final String[]					MIMETYPE							= { "application/postscript" };
	private static final String						WELLFORMED							= "PostScript Language Document Structuring Conventions (DSC) Specification Version 3.0 #5001: "
																								+ "'Conform to the document structuring conventions (DSC) version 3.0.. "
																								+ "A conforming document is recognized by the header "
																								+ "comment %!PS-Adobe-[version] and is optionally followed by "
																								+ "keywords indicating the type of document. ... "
																								+ "A conforming PostScript language document description must have a clearly defined "
																								+ "prolog and script separated by the %%EndProlog comment.' "
																								+ "For this module a document is not wellformed if: "
																								+ "- various comment-keywords have wrong syntax	"
																								+ "- 'atend' is used where not allowed "
																								+ "- 'EndProlog' is not set after 'EndComments' "
																								+ "- Version 1.0 up to 2.0 does not have the required comments ('EndProlog', 'Trailer', 'Page', 'DocumentFonts') "
																								+ "- first line has wrong syntax, not '%!PS-Adobe-[version]' "
																								+ "- an error occurs while reading the document"
																								+ "- a line is longer than 255 byte"
																								+ "- declared value of '%%Pages:' doesn't match number of '%%Page:'"
																								+ "- declared '%%IncludeResource:' and '%%DocumentNeededResources:' don't match "
																								+ "- declared '%%PlateColor:' has wrong syntax. "
																								+ "The following syntax-errors lead only to warnings per infoMessage: "
																								+ "- unknown commenttype "
																								+ "- Version 2.1-discontinued comment used "
																								+ "- Version 3-deprecated comment used "
																								+ "- not specified comment for version "
																								+ "- not specified comment with or without keyword/colon.";
	private static final String						VALIDITY							= "As discussed in Chapter 3 of the "
																								+ "PostScript Language Reference Manual, Second Edition, "
																								+ "the PostScript (TM) language standard does not specify the overall "
																								+ "structure of a PostScript language program. "
																								+ "Any sequence of tokens conforming to the syntax and semantics "
																								+ "of the PostScript language is a valid program that may be "
																								+ "presented to a PostScript interpreter for execution. "
																								+ "For this module a document is "
																								+ "valid if "
																								+ "the PostScript interpreter Gostscript "
																								+ "(AFPL Ghostscript 8.54 (2006-05-17) Copyright (C) 2005 artofcode LLC, Benicia, CA.) "
																								+ "executes with exit 0, "
																								+ "not valid if "
																								+ "its exit is < 0 and "
																								+ "the validity is undetermined "
																								+ "in all other cases, also if the interpreter is not executed "
																								+ "or the process is destroyed by this module before having been finished. "
																								+ "The process is only started if this.ghostscriptPath is set, "
																								+ "it is destroyed if proc.exitValue() catches an IllegalThreadStateException "
																								+ "or at the end of this.executeGhostscript.";
	private static final String						NOTE								= null;
	private static final String						REPINFO								= "The output properties can be choosen for 11 different comment groups: "
																								+ "WITHOUTKEYCOMMENT, GENERALHEADERCOMMENT, GENERALBODYCOMMENT, "
																								+ "GENERALPAGECOMMENT, GENERALTRAILERCOMMENT, REQUIREMENTHEADERCOMMENT, "
																								+ "COLORHEADERCOMMENT, COLORBODYCOMMENT, COLORPAGECOMMEN, QUERYCOMMENT, "
																								+ "EXITSERVERCOMMENT, NOTSPECIFIEDCOMMENT, PAGELAYOUTCOMMENT."
																								+ " Several messages are created with:"
																								+ " The first line of each embedded PostScript file "
																								+ "(if the length is > 255 byte, its shortened to 50 byte)."
																								+ " Concerning the process that starts the interpreter the exit-Status and the output of the process."
																								+ " The name of embedded resources ('%%Include[Document | Font | ProcSet | Resource | File | Feature]').";

	private static final String						RIGHTS								= "Copyright 2005-2007 by Project kopal, "
																								+ "http://kopal.langzeitarchivierung.de/index.php.en";

	private Checksummer								_ckSummer;
	private ChecksumInputStream						_cstream;
	private DataInputStream							_dstream;

	private boolean									_sigMatch;

	private long									offset;
	private String									line;
	private String									firstLine;
	private String									version;
	private String									profile;
	private String									jobType;
	private String									fileOrUri;
	private String									psDocName;

	// used for execution of gostscript
	// AFPL Ghostscript 8.54 (2006-05-17)
	// Copyright (C) 2005 artofcode LLC, Benicia, CA.
	// All rights reserved.
	// This software comes with NO WARRANTY: see the file PUBLIC for details.
	private String									systemOsName;
	private String									ghostscriptPath						= "";
	private String									ghostscriptOutput;
	private String									ghostscriptErrorOutput;

	// filled with filename and versionNumber of all postscript-file
	// (mainDoc and embedded)
	private java.util.Hashtable<String, Integer>	listDocs;

	// filled per single document (mainDoc or embedded document)
	private Vector<Vector<Object>>					allComLinesPerDoc;
	private Vector<ArrayList>						commentsWithKeywordsPerDoc;
	private Vector<ArrayList>						commentsWithoutKeywordsPerDoc;
	private Vector<ArrayList>						beginEndCommentsPerDoc;
	private java.util.Hashtable<String, Long>		hashBeginEndCommentsPerDoc;
	private java.util.Hashtable<String, String>		docNeededRes;
	private java.util.Hashtable<String, String>		docInclRes;
	private int										valuePages;
	private int										numberOfPages;
	private int										versionNumber;

	// also listed in allPostscriptComment.pdf
	private ArrayList<String>						allCommentsWOKeywords;
	private java.util.Hashtable<String, Integer>	allCommentsWithKeywords;
	private ArrayList<String>						allV3DeprecatedComments;
	private ArrayList<String>						allAtendComments;
	private ArrayList<String>						requiredV1V20Comments;
	private ArrayList<String>						allV1V20Comments;
	private ArrayList<String>						allV21Comments;
	private ArrayList<String>						allV3Comments;

	private Hashtable<Long, String>					unknownComments;

	private ArrayList<String>						docdata;
	private ArrayList<String>						emulationModes;
	private ArrayList<String>						extensions;
	private ArrayList<String>						orientations;
	private ArrayList<String>						orders;
	private ArrayList<String>						types;
	private ArrayList<String>						modes;
	private ArrayList<String>						colors;
	private ArrayList<String>						dcColornames;
	private ArrayList<String>						pcColornames;
	private String									plateColorname;
	private ArrayList<String>						resources;
	private ArrayList<String>						requirements;
	private ArrayList<String>						vmlocations;

	// comment types
	private static final int						GENERALHEADERCOMMENT				= 0;
	private static final int						GENERALBODYCOMMENT					= 1;
	private static final int						GENERALPAGECOMMENT					= 2;
	private static final int						GENERALTRAILERCOMMENT				= 3;
	private static final int						REQUIREMENTHEADERCOMMENT			= 4;
	private static final int						COLORHEADERCOMMENT					= 5;
	private static final int						COLORBODYCOMMENT					= 6;
	private static final int						COLORPAGECOMMENT					= 7;
	private static final int						QUERYCOMMENT						= 8;
	private static final int						EXITSERVERCOMMENT					= 9;
	private static final int						WITHOUTKEYCOMMENT					= 10;
	private static final int						NOTSPECIFIEDCOMMENT					= 11;
	private static final int						PAGELAYOUTCOMMENT					= 12;

	// Postscript versions
	private static final int						PSVERSION1							= 1;
	private static final int						PSVERSION20							= 20;
	private static final int						PSVERSION21							= 21;
	private static final int						PSVERSION3							= 3;

	// return values this.executeGhostscript
	private static final int						GHOSTSCRIPT_NOERROR					= 0;
	private static final int						GHOSTSCRIPT_ERROR					= -1;
	private static final int						GHOSTSCRIPT_FATALERROR				= -2;
	private static final int						GHOSTSCRIPT_PROCESSDESTROYED		= -3;
	private static final int						GHOSTSCRIPT_WRONGCOMMAND			= -4;
	private static final int						GHOSTSCRIPT_UNKNOWNEXCEPTION		= -5;
	private static final int						GHOSTSCRIPT_EXECUTION_NOT_ALLOWED	= -6;

	// output Jhove-properties per comment type
	private static boolean							outputGeneralHeaderComments;
	private static boolean							outputGeneralBodyComments;
	private static boolean							outputGeneralPageComments;
	private static boolean							outputGeneralTrailerComments;
	private static boolean							outputRequirementHeaderComments;
	private static boolean							outputColorHeaderComments;
	private static boolean							outputColorBodyComments;
	private static boolean							outputColorPageComments;
	private static boolean							outputQueryComments;
	private static boolean							outputExitServerComments;
	private static boolean							outputWithoutKeyComments;
	private static boolean							outputNotSpecifiedComments;
	private static boolean							outputPageLayoutComments;

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * edu.harvard.hul.ois.jhove.ModuleBase.ModuleBase(String name, String
	 * release, int[] date, String[] format, String coverage, String[] mimeType,
	 * String wellFormedNote, String validityNote, String repInfoNote, String
	 * note, String rights, boolean isRandomAccess) Constructors of all
	 * subclasses of ModuleBase should call this as a super constructor.
	 * 
	 * Parameters: name Name of the module release Release identifier date Last
	 * modification date of the module code, in the form of an array of three
	 * numbers. date[0] is the year, date[1] the month, and date[2] the day.
	 * format Array of format names supported by the module coverage Details as
	 * to the specific format versions or variants that are supported by the
	 * module mimeType Array of MIME type strings for formats supported by the
	 * module wellFormedNote Brief explanation of what constitutes well-formed
	 * content validityNote Brief explanation of what constitutes valid content
	 * repInfoNote Note pertaining to RepInfo (may be null) note Additional
	 * information about the module (may be null) rights Copyright notice for
	 * the module isRandomAccess true if the module treats content as
	 * random-access data, if it treats content as stream data
	 **************************************************************************/
	public PsModule() {
		super(NAME, RELEASE, DATE, FORMAT, COVERAGE, MIMETYPE, WELLFORMED,
				VALIDITY, REPINFO, NOTE, RIGHTS, false);

		Agent agent = new Agent(
				"Goettingen State and University Library (SUB)",
				AgentType.EDUCATIONAL);
		agent.setAddress("Papendiek 14, 37073 Goettingen, Germany");
		agent.setTelephone("+49 551 39 0");
		agent.setEmail("kolibri@kopal.langzeitarchivierung.de");
		this._vendor = agent;

		Agent agentAdobe = new Agent("Adobe Systems Incorporated",
				AgentType.COMMERCIAL);
		agentAdobe.setAddress("345 Park Avenue, San Jose, CA 95110-2704");
		agentAdobe.setTelephone("408-536-6000");
		agentAdobe.setFax("408-537-6000");
		agentAdobe.setWeb("http://www.adobe.com/");
		Document doc = new Document(
				"PostScript Language Reference, Second Edition",
				DocumentType.BOOK);
		doc.setNote("1999-05.07: This second edition of the PostScript Language Reference Manual is now obsolete,"
				+ " and is superseded by the third edition: (PostScript(R) Language Reference, Third Edition)");
		doc.setAuthor(agentAdobe);
		doc.setPublisher(agentAdobe);
		doc.setDate("1990-12");
		doc.setIdentifier(new Identifier(
				"http://partners.adobe.com/public/developer/en/ps/psrefman.pdf",
				IdentifierType.URL));
		doc.setIdentifier(new Identifier("0-201-18127-4", IdentifierType.ISBN));
		doc.setPublisher(new Agent("Addison-Wesley", AgentType.COMMERCIAL));
		this._specification.add(doc);

		doc = new Document(
				"PostScript Language Document Structuring Conventions (DSC) Specification Version 3.0 #5001",
				DocumentType.BOOK);
		doc.setAuthor(agentAdobe);
		doc.setPublisher(agentAdobe);
		doc.setDate("1992-09-25");
		doc.setIdentifier(new Identifier(
				"http://partners.adobe.com/public/developer/en/ps/5001.DSC_Spec.pdf",
				IdentifierType.URL));
		this._specification.add(doc);

		doc = new Document(
				"PostScript Language Reference Supplement for Version 3010 and 3011",
				DocumentType.BOOK);
		doc.setAuthor(agentAdobe);
		doc.setPublisher(agentAdobe);
		doc.setDate("1999-08-30");
		doc.setIdentifier(new Identifier(
				"http://partners.adobe.com/public/developer/en/ps/PS3010and3011.Supplement.pdf",
				IdentifierType.URL));
		this._specification.add(doc);

		doc = new Document(
				"Errata, For Postscript Language Reference, Third Edition, and for PostScript Language Reference Supplement for Version 3010 and 3011",
				DocumentType.BOOK);
		doc.setAuthor(agentAdobe);
		doc.setPublisher(agentAdobe);
		doc.setDate("2004-01-27");
		doc.setIdentifier(new Identifier(
				"http://partners.adobe.com/public/developer/en/ps/PSerrata.txt",
				IdentifierType.URL));
		this._specification.add(doc);

		doc = new Document("PostScript(R) Language Reference, Third Edition",
				DocumentType.BOOK);
		doc.setAuthor(agentAdobe);
		doc.setPublisher(agentAdobe);
		doc.setIdentifier(new Identifier(
				"http://www.adobe.com/products/postscript/pdfs/PLRM.pdf",
				IdentifierType.URL));
		doc.setIdentifier(new Identifier("0-201-37922-8", IdentifierType.ISBN));
		doc.setPublisher(new Agent("Addison-Wesley", AgentType.COMMERCIAL));
		this._specification.add(doc);

		doc = new Document(
				"PostScript(R) Language Document Comment Extensions for Page Layout",
				DocumentType.BOOK);
		doc.setNote("Technical Note #5644, "
				+ "\"These comments are not yet official Document Structuring Convention (DSC) comments,"
				+ " and hence will be referred to as structuring comments , and not as DSC comments.\"");
		doc.setAuthor(agentAdobe);
		doc.setPublisher(agentAdobe);
		doc.setDate("1999-05-20");
		doc.setIdentifier(new Identifier(
				"http://partners.adobe.com/public/developer/en/ps/5644.Comment_Ext.pdf",
				IdentifierType.URL));
		this._specification.add(doc);

		Agent agentAladdin = new Agent("Aladdin Enterprises",
				AgentType.COMMERCIAL);
		agentAladdin.setAddress("Menlo Park, California, U.S.A.");
		agentAladdin.setWeb("http://www.aladdin.com/");
		agentAdobe.setWeb("http://www.adobe.com/");

		doc = new Document("Aladdin Free Public License, Version 9",
				DocumentType.WEB);
		doc.setDate("2000-09-18");
		doc.setAuthor(agentAladdin);
		doc.setPublisher(agentAladdin);
		doc.setIdentifier(new Identifier(
				"http://www.artifex.com/downloads/doc/Public.htm",
				IdentifierType.URL));

		this._signature.add(new ExternalSignature(".ps",
				SignatureType.EXTENSION, SignatureUseType.OPTIONAL));
		this._signature.add(new ExternalSignature(".eps",
				SignatureType.EXTENSION, SignatureUseType.OPTIONAL));

		this._bigEndian = false;

		PsModule.outputColorBodyComments = true;
		PsModule.outputColorHeaderComments = true;
		PsModule.outputColorPageComments = true;
		PsModule.outputExitServerComments = true;
		PsModule.outputGeneralBodyComments = true;
		PsModule.outputGeneralHeaderComments = true;
		PsModule.outputGeneralPageComments = true;
		PsModule.outputGeneralTrailerComments = true;
		PsModule.outputQueryComments = true;
		PsModule.outputRequirementHeaderComments = true;
		PsModule.outputWithoutKeyComments = true;
		PsModule.outputNotSpecifiedComments = true;
		PsModule.outputPageLayoutComments = true;
	}

	/***************************************************************************
	 * Reset parameter settings. Returns to a default state without any
	 * parameters.
	 **************************************************************************/
	public void resetParams() throws Exception {
		this.ghostscriptPath = "";
	}

	/***************************************************************************
	 * Per-action initialization. May be called multiple times.
	 * 
	 * @param The
	 *            module parameter; under command-line Jhove, the -p parameter.
	 *            The parameter in jhove.conf sets the path to th ghostscript
	 *            exe. The parameter is case-independent. A null parameter is
	 *            equivalent to the empty string.
	 **************************************************************************/
	public void param(String param) {
		if (param != null) {
			// this.setGhostscriptPath("C:\\Programme\\gs\\gs8.54\\bin\\");
			this.setGhostscriptPath(param.trim());
		}
	}

	/***************************************************************************
	 * Parsing methods.
	 **************************************************************************/

	/***************************************************************************
	 * Check if the digital object conforms to this Module's internal signature
	 * information. Parse the first line and the comments of a Postscript file
	 * and store the results in RepInfo.
	 * 
	 * @param stream
	 *            An InputStream, positioned at its beginning, which is
	 *            generated from the object to be parsed
	 * @param info
	 *            - the RepInfo object A fresh RepInfo object which will be
	 *            modified to reflect the results of the parsing
	 * @param parseIndex
	 *            Must be 0 in first call to <code>parse</code>. If
	 *            <code>parse</code> returns a nonzero value, it must be called
	 *            again with <code>parseIndex</code> equal to that return value.
	 **************************************************************************/
	public int parse(InputStream stream, RepInfo info, int parseIndex)
			throws IOException {
		try {
			initParse();
			info.setModule(this);
			info.setFormat(this._format[0]);

			info.setMimeType(PsModule.MIMETYPE[0]);

			if (this._app != null && this._je.getChecksumFlag()
					&& info.getChecksum().size() == 0) {
				this._ckSummer = new Checksummer();
				this._cstream = new ChecksumInputStream(stream, this._ckSummer);
				this._dstream = getBufferedDataStream(this._cstream,
						this._app != null ? this._je.getBufferSize() : 0);
			} else {
				this._dstream = getBufferedDataStream(stream,
						this._app != null ? this._je.getBufferSize() : 0);
			}
			this.psDocName = info.getUri()
					.substring(info.getUri().lastIndexOf('\\') + 1).trim();
			this.listDocs.put(this.psDocName.trim(), 0);
			// read all lines
			// save vector with commentlines and docname to vector
			// allComLinesPerDoc
			// and read and check first line
			if (!this.readLines(this.psDocName, info)) {
				info.setWellFormed(RepInfo.FALSE);
				return 0;
			}
			// get the docname and all commentlines per document
			// and distribute and check them
			String docName;
			int i = 0;
			for (Iterator iter = this.allComLinesPerDoc.iterator(); iter
					.hasNext();) {
				this.beginEndCommentsPerDoc.clear();
				this.hashBeginEndCommentsPerDoc.clear();
				this.commentsWithoutKeywordsPerDoc.clear();
				this.commentsWithKeywordsPerDoc.clear();
				this.requiredV1V20Comments.clear();
				this.unknownComments.clear();
				this.numberOfPages = 0;
				this.valuePages = 0;
				this.docInclRes = new java.util.Hashtable<String, String>();
				this.docNeededRes = new java.util.Hashtable<String, String>();
				if (this.dcColornames != null)
					this.dcColornames.clear();
				this.plateColorname = "";

				// get the docname and all commentlines per document
				Vector comLin = (Vector) iter.next();
				docName = comLin.get(0).toString();

				int versNum = 0;
				comLin.remove(0);
				comLin = this.concatenateMultilineComments(comLin);
				if (this.listDocs.containsKey(docName)) {
					versNum = (this.listDocs.get(docName)).intValue();
				}
				for (i = 0; i < comLin.size(); i++) {
					// distribute and check the single comments
					this.distributeComment(docName, versNum,
							(ArrayList) comLin.get(i), info);
				}

				if (this.listDocs.containsKey(docName)) {
					this.checkVersionCompatibility(docName, versNum,
							this.commentsWithKeywordsPerDoc, info);
					this.checkVersionCompatibility(docName, versNum,
							this.commentsWithoutKeywordsPerDoc, info);

					if (!this.unknownComments.isEmpty()) {
						for (Enumeration e = this.unknownComments.keys(); e
								.hasMoreElements();) {
							Long offkey = (Long) e.nextElement();
							info.setMessage(new InfoMessage(
									this.unknownComments.get(offkey), docName,
									offkey.longValue()));

						}
					}
					this.checkDocStructure(docName, info);

					if (this.numberOfPages != this.valuePages) {
						info.setMessage(new ErrorMessage(
								"'"
										+ docName
										+ "': '%%Pages:' doesn't match number of '%%Page:'"));
						info.setWellFormed(RepInfo.FALSE);
					}
					if (versNum == PSVERSION1 || versNum == PSVERSION20) {
						this.checkRequiredCommentsV1V20(docName, info);
					}

					if (versNum == PSVERSION3) {
						// check docResources
						if (!this.docInclRes.isEmpty()
								|| !this.docNeededRes.isEmpty()) {
							if (!this.checkDocResources()) {
								info.setMessage(new ErrorMessage(
										"'"
												+ docName
												+ "': '%%IncludeResource:' and '%%DocumentNeededResources:' don't match"));
								info.setWellFormed(RepInfo.FALSE);
							}
						}
						// check plateColorname
						if (this.plateColorname.length() > 0) {
							if (this.dcColornames == null
									|| !this.dcColornames
											.contains(this.plateColorname)) {
								info.setMessage(new ErrorMessage("'"
										+ this.plateColorname + "' "
										+ "is unknown plateColorname."));
								info.setWellFormed(RepInfo.FALSE);
							}
						}
					}
				}
				// set the properties per document
				this.setProperties(docName, info);
			}

			this.fileOrUri = info.getUri().trim();

			if (this.ghostscriptPath.length() == 0) {
				info.setMessage(new InfoMessage(
						"Cannot execute Ghostscript, path not set."));
				info.setValid(RepInfo.UNDETERMINED);
			} else {
				int exitGhostscript = executeGhostscript();
				if (this.ghostscriptOutput.length() > 0) {
					info.setMessage(new InfoMessage("Ghostscript output: "
							+ this.ghostscriptOutput));
				}
				if (this.ghostscriptErrorOutput.length() > 0) {
					info.setMessage(new ErrorMessage(
							"Ghostscript error output: "
									+ this.ghostscriptErrorOutput));
				}
				String message = new String();
				boolean isError = true;

				switch (exitGhostscript) {
				case PsModule.GHOSTSCRIPT_NOERROR:
					message = "Ghostscript sends exit " + exitGhostscript;
					isError = false;
					info.setValid(RepInfo.TRUE);
					break;
				case PsModule.GHOSTSCRIPT_ERROR:
					message = "Ghostscript sends error: " + exitGhostscript;
					info.setValid(RepInfo.FALSE);
					break;
				case PsModule.GHOSTSCRIPT_FATALERROR:
					message = "Ghostscript sends fatal error: "
							+ exitGhostscript;
					info.setValid(RepInfo.FALSE);
					break;
				case PsModule.GHOSTSCRIPT_WRONGCOMMAND:
					message = "Could not execute Ghostscript, wrong command.";
					info.setValid(RepInfo.UNDETERMINED);
					break;
				case PsModule.GHOSTSCRIPT_PROCESSDESTROYED:
					message = "Ghostscript process has been destroyed.";
					info.setValid(RepInfo.UNDETERMINED);
					break;
				case PsModule.GHOSTSCRIPT_UNKNOWNEXCEPTION:
					message = "Unknown exception during Ghostscript-execution.";
					info.setValid(RepInfo.UNDETERMINED);
					break;
				case PsModule.GHOSTSCRIPT_EXECUTION_NOT_ALLOWED:
					message = "Security manager doesn't allow creation of a subprocess.";
					info.setValid(RepInfo.UNDETERMINED);
					break;
				default:
					message = "Console sends exit: " + exitGhostscript;
					info.setValid(RepInfo.FALSE);
					break;
				}
				if (isError) {
					info.setMessage(new ErrorMessage(message));
				} else {
					info.setMessage(new InfoMessage(message));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new IOException(e.getMessage());
		}
		return 0;
	}

	/***************************************************************************
	 * Initializes the state of the module for parsing.
	 * 
	 * @see edu.harvard.hul.ois.jhove.ModuleBase#initParse()
	 **************************************************************************/
	protected void initParse() {
		super.initParse();
		this._sigMatch = true;
		this.firstLine = new String();
		this.line = new String();
		this.version = new String();
		this.version = null;
		this.versionNumber = 0;
		this.profile = new String();
		this.profile = null;
		this.offset = 0;
		this.jobType = new String();
		this.ghostscriptOutput = new String();
		this.ghostscriptErrorOutput = new String();

		this.allComLinesPerDoc = new Vector<Vector<Object>>();
		this.commentsWithoutKeywordsPerDoc = new Vector<ArrayList>();
		this.beginEndCommentsPerDoc = new Vector<ArrayList>();
		this.hashBeginEndCommentsPerDoc = new java.util.Hashtable<String, Long>();
		this.unknownComments = new java.util.Hashtable<Long, String>();

		this.setColors();

		this.listDocs = new java.util.Hashtable<String, Integer>();
		this.commentsWithKeywordsPerDoc = new Vector<ArrayList>();
		this.requiredV1V20Comments = new ArrayList<String>();

		this.fillAllCommentsWOKeywords();
		this.fillAllCommentsWithKeywords();

		this.fillAllV1V20Comments();
		this.fillAllV21Comments();
		this.fillAllV3Comments();
		this.fillAllV3DeprecatedComments();
		this.fillAllAtendComments();
	}

	/***************************************************************************
	 * Read and verify first line, read commentlines and save to Vector
	 * comLines, start this method for each embedded [Document, Font, ProcSet,
	 * Resource, File, Feature] and save all comLines to Vector
	 * allComLinesPerDoc.
	 * 
	 * @param docName
	 * @param info
	 *            - the RepInfo object
	 * @return
	 **************************************************************************/
	private boolean readLines(String docName, RepInfo info) {
		Vector<Object> comLines = new Vector<Object>();
		comLines.add(docName);
		int a = 0;
		this.line = "";
		try {
			// read first line and check it
			// and create as message for embedded documents
			if (this.listDocs.containsKey(docName.trim())) {
				this.readFirstLine(docName, info);
				boolean maindoc = docName.equals(info.getUri()
						.substring(info.getUri().lastIndexOf('\\') + 1).trim());
				if (maindoc) {
					if (!this._sigMatch) {
						return false;
					}
				} else {
					info.setMessage(new InfoMessage("First line: '"
							+ this.firstLine + "'", docName, this.offset));
				}
				this.listDocs.remove(docName);
				this.listDocs.put(docName.trim(), this.versionNumber);
			}
			if (this.firstLine.startsWith("%%")) {
				this.line = this.firstLine.substring(2);
				ArrayList<Object> a1 = new ArrayList<Object>(2);
				a1.add(0, this.offset);
				a1.add(1, this.line.trim());
				comLines.addElement(a1);
				this.line = "";
			}
			while (this._dstream.available() > 1) {
				a = readUnsignedByte(this._dstream, this);
				while ((a == 10 || a == 13) && this._dstream.available() >= 1) {
					a = readUnsignedByte(this._dstream, this);
				}
				this.offset = getNByte() - 1;
				this.line = this.line + (char) a;
				while (a != 10 && a != 13 && this._dstream.available() >= 1) {
					a = readUnsignedByte(this._dstream, this);
					this.line = this.line + (char) a;
				}
				// cut the last byte
				if (a == 10 || a == 13) {
					this.line = this.line.substring(0, this.line.length() - 1);
				}
				// check the linelength
				if ((this.line.length() - 1) > 255) {
					info.setWellFormed(RepInfo.FALSE);
					info.setMessage(new ErrorMessage(
							"Linelength longer than 255", docName, this.offset));
				}
				// save the commentlines
				if (this.line.length() >= 3
						&& this.line.substring(0, 2).equals("%%")) {
					this.line = this.line.substring(2);
					ArrayList<Object> a1 = new ArrayList<Object>(2);
					a1.add(0, this.offset);
					a1.add(1, this.line.trim());
					comLines.addElement(a1);
					String name;
					int x;
					if (this.line.startsWith("BeginDocument")) {
						x = this.line.indexOf(':');
						name = this.line.substring(x + 1).trim();
						info.setMessage(new InfoMessage("Embedded Document"
								+ " : " + name, docName, this.offset));
						this.listDocs.put(name, 0);
						this.readLines(name, info);
					} else if (this.line.startsWith("EndDocument")
							|| this._dstream.available() < 1) {
						this.allComLinesPerDoc.add(comLines);
						return true;
					}
					String[] _types = { "Font", "ProcSet", "Resource", "File",
							"Feature" };
					for (int i = 0; i < _types.length; i++) {
						if (this.line.startsWith("Begin" + _types[i])) {
							x = this.line.indexOf(':');
							name = this.line.substring(x + 1).trim();
							info.setMessage(new InfoMessage("Embedded "
									+ _types[i] + " : " + name, docName,
									this.offset));
							this.readLines(name, info);
						} else if (this.line.startsWith("End" + _types[i])
								|| this._dstream.available() < 1) {
							this.allComLinesPerDoc.add(comLines);
							return true;
						}
					}
					this.line = "";
				}// endif %%
				this.line = "";
			}// endwhile
		} catch (IOException e) {
			info.setWellFormed(RepInfo.FALSE);
			info.setMessage(new ErrorMessage("Error while reading document",
					docName, this.offset));
			return true;
		} // endcatch
		this.allComLinesPerDoc.add(comLines);
		return true;
	}

	/***************************************************************************
	 * Read the first line, check if it is invalid with more than 255 byte,
	 * store to this.firstLine and call this.checkFirstLine(docName, info).
	 * 
	 * @param info
	 *            - the RepInfo object
	 * @return
	 * @throws IOException
	 **************************************************************************/
	private void readFirstLine(String docName, RepInfo info) throws IOException {
		String badHeader = docName + " has invalid first line";
		int b = 0;
		int i = 0;
		this.firstLine = "";
		// Lines must be terminated with one of the following combinations of
		// characters:
		// CR, LF, or CR LF. CR is the carriage-return character and LF
		// is the line-feed character (decimal ASCII 13 and 10, respectively).
		try {
			b = readUnsignedByte(this._dstream, this);
			while ((b == 10 || b == 13) && this._dstream.available() >= 1) {
				b = readUnsignedByte(this._dstream, this);
				i++;
			}
			this.offset = getNByte() - 1 + i;
			this.firstLine = this.firstLine + (char) b;
			i = 1;
			// read till end of line
			while (this._dstream.available() >= 1 && !(b == 10 || b == 13)) {
				b = readUnsignedByte(this._dstream, this);
				i++;
				this.firstLine = this.firstLine + (char) b;
			}
		} catch (EOFException e) {
			info.setWellFormed(RepInfo.FALSE);
			info.setMessage(new ErrorMessage("Error while reading document '"
					+ docName + "'", getNByte() - 1));
			return;
		}
		if (b == 10 || b == 13) {
			this.firstLine = this.firstLine.substring(0,
					this.firstLine.length() - 1); // cut the last byte
			i--;
		}
		if (i > 255) {
			info.setMessage(new ErrorMessage(badHeader
					+ ", longer than 255 byte", docName, this.offset));
			this.firstLine = this.firstLine.substring(0, 50) + " ....";
		}
		this.firstLine = this.firstLine.trim();
		this.checkFirstLine(docName, info);
		return;

	}

	/***************************************************************************
	 * Verify the first line, read out and store version number (PSVERSION[x]),
	 * possible jobtype (Font | Query | ExitServer | Resource) as part from
	 * profile and set version and profile.
	 * 
	 * @param docName
	 * @param info
	 *            - the RepInfo object
	 * @return true if sigmatch PS
	 **************************************************************************/
	private void checkFirstLine(String docName, RepInfo info) {
		String badHeader = "Invalid first line";
		boolean maindoc = docName.equals(info.getUri()
				.substring(info.getUri().lastIndexOf('\\') + 1).trim());
		this._sigMatch = false;
		String vers = null;
		String prof = null;
		this.jobType = null;
		this.versionNumber = 0;
		int length = this.firstLine.length();
		if (!this.firstLine.startsWith("%!PS-Adobe-")) {
			if (this.firstLine.trim().startsWith("%!PS-AdobeFont-1.0")
					|| this.firstLine.trim().startsWith("%!PS-AdobeFont-1.1")
					|| this.firstLine.startsWith("%!FontType1-1.0")) {
				if (maindoc) {
					this.jobType = "Font";
					vers = this.firstLine.trim();
					this._sigMatch = true;
				}
			} else {
				info.setMessage(new ErrorMessage(badHeader + ": "
						+ this.firstLine.trim(), docName, this.offset));
				info.setWellFormed(RepInfo.FALSE);
				return;
			}
		} else // this.firstLine.startsWith("%!PS-Adobe-")
		{
			this._sigMatch = true;
			if (this.firstLine.startsWith("%!PS-Adobe-1")) {
				this.versionNumber = PSVERSION1;
				if (maindoc) {
					vers = "Level 1";
					prof = "PS 1.0";
				}
			} else if (this.firstLine.startsWith("%!PS-Adobe-2")) {
				if (maindoc) {
					vers = "Level 2";
				}
				if (this.firstLine.startsWith("%!PS-Adobe-2.0")) {
					this.versionNumber = PSVERSION20;
					if (maindoc) {
						prof = "PS 2.0";
					}
				}
				if (this.firstLine.startsWith("%!PS-Adobe-2.1")) {
					this.versionNumber = PSVERSION21;
					if (maindoc) {
						prof = "PS 2.1";
					}
				}
			} else if (this.firstLine.startsWith("%!PS-Adobe-3")) {
				this.versionNumber = PSVERSION3;
				if (maindoc) {
					vers = "Level 3";
					if (this.firstLine.startsWith("%!PS-Adobe-3.0")) {
						prof = "PS 3.0";
					} else {
						prof = "PS 3";
					}
				}
			} else {
				info.setMessage(new ErrorMessage(badHeader + ": "
						+ this.firstLine.trim(), docName, this.offset));
				info.setWellFormed(RepInfo.FALSE);
				return;
			}
		}
		if (length >= 19) {
			if (this.firstLine.substring(14).startsWith(" Query")) {
				if (maindoc) {
					this.jobType = "Query";
				}
			} else if (this.firstLine.substring(14).startsWith(" EPSF")) {
				if (maindoc) {
					prof = this.firstLine.substring(14);
				}
			} else if (this.firstLine.substring(14).startsWith(" ExitServer")) {
				if (maindoc) {
					this.jobType = "ExitServer";
				}
			} else if (this.firstLine.substring(14).startsWith(" Resource-")) {
				if (maindoc) {
					this.jobType = "Resource";
				}
				if (this.resources == null) {
					this.setResources();
				}
				if (length >= 25
						&& this.resources.contains(this.firstLine.substring(24)
								.trim().toLowerCase())) {
					if (maindoc) {
						this.jobType = this.jobType + " "
								+ this.firstLine.substring(24).trim();
					}
				} else {
					info.setMessage(new ErrorMessage(badHeader + ": "
							+ this.firstLine, docName, this.offset));
					info.setWellFormed(RepInfo.FALSE);
				}
			}
		}
		if (this.jobType != null && !this.jobType.equals("")) {
			if (prof != null) {
				prof = prof + ", Jobtype: " + this.jobType;
			} else {
				prof = "Jobtype: " + this.jobType;
			}
		}
		if (maindoc) {
			this.version = vers;
			info.setVersion(this.version);
			this.profile = prof;
			info.setProfile(this.profile);
			if (this._sigMatch) {
				info.setSigMatch(this._name);
			}
		}
	}

	/***************************************************************************
	 * Filter the comments starting with "Begin" or "End" and store in
	 * this.beginEndCommentsPerDoc and this.hashBeginEndCommentsPerDoc.
	 * 
	 * Divide comments into with or without keyword and distribute to the
	 * commentsWithKeywordsPerDoc, commentsWithKeywordsHash,
	 * commentsWithoutKeywordsPerDoc.
	 * 
	 * Check if the comment is listed in this.allCommentsWOKeywords or
	 * this.allCommentsWithKeywords.
	 * 
	 * If the comment value is equal to "atend" call this.checkAtends(key, off,
	 * info) to check if "atend" is allowed here.
	 * 
	 * Store the per "%%Pages:" specified number of Pages.
	 * 
	 * If version 1 up to 2.0, store the required comments.
	 * 
	 * For different versions check the discontinued or deprecated comments.
	 * 
	 * @param commentLine
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void distributeComment(String docname, int versNum,
			ArrayList commentLine, RepInfo info) {
		String comment = commentLine.get(1).toString().trim();

		Long offL = (Long) commentLine.get(0);
		long off = offL.longValue();
		int x = 0;
		String key = "";
		String value = "";
		int commentType = -1;

		if (comment.trim().length() >= 5) {
			if (comment.startsWith("Begin") || comment.startsWith("End")) {
				x = comment.indexOf(':');
				if (x > 0) {
					key = comment.substring(0, x).trim();
					value = comment.substring(x + 1).trim();
				} else {
					key = comment;
				}
				ArrayList<Object> listNew = new ArrayList<Object>(3);
				listNew.add(0, offL);
				listNew.add(1, key);
				listNew.add(2, value);
				this.beginEndCommentsPerDoc.addElement(listNew);
				this.hashBeginEndCommentsPerDoc.put(key, offL);
			}
		}
		// look for colon and put to commentsWithKeywordsPerDoc or
		// commentsWithoutKeywordsPerDoc
		// and check the 'atend's
		x = comment.indexOf(':');
		String message;
		if (x <= 0) // there is no colon
		{
			ArrayList<Object> list2 = new ArrayList<Object>(2);
			key = comment.trim();
			commentType = WITHOUTKEYCOMMENT;
			list2.add(0, offL);
			list2.add(1, key);
			list2.add(2, commentType);

			this.commentsWithoutKeywordsPerDoc.addElement(list2);
			if (!this.allCommentsWOKeywords.contains(key)) {
				if (!this.unknownComments.containsKey(offL)) {
					message = "'"
							+ key
							+ "' is not specified comment without keyword/colon";
					this.unknownComments.put(offL, message);
					// info.setMessage(new InfoMessage(message, docname, off));
					// // info.setWellFormed(RepInfo.FALSE);
				} else {
					message = this.unknownComments.get(offL)
							+ ", without keyword/colon";
					this.unknownComments.remove(offL);
					this.unknownComments.put(offL, message);
				}
			}
		} else // x > 0, there is a colon
		{
			value = comment.substring(x + 1, comment.length()).trim();
			if (value.startsWith("%%+")) {
				value = value.substring(3);
			}
			key = comment.substring(0, x).trim();
			commentType = this.getCommentType(key);
			if (commentType == PsModule.NOTSPECIFIEDCOMMENT) {
				if (!this.unknownComments.containsKey(offL)) {
					message = "'" + key
							+ "' is not specified comment with keyword/colon";
					this.unknownComments.put(offL, message);
				} else {
					message = this.unknownComments.get(offL)
							+ ", with keyword/colon";
					this.unknownComments.remove(offL);
					this.unknownComments.put(offL, message);
				}
				// if ( this.unknownComments.containsKey(off))
				// String message = "'" + key + " is not specified comment" +
				// "with keyword/colon";
				// this.unknownComments.put(off, key + "with keyword/colon");
				// String message = "'" + key
				// + "' is not specified comment with keyword/colon";
				// info.setMessage(new InfoMessage(message, docname, off));
			}
			if (value.equals("(atend)")) {
				this.checkAtends(key, off, info);
			} else {
				ArrayList<Object> list1 = new ArrayList<Object>(3);
				list1.add(0, offL);
				list1.add(1, key);
				list1.add(2, value);
				if (key.equals("Pages")) {
					this.valuePages = Integer.parseInt(value);
				}
				if (key.equals("Page")) {
					this.numberOfPages++;
				}
				String[] splitted = this.splitMultilineComment(value);
				for (int i = 0; i < splitted.length; i++) {
					ArrayList<Object> list2 = new ArrayList<Object>(3);
					list2.add(0, offL);
					list2.add(1, key);
					list2.add(2, splitted[i].toString().trim());
					this.checkSyntaxComments(list2, info);
				}
				list1.add(3, commentType);
				this.commentsWithKeywordsPerDoc.add(list1);
			}
		}
		if (versNum == PSVERSION1) {
			if (key.startsWith("EndProlog") || key.startsWith("Trailer")
					|| key.startsWith("DocumentFonts")
					|| key.startsWith("Page")) {
				this.requiredV1V20Comments.add(key);
			}
		}
		if (versNum == PSVERSION21) {
			// check the discontinued comment
			if (key.equals("ChangeFont")) {
				if (!this.unknownComments.containsKey(offL)) {
					message = "'" + key + "' is Version 2.1-discontinued";
					this.unknownComments.put(offL, message);
				} else {
					message = this.unknownComments.get(offL)
							+ "and Version 2.1-discontinued";
					this.unknownComments.remove(offL);
					this.unknownComments.put(offL, message);
				}
				// info.setMessage(new InfoMessage(
				// "Version 2.1-discontinued comment used: '" + key + "'",
				// docname, off));
				// info.setWellFormed(RepInfo.FALSE);
			}
		}
		if (versNum == PSVERSION3) { // check if deprecated comment
			if (this.allV3DeprecatedComments.contains(key)) {
				if (!this.unknownComments.containsKey(offL)) {
					message = "'" + key + "' is Version 3-deprecated";
					this.unknownComments.put(offL, message);
				} else {
					message = this.unknownComments.get(offL)
							+ "and Version 3-deprecated";
					this.unknownComments.remove(offL);
					this.unknownComments.put(offL, message);
				}
				// info.setMessage(new InfoMessage(
				// "Version 3-deprecated comment used: '" + key + "'",
				// docname, off));
			}
		}
	}

	/***************************************************************************
	 * Check if all for version 1 up to 2.0 required comments are existent. In
	 * DSC version 1.0, there were several comment conventions that were
	 * required to minimally conform to that version of the specification. These
	 * comments were: %%DocumentFonts:, %%EndProlog, %%Page:, %%Trailer. As of
	 * version 2.1, there no longer are any required comments.
	 * 
	 * @param docName
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkRequiredCommentsV1V20(String docName, RepInfo info) {
		if (!(this.requiredV1V20Comments.contains("EndProlog")
				&& this.requiredV1V20Comments.contains("Trailer")
				&& this.requiredV1V20Comments.contains("Page") && this.requiredV1V20Comments
					.contains("DocumentFonts"))) {
			info.setMessage(new ErrorMessage(
					"Version 1.0 to 2.0 required comments are missing.",
					docName));
			info.setWellFormed(RepInfo.FALSE);
		}
	}

	/***************************************************************************
	 * Check if the used comments are known by the specified version.
	 * 
	 * @param docName
	 * @param versNum
	 * @param comments
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkVersionCompatibility(String docName, int versNum,
			Vector comments, RepInfo info) {
		ArrayList list;
		String comment;
		// long _offset;
		for (Iterator iterator = comments.iterator(); iterator.hasNext();) {
			list = (ArrayList) iterator.next();
			comment = list.get(1).toString();
			boolean error = false;
			if (versNum == PSVERSION1 || versNum == PSVERSION20) {
				if (!this.allV1V20Comments.contains(comment)) {
					error = true;
				}
			} else if (versNum == PSVERSION21) {
				if (!this.allV21Comments.contains(comment)) {
					error = true;
				}
			} else if (versNum == PSVERSION3) {
				if (!this.allV3Comments.contains(comment)) {
					error = true;
				}
			}
			if (error) {
				Long off = (Long) list.get(0);
				// long _offset = off.longValue();
				String vn = String.valueOf(versNum);
				if (versNum > 9) {
					vn = vn.substring(0, 1) + "." + vn.substring(1);
				}
				String message;
				if (!this.unknownComments.containsKey(off)) {
					message = "'" + comment + "' is not specified for version "
							+ vn;
					this.unknownComments.put(off, message);
				} else {
					message = this.unknownComments.get(off)
							+ " and for version " + vn;
					this.unknownComments.remove(off);
					this.unknownComments.put(off, message);
				}
				// info.setMessage(new InfoMessage("'" + comment
				// + "' is not specified comment for version " + vn, docName,
				// _offset));
				// info.setWellFormed(RepInfo.FALSE);
			}
		}
	}

	/***************************************************************************
	 * Check if the used (atend) notation is allowed for this comment.
	 * 
	 * @param comment
	 * @param offset
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkAtends(String comment, long _offset, RepInfo info) {
		if (!this.allAtendComments.contains(comment)) {
			info.setMessage(new ErrorMessage("'" + comment + "'"
					+ " has wrong syntax, no 'atend' allowed", _offset));
			info.setWellFormed(RepInfo.FALSE);
		}
	}

	/***************************************************************************
	 * Check document structure concerning: A conforming PostScript language
	 * document description must have a clearly defined prolog and script
	 * separated by the %%EndProlog comment(, after %%EndComments).
	 * 
	 * @param docName
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkDocStructure(String docName, RepInfo info) {
		boolean hasEndComments = false;
		boolean hasEndProlog = false;
		boolean orderOk = true;
		long off1 = 0;
		long off2 = 0;
		if (this.hashBeginEndCommentsPerDoc.containsKey("EndProlog")) {
			hasEndProlog = true;
		}
		if (this.hashBeginEndCommentsPerDoc.containsKey("EndComments")) {
			hasEndComments = true;
		}
		if (!hasEndComments || !hasEndProlog) {
			orderOk = false;
		} else {
			off1 = this.hashBeginEndCommentsPerDoc.get("EndProlog").longValue();
			off2 = this.hashBeginEndCommentsPerDoc.get("EndComments")
					.longValue();
			if (off1 <= off2) {
				orderOk = false;
			}
		}
		if (!orderOk) {
			info.setMessage(new ErrorMessage("'" + docName
					+ "' has wrong document structure"));
			info.setWellFormed(RepInfo.FALSE);
		}
	}

	/***************************************************************************
	 * Check the syntax of several comments For syntax check of the first line
	 * see this.checkFirstLine().
	 * 
	 * @param comment
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkSyntaxComments(ArrayList comment, RepInfo info) {
		// check the syntax of
		// General Header Comments
		String key = comment.get(1).toString().trim();
		// %%BoundingBox: { <llx> <lly> <urx>
		// <ury> } | (atend)
		if (key.equals("BoundingBox")) {
			String regex = "-?[0-9]+ -?[0-9]+ -?[0-9]+ -?[0-9]+";
			checkSyntaxPerRegex(comment, regex, info);
		}
		// %%DocumentData: Clean7Bit | Clean8Bit | Binary
		if (key.equals("DocumentData")) {
			if (this.docdata == null) {
				setDocdata();
			}
			this.checkSyntaxPerConstants(comment, this.docdata, info);
		}
		// %%Emulation: <mode>
		if (key.equals("Emulation")) {
			if (this.emulationModes == null) {
				setEmulationModes();
			}
			checkSyntaxPerConstants(comment, this.emulationModes, info);
		}
		// %%Extensions: <extension> ...
		if (key.equals("Extension")) {
			if (this.extensions == null) {
				setExtensions();
			}
			checkSyntaxPerConstants(comment, this.extensions, info);
		}
		// %%LanguageLevel: <uint>
		if (key.equals("LanguageLevel")) {
			checkSyntaxPerRegex(comment, "[0-9]*", info);
		}
		// { <orientation> ... } | (atend)
		if (key.equals("Orientation")) {
			if (this.orientations == null) {
				setOrientations();
			}
			checkSyntaxPerConstants(comment, this.orientations, info);
		}
		// %%Pages: <numpages> | (atend)
		if (key.equals("Pages")) {
			checkSyntaxPerRegex(comment, "[0-9]*", info);
		}
		// <order> | (atend)
		if (key.equals("PageOrder")) {
			if (this.orders == null) {
				setOrders();
			}
			checkSyntaxPerConstants(comment, this.orders, info);
		}
		// %%Version: <version> <revision>
		if (key.equals("Version")) {
			checkSyntaxPerRegex(comment, "[-+]?[0-9]+(.[0-9]+)? [0-9]+", info);
		}
		/*
		 * %%Copyright: <textline> %%Creator: <textline> %%CreationDate:
		 * <textline> %%EndComments (no keywords) %%For: <textline> %%Routing:
		 * <textline> %%Title: <textline>
		 */
		// ///////////// General Body Comments
		// %%BeginBinary: <bytecount>
		if (key.equals("BeginBinary")) {
			checkSyntaxPerRegex(comment, "[0-9]*", info);
		}
		// %%BeginData: <numberof>[ <type> [ <bytesorlines> ] ]
		if (key.equals("BeginData")) {
			if (this.types == null) {
				setTypes();
			}
			checkSyntaxBeginData(comment, info);
		}
		// %%BeginEmulation: <mode>
		if (key.equals("BeginEmulation")) {
			if (this.emulationModes.isEmpty()) {
				setEmulationModes();
			}
			checkSyntaxPerConstants(comment, this.emulationModes, info);
		}
		// %%BeginPreview: <width> <height> <depth> <lines>
		if (key.equals("BeginPreview")) {
			checkSyntaxPerRegex(comment, "[0-9]+ [0-9]+ [0-9]+ [0-9]+", info);
		}
		/*
		 * %%EndData (no keywords) %%BeginDefaults (no keywords) %%EndDefaults
		 * (no keywords) %%EndBinary (no keywords) %%EndEmulation (no keyword)
		 * %%EndPreview (no keywords) %%BeginProlog (no keywords) %%EndProlog
		 * (no keywords) %%BeginSetup (no keywords) %%EndSetup (no keywords)
		 */
		// /////////////////// General Page Comments
		// %%Page: <label> <ordinal>
		// <label> ::= <text> (Page name) <ordinal> ::= <uint> (Page position)
		if (key.equals("Page")) {
			checkSyntaxPage(comment, info);
		}
		// %%PageBoundingBox: { <llx> <lly> <urx> <ury> } | (atend)
		if (key.equals("PageBoundingBox")) {
			String regex = "-?[0-9]+ -?[0-9]+ -?[0-9]+ -?[0-9]+";
			checkSyntaxPerRegex(comment, regex, info);
		}
		// %%PageOrientation: Portrait | Landscape
		if (key.equals("PageOrientation")) {
			if (this.orientations.isEmpty()) {
				setOrientations();
			}
			checkSyntaxPerConstants(comment, this.orientations, info);
		}
		/*
		 * %%BeginObject: <name> [ <code> ] %%EndObject (no keywords)
		 * %%BeginPageSetup (no keywords) %%EndPageSetup (no keywords)
		 */
		// ///////////// General Trailer Comments
		/*
		 * %%PageTrailer (no keywords) %%Trailer (no keywords) %%EOF (no
		 * keywords)
		 */
		// ///////////// Requirement Header Comments
		/*
		 * %%DocumentMedia: <medianame> <attributes> <medianame> ::= <text> (Tag
		 * name of the media) <attributes> ::= <width> <height> <weight> <color>
		 * <type> <width> ::= <real> (Width in PostScript units) <height> ::=
		 * <real> (Height in PostScript units) <weight> ::= <real> (Weight in
		 * g/m2) <color> ::= <text> (Paper color) <type> ::= <text> (Type of
		 * pre-printed form)
		 */
		if (key.equals("DocumentMedia")) {
			checkSyntaxDocumentMedia(comment, info);
		}
		// %%DocumentNeededResources: <resources> | (atend)
		if (key.equals("DocumentNeededResources")) {
			checkSyntaxResourcesComments(comment, info);
			this.saveDocResources(comment, "needed");
		}
		// %%DocumentSuppliedResources: <resources> | (atend)
		if (key.equals("DocumentSuppliedResources")) {
			checkSyntaxResourcesComments(comment, info);
		}
		// %%ProofMode: <mode> <mode> ::= TrustMe | Substitute | NotifyMe
		if (key.equals("ProofMode")) {
			if (this.modes == null) {
				setModes();
			}
			checkSyntaxPerConstants(comment, this.modes, info);
		}
		// %%Requirements: <requirement> [(<style> ...)] ...
		// <requirement> ::= collate | color | duplex | faceup | fax | fold |
		// jog | manualfeed | numcopies | punch | resolution | rollfed | staple
		// <style> ::= <text>
		if (key.equals("Requirements")) {
			if (this.requirements == null) {
				setRequirements();
			}
			checkSyntaxRequirements(comment, info);
		}
		if (key.equals("VMlocation")) {
			if (this.vmlocations == null) {
				setVmlocations();
			}
			checkSyntaxPerConstants(comment, this.vmlocations, info);
		}

		// %%VMusage: <max> <min>
		// <max> ::= <uint> (Maximum VM used by resource)<min> ::= <uint>
		// (Minimum VM used by resource)
		if (key.equals("VMusage")) {
			checkSyntaxPerRegex(comment, "[0-9]+ [0-9]+", info);
		}
		// %%IncludeResource: <resource>
		// %%BeginResource: <resource> [<max> <min>]
		// %%PageResources: { <resource> ... }

		if (key.equals("IncludeResource") || key.equals("BeginResource")
				|| key.equals("PageResources")) {
			checkSyntaxResourcesComments(comment, info);
			if (key.equals("IncludeResource")) {
				// System.err.println("save "+list.toString());
				this.saveDocResources(comment, "included");
			}
		}
		// %%DocumentNeededFiles: { <filename> ... } | (atend)
		// %%DocumentNeededProcSets: { <procname> ... } | (atend)
		// %%DocumentNeededFonts: { <fontname> ... } | (atend)
		/*
		 * %%DocumentPrinterRequired: <print> <prod> [<vers> [<rev>] ]
		 * %%DocumentSuppliedFiles: { <filename> ... } | (atend)
		 * %%DocumentFonts: { <fontname> ... } | (atend)
		 * %%DocumentSuppliedFonts: { <fontname> ... } | (atend)
		 * %%DocumentProcSets: { <procname> ... } | (atend)
		 * %%DocumentSuppliedProcSets: { <procname> ... } | (atend)
		 * %%OperatorIntervention: [ <password> ] %%OperatorMessage: <textline>
		 * %%Requirements: <requirement> [(<style> ...)] ... %%BeginDocument:
		 * <name> [ <version> [ <type> ] ] %%EndDocument (no keywords)
		 * %%IncludeDocument: <name> [<version> [<revision>] ] %%BeginFeature:
		 * <featuretype> [ <option> ] %%EndFeature (no keywords)
		 * %%IncludeFeature: <featuretype> [ <option> ] %%BeginFile: <filename>
		 * %%EndFile (no keywords) %%IncludeFile: <filename> %%BeginFont:
		 * <fontname> [ <printername> ] %%EndFont (no keywords) %%IncludeFont:
		 * <fontname> %%BeginProcSet: <procname> %%EndProcSet (no keywords)
		 * %%IncludeProcSet: <procname> %%EndResource (no keywords) %%PageFonts:
		 * { <fontname> ... } | (atend) %%PageFiles: { <filename> ... } |
		 * (atend) %%PageMedia: <medianame> %%PageRequirements: <requirement>
		 * [(<style>)] ...
		 */

		// //////////Color Header Comments
		// %%DocumentCustomColors: { <colorname> ... } | (atend)
		if (key.equals("DocumentCustomColors")) {
			setDCColornames(comment.get(2).toString().trim());
			checkSyntaxDocumentCustomColors(
					((Long) comment.get(0)).longValue(), info);
		}
		// %%CMYKCustomColor: <cya> <mag> <yel> <blk> <colorname>
		if (key.equals("CMYKCustomColor")) {
			checkSyntaxCMYKCustomColor(comment, info);
		}
		// %%DocumentProcessColors: { <color> ... } | (atend)
		// <color> ::= Cyan | Magenta | Yellow | Black
		if (key.equals("DocumentProcessColors")) {
			if (this.colors == null) {
				setColors();
			}
			checkSyntaxDocumentProcessColors(comment, info);
		}
		// %%RGBCustomColor: <red> <green> <blue> <colorname>
		// <red> ::= <real> <green> ::= <real> <blue> ::= <real> <colorname> ::=
		// <text> (Custom color name)
		if (key.equals("RGBCustomColor")) {
			checkSyntaxRGBCustomColor(comment, info);
		}

		// ////////////Color Body Comments

		// %%BeginProcessColor: <color>
		// <color> ::= Cyan | Magenta | Yellow | Black
		if (key.equals("BeginProcessColor")) {
			if (this.colors.isEmpty()) {
				setColors();
			}
			checkSyntaxPerConstants(comment, this.colors, info);
		}

		/*
		 * %%BeginCustomColor: <colorname> %%EndCustomColor (no keywords)
		 * %%EndProcessColor (no keywords)
		 */

		// ////////////Color Page Comments
		// --> %%PageProcessColors: { <color> ... } | (atend)
		// ////////////Query Comments
		// %%?BeginResourceListQuery: font | file | procset | pattern | form |
		// encoding
		// %%PageCustomColors: { <colorname> ... } | (atend)
		if (key.equals("PageCustomColors")) {
			setPCColornames(comment.get(2).toString().trim());
			checkSyntaxPageCustomColors(((Long) comment.get(0)).longValue(),
					info);
		}

		// /////////////Query Comments
		// %%?BeginResourceListQuery: <resource>...
		if (key.equals("?BeginResourceListQuery")) {
			if (this.resources == null) {
				setResources();
			}
			checkSyntaxPerConstants(comment, this.resources, info);
		}
		// %%?BeginResourceQuery: <resource>...
		if (key.equals("?BeginResourceQuery")) {
			checkSyntaxResourcesComments(comment, info);
		}
		/*
		 * %!PS-Adobe-3.0 Query (no keywords) %%?BeginFeatureQuery:
		 * <featuretype> [ <option> ] %%?EndFeatureQuery: <default>
		 * %%?BeginFileQuery: <filename> %%?EndFileQuery: <default>
		 * %%?BeginFontListQuery (no keywords) %%?EndFontListQuery: <default>
		 * %%?BeginFontQuery: <fontname> ... %%?EndFontQuery: <default>
		 * %%?BeginPrinterQuery (no keywords) %%?EndPrinterQuery: <default>
		 * %%?BeginProcSetQuery: <procname> %%?EndProcSetQuery: <default>
		 * %%?BeginQuery: <identifier> %%?EndQuery: <default>
		 * %%EndResourceListQuery: <text> %%?EndResourceQuery: <default>
		 * %%?BeginVMStatus (no keywords) %%?EndVMStatus: <default>
		 */
		/*
		 * 9 Open Structuring Conventions 10 Special Structuring Conventions
		 * %%BeginExitServer: <password> %%EndExitServer (no keywords)
		 */

		// //////////// Page Layout Comments
		// %%DocumentSuppliedFeatures
		// %%PageFeatures
		// %%TargetDevice
		// %%TrailerLength
		// %%CropBox: , %%PageCropBox: , %%HiResBoundingBox: ,
		// %%PageHiResBoundingBox:
		// {<llx> <lly> <urx> <ury>} | (atend)
		// <llx> ::= <real>(Lower left x coordinate)
		// <lly> ::= <real>(Lower left y coordinate)
		// <urx> ::= <real>(Upper right x coordinate)
		// <ury> ::= <real>(Upper right y coordinate)
		String regex = "-?[0-9]+ -?[0-9]+ -?[0-9]+ -?[0-9]+";
		if (key.equals("CropBox")) {
			checkSyntaxPerRegex(comment, regex, info);
		}
		if (key.equals("PageCropBox")) {
			checkSyntaxPerRegex(comment, regex, info);
		}
		if (key.equals("HiResBoundingBox")) {
			checkSyntaxPerRegex(comment, regex, info);
		}
		if (key.equals("PageHiResBoundingBox")) {
			checkSyntaxPerRegex(comment, regex, info);
		}
		// %%PlateColor: <colorant> | (atend)
		// <colorant> ::= Cyan | Magenta | Yellow | Black | Other | Custom
		// <name> | <name>
		// <name> ::= <textline>
		// Any color listed by %%PlateColor: must also be listed in the
		// appropriate
		// %%DocumentProcessColors: or %%DocumentCustomColors: comment in
		// the outermost context of the document.
		if (key.equals("PlateColor")) {
			if (this.colors.isEmpty()) {
				setColors();
			}
			String value = comment.get(2).toString().trim();
			if (!this.colors.contains(value)) {
				this.plateColorname = value;
			}
		}
	}

	/***************************************************************************
	 * @param comment
	 * @param regex
	 *            - regular expression
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkSyntaxPerRegex(ArrayList comment, String regex,
			RepInfo info) {
		String key = comment.get(1).toString().trim();
		long off = ((Long) comment.get(0)).longValue();
		String value = comment.get(2).toString().trim();
		if (!value.matches(regex)) {
			info.setMessage(new ErrorMessage("'" + key + "' has wrong syntax",
					off));
			info.setWellFormed(RepInfo.FALSE);
		}
	}

	/***************************************************************************
	 * Check if listed values are allowed items.
	 * 
	 * @param comment
	 * @param list
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkSyntaxPerConstants(ArrayList comment, ArrayList list,
			RepInfo info) {
		String key = comment.get(1).toString().trim();
		long off = ((Long) comment.get(0)).longValue();
		String value = comment.get(2).toString().trim();
		if (!list.contains(value)) {
			info.setMessage(new ErrorMessage("'" + key + "' has wrong syntax",
					off));
			info.setWellFormed(RepInfo.FALSE);
		}
	}

	/***************************************************************************
	 * %%BeginData: <numberof>[ <type> [ <bytesorlines> ] ] <numberof> ::=
	 * <uint> (Lines or physical bytes) <type> ::= Hex | Binary | ASCII (Type of
	 * data) <bytesorlines> ::= Bytes | Lines (Read in bytes or lines)
	 * 
	 * @param comment
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkSyntaxBeginData(ArrayList comment, RepInfo info) {
		long off = ((Long) comment.get(0)).longValue();
		String temp = comment.get(2).toString().trim();
		int x = temp.indexOf(" ");
		String numberof;
		String type = "";
		String bytesorlines = "";
		String error = new String();
		if (x > 0) {
			numberof = temp.substring(0, x - 1);
			type = temp.substring(x).trim();
			int y = type.indexOf(" ");
			if (y > 0) {
				bytesorlines = type.substring(y).trim();
				type = type.substring(0, y).trim();
			}
		} else {
			numberof = temp;
		}
		if (!(numberof.matches("[1-9][0-9]*"))) {
			error = ", numberof is no number";
		}
		if (type.length() > 0) {
			if (!this.types.contains(type)) {
				error = error + ", wrong type";
			}
		}
		if (bytesorlines.length() > 0) {
			if (!(bytesorlines.equals("Bytes") || bytesorlines.equals("Lines"))) {
				error = error + ", wrong bytesorlines";
			}
		}
		if (error.length() > 0) {
			temp = "";
			info.setMessage(new ErrorMessage("'BeginData' has wrong syntax"
					+ error, off));
			info.setWellFormed(RepInfo.FALSE);
		}
	}

	/***************************************************************************
	 * %%Page: <label> <ordinal> <label> ::= <text>, <ordinal> ::= <uint>
	 * 
	 * @param comment
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkSyntaxPage(ArrayList comment, RepInfo info) {
		// %%Page: <label> <ordinal>
		// <label> ::= <text> (Page name)
		// <ordinal> ::= <uint> (Page position)
		String key = "Page";
		long off = ((Long) comment.get(0)).longValue();
		String temp = comment.get(2).toString().trim();
		String[] s = this.splitMultilineComment(temp);
		for (int i = 0; i < s.length; i++) {
			boolean error = false;
			int x = s[i].indexOf(" ");
			String ordinal;
			if (x > 0) {
				ordinal = s[i].substring(x).trim();
				if (!(ordinal.matches("[0-9]+"))) {
					error = true;
				}
			} else {
				error = true;
			}
			if (error) {
				info.setMessage(new ErrorMessage("'" + key
						+ "' (-position) has wrong syntax", off));
				info.setWellFormed(RepInfo.FALSE);
			}
		}
	}

	/***************************************************************************
	 * %%DocumentMedia: <medianame> <attributes> <medianame> ::= <text> (Tag
	 * name of the media) <attributes> ::= <width> <height> <weight> <color>
	 * <type> <width> ::= <real> (Width in PostScript units) <height> ::= <real>
	 * (Height in PostScript units) <weight> ::= <real> (Weight in g/m2) <color>
	 * ::= <text> (Paper color) <type> ::= <text> (Type of pre-printed form)
	 * 
	 * @param comment
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkSyntaxDocumentMedia(ArrayList comment, RepInfo info) {
		long off = ((Long) comment.get(0)).longValue();
		String temp = comment.get(2).toString().trim();
		String key = "DocumentMedia";
		String[] lines = splitMultilineComment(temp); // get the single lines
		for (int j = 0; j < lines.length; j++) {
			String[] s = this.splitPerWhitespace(lines[j]);
			// check the first 3 attributes
			for (int i = 1; i < 4; i++) {
				if (!s[i].matches("[-+]?[0-9]+[.]?[0-9]*")) {
					temp = "";
					info.setMessage(new ErrorMessage("'" + key
							+ "' (attributes) has wrong syntax", off));
					info.setWellFormed(RepInfo.FALSE);
				}
			}
		}
	}

	/***************************************************************************
	 * @param comment
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkSyntaxResourcesComments(ArrayList comment, RepInfo info) {
		if (this.resources == null) {
			setResources();
		}
		long off = ((Long) comment.get(0)).longValue();
		String key = comment.get(1).toString().trim();
		String temp = comment.get(2).toString().trim();
		String[] first = this.splitPerWhitespace(temp.trim());
		ErrorMessage error = new ErrorMessage("'" + key + "' has wrong syntax",
				off);
		if (first == null) {
			info.setMessage(error);
			info.setWellFormed(RepInfo.FALSE);
			return;
		}
		if (!this.resources.contains(first[0].trim())) // first[0] is not a
		// resource
		{
			temp = "";
			info.setMessage(error);
			info.setWellFormed(RepInfo.FALSE);
		}
	}

	/***************************************************************************
	 * <requirement> ::= collate | color[(separation)] | duplex[(tumble)] |
	 * faceup | fax | fold | jog | manualfeed | numcopies[(<uint>)] |
	 * punch[(<uint>)] | resolution[(x, y)] | rollfed |
	 * staple[([position],[orient])] <style> ::= <text>
	 * 
	 * @param comment
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkSyntaxRequirements(ArrayList comment, RepInfo info) {
		long _offset = ((Long) comment.get(0)).longValue();
		String temp = comment.get(2).toString().trim();
		String key = "Requirements";
		boolean error = false;
		// cut the styles
		String[] r = temp.split("[(][^)]*[)]");
		temp = r[0];
		if (r.length > 1) {
			for (int i = 1; i < r.length; i++) {
				temp = temp + r[i];
			}
		}
		String[] reqs = this.splitPerWhitespace(temp);
		for (int i = 0; i < reqs.length; i++) {
			if (!this.requirements.contains(reqs[i].trim())) {
				error = true;
			}
		}
		if (error) {
			temp = "";
			info.setMessage(new ErrorMessage("'" + key + "' has wrong syntax",
					_offset));
			info.setWellFormed(RepInfo.FALSE);
		}
	}

	/***************************************************************************
	 * %%CMYKCustomColor: <cya> <mag> <yel> <blk> <colorname> <cya> :: = <real>
	 * (Cyan percentage), <mag> ::= <real> (Magenta percentage) <yel> ::= <real>
	 * (Yellow percentage), <blk> ::= <real> (Black percentage) <colorname> ::=
	 * <text> (Custom color name)
	 * 
	 * @param comment
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkSyntaxCMYKCustomColor(ArrayList comment, RepInfo info) {
		String key = "CMYKCustomColor";
		long off = ((Long) comment.get(0)).longValue();
		String temp = comment.get(2).toString().trim();
		String[] splitted = this.splitPerWhitespace(temp);
		for (int i = 0; i < splitted.length - 1; i++) // percentages
		{
			if (!splitted[i].matches("0") && !splitted[i].matches("0.[0-9]*")
					&& !splitted[i].matches("1[.0]?")
					&& !splitted[i].matches("1.0")) {
				temp = "";
				info.setMessage(new ErrorMessage("'" + key
						+ "' has wrong syntax: " + splitted[i], off));
				info.setWellFormed(RepInfo.FALSE);
				return;
			}
		}
		if (!this.dcColornames.contains(splitted[splitted.length - 1])) // colorname
		{
			temp = "";
			info.setMessage(new ErrorMessage("'" + key + "' has wrong syntax: "
					+ splitted[splitted.length - 1], off));
			info.setWellFormed(RepInfo.FALSE);
		}
	}

	/***************************************************************************
	 * Check if listed colornames are correct. Normally, the colorname specified
	 * can be any arbitrary string except Cyan, Magenta, Yellow, or Black.
	 * 
	 * @param offset
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkSyntaxDocumentCustomColors(long _offset, RepInfo info) {
		// <colorname> ::= <text> (Custom color name)
		// Normally, the colorname specified can be any arbitrary string
		// except Cyan, Magenta, Yellow, or Black. // element in vector colors
		String comment = "DocumentCustomColors";
		String s = "";
		for (int i = 0; i < this.dcColornames.size(); i++) {
			s = this.dcColornames.get(i).toString();
			if (this.colors.contains(s)) {
				info.setMessage(new ErrorMessage("'" + comment
						+ "' has wrong syntax: " + s, _offset));
				info.setWellFormed(RepInfo.FALSE);
				s = "";
			}
		}
	}

	/***************************************************************************
	 * Check if listed colors are correct. <color> ::= Cyan | Magenta | Yellow |
	 * Black
	 * 
	 * @param list
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkSyntaxDocumentProcessColors(ArrayList list, RepInfo info) {
		// %%DocumentProcessColors: { <color> ... } | (atend)
		// <color> ::= Cyan | Magenta | Yellow | Black
		String comment = "DocumentProcessColors";
		long off = ((Long) list.get(0)).longValue();
		String temp = list.get(2).toString().trim();

		String[] s = this.splitPerWhitespace(temp);
		for (int i = 0; i < s.length; i++) {
			if (!this.colors.contains(s[i])) {
				temp = "";
				info.setMessage(new ErrorMessage("'" + comment
						+ "' has wrong syntax: " + s[i], off));
				info.setWellFormed(RepInfo.FALSE);
			}
		}
	}

	/***************************************************************************
	 * %%RGBCustomColor: <cya> <mag> <yel> <blk> <colorname> <cya> :: = <real>
	 * (Cyan percentage) //<mag> ::= <real> (Magenta percentage) <yel> ::=
	 * <real> (Yellow percentage) //<blk> ::= <real> (Black percentage)
	 * <colorname> ::= <text> (Custom color name)
	 * 
	 * @param comment
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkSyntaxRGBCustomColor(ArrayList comment, RepInfo info) {
		String key = "RGBCustomColor";
		long off = ((Long) comment.get(0)).longValue();
		String temp = comment.get(2).toString().trim();
		String[] s = this.splitPerWhitespace(temp);
		if (s.length < 4) {
			temp = "";
			info.setMessage(new ErrorMessage("'" + key + "' has wrong syntax",
					off));
			info.setWellFormed(RepInfo.FALSE);
		} else {
			for (int i = 0; i < s.length - 1; i++) // percentages
			{
				if (!s[i].matches("0") && !s[i].matches("0.[0-9]*")
						&& !s[i].matches("1[.0]?") && !s[i].matches("1.0")) {
					temp = "";
					info.setMessage(new ErrorMessage("'" + key
							+ "' has wrong syntax: " + s[i], off));
					info.setWellFormed(RepInfo.FALSE);
					return;
				}
			}
			if (!this.dcColornames.contains(s[s.length - 1])) // colorname
			{
				temp = "";
				info.setMessage(new ErrorMessage("'" + key
						+ "' has wrong syntax: " + s[s.length - 1], off));
				info.setWellFormed(RepInfo.FALSE);
			}
		}
	}

	/***************************************************************************
	 * Check if listed colornames are correct. Normally, the colorname specified
	 * can be any arbitrary string except Cyan, Magenta, Yellow, or Black.
	 * 
	 * @param offset
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void checkSyntaxPageCustomColors(long _offset, RepInfo info) {
		// <colorname> ::= <text> (Custom color name)
		String comment = "PageCustomColors";
		String s = "";
		for (int i = 0; i < this.pcColornames.size(); i++) {
			s = this.pcColornames.get(i).toString();
			if (this.colors.contains(s)) {
				info.setMessage(new ErrorMessage("'" + comment
						+ "' has wrong syntax: " + s, _offset));
				info.setWellFormed(RepInfo.FALSE);
				s = "";
			}
		}
	}

	/***************************************************************************
	 * Save the listed resources to the hashtables this.docInclRes or
	 * this.docNeededRes.
	 * 
	 * @param comment
	 * @param inclOrNeeded
	 **************************************************************************/
	private void saveDocResources(ArrayList comment, String inclOrNeeded) {
		String key = "";
		String value = "";
		String all = comment.get(2).toString();
		if (all.length() == 0)
			return;
		String[] s = this.splitPerWhitespace(all);
		java.util.Hashtable<String, String> hash = new java.util.Hashtable<String, String>();
		for (int i = 0; i < s.length; i++) {
			if (this.resources.contains(s[i])) {
				key = s[i];
				if (hash.containsKey(key)) {
					value = hash.get(key).toString();
					hash.remove(key);
				}
				value = "";
			} else {
				value = value + " " + s[i];
			}
			hash.put(key, value.trim());

		}
		if (inclOrNeeded.equals("included")) {
			this.saveToHash(hash, this.docInclRes);
		}
		if (inclOrNeeded.equals("needed")) {
			this.saveToHash(hash, this.docNeededRes);
		}
	}

	/***************************************************************************
	 * Check if the resources listed in %%DocumentNeededResources and in
	 * %%IncludeResource match together.
	 * 
	 * @return
	 **************************************************************************/
	private boolean checkDocResources() {
		String key, value;
		String[] values;
		for (Enumeration e1 = this.docInclRes.keys(); e1.hasMoreElements();) {
			key = e1.nextElement().toString();
			value = this.docInclRes.get(key).toString();
			values = value.split(" ");
			if (!this.docNeededRes.containsKey(key)) {
				return false;
			}

			for (int i = 0; i < values.length; i++) {
				if (!this.docNeededRes.get(key).toString().contains(values[i])) {
					return false;
				}
			}
		}
		for (Enumeration e2 = this.docNeededRes.keys(); e2.hasMoreElements();) {
			key = e2.nextElement().toString();
			value = this.docNeededRes.get(key).toString().trim();
			values = value.split(" ");

			if (!this.docInclRes.containsKey(key)) {
				return false;
			}

			for (int i = 0; i < values.length; i++) {
				if (!this.docInclRes.get(key).toString().contains(values[i])) {
					return false;
				}
			}
		}

		return true;
	}

	/***************************************************************************
	 * Save the content of one hashtabele to another. Concatenate the values
	 * with one space as separator if a key of the source-Hashtable is alredy
	 * one in the destinationHashtable.
	 * 
	 * @param sourceHash
	 * @param destinationHash
	 **************************************************************************/
	private void saveToHash(java.util.Hashtable<String, String> sourceHash,
			java.util.Hashtable<String, String> destinationHash) {
		String key, value;
		for (Enumeration e = sourceHash.keys(); e.hasMoreElements();) {
			key = e.nextElement().toString();
			value = sourceHash.get(key).toString();

			if (destinationHash.containsKey(key)) {
				value = destinationHash.get(key).toString() + " " + value;
				destinationHash.remove(key);
			}
			destinationHash.put(key, value.trim());
		}
	}

	/***************************************************************************
	 * Build the command line to execute Ghostscript, start a process, buffers
	 * its output into ghostscriptOutput and get the exit code. If the process
	 * is not in an appropriate state to get the exit code destroy it. [AFPL
	 * Ghostscript 8.54 (2006-05-17) Copyright (C) 2005 artofcode LLC, Benicia,
	 * CA. All rights reserved. This software comes with NO WARRANTY: see the
	 * file PUBLIC for details.] Return codes from gsapi_*() Code Status 0 No
	 * errors e_Quit "quit" has been executed. This is not an error.
	 * gsapi_exit() must be called next. e_NeedInput More input is needed by
	 * gsapi_run_string_continue(). This is not an error. e_Info "gs -h" has
	 * been executed. This is not an error. gsapi_exit() must be called next. <
	 * 0 Error <= -100 Fatal error. gsapi_exit() must be called next. The only
	 * exception is GSRunStringContinue() which will return e_NeedInput (-106)
	 * if all is well.
	 * 
	 * On other systems the executable may have a different name: System
	 * invocation name Unix gs VMS gs MS Windows 95 and later gswin32c MS
	 * Windows 3.1/Win32s gswin32 OS/2 gsos2
	 * 
	 * @return exit code Ghostscript or PsModule.GHOSTSCRIPT_PROCESSDESTROYED,
	 *         PsModule.GHOSTSCRIPT_UNKNOWNEXCEPTION,
	 *         PsModule.GHOSTSCRIPT_WRONGCOMMAND
	 **************************************************************************/
	private int executeGhostscript() {
		// set the command line
		this.systemOsName = this.getSystemsOsName();
		String[] cmd_array = new String[7];

		if ("Windows NT".equals(this.systemOsName)
				|| "Windows XP".equals(this.systemOsName)) {
			cmd_array[0] = "cmd.exe";
			cmd_array[1] = "/C";
			cmd_array[2] = this.getGhostscriptPath() + "gswin32c";
			cmd_array[3] = "-dNODISPLAY";
			cmd_array[4] = "-dBATCH";
			cmd_array[5] = "-dNOPAUSE";
			cmd_array[6] = this.fileOrUri;
		} else if ("Windows 95".equals(this.systemOsName)) {
			cmd_array[0] = "command.exe";
			cmd_array[1] = "/C";
			cmd_array[2] = this.getGhostscriptPath() + "gswin32c";
			cmd_array[3] = "-dNODISPLAY";
			cmd_array[4] = "-dBATCH";
			cmd_array[5] = "-dNOPAUSE";
			cmd_array[6] = this.fileOrUri;
		}
		// else if (false)// TODO other OS (Linux...)
		// {
		// cmd_array[0] = "sh";
		// cmd_array[1] = this.getGhostscriptPath() + "gs";
		// cmd_array[2] = "-dNODISPLAY";
		// cmd_array[3] = "-dBATCH";
		// cmd_array[4] = "-dNOPAUSE";
		// cmd_array[6] = this.fileOrUri;
		// }
		else {
			cmd_array[0] = null;
		}
		if (cmd_array[0].equals(null)) {
			return PsModule.GHOSTSCRIPT_WRONGCOMMAND;
		}
		int exitGhostscript;

		// start a process, buffer its output
		// and get the exit code
		Runtime rt = Runtime.getRuntime();
		Process proc = null;
		try {
			proc = rt.exec(cmd_array);
		} catch (SecurityException se) {
			// If a security manager exists and its checkExec method doesn't
			// allow creation of the subprocess
			return PsModule.GHOSTSCRIPT_EXECUTION_NOT_ALLOWED;
		} catch (Exception e) {
			return PsModule.GHOSTSCRIPT_WRONGCOMMAND;
			// IOException If an I/O error occurs
			// NullPointerException If cmdarray is null, or one of the
			// elements of cmdarray is null
			// IndexOutOfBoundsException If cmdarray is an empty array (has
			// length 0)
		}
		try {
			InputStreamReader isr = new InputStreamReader(proc.getInputStream());
			BufferedReader br = new BufferedReader(isr);
			String _line = "";
			while ((_line = br.readLine()) != null) {
				this.ghostscriptOutput = this.ghostscriptOutput + " " + _line;
			}
			isr = new InputStreamReader(proc.getErrorStream());
			br = new BufferedReader(isr);
			_line = "";
			while ((_line = br.readLine()) != null) {
				this.ghostscriptErrorOutput = this.ghostscriptErrorOutput + " "
						+ _line;
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		try {
			exitGhostscript = proc.exitValue();
		} catch (IllegalThreadStateException e) {
			System.err.println(e.toString());
			proc.destroy();
			return PsModule.GHOSTSCRIPT_PROCESSDESTROYED;
		}
		if (exitGhostscript < 0) {
			if (exitGhostscript > -100) {
				return PsModule.GHOSTSCRIPT_FATALERROR;
			} else {
				return PsModule.GHOSTSCRIPT_ERROR;
			}
		}
		proc.destroy();
		return exitGhostscript;
	}

	/***************************************************************************
	 * Create the Jhove properties and adds them to the property map for all
	 * comment types where output[commenttype] is set true.
	 * 
	 * @param docName
	 * @param info
	 *            - the RepInfo object
	 **************************************************************************/
	private void setProperties(String docName, RepInfo info) {
		Vector<Property> props = new Vector<Property>();
		for (Iterator iter = this.commentsWithKeywordsPerDoc.iterator(); iter
				.hasNext();) {
			boolean output = false;
			Vector<Property> propVec = new Vector<Property>();
			ArrayList list = (ArrayList) iter.next();
			int commentType = ((Integer) list.get(3)).intValue();
			switch (commentType) {
			case GENERALHEADERCOMMENT:
				if (PsModule.outputGeneralHeaderComments) {
					output = true;
				}
				break;
			case GENERALBODYCOMMENT:
				if (PsModule.outputGeneralHeaderComments) {
					output = true;
				}
				break;
			case GENERALPAGECOMMENT:
				if (PsModule.outputGeneralHeaderComments) {
					output = true;
				}
				break;
			case GENERALTRAILERCOMMENT:
				if (PsModule.outputGeneralTrailerComments) {
					output = true;
				}
				break;
			case REQUIREMENTHEADERCOMMENT:
				if (PsModule.outputRequirementHeaderComments) {
					output = true;
				}
				break;
			case COLORHEADERCOMMENT:
				if (PsModule.outputColorHeaderComments) {
					output = true;
				}
				break;
			case COLORBODYCOMMENT:
				if (PsModule.outputColorBodyComments) {
					output = true;
				}
				break;
			case COLORPAGECOMMENT:
				if (PsModule.outputColorPageComments) {
					output = true;
				}
				break;
			case QUERYCOMMENT:
				if (PsModule.outputQueryComments) {
					output = true;
				}
				break;
			case EXITSERVERCOMMENT:
				if (PsModule.outputExitServerComments) {
					output = true;
				}
				break;
			case PAGELAYOUTCOMMENT:
				if (PsModule.outputPageLayoutComments) {
					output = true;
				}
				break;
			case NOTSPECIFIEDCOMMENT:
				if (PsModule.outputNotSpecifiedComments) {
					output = true;
				}
				break;
			default:
				break;
			}
			if (output) {
				long _offset = ((Long) list.get(0)).longValue();
				String key = list.get(1).toString();
				String value = list.get(2).toString();
				propVec.add(new Property("value", PropertyType.STRING, value));
				propVec.add(new Property("offset", PropertyType.LONG, offset));
				props.add(new Property(key, PropertyType.PROPERTY,
						PropertyArity.ARRAY, vectorToPropArray(propVec)));
			}
		}
		if (PsModule.outputWithoutKeyComments) {
			for (Iterator iter2 = this.commentsWithoutKeywordsPerDoc.iterator(); iter2
					.hasNext();) {
				Vector<Property> propVec2 = new Vector<Property>();
				ArrayList list = (ArrayList) iter2.next();
				long offset2 = ((Long) list.get(0)).longValue();
				String key = list.get(1).toString();
				propVec2.add(new Property("offset", PropertyType.LONG, offset2));
				props.add(new Property(key, PropertyType.PROPERTY,
						PropertyArity.ARRAY, vectorToPropArray(propVec2)));
			}
		}
		if (props.size() > 0) {
			info.setProperty(new Property(docName, PropertyType.PROPERTY,
					PropertyArity.ARRAY, vectorToPropArray(props)));
		}
	}

	/***************************************************************************
	 * Split a string around "white space" characters. "White space" characters
	 * within comments may be either spaces or tabs (decimal ASCII 32 and 9,
	 * respectively).
	 * 
	 * @param temp
	 * @return
	 **************************************************************************/
	private String[] splitPerWhitespace(String temp) {
		String[] s = null;
		if (temp.contains("\t")) {
			temp = temp.replace((char) 9, (char) 32);
		}
		if (temp.contains(" ")) {
			s = temp.split(" ");
		}
		return s;
	}

	/***************************************************************************
	 * Take all commentlines of one document, find out multiline comment and
	 * build to one line with separator " %%".
	 * 
	 * @param commLines
	 * @return
	 **************************************************************************/
	private Vector concatenateMultilineComments(Vector<Object> commLines) {
		String comment = "";
		String commentNew = "";
		Long offsetNew = 0l;
		int size = commLines.size();
		for (int i = size - 1; i >= 0; i--) {
			comment = ((ArrayList) commLines.elementAt(i)).get(1).toString();
			if (comment.startsWith("+")) {
				commentNew = comment;
				commLines.remove(i);
				comment = ((ArrayList) commLines.elementAt(i - 1)).get(1)
						.toString();
				commentNew = comment + " %%" + commentNew;
				offsetNew = (Long) ((ArrayList) commLines.elementAt(i - 1))
						.get(0);
				ArrayList<Object> list = new ArrayList<Object>(2);
				list.add(0, offsetNew);
				list.add(1, commentNew);
				commLines.remove(i - 1);
				commLines.add(i - 1, list);
			}
		}
		return commLines;
	}

	/***************************************************************************
	 * split a multiline comment, where several lines are stored in one string
	 * with separator "%%" by concatenateMultilineComments(commentLines)
	 * 
	 * @param comment
	 * @return
	 **************************************************************************/
	private String[] splitMultilineComment(String comment) {
		String[] s = comment.split("%%");
		for (int i = 1; i < s.length; i++) {
			s[i] = s[i].substring(1).trim();
		}
		return s;
	}

	/***************************************************************************
	 * fill ArrayList allAtendComments<String> with all comments supporting the
	 * (atend) convention
	 * 
	 * @throws NullPointerException
	 **************************************************************************/
	private void fillAllAtendComments() throws NullPointerException {
		this.allAtendComments = new ArrayList<String>(29);
		this.allAtendComments.add("BoundingBox");
		this.allAtendComments.add("DocumentSuppliedProcSets");
		this.allAtendComments.add("DocumentCustomColors");
		this.allAtendComments.add("DocumentSuppliedResources");
		this.allAtendComments.add("DocumentFiles");
		this.allAtendComments.add("Orientation");
		this.allAtendComments.add("DocumentFonts");
		this.allAtendComments.add("Pages");
		this.allAtendComments.add("DocumentNeededFiles");
		this.allAtendComments.add("PageBoundingBox");

		this.allAtendComments.add("DocumentNeededFonts");
		this.allAtendComments.add("PageCustomColors");
		this.allAtendComments.add("DocumentNeededProcSets");
		this.allAtendComments.add("PageFiles");
		this.allAtendComments.add("DocumentNeededResources");
		this.allAtendComments.add("PageFonts");
		this.allAtendComments.add("DocumentProcSets");
		this.allAtendComments.add("PageOrder");
		this.allAtendComments.add("DocumentProcessColors");
		this.allAtendComments.add("PageOrientation");

		this.allAtendComments.add("DocumentSuppliedFiles");
		this.allAtendComments.add("PageProcessColors");
		this.allAtendComments.add("DocumentSuppliedFonts");
		this.allAtendComments.add("PageResources");
		this.allAtendComments.add("CropBox");
		this.allAtendComments.add("PageCropBox");
		this.allAtendComments.add("HiResBoundingBox");
		this.allAtendComments.add("PlateColor");
		this.allAtendComments.add("PageHiResBoundingBox");
	}

	/***************************************************************************
	 * fill ArrayList allV3DeprecatedComments<String> with all version 3 -
	 * deprecated comments
	 * 
	 * @throws NullPointerException
	 **************************************************************************/
	private void fillAllV3DeprecatedComments() throws NullPointerException {
		this.allV3DeprecatedComments = new ArrayList<String>(27);
		this.allV3DeprecatedComments.add("EndBinary");
		this.allV3DeprecatedComments.add("DocumentNeededFiles");
		this.allV3DeprecatedComments.add("DocumentSuppliedFiles");
		this.allV3DeprecatedComments.add("DocumentFonts");
		this.allV3DeprecatedComments.add("DocumentNeededFonts");
		this.allV3DeprecatedComments.add("DocumentSuppliedFonts");
		this.allV3DeprecatedComments.add("DocumentProcSets");
		this.allV3DeprecatedComments.add("DocumentNeededProcSets");
		this.allV3DeprecatedComments.add("DocumentSuppliedProcSets");
		this.allV3DeprecatedComments.add("EndFile");
		this.allV3DeprecatedComments.add("IncludeFile");
		this.allV3DeprecatedComments.add("BeginFont");
		this.allV3DeprecatedComments.add("EndFont");
		this.allV3DeprecatedComments.add("IncludeFont");
		this.allV3DeprecatedComments.add("BeginProcSet");
		this.allV3DeprecatedComments.add("EndProcSet");
		this.allV3DeprecatedComments.add("IncludeProcSet");
		this.allV3DeprecatedComments.add("PageFonts");
		this.allV3DeprecatedComments.add("PageFiles");
		this.allV3DeprecatedComments.add("BeginFileQuery");
		this.allV3DeprecatedComments.add("EndFileQuery");
		this.allV3DeprecatedComments.add("BeginFontListQuery");
		this.allV3DeprecatedComments.add("EndFontListQuery");
		this.allV3DeprecatedComments.add("BeginFontQuery");
		this.allV3DeprecatedComments.add("EndFontQuery");
		this.allV3DeprecatedComments.add("BeginProcSetQuery");
		this.allV3DeprecatedComments.add("EndProcSetQuery");
	}

	/***************************************************************************
	 * fill ArrayList allCommentsWOKeywords<String> with all known comments
	 * without keywords up to version 3
	 **************************************************************************/
	private void fillAllCommentsWOKeywords() {
		this.allCommentsWOKeywords = new ArrayList<String>(31);
		this.allCommentsWOKeywords.add("EndComments");
		this.allCommentsWOKeywords.add("EndBinary");
		this.allCommentsWOKeywords.add("EndData");
		this.allCommentsWOKeywords.add("BeginDefaults");
		this.allCommentsWOKeywords.add("EndDefaults");
		this.allCommentsWOKeywords.add("EndEmulation");
		this.allCommentsWOKeywords.add("EndPreview");
		this.allCommentsWOKeywords.add("BeginProlog");
		this.allCommentsWOKeywords.add("EndProlog");
		this.allCommentsWOKeywords.add("BeginSetup");

		this.allCommentsWOKeywords.add("EndSetup");
		this.allCommentsWOKeywords.add("EndObject");
		this.allCommentsWOKeywords.add("EndDocument");
		this.allCommentsWOKeywords.add("BeginPageSetup");
		this.allCommentsWOKeywords.add("EndPageSetup");
		this.allCommentsWOKeywords.add("PageTrailer");
		this.allCommentsWOKeywords.add("Trailer");
		this.allCommentsWOKeywords.add("EOF");
		this.allCommentsWOKeywords.add("EndFeature");
		this.allCommentsWOKeywords.add("EndFile");

		this.allCommentsWOKeywords.add("EndFont");
		this.allCommentsWOKeywords.add("EndProcSet");
		this.allCommentsWOKeywords.add("EndResource");
		this.allCommentsWOKeywords.add("EndCustomColor");
		this.allCommentsWOKeywords.add("EndProcessColor");
		this.allCommentsWOKeywords.add("?BeginFontListQuery");
		this.allCommentsWOKeywords.add("?BeginPrinterQuery");
		this.allCommentsWOKeywords.add("?BeginVMStatus");
		this.allCommentsWOKeywords.add("EndExitServer");
		this.allCommentsWOKeywords.add("EndPaperSize");

		this.allCommentsWOKeywords.add("EndPageComments");
	}

	/***************************************************************************
	 * fill Hashtable allCommentsWithKeywords<String comment, Integer
	 * commentType> with all known comments with keywords up to version 3
	 **************************************************************************/
	private void fillAllCommentsWithKeywords() {
		this.allCommentsWithKeywords = new java.util.Hashtable<String, Integer>(
				105);

		this.allCommentsWithKeywords.put("BoundingBox",	GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("Copyright", GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("Creator", GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("CreationDate", GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentData", GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("Emulation", GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("Extensions", GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("For", GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("LanguageLevel", GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("Orientation", GENERALHEADERCOMMENT);

		this.allCommentsWithKeywords.put("Pages", GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("PageOrder", GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("Routing", GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("Title", GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("Version", GENERALHEADERCOMMENT);
		this.allCommentsWithKeywords.put("BeginBinary", GENERALBODYCOMMENT);
		this.allCommentsWithKeywords.put("BeginData", GENERALBODYCOMMENT);
		this.allCommentsWithKeywords.put("BeginEmulation", GENERALBODYCOMMENT);
		this.allCommentsWithKeywords.put("BeginPreview", GENERALBODYCOMMENT);
		this.allCommentsWithKeywords.put("BeginObject", GENERALPAGECOMMENT);

		this.allCommentsWithKeywords.put("Page", GENERALPAGECOMMENT);
		this.allCommentsWithKeywords.put("PageBoundingBox", GENERALPAGECOMMENT);
		this.allCommentsWithKeywords.put("PageOrientation", GENERALPAGECOMMENT);
		this.allCommentsWithKeywords.put("DocumentMedia", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentNeededResources", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentSuppliedResources", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentPrinterRequired", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentNeededFiles", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentSuppliedFiles", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentFonts", REQUIREMENTHEADERCOMMENT);

		this.allCommentsWithKeywords.put("DocumentNeededFonts", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentSuppliedFonts", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentProcSets", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentNeededProcSets", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentSuppliedProcSets", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("OperatorIntervention", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("OperatorMessage", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("ProofMode", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("Requirements", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("VMlocation", REQUIREMENTHEADERCOMMENT);

		this.allCommentsWithKeywords.put("VMusage", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("BeginDocument", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("IncludeDocument", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("BeginFeature", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("IncludeFeature", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("BeginFile", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("IncludeFile", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("BeginFont", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("IncludeFont", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("BeginProcSet", REQUIREMENTHEADERCOMMENT);

		this.allCommentsWithKeywords.put("IncludeProcSet", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("BeginResource", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("IncludeResource", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("PageFonts", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("PageFiles", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("PageMedia", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("PageRequirements", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("PageResources", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("ExecuteFile", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("Feature", REQUIREMENTHEADERCOMMENT);

		this.allCommentsWithKeywords.put("CMYKCustomColor", COLORHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentCustomColors", COLORHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentProcessColors", COLORHEADERCOMMENT);
		this.allCommentsWithKeywords.put("RGBCustomColor", COLORHEADERCOMMENT);
		this.allCommentsWithKeywords.put("BeginCustomColor", COLORBODYCOMMENT);
		this.allCommentsWithKeywords.put("BeginProcessColor", COLORBODYCOMMENT);
		this.allCommentsWithKeywords.put("PageCustomColors", COLORPAGECOMMENT);
		this.allCommentsWithKeywords.put("PageProcessColors", COLORPAGECOMMENT);
		this.allCommentsWithKeywords.put("?BeginFeatureQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?EndFeatureQuery", QUERYCOMMENT);

		this.allCommentsWithKeywords.put("?BeginFileQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?EndFileQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?EndFontListQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?BeginFontQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?EndFontQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?EndPrinterQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?BeginProcSetQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?EndProcSetQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?BeginQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?EndQuery", QUERYCOMMENT);

		this.allCommentsWithKeywords.put("?BeginResourceListQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?EndResourceListQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?BeginResourceQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?EndResourceQuery", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("?EndVMStatus", QUERYCOMMENT);
		this.allCommentsWithKeywords.put("BeginExitServer", EXITSERVERCOMMENT);
		this.allCommentsWithKeywords.put("BeginPaperSize", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentPaperColors", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentPaperForms", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("DocumentPaperSizes", REQUIREMENTHEADERCOMMENT);

		this.allCommentsWithKeywords.put("DocumentPaperWeights", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("PaperColor", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("PaperForm", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("PaperSize", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("PaperWeight", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("ChangeFont", REQUIREMENTHEADERCOMMENT);
		this.allCommentsWithKeywords.put("CropBox", PAGELAYOUTCOMMENT);
		this.allCommentsWithKeywords.put("PageCropBox", PAGELAYOUTCOMMENT);
		this.allCommentsWithKeywords.put("HiResBoundingBox", PAGELAYOUTCOMMENT);
		this.allCommentsWithKeywords.put("PageHiResBoundingBox", PAGELAYOUTCOMMENT);

		this.allCommentsWithKeywords.put("PlateColor", PAGELAYOUTCOMMENT);
		this.allCommentsWithKeywords.put("DocumentSuppliedFeatures", PAGELAYOUTCOMMENT);
		this.allCommentsWithKeywords.put("PageFeatures", PAGELAYOUTCOMMENT);
		this.allCommentsWithKeywords.put("TargetDevice", PAGELAYOUTCOMMENT);
		this.allCommentsWithKeywords.put("TrailerLength", PAGELAYOUTCOMMENT);
	}

	/***************************************************************************
	 * fill ArrayList allV1V20Comments<String> with all comments for version 1
	 * up to 2.0
	 **************************************************************************/
	private void fillAllV1V20Comments() {
		this.allV1V20Comments = new ArrayList<String>(12);
		this.allV1V20Comments.add("BoundingBox");
		this.allV1V20Comments.add("ChangeFont");
		this.allV1V20Comments.add("CreationDate");
		this.allV1V20Comments.add("Creator");
		this.allV1V20Comments.add("DocumentFonts");
		this.allV1V20Comments.add("For");
		this.allV1V20Comments.add("Page");
		this.allV1V20Comments.add("Pages");
		this.allV1V20Comments.add("Title");
		this.allV1V20Comments.add("EndComments");
		this.allV1V20Comments.add("EndProlog");
		this.allV1V20Comments.add("Trailer");
	}

	/***************************************************************************
	 * fill ArrayList allV21Comments<String> with all comments for version 2.1
	 **************************************************************************/
	private void fillAllV21Comments() {
		this.allV21Comments = new ArrayList<String>(88);
		this.allV21Comments.add("BoundingBox");
		this.allV21Comments.add("CreationDate");
		this.allV21Comments.add("Creator");
		this.allV21Comments.add("DocumentFonts");
		this.allV21Comments.add("For");
		this.allV21Comments.add("Page");
		this.allV21Comments.add("Pages");
		this.allV21Comments.add("Title");
		this.allV21Comments.add("EndComments");
		this.allV21Comments.add("EndProlog");

		this.allV21Comments.add("Trailer");
		this.allV21Comments.add("?BeginFeatureQuery");
		this.allV21Comments.add("?BeginFileQuery");
		this.allV21Comments.add("?BeginFontListQuery");
		this.allV21Comments.add("?BeginFontQuery");
		this.allV21Comments.add("?BeginPrinterQuery");
		this.allV21Comments.add("?BeginProcSetQuery");
		this.allV21Comments.add("?BeginQuery");
		this.allV21Comments.add("?BeginVMStatus");
		this.allV21Comments.add("?EndFeatureQuery");

		this.allV21Comments.add("?EndFileQuery");
		this.allV21Comments.add("?EndFontListQuery");
		this.allV21Comments.add("?EndFontQuery");
		this.allV21Comments.add("?EndPrinterQuery");
		this.allV21Comments.add("?EndProcSetQuery");
		this.allV21Comments.add("?EndQuery");
		this.allV21Comments.add("?EndVMStatus");
		this.allV21Comments.add("BeginBinary");
		this.allV21Comments.add("BeginCustomColor");
		this.allV21Comments.add("BeginDocument");

		this.allV21Comments.add("BeginExitServer");
		this.allV21Comments.add("BeginFeature");
		this.allV21Comments.add("BeginFile");
		this.allV21Comments.add("BeginFont");
		this.allV21Comments.add("BeginObject");
		this.allV21Comments.add("BeginPageSetup");
		this.allV21Comments.add("BeginPaperSize");
		this.allV21Comments.add("BeginProcessColor");
		this.allV21Comments.add("BeginProcSet");
		this.allV21Comments.add("BeginSetup");

		this.allV21Comments.add("CMYKCustomColor");
		this.allV21Comments.add("DocumentCustomColors");
		this.allV21Comments.add("DocumentNeededFiles");
		this.allV21Comments.add("DocumentNeededFonts");
		this.allV21Comments.add("DocumentNeededProcSets");
		this.allV21Comments.add("DocumentPaperColors");
		this.allV21Comments.add("DocumentPaperForms");
		this.allV21Comments.add("DocumentPaperSizes");
		this.allV21Comments.add("DocumentPaperWeights");
		this.allV21Comments.add("DocumentPrinterRequired");

		this.allV21Comments.add("DocumentProcessColors");
		this.allV21Comments.add("DocumentProcSets");
		this.allV21Comments.add("DocumentSuppliedFiles");
		this.allV21Comments.add("DocumentSuppliedFonts");
		this.allV21Comments.add("DocumentSuppliedProcSets");
		this.allV21Comments.add("EndBinary");
		this.allV21Comments.add("EndCustomColor");
		this.allV21Comments.add("EndDocument");
		this.allV21Comments.add("EndExitServer");
		this.allV21Comments.add("EndFeature");

		this.allV21Comments.add("EndFile");
		this.allV21Comments.add("EndFont");
		this.allV21Comments.add("EndObject");
		this.allV21Comments.add("EndPageSetup");
		this.allV21Comments.add("EndPaperSize");
		this.allV21Comments.add("EndProcessColor");
		this.allV21Comments.add("EndProcSet");
		this.allV21Comments.add("EndSetup");
		this.allV21Comments.add("EOF");
		this.allV21Comments.add("ExecuteFile");

		this.allV21Comments.add("Feature");
		this.allV21Comments.add("IncludeFile");
		this.allV21Comments.add("IncludeFont");
		this.allV21Comments.add("IncludeProcSet");
		this.allV21Comments.add("PageBoundingBox");
		this.allV21Comments.add("PageCustomColors");
		this.allV21Comments.add("PageFiles");
		this.allV21Comments.add("PageFonts");
		this.allV21Comments.add("PageProcessColors");
		this.allV21Comments.add("PageTrailer");

		this.allV21Comments.add("PaperColor");
		this.allV21Comments.add("PaperForm");
		this.allV21Comments.add("PaperSize");
		this.allV21Comments.add("PaperWeight");
		this.allV21Comments.add("ProofMode");
		this.allV21Comments.add("Requirements");
		this.allV21Comments.add("RGBCustomColor");
		this.allV21Comments.add("Routing");
	}

	/***************************************************************************
	 * fill ArrayList allV3Comments<String> with all comments for version 3
	 **************************************************************************/
	private void fillAllV3Comments() {
		this.allV3Comments = new ArrayList<String>(125);
		this.allV3Comments.add("?BeginFeatureQuery");
		this.allV3Comments.add("?BeginFileQuery");
		this.allV3Comments.add("?BeginFontListQuery");
		this.allV3Comments.add("?BeginFontQuery");
		this.allV3Comments.add("?BeginPrinterQuery");
		this.allV3Comments.add("?BeginProcSetQuery");
		this.allV3Comments.add("?BeginQuery");
		this.allV3Comments.add("?BeginResourceListQuery");
		this.allV3Comments.add("?BeginResourceQuery");
		this.allV3Comments.add("?BeginVMStatus");

		this.allV3Comments.add("?EndFeatureQuery");
		this.allV3Comments.add("?EndFileQuery");
		this.allV3Comments.add("?EndFontListQuery");
		this.allV3Comments.add("?EndFontQuery");
		this.allV3Comments.add("?EndPrinterQuery");
		this.allV3Comments.add("?EndProcSetQuery");
		this.allV3Comments.add("?EndQuery");
		this.allV3Comments.add("?EndResourceListQuery");
		this.allV3Comments.add("?EndResourceQuery");
		this.allV3Comments.add("?EndVMStatus");

		this.allV3Comments.add("BeginBinary");
		this.allV3Comments.add("BeginCustomColor");
		this.allV3Comments.add("BeginData");
		this.allV3Comments.add("BeginDefaults");
		this.allV3Comments.add("BeginDocument");
		this.allV3Comments.add("BeginEmulation");
		this.allV3Comments.add("BeginExitServer");
		this.allV3Comments.add("BeginFeature");
		this.allV3Comments.add("BeginFile");
		this.allV3Comments.add("BeginFont");

		this.allV3Comments.add("BeginObject");
		this.allV3Comments.add("BeginPageSetup");
		this.allV3Comments.add("BeginPreview");
		this.allV3Comments.add("BeginProcessColor");
		this.allV3Comments.add("BeginProcSet");
		this.allV3Comments.add("BeginProlog");
		this.allV3Comments.add("BeginResource");
		this.allV3Comments.add("BeginSetup");
		this.allV3Comments.add("BoundingBox");
		this.allV3Comments.add("CMYKCustomColor");

		this.allV3Comments.add("Copyright");
		this.allV3Comments.add("CreationDate");
		this.allV3Comments.add("Creator");
		this.allV3Comments.add("DocumentCustomColors");
		this.allV3Comments.add("DocumentData");
		this.allV3Comments.add("DocumentFonts");
		this.allV3Comments.add("DocumentMedia");
		this.allV3Comments.add("DocumentNeededFiles");
		this.allV3Comments.add("DocumentNeededFonts");
		this.allV3Comments.add("DocumentNeededProcSets");

		this.allV3Comments.add("DocumentNeededResources");
		this.allV3Comments.add("DocumentPrinterRequired");
		this.allV3Comments.add("DocumentProcessColors");
		this.allV3Comments.add("DocumentProcSets");
		this.allV3Comments.add("DocumentSuppliedFiles");
		this.allV3Comments.add("DocumentSuppliedFonts");
		this.allV3Comments.add("DocumentSuppliedProcSets");
		this.allV3Comments.add("DocumentSuppliedResources");
		this.allV3Comments.add("Emulation");
		this.allV3Comments.add("EndBinary");

		this.allV3Comments.add("EndComments");
		this.allV3Comments.add("EndCustomColor");
		this.allV3Comments.add("EndData");
		this.allV3Comments.add("EndDefaults");
		this.allV3Comments.add("EndDocument");
		this.allV3Comments.add("EndEmulation");
		this.allV3Comments.add("EndExitServer");
		this.allV3Comments.add("EndFeature");
		this.allV3Comments.add("EndFile");
		this.allV3Comments.add("EndFont");

		this.allV3Comments.add("EndObject");
		this.allV3Comments.add("EndPageSetup");
		this.allV3Comments.add("EndPreview");
		this.allV3Comments.add("EndProcessColor");
		this.allV3Comments.add("EndProcSet");
		this.allV3Comments.add("EndProlog");
		this.allV3Comments.add("EndResource");
		this.allV3Comments.add("EndSetup");
		this.allV3Comments.add("EOF");
		this.allV3Comments.add("ExecuteFile");

		this.allV3Comments.add("Extensions");
		this.allV3Comments.add("Feature");
		this.allV3Comments.add("For");
		this.allV3Comments.add("IncludeDocument");
		this.allV3Comments.add("IncludeFeature");
		this.allV3Comments.add("IncludeFile");
		this.allV3Comments.add("IncludeFont");
		this.allV3Comments.add("IncludeProcSet");
		this.allV3Comments.add("IncludeResource");
		this.allV3Comments.add("LanguageLevel");

		this.allV3Comments.add("OperatorIntervention");
		this.allV3Comments.add("OperatorMessage");
		this.allV3Comments.add("Orientation");
		this.allV3Comments.add("Page");
		this.allV3Comments.add("PageBoundingBox");
		this.allV3Comments.add("PageCustomColors");
		this.allV3Comments.add("PageFiles");
		this.allV3Comments.add("PageFonts");
		this.allV3Comments.add("PageMedia");
		this.allV3Comments.add("PageOrder");

		this.allV3Comments.add("PageOrientation");
		this.allV3Comments.add("PageProcessColors");
		this.allV3Comments.add("PageRequirements");
		this.allV3Comments.add("PageResources");
		this.allV3Comments.add("Pages");
		this.allV3Comments.add("PageTrailer");
		this.allV3Comments.add("ProofMode");
		this.allV3Comments.add("Requirements");
		this.allV3Comments.add("RGBCustomColor");
		this.allV3Comments.add("Routing");

		this.allV3Comments.add("Title");
		this.allV3Comments.add("Trailer");
		this.allV3Comments.add("Version");
		this.allV3Comments.add("VMlocation");
		this.allV3Comments.add("VMusage");
		this.allV3Comments.add("CropBox");
		this.allV3Comments.add("DocumentSuppliedFeatures");
		this.allV3Comments.add("EndPageComments");
		this.allV3Comments.add("HiResBoundingBox");
		this.allV3Comments.add("PageCropBox");

		this.allV3Comments.add("PlateColor");
		this.allV3Comments.add("PageHiResBoundingBox");
		this.allV3Comments.add("PageFeatures");
		this.allV3Comments.add("TargetDevice");
		this.allV3Comments.add("TrailerLength");
	}

	/***************************************************************************
	 * fill ArrayList emulationModes
	 **************************************************************************/
	public void setEmulationModes() {
		this.emulationModes = new ArrayList<String>(7);
		this.emulationModes.add("diablo630");
		this.emulationModes.add("fx100");
		this.emulationModes.add("lj2000");
		this.emulationModes.add("hpgl");
		this.emulationModes.add("hplj");
		this.emulationModes.add("impress");
		this.emulationModes.add("ti855");
	}

	/***************************************************************************
	 * fill ArrayList extensions
	 **************************************************************************/
	public void setExtensions() {
		this.extensions = new ArrayList<String>(4);
		this.extensions.add("DPS");
		this.extensions.add("CMYK");
		this.extensions.add("Composite");
		this.extensions.add("FileSystem");
	}

	/***************************************************************************
	 * fill ArrayList docdata
	 **************************************************************************/
	public void setDocdata() {
		this.docdata = new ArrayList<String>(3);
		this.docdata.add("Clean7Bit");
		this.docdata.add("Clean8Bit");
		this.docdata.add("Binary");
	}

	/***************************************************************************
	 * fill ArrayList orientations
	 **************************************************************************/
	public void setOrientations() {
		this.orientations = new ArrayList<String>(2);
		this.orientations.add("Portrait");
		this.orientations.add("Landscape");
	}

	/***************************************************************************
	 * fill ArrayList orders
	 **************************************************************************/
	public void setOrders() {
		this.orders = new ArrayList<String>(3);
		this.orders.add("Ascend");
		this.orders.add("Descend");
		this.orders.add("Special");
	}

	/***************************************************************************
	 * fill ArrayList types
	 **************************************************************************/
	public void setTypes() {
		this.types = new ArrayList<String>(3);
		this.types.add("Hex");
		this.types.add("Binary");
		this.types.add("ASCII");
	}

	/***************************************************************************
	 * fill ArrayList modes
	 **************************************************************************/
	public void setModes() {
		this.modes = new ArrayList<String>(3);
		this.modes.add("TrustMe");
		this.modes.add("Substitute");
		this.modes.add("NotifyMe");
	}

	/***************************************************************************
	 * fill ArrayList colors
	 **************************************************************************/
	public void setColors() {
		this.colors = new ArrayList<String>(4);
		this.colors.add("Cyan");
		this.colors.add("Magenta");
		this.colors.add("Yellow");
		this.colors.add("Black");
	}

	/***************************************************************************
	 * fill ArrayList resources
	 **************************************************************************/
	public void setResources() {
		this.resources = new ArrayList<String>(6);
		this.resources.add("font");
		this.resources.add("pattern");
		this.resources.add("file");
		this.resources.add("procset");
		this.resources.add("form");
		this.resources.add("encoding");
	}

	/***************************************************************************
	 * fill ArrayList requirements
	 **************************************************************************/
	public void setRequirements() {
		this.requirements = new ArrayList<String>(13);
		this.requirements.add("collate");
		this.requirements.add("color");
		this.requirements.add("duplex");
		this.requirements.add("faceup");
		this.requirements.add("fax");
		this.requirements.add("fold");
		this.requirements.add("jog");
		this.requirements.add("manualfeed");
		this.requirements.add("numcopies");
		this.requirements.add("punch");
		this.requirements.add("resolution");
		this.requirements.add("rollfed");
		this.requirements.add("staple");
	}

	/***************************************************************************
	 * fill ArrayList vmlocations with "global" and "local"
	 **************************************************************************/
	public void setVmlocations() {
		this.vmlocations = new ArrayList<String>(2);
		this.vmlocations.add("global");
		this.vmlocations.add("local");
	}

	/***************************************************************************
	 * fill ArrayList dcolornames
	 * 
	 * @param str
	 **************************************************************************/
	public void setDCColornames(String str) {
		this.dcColornames = new ArrayList<String>();
		String[] ss = this.splitPerWhitespace(str);
		for (int i = 0; i < ss.length; i++) {
			this.dcColornames.add(ss[i].trim());
		}
	}

	/***************************************************************************
	 * fill ArrayList pcColornames
	 * 
	 * @param str
	 **************************************************************************/
	public void setPCColornames(String str) {
		this.pcColornames = new ArrayList<String>();
		String[] ss = this.splitPerWhitespace(str);
		for (int i = 0; i < ss.length; i++) {
			this.pcColornames.add(ss[i].trim());
		}
	}

	/***************************************************************************
	 * @param key
	 * @return commenttype
	 * @throws NullPointerException
	 **************************************************************************/
	private int getCommentType(String key) throws NullPointerException {
		int type = PsModule.NOTSPECIFIEDCOMMENT;
		if (this.allCommentsWithKeywords.containsKey(key)) {
			type = ((Integer) this.allCommentsWithKeywords.get(key)).intValue();
		}
		return type;
	}

	/***************************************************************************
	 * @return Operating System name
	 **************************************************************************/
	public String getSystemsOsName() {
		this.systemOsName = System.getProperty("os.name");
		return this.systemOsName;
	}

	/***************************************************************************
	 * @return ArrayList orders
	 **************************************************************************/
	public ArrayList getOrders() {
		return this.orders;
	}

	/***************************************************************************
	 * @return path to gostscript exe
	 **************************************************************************/
	public String getGhostscriptPath() {
		return this.ghostscriptPath;
	}

	/***************************************************************************
	 * Set the path to ghostscript exe
	 * 
	 * @param ghostscriptPath
	 **************************************************************************/
	public void setGhostscriptPath(String pathGS) {
		this.ghostscriptPath = pathGS;
	}

	/***************************************************************************
	 * @return
	 **************************************************************************/
	public static boolean isOutputColorBodyComments() {
		return PsModule.outputColorBodyComments;
	}

	/***************************************************************************
	 * @param outputColorBodyComments
	 **************************************************************************/
	public static void setOutputColorBodyComments(
			boolean outputColorBodyComments) {
		PsModule.outputColorBodyComments = outputColorBodyComments;
	}

	/***************************************************************************
	 * @return
	 **************************************************************************/
	public static boolean isOutputColorHeaderComments() {
		return PsModule.outputColorHeaderComments;
	}

	/***************************************************************************
	 * @param outputColorHeaderComments
	 **************************************************************************/
	public static void setOutputColorHeaderComments(
			boolean outputColorHeaderComments) {
		PsModule.outputColorHeaderComments = outputColorHeaderComments;
	}

	/***************************************************************************
	 * @return
	 **************************************************************************/
	public static boolean isOutputColorPageComments() {
		return PsModule.outputColorPageComments;
	}

	/***************************************************************************
	 * @param outputColorPageComments
	 **************************************************************************/
	public static void setOutputColorPageComments(
			boolean outputColorPageComments) {
		PsModule.outputColorPageComments = outputColorPageComments;
	}

	/***************************************************************************
	 * @return
	 **************************************************************************/
	public static boolean isOutputGeneralBodyComments() {
		return PsModule.outputGeneralBodyComments;
	}

	/***************************************************************************
	 * @param outputGeneralBodyComments
	 **************************************************************************/
	public static void setOutputGeneralBodyComments(
			boolean outputGeneralBodyComments) {
		PsModule.outputGeneralBodyComments = outputGeneralBodyComments;
	}

	/***************************************************************************
	 * @return
	 **************************************************************************/
	public static boolean isOutputGeneralHeaderComments() {
		return PsModule.outputGeneralHeaderComments;
	}

	/***************************************************************************
	 * @param outputGeneralHeaderComments
	 **************************************************************************/
	public static void setOutputGeneralHeaderComments(
			boolean outputGeneralHeaderComments) {
		PsModule.outputGeneralHeaderComments = outputGeneralHeaderComments;
	}

	/***************************************************************************
	 * @return
	 **************************************************************************/
	public static boolean isOutputGeneralPageComments() {
		return PsModule.outputGeneralPageComments;
	}

	/***************************************************************************
	 * @param outputGeneralPageComments
	 **************************************************************************/
	public static void setOutputGeneralPageComments(
			boolean outputGeneralPageComments) {
		PsModule.outputGeneralPageComments = outputGeneralPageComments;
	}

	/***************************************************************************
	 * @return
	 **************************************************************************/
	public static boolean isOutputGeneralTrailerComments() {
		return PsModule.outputGeneralTrailerComments;
	}

	/***************************************************************************
	 * @param outputGeneralTrailerComments
	 **************************************************************************/
	public static void setOutputGeneralTrailerComments(
			boolean outputGeneralTrailerComments) {
		PsModule.outputGeneralTrailerComments = outputGeneralTrailerComments;
	}

	/***************************************************************************
	 * @return
	 **************************************************************************/
	public static boolean isOutputQueryComments() {
		return PsModule.outputQueryComments;
	}

	/***************************************************************************
	 * @param outputQueryComments
	 **************************************************************************/
	public static void setOutputQueryComments(boolean outputQueryComments) {
		PsModule.outputQueryComments = outputQueryComments;
	}

	/***************************************************************************
	 * @return
	 **************************************************************************/
	public static boolean isOutputRequirementHeaderComments() {
		return PsModule.outputRequirementHeaderComments;
	}

	/***************************************************************************
	 * @param outputRequirementHeaderComments
	 **************************************************************************/
	public static void setOutputRequirementHeaderComments(
			boolean outputRequirementHeaderComments) {
		PsModule.outputRequirementHeaderComments = outputRequirementHeaderComments;
	}

	/***************************************************************************
	 * @return
	 **************************************************************************/
	public static boolean isOutputWithoutKeyComments() {
		return PsModule.outputWithoutKeyComments;
	}

	/***************************************************************************
	 * @param outputWithoutKeyComments
	 **************************************************************************/
	public static void setOutputWithoutKeyComments(
			boolean outputWithoutKeyComments) {
		PsModule.outputWithoutKeyComments = outputWithoutKeyComments;
	}

	/***************************************************************************
	 * @return
	 **************************************************************************/
	public static boolean isOutputExitServerComments() {
		return PsModule.outputExitServerComments;
	}

	/***************************************************************************
	 * @param outputExitServerComments
	 **************************************************************************/
	public static void setOutputExitServerComments(
			boolean outputExitServerComments) {
		PsModule.outputExitServerComments = outputExitServerComments;
	}

	/***************************************************************************
	 * @return
	 **************************************************************************/
	public static boolean isOutputNotSpecifiedComments() {
		return PsModule.outputNotSpecifiedComments;
	}

	/***************************************************************************
	 * @param outputNotSpecifiedComments
	 **************************************************************************/
	public static void setOutputNotSpecifiedComments(
			boolean outputNotSpecifiedComments) {
		PsModule.outputNotSpecifiedComments = outputNotSpecifiedComments;
	}

	/***************************************************************************
	 * @return
	 **************************************************************************/
	public static boolean isOutputPageLayoutComments() {
		return PsModule.outputPageLayoutComments;
	}

	/***************************************************************************
	 * @param outputPageLayoutComments
	 **************************************************************************/
	public static void setOutputPageLayoutComments(
			boolean outputPageLayoutComments) {
		PsModule.outputPageLayoutComments = outputPageLayoutComments;
	}

	/***************************************************************************
	 * Set output of Jhove properties for the different commenttypes
	 * 
	 * @param generalHeaderComments
	 * @param generalBodyComments
	 * @param generalPageComments
	 * @param generalTrailerComments
	 * @param requirementHeaderComments
	 * @param colorHeaderComments
	 * @param colorBodyComments
	 * @param colorPageComments
	 * @param queryComments
	 * @param exitServerComments
	 * @param withoutKeyComments
	 * @param notSpecifiedComments
	 * @param pageLayoutComments
	 **************************************************************************/
	public static void setOutputComments(boolean generalHeaderC,
			boolean generalBodyC, boolean generalPageC,
			boolean generalTrailerC, boolean requirementHeaderC,
			boolean colorHeaderC, boolean colorBodyC, boolean colorPageC,
			boolean queryC, boolean exitServerC, boolean withoutKeyC,
			boolean notSpecifiedC, boolean pageLayoutC) {
		PsModule.setOutputGeneralHeaderComments(generalHeaderC);
		PsModule.setOutputGeneralBodyComments(generalBodyC);
		PsModule.setOutputGeneralPageComments(generalPageC);
		PsModule.setOutputGeneralTrailerComments(generalTrailerC);
		PsModule.setOutputRequirementHeaderComments(requirementHeaderC);
		PsModule.setOutputColorHeaderComments(colorHeaderC);
		PsModule.setOutputColorBodyComments(colorBodyC);
		PsModule.setOutputColorPageComments(colorPageC);
		PsModule.setOutputQueryComments(queryC);
		PsModule.setOutputExitServerComments(exitServerC);
		PsModule.setOutputWithoutKeyComments(withoutKeyC);
		PsModule.setOutputNotSpecifiedComments(notSpecifiedC);
		PsModule.setOutputPageLayoutComments(pageLayoutC);
	}

}
