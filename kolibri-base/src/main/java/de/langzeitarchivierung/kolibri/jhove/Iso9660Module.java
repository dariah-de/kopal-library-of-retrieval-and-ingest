/*******************************************************************************
 * de.langzeitarchivierung.kolibri.jhove / Iso9660Module.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.jhove;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.langzeitarchivierung.kolibri.jhove.iso9660.Iso9660Directory;
import de.langzeitarchivierung.kolibri.jhove.iso9660.Iso9660File;
import edu.harvard.hul.ois.jhove.Agent;
import edu.harvard.hul.ois.jhove.AgentType;
import edu.harvard.hul.ois.jhove.Checksummer;
import edu.harvard.hul.ois.jhove.Document;
import edu.harvard.hul.ois.jhove.DocumentType;
import edu.harvard.hul.ois.jhove.ErrorMessage;
import edu.harvard.hul.ois.jhove.ExternalSignature;
import edu.harvard.hul.ois.jhove.Identifier;
import edu.harvard.hul.ois.jhove.IdentifierType;
import edu.harvard.hul.ois.jhove.InternalSignature;
import edu.harvard.hul.ois.jhove.Property;
import edu.harvard.hul.ois.jhove.PropertyArity;
import edu.harvard.hul.ois.jhove.PropertyType;
import edu.harvard.hul.ois.jhove.RepInfo;
import edu.harvard.hul.ois.jhove.Signature;
import edu.harvard.hul.ois.jhove.SignatureType;
import edu.harvard.hul.ois.jhove.SignatureUseType;

/*******************************************************************************
 * JHOVE Module to analyse CD-ROM Images created using the ISO9660 standard
 * 
 * @author Matthias Neubauer, German National Library
 * @version 20051007
 * @see edu.harvard.hul.ois.jhove.Module
 * @since 20051007
 ******************************************************************************/

public class Iso9660Module extends edu.harvard.hul.ois.jhove.ModuleBase
{

	// CONSTANTS **********************************************************

	private static final String		NAME		= "ISO9660-dnb";
	private static final String		RELEASE		= "1.0";
	private static final int[]		DATE		= { 2006, 7, 4 };
	private static final String[]	FORMAT		= { "ISO9660",
			"CD-ROM Image according to ISO-specification 9660" };
	private static final String		COVERAGE	= "Standard ISO9660 "
														+ "Images. Support of Rock Ridge"
														+ " name extentions";

	private static final String[]	MIMETYPE	= { "application/x-iso9660-image" };
	private static final String		WELLFORMED	= "An ISO9660 is wellformed, if it"
														+ " has the Standard Identifier \"CD001\" at byteposition 8002h";
	private static final String		VALIDITY	= null;
	private static final String		REPINFO		= null;
	private static final String		NOTE		= null;
	private static final String		RIGHTS		= "Copyright 2005-2007 by Project kopal "
														+ "http://kopal.langzeitarchivierung.de/";
	private static final boolean	RANDOM		= true;

	// STATE (Instance variables) ****************************************

	protected NumberFormat			gmtFormater	= NumberFormat
														.getIntegerInstance();
	private RandomAccessFile		_raf;
	private ArrayList				dirlist;

	// CREATION (Constructors, factory methods, static/inst init) *******

	/***************************************************************************
	 * Default-constructor
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051011
	 * @since 20051011
	 **************************************************************************/
	public Iso9660Module() {
		super(NAME, RELEASE, DATE, FORMAT, COVERAGE, MIMETYPE, WELLFORMED,
				VALIDITY, REPINFO, NOTE, RIGHTS, RANDOM);

		// Define vendor agent (DDB)
		Agent agent = new Agent("German National Library",
				AgentType.EDUCATIONAL);
		agent.setAddress("Adickesallee 1, 60322 Frankfurt, Germany");
		agent.setTelephone("+49 (69) 1525-0");
		agent.setEmail("kolibri@kopal.langzeitarchivierung.de");
		this._vendor = agent;

		// Define ISO 9660 document with ISO agent
		Document doc = new Document("ISO 9660", DocumentType.REPORT);
		Agent isoAgent = new Agent("International Organization for "
				+ "Standardization", AgentType.NONPROFIT);
		agent = isoAgent; // save making multiple copies of Iso
		agent.setAddress(" 1, rue de Varembé, Case postale 56, "
				+ "CH-1211 Geneva 20, Switzerland");
		agent.setTelephone("+41 22 749 01 11");
		agent.setFax("+41 22 733 34 30");
		agent.setWeb("http://www.iso.org");
		doc.setPublisher(agent);
		doc.setDate("2005-01-06");
		doc.setEdition("ISO 9660:1988	Stage 90.93");
		doc.setIdentifier(new Identifier("ISO Standard 9660:1988 Stage"
				+ " 90.93", IdentifierType.ISO));
		this._specification.add(doc);

		// Add signatures

		int[] sigbyteI = { 0x43, 0x44, 0x30, 0x30, 0x31 };
		Signature sig = new InternalSignature(sigbyteI, SignatureType.MAGIC,
				SignatureUseType.MANDATORY_IF_APPLICABLE, 0x8001,
				"Must read \"CD001\"!");
		this._signature.add(sig);

		sig = new ExternalSignature(".iso", SignatureType.EXTENSION,
				SignatureUseType.OPTIONAL);
		this._signature.add(sig);
	}

	// MANIPULATION (Manipulation - what the object does) ****************

	/***************************************************************************
	 * Parses an ISO9660-Image
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20060620
	 * @since 20051011
	 * @param raf - RandomAccessFile representing the file to be parsed
	 * @param info - RepInfo-object to hold the created metadata
	 * @exception IOException
	 **************************************************************************/
	public void parse(RandomAccessFile raf, RepInfo info) throws IOException {

		long pathTableSize = 0;
		long lPath = 0;
		long optlPath = 0;
		long mPath = 0;
		long optmPath = 0;

		long rootExtentPosition = 0;


		long logicalBlocksize = 0;

		this.gmtFormater.setGroupingUsed(false);
		this.gmtFormater.setMinimumIntegerDigits(2);

		long startbyte = 0x8000; // aka Byte Position 1

		super.initParse();

		this._raf = raf;

		try {
			this._raf.seek(startbyte + 1);
			byte ch0 = this._raf.readByte();
			byte ch1 = this._raf.readByte();
			byte ch2 = this._raf.readByte();
			byte ch3 = this._raf.readByte();
			byte ch4 = this._raf.readByte();
			if (ch2 != ch3
					|| !(ch0 == 0x43 && ch1 == 0x44 && ch2 == 0x30
							&& ch3 == 0x30 && ch4 == 0x31)) {
				info.setWellFormed(false);
				info.setMessage(new ErrorMessage(
						"No Magic Number found for Iso9660!"));
				return;
			}
		}
		catch (Exception e) {
			info.setWellFormed(false);
			info.setMessage(new ErrorMessage(
					"No Magic Number found for Iso9660!"));
			return;
		}

		/* If we got this far, take note that the signature is OK. */
		info.setSigMatch(this._name);

		// We identified the File as Iso-Image, so we set the format,
		// mimetype and module
		info.setFormat(Iso9660Module.FORMAT[0]);
		info.setMimeType(Iso9660Module.MIMETYPE[0]);
		info.setModule(this);

		// Set wellformedness to "undetermined" as it is not checked
		info.setWellFormed(RepInfo.TRUE);

		/* Calculate checksums, if necessary. */
		if (this._je != null && this._je.getChecksumFlag()) {
			if (info.getChecksum().size() == 0) {
				Checksummer ckSummer = new Checksummer();
				calcRAChecksum(ckSummer, this._raf);
				setChecksums(ckSummer, info);
			}
		}

		// Set Size
		info.setSize(this._raf.length());

		List propList = new LinkedList();
		Property isoProp = new Property("ISO9660Metadata",
				PropertyType.PROPERTY, PropertyArity.LIST, propList);

		// Read System Identifier
		this._raf.seek(startbyte + 8);

		byte[] buff = new byte[32];
		this._raf.read(buff);
		Object temp = new String(buff).trim();

		propList.add(new Property("System Identifier", PropertyType.STRING,
				temp));

		// Read Volume Identifier
		buff = new byte[32];
		this._raf.read(buff);
		temp = new String(buff).trim();

		propList.add(new Property("Volume Identifier", PropertyType.STRING,
				temp));

		// Read Volume Space Size
		this._raf.seek(startbyte + 80);
		buff = new byte[8];
		this._raf.read(buff);
		long i = 0;
		i += (buff[4] & 0xFF) << 24;
		i += (buff[5] & 0xFF) << 16;
		i += (buff[6] & 0xFF) << 8;
		i += (buff[7] & 0xFF) << 0;

		temp = (Long) i;

		propList
				.add(new Property("Volume Space Size", PropertyType.LONG, temp));

		// Read Volume Set Size
		this._raf.seek(startbyte + 120);
		buff = new byte[4];
		this._raf.read(buff);
		i = 0;
		i += (buff[2] & 0xFF) << 8;
		i += (buff[3] & 0xFF) << 0;

		temp = (Long) i;

		propList.add(new Property("Volume Set Size", PropertyType.LONG, temp));

		// Read Volume Sequence Number
		buff = new byte[4];
		this._raf.read(buff);
		i = 0;
		i += (buff[2] & 0xFF) << 8;
		i += (buff[3] & 0xFF) << 0;

		temp = (Long) i;

		propList.add(new Property("Volume Sequence Number", PropertyType.LONG,
				temp));

		// Read Logical Block Size
		buff = new byte[4];
		this._raf.read(buff);
		logicalBlocksize += (buff[2] & 0xFF) << 8;
		logicalBlocksize += (buff[3] & 0xFF) << 0;

		temp = (Long) logicalBlocksize;

		propList
				.add(new Property("Logical Block Size", PropertyType.LONG, temp));

		// Read Path Table Size
		buff = new byte[8];
		this._raf.read(buff);
		pathTableSize = 0;
		pathTableSize += (buff[4] & 0xFF) << 24;
		pathTableSize += (buff[5] & 0xFF) << 16;
		pathTableSize += (buff[6] & 0xFF) << 8;
		pathTableSize += (buff[7] & 0xFF) << 0;

		temp = (Long) pathTableSize;

		propList.add(new Property("Path Table Size", PropertyType.LONG, temp));

		// Read Location of Type L Path Table
		buff = new byte[4];
		this._raf.read(buff);
		lPath += (buff[0] & 0xFF) << 0;
		lPath += (buff[1] & 0xFF) << 8;
		lPath += (buff[2] & 0xFF) << 16;
		lPath += (buff[3] & 0xFF) << 24;

		temp = "Logical Block " + lPath;

		propList.add(new Property("Location of Type L Path Table",
				PropertyType.STRING, temp));

		// Read Location of Optional Type L Path Table
		buff = new byte[4];
		this._raf.read(buff);
		optlPath += (buff[0] & 0xFF) << 0;
		optlPath += (buff[1] & 0xFF) << 8;
		optlPath += (buff[2] & 0xFF) << 16;
		optlPath += (buff[3] & 0xFF) << 24;

		temp = "Logical Block " + optlPath;

		propList.add(new Property("Location of Optional Type L Path Table",
				PropertyType.STRING, temp));

		// Read Location of Type M Path Table
		buff = new byte[4];
		this._raf.read(buff);
		mPath += (buff[0] & 0xFF) << 24;
		mPath += (buff[1] & 0xFF) << 16;
		mPath += (buff[2] & 0xFF) << 8;
		mPath += (buff[3] & 0xFF) << 0;

		temp = "Logical Block " + mPath;

		propList.add(new Property("Location of Type M Path Table",
				PropertyType.STRING, temp));

		// Read Location of Optional Type M Path Table
		buff = new byte[4];
		this._raf.read(buff);

		optmPath += (buff[0] & 0xFF) << 24;
		optmPath += (buff[1] & 0xFF) << 16;
		optmPath += (buff[2] & 0xFF) << 8;
		optmPath += (buff[3] & 0xFF) << 0;

		temp = "Logical Block " + optmPath;

		propList.add(new Property("Location of Optional Type M Path Table",
				PropertyType.STRING, temp));

		// -Read Directory record for Root Directory------------------------
		Property[] rdrMetadata = new Property[9];

		// Read Length of Directory Record
		buff = new byte[1];
		this._raf.read(buff);
		temp = Integer.valueOf(buff[0]);
		rdrMetadata[0] = new Property("Length of Directory Record",
				PropertyType.INTEGER, temp);

		// Read Extended Attribute Record Length
		buff = new byte[1];
		this._raf.read(buff);
		temp = Integer.valueOf(buff[0]);
		rdrMetadata[1] = new Property("Extended Attribute Record Length",
				PropertyType.INTEGER, temp);

		// Read Location of Extent
		buff = new byte[8];
		this._raf.read(buff);

		rootExtentPosition += (buff[4] & 0xFF) << 24;
		rootExtentPosition += (buff[5] & 0xFF) << 16;
		rootExtentPosition += (buff[6] & 0xFF) << 8;
		rootExtentPosition += (buff[7] & 0xFF) << 0;

		temp = "Logical Block " + rootExtentPosition;
		rdrMetadata[2] = new Property("Location of Extent",
				PropertyType.STRING, temp);

		// Read Data Length
		buff = new byte[8];
		this._raf.read(buff);

		i = 0;
		i += (buff[4] & 0xFF) << 24;
		i += (buff[5] & 0xFF) << 16;
		i += (buff[6] & 0xFF) << 8;
		i += (buff[7] & 0xFF) << 0;

		temp = (Long) i;
		rdrMetadata[3] = new Property("Data Length", PropertyType.LONG, temp);

		// Read Recording Date and Time

		rdrMetadata[4] = new Property("Volume Creation Date and Time",
				PropertyType.STRING, readNumberDateAndTime());

		// Read File Flags
		buff = new byte[1];
		this._raf.read(buff);
		// Not needed yet!

		// Read File Unit Size
		buff = new byte[1];
		this._raf.read(buff);
		temp = Integer.valueOf(buff[0]);
		rdrMetadata[5] = new Property("File Unit Size", PropertyType.INTEGER,
				temp);

		// Read Interleave Gap Size
		buff = new byte[1];
		this._raf.read(buff);
		temp = Integer.valueOf(buff[0]);
		rdrMetadata[6] = new Property("Interleave Gap Size",
				PropertyType.INTEGER, temp);

		// Read Volume Sequence Number
		buff = new byte[4];
		this._raf.read(buff);
		i = 0;
		i += (buff[2] & 0xFF) << 8;
		i += (buff[3] & 0xFF) << 0;

		temp = (Long) i;
		rdrMetadata[7] = new Property("Volume Sequence Number",
				PropertyType.LONG, temp);

		// Read Length of File Identifier
		buff = new byte[1];
		this._raf.read(buff);
		temp = Integer.valueOf(buff[0]);
		int len_fi = buff[0];
		rdrMetadata[8] = new Property("Length of File Identifier",
				PropertyType.INTEGER, temp);

		// Read File Identifier
		buff = new byte[len_fi];
		this._raf.read(buff);
		// Not needed for root!

		if (rdrMetadata.length > 0) { // If property list is not empty
			propList.add(new Property("Directory record for Root Directory",
					PropertyType.PROPERTY, PropertyArity.ARRAY, rdrMetadata));
		}
		// -----------------------------------------------------------------

		// Read Volume Set Identifier
		buff = new byte[128];
		this._raf.read(buff);
		temp = new String(buff).trim();

		propList.add(new Property("Volume Set Identifier", PropertyType.STRING,
				temp));

		// Read Publisher Identifier
		buff = new byte[128];
		this._raf.read(buff);
		temp = new String(buff).trim();

		propList.add(new Property("Publisher Identifier", PropertyType.STRING,
				temp));

		// Data Preparer Identifier
		buff = new byte[128];
		this._raf.read(buff);
		temp = new String(buff).trim();

		propList.add(new Property("Data Preparer Identifier",
				PropertyType.STRING, temp));

		// Application Identifier
		buff = new byte[128];
		this._raf.read(buff);
		temp = new String(buff).trim();

		propList.add(new Property("Application Identifier",
				PropertyType.STRING, temp));

		// Copyright File Identifier
		buff = new byte[37];
		this._raf.read(buff);
		temp = new String(buff).trim();

		propList.add(new Property("Copyright File Identifier",
				PropertyType.STRING, temp));

		// Abstract File Identifier
		buff = new byte[37];
		this._raf.read(buff);
		temp = new String(buff).trim();

		propList.add(new Property("Abstract File Identifier",
				PropertyType.STRING, temp));

		// Bibliographic File Identifier
		buff = new byte[37];
		this._raf.read(buff);
		temp = new String(buff).trim();

		propList.add(new Property("Bibliographic File Identifier",
				PropertyType.STRING, temp));

		// Volume Creation Date and Time

		propList.add(new Property("Volume Creation Date and Time",
				PropertyType.STRING, readCharacterDateAndTime()));

		// Volume Modification Date and Time

		propList.add(new Property("Volume Modification Date and Time",
				PropertyType.STRING, readCharacterDateAndTime()));

		// Volume Expiration Date and Time

		propList.add(new Property("Volume Expiration Date and Time",
				PropertyType.STRING, readCharacterDateAndTime()));

		// Volume Effective Date and Time

		propList.add(new Property("Volume Effective Date and Time",
				PropertyType.STRING, readCharacterDateAndTime()));

		// -Read the extent of the root and list its files and directories--

		Iso9660Directory root = new Iso9660Directory();
		root.setDirNumber(1);

		// Read path table and create skeletons for directories

		this.dirlist = new ArrayList();

		if (mPath > 0) {
			processMPathTable(mPath * logicalBlocksize, logicalBlocksize,
					pathTableSize);
		}
		else if (lPath > 0) {
			processLPathTable(lPath * logicalBlocksize, logicalBlocksize,
					pathTableSize);
		}
		else if (optmPath > 0) {
			processMPathTable(optmPath * logicalBlocksize, logicalBlocksize,
					pathTableSize);
		}
		else if (optlPath > 0) {
			processLPathTable(optlPath * logicalBlocksize, logicalBlocksize,
					pathTableSize);
		}

		// Gather informations

		// Save a list with extent positions to identify new directory entries
		long[] extentlist = new long[this.dirlist.size()];
		for (int m = 0; m < this.dirlist.size(); m++) {
			extentlist[m] = ((Iso9660Directory) this.dirlist.get(m))
					.getExtentPosition();
		}

		// process the extent of all directories
		for (int j = 0; j < this.dirlist.size(); j++) {
			processExtent((Iso9660Directory) this.dirlist.get(j),
					((Iso9660Directory) this.dirlist.get(j))
							.getExtentPosition(), logicalBlocksize, extentlist);
		}

		// process the extent of the root directory
		if (rootExtentPosition > 0) {
			processExtent(root, rootExtentPosition * logicalBlocksize,
					logicalBlocksize, extentlist);
		}

		// order the list of directories and thus create a tree structure
		for (int x = 0; x < this.dirlist.size(); x++) {

			int tempdirnumber = ((Iso9660Directory) this.dirlist.get(x))
					.getParentDirNumber();

			if (tempdirnumber != 1) {
				for (int y = 0; y < this.dirlist.size(); y++) {
					if (((Iso9660Directory) this.dirlist.get(y)).getDirNumber() == tempdirnumber) {
						((Iso9660Directory) this.dirlist.get(y))
								.addToFileList((Iso9660Directory) this.dirlist
										.get(x));
					}
				}
			}
		}

		// add root-subdirectories to the root dir
		for (int x = 0; x < this.dirlist.size(); x++) {

			int tempdirnumber = ((Iso9660Directory) this.dirlist.get(x))
					.getParentDirNumber();

			if (tempdirnumber == 1) {
				root.addToFileList((Iso9660Directory) this.dirlist.get(x));
			}
		}

		// Create properties for the file structure

		ArrayList fileMetadata = new ArrayList();

		fileMetadata = processFileData(root.getFileList());

		propList.add(new Property("File Structure", PropertyType.PROPERTY,
				PropertyArity.LIST, fileMetadata));

		// -----------------------------------------------------------------

		info.setProperty(isoProp);
	}

	// INTERNAL (Internal - implementation details, local classes, ...) **

	/***************************************************************************
	 * Reads the Type M Path Table for a Directory
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20060620
	 * @since 20060620
	 * @param location - The location of the file decriptors of the content
	 * @param logicalBlocksize - The size of a logical block in this image
	 * @param pathTableSize
	 * @exception IOException
	 **************************************************************************/
	private void processMPathTable(long location, long logicalBlocksize,
			long pathTableSize) throws IOException {

		// Skip root-dir entry
		this._raf.seek(location + 10);

		byte[] buff = new byte[0];
		int len_di = 0;
		long extloc = 0;
		int parentdirnr = 0;
		String dI = "";
		int dirCounter = 1;

		while (this._raf.getFilePointer() < (location + pathTableSize)) {
			Iso9660Directory tempdir = new Iso9660Directory();

			dirCounter++;

			tempdir.setDirNumber(dirCounter);

			// Read length of Directory Identifier
			buff = new byte[1];
			this._raf.read(buff);
			len_di = unsignedByteToInt(buff[0]);

			// Read Extended Attribute Record Length
			buff = new byte[1];
			this._raf.read(buff);

			// Read Location of Extent
			buff = new byte[4];
			this._raf.read(buff);
			extloc = 0;
			extloc += (buff[0] & 0xFF) << 24;
			extloc += (buff[1] & 0xFF) << 16;
			extloc += (buff[2] & 0xFF) << 8;
			extloc += (buff[3] & 0xFF) << 0;

			extloc = extloc * logicalBlocksize;

			tempdir.setExtentPosition(extloc);

			// Read Parent Directory Number
			buff = new byte[2];
			this._raf.read(buff);
			parentdirnr = 0;
			parentdirnr += (buff[0] & 0xFF) << 8;
			parentdirnr += (buff[1] & 0xFF) << 0;
			tempdir.setParentDirNumber(parentdirnr);

			// Read Directory Identifier
			buff = new byte[len_di];
			this._raf.read(buff);
			dI = new String(buff);
			tempdir.setFileIdentifier(dI);

			// Skip Padding Field if length of directory identifier
			// is an odd number
			if (len_di % 2 != 0) {
				this._raf.read();
			}

			this.dirlist.add(tempdir);
		}
	}

	/***************************************************************************
	 * Reads the Type L Path Table for a Directory
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20060620
	 * @since 20060620
	 * @param location - The location of the file decriptors of the content
	 * @param logicalBlocksize - The size of a logical block in this image
	 * @param pathTableSize
	 * @exception IOException
	 **************************************************************************/
	private void processLPathTable(long location, long logicalBlocksize,
			long pathTableSize) throws IOException {

		// Skip root-dir entry
		this._raf.seek(location + 10);

		byte[] buff = new byte[0];
		int len_di = 0;
		long extloc = 0;
		int parentdirnr = 0;
		String dI = "";
		int dirCounter = 1;

		while (this._raf.getFilePointer() < (location + pathTableSize)) {
			Iso9660Directory tempdir = new Iso9660Directory();

			dirCounter++;

			tempdir.setDirNumber(dirCounter);

			// Read length of Directory Identifier
			buff = new byte[1];
			this._raf.read(buff);
			len_di = unsignedByteToInt(buff[0]);

			// Read Extended Attribute Record Length
			buff = new byte[1];
			this._raf.read(buff);

			// Read Location of Extent
			buff = new byte[4];
			this._raf.read(buff);
			extloc = 0;
			extloc += (buff[0] & 0xFF) << 0;
			extloc += (buff[1] & 0xFF) << 8;
			extloc += (buff[2] & 0xFF) << 16;
			extloc += (buff[3] & 0xFF) << 24;

			extloc = extloc * logicalBlocksize;

			tempdir.setExtentPosition(extloc);

			// Read Parent Directory Number
			buff = new byte[2];
			this._raf.read(buff);
			parentdirnr = 0;
			parentdirnr += (buff[0] & 0xFF) << 0;
			parentdirnr += (buff[1] & 0xFF) << 8;
			tempdir.setParentDirNumber(parentdirnr);

			// Read Directory Identifier
			buff = new byte[len_di];
			this._raf.read(buff);
			dI = new String(buff);
			tempdir.setFileIdentifier(dI);

			// Skip Padding Field if length of directory identifier
			// is an odd number
			if (len_di % 1 == 0) {
				this._raf.read();
			}

			this.dirlist.add(tempdir);
		}
	}

	/***************************************************************************
	 * Reads the Directory Record for a Directory
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051123
	 * @since 20060620
	 * @param dir - The directory whose content is to be processed
	 * @param location - The location of the file decriptors of the content
	 * @param logicalBlocksize - The size of a logical block in this image
	 * @param extentPositionList - List with startingpoints of directories
	 *            records
	 * @exception IOException
	 **************************************************************************/
	private void processExtent(Iso9660Directory dir, long location,
			long logicalBlocksize, long[] extentPositionList)
			throws IOException {

		boolean isDir = false;

		this._raf.seek(location);

		byte[] buff = new byte[1];
		this._raf.read(buff);
		int len_dr = unsignedByteToInt(buff[0]);

		// Skip root-dir entries
		for (int f = 0; f < 2; f++) {
			this._raf.seek(this._raf.getFilePointer() + len_dr - 1);

			buff = new byte[1];
			this._raf.read(buff);
			len_dr = unsignedByteToInt(buff[0]);
		}

		// long currentpos = 0;
		while (len_dr > 0) {
			long currpos = this._raf.getFilePointer() - 1;

			// Skip Extended Attribute Record Length
			this._raf.read();

			// Read Location of Extent
			long dirExtent = 0;

			buff = new byte[8];
			this._raf.read(buff);

			dirExtent += (buff[4] & 0xFF) << 24;
			dirExtent += (buff[5] & 0xFF) << 16;
			dirExtent += (buff[6] & 0xFF) << 8;
			dirExtent += (buff[7] & 0xFF) << 0;

			dirExtent = logicalBlocksize * dirExtent;

			// Read Data Length
			long dL = 0;

			buff = new byte[8];
			this._raf.read(buff);

			dL += (buff[4] & 0xFF) << 24;
			dL += (buff[5] & 0xFF) << 16;
			dL += (buff[6] & 0xFF) << 8;
			dL += (buff[7] & 0xFF) << 0;

			// Read Recording Date and Time
			String rDT = readNumberDateAndTime();

			// Check File Flags
			buff = new byte[1];
			this._raf.read(buff);
			if ((buff[0] & 2) == 2) {
				isDir = true;
			}

			// Read File Unit Size
			long fUS = 0;

			buff = new byte[1];
			this._raf.read(buff);
			fUS = buff[0];

			// Read Interleave Gap Size
			long iGS = 0;

			buff = new byte[1];
			this._raf.read(buff);
			iGS = buff[0];

			// Read Volume Sequence Number
			long vSN = 0;

			buff = new byte[4];
			this._raf.read(buff);
			vSN += (buff[2] & 0xFF) << 8;
			vSN += (buff[3] & 0xFF) << 0;

			// Read Length of File Identifier
			buff = new byte[1];
			this._raf.read(buff);
			int len_fi = buff[0];

			// Read File Identifier
			String fI = "";

			buff = new byte[len_fi];
			this._raf.read(buff);

			fI = new String(buff).toLowerCase();

			// Skip Padding Field if length of file identifier
			// is an even number
			if (len_fi % 2 == 0) {
				this._raf.read();
			}

			// remember current position
			long pos = this._raf.getFilePointer();

			// Read System Use Signature Word Identifier
			buff = new byte[2];
			this._raf.read(buff);

			// if the system use field contains rockridge extensions
			if (buff[0] == 'R' && buff[1] == 'R') {
				int length = 0;

				// Read the length of the system use field
				buff = new byte[1];
				this._raf.read(buff);
				length = buff[0];

				// jump to the next field
				this._raf.seek(pos + length);

				for (int j = 0; j < 9; j++) {
					// remember position
					pos = this._raf.getFilePointer();
					buff = new byte[2];
					this._raf.read(buff);
					if (buff[0] == 'N' && buff[1] == 'M') {
						buff = new byte[1];
						this._raf.read(buff);
						int len_nm = buff[0] - 5;
						// Skip system use field
						this._raf.read();
						// Skip file flags field
						this._raf.read();
						// Read the extended file identifier
						buff = new byte[len_nm];
						this._raf.read(buff);
						fI = new String(buff);
						break;
					}
					// Else search next system use field
					buff = new byte[1];
					this._raf.read(buff);
					length = buff[0];

					// jump to the next field
					this._raf.seek(pos + length);
				}

				// Go to the end of the directory record
				this._raf.seek(currpos + len_dr);

			}
			else {
				// Go to the end of the directory record
				this._raf.seek(currpos + len_dr);
			}

			if (isDir) { // If processed entry is a directory
				isDir = false;

				// look if this directory is in the list and update the name
				for (int x = 0; x < this.dirlist.size(); x++) {

					long tempextent = ((Iso9660Directory) this.dirlist.get(x))
							.getExtentPosition();

					if (tempextent == dirExtent) {
						((Iso9660Directory) this.dirlist.get(x))
								.setFileIdentifier(fI);
					}
				}

			}
			else { // If processed entry is a file
				// Add gathered Informations to the File List
				dir.addToFileList(new Iso9660File(dL, rDT, fUS, iGS, vSN, fI));
			}

			// Read next len_dr to check if more files or directories follow
			buff = new byte[1];
			this._raf.read(buff);
			len_dr = unsignedByteToInt(buff[0]);

			// Check if the directory record maybe spreads into the next sector
			try {
				if (len_dr == 0) {
					buff = new byte[1];
					long next = this._raf.getFilePointer() + 2048
							- (this._raf.getFilePointer() % 2048);

					this._raf.seek(next);
					this._raf.read(buff);
					len_dr = unsignedByteToInt(buff[0]);

					if (len_dr > 0) {
						long max = extentPositionList[0];
						for (int h = 1; h < extentPositionList.length; h++) {
							if (extentPositionList[h] > max) {
								max = extentPositionList[h];
							}
						}

						for (int l = 0; l < extentPositionList.length; l++) {
							if (extentPositionList[l] == next
									|| next >= max + 2048) {
								len_dr = 0;
							}
						}
					}
				}
			}
			catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}

	/***************************************************************************
	 * Reads the Directory Record for a Directory
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051212
	 * @since 20051212
	 * @param datalist - ArrayList with the gathered File-Informations
	 * @return ArrayList - The Property List for the ISO9660 Image
	 **************************************************************************/
	private ArrayList processFileData(ArrayList datalist) {

		ArrayList filelist = new ArrayList();

		for (int a = 0; a < datalist.size(); a++) {

			ArrayList templist = new ArrayList();

			// Store Informations in Property List
			Iso9660File tempfile = (Iso9660File) datalist.get(a);

			Object tempobj = new Object();

			String filename = tempfile.getFileIdentifier();

			if (datalist.get(a) instanceof Iso9660Directory) {

				tempobj = "Directory";
				templist
						.add(new Property("Type", PropertyType.STRING, tempobj));

				// tempobj = tempfile.getRecordingDateAndTime();
				// templist.add(
				// new Property("Recorded", PropertyType.STRING, tempobj));

				// tempobj = new Long(tempfile.getFileUnitSize());
				// templist.add(
				// new Property("File Unit Size", PropertyType.LONG, tempobj));

				// tempobj = new Long(tempfile.getInterleaveGapSize());
				// templist.add(
				// new Property("Interleave Gap Size",
				// PropertyType.LONG, tempobj));

				// tempobj = new Long(tempfile.getDataLength());
				// templist.add(
				// new Property("Volume Sequence Number",
				// PropertyType.LONG, tempobj));

				Iso9660Directory tempdir = (Iso9660Directory) tempfile;

				// Process directory recusively
				ArrayList directorylist = new ArrayList();

				directorylist = processFileData(tempdir.getFileList());

				if (!directorylist.isEmpty()) {
					templist.add(new Property("Content", PropertyType.PROPERTY,
							PropertyArity.LIST, directorylist));
				}
			}
			else {

				tempobj = "File";
				templist
						.add(new Property("Type", PropertyType.STRING, tempobj));

				tempobj = tempfile.getDataLength();
				templist.add(new Property("Size", PropertyType.LONG, tempobj));

				tempobj = tempfile.getRecordingDateAndTime();
				templist.add(new Property("Recorded", PropertyType.STRING,
						tempobj));

				// tempobj = new Long(tempfile.getFileUnitSize());
				// templist.add(
				// new Property("File Unit Size", PropertyType.LONG, tempobj));

				// tempobj = new Long(tempfile.getInterleaveGapSize());
				// templist.add(
				// new Property("Interleave Gap Size",
				// PropertyType.LONG, tempobj));

				// tempobj = new Long(tempfile.getDataLength());
				// templist.add(
				// new Property("Volume Sequence Number",
				// PropertyType.LONG, tempobj));

				filename = filename.split(";")[0];

				if (filename.endsWith(".")) {
					filename = filename.replace('.', ' ');
				}
			}

			filelist.add(new Property(filename.trim(), PropertyType.PROPERTY,
					PropertyArity.LIST, templist));

		}

		return filelist;
	}

	/***************************************************************************
	 * Reads the Directory Record for a Directory
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051212
	 * @since 20051212
	 * @param b - Signed byte that shall be converted
	 * @return int - The created unsigned int
	 **************************************************************************/
	private int unsignedByteToInt(byte b) {
		return b & 0xFF;
	}

	/***************************************************************************
	 * Reads a Date and Time according to the following format:
	 * 
	 * RBP Interpretation Content
	 * ----------------------------------------------------------------- 1 to 4
	 * Year from 1 to 9999 Digits 5 to 6 Month of the year from 1 to 12 Digits 7
	 * to 8 Day of the Month from 1 to 31 Digits 9 to 10 Hour of the Day from 0
	 * to 23 Digits 11 to 12 Minute of the hour from 0 to 59 Digits 13 to 14
	 * Second of the minute from 0 to 59 Digits 15 to 16 Hundredths of a second
	 * Digits 17 Offset from Greenwich Mean Time in numerical value number of 15
	 * min intervals from -48 (West) to +52 (East) recorded as 8-bit signed
	 * numerical value
	 * 
	 * Reading starts from the current position of the modules RandomAccessFile
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20060324
	 * @since 20060324
	 * @return String - The date and time in form of a string
	 * @exception IOException
	 **************************************************************************/
	private String readCharacterDateAndTime() throws IOException {
		String time = new String("");

		// Read the year
		byte[] buffer = new byte[4];
		this._raf.read(buffer);
		time += new String(buffer);

		time += '-';

		// Read the month
		buffer = new byte[2];
		this._raf.read(buffer);
		time += new String(buffer);

		time += '-';

		// Read the day
		buffer = new byte[2];
		this._raf.read(buffer);
		time += new String(buffer);

		time += 'T';

		// Read the hour
		buffer = new byte[2];
		this._raf.read(buffer);
		time += new String(buffer);

		time += ':';

		// Read the minutes
		buffer = new byte[2];
		this._raf.read(buffer);
		time += new String(buffer);

		time += ':';

		// Read the seconds
		buffer = new byte[2];
		this._raf.read(buffer);
		time += new String(buffer);

		time += ':';

		// Read the Hundreds of a second
		buffer = new byte[2];
		this._raf.read(buffer);
		time += new String(buffer);

		// Read the offset from GMT
		buffer = new byte[1];
		this._raf.read(buffer);

		int gmtinteger = buffer[0];

		if (gmtinteger >= 0) {
			time += '+';
		}

		// Calculate GMT hours
		time += this.gmtFormater.format(gmtinteger / 4);

		time += ':';

		// Calculate GMT minutes
		int gmtmin = (gmtinteger % 4) * 15;
		if (gmtmin < 0) {
			gmtmin *= -1;
		}
		time += this.gmtFormater.format(gmtmin);

		time += "GMT";

		return time;
	}

	/***************************************************************************
	 * Reads a Date and Time according to the following format:
	 * 
	 * RBP Interpretation Content
	 * ----------------------------------------------------------------- 1
	 * Number of years since 1900 numerical value 2 Month of the year from 1 to
	 * 12 numerical value 3 Day of the Month from 1 to 31 numerical value 4 Hour
	 * of the Day from 0 to 23 numerical value 5 Minute of the hour from 0 to 59
	 * numerical value 6 Second of the minute from 0 to 59 numerical value 7
	 * Offset from Greenwich Mean Time in numerical value number of 15 min
	 * intervals from -48 (West) to +52 (East) recorded as 8-bit signed
	 * numerical value
	 * 
	 * Reading starts from the current position of the modules RandomAccessFile
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20060328
	 * @since 20060328
	 * @return String - The date and time in form of a string
	 * @exception IOException
	 **************************************************************************/
	private String readNumberDateAndTime() throws IOException {
		String time = new String("");
		int tempint = 0;

		// Read the year
		byte[] buffer = new byte[1];
		this._raf.read(buffer);
		tempint = buffer[0];
		time += 1900 + tempint;

		time += '-';

		// Read the month
		buffer = new byte[1];
		this._raf.read(buffer);
		tempint = buffer[0];
		time += this.gmtFormater.format(tempint);

		time += '-';

		// Read the day
		buffer = new byte[1];
		this._raf.read(buffer);
		tempint = buffer[0];
		time += this.gmtFormater.format(tempint);

		time += 'T';

		// Read the hour
		buffer = new byte[1];
		this._raf.read(buffer);
		tempint = buffer[0];
		time += this.gmtFormater.format(tempint);

		time += ':';

		// Read the minutes
		buffer = new byte[1];
		this._raf.read(buffer);
		tempint = buffer[0];
		time += this.gmtFormater.format(tempint);

		time += ':';

		// Read the seconds
		buffer = new byte[1];
		this._raf.read(buffer);
		tempint = buffer[0];
		time += this.gmtFormater.format(tempint);

		// Read the offset from GMT
		buffer = new byte[1];
		this._raf.read(buffer);

		int gmtint = buffer[0];

		if (gmtint >= 0) {
			time += '+';
		}

		// Calculate GMT hours
		time += this.gmtFormater.format(gmtint / 4);

		time += ':';

		// Calculate GMT minutes
		int gmtminutes = (gmtint % 4) * 15;
		if (gmtminutes < 0) {
			gmtminutes *= -1;
		}
		time += this.gmtFormater.format(gmtminutes);

		time += "GMT";

		return time;
	}

}
