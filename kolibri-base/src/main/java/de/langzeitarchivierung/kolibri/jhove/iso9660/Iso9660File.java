/*******************************************************************************
 * de.langzeitarchivierung.kolibri.jhove.iso9660 / Iso9660File.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.jhove.iso9660;

/*******************************************************************************
 * Representation of an Iso9660 File Descriptor
 * 
 * @author Matthias Neubauer, German National Library
 * @version 20051209
 * @see de.langzeitarchivierung.kolibri.jhove.Iso9660Module
 * @since 20051123
 ******************************************************************************/

public class Iso9660File
{

	// CLASS VARIABLES (Static variables) **********************************

	protected long		dataLength				= 0;
	protected String	recordingDateAndTime	= "";
	protected long		fileUnitSize			= 0;
	protected long		interleaveGapSize		= 0;
	protected long		volumeSequenceNumber	= 0;
	protected String	fileIdentifier			= "";

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * Default constructor
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051209
	 * @since 20051209
	 * 
	 **************************************************************************/
	public Iso9660File() {
	// Default constructor
	}

	/***************************************************************************
	 * Constructor that sets all information fields
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051209
	 * @since 20051209
	 * @param dL - The data length
	 * @param rDT - The recording date and time
	 * @param fUS - The file unit size
	 * @param iGS - The interleave gap size
	 * @param vSN - The volume sequence number
	 * @param fI - The file identifier
	 * 
	 **************************************************************************/
	public Iso9660File(long dL, String rDT, long fUS, long iGS, long vSN,
			String fI) {
		this.dataLength = dL;
		this.recordingDateAndTime = rDT;
		this.fileUnitSize = fUS;
		this.interleaveGapSize = iGS;
		this.volumeSequenceNumber = vSN;
		this.fileIdentifier = fI;
	}

	// GET & SET METHODS **************************************************

	/***************************************************************************
	 * Get-method for the data length
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @return long - The data length
	 **************************************************************************/
	public long getDataLength() {
		return this.dataLength;
	}

	/***************************************************************************
	 * Set-method for the data length
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @param dL - The data length
	 **************************************************************************/
	public void setDataLength(long dL) {
		this.dataLength = dL;
	}

	/***************************************************************************
	 * Get-method for the data length
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @return long - The file identifier
	 **************************************************************************/
	public String getFileIdentifier() {
		return this.fileIdentifier;
	}

	/***************************************************************************
	 * Set-method for the data length
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @param fI - The file identifier
	 **************************************************************************/
	public void setFileIdentifier(String fI) {
		this.fileIdentifier = fI;
	}

	/***************************************************************************
	 * Get-method for the data length
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @return long - The file unit size
	 **************************************************************************/
	public long getFileUnitSize() {
		return this.fileUnitSize;
	}

	/***************************************************************************
	 * Set-method for the data length
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @param fUS - The file unit size
	 **************************************************************************/
	public void setFileUnitSize(long fUS) {
		this.fileUnitSize = fUS;
	}

	/***************************************************************************
	 * Get-method for the data length
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @return long - The interleave gap size
	 **************************************************************************/
	public long getInterleaveGapSize() {
		return this.interleaveGapSize;
	}

	/***************************************************************************
	 * Set-method for the data length
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @param iGS - The interleave gap size
	 **************************************************************************/
	public void setInterleaveGapSize(long iGS) {
		this.interleaveGapSize = iGS;
	}

	/***************************************************************************
	 * Get-method for the data length
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @return long - The recording date and time
	 **************************************************************************/
	public String getRecordingDateAndTime() {
		return this.recordingDateAndTime;
	}

	/***************************************************************************
	 * Set-method for the data length
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @param rDT - The recording date and time
	 **************************************************************************/
	public void setRecordingDateAndTime(String rDT) {
		this.recordingDateAndTime = rDT;
	}

	/***************************************************************************
	 * Get-method for the data length
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @return long - The volume sequence number
	 **************************************************************************/
	public long getVolumeSequenceNumber() {
		return this.volumeSequenceNumber;
	}

	/***************************************************************************
	 * Set-method for the volume sequence number
	 * 
	 * @author Matthias Neubauer, German National Library
	 * @version 20051124
	 * @since 20051124
	 * @param vSN - The volume sequence number
	 **************************************************************************/
	public void setVolumeSequenceNumber(long vSN) {
		this.volumeSequenceNumber = vSN;
	}

}
