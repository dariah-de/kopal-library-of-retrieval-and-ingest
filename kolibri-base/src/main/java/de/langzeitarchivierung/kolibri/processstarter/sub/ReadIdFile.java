/*******************************************************************************
 * de.langzeitarchivierung.kolibri.processStarter.sub / ReadIdFile.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA
 * 
 * 
 *******************************************************************************
 *
 *
 * CHANGELOG:
 * 26.04.2010 Funk: Added the "#" as not used line criteria (comment) in ID
 * 					file.
 * 23.04.2010 Funk: Added some setter for config values, generalised a bit.
 * 11.10.2006 Funk: Removed the method message().
 * 14.02.2006 Funk: Monitor for parallel processing now is this.processQueue
 * 					instead of ProcessQueue.class
 * 02.02.2006 Funk: Merged two versions. TODO Look for the nasty ID thing.
 * 14.09.2005 Ludwig: first Version
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.processstarter.sub;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.ProcessQueue;
import de.langzeitarchivierung.kolibri.processstarter.ProcessStarter;

/*******************************************************************************
 * Reads from a file containing IDs only.
 * 
 * @author Ludwig
 * @version 20100423
 * @since 20050914
 ******************************************************************************/
public class ReadIdFile implements ProcessStarter {

	// CLASS VARIABLES (Static variables) ***********************************

	private static Logger	defaultLogger	= Logger.getLogger("de.langzeitarchivierung.kolibri");

	// STATE (Instance variables) ******************************************

	private String			workDir;
	private String			defaultPolicyName;

	private ProcessQueue	processQueue;
	private File			idFile;
	private String			idFilename;
	private String			idPrefix		= "ppn";
	private String			idName			= "PPN";

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * 
	 **************************************************************************/
	public ReadIdFile() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#run()
	 */
	public void run() {
		try {
			// TODO Catch exceptions!
			this.idFile = new File(this.idFilename);

			BufferedReader source = new BufferedReader(new FileReader(
					this.idFile));
			String line = source.readLine();

			// While there are unread characters
			while (line != null) {
				if (!line.equals("") && !line.startsWith("#")) {
					addAsset(line);
				}
				line = source.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
			defaultLogger.log(Level.SEVERE, "Error reading the ID file '"
					+ this.idFile.getName() + "': " + e.getMessage());
		}

	}

	/***************************************************************************
	 * Adds a given directory and a given ID as an asset to complete to the
	 * process list.
	 * 
	 * @param id
	 *            The lmerID used in the metsLmers persistentIdentifier.
	 **************************************************************************/
	private void addAsset(String id) {
		try {
			defaultLogger.log(Level.INFO, "Adding " + this.idName + " = " + id);

			// Add the element using the default policy from the config file and
			// the ID as ProcessName to identify the list element.
			ProcessData pd = new ProcessData(this.defaultPolicyName, id);

			defaultLogger.log(Level.INFO, "ProcessData created");

			pd.getMetadata().setObjectId(id);

			defaultLogger.log(Level.INFO, "Set ObjectId");

			pd.setPathToContentFiles(this.workDir);

			defaultLogger.log(Level.INFO, "Set PathTo ContentFiles");

			pd.getCustomIds().put(this.idName, id);

			this.processQueue.addElement(pd);
			defaultLogger.log(Level.INFO, "Added new " + this.idName
					+ " with value '" + id + "' to the process list");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#setProcessQueue
	 * (de.langzeitarchivierung.kolibri.ProcessQueue)
	 */
	public void setProcessQueue(ProcessQueue _processQueue) {
		this.processQueue = _processQueue;
	}

	/***********************************************************************
	 * @param workDir
	 *            The workDir to set.
	 **********************************************************************/
	public void setWorkDir(String workDir) {
		this.workDir = workDir;
	}

	/***********************************************************************
	 * @param defaultPolicyName
	 *            The defaultPolicyName to set.
	 **********************************************************************/
	public void setDefaultPolicyName(String defaultPolicyName) {
		this.defaultPolicyName = defaultPolicyName;
	}

	/**************************************************************************
	 * @param idFilename
	 **************************************************************************/
	public void setIdFilename(String idFilename) {
		this.idFilename = idFilename;
	}

	/**************************************************************************
	 * @param idPrefix
	 **************************************************************************/
	public void setIdPrefix(String idPrefix) {
		this.idPrefix = idPrefix;
	}

	/**************************************************************************
	 * @param idName
	 **************************************************************************/
	public void setIdName(String idName) {
		this.idName = idName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#setInput(
	 * java.lang.String)
	 */
	public void setInput(String input) {
		this.idFile = new File(input);
	}

}
