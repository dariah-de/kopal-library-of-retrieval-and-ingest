/*******************************************************************************
 * de.langzeitarchivierung.kolibri.processstarter.sub / MonitorHotfolder.java
 *   
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 *******************************************************************************
 *
 *
 * CHANGELOG
 * 	2011-09-29	Funk	Added getters and setters for database usage.
 *	2010-11-24	Funk	Renamed to MonitorHotfolder.
 *	2007-07-10	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.processstarter;

import java.io.File;
import java.util.HashMap;

/*******************************************************************************
 * <p>
 * The example hotfolder process starter. In this class only the abstract
 * methods are defined. Please edit as you wish.
 * </p>
 * 
 * @author Stefan Funk, SUB Göttingen
 * @version 2011-09-29
 * @see de.langzeitarchivierung.kolibri.processstarter
 * @since 2007-01-23
 ******************************************************************************/

public class MonitorHotfolder extends MonitorHotfolderBase {

	/*
	 * <p> Think of any name that identifies the object properly. We take the
	 * folder's name here. </p>
	 * 
	 * (non-Javadoc)
	 * 
	 * @seede.langzeitarchivierung.kolibri.processstarter.MonitorHotfolderBase#
	 * getProcessName(java.io.File)
	 */
	@Override
	protected String getProcessName(File currentFile) {
		return currentFile.getName();
	}

	/*
	 * <p> Set as much custom identifier as you want to, all of them are written
	 * to the database. </p>
	 * 
	 * (non-Javadoc)
	 * 
	 * @seede.langzeitarchivierung.kolibri.processstarter.MonitorHotfolderBase#
	 * getCustomIdList(java.io.File)
	 */
	@Override
	protected HashMap<String, String> getCustomIdList(File currentFile) {
		HashMap<String, String> result = new HashMap<String, String>();

		return result;
	}

	/*
	 * <p> You can specify a persistent identifier here or use a custom action
	 * module to do so later on. We also use the name of the folder here. </p>
	 * 
	 * (non-Javadoc)
	 * 
	 * @seede.langzeitarchivierung.kolibri.processstarter.MonitorHotfolderBase#
	 * getPersistentIdentifier(java.io.File)
	 */
	@Override
	protected String getPersistentIdentifier(File currentFile) {
		return currentFile.getName();
	}

	/*
	 * <p> Restrict file if currentFile is a file and readDirectoriesOnly is set
	 * to TRUE. Hidden files are NOT included either! </p>
	 * 
	 * @seede.langzeitarchivierung.kolibri.processstarter.MonitorHotfolderBase#
	 * fileIsRestricted(java.io.File)
	 */
	@Override
	protected boolean sipIsRestricted(File currentFile) {

		if (this.readDirectoriesOnly && !currentFile.isDirectory()) {
			return true;
		}
		if (this.ignoreHiddenFiles && currentFile.isHidden()) {
			return true;
		}

		return false;
	}

	// **
	// GETTERS AND SETTERS
	// **

	/**
	 * @param dbHost
	 */
	public void setDbHost(String dbHost) {
		this.dbHost = dbHost;
	}

	/**
	 * @param dbName
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	/**
	 * @param dbUser
	 */
	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}

	/**
	 * @param dbPass
	 */
	public void setDbPass(String dbPass) {
		this.dbPass = dbPass;
	}

	/**
	 * @param dbOwner
	 */
	public void setDbOwner(String dbOwner) {
		this.dbOwner = dbOwner;
	}

	/**
	 * @param dbServer
	 */
	public void setDbServer(String dbServer) {
		this.dbServer = dbServer;
	}

}
