/**
 * de.langzeitarchivierung.kolibri.processstarter / MonitorHotfolderBase.java
 * 
 * Copyright 2005-2009 by project kopal Copyright 2010-2011 by project DP4lib Copyright 2011-2015 by
 * project TextGrid Copyright 2015-2017 by project DARIAH-DE
 * 
 * http://kopal.langzeitarchivierung.de http://dp4lib.langzeitarchivierung.de https://textgrid.de
 * https://de.dariah.eu
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 * 
 **
 *
 * CHANGELOG
 * 
 * 2017-03-29 Funk Checking threading...
 * 
 * 2010-11-25 Funk Added ignoreHiddenFiles.
 * 
 * 2010-11-22 Funk Changes while{} into do{}while() loop for re-reading hotfolder.
 * 
 * 2010-11-12 Funk Added dbOwner and dbServer.
 * 
 * 2010-10-29 Funk Removed all the System.exit()s.
 * 
 * 2010-10-22 Funk Made checkSipSource configurable.
 * 
 * 2010-08-20 Funk Changed monitorHotfolderLocation() to protected.
 * 
 * 2008-05-21 Funk Added the recheckHotfolder member to configure re-checking of the hotfolder.
 * 
 * 2007-07-10 koLibRI version 1.0
 */

package de.langzeitarchivierung.kolibri.processstarter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import de.langzeitarchivierung.kolibri.Policy;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.ProcessQueue;
import de.langzeitarchivierung.kolibri.util.FileUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;

/**
 * <p>
 * This is a general process starter, which looks up a hotfolder every X seconds and if something
 * new is in it, the folder (or file if wished) is added to the process list. There are three
 * abstract classes to set the process name, the custom identifier list and the persistent
 * identifier of the object.
 * </p>
 * 
 * <p>
 * PRECONDITION: Needs a valid directory out of the -i flag of the WorkflowTool or out of the main
 * config file.
 * </p>
 * 
 * <p>
 * RESULT: Adds all directories (and if wished also files) out of the source directory to the
 * process list and sets the ID (taken from the directory name) and the pathToContentFiles.
 * </p>
 * 
 * TODO Test mixed Hotfolder and HttpLocations with one database!! It is not supposed to work yet!!
 * 
 * @author Stefan Funk, SUB Göttingen
 * @version 2017-03-29
 * @since 2005-10-23
 * @see de.langzeitarchivierung.kolibri.processstarter
 */

public abstract class MonitorHotfolderBase implements ProcessStarter {

  // FINAL VARIABLES (Static variables) ***********************************

  /** Constants in the database for various data (varioustype) */
  public static final String VAR_SIPSOURCEASURI = "sipSourceAsUri";

  // CLASS VARIABLES (Static variables) ***********************************

  protected static Logger defaultLogger = Logger
      .getLogger("de.langzeitarchivierung.kolibri");

  // TODO I had problems with this static HashMap and ArrayList in
  // /kolibri-addon-textgrid-import/src/main/java/de/langzeitarchivierung/kolibri/processstarter/textgrid/Folder.java.
  // Please check here eventually!
  protected static ArrayList<File> listOfFiles = new ArrayList<File>();
  protected static HashMap<File, Long> fileSizes = new HashMap<File, Long>();
  protected static boolean checkSipSource = true;
  protected static Policy policy = null;

  // STATE (Instance variables) ******************************************

  protected String inputDirFromCli;
  protected ProcessQueue processQueue;
  protected long startingOffset = 0;
  protected long checkingOffset = 10000;
  protected long addingOffset = 3000;
  protected String defaultPolicyName;
  protected File hotfolderDir;
  protected boolean readDirectoriesOnly = true;
  protected boolean ignoreHiddenFiles = false;
  protected boolean recheckHotfolder = true;
  protected String dbHost;
  protected String dbName;
  protected String dbUser;
  protected String dbPass;
  protected String dbOwner;
  protected String dbServer;

  // CREATION (Constructors, factory methods, static/inst init) **********

  /**
   * <p>
   * Constructor
   * </p>
   */
  public MonitorHotfolderBase() {
    super();
  }

  // MANIPULATION (Manipulation - what the object does) ******************

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#run()
   */
  public void run() {

    // Take the hotfolder's path out of the command line if existing.
    if (this.inputDirFromCli != null && !this.inputDirFromCli.equals("")) {
      this.hotfolderDir = new File(this.inputDirFromCli);
    }

    defaultLogger.log(Level.INFO, "Using policy '" + this.defaultPolicyName + "'");
    defaultLogger.log(Level.INFO,
        "'" + this.hotfolderDir + "' is taken " + "as input hotfolder, waiting for input");
    defaultLogger.log(Level.INFO,
        "Hotfolder scheduling starts in " + KolibriTimestamp.getDurationInHours(this.startingOffset)
            + ", "
            + (this.recheckHotfolder
                ? "recheck every " + KolibriTimestamp.getDurationInHours(this.checkingOffset)
                : "no recheck"));

    try {
      Thread.sleep(this.startingOffset);

      // TODO Maybe add a possibility here to exit the process starter without sending a CTRL-C?
      do {
        monitorHotfolderLocation();
        Thread.sleep(this.checkingOffset);
      } while (this.recheckHotfolder);
    } catch (InterruptedException e) {
      defaultLogger.log(Level.SEVERE, "ProcessStarter was interrupted: " + e.getMessage());
      e.printStackTrace();
      return;
    } catch (IOException e) {
      defaultLogger.log(Level.SEVERE,
          "IOException during ProcessStarter processing: " + e.getMessage());
      e.printStackTrace();
      return;
    }
  }

  // INTERNAL (Internal - implementation details, local classes, ...)

  /**
   * <p>
   * Check the hotfolder location for new files and folders.
   * </p>
   * 
   * @throws IOException
   */
  protected void monitorHotfolderLocation() throws IOException {

    File fileList[] = this.hotfolderDir.listFiles();
    File currentFile = null;

    // Check if the given directory exists.
    if (!this.hotfolderDir.exists() || !this.hotfolderDir.isDirectory()) {
      String message = "The given hotfolder path does not exist or is not a directory";
      defaultLogger.log(Level.SEVERE, message);
      throw new IOException(message);
    }

    defaultLogger.log(Level.INFO,
        "Checking hotfolder " + this.hotfolderDir.getAbsolutePath() + " for new content");

    // Go through all the files in the given dir.
    int sipsToRun = fileList.length;

    for (int i = 0; i < sipsToRun; i++) {
      currentFile = fileList[i];

      // Process all files not contained in the list yet.
      if (!MonitorHotfolderBase.listOfFiles.contains(currentFile)) {
        MonitorHotfolderBase.listOfFiles.add(currentFile);

        // Check if file is restricted.
        if (sipIsRestricted(currentFile)) {
          // Add the file to the listOfFiles if so.
          defaultLogger.log(Level.FINEST,
              "File '" + currentFile + "' is marked as restricted, skipping this file");
        }

        // If not, add the file to the scheduling list.
        else {
          // Calculate and save folder size.
          MonitorHotfolderBase.fileSizes.put(currentFile, FileUtils.getCompleteFileSize(currentFile));

          // Give some scheduling log messages.
          Long filesize = MonitorHotfolderBase.fileSizes.get(currentFile);
          String timestamp =
              KolibriTimestamp.getDurationInHours(MonitorHotfolderBase.this.addingOffset);
          defaultLogger.log(Level.INFO,
              "Scheduling file '" + currentFile.getName() + "' (" + filesize
                  + " bytes) for adding to the process list, " + "waiting " + timestamp);

          // Schedule the addition of the new SIP.
          schedule(currentFile);
        }
      }
    }

    // FIXME: Terminate if no re-check wanted.
    if (this.recheckHotfolder) {
      defaultLogger.log(Level.INFO, "All current files scheduled, waiting for more");
    } else {
      defaultLogger.log(Level.INFO, "All current files scheduled, exiting");
    }
  }

  /**
   * <p>
   * Starts a new TimerTask for the given file.
   * </p>
   * 
   * @param currentFile
   */
  protected void schedule(File currentFile) {

    Timer timer = new Timer();
    AddAssetTask aat = new AddAssetTask();
    aat.setCurrentFile(currentFile);
    timer.schedule(aat, MonitorHotfolderBase.this.addingOffset);
  }

  /**
   * <p>
   * The name of the process to identify the file's process in the process list must be defined in
   * an abstract class.
   * </p>
   * 
   * @param currentFile The process name for this file is created here.
   * @return String The process name to return.
   */
  protected abstract String getProcessName(File currentFile);

  /**
   * <p>
   * A custom identifier (if existing) must be defined in an abstract class.
   * </p>
   * 
   * @param currentFile The current file to set the custom identifier for.
   * @return HashMap<String, String> The hash map containing all the custom IDs.
   */
  protected abstract HashMap<String, String> getCustomIdList(
      File currentFile);

  /**
   * <p>
   * The <lmerObject:persistentIdentifier> must be defined in an abstract class.
   * </p>
   * 
   * @param currentFile The file to create a persistent ID for.
   * @return String The persistent ID to return.
   */
  protected abstract String getPersistentIdentifier(File currentFile);

  /**
   * <p>
   * Here you have the ability to restrict the added files somehow, e.g. only add SIPs to the
   * process list that includes a mets.xml file.
   * </p>
   * 
   * @param currentFile
   * @return TRUE if the file shall be added to the process list, FALSE if not.
   */
  protected abstract boolean sipIsRestricted(File currentFile);

  // GET & SET METHODS **************************************************

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#setInput (java.lang.String)
   */
  public void setInput(String input) {
    this.inputDirFromCli = input;
  }

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.processstarter.ProcessStarter# setProcessQueue
   * (de.langzeitarchivierung.kolibri.ProcessQueue)
   */
  public void setProcessQueue(ProcessQueue processQueue) {
    this.processQueue = processQueue;
  }

  /**
   * @param defPolicyName The defaultPolicyName to set.
   */
  public void setDefaultPolicyName(String defPolicyName) {
    this.defaultPolicyName = defPolicyName;
  }

  /**
   * @return
   */
  public String getDefaultPolicyName() {
    return this.defaultPolicyName;
  }

  /**
   * @param hotfolderDir The hotfolder directory to set.
   */
  public void setHotfolderDir(String hotfolderDir) {
    this.hotfolderDir = new File(hotfolderDir);
  }

  /**
   * @param readDirectoriesOnly Should only directories be processed as an asset?
   */
  public void setReadDirectoriesOnly(boolean readDirectoriesOnly) {
    this.readDirectoriesOnly = readDirectoriesOnly;
  }

  /**
   * @return Returns the addingOffset.
   */
  public long getAddingOffset() {
    return this.addingOffset;
  }

  /**
   * @param addingOffset The addingOffset to set.
   */
  public void setAddingOffset(long addingOffset) {
    this.addingOffset = addingOffset;
  }

  /**
   * @return Returns the checkingOffset.
   */
  public long getCheckingOffset() {
    return this.checkingOffset;
  }

  /**
   * @param checkingOffset The checkingOffset to set.
   */
  public void setCheckingOffset(long checkingOffset) {
    this.checkingOffset = checkingOffset;
  }

  /**
   * @return Returns the startingOffset.
   */
  public long getStartingOffset() {
    return this.startingOffset;
  }

  /**
   * @param startingOffset The startingOffset to set.
   */
  public void setStartingOffset(long startingOffset) {
    this.startingOffset = startingOffset;
  }

  /**************************************************************************
   * @param checkSipSource
   */
  public static void setCheckSipSource(boolean checkSipSource) {
    MonitorHotfolderBase.checkSipSource = checkSipSource;
  }

  /**
   * @return Returns the recheckHotfolder.
   */
  public boolean isRecheckHotfolder() {
    return this.recheckHotfolder;
  }

  /**
   * @param recheckHotfolder The recheckHotfolder to set.
   */
  public void setRecheckHotfolder(boolean recheckHotfolder) {
    this.recheckHotfolder = recheckHotfolder;
  }

  /**
   * @param ignoreHiddenFiles
   */
  public void setIgnoreHiddenFiles(boolean ignoreHiddenFiles) {
    this.ignoreHiddenFiles = ignoreHiddenFiles;
  }

  /**
   * <p>
   * Adds a given directory or file including all necesarry things to the process list.
   * </p>
   */

  protected class AddAssetTask extends TimerTask {

    // STATE (Instance variables) ******************************************

    protected File currentFile = null;

    /*
     * (non-Javadoc)
     * 
     * @see java.util.TimerTask#run()
     */
    @Override
    public void run() {

      long oldFileSize;

      try {
        // Get the complete file size.
        oldFileSize = (MonitorHotfolderBase.fileSizes
            .get(this.currentFile)).longValue();
        log(Level.FINE, "Recalculating complete file size");

        // If it's equal to the latest calculated size, process the
        // element.
        if (oldFileSize == FileUtils
            .getCompleteFileSize(this.currentFile)) {
          log(Level.FINE,
              "Complete file size does match the recently calculated size, prepare to process");

          // Remove the completeFileSize from the HashMap.
          MonitorHotfolderBase.fileSizes.remove(this.currentFile);

          log(Level.WARNING, "SIP source is not being checked");

          // If it's not existing, add the SIP to the process queue.
          addSipToProcessQueue();
        }

        // If the file size is not equal to the latest calculated one,
        // retry later.
        else {
          // Remove file from fileList to schedule again later.
          MonitorHotfolderBase.listOfFiles.remove(this.currentFile);

          log(Level.INFO,
              "Scheduling of file delayed because of file size differences, maybe uploading is still in progress");
        }
      } catch (Exception e) {
        defaultLogger.log(Level.SEVERE,
            "Error adding file to the process list: "
                + e.getMessage(),
            this.currentFile);
        e.printStackTrace();
      }
    }

    // INTERNAL (Internal - implementation details, local classes, ...)

    /**
     * <p>
     * Add the new element to the process queue.
     * </p>
     * 
     * @throws Exception
     */
    private void addSipToProcessQueue() throws Exception {

      // Add the element using the default policy from the config file and
      // the directory name as a process name to identify the list
      // element.
      ProcessData pd = new ProcessData(
          // MonitorHotfolderBase.this.getDatabase(),
          MonitorHotfolderBase.this.defaultPolicyName,
          getProcessName(this.currentFile));

      // Set the LMER persistent identifier (URN).
      pd.getMetadata()
          .setObjectId(getPersistentIdentifier(this.currentFile));

      // Set the path to the SIP's content files.
      log(Level.INFO, "Setting path to content files");
      pd.setPathToContentFiles(this.currentFile.getAbsolutePath());

      // Put it as SIP source in the custom data.
      String customSipSource = this.currentFile.toURI().toString();
      pd.getCustomData().put(MonitorHotfolderBase.VAR_SIPSOURCEASURI,
          customSipSource);
      log(Level.FINE,
          "Added custom date "
              + MonitorHotfolderBase.VAR_SIPSOURCEASURI + " = "
              + customSipSource);

      // If there are some custom IDs, set the custom IDs in newly created ID items.
      if (getCustomIdList(this.currentFile) != null) {
        HashMap<String, String> customIdList = getCustomIdList(this.currentFile);
        pd.getCustomIds().putAll(customIdList);

        Iterator<String> iter = customIdList.keySet().iterator();

        while (iter.hasNext()) {
          String name = iter.next();
          String value = customIdList.get(name);
          log(Level.FINE, "Added custom ID " + name + " = " + value);
        }
      }

      // Add the SIP to the process list, if set with owner and server information.
      if (MonitorHotfolderBase.this.dbOwner != null
          && MonitorHotfolderBase.this.dbServer != null
          && !MonitorHotfolderBase.this.dbOwner.equals("")
          && !MonitorHotfolderBase.this.dbServer.equals("")) {
        MonitorHotfolderBase.this.processQueue.addElement(pd,
            MonitorHotfolderBase.this.dbOwner,
            MonitorHotfolderBase.this.dbServer);
      } else {
        MonitorHotfolderBase.this.processQueue.addElement(pd);
      }

      if (getPersistentIdentifier(this.currentFile) == null) {
        log(Level.INFO, "Added to the process list");
        log(Level.WARNING, "Persistent identifier yet unknown");
      } else {
        log(Level.INFO, "Added to the process list, persistent identifier set to "
            + getPersistentIdentifier(this.currentFile));
      }
    }

    /**
     * <p>
     * Just a small logging method.
     * </p>
     * 
     * @param level
     * @param message
     */
    protected void log(Level level, String message) {
      defaultLogger.log(level, getProcessName(this.currentFile) + "  -->  " + message);
    }

    // GET & SET METHODS ***************************************************

    /**
     * @param currentFile
     */
    public void setCurrentFile(File currentFile) {
      this.currentFile = currentFile;
    }
  }

}
