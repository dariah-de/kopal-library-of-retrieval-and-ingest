/*******************************************************************************
 * de.langzeitarchivierung.kolibri.processstarter / ProcessStarter.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 *
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.processstarter;

import de.langzeitarchivierung.kolibri.ProcessQueue;

/*******************************************************************************
 * Interface for classes which adds processes and their Policies to the process
 * queue.
 * 
 * TODO Interface will be modified in the future.
 * 
 * @author Ludwig
 * @since 20050914
 * @version 20060126
 ******************************************************************************/

public interface ProcessStarter extends Runnable {

	/***************************************************************************
	 * @see java.lang.Runnable#run()
	 **************************************************************************/
	public void run();

	/***************************************************************************
	 * Gives the processStarter the process list to which new assets should be
	 * added.
	 * 
	 * @param processQueue
	 *            the process list to which new assets should be added
	 **************************************************************************/
	public void setProcessQueue(ProcessQueue processQueue);

	/***************************************************************************
	 * Gives the processStarter the input source from the commandline.
	 * 
	 * @param input
	 *            the specified input (from the commandline) for this
	 *            processstarter
	 **************************************************************************/
	public void setInput(String input);

}
