/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionmodule / Executor.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG: 10.07.2007 koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;

/*******************************************************************************
 * Module that executes a command it receives as constructor parameter or from the customData under
 * EXECUTER_COMMAND.
 * 
 * Precondition: as the command requires result: the execution of the command
 * 
 * TODO Error output!
 * 
 * @author Ludwig
 * @version 20070710
 * @since 20050707
 ******************************************************************************/

public class Executor implements ActionModule {

  // STATE (Instance variables) *****************************************

  /***************************************************************************
   * String for Executer command
   **************************************************************************/
  public static final String EXECUTER_COMMAND = "executer_command";

  protected ProcessData processData;
  protected Step step;

  protected String command;

  // CREATION (Constructors, factory methods, static/inst init) **********

  /***************************************************************************
   * Default constructor. Uses the command from the custom data entry with the constant
   * EXECUTER_COMMAND in the processData object.
   **************************************************************************/
  public Executor() {
    super();
    this.command = (String) this.processData.getCustomData().get(
        EXECUTER_COMMAND);
  }

  // MANIPULATION (Manipulation - what the object does) ******************

  /***************************************************************************
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   **************************************************************************/
  @Override
  public void go() {
    this.step.setStatus(Status.RUNNING, "Executer tries running '"
        + this.command + "'");
    execute();
  }

  // INTERNAL (Internal - implementation details, local classes, ...) ****

  /***************************************************************************
   * Does the main work. Creates a new process, executes it and put the output to the default
   * logger.
   **************************************************************************/
  protected void execute() {
    Process runningProcess = null;
    try {
      // Execute the command.
      runningProcess = Runtime.getRuntime().exec(this.command);

      // Get the output stream of the command.
      InputStream processOutput = new BufferedInputStream(
          runningProcess.getInputStream());

      // TODO What to do with the error messages? Log them using the
      // default logger?
      // InputStream processErrorOutput = new BufferedInputStream(
      // runningProcess.getErrorStream());

      // Print the output.
      // TODO Maybe put it to a file if wanted?
      int input = processOutput.read();
      while (input != -1) {
        System.out.print((char) input);
        input = processOutput.read();
      }
    } catch (IOException e) {
      this.step.setStatus(Status.ERROR, "Output error in Executer");
      return;
    }

    try {
      // Wait for the process to exit and print status.
      switch (runningProcess.waitFor()) {
        case 0:
          this.step.setStatus(Status.DONE, "Executer was successful");
          break;
        default:
          this.step.setStatus(Status.ERROR, "Error in Executer. "
              + "Exit status: " + runningProcess.exitValue());
          break;
      }
    } catch (InterruptedException e) {
      this.step.setStatus(Status.ERROR, "Interrupt error in Executer");
      return;
    }
  }

  // GET & SET METHODS *******************************************************

  /***************************************************************************
   * @param _command The command to set.
   **************************************************************************/
  public void setCommand(String _command) {
    this.command = _command;
  }

  /***************************************************************************
   * @param _processData The processData to set.
   **************************************************************************/
  @Override
  public void setProcessData(ProcessData _processData) {
    this.processData = _processData;
  }

  /***************************************************************************
   * @param _step The step to set.
   **************************************************************************/
  @Override
  public void setStep(Step _step) {
    this.step = _step;
  }

}
