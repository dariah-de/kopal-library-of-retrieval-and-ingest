/**
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 * 
 **
 *
 * CHANGELOG
 * 
 * 2023-04-17 - Funk - Remove DB related code.
 * 
 * 17.08.2011 - Funk Added UUID ID generation if useDatabase is set to FALSE.
 * 
 * 22.10.2010 - Funk - Generalized and renamed to GenerateIdentifier. URNs can still be created if
 * configuring the LPI and prefixes correctly in the config file, as the URN NBN checksum char
 * creation.
 * 
 * 13.02.2007 - Funk - Test of useDatabase added.
 * 
 * 04.12.2006 - Funk - Edited for new release.
 * 
 * 11.11.2005 - Funk - Added duration. 21.10.2005 Funk Using the lmer persistentIdentifier now
 * because of the issue number (for GDZ).
 * 
 * 13.10.2005 - Ludwig- first Version
 */

package de.langzeitarchivierung.kolibri.actionmodule;

import java.util.UUID;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;

public class GenerateIdentifier implements ActionModule {

  // STATE (Instance variables) **********************************************

  private ProcessData processData;
  private Step step;
  private String idPrefix = "urn:prefix:";

  // CREATION (Constructors, factory methods, static/inst init) **********

  public GenerateIdentifier() {
    super();
  }

  // MANIPULATION (Manipulation - what the object does) ******************

  /**
   *
   */
  @Override
  public void go() {

    // Crate a random UUID as identifier.
    String id = this.idPrefix + UUID.randomUUID().toString();

    // Set ID to metadata.
    try {
      this.processData.getMetadata().setObjectId(id);
    } catch (Exception e) {
      this.step.setStatus(Status.ERROR, "Error accessing metadata object ID: " + e.getMessage());
      e.printStackTrace();
      return;
    }

    this.step.setStatus(Status.DONE, "ID was created as random UUID: " + id);
  }

  // GET & SET METHODS **************************************************

  /**
   *
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   *
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param idPrefix
   */
  public void setIdPrefix(String idPrefix) {
    this.idPrefix = idPrefix;
  }

}
