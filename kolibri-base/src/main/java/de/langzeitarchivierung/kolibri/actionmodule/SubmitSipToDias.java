/**
 * This software is copyright (c) 2019 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Matthias Neubauer, German National Library
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.ingest.DiasIngest;
import de.langzeitarchivierung.kolibri.ingest.DiasIngestResponse;
import de.langzeitarchivierung.kolibri.util.Configurator;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2023-04-27 - Funk - Remove DB related code.
 * 
 * 2019-11-18 - Funk - Testing new koala test system.
 * 
 * 2007-07-10 - koLibRI version 1.0
 */

/**
 * <p>
 * Actionmodule that transfers the created SIP to the DIAS via FTP.
 * </p>
 * 
 * <p>
 * PRECONDITION: A completely created DIAS conform SIP with its path stored in the according
 * parameter of the LmerSIP object.
 * </p>
 * 
 * <p>
 * RESULT: The LmerSIP has been transfered to the DIAS system. The ticket ID is stored in the
 * database.
 * </p>
 * 
 * @version 2019-11-26
 * @since 2005-09-07
 * @see de.langzeitarchivierung.kolibri.ingest.DiasIngest
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 */

public class SubmitSipToDias implements ActionModule {

  public static final String ID_DIASTICKETID = "DIASTICKETID";

  // **
  // STATE (Instance variables)
  // **

  private ProcessData processData;
  private Step step;

  // **
  // CREATION (Constructors, factory methods, static/inst init)
  // **

  /**
   * 
   */
  public SubmitSipToDias() {
    super();
  }

  // **
  // MANIPULATION (Manipulation - what the object does)
  // **

  /**
   *
   */
  @Override
  public void go() {

    // Configure DiasIngest.
    DiasIngest di = new DiasIngest();
    try {
      Configurator.configure(di);
    } catch (InvocationTargetException | IllegalAccessException e) {
      this.step.setStatus(Status.ERROR,
          "Error configuring DiasIngest object. System message: " + e.getMessage());
      return;
    }

    // Set the SIP's name.
    String sipIngestName = this.processData.getSipLocation().getName();

    // Get an ingest ticket from the DIAS and parse it.
    this.step.setStatus(Status.RUNNING,
        "Requesting a SIP ingest ticket ID from DIAS for SIP '" + sipIngestName + "'");

    // Get DIAS ingest response.
    String diasResponse = "";
    try {
      diasResponse = di.requestSipIngest(sipIngestName);
    } catch (IOException e) {
      this.step.setStatus(Status.ERROR,
          "Error requesting a SIP ingest ticket from the DIAS. System message: " + e.getMessage());
      return;
    }

    // Parse DIAS ingest response.
    DiasIngestResponse dir = new DiasIngestResponse();
    try {
      dir = DiasIngest.parseDiasResponse(new StringReader(diasResponse));
    } catch (IOException | SAXException e) {
      this.step.setStatus(Status.ERROR,
          "Error parsing SIP ingest repsonse. System message: " + e.getMessage());
      return;
    }

    // Set ticket ID and check for errors.
    String ticketId = dir.getMetadata().get(0).getSipIngestTicketNumber();
    String alert = dir.getMetadata().get(0).getAlert();
    if (!alert.equals(DiasIngestResponse.NOT_IN_DIAS_RESPONSE)) {
      this.step.setStatus(Status.ERROR,
          "SIP ingest ticket ID request failed. DIAS alert: " + alert);
      return;
    } else if (ticketId.equals(DiasIngestResponse.NOT_IN_DIAS_RESPONSE)) {
      this.step.setStatus(Status.ERROR,
          "SIP ingest ticket ID request failed. No ticket ID available in DIAS response.");
      return;
    }
    this.step.setStatus(Status.RUNNING, "SIP ingest ticket ID is '" + ticketId + "'");

    // Set the ticket ID in custom ID hash map.
    HashMap<Object, Object> customIds = this.processData.getCustomIds();
    customIds.put(ID_DIASTICKETID, ticketId);

    // Start submitting the SIP to DIAS.
    this.step.setStatus(Status.RUNNING, "Submitting SIP to the DIAS");
    long submitStartTime = System.currentTimeMillis();

    try {
      di.submitFileToDias(this.processData.getSipLocation());
    } catch (Exception e) {
      this.step.setStatus(Status.ERROR, "Transfer failed. System message: " + e.getMessage());
      return;
    }

    long totalSubmissionTime = System.currentTimeMillis() - submitStartTime;
    this.step.setStatus(Status.DONE,
        "SIP '" + this.processData.getSipLocation().getName()
            + "' has been sucessfully submitted in "
            + KolibriTimestamp.getDurationInHours(totalSubmissionTime) + " ("
            + (this.processData.getSipLocation().length() / totalSubmissionTime) + " kb/s)");
  }

  // **
  // GET & SET METHODS
  // **

  /**
   *
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   *
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

}
