/*******************************************************************************
 * Copyright 2005-2007 by Project kopal
 *
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 * 
 *******************************************************************************
 *
 * CHANGELOG
 * 
 * 10.07.2007 koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.util.FileUtils;
import de.langzeitarchivierung.kolibri.util.KolibriSeparateLogFormatter;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import de.langzeitarchivierung.kolibri.util.TiffIMP;
import edu.harvard.hul.ois.jhove.JhoveBase;
import edu.harvard.hul.ois.jhove.Message;
import edu.harvard.hul.ois.jhove.RepInfo;

/*******************************************************************************
 * Reads all TIFF files including the TIFF header metadata. At the moment this class is correcting
 * the following TIFF header errors:
 * 
 * <li>All offset values of the TIFF_ASCII metadata will be set to correct word aligned
 * offsets.</li>
 * 
 * <li>The count of the TIFF_SHORT in tag 297 (PageNumber) is raised to the correct value 2
 * (PageNumber[1] is set to 0 then, PageNumbe[0] contains the original value), if only one value is
 * existing.</li>
 * 
 * Please use the jai_imageio library > jai_imageio 1.1 daily build 20061003, earlier builds maybe
 * do not save the TIFF ASCII values and the TIFF IFDs word-aligned!
 * 
 * PRECONDITION: The ProcessData file list is filled. TiffImageMetadataProcessor locates the files
 * through these entries.
 * 
 * RESULT: All TIFF images are correct and valid (hopefully!).
 * 
 * TODO WITH A LATER VERSION OF JAI_IMAGEIO: Change the TIFF header metadata without storing the
 * whole picture again.
 * 
 * @author Stefan Funk, SUB Göttingen
 * @version 2007-06-08
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 * @since 2006-06-28
 * @deprecated
 ******************************************************************************/
@Deprecated
public class TiffImageMetadataProcessor implements ActionModule {

  // CONSTANTS **********************************************************

  private final boolean VALIDATION = false;
  private final boolean REVALIDATION = true;
  private static final String VAR_SIPSOURCEASURI = "sipSourceAsUri";

  // STATE (Instance variables) ******************************************

  ProcessData processData;
  Step step;

  boolean correctInvalidTiffHeaders = false;
  private boolean verbose = false;
  private boolean separateLogging = false;
  private String separateLogfileDir = "";
  private String workDir = "";
  private String jhoveConfigFile = "";
  private long nonVerboseReportingTime = 20000;
  private Logger separateLogger;
  protected int totalAmountOfImages;
  protected int imageCount;

  // CREATION (Constructors, factory methods, static/inst init) **********

  /***************************************************************************
   * Default constructor
   **************************************************************************/
  public TiffImageMetadataProcessor() {
    super();
  }

  // MANIPULATION (Manipulation - what the object does) ******************

  /***************************************************************************
   * Starts the essential task of this class. It really does! :-D
   **************************************************************************/
  public void go() {
    String LogfileName = "";

    try {
      // Start the logger if separateLogging is enabled.
      if (this.separateLogging) {
        LogfileName = startLogging();
      } else {
        this.step.setStatus(Status.RUNNING, "Separate logging "
            + "is disabled");
      }

      // Check the LmerFileList for TIFF images and build up a new file
      // list only containing TIFF images.
      List tiffFileList = buildUpTiffFileList();

      if (tiffFileList.size() == 0) {
        log(Status.DONE, Level.WARNING, "Module completed, "
            + "no TIFF files available in this SIP");
        return;
      }

      // Process all TIFF images and return if some still are not valid.
      if (!processTiffFileList(tiffFileList)) {
        return;
      }

      // Do some logging and set status to DONE.
      log(Status.DONE, Level.INFO, "Validating and postprocessing "
          + "complete. All TIFF image headers are valid");

      if (this.separateLogging && !LogfileName.equals("")) {
        this.separateLogger.log(Level.INFO, "Logging ends here! "
            + "Hava a nice day!");
        this.separateLogger.removeHandler(this.separateLogger
            .getHandlers()[0]);

        // TODO Remove the lock file gracefully!! For the moment just
        // delete it at the end!
        File lockFile = new File(LogfileName + ".lck");
        if (lockFile.exists()) {
          lockFile.deleteOnExit();
        }
      }
    } catch (Exception e) {
      log(Status.ERROR, Level.SEVERE, "Error validating TIFF images. "
          + "System message: " + e.getMessage()
          + ". Please report to sickbay.");
      e.printStackTrace();
    }
  }

  // INTERNAL (Internal - implementation details, local classes, ...) ******

  /***************************************************************************
   * Defines and starts the logging infrastructure.
   * 
   * @return String
   **************************************************************************/
  private String startLogging() {
    FileHandler fhFile = null;

    // Get the logger and take the process name as logger name.
    this.separateLogger = Logger.getLogger(this.processData
        .getProcessName());

    // At the very beginning turn off the parent loggers console output.
    this.separateLogger.setUseParentHandlers(false);

    // Check the separateLogfileDir.
    if (this.separateLogfileDir.equals("")) {
      setSeparateLogfileDir(this.workDir);
    }

    // Create logfile name.
    String logfileName = "";
    try {
      logfileName = this.separateLogfileDir
          + FileUtils
              .getFilenamePersistentIdentifier(this.processData
                  .getProcessName())
          + "-"
          + KolibriTimestamp.getFilenameTimestamp() + ".log";
    } catch (Exception e) {
      this.step.setStatus(Status.WARNING,
          "System message: " + e.getMessage());
      e.printStackTrace();

      return null;
    }

    // Set the file and the console log handlers and assign them to our
    // defaultLogger.
    try {
      fhFile = new FileHandler(logfileName);
    } catch (IOException e) {
      this.step.setStatus(Status.WARNING, "Could not create separate "
          + "logfile, separate logging disabled",
          new File(
              this.separateLogfileDir));
      this.separateLogging = false;

      if (!new File(this.separateLogfileDir).isDirectory()) {
        this.step.setStatus(Status.WARNING, "Directory does not exist",
            new File(this.separateLogfileDir));
      } else {
        this.step.setStatus(Status.WARNING,
            "System message: " + e.getMessage());
      }

      return null;
    }

    fhFile.setFormatter(new KolibriSeparateLogFormatter());
    this.separateLogger.addHandler(fhFile);
    this.separateLogger.setLevel(Level.parse("INFO"));

    // First separate log messages.
    log(Status.RUNNING, Level.INFO, "Logging service up and running."
        + " Writing to file '" + logfileName + "'");
    this.separateLogger.log(Level.INFO, "Log output for source identified "
        + "by koLibRI as '" + this.processData.getProcessName() + "'");
    this.separateLogger.log(Level.INFO, "Path to the content files is '"
        + this.processData.getPathToContentFiles() + "'");

    return logfileName;
  }

  /***************************************************************************
   * Build up and return the TIFF file list.
   * 
   * @return A TIFF file list containing TIFF images only.
   **************************************************************************/
  private List buildUpTiffFileList() {
    ArrayList resultList;

    // Clone the LMER file list from process data.
    resultList = (ArrayList) ((ArrayList) this.processData.getFileList())
        .clone();

    // And work on the tiffFileList from now on.
    Iterator iterator = resultList.iterator();

    // Check each file for a TIFF image extension, and delete non-TIFF files
    // from the tiffFileList.
    File contentFile;
    while (iterator.hasNext()) {
      contentFile = (File) iterator.next();
      // TODO Use the SigMatch from JHOVE here! Use the AuditHandler or
      // just examine ALL files and validate those with a TIFF SigMatch.
      if (!((contentFile.getName().endsWith(".tif"))
          || (contentFile.getName().endsWith(".tiff"))
          || (contentFile.getName().endsWith(".TIF")) || (contentFile
              .getName().endsWith(".TIFF")))) {
        iterator.remove();
      }
    }

    log(Status.RUNNING, Level.INFO, "Found " + resultList.size()
        + " TIFF image" + (resultList.size() == 1 ? "" : "s")
        + " to validate");

    return resultList;
  }

  /***************************************************************************
   * @param tiffFileList
   * @return boolean
   * @throws Exception
   **************************************************************************/
  private boolean processTiffFileList(List tiffFileList) throws Exception {
    int allImages = tiffFileList.size();
    int goodImages = 0;
    int badImages = 0;
    int veryBadImages = 0;
    int tagsCorrected = 0;
    int allTagsCorrected = 0;
    File tiffFile;

    Timer timer = new Timer();
    ProgressNotificationTask pnt = new ProgressNotificationTask();

    Iterator iterator = tiffFileList.iterator();
    this.totalAmountOfImages = tiffFileList.size();

    // Just log.
    this.log(
        Status.RUNNING,
        Level.INFO,
        "Validating"
            + (TiffImageMetadataProcessor.this.correctInvalidTiffHeaders ? " and postprocessing"
                : "")
            + " TIFF images");

    // Use scheduler to log the process' progress.
    if (!this.verbose && this.totalAmountOfImages != 0) {
      pnt.setTotalAmount(this.totalAmountOfImages);
      timer.schedule(pnt, this.nonVerboseReportingTime,
          this.nonVerboseReportingTime);
    }

    // Get a new JhoveBase object to process all the files.
    JhoveBase jb = TiffIMP.initializeTheJhove(new File(this
        .getJhoveConfigFile()));

    // Process the file list.
    while (iterator.hasNext()) {
      tiffFile = (File) iterator.next();

      // Create a TiffIMP object and set all needed attributes.
      TiffIMP tiffImp = new TiffIMP(jb);
      tiffImp.setInputFile(tiffFile);
      tiffImp.setOutputFile(tiffFile);
      tiffImp.setQuiet(true);

      // Validate the TIFF image and output the Jhove output.
      boolean outcome = tiffImp.validate();
      logJhoveValidationOutcome(tiffImp, this.VALIDATION);

      // Count the images for the process notification task and set the
      // image count for the process notification task.
      this.imageCount++;
      pnt.setProgImageCount(this.imageCount);

      if (outcome) {
        // If the file is valid according to JHOVE, delete it from the
        // file list.
        iterator.remove();
        goodImages++;
      } else {
        // Correct the TIFF header if wanted.
        if (this.correctInvalidTiffHeaders) {
          tiffImp.load();
          if (this.verbose) {
            log(Status.RUNNING, Level.INFO, "  Image loaded");
          }

          tagsCorrected = tiffImp.correctHeaderMetadata();
          if (this.verbose) {
            log(Status.RUNNING, Level.INFO, "  " + tagsCorrected
                + " TIFF tag" + (tagsCorrected == 1 ? "" : "s")
                + " corrected");
          }
          allTagsCorrected += tagsCorrected;

          tiffImp.store();
          badImages++;

          // And revalidate:
          outcome = tiffImp.validate();
          logJhoveValidationOutcome(tiffImp, this.REVALIDATION);

          if (outcome) {
            iterator.remove();
            goodImages++;
          } else {
            veryBadImages++;
          }

          if (this.verbose) {
            log(Status.RUNNING, Level.INFO, "  Image stored "
                + "and revalidated -> "
                + tiffImp.getStatusString().toUpperCase());
          }
        }
      }
    }

    // Cancel the timer notification if ready.
    timer.cancel();

    // Assemble messages and give output.
    String message = allImages + " image" + (allImages == 1 ? "" : "s")
        + " validated, " + goodImages + " valid";
    if (allTagsCorrected != 0 || badImages != 0) {
      message += ", " + allTagsCorrected + " TIFF tag"
          + (allTagsCorrected == 1 ? "" : "s") + " corrected in "
          + badImages + " image" + (badImages == 1 ? "" : "s");
    }
    log(Status.RUNNING, Level.INFO, message);

    if (veryBadImages != 0) {
      message = veryBadImages + " image"
          + (veryBadImages == 1 ? "" : "s") + " still "
          + (veryBadImages == 1 ? "is" : "are")
          + " not valid and can not yet be corrected"
          + " automatically";
      log(Status.ERROR, Level.SEVERE, message);
      return false;
    }

    return true;
  }

  /***************************************************************************
   * Outcome of the TIFF image validation for one file.
   * 
   * @param tiffImp
   * @param revalidation
   * @return true if the TIFF is valid, false if not.
   **************************************************************************/
  private boolean logJhoveValidationOutcome(TiffIMP tiffImp,
      boolean revalidation) {
    boolean result = false;
    RepInfo info = tiffImp.getRepInfo();
    String status = "";
    String path = gimmyTheRelativePath(tiffImp.getInputFile());

    // Set the status.
    status = tiffImp.getStatusString();

    // Log things always at REVALIDATION time (if not valid) AND at
    // VALIDATION time, if verbose mode is set.
    if ((revalidation && info.getValid() != RepInfo.TRUE) || this.verbose) {
      this.step.setStatus(Status.RUNNING,
          gimmyTheRelativePath(tiffImp.getInputFile()) + " -> "
              + status.toUpperCase());

      if (info.getValid() != RepInfo.TRUE) {
        String mimetype = (info.getMimeType() != null ? info
            .getMimeType() : "unknown");
        String format = (info.getFormat() != null ? info.getFormat()
            : "unknown");
        String version = (info.getVersion() != null ? info.getVersion()
            : "unknown");

        this.step.setStatus(Status.WARNING, "Mimetype: " + mimetype
            + ", Format: " + format + ", Version: " + version,
            new File(path));
        if (info.getMessage().size() > 0) {
          this.step.setStatus(Status.WARNING, logJhoveMessages(info),
              new File(path));
        }
      }
    }

    // Log things at REVALIDATION time separately, if separateLogging
    // is enabled.
    if (this.separateLogging && revalidation) {
      if (info.getValid() != RepInfo.TRUE) {
        this.separateLogger.log(Level.WARNING, status.toUpperCase()
            + ":");

        // Get the source from the custom data if existing and log it.
        // TODO Is that wise here to address the
        // MonitorHotfolderLocation's attributes?
        String source = (String) this.processData.getCustomData().get(
            VAR_SIPSOURCEASURI);
        if (!source.equals("")) {
          this.separateLogger.log(Level.WARNING, "\tSource: '"
              + source + "'");
        }

        this.separateLogger.log(Level.WARNING,
            "\tFile: '" + tiffImp.getInputFile() + "'");
        this.separateLogger.log(
            Level.WARNING,
            "\tMimetype: "
                + (info.getMimeType() != null ? info
                    .getMimeType() : "unknown")
                + ", Format: "
                + (info.getFormat() != null ? info.getFormat()
                    : "unknown")
                + ", Version: "
                + (info.getVersion() != null ? info
                    .getVersion() : "unknown"));
        if (info.getMessage().size() > 0) {
          this.separateLogger.log(Level.WARNING, "\t"
              + logJhoveMessages(info));
        }
      }
    }

    // If the TIFF is well-formed and valid according to JHOVE, return
    // true.
    if ((info.getWellFormed() == 1) && (info.getValid() == 1)) {
      result = true;
    }

    return result;
  }

  /***************************************************************************
   * @param info
   * @return String
   **************************************************************************/
  private synchronized String logJhoveMessages(RepInfo info) {
    String result = "";
    String message = "";
    String messageType = "";
    String subMessage = "";
    String subMessageType = "";

    Iterator it = info.getMessage().listIterator();

    while (it.hasNext()) {
      Message messages = (Message) it.next();

      // Get the message.
      messageType = messages.getClass().getName();
      messageType = messageType
          .substring(messageType.lastIndexOf('.') + 1);
      message = messages.getMessage();

      result = "Jhove " + messageType + ": " + message;

      // Get the sub message, if existing.
      if (messages.getSubMessage() != null) {
        subMessageType = messages.getSubMessage().getClass().getName();
        subMessageType = subMessageType.substring(subMessageType
            .lastIndexOf('.') + 1);
        subMessage = messages.getSubMessage();

        result += "Jhove " + subMessageType + ": " + subMessage;
      }
    }

    return result;
  }

  /***************************************************************************
   * Just gives back the relative path of the given file based on the current SIP.
   * 
   * @param contentFile
   * @return String
   **************************************************************************/
  private String gimmyTheRelativePath(File contentFile) {
    String result = "";

    // Take the relative path from the current file for better logging.
    result = new File(this.processData.getPathToContentFiles()).toURI()
        .relativize(contentFile.toURI()).toString();

    return result;
  }

  /***************************************************************************
   * Just a small logging method to avoid code.
   * 
   * @param status
   * @param level
   * @param message
   **************************************************************************/
  private void log(int status, Level level, String message) {
    // Log the messages if necessary.
    if (this.separateLogging) {
      this.separateLogger.log(level, message);
    }
    // Log the status message.
    this.step.setStatus(status, message);
  }

  // GET & SET METHODS **************************************************

  /***************************************************************************
   * @param processData The processData to set.
   **************************************************************************/
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /***************************************************************************
   * @param step The step to set.
   **************************************************************************/
  public void setStep(Step step) {
    this.step = step;
  }

  /***************************************************************************
   * @param correctInvalidTiffHeaders Enable the TIFF header correction.
   **************************************************************************/
  public void setCorrectInvalidTiffHeaders(boolean correctInvalidTiffHeaders) {
    this.correctInvalidTiffHeaders = correctInvalidTiffHeaders;
  }

  /***************************************************************************
   * @param verbose Enable verbose mode.
   **************************************************************************/
  public void setVerbose(boolean verbose) {
    this.verbose = verbose;
  }

  /***************************************************************************
   * @param separateLogging Enable the logging.
   **************************************************************************/
  public void setSeparateLogging(boolean separateLogging) {
    this.separateLogging = separateLogging;
  }

  /***************************************************************************
   * @param separateLogfileDir The logfile directory to set.
   **************************************************************************/
  public void setSeparateLogfileDir(String separateLogfileDir) {
    this.separateLogfileDir = separateLogfileDir;
  }

  /***************************************************************************
   * @param workDir The working directory to set.
   **************************************************************************/
  public void setWorkDir(String workDir) {
    this.workDir = workDir;
  }

  /***************************************************************************
   * @param nonVerboseReportingTime The nonVerboseReportingTime to set.
   **************************************************************************/
  public void setNonVerboseReportingTime(long nonVerboseReportingTime) {
    this.nonVerboseReportingTime = nonVerboseReportingTime;
  }

  /***************************************************************************
   * @return Returns the jhoveConfigFile.
   **************************************************************************/
  public String getJhoveConfigFile() {
    return this.jhoveConfigFile;
  }

  /***************************************************************************
   * @param jhoveConfigFile The jhoveConfigFile to set.
   **************************************************************************/
  public void setJhoveConfigFile(String jhoveConfigFile) {
    this.jhoveConfigFile = jhoveConfigFile;
  }

  /***************************************************************************
   * @author funk
   **************************************************************************/
  protected class ProgressNotificationTask extends TimerTask {

    // STATE (Instance variables) ******************************************

    private int progTotalAmount = 0;
    private int progImageCount = 0;

    // MANIPULATION (Manipulation - what the object does) ******************

    /*
     * (non-Javadoc)
     * 
     * @see java.util.TimerTask#run()
     */
    @Override
    public void run() {
      TiffImageMetadataProcessor.this.step
          .setStatus(
              Status.RUNNING,
              "Validating"
                  + (TiffImageMetadataProcessor.this.correctInvalidTiffHeaders
                      ? " and postprocessing"
                      : "")
                  + ": "
                  + (this.progImageCount * 100 / this.progTotalAmount)
                  + "% (" + this.progImageCount + " of "
                  + this.progTotalAmount + ")");
    }

    /***********************************************************************
     * @return Returns the progImageCount.
     **********************************************************************/
    public int getProgImageCount() {
      return this.progImageCount;
    }

    /***********************************************************************
     * @param progImageCount The progImageCount to set.
     **********************************************************************/
    public void setProgImageCount(int progImageCount) {
      this.progImageCount = progImageCount;
    }

    /***********************************************************************
     * @return Returns the progTotalAmount.
     **********************************************************************/
    public int getTotalAmount() {
      return this.progTotalAmount;
    }

    /***********************************************************************
     * @param totalAmount The progTotalAmount to set.
     **********************************************************************/
    public void setTotalAmount(int totalAmount) {
      this.progTotalAmount = totalAmount;
    }
  }

}
