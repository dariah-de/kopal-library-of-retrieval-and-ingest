/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionmodule / Zip.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 20.05.2011 	Hausner	Changed handling of METS-File.
 * 31.08.2009	Funk	Added NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT.
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.util.FileUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;

/*******************************************************************************
 * Compresses all files in the directory PathToContentFiles to an asset named ID
 * plus Timestamp. PRECONDITION: The subdirectory named PathToContentFiles in
 * the ConfigData working directory and the ID as string in the ID field of the
 * processData's ID data are existing. RESULT: The ZIPped SIP will be existing
 * in the destination directory, and the SipLocation in processData will be set.
 * 
 * @author Ludwig
 * @version 20061130
 * @since 20060119
 ******************************************************************************/

public class Zip implements ActionModule {

	// STATE (Instance variables) *****************************************

	ProcessData processData;
	Step step;

	File srcDir;
	String destinationDir;

	// TODO Makes no sense to make this value settable, because it is not
	// settable in the FileUtils, and KolibriTimestamp classes (yet).
	// TODO Use URIs here?
	String NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT = "_";

	int compressionLevel = 9;

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * Default Constructor
	 **************************************************************************/
	public Zip() {
		super();
	}

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
	 **************************************************************************/
	public void go() {
		long startTime = System.currentTimeMillis();

		try {
			this.srcDir = new File(this.processData.getPathToContentFiles());
			File destFile = new File(this.destinationDir, getAssetName());

			// If the destination file already exists generate a new SIP name.
			while (destFile.exists()) {
				destFile = new File(this.destinationDir, getAssetName());
			}

			this.step.setStatus(Status.RUNNING, "Compressing files to asset '"
					+ destFile.getName() + "'");

			ZipOutputStream assetFile = new ZipOutputStream(
					new BufferedOutputStream(new FileOutputStream(destFile)));

			// Only needed for storing support (means no compression, I think).
			if (this.compressionLevel == 0) {
				assetFile.setMethod(ZipOutputStream.STORED);
			} else {
				// Is the default anyway, but nevertheless: set it!
				// assetFile.setMethod(ZipOutputStream.DEFLATED);
				assetFile.setMethod(ZipOutputStream.DEFLATED);

				if (this.compressionLevel != -1) {
					assetFile.setLevel(this.compressionLevel);
				}
			}

			writeSrcDirInAsset(assetFile);
			assetFile.close();

			// Set the ZIP's path.
			this.processData.setSipLocation(destFile);

		} catch (Exception e) {
			this.step.setStatus(Status.ERROR, "Asset generation failed. "
					+ "System message: " + e.getMessage());
			e.printStackTrace();
			return;
		}

		this.step.setStatus(Status.DONE, "Asset compressed in "
				+ KolibriTimestamp.getDurationInHours(System
						.currentTimeMillis()
						- startTime));
	}

	// INTERNAL (Internal - implementation details, local classes, ...) ****

	/***************************************************************************
	 * Generates the filename of the asset. It consists of the identifier and a
	 * timestamp and has less than 255 characters. TODO Determine the filename
	 * restrictions of the enviroment.
	 * 
	 * @return the filename of the asset.
	 * @throws Exception
	 **************************************************************************/
	private String getAssetName() throws Exception {
		String result = null;

		result = FileUtils.getFilenamePersistentIdentifier(this.processData
				.getMetadata().getObjectId())
				+ this.NOT_LETTER_OR_DIGIT_CHAR_REPLACEMENT
				+ KolibriTimestamp.getFilenameTimestamp() + ".zip";

		// TODO Test this!
		// We suppose that a filename with more than 255 characters is in
		// general not possible. If it has more characters cut the identifier.
		if (result.length() >= 255) {
			String id = FileUtils
					.getFilenamePersistentIdentifier(this.processData
							.getMetadata().getObjectId());
			// Cut the last characters of the ID so that the SIP's filename
			// is in the limit.
			result = id.substring(0, (id.length() - (result.length() - 255)))
					+ "_" + KolibriTimestamp.getFilenameTimestamp() + ".zip";
		}

		return result;
	}

	/***************************************************************************
	 * Write everything in the source directory into the asset file.
	 * 
	 * @param assetFile
	 *            destination file/stream
	 * @throws IOException
	 **************************************************************************/
	private void writeSrcDirInAsset(ZipOutputStream assetFile)
			throws IOException {
		File srcDirEntries[] = this.srcDir.listFiles();

		// For performance reasons "mets.xml" should be the first ZIP file
		// entry.
		File metsFile = new File(this.srcDir, "mets.xml");
		
		// As the format might be ORE the METS-file might not exist...
		if (metsFile.exists()) {
			writeFileInAsset(metsFile, assetFile);
		}
		for (int i = 0; i < srcDirEntries.length; i++) {
			if (srcDirEntries[i].isDirectory()) {
				writeDirInAsset(srcDirEntries[i], assetFile);
			} else {
				// We already added "mets.xml", so don't do it again.
				if (!srcDirEntries[i].getName().equals("mets.xml")) {
					writeFileInAsset(srcDirEntries[i], assetFile);
				}
			}
		}
	}

	/***************************************************************************
	 * Write everything in the specified directory into the asset file.
	 * 
	 * @param dir
	 *            The directory to write
	 * @param assetFile
	 *            destination file/stream
	 * @throws IOException
	 **************************************************************************/
	private void writeDirInAsset(File dir, ZipOutputStream assetFile)
			throws IOException {
		File srcDirEntries[] = dir.listFiles();
		for (int i = 0; i < srcDirEntries.length; i++) {
			if (!srcDirEntries[i].isDirectory()) {
				writeFileInAsset(srcDirEntries[i], assetFile);
			} else {
				writeDirInAsset(srcDirEntries[i], assetFile);
			}
		}

		// ZIP entry of the directory relativ to the working directory.
		ZipEntry zipEntry = new ZipEntry(dir.getAbsolutePath().substring(
				(this.srcDir.getAbsolutePath() + File.separatorChar).length())
				+ "/");

		// TODO If you want to support storing instead of compressing only, you
		// still have to set directory zipEntries to DEFLATED. Reason: unknown.
		// zipEntry.setMethod(ZipOutputStream.DEFLATED);

		// TODO Remove debug output.
		//
		// System.out.println("DIR");
		// System.out.println("comment: " + zipEntry.getComment());
		// System.out.println("compsize: " + zipEntry.getCompressedSize());
		// System.out.println("crc: " + zipEntry.getCrc());
		// System.out.println("extra: " + zipEntry.getExtra());
		// System.out.println("method: " + zipEntry.getMethod());
		// System.out.println("name: " + zipEntry.getName());
		// System.out.println("size: " + zipEntry.getSize());
		// System.out.println("time: " + zipEntry.getTime());
		// System.out.println();
		//
		// TODO Remove debug output.

		assetFile.putNextEntry(zipEntry);
		assetFile.closeEntry();
	}

	/**************************************************************************
	 * Only needed for storing support.
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 **************************************************************************/
	protected long crc32(FileInputStream in) throws IOException {
		CRC32 checksum = new CRC32();
		byte md[] = new byte[8192];
		for (int n = 0; (n = in.read(md)) > -1;) {
			checksum.update(md, 0, n);
		}
		return checksum.getValue();
	}

	/***************************************************************************
	 * Puts a file into a zip outputstream.
	 * 
	 * @param file
	 *            The Source file.
	 * @param assetFile
	 *            The archive outputstream.
	 * @throws IOException
	 *             If input or output failed.
	 **************************************************************************/
	private void writeFileInAsset(File file, ZipOutputStream assetFile)
			throws IOException {
		BufferedInputStream fileStream = new BufferedInputStream(
				new FileInputStream(file));

		// ZIP entry of the file relativ to the working directory.
		ZipEntry zipEntry = new ZipEntry(file.getAbsolutePath().substring(
				(this.srcDir.getAbsolutePath() + File.separatorChar).length()));

		// Only needed for storing support.
		if (this.compressionLevel == 0) {
			zipEntry.setSize(file.length());
			try {
				FileInputStream crcStream = new FileInputStream(file);
				zipEntry.setCrc(crc32(crcStream));
				crcStream.close();
			} catch (Exception e) {
				// TODO
				e.printStackTrace();
			}
		}

		// TODO Remove debug output.
		//
		// System.out.println("FILE");
		// System.out.println("comment: " + zipEntry.getComment());
		// System.out.println("compsize: " + zipEntry.getCompressedSize());
		// System.out.println("crc: " + zipEntry.getCrc());
		// System.out.println("extra: " + zipEntry.getExtra());
		// System.out.println("method: " + zipEntry.getMethod());
		// System.out.println("name: " + zipEntry.getName());
		// System.out.println("size: " + zipEntry.getSize());
		// System.out.println("time: " + zipEntry.getTime());
		// System.out.println();
		//
		// TODO Remove debug output.

		// Compress file.
		assetFile.putNextEntry(zipEntry);

		final int BUFFER_LENGTH = 1024 * 1024;
		byte[] buffer = new byte[BUFFER_LENGTH];

		int bytesRead = fileStream.read(buffer);
		while (bytesRead > 0) {
			assetFile.write(buffer, 0, bytesRead);
			bytesRead = fileStream.read(buffer);
		}

		assetFile.closeEntry();
		fileStream.close();
	}

	// GET & SET METHODS **************************************************

	/***************************************************************************
	 * @param _destinationDir
	 *            The destinationDir to set.
	 **************************************************************************/
	public void setDestinationDir(String _destinationDir) {
		this.destinationDir = _destinationDir;
	}

	/***************************************************************************
	 * @param _processData
	 *            The processData to set.
	 **************************************************************************/
	public void setProcessData(ProcessData _processData) {
		this.processData = _processData;
	}

	/***************************************************************************
	 * @param _step
	 *            The step to set.
	 **************************************************************************/
	public void setStep(Step _step) {
		this.step = _step;
	}

	/***************************************************************************
	 * @return the compression level
	 **************************************************************************/
	public int getCompressionLevel() {
		return this.compressionLevel;
	}

	/***************************************************************************
	 * Sets the compression level. 9 is best and slowest, 0 worst and fastest. 9
	 * is default.
	 * 
	 * @param _compressionLevel
	 **************************************************************************/
	public void setCompressionLevel(int _compressionLevel) {
		if (this.compressionLevel > 9 || this.compressionLevel < 0) {
			this.compressionLevel = 9;
		} else {
			this.compressionLevel = _compressionLevel;
		}
	}

}
