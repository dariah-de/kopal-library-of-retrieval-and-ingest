/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionModules / CleanUpFiles.java
 *  
 * Copyright 2005-2009 by Project kopal
 * Copyright 2010 by Project DP4lib
 * 
 * http://kopal.langzeitarchivierung.de
 * http://dp4lib.langzeitarchivierung.de
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.11.2010	Funk	Added new header.
 * 				Funk	Refactored the CleanPathToContentFiles() and
 * 						DeletePathToContentFiels() methods in here.
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import java.io.File;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.util.FileUtils;

/*******************************************************************************
 * <p>
 * Deletes the SIP in the SIP location (the dest dir at the moment) and an
 * unpacked mets.xml (named zipFileName with .xml instead of .zip like produced
 * by UnzipMets) in the temp dir.
 * </p>
 * 
 * @author Ludwig
 * @version 2010-11-10
 * @since 2007-07-10
 ******************************************************************************/

public class CleanUpFiles implements ActionModule {

	// STATE (Instance variables) *****************************************

	ProcessData		processData;
	Step			step;

	private String	tempDir;
	private boolean	deletePathToContentFiles	= false;
	private boolean	cleanPathToContentFiles		= false;
	private boolean	deleteDestinationSip		= false;

	// CREATION (Constructors, factory methods, static/inst init) **********

	/** Constructor */
	public CleanUpFiles() {
		super();
	}

	// MANIPULATION (Manipulation - what the object does) ******************

	/**
	 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
	 */
	@Override
  public void go() {

		// Delete SIP and METS.
		if (this.deleteDestinationSip) {
			this.step.setStatus(Status.RUNNING,
					"Deleting SIP and mets.xml in folder '" + this.tempDir
							+ "'");

			File tempSip = this.processData.getSipLocation();
			String sipName = tempSip.getName();
			File tempMets = null;

			// Determine the name of the mets file, depends on the file format.
			if (sipName.endsWith(".zip") || sipName.endsWith(".tar")) {
				tempMets = new File(this.tempDir, sipName.substring(0,
						sipName.length() - 4)
						+ ".xml");
			} else if (sipName.endsWith(".tar.gz")) {
				tempMets = new File(this.tempDir, sipName.substring(0,
						sipName.length() - 7)
						+ ".xml");
			} else {
				this.step
						.setStatus(Status.ERROR,
								"Unknown SIP file format. Please delete SIP and its METS file manually");
				return;
			}

			// Delete and check if things were deleted correctly.
			if (tempSip.delete()) {
				this.step.setStatus(Status.RUNNING, "Deletion of SIP complete");
			} else {
				this.step
						.setStatus(Status.ERROR,
								"Deletion of SIP failed. Check the reason and delete SIP manually");
				return;
			}

			if (tempMets.delete()) {
				this.step.setStatus(Status.RUNNING,
						"Deletion of METS file complete");
			} else if (!tempMets.exists()) {
				this.step.setStatus(Status.RUNNING,
						"No METS file for deletion available");
			} else {
				this.step
						.setStatus(Status.ERROR,
								"Deletion of METS file failed. Check the reason and delete file manually");
				return;
			}
		}

		// Delete Path to content files.
		if (this.deletePathToContentFiles) {
			this.step.setStatus(Status.RUNNING,
					"Recursively deleting temporary content file directory '"
							+ this.processData.getPathToContentFiles() + "'");
			FileUtils.deleteDir(new File(this.processData
					.getPathToContentFiles()));

			this.step.setStatus(Status.RUNNING, "Done");
		}

		// Delete Content file directory.
		if (this.cleanPathToContentFiles) {
			this.step.setStatus(
					Status.RUNNING,
					"Deleting files and folders in '"
							+ this.processData.getPathToContentFiles() + "'");
			FileUtils.deleteFiles(new File(this.processData
					.getPathToContentFiles()));

			this.step.setStatus(Status.RUNNING, "Done");
		}

		if (!this.cleanPathToContentFiles && !this.deleteDestinationSip
				&& !this.deletePathToContentFiles) {
			this.step.setStatus(Status.DONE, "Nothing deleted");
			return;
		}

		this.step.setStatus(Status.DONE, "Everything cleaned up");
	}

	// GET & SET METHODS *******************************************************

	/***************************************************************************
	 * @param tempDir
	 *            The tempDir to set.
	 **************************************************************************/
	public void setTempDir(String tempDir) {
		this.tempDir = tempDir;
	}

	/***************************************************************************
	 * @param processData
	 *            The processData to set.
	 **************************************************************************/
	@Override
  public void setProcessData(ProcessData processData) {
		this.processData = processData;
	}

	/***************************************************************************
	 * @param step
	 *            The step to set.
	 **************************************************************************/
	@Override
  public void setStep(Step step) {
		this.step = step;
	}

	/**
	 * @param deletePathToContentFiles
	 */
	public void setDeletePathToContentFiles(boolean deletePathToContentFiles) {
		this.deletePathToContentFiles = deletePathToContentFiles;
	}

	/**
	 * @param cleanPathToContentFiles
	 */
	public void setCleanPathToContentFiles(boolean cleanPathToContentFiles) {
		this.cleanPathToContentFiles = cleanPathToContentFiles;
	}

	/**
	 * @param deleteDestinationSip
	 */
	public void setDeleteDestinationSip(boolean deleteDestinationSip) {
		this.deleteDestinationSip = deleteDestinationSip;
	}

}
