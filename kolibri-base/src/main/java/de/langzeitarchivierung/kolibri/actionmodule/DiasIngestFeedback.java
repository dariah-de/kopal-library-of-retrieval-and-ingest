/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Ludwig
 * @author Stefan E. Funk, SUB Göttingen
 */

package de.langzeitarchivierung.kolibri.actionmodule;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.ingest.DiasIngest;
import de.langzeitarchivierung.kolibri.ingest.DiasIngestMetadataResponse;
import de.langzeitarchivierung.kolibri.ingest.DiasIngestResponse;
import de.langzeitarchivierung.kolibri.util.Configurator;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2019-11-27 - Funk - Adapted to GWDG koala service.
 * 
 * 2007-07-10 - koLibRI version 1.0
 */

/**
 * <p>
 * Checks if the SIP was successfully ingested and updates the database ingest information.
 * </p>
 * 
 * @version 2019-11-28
 * @since 2007-07-10
 */

public class DiasIngestFeedback implements ActionModule {

  // **
  // FINALS
  // **

  public static final String ID_DIASTICKETID = "DIASTICKETID";

  // **
  // STATE (Instance variables)
  // **

  ProcessData processData;
  Step step;

  private long lastCheck = 0;
  private long waitUntilNextTry = 10000;

  /**
   * 
   */
  public DiasIngestFeedback() {
    super();
  }

  // **
  // MANIPULATION (Manipulation - what the object does)
  // **

  /**
   *
   */
  @Override
  public void go() {

    String ticketId;

    // If we checked the DIAS feedback less than waitUntilNextTry seconds ago, it is obvious that we
    // have not much to do. So we have to sleep().
    if ((System.currentTimeMillis() - this.lastCheck) < this.waitUntilNextTry) {
      try {
        Thread.sleep(this.waitUntilNextTry);
      } catch (Exception e) {
        this.step.setStatus(Status.WARNING,
            "Error while waiting. System message: " + e.getMessage());
      }
    }

    this.lastCheck = System.currentTimeMillis();

    // Get the ticket ID from the custom ID hash table and check for NULL value.
    ticketId = (String) this.processData.getCustomIds().get(ID_DIASTICKETID);
    if (ticketId == null) {
      this.step.setStatus(Status.ERROR, "Ticket ID value is NULL");
      return;
    }

    // Start a DIAS ingest status request using the ticket ID.
    DiasIngest di = new DiasIngest();

    try {
      Configurator.configure(di);
    } catch (InvocationTargetException | IllegalAccessException e) {
      this.step.setStatus(Status.ERROR,
          "Error configuring DiasIngest object. System message: " + e.getMessage());
      return;
    }

    // Request the SIP's ingest status.
    DiasIngestResponse dir = new DiasIngestResponse();
    try {
      String diasResponse = di.requestSipIngestStatus(ticketId);
      dir = DiasIngest.parseDiasResponse(new StringReader(diasResponse));
    } catch (IOException | SAXException e) {
      this.step.setStatus(Status.ERROR,
          "Error parsing SIP ingest repsonse. System message: " + e.getMessage());
      return;
    }

    // Get the metadata response. We assume we only get one element in the response.
    DiasIngestMetadataResponse dimr = new DiasIngestMetadataResponse();
    dimr = dir.getMetadata().get(0);

    // First check if the state is "done"...
    if (dir.getMetadata().get(0).getState().equals(DiasIngestMetadataResponse.STATE_DONE)) {
      try {
        this.step.setStatus(Status.RUNNING,
            "Ingest complete for SIP " + this.processData.getMetadata().getObjectId()
                + ". DIAS says ingest state is: " + dimr.getState());

      } catch (Exception e) {
        this.step.setStatus(Status.ERROR,
            "Error updating database. System message: " + e.getMessage());
        return;
      }
    }

    // ...then at last check if state is "error"!
    else if (dir.getMetadata().get(0).getState().equals(DiasIngestMetadataResponse.STATE_ERROR)) {

      // Status must be ERROR here, not RUNNING!
      this.step.setStatus(Status.ERROR, "DIAS says ingest state is " + dimr.getState() + ": "
          + dimr.getSubstate() + ", " + dimr.getSupplementaryInformation());

      return;
    }

    // ...at last check if type is "busy"...
    else if (dir.getResponseType().equals(DiasIngestResponse.RESPONSE_TYPE_BUSY)) {
      // ...and has ingest state...
      if (dimr.getState() != DiasIngestResponse.NOT_IN_DIAS_RESPONSE) {
        this.step.setStatus(Status.TODO, "Ingest is not yet complete. DIAS says ingest state is: "
            + dimr.getState() + ". We try again in " + this.waitUntilNextTry + " seconds.");
      }

      // ...if not, just log the alert and try again later...
      else {
        this.step.setStatus(Status.TODO, "Ingest is not yet complete. DIAS submits ingest alert: "
            + dimr.getAlert() + ". We try again in " + this.waitUntilNextTry + " seconds.");
      }
    }
  }

  // **
  // GET & SET METHODS
  // **

  /**
   *
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   *
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

}
