/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionModules / CleanUpHotfolder.java
 *  
 * Copyright 2010 by Project DP4lib
 * 
 * http://dp4lib.langzeitarchivierung.de
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.11.2010	First version.
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import java.io.File;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.util.FileUtils;

/*******************************************************************************
 * <p>
 * Deletes the file or folder taken from the hotfolder.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2010-11-10
 * @since 2010-11-10
 ******************************************************************************/

public class CleanUpHotfolder implements ActionModule {

	// STATE (Instance variables) *****************************************

	ProcessData		processData;
	Step			step;

	private String	hotfolderDir	= null;

	// CREATION (Constructors, factory methods, static/inst init) **********

	/**
	 * <p>
	 * Constructor
	 * </p>
	 */
	public CleanUpHotfolder() {
		super();
	}

	// MANIPULATION (Manipulation - what the object does) ******************

	/**
	 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
	 */
	@Override
  public void go() {
		if (this.hotfolderDir == null || this.hotfolderDir.equals("")) {
			this.step.setStatus(Status.ERROR,
					"Hotfolder directory is not configured");
		}

		// Delete hotfolder folder recursively.
		File fileToDelete = new File(this.hotfolderDir + File.separatorChar
				+ this.processData.getProcessName());

		this.step.setStatus(Status.RUNNING,
				"Recursively deleting files from the hotfolder: "
						+ fileToDelete.getAbsolutePath() + "'");

		// If directory, delete files.
		if (fileToDelete.isDirectory()) {
			FileUtils.deleteFiles(fileToDelete);
		}

		// Then delete folder (or file only).
		boolean result = fileToDelete.delete();

		if (result) {
			this.step.setStatus(Status.DONE, "Done");
		} else {
			this.step.setStatus(Status.WARNING,
					"File '" + fileToDelete.getAbsolutePath()
							+ "' was not deleted");
		}
	}

	// GET & SET METHODS *******************************************************

	/***************************************************************************
	 * @param processData
	 *            The processData to set.
	 **************************************************************************/
	@Override
  public void setProcessData(ProcessData processData) {
		this.processData = processData;
	}

	/***************************************************************************
	 * @param step
	 *            The step to set.
	 **************************************************************************/
	@Override
  public void setStep(Step step) {
		this.step = step;
	}

	/**
	 * @param hotfolderDir
	 */
	public void setHotfolderDir(String hotfolderDir) {
		this.hotfolderDir = hotfolderDir;
	}

}
