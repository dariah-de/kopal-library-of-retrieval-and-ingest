package de.langzeitarchivierung.kolibri.actionmodule;

import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.xmlbeans.XmlObject;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.formats.MetadataFormat;
import de.langzeitarchivierung.kolibri.formats.TechnicalMetadata;
import de.langzeitarchivierung.kolibri.formats.TechnicalMetadataJHOVEImpl;
import de.langzeitarchivierung.kolibri.util.Configurator;
import de.langzeitarchivierung.kolibri.util.HTMLUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import edu.harvard.hul.ois.jhove.Module;
import edu.harvard.hul.ois.jhove.RepInfo;
import edu.harvard.hul.ois.xml.ns.jhove.JhoveDocument;

/**
 * @author hausner
 */
public class GenericMetadataGenerator extends MetadataGenerator {

	public void go() {
		long startTime = System.currentTimeMillis();
		this.step.setStatus(Status.RUNNING, "Initializing JHOVE");
		iniJhove();
		// If an Error occured during initialization, quit this module
		if (this.step.getStatus().getType() == Status.ERROR) {
			return;
		}
		this.step.setStatus(Status.RUNNING, "Creating technical metadata");
		// Create a new KopalHandler
		KolibriHandler kopalhandler = new KolibriHandler();
		try {
			Configurator.configure(kopalhandler, this);
		} catch (Exception e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR, "Could not configure "
					+ "KopalHandler: " + e.getMessage());
			return;
		}

		// Go through the filelist of the ProcessData and process each of the
		// files to generate metadata for it
		List filelist = this.processData.getFileList();
		if (filelist.size() > 0) {
			for (int i = 0; i < filelist.size(); i++) {
				File file = (File) filelist.get(i);
				// Set the contentfile for the handler
				kopalhandler.setContentFile(file);
				// Process the file with JHOVE
				try {
					if (!process(file, kopalhandler)) {
						this.step.setStatus(Status.ERROR,
								"File '" + file.getName()
										+ "' could not be processed", file);
						return;
					}
				} catch (Exception e) {
					e.printStackTrace();
					this.step
							.setStatus(
									Status.ERROR,
									"File '"
											+ file.getName()
											+ "' could not be processed. System message: "
											+ e.getMessage(), file);
					return;
				}
			}
		} else {
			// No files to process. Error!
			this.step.setStatus(Status.ERROR, "No file to analyze!");
			return;
		}
		this.step.setStatus(
				Status.DONE,
				"Metadata successfully created in "
						+ KolibriTimestamp.getDurationInHours(System
								.currentTimeMillis() - startTime));
	}

	protected boolean process(File file, KolibriHandler handler)
			throws Exception {
		long lastModified = -1;
		// Create a RepInfo for this file.
		RepInfo info = new RepInfo(file.getAbsolutePath());
		if (!file.exists()) {
			this.step.setStatus(Status.ERROR, "File not found", file);
			return false;
		} else if (!file.isFile() || !file.canRead()) {
			this.step.setStatus(Status.ERROR, "File cannot be read", file);
			return false;
		}
		// If the handler can process the file...
		else if (handler.okToProcess(file.getAbsolutePath())) {
			// ...read its size...
			info.setSize(file.length());
			// ...and read the date of the last modification.
			if (lastModified < 0) {
				lastModified = file.lastModified();
			}
			info.setLastModified(new Date(lastModified));
			/*
			 * JHOVE Comment: Invoke all modules until one returns well formed.
			 * If a module doesn't know how to validate, we don't want to throw
			 * arbitrary files at it, so we'll skip it.
			 */
			Iterator iter = this._moduleList.iterator();
			while (iter.hasNext()) {
				Module mod = (Module) iter.next();
				RepInfo infc = (RepInfo) info.clone();
				if (mod.hasFeature("edu.harvard.hul.ois.jhove.canValidate")) {
					try {
						if (!processFile(mod, false, file, infc)) {
							return false;
						}
						if (infc.getWellFormed() == RepInfo.TRUE) {
							info.copy(infc);
							break;
						}
						// If the modules does not tag the file as
						// "well-formed", check, if the file signature matched,
						// and log the possible match and maybe existing
						// messages.
						if (infc.getSigMatch().contains(mod.getName())) {
							if (!infc.getMessage().isEmpty()) {
								this.step.setStatus(Status.WARNING, "File '"
										+ file.getName()
										+ "' is NOT well-formed for module "
										+ mod.getName()
										+ ". Filetype was presumed to be "
										+ infc.getFormat()
										+ ", but the according module"
										+ " printed some warnings", file);
								logJhoveMessages(infc.getMessage());
							} else {
								this.step.setStatus(Status.WARNING, "File '"
										+ file.getName()
										+ "' is NOT well-formed for module "
										+ mod.getName()
										+ ". Filetype was presumed to be "
										+ infc.getFormat()
										+ ", but the according module "
										+ "couldn't finish", file);
							}
							if (this.errorIfSigmatchDiffers) {
								// TODO: Make option available to only ignore
								// sigmatch errors in HTML files, as they
								// are never valid!
								return false;
							}
						}
						// -----------------------------------
						// JHOVE Comment:
						// We want to know what modules matched the
						// signature, so we force the sigMatch
						// property to be persistent.
						info.setSigMatch(infc.getSigMatch());
					} catch (Exception e) {
						/*
						 * JHOVE Comment: The assumption is that in trying to
						 * analyze the wrong type of file, the module may go off
						 * its track and throw an exception, so we just continue
						 * on to the next module.
						 */
						continue;
					}
				}
			}
			// Finally write all the gathered information into the LmerFile
			// objects.
			info.show(handler);
			// NOTE: If you get strange XML errors here (e.g. not allowed XML
			// chars), please check the validity of the techMD section and then
			// extend the correctUmlautsInMixSections() method in HTMLUtils with
			// the faulty char.
			JhoveDocument testJhove = JhoveDocument.Factory.parse(HTMLUtils
					.correctUmlautsInMixSections(handler.getXmlData()));
			MetadataFormat metadataFormat = this.processData.getMetadata();

			// Create technical metadata object.
			TechnicalMetadata techMd = new TechnicalMetadataJHOVEImpl(
					testJhove.toString());

			// Add techMd to the metadata format.
			metadataFormat.addFile(file, techMd);
		}
		return true;
	}

}
