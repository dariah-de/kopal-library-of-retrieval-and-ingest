/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionmodule / MetadataExtractorBase.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;

/*******************************************************************************
 * Base class to be extended from all different metadata extractors.
 * 
 * @author Kadir Karaca Koçer, German National Library
 * @version 20061130
 * @see de.langzeitarchivierung.kolibri.actionmodule.MetadataGenerator
 * @since 20050811
 ******************************************************************************/

public class MetadataExtractorBase implements ActionModule {

	// STATE (Instance variables) *****************************************

	protected Step			step;
	protected ProcessData	processData;

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * Does the main job - must be overwritten.
	 **************************************************************************/
	public void go() {
		this.step.setStatus(Status.RUNNING, "MetadataExtractorBase is started");
		this.step.setStatus(Status.DONE, "MetadataExtractorBase is done");
	}

	// GET & SET METHODS **************************************************

	/***************************************************************************
	 * @param _processData
	 *            The processData to set.
	 **************************************************************************/
	public void setProcessData(ProcessData _processData) {
		this.processData = _processData;
	}

	/***************************************************************************
	 * @param _step
	 *            The step to set.
	 **************************************************************************/
	public void setStep(Step _step) {
		this.step = _step;
	}

}
