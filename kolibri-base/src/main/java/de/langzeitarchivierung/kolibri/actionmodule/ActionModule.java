/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionmodule / ActionModule.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 * 
 * 
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Step;

/*******************************************************************************
 * <p>
 * The action which a node of a policy tree refers to.
 * </p>
 * 
 * <p>
 * The module should immediately set its status to RUNNING. If everything else
 * has run successful and the module has done its work, it must set it's status
 * to DONE as the final action. All output should be made by using
 * step.setStatus(). If a module can't proceed and wants to wait for an action,
 * another module should be allowed to proceed meanwhile. This is done by
 * setting the status back to TODO again, so that it is invoked again later.
 * Other states are CANCELLED, WARNING and ERROR. Only the ERROR state is logged
 * with loglevel SEVERE, WARNING is logged as loglevel WARNING.
 * </p>
 * 
 * @author Ludwig
 * @version 2011-10-04
 * @since 2005-06-23
 * @see de.langzeitarchivierung.kolibri.Status
 * @see de.langzeitarchivierung.kolibri.Step#setStatus(int, String)
 ******************************************************************************/

public interface ActionModule {

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * <p>
	 * This method is called when all previous nodes in the policy tree ran
	 * succesfully.
	 * </p>
	 * 
	 * <p>
	 * The module should immediately sets it's status to RUNNING, like
	 * step.setStatus(Status.RUNNING, "This module is doing something"). If
	 * everything else has run successfully and the module has done it's work,
	 * it must set it's status to DONE as the final action, like
	 * step.setStatus(Status.DONE, "This module is done");
	 * </p>
	 **************************************************************************/
	public void go();

	/***************************************************************************
	 * <p>
	 * Set method for the Step object.
	 * </p>
	 * 
	 * @param step
	 *            - The Step to set
	 **************************************************************************/
	public void setStep(Step step);

	/***************************************************************************
	 * <p>
	 * Set method for the ProcessData object
	 * </p>
	 * 
	 * @param processData
	 *            - The ProcessData to set
	 **************************************************************************/
	public void setProcessData(ProcessData processData);

}
