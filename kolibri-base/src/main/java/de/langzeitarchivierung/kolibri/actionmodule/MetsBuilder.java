/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionmodule / MetsBuilder.java
 * 
 * Copyright 2005-2007 by Project kopal
 *
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 *******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 *************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import java.io.File;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;

/*******************************************************************************
 * Class to generate and save Mets Objects. We are using XMLBeans to generate
 * the METS file.
 * 
 * @author Kadir Karaca Koçer, German National Library
 * @version 20070710
 * @since 20050721
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 ******************************************************************************/

public class MetsBuilder implements ActionModule {

	// STATE (Instance variables) *****************************************

	protected Step			step;
	protected ProcessData	processData;

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * Just saves the Uof object created with XMLBeans.
	 **************************************************************************/
	public void go() {
		this.step.setStatus(Status.RUNNING, "MetsBuilder started");
		try {
			this.processData.getMetadata().saveMetadataFormat(
					new File(this.processData.getPathToContentFiles(),
							"mets.xml"));
		} catch (Exception e) {
			this.step.setStatus(Status.ERROR,
					"MetsBuilder Error: " + e.getMessage());
			e.printStackTrace();
		}

		this.step.setStatus(Status.DONE, "MetsBuilder is done");
	}

	// GET & SET METHODS **************************************************

	/***************************************************************************
	 * @param _step
	 *            The step to set.
	 **************************************************************************/
	public void setStep(Step _step) {
		this.step = _step;
	}

	/***************************************************************************
	 * @param _processdata
	 *            The processData to set.
	 **************************************************************************/
	public void setProcessData(ProcessData _processdata) {
		this.processData = _processdata;
	}

}
