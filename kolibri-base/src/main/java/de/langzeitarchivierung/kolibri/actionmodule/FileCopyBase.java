/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionmodule / FileCopyBase.java
 * 
 * Copyright 2005-2007 by Project kopal
 *
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 *******************************************************************************
 *
 *	CHANGELOG
 *
 *	2014-07-17	Funk	Removed single file copying extra handling due to wrong
 *						source file setting.
 *	2012-05-07	Funk	Added duration to logging.
 * 	2011-05-25	Funk	Added ignoreHiddenFiles field and hidden file
 * 						FileFilter.
 *	2008-04-03	Funk	File copy notifications only are logged, if more than
 * 						one file shall be copied.
 * 	2007-07-10	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.util.FileUtils;
import de.langzeitarchivierung.kolibri.util.HiddenFileFilter;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;

/*******************************************************************************
 * <p>
 * Actionmodule that copies all the files in this path to a new directory in the
 * temp directory named with the current timestamp in milliseconds. It then sets
 * the path to the content files to this new temp dir.
 * </p>
 * 
 * <p>
 * Precondition: a set path to content files
 * </p>
 * 
 * <p>
 * Result: A new temporary directory in the temp directory, with the name of the
 * current timestamp and the path to the content files set to this new temporary
 * directory
 * </p>
 * 
 * @author Matthias Neubauer, German National Library
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2015-01-27
 * @since 2005-09-27
 * @see java.io.File
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 ******************************************************************************/

public class FileCopyBase implements ActionModule {

	// STATE (Instance variables) ****************************************

	protected ProcessData	processData;
	protected Step			step;
	protected boolean		ignoreHiddenFiles			= false;
	protected String		tempDir;
	protected int			totalFileAmount				= 0;
	protected int			filesCopied					= 0;
	protected long			fileCopyNotificationTime	= 10000;
	protected String		defaultProcessStarter;

	// MANIPULATION (Manipulation - what the object does) ****************

	/***************************************************************************
	 * <p>
	 * Starts the essential task of this class.
	 * </p>
	 **************************************************************************/
	@Override
  public void go() {

		// Set start time.
		long startTime = System.currentTimeMillis();

		try {
			copyFiles();
		} catch (Exception e) {
			this.step.setStatus(Status.ERROR, "Error during file copy! "
					+ "Please check if your ProcessStarter '"
					+ this.defaultProcessStarter + "' fits to this '"
					+ this.getClass().getName() + "' class. System message: "
					+ e.getMessage());
			e.printStackTrace();

			return;
		}

		// Log duration and set status to DONE.
		String duration = KolibriTimestamp.getDurationWithMillis(System
				.currentTimeMillis() - startTime);
		this.step.setStatus(Status.DONE, "Filecopy completed in " + duration);
	}

	// INTERNAL (Internal - implementation details, local classes, ...) **

	/***************************************************************************
	 * <p>
	 * Goes through the list of files in the path to the content files, invokes
	 * the recurse method for these files and folders, and finally sets the path
	 * to content files to the newly created temp directory.
	 * </p>
	 * 
	 * @throws Exception
	 **************************************************************************/
	protected void copyFiles() throws Exception {

		// Check if path to content files is not null.
		if (this.processData.getPathToContentFiles() == null) {
			throw new FileNotFoundException(
					"Path to content files is not specified");
		}

		// Check if source file does exist.
		File source = new File(this.processData.getPathToContentFiles());
		if (!source.exists()) {
			throw new FileNotFoundException(
					"Given path to content files does not exist");
		}

		this.step.setStatus(Status.RUNNING, "Transfering files from '"
				+ this.processData.getPathToContentFiles()
				+ "' to temporary directory");

		// Add a folder named by the current timestamp to the path.
		File destination = new File(this.tempDir
				+ File.separatorChar
				+ java.lang.Long.toString(System.currentTimeMillis())
				+ "_"
				+ FileUtils.getFilenamePersistentIdentifier(this.processData
						.getProcessName()));
		destination.mkdir();

		// Get the total file amount.
		this.totalFileAmount = getCompleteFileAmount(source);

		// Copy all files and folders (or the single file) to the temp
		// directory. Show a notification every fileCopyNotificationTime
		// seconds, until status is set to DONE.
		Timer timer = new Timer();
		timer.schedule(new ProgressNotificationTask(), 0,
				this.fileCopyNotificationTime);

		File filelist[] = source.listFiles(new HiddenFileFilter());

		if (filelist == null && source.isFile()) {
			recurseCopyFiles(source, destination);
		} else {
			for (int i = 0; i < filelist.length; i++) {
				recurseCopyFiles(filelist[i], destination);
			}
		}

		// Stop the timer if completed copying.
		timer.cancel();

		// Just log.
		this.step.setStatus(Status.RUNNING, "Files copied: 100% ("
				+ this.totalFileAmount + " of " + this.totalFileAmount + ")");
		this.step.setStatus(Status.RUNNING,
				"Re-setting path to content files to new temp direction");

		// Set the new path to the content files.
		this.processData.setPathToContentFiles(destination.getAbsolutePath()
				+ File.separatorChar);
	}

	/***************************************************************************
	 * <p>
	 * Goes recursively through the directories and invokes copy method for
	 * files.
	 * </p>
	 * 
	 * @param source
	 *            - The folder containing the source data
	 * @param destination
	 *            - The destination folder
	 * @throws Exception
	 **************************************************************************/
	protected void recurseCopyFiles(File source, File destination)
			throws Exception {

		if (source.isDirectory()) {
			File[] filelist = source.listFiles(new HiddenFileFilter());
			File newdir = new File(destination, source.getName());

			// Create a new directory in the destination folder.
			newdir.mkdir();
			for (int i = 0; i < filelist.length; i++) {
				// Recurse through the directory.
				recurseCopyFiles(filelist[i], newdir);
			}
		}
		if (source.isFile()) {
			File destfile = new File(destination, source.getName());

			copyFile(source, destfile);
		}
	}

	/***************************************************************************
	 * <p>
	 * Copies a single file from a given source to a given destination.
	 * </p>
	 * 
	 * <p>
	 * NOTE: Not moved to util.FileUtils, because of the filesCopied value.
	 * </p>
	 * 
	 * @param source
	 *            - The source file
	 * @param destination
	 *            - The destination folder
	 * @throws Exception
	 **************************************************************************/
	protected void copyFile(File source, File destination)
			throws FileNotFoundException, IOException {

		// Open input- and output streams for the source and destination
		// file.
		FileInputStream fis = new FileInputStream(source);
		FileOutputStream fos = new FileOutputStream(destination);

		// Copy the source file to the destination file through a buffer.
		byte[] buf = new byte[1048576];
		int i = 0;
		while ((i = fis.read(buf)) != -1) {
			fos.write(buf, 0, i);
		}

		// Close the Streams.
		fis.close();
		fos.close();
		this.filesCopied++;
	}

	/***************************************************************************
	 * <p>
	 * Computes the amount of files contained in a given directory recursively.
	 * </p>
	 * 
	 * @param currentFile
	 * @return The file amount of course :) What else?
	 **************************************************************************/
	protected int getCompleteFileAmount(File currentFile) {

		// If it is a file, directly return 1.
		if (currentFile.isFile()) {
			return 1;
		}

		// Otherwise get the file list and go throught all directories and
		// add the file amount.
		File fileList[] = currentFile.listFiles(new HiddenFileFilter());

		int fileAmount = 0;
		for (int i = 0; i < fileList.length; i++) {
			if (fileList[i].isDirectory()) {
				// Count folder as file, add 1 first.
				fileAmount += 1 + getCompleteFileAmount(fileList[i]);
			} else {
				fileAmount++;
			}
		}

		return fileAmount;
	}

	// GET & SET METHODS **************************************************

	/***************************************************************************
	 * @param tempDir
	 *            The tempDir to set.
	 **************************************************************************/
	public void setTempDir(String tempDir) {
		this.tempDir = tempDir;
	}

	/***************************************************************************
	 * @param processData
	 *            The processData to set.
	 **************************************************************************/
	@Override
  public void setProcessData(ProcessData processData) {
		this.processData = processData;
	}

	/***************************************************************************
	 * @param step
	 *            The step to set.
	 **************************************************************************/
	@Override
  public void setStep(Step step) {
		this.step = step;
	}

	/***************************************************************************
	 * @param fileCopyNotificationTime
	 **************************************************************************/
	public void setFileCopyNotificationTime(long fileCopyNotificationTime) {
		this.fileCopyNotificationTime = fileCopyNotificationTime;
	}

	/***************************************************************************
	 * @param defaultProcessStarter
	 *            Default process starter.
	 **************************************************************************/
	public void setDefaultProcessStarter(String defaultProcessStarter) {
		this.defaultProcessStarter = defaultProcessStarter;
	}

	/**
	 * @param ignoreHiddenFiles
	 */
	public void setIgnoreHiddenFiles(boolean ignoreHiddenFiles) {
		this.ignoreHiddenFiles = ignoreHiddenFiles;
	}

	/***************************************************************************
	 * <p>
	 * Just gives notification log output.
	 * </p>
	 **************************************************************************/
	protected class ProgressNotificationTask extends TimerTask {

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.TimerTask#run()
		 */
		@Override
		public synchronized void run() {
			int copied = FileCopyBase.this.filesCopied;
			int fileAmount = FileCopyBase.this.totalFileAmount;

			if (fileAmount != 0 && fileAmount > copied) {
				FileCopyBase.this.step.setStatus(Status.RUNNING,
						"Files copied: " + (copied * 100 / fileAmount) + "% ("
								+ copied + " of " + fileAmount + ")");
			}
		}
	}

}
