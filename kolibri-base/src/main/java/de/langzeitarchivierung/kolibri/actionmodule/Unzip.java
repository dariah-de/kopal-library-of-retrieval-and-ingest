/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionmodule / Unzip.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import java.io.File;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.util.FileUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;

/*******************************************************************************
 * Decompresses all files from a .zip file stored under SipLocation in a
 * destination directory. If the parameter usePathToContentFiles is set to TRUE
 * the first file in the directory PathToContentFiles will be used as input.
 * 
 * TODO Put all the unzip methods in the FileUtils class!
 * 
 * PRECONDITION: The file under SipLocation / PathToContentFiles does exist and
 * is a .zip file.
 * 
 * RESULT: The Unzipped DIP will be existing in a subdirectory of the temporary
 * directory, and the PathToContentFiles in processData will be set.
 * 
 * @author Funk
 * @version 20070503
 * @since 20070502
 ******************************************************************************/

public class Unzip implements ActionModule {

	// STATE (Instance variables) *****************************************

	ProcessData processData;
	Step step;

	File sourceFile;
	String tempDir;
	boolean deleteZipOnExit = false;
	boolean usePathToContentFiles = false;

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * Default Constructor
	 **************************************************************************/
	public Unzip() {
		super();
	}

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
	 **************************************************************************/
	public void go() {
		long startTime = System.currentTimeMillis();

		try {
			if (usePathToContentFiles) {
				File contentDir = new File(this.processData
						.getPathToContentFiles());
				File[] files = contentDir.listFiles();
				// Check if there is at least one file
				if (!(files.length > 0)) {
					this.step.setStatus(Status.ERROR, "File does not exist.");
					return;
				}
				this.sourceFile = files[0];
			} else {
				this.sourceFile = this.processData.getSipLocation();
			}

			// Check if really a ZIP file.
			if (!this.sourceFile.getName().endsWith(".zip")
					&& !this.sourceFile.getName().endsWith(".ZIP")) {
				this.step.setStatus(Status.ERROR, "File is not a ZIP file "
						+ "or at least has no ZIP file extension",
						this.sourceFile);
				return;
			}

			File destFolder = new File(this.sourceFile.getParent(), "unpacked_"
					+ this.sourceFile.getName());

			destFolder.mkdir();

			this.step.setStatus(Status.RUNNING, "Decompressing DIP");

			FileUtils.storeZipContent(this.sourceFile, destFolder);

			// Set the content file's path.
			this.processData
					.setPathToContentFiles(destFolder.getAbsolutePath());
		} catch (Exception e) {
			this.step.setStatus(Status.ERROR, "ZIP decompression failed. "
					+ "System message: " + e.getMessage());
			e.printStackTrace();
			return;
		}

		// Delete ZIP if wanted.
		if (this.deleteZipOnExit) {
			boolean deleted = this.sourceFile.delete();
			if (deleted) {
				this.step.setStatus(Status.RUNNING, "Original ZIP file "
						+ "deleted");
			} else {
				this.step.setStatus(Status.WARNING, "Original ZIP file "
						+ "could not be deleted");
			}
		}

		this.step.setStatus(Status.DONE, "DIP decompressed in "
				+ KolibriTimestamp.getDurationInHours(System
						.currentTimeMillis()
						- startTime));
	}

	// GET & SET METHODS **************************************************

	/***************************************************************************
	 * @param _tempDir
	 *            The tempDir to set.
	 **************************************************************************/
	public void setTempDir(String _tempDir) {
		this.tempDir = _tempDir;
	}

	/***************************************************************************
	 * @param _processData
	 *            The processData to set.
	 **************************************************************************/
	public void setProcessData(ProcessData _processData) {
		this.processData = _processData;
	}

	/***************************************************************************
	 * @param _step
	 *            The step to set.
	 **************************************************************************/
	public void setStep(Step _step) {
		this.step = _step;
	}

	/***************************************************************************
	 * @param _deleteZipOnExit
	 *            The deleteZipOnExit to set.
	 **************************************************************************/
	public void setDeleteZipOnExit(boolean _deleteZipOnExit) {
		this.deleteZipOnExit = _deleteZipOnExit;
	}

	/***************************************************************************
	 * @param _usePathToContentFiles
	 **************************************************************************/
	public void setUsePathToContentFiles(boolean _usePathToContentFiles) {
		this.usePathToContentFiles = _usePathToContentFiles;
	}

}
