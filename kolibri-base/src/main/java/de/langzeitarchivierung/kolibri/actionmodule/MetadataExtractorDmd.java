/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionmodule / MetadataExtractorDmd.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import org.purl.dc.elements.x11.DcSetDocument.DcSet;

import de.langzeitarchivierung.kolibri.Status;

/*******************************************************************************
 * This class describes how to add some descriptive metadata (DC Simple in this
 * case) to the metadata object of the processData object. To get DC metadata
 * you can write your own modules i.e. to query your library catalogue via OAI
 * or extract them from some other XML file.
 * 
 * PRECONDITION: The metadata object (uof.class in our case) already exists.
 * 
 * RESULT: The asset's descriptive metadata section is filled with some dummy DC
 * data.
 * 
 * @author Stefan Funk, SUB Göttingen
 * @version 20061130
 * @since 20060124
 ******************************************************************************/

public class MetadataExtractorDmd extends MetadataExtractorBase
{

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * An example class to show how to use the DC descriptive metadata sections.
	 * Please edit to fit to your needs.
	 * 
	 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
	 **************************************************************************/
	@Override
	public void go() {
		// Create a new DC set using the XML beans.
		DcSet dcSet = DcSet.Factory.newInstance();

		this.step.setStatus(Status.RUNNING, "Extract DC Simple "
				+ "descriptive metadata from somewhere and put in some "
				+ "dummy values");

		// Create some single DC items and add them to the DcSet using dummy
		// values.
		dcSet.addContributor("*** Put in DC contributor value here ***");
		dcSet.addCoverage("*** Put in DC coverage value here ***");
		dcSet.addCreator("*** Put in DC creator value here ***");
		dcSet.addDate("*** Put in DC date value here ***");
		dcSet.addDescription("*** Put in DC description value here ***");
		dcSet.addFormat("*** Put in DC format value here ***");
		dcSet.addIdentifier("*** Put in DC identifier value here ***");
		dcSet.addLanguage("*** Put in DC language value here ***");
		dcSet.addPublisher("*** Put in DC publisher value here ***");
		dcSet.addRelation("*** Put in DC relation value here ***");
		dcSet.addRights("*** Put in DC rights value here ***");
		dcSet.addSource("*** Put in DC source value here ***");
		dcSet.addSubject("*** Put in DC subject value here ***");
		dcSet.addTitle("*** Put in DC title value here ***");
		dcSet.addType("*** Put in DC type value here ***");

		// Add the dcSet to the metadata object.
		try {
			this.processData.getMetadata().addDescMd(dcSet.toString(), "dc");
		}
		catch (Exception e) {
			this.step.setStatus(Status.ERROR, "Error accessing descriptive "
					+ "metadata. System message: " + e.getMessage());
			return;
		}

		this.step.setStatus(Status.DONE,
				"A dummy DC metdata set has been added to the asset");
	}

}
