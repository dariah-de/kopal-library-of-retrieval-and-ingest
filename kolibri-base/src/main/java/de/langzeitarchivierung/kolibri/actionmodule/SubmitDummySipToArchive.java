/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionmodule / SubmitDummySipToArchive.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;

/*******************************************************************************
 * This class submits a SIP with custom protocol to the client loader, which is
 * another instance of the WorkflowTool.class used as a server. It is ment to be
 * used on a different machine. Please have a look at the koLibRI documentation.
 * 
 * PRECONDITION: A Server must be running to start a ClientLoaderServerThread to
 * exchange data (in this case the SIPs filename and policy and the asset
 * itself).
 * 
 * RESULT: The SIP is submitted to the client loader and stored locally on a
 * client loaders file location.
 * 
 * @author Stefan Funk, SUB Göttingen
 * @version 20060201
 * @since 20050628
 * @see de.langzeitarchivierung.kolibri.processstarter
 * @see de.langzeitarchivierung.kolibri.actionmodule.SubmitSipToDias
 ******************************************************************************/

public class SubmitDummySipToArchive implements ActionModule
{

	// STATE (Instance variables) ******************************************

	ProcessData	processData;
	Step		step;

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * Default constructor
	 **************************************************************************/
	public SubmitDummySipToArchive() {
		super();
	}

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * This is the Main method, which in this case not really submits any SIP.
	 * This class must be edited to send assets to a real archive. As an example
	 * please have a look at the class SubmitSipToDias, which implements the SIP
	 * specification of the DIAS and is used to ingest SIPs from the kopal
	 * koLibRI system.
	 * 
	 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
	 **************************************************************************/
	public void go() {
		this.step.setStatus(Status.RUNNING, "Dummy SIP submission started");

		if (this.processData.getSipLocation().exists()) {
			this.step.setStatus(Status.RUNNING, "Dummy asset submission "
					+ "running... submitting SIP '"
					+ this.processData.getSipLocation() + "'");
		}
		else {
			this.step.setStatus(Status.ERROR, "Dummy SIP not found",
					this.processData.getSipLocation());
			return;
		}

		// *********************************
		// *** PLEASE DO SUBMIT SIP HERE ***
		// *********************************

		this.step.setStatus(Status.DONE, "Dummy SIP Submission sucessful");
	}

	// GET & SET METHODS **************************************************

	/***************************************************************************
	 * @param _processData The processData to set.
	 **************************************************************************/
	public void setProcessData(ProcessData _processData) {
		this.processData = _processData;
	}

	/***************************************************************************
	 * @param _step The step to set.
	 **************************************************************************/
	public void setStep(Step _step) {
		this.step = _step;
	}

}
