/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionmodule.sub / Utf8Controller.java
 * 
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 *
 *
 *******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.util.Utf8FilterInputStream;

/*******************************************************************************
 * Checks a file for UTF-8 conformness using the Utf8FilterInputStream and
 * corrects invalid chars.
 * 
 * PRECONDITION: The file must be existing.
 * 
 * RESULT: The newly created file is conform to UTF-8.
 * 
 * @author Funk
 * @version 20061130
 * @since 20060821
 ******************************************************************************/

public class Utf8Controller implements ActionModule
{

	// STATE (Instance variables) *****************************************

	ProcessData	processData;
	Step		step;

	String		filename			= "mets.xml";
	String		originalPostfix		= ".orig";
	boolean		storeOriginalFile	= false;

	// CREATION (Constructors, factory methods, static/inst init) **********

	/***************************************************************************
	 * Constructor.
	 **************************************************************************/
	public Utf8Controller() {
		super();
	}

	// MANIPULATION (Manipulation - what the object does) ******************

	/**
	 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
	 */
	public void go() {
		int errors = 0;
		ArrayList errorList = new ArrayList();

		File theFile = new File(this.processData.getPathToContentFiles(),
				this.filename);
		String currentFilename = theFile.getAbsolutePath();
		theFile.renameTo(new File(this.processData.getPathToContentFiles(),
				this.filename + this.originalPostfix));
		File theOutputFile = new File(this.processData.getPathToContentFiles(),
				this.filename);

		this.step.setStatus(Status.RUNNING, "Copying the file " + this.filename
				+ " of SIP " + this.processData.getProcessName()
				+ " via UTF-8 filter stream to get a valid UTF-8 document");

		try {
			// Set the input stream.
			Utf8FilterInputStream ufis = new Utf8FilterInputStream(
					new BufferedInputStream(new FileInputStream(new File(
							currentFilename + this.originalPostfix))));

			// Set an output stream.
			OutputStream fos = new FileOutputStream(theOutputFile);

			// Get all UTF-8 chars from this stream as byte array and write it
			// to the given output stream.
			byte[] buf = new byte[1048576];
			int i = 0;
			while ((i = ufis.read(buf)) != -1) {
				fos.write(buf, 0, i);
			}

			fos.close();
			ufis.close();

			errorList = ufis.getErrorList();
			errors = errorList.size();
		}
		catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
			this.step.setStatus(Status.ERROR, "File not found error", theFile);
			return;
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
			this.step.setStatus(Status.ERROR, "Input/output error");
			return;
		}

		// Remove the .orig file if it shall not be saved.
		if (!this.storeOriginalFile) {
			if (new File(currentFilename + this.originalPostfix).delete()) {
				this.step.setStatus(Status.RUNNING, "Sucessfully deleted "
						+ "original file '" + currentFilename
						+ this.originalPostfix + "'");
			}
			else {
				this.step.setStatus(Status.WARNING, "Unable to delete "
						+ "original file", new File(currentFilename
						+ this.originalPostfix));
			}
		}
		else {
			this.step.setStatus(Status.RUNNING, "Keeping original file '"
					+ currentFilename + "' with file extension '"
					+ this.originalPostfix + "'");
		}

		// End of module.
		if (errors == 0) {
			this.step.setStatus(Status.DONE, "The file '" + currentFilename
					+ "' is conform " + "to UTF-8");
		}
		else {
			Iterator iter = errorList.iterator();

			while (iter.hasNext()) {
				this.step.setStatus(Status.RUNNING, (String) iter.next());
			}

			this.step.setStatus(Status.DONE, "The file '" + currentFilename
					+ "'is conform to UTF-8 now, corrected " + errors
					+ " error" + (errors == 1 ? "" : "s"));
		}

	}

	// INTERNAL (Internal - implementation details, local classes, ...) ******

	/***************************************************************************
	 * @param _processData The processData to set.
	 **************************************************************************/
	public void setProcessData(ProcessData _processData) {
		this.processData = _processData;
	}

	/***************************************************************************
	 * @param _step The step to set.
	 **************************************************************************/
	public void setStep(Step _step) {
		this.step = _step;
	}

	/***************************************************************************
	 * @param _storeOriginalFile
	 **************************************************************************/
	public void setStoreOriginalFile(boolean _storeOriginalFile) {
		this.storeOriginalFile = _storeOriginalFile;
	}

	/***************************************************************************
	 * @param _filename The filename to set.
	 **************************************************************************/
	public void setFilename(String _filename) {
		this.filename = _filename;
	}

	/***************************************************************************
	 * @param _originalPostfix The originalPostfix to set.
	 **************************************************************************/
	public void setOriginalPostfix(String _originalPostfix) {
		this.originalPostfix = _originalPostfix;
	}

}
