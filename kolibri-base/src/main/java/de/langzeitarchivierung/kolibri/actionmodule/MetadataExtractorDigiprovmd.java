/*******************************************************************************
 * de.langzeitarchivierung.kolibri.actionmodule / MetadataExtractorDigiprovmd.java
 *  
 * Copyright 2005-2007 by Project kopal
 * 
 * http://kopal.langzeitarchivierung.de/
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307, USA
 * 
 * 
 ******************************************************************************
 *
 *
 * CHANGELOG:
 * 10.07.2007	koLibRI version 1.0
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule;

import java.io.File;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import de.ddb.lmeRprocess.LmerProcess;
import de.ddb.lmeRprocess.LmerProcess.Steps;
import de.ddb.lmeRprocess.LmerProcess.Steps.Step;
import de.langzeitarchivierung.kolibri.Status;

/*******************************************************************************
 * This class describes how to add some provenance metadata (DigiprovMD) to the
 * metadata object of the processData object. To get your provenance metadata
 * you have to write your own modules.
 * 
 * NOTE: Normally the MigrationManager adds the provenance metadata to the SIPs,
 * so please takt this class as an example only!
 * 
 * PRECONDITION: The metadata object (uof.class in our case) already exists.
 * 
 * RESULT: The SIP's provenance metadata section is filled with some dummy
 * lmerProcess data.
 * 
 * @author Stefan Funk, SUB Göttingen
 * @version 20070710
 * @since 20070221
 ******************************************************************************/

public class MetadataExtractorDigiprovmd extends MetadataExtractorBase
{

	// MANIPULATION (Manipulation - what the object does) ******************

	/***************************************************************************
	 * An example class to show how to use the provenance metadata sections
	 * (DigiprovMD). Please edit to fit to your needs.
	 * 
	 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
	 **************************************************************************/
	@Override
	public void go() {
		// Set masses of dummy provenance information.
		this.step.setStatus(Status.RUNNING, "Setting some dummy provenance "
				+ "metadata values for the object and all files");

		// Add x provenance metadata sections.
		int x = 4;
		for (int i = 0; i < x; i++) {
			addObjectProvenanceMetadata(String.valueOf(i));
			addFileProvenanceMetadata(String.valueOf(i));
		}

		// DEGUB OUTPUT!
		//
		// showObjectProvenanceMetadata();
		// showFileProvenanceMetadata();
		//
		// DEGUB OUTPUT!

		this.step.setStatus(Status.DONE, "Some dummy provenance metadata has "
				+ "been set correctly");
	}

	// INTERNAL (Internal - implementation details, local classes, ...) ****

	/***************************************************************************
	 * Add the object's provenance metadata.
	 **************************************************************************/
	private void addObjectProvenanceMetadata(String someId) {
		// Create a new provenance object for the SIP.
		LmerProcess objectProvMetadata = LmerProcess.Factory.newInstance();

		// Fill the object now.
		objectProvMetadata.setOldMetadataRecordCreator(someId
				+ ". The oldMetadataRecordCreator");
		objectProvMetadata.setComments(someId + ". The comments");
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		objectProvMetadata.setCompletionDate(cal);
		objectProvMetadata.setOldObjectIdentifier(someId
				+ ". The oldObjectIdentifier");
		objectProvMetadata.setOldObjectVersion(someId
				+ ". The oldObjectVersion");
		objectProvMetadata.setPermission(someId + ". The permission");
		objectProvMetadata.setPermissionDate(cal);
		objectProvMetadata.setProcessCreator(someId + ". The processCreator");
		objectProvMetadata.setPurpose(someId + ". The purpose");
		objectProvMetadata.setResult(someId + ". The result");
		Steps _steps = objectProvMetadata.addNewSteps();
		for (long i = 1; i < 11; i++) {
			Step _step = _steps.addNewStep();
			_step.setNUMBER(BigInteger.valueOf(i));
			_step.setStringValue(someId + ". What did I do? "
					+ "Oh, yes, you're right! I processed step " + i);
		}

		// Add the provenance metadata to the metadata object.
		try {
			this.processData.getMetadata().addProvMd(objectProvMetadata);
		}
		catch (Exception e) {
			this.step.setStatus(Status.ERROR, "Error adding provenance "
					+ "metadata. System message: " + e.getMessage());
			e.printStackTrace();
			return;
		}
	}

	/***************************************************************************
	 * Add the file's provenance metadata.
	 **************************************************************************/
	private void addFileProvenanceMetadata(String someId) {
		List<File> fileList = this.processData.getFileList();

		Iterator iter = fileList.iterator();
		while (iter.hasNext()) {
			// Create a new provenance object for each file.
			LmerProcess fileProvMetadata = LmerProcess.Factory.newInstance();

			// Set masses of dummy information for each file.
			fileProvMetadata.setComments(someId + ". The comments");
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			fileProvMetadata.setCompletionDate(cal);
			fileProvMetadata.setPermission(someId + ". The permission");
			fileProvMetadata.setPermissionDate(cal);
			fileProvMetadata.setProcessCreator(someId + ". The processCreator");
			fileProvMetadata.setPurpose(someId + ". The purpose");
			fileProvMetadata.setResult(someId + ". The result");
			Steps _steps = fileProvMetadata.addNewSteps();
			for (long i = 1; i < 11; i++) {
				Step _step = _steps.addNewStep();
				_step.setNUMBER(BigInteger.valueOf(i));
				_step.setStringValue(someId + ". What did I do? "
						+ "Oh, yes, you're right! I processed step " + i);
			}

			try {
				File grumpf = (File) iter.next();
				this.processData.getMetadata().addProvMd(grumpf,
						fileProvMetadata);
			}
			catch (Exception e) {
				this.step.setStatus(Status.ERROR, "Error adding provenance "
						+ "metadata. System message: " + e.getMessage());
				e.printStackTrace();
				return;
			}
		}
	}

	/***************************************************************************
	 * Show the object's provenance metadata to System.out.
	 **************************************************************************/
	private void showObjectProvenanceMetadata() {
		try {
			List<String> theList = this.processData.getMetadata().getProvMd();

			Iterator iter = theList.iterator();
			while (iter.hasNext()) {
				System.out.println((String) iter.next());
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/***************************************************************************
	 * Show provenance metadata of all files to System.out.
	 **************************************************************************/
	private void showFileProvenanceMetadata() {
		List<File> fileList = this.processData.getFileList();

		Iterator iter = fileList.iterator();
		while (iter.hasNext()) {
			File contentFile = (File) iter.next();
			List<String> theList;

			try {
				theList = this.processData.getMetadata().getProvMd(contentFile);

				Iterator iterZwo = theList.iterator();
				while (iterZwo.hasNext()) {
					System.out.println(iterZwo.next().toString());
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
