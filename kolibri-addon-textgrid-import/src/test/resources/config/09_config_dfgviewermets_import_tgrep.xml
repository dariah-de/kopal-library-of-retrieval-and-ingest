<?xml version="1.0" encoding="UTF-8"?>
<config xmlns="koLibRI-config" xmlns:koLibRI="koLibRI-config"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="koLibRI-config http://kopal.langzeitarchivierung.de/schema/koLibRI/config.xsd">
    <common>

        <!-- This configuration file is pre-configured to ingest into the TextGrid 
            Laboratory, please just provide the project ID and the RBAC session ID! -->

        <!-- THE LOG LEVEL. DO EDIT AS YOU LIKE! -->
        <property>
            <field>logLevel</field>
            <value>SEVERE</value>
            <description>The log level for the WorkflowTool application. Possible are ALL, FINEST, FINER, FINE, INFO, WARNING, SEVERE and OFF.</description>
        </property>

        <!-- EDIT COMMON TEXTGRID USER SETTINGS BELOW -->
        <property>
            <field>defaultPolicyName</field>
            <value>dfgviewermets_import</value>
            <description>For more information please have a look at the policy.xml file</description>
        </property>
        <property>
            <field>hotfolderDir</field>
            <value>./src/test/resources/folders/folder_dfgviewermets_import/</value>
            <description>The folder containing the data. PLEASE NOTE: There must be ONLY ONE folder in the hotfolder containing all the data. This folder will NOT be ingested, if aggregation import is chosen (in this case the data/ folder).</description>
        </property>
        <property>
            <field>projectId</field>
            <value>TGPR-0000</value>
            <description>Put in a valid TextGrid project ID, create TextGrid project first using the TG-lab.</description>
        </property>
        <property>
            <field>rbacSessionId</field>
            <value>1234-5678</value>
            <description>Add a valid RBAC session ID. SID's user must have CREATE right for the given project to ingest into TG-lab, and PUBLISH right to ingest directly into the TG-rep.</description>
        </property>
        <!-- EDIT COMMON TEXTGRID USER SETTINGS ABOVE -->

        <!-- EDIT COMMON TEXTGRID VALUES BELOW -->
        <property>
            <field>tgcrudServerUrl</field>
            <value>https://localhost/1.0/tgcrud/TGCrudService</value>
            <description>Put in TG-crud instance to use for import. Specifies, if dynamic import (TG-lab) or static import (TG-rep sandbox), and of course if productive or test instance of TG-crud is used.</description>
        </property>
        <property>
            <field>tgauthServerUrl</field>
            <value>https://localhost/1.0/tgauth/tgextra.php</value>
            <description>Put in TG-auth instance to use for publishing. Only used in static import (TG-rep) after import.</description>
        </property>
        <property>
            <field>tgsearchServerUrl</field>
            <value>https://localhost/1.0/tgsearch</value>
            <description>Put in TG-search instance to use for publishing. Only used in static import (TG-rep) after import.</description>
        </property>
        <property>
            <field>sesameServerUrl</field>
            <value>https://localhost/1.0/triplestore/textgrid-nonpublic</value>
            <description>Put in the Sesame server URL for deleting and publishing objects.</description>
        </property>
        <property>
            <field>tgpublishServerUrl</field>
            <value>https://localhost/1.0/tgpublish/</value>
            <description>Put in the TG-publish server URL for finally publishing.</description>
        </property>
        <property>
            <field>tgpidServerUrl</field>
            <value>https://localhost/1.0/tgpid/</value>
            <description>The TG-pid service URL.</description>
        </property>
        <property>
            <field>logParameter</field>
            <value></value>
            <description>TextGrid log parameter (may be empty)</description>
        </property>
        <property>
            <field>createNewRevisions</field>
            <value>false</value>
            <description>Creates new revisions for every file submitted (only used in TextGrid modules GetUris, RenameAndRewrite, and SubmitFiles!).</description>
        </property>
        <property>
            <field>ignoredUrlPrefixes</field>
            <value>http://</value>
            <value>www.</value>
            <value>file://</value>
            <description>URLs with this prefixes are ignored during URL and URI rewriting.</description>
        </property>
        <!-- EDIT COMMON TEXTGRID VALUES ABOVE -->

        <!-- EDIT DFG VIEWER METS IMPORT VALUES BELOW -->
        <property>
            <field>rootAggregationMimetype</field>
            <value>text/tg.edition+tg.aggregation+xml</value>
            <description>The given METS file and its objects are imported as an Edition (text/tg.edition+tg.aggregation+xml) or Collection (text/tg.collection+tg.aggregation+xml). And metadata is collected for those objects, if possible.</description>
        </property>
        <!-- EDIT DFG VIEWER METS IMPORT VALUES ABOVE -->

        <!-- PRE-CONFIGURED VALLUES FOR INGESTING E.G. "DIGITALE BIBLIOTHEK" BELOW -->
        <property>
            <field>metadataSuffix</field>
            <value>.meta</value>
            <description>All files with this suffix will be handled as TextGrid metadata files!</description>
        </property>
        <property>
            <field>aggregationSuffix</field>
            <value>.aggregation</value>
            <description>All files with this suffix will be handled as TextGrid aggregation files!</description>
        </property>
        <property>
            <field>editionSuffix</field>
            <value>.edition</value>
            <description>All files with this suffix will be handled as TextGrid edition files!</description>
        </property>
        <property>
            <field>collectionSuffix</field>
            <value>.collection</value>
            <description>All files with this suffix will be handled as TextGrid collection files!</description>
        </property>
        <property>
            <field>xmlSuffix</field>
            <value>.xml</value>
            <description>All files with this suffix will be handled as TextGrid XML content files!</description>
        </property>
        <property>
            <field>workSuffix</field>
            <value>.work</value>
            <description>All files with this suffix will be handled as TextGrid work files!</description>
        </property>
        <property>
            <field>jhoveMetadataSuffix</field>
            <value>.jhove</value>
            <description>The technical metadata extracted by JHOVE will be stored with this suffix.</description>
        </property>
        <!-- PRE-CONFIGURED VALLUES FOR INGESTING E.G. "DIGITALE BIBLIOTHEK" ABOVE -->

        <!-- FIXED COMMON TEXTGRID VALUES BELOW (DO NOT EDIT!) -->
        <property>
            <field>uriPrefix</field>
            <value>textgrid</value>
            <description>The TextGrid URI prefix</description>
        </property>
        <property>
            <field>jhoveConfigFile</field>
            <value>./config/koLibRI/jhove.conf</value>
        </property>
        <!-- FIXED COMMON TEXTGRID VALUES ABOVE (DO NOT EDIT!) -->

        <!-- MORE SETTINGS BELOW (DO ONLY EDIT IF YOU KNOW WHAT YOU DO!) -->
        <property>
            <field>defaultCharset</field>
            <value>UTF-8</value>
        </property>
        <property>
            <field>defaultProcessStarter</field>
            <value>textgrid.Folder</value>
        </property>
        <property>
            <field>policyFile</field>
            <value>./src/test/resources/config/policies.xml</value>
        </property>
        <property>
            <field>logfileDir</field>
            <value>./src/test/resources/folders/log/09_log_config_dfgviewermets_import_tgrep/</value>
        </property>
        <property>
            <field>destinationDir</field>
            <value>./src/test/resources/folders/dest/</value>
        </property>
        <property>
            <field>workDir</field>
            <value>./src/test/resources/folders/work/</value>
        </property>
        <property>
            <field>tempDir</field>
            <value>./src/test/resources/folders/temp/09_temp_config_dfgviewermets_import_tgrep/</value>
        </property>
        <property>
            <field>defaultOwner</field>
            <value>***</value>
        </property>
        <property>
            <field>defaultServer</field>
            <value>***</value>
        </property>
        <property>
            <field>accessServer</field>
            <value>***</value>
        </property>
        <property>
            <field>accessPort</field>
            <value>***</value>
        </property>
        <property>
            <field>knownHostsFile</field>
            <value>***</value>
        </property>
        <property>
            <field>maxFiles</field>
            <value>5000</value>
        </property>
        <property>
            <field>dmdPrefix</field>
            <value>DMD</value>
        </property>
        <property>
            <field>amdPrefix</field>
            <value>AMDTECH</value>
        </property>
        <property>
            <field>techMdPrefix</field>
            <value>TECHMD</value>
        </property>
        <property>
            <field>mdWrapPrefix</field>
            <value>MDWRAP</value>
        </property>
        <property>
            <field>mdClassName</field>
            <value>de.langzeitarchivierung.kolibri.formats.Uof</value>
        </property>
        <property>
            <field>mdTemplateFile</field>
            <value>./config/koLibRI/uof_template.xml</value>
        </property>
        <property>
            <field>maxNumberOfThreads</field>
            <value>2</value>
        </property>
    </common>
    <modules>
        <class name="formats.Uof">
            <property>
                <field>noWhiteSpacesInTechMd</field>
                <value>false</value>
            </property>
            <property>
                <field>checkTechMdSize</field>
                <value>false</value>
            </property>
        </class>
        <class name="util.FormatRegistry">
            <property>
                <field>formatMapFileLocation</field>
                <value>./config/koLibRI/map_dias2jhove.xml</value>
            </property>
            <property>
                <field>formatRegistryBackupFile</field>
                <value>./config/koLibRI/dias_formatregistry.xml</value>
            </property>
            <property>
                <field>useBackupFileOnly</field>
                <value>true</value>
            </property>
        </class>
        <class name="processstarter.textgrid.Folder">
            <!-- Commonly defined values: defaultPolicyName, hotfolderDir -->
            <property>
                <field>readDirectoriesOnly</field>
                <value>false</value>
            </property>
            <property>
                <field>ignoreHiddenFiles</field>
                <value>true</value>
            </property>
            <property>
                <field>startingOffset</field>
                <value>2000</value>
            </property>
            <property>
                <field>addingOffset</field>
                <value>1000</value>
            </property>
        </class>
        <class name="actionmodule.FileCopyBase">
            <property>
                <field>fileCopyNotificationTime</field>
                <value>5000</value>
            </property>
        </class>
        <class name="actionmodule.MetadataGenerator">
            <!-- Commonly defined values: jhoveMetadataSuffix, jhoveConfigFile -->
            <property>
                <field>showHtmlImages</field>
                <value>false</value>
            </property>
            <property>
                <field>showHtmlLinks</field>
                <value>false</value>
            </property>
            <property>
                <field>showGifGlobalColorTable</field>
                <value>false</value>
            </property>
            <property>
                <field>showPdfPages</field>
                <value>false</value>
            </property>
            <property>
                <field>showPdfImages</field>
                <value>false</value>
            </property>
            <property>
                <field>showPdfOutlines</field>
                <value>false</value>
            </property>
            <property>
                <field>showPdfFonts</field>
                <value>false</value>
            </property>
            <property>
                <field>showTiffMixColorMap</field>
                <value>false</value>
            </property>
            <property>
                <field>showTiffPhotoshopProperties</field>
                <value>false</value>
            </property>
            <property>
                <field>showWaveAESAudioMetadata</field>
                <value>false</value>
            </property>
            <property>
                <field>acceptedUnknownFileFormats</field>
                <value>ALL</value>
            </property>
            <property>
                <field>jhoveIsVerbose</field>
                <value>false</value>
            </property>
            <property>
                <field>errorIfSigmatchDiffers</field>
                <value>false</value>
            </property>
            <property>
                <field>storeTechnicalMetadata</field>
                <value>false</value>
                <description>If set to true, the JHOVE metadata is stored as temporary file to be used for technical metadata usage later (e.g. in the TextgridMetadataProcessor class).</description>
            </property>
        </class>
        <class name="actionmodule.TiffImageMetadataProcessor">
            <!-- Commonly defined values: jhoveConfigFile -->
            <property>
                <field>correctInvalidTiffHeaders</field>
                <value>true</value>
            </property>
            <property>
                <field>verbose</field>
                <value>false</value>
            </property>
            <property>
                <field>separateLogging</field>
                <value>false</value>
            </property>
            <property>
                <field>separateLogfileDir</field>
                <value>./src/test/resources/folders/log/</value>
            </property>
            <property>
                <field>nonVerboseReportingTime</field>
                <value>10000</value>
            </property>
        </class>
        <class name="actionmodule.textgrid.TextgridMetadataProcessor">
            <!-- Commonly defined values: metadataSuffix, aggregationSuffix, jhoveMetadataSuffix -->
            <property>
                <field>textgridMetadataTemplate</field>
                <value>./src/test/resources/config/textgrid_metadata_template.xml</value>
            </property>
            <property>
                <field>useUofTechnicalMetadata</field>
                <value>false</value>
            </property>
            <property>
                <field>omitFileSuffix</field>
                <value>true</value>
            </property>
        </class>
        <class name="actionmodule.CleanUpFiles">
            <property>
                <field>deletePathToContentFiles</field>
                <value>true</value>
            </property>
            <property>
                <field>cleanPathToContentFiles</field>
                <value>true</value>
            </property>
            <property>
                <field>deleteDestinationSip</field>
                <value>false</value>
            </property>
        </class>
        <class name="actionmodule.textgrid.test.GetUris">
            <!-- Commonly defined value: metadataSuffix, createNewRevisions -->
            <property>
                <field>urisForFolders</field>
                <value>false</value>
                <description>Is also set in the policies.xml file, so this value maybe does not matter.</description>
            </property>
        </class>
        <class name="actionmodule.textgrid.CreateAggregations">
            <!-- Commonly defined values: metadataSuffix, aggregationSuffix, referenceSuffix, 
                workSuffix -->
            <property>
                <field>useBaseUrisInAggregations</field>
                <value>false</value>
                <description>Base URIs will be used for references in aggregation files. Set to true, if objects shall be ingested into the TG-lab. Set to false, if objects shall be ingested into the TG-rep.</description>
            </property>
            <property>
                <field>sortAlphabetically</field>
                <value>true</value>
                <description>Sorts file lists used for aggregation creation alphabetically instead instead of non-sorted. Not tested yet for normal import, only tested for DFG-Viewer import.</description>
            </property>
            <property>
                <field>omitWorkReferences</field>
                <value>true</value>
                <description>Work references are not put into the aggregations if set to true, default is set to false.</description>
            </property>
        </class>
        <class name="actionmodule.textgrid.RenameAndRewrite">
            <!-- Commonly defined values: uriPrefix, metadataSuffix, aggregationSuffix, 
                editionSuffix, collectionSuffix, xmlSuffix, ignoredUrlprefixes, jhoveMetadataSuffix, 
                createNewRevisions, tgsearchServerUrl -->
            <property>
                <field>rewritePrefix</field>
                <value>tmp.</value>
            </property>
            <property>
                <field>useBaseUrisInAggregations</field>
                <value>false</value>
                <description>Base URIs will be used for references in aggregation files. Set to true, if objects shall be ingested into the TG-lab. Set to false, if objects shall be ingested into the TG-rep.</description>
            </property>
            <property>
                <field>otherTgsearchServerUrl</field>
                <value>https://localhost/1.0/tgsearch-public</value>
                <description>We do need BOTH tgsearch server URLs in this class, so we put the other in here! DO NOT CHANGE!</description>
            </property>
        </class>
        <class name="actionmodule.textgrid.test.GetPidsAndRewrite">
            <!-- Commonly defined values: metadataSuffix, uriPrefix, rbacSessionId, 
                logParameter, ignoredUrlprefixes, tgpidServerUrl -->
            <property>
                <field>getPids</field>
                <value>true</value>
                <description>PIDs are created for every TextGrid object.</description>
            </property>
            <property>
                <field>amountOfPidsAtOnce</field>
                <value>10</value>
            </property>
            <property>
                <field>rewriteMetadataUris</field>
                <value>false</value>
                <description>URIs in metadata files will be rewritten to PIDs.</description>
            </property>
            <property>
                <field>rewriteAggregationUris</field>
                <value>false</value>
                <description>URIs in aggregation files will be rewritten to PIDs.</description>
            </property>
            <property>
                <field>rewriteTeiUris</field>
                <value>true</value>
                <description>URIs in TEI files will be rewritten to PIDs.</description>
            </property>
            <property>
                <field>rewriteXsdUris</field>
                <value>true</value>
                <description>URIs in XSD files will be rewritten to PIDs.</description>
            </property>
            <property>
                <field>rewriteLinkEditorUris</field>
                <value>true</value>
                <description>URIs in Link Editor files will be rewritten to PIDs.</description>
            </property>
            <property>
                <field>removeOldPids</field>
                <value>false</value>
                <description>Removes all PIDs from the generated metadata, of existing.</description>
            </property>
        </class>
        <class name="actionmodule.textgrid.PublishCheck">
            <property>
                <field>xpathNamespaces</field>
                <value>tg=http://textgrid.info/namespaces/metadata/core/2010</value>
                <description>Add required namespaces for evaluating xpath-expressions, separate prefix from URL with "=".</description>
            </property>
            <property>
                <field>editionRequiredFields</field>
                <value>boolean(/tg:object/tg:edition/tg:isEditionOf/text()!='')</value>
                <value>boolean(/tg:object/tg:edition/tg:license/text()!='')</value>
                <description>Add boolean (!) XPath expressions for fields which are required for publishing an Edition here.</description>
            </property>
            <property>
                <field>itemRequiredFields</field>
                <value>boolean(/tg:object/tg:item/tg:rightsHolder/text()!='')</value>
                <description>Add boolean (!) XPath expressions for fields which are required for publishing an Item here.</description>
            </property>
            <property>
                <field>collectionRequiredFields</field>
                <value>boolean(/tg:object/tg:collection/tg:collector/text()!='')</value>
                <description>Add boolean (!) XPath expressions for fields which are required for publishing a collection here.</description>
            </property>
            <property>
                <field>workRequiredFields</field>
                <value>boolean(/tg:object/tg:work/tg:agent/text()!='')</value>
                <value>/tg:object/tg:work/tg:dateOfCreation[@notBefore!='' and @notAfter!='']/@* or /tg:object/tg:work/tg:dateOfCreation/text()!=''</value>
                <value>boolean(/tg:object/tg:work/tg:genre/text()!='')</value>
                <description>Add boolean (!) XPath expressions for fields which are required for publishing a work here.</description>
            </property>
        </class>
        <class name="actionmodule.textgrid.test.SubmitFiles">
            <!-- Commonly defined values: uriPrefix, tgcrudServerUrl, rbacSessionId, 
                projectId, metadataSuffix, createNewRevisions -->
            <property>
                <field>deleteIfIngested</field>
                <value>true</value>
                <description>The data and metadata files are deleted if ingested correctly, so you can continue with the files NOT ingested yet.</description>
            </property>
            <property>
                <field>storeResponseMetadata</field>
                <value>false</value>
                <description>Store the metadata returned by the TG-crud after creating objects.</description>
            </property>
        </class>
        <class name="actionmodule.textgrid.DeleteFiles">
            <!-- Commonly defined values: rbacSessionId, logParameter, tgsearchServiceUrl, 
                tgcrudServiceUrl -->
            <property>
                <field>objectUri</field>
                <value>textgrid:12345.0</value>
                <description>Please use import mapping file, project ID, or root URI of TextGrid object, such as "file:./folders/temp/1470065621459_data_URI.imex", "textgrid:12345.0", or "project:TGPR-f1867520-4a53-9ced-9da5-503762ba0f61".</description>
            </property>
            <property>
                <field>dryrun</field>
                <value>false</value>
                <description>Use to check before deleting anything. Will not delete anything unless set to false!</description>
            </property>
        </class>
        <class name="actionmodule.textgrid.PublishFiles">
            <!-- Commonly defined values: rbacSessionId, logParameter, tgpublishServerUrl, 
                sesameServerUrl -->
            <property>
                <field>objectUri</field>
                <value>textgrid:12345.0</value>
                <description>Please use import mapping file, project ID, or root URI of TextGrid object, such as "file:./folders/temp/1470065621459_data_URI.imex", "textgrid:12345.0", or "project:TGPR-f1867520-4a53-9ced-9da5-503762ba0f61".</description>
            </property>
            <property>
                <field>dryrun</field>
                <value>false</value>
                <description>Use to check before publishing anything. Will not publish anything unless set to false!</description>
            </property>
        </class>
        <class name="actionmodule.textgrid.test.ProcessDfgViewerMets">
            <!-- Commonly defined values: aggregationSuffix, workSuffix, rootAggregationMimetype -->
            <property>
                <field>referenceSuffix</field>
                <value>.reference</value>
                <description>Used for linking PHYS files to LOGICAL files.</description>
            </property>
            <property>
                <field>processStructMapPhysical</field>
                <value>true</value>
                <description>StructMap Physical is processed and aggregations are created.</description>
            </property>
            <property>
                <field>processStructMapLogical</field>
                <value>true</value>
                <description>StructMap Logical is processed and aggregations are created.</description>
            </property>
            <property>
                <field>structMapLogicalAggregationName</field>
                <value>StructMapLogical</value>
                <description>The name of the StructMapLogical aggregation.</description>
            </property>
            <property>
                <field>structMapPhysicalAggregationName</field>
                <value>StructMapPhysical</value>
                <description>The name of the StructMapPhysical aggregation.</description>
            </property>
            <property>
                <field>fileGroups</field>
                <value>DEFAULT</value>
                <value>MIN</value>
                <value>MAX</value>
                <value>THUMBS</value>
                <value>DOWNLOAD</value>
                <description>The file groups (USE) to download files from (please just comment out those you do not want to import!).</description>
            </property>
            <property>
                <field>ignoreMetsValidationFailures</field>
                <value>false</value>
                <description>Ignore some METS validation errors if occurring while processing METS file.</description>
            </property>
        </class>
        <class name="actionmodule.textgrid.DfgViewerMetadataProcessor">
            <!-- Commonly defined values: metadataSuffix, aggregationSuffix, workSuffix 
                rootAggregationMimetype -->
            <property>
                <field>itemTransformationFile</field>
                <value>./config/transformation/dfg-viewer/item.xsl</value>
            </property>
            <property>
                <field>aggregationTransformationFile</field>
                <value>./config/transformation/dfg-viewer/aggregation.xsl</value>
            </property>
            <property>
                <field>collectionTransformationFile</field>
                <value>./config/transformation/dfg-viewer/collection_mods.xsl</value>
            </property>
            <property>
                <field>workTransformationFile</field>
                <value>./config/transformation/dfg-viewer/work_mods.xsl</value>
            </property>
            <property>
                <field>editionTransformationFile</field>
                <value>./config/transformation/dfg-viewer/edition_mods.xsl</value>
            </property>
            <property>
                <field>forceMimetypeExtraction</field>
                <value>true</value>
            </property>
        </class>
        <class name="actionmodule.textgrid.RewriteDfgViewerUrls">
            <!-- Commonly defined values: uriPrefix, metadataSuffix -->
            <property>
                <field>rewriteToPids</field>
                <value>true</value>
                <description>URLs are rewritten to URIs per default using the resolver prefix below. If PIDs shall be used, please set to true. Please note that rewriting URLs only makes sense it the objects are or will be publicly available!</description>
            </property>
            <property>
                <field>resolverPrefix</field>
                <value>https://hdl.handle.net/</value>
                <description>The prefix to use if rewriting URLs. Please use "https://textgridrep.org/" for TextGrid URIs and "https://hdl.handle.net" or any other Handle resolver for TextGrid PIDs.</description>
            </property>
            <property>
                <field>metadataIdType</field>
                <value>METSXMLID</value>
                <description>The ID attribute from the TextGrid metadata to map the PIDs to CONTENTIDS.</description>
            </property>
            <property>
                <field>prettyPrint</field>
                <value>true</value>
                <description>The METS file can now be pretty printed, if set to true.</description>
            </property>
            <property>
                <field>rewriteDvReference</field>
                <value>true</value>
                <description>The dv:reference of the digiprovMD now can be rewritten with the URI or PID of the METS file itself.</description>
            </property>
        </class>
    </modules>
</config>
