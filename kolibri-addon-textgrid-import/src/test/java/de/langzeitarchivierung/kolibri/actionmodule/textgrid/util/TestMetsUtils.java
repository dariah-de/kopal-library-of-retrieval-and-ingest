/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.util;

import static org.junit.Assert.assertFalse;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import org.apache.xmlbeans.XmlException;
import org.junit.Test;
import de.langzeitarchivierung.kolibri.textgrid.util.MetsUtils;

/**
 * TODOLOG
 **
 * CHANGELOG
 *
 * 2016-11-25 - Funk - First version.
 */

/**
 * <p>
 * Test the METS utility class.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2017-03-22
 * @since 2016-11-25
 */

public class TestMetsUtils {

  /**
   * <p>
   * Tests METS validation with valid METS file.
   * </p>
   * 
   * @throws URISyntaxException
   * @throws IOException
   * @throws XmlException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testValidateMetsValid()
      throws URISyntaxException, XmlException, IOException {

    System.out.println("Testing METS validation...");

    URL resource = getClass().getClassLoader()
        .getResource("./mets/S0001-digitalisat-valid.xml");
    File mets = new File(resource.toURI());

    String metsValidationOutput = MetsUtils.validateMets(mets);

    assertFalse(
        "METS " + mets.getName() + " is NOT valid: "
            + metsValidationOutput,
        !metsValidationOutput.equals(""));

    System.out.println("METS is valid, as expected.");
  }

  /**
   * <p>
   * Tests METS validation with invalid METS file.
   * </p>
   * 
   * @throws URISyntaxException
   * @throws IOException
   * @throws XmlException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testValidateMetsInvalid() throws URISyntaxException, XmlException, IOException {

    System.out.println("Testing METS validation...");

    URL resource = getClass().getClassLoader().getResource("./mets/S0001-digitalisat-invalid.xml");
    File mets = new File(resource.toURI());

    String metsValidationOutput = MetsUtils.validateMets(mets);

    assertFalse("METS " + mets.getName() + " is valid!", metsValidationOutput.equals(""));

    System.out.println("METS is not valid, as expected.");
  }

}
