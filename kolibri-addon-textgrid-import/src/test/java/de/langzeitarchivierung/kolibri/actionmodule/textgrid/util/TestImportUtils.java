/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.util;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import org.junit.Test;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.textgrid.util.ImportUtils;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import jakarta.xml.bind.JAXBException;

/**
 * CHANGELOG
 * 
 * 2020-05-14 - Funk - Add validation test.
 * 
 * 2017-03-18 - Funk First version.
 */

/**
 * <p>
 * Some tests.
 * </p>
 * 
 * @author Stefan Funk, SUB Göttingen
 * @version 2020-05-14
 * @since 2017-03-18
 */

public class TestImportUtils {

  /**
   * @throws URISyntaxException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testReadingObjectTypeMetadata() throws URISyntaxException {

    String lookingForTitle = "Wolzogen, Caroline von";

    URL resource = getClass().getClassLoader().getResource("./reading/objectType.meta");
    File metadataFile = new File(resource.toURI());

    ObjectType metadata = ImportUtils.getMetadata(metadataFile);
    if (metadata != null
        && metadata.getGeneric().getProvided().getTitle().get(0).equals(lookingForTitle)) {
    } else {
      System.out.println("Title " + lookingForTitle + " not found in metadata file");
      assertTrue(false);
    }
  }

  /**
   * @throws URISyntaxException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testReadingMetadataContainerTypeMetadata() throws URISyntaxException {

    String lookingForUri = "textgrid:2v98c.0";

    URL resource = getClass().getClassLoader().getResource("./reading/metadataContainerType.meta");
    File metadataFile = new File(resource.toURI());

    ObjectType metadata = ImportUtils.getMetadata(metadataFile);
    if (metadata != null
        && metadata.getGeneric().getGenerated().getTextgridUri().getValue().equals(lookingForUri)) {
    } else {
      System.out.println("URI " + lookingForUri + " not found in metadata file");
      assertTrue(false);
    }
  }

  /**
   * 
   */
  @Test
  public void testValidateValidMetadataObjectFile() {

    File metadataFile = new File("./src/test/resources/validating/work_metadata.meta");

    String filepath = metadataFile.getAbsolutePath();
    try {
      filepath = metadataFile.getCanonicalPath();
      ImportUtils.validateMetadataFile(metadataFile);
    } catch (JAXBException | SAXException | IOException e) {
      System.out
          .println("error validating metdata file " + filepath + ": " + e.getCause().getMessage());
      assertTrue(false);
    }
  }

  /**
   * 
   */
  @Test
  public void testValidateValidMetadataContainerFile() {

    File metadataFile =
        new File("./src/test/resources/validating/work_metadata_tgObjectMetadata.meta");

    String filepath = metadataFile.getAbsolutePath();
    try {
      filepath = metadataFile.getCanonicalPath();
      ImportUtils.validateMetadataFile(metadataFile);
    } catch (JAXBException | SAXException | IOException e) {
      System.out
          .println("error validating metdata file " + filepath + ": " + e.getCause().getMessage());
      assertTrue(false);
    }
  }

  /**
   * 
   */
  @Test
  public void testValidateInvalidMetadataFile() {

    File metadataFile = new File("./src/test/resources/validating/work_metadata_invalid.meta");

    String filepath = metadataFile.getAbsolutePath();
    try {
      filepath = metadataFile.getCanonicalPath();
      ImportUtils.validateMetadataFile(metadataFile);
      assertTrue(false);
    } catch (JAXBException | SAXException | IOException e) {
      System.out.println(
          "expexted error validating metdata file " + filepath + ": " + e.getCause().getMessage());
    }
  }

}
