/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.test;

import java.util.UUID;
import de.langzeitarchivierung.kolibri.actionmodule.textgrid.GetPidsAndRewriteAbs;

/**
 * TODOLOG
 **
 * CHANGELOG
 * 
 * 2017-03-27 - Funk - First version.
 */

/**
 * <p>
 * Extension of ActionModule ProcessDfgViewerMetsAbs for testing. Abstract class for getting fake
 * PIDs from UUIDs.
 * </p>
 * 
 * @author Stefan Funk, DAASI International GmbH
 * @version 2020-02-21
 * @since 2017-03-27
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 */

public class GetPidsAndRewrite extends GetPidsAndRewriteAbs {

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.textgrid.
   * GetPidsAndRewriteAbs#getPids(java.lang.String, java.lang.String, java.lang.String,
   * java.lang.String)
   */
  @Override
  protected String getPids(String theRbacSessionId, String theUriString,
      String theFilesizeString, String theChecksumString) {

    String result = "";

    // Split URIs.
    String uris[] = theUriString.split(",");

    // Fake PID for every URI. Should look like "URL@PID,URL@PID,URL@PID"
    StringBuffer sb = new StringBuffer();
    for (String uri : uris) {
      sb.append(uri + "@" + UUID.randomUUID().toString() + ",");
    }
    result = sb.toString();
    result = result.substring(0, result.length() - 1);

    return result;
  }

}
