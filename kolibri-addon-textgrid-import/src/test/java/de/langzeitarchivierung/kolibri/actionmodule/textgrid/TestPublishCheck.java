/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import org.junit.Test;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 */

/**
 * @author Stefan E. Funk
 * @version 2020-05-14
 * @since 2020-02-18
 */

public class TestPublishCheck extends TestTextGridImport {

  /**
   * <p>
   * Testing policy aggregation_import here. Assuming: A TG-rep import!
   * </p>
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testPublishCheckAggregationImportTgrepFAIL() throws Exception {

    String tempFile = "/folders/temp/07_temp_config_aggregation_import_tgrep_fail/";
    String logFile = "/folders/log/07_log_config_aggregation_import_tgrep_fail/";
    String configFile = "/config/07_config_aggregation_import_tgrep_fail.xml";

    testImport(tempFile, logFile, configFile, TEST_PID_IMEX_FILE, DATA_ENDING, ERROR_EXPECTED);
  }

  /**
   * <p>
   * Testing policy aggregation_import here. Assuming: A TG-rep import!
   * </p>
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testPublishCheckAggregationImportTgrep() throws Exception {

    String tempFile = "/folders/temp/07_temp_config_aggregation_import_tgrep/";
    String logFile = "/folders/log/07_log_config_aggregation_import_tgrep/";
    String configFile = "/config/07_config_aggregation_import_tgrep.xml";

    testImport(tempFile, logFile, configFile, TEST_PID_IMEX_FILE, DATA_ENDING, NO_ERROR_EXPECTED);
  }

  /**
   * <p>
   * Testing policy complete_import here. Assuming: A TG-rep import!
   * </p>
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testPublishCheckCompleteImportTgrep() throws Exception {

    String tempFile = "/folders/temp/08_temp_config_complete_import_tgrep/";
    String logFile = "/folders/log/08_log_config_complete_import_tgrep/";
    String configFile = "/config/08_config_complete_import_tgrep.xml";

    testImport(tempFile, logFile, configFile, TEST_PID_IMEX_FILE, DATA_ENDING, NO_ERROR_EXPECTED);
  }

  /**
   * <p>
   * Testing policy complete_import with Distant Reading example files. Assuming: A TG-rep import!
   * </p>
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testPublishCheckCompleteImportTgrepDistantReading() throws Exception {

    String tempFile = "/folders/temp/10_temp_config_complete_import_tgrep_distant_reading/";
    String logFile = "/folders/log/10_log_config_complete_import_tgrep_distant_reading/";
    String configFile = "/config/10_config_complete_import_tgrep_distant_reading.xml";

    testImport(tempFile, logFile, configFile, TEST_PID_IMEX_FILE, DATA_ENDING, NO_ERROR_EXPECTED);
  }

  /**
   * <p>
   * Testing policy complete_import with Distant Reading example files with false rdf:about URI.
   * Assuming: A TG-rep import!
   * </p>
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testPublishCheckCompleteImportTgrepDistantReadingWrongAboutURI() throws Exception {

    String tempFile =
        "/folders/temp/11_temp_config_complete_import_tgrep_distant_reading_wrong_about_uri/";
    String logFile =
        "/folders/log/11_log_config_complete_import_tgrep_distant_reading_wrong_about_uri/";
    String configFile =
        "/config/11_config_complete_import_tgrep_distant_reading_wrong_about_uri.xml";

    testImport(tempFile, logFile, configFile, TEST_PID_IMEX_FILE, DATA_ENDING, ERROR_EXPECTED);
  }

  /**
   * <p>
   * Testing policy complete_import with Distant Reading example files with false rdf:resource path.
   * Assuming: A TG-rep import!
   * </p>
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testPublishCheckCompleteImportTgrepDistantReadingWrongResoureURI() throws Exception {

    String tempFile =
        "/folders/temp/12_temp_config_complete_import_tgrep_distant_reading_wrong_resource_uri/";
    String logFile =
        "/folders/log/12_log_config_complete_import_tgrep_distant_reading_wrong_resource_uri/";
    String configFile =
        "/config/12_config_complete_import_tgrep_distant_reading_wrong_resource_uri.xml";

    testImport(tempFile, logFile, configFile, TEST_PID_IMEX_FILE, DATA_ENDING, ERROR_EXPECTED);
  }

  /**
   * <p>
   * Testing policy complete_import with wrong path to work file. Assuming: A TG-rep import!
   * </p>
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testPublishCheckCompleteImportTgrepFalsePathToWorkFile() throws Exception {

    String tempFile =
        "/folders/temp/13_temp_config_complete_import_tgrep_false_path_to_work_file/";
    String logFile =
        "/folders/log/13_log_config_complete_import_tgrep_false_path_to_work_file/";
    String configFile =
        "/config/13_config_complete_import_tgrep_false_path_to_work_file.xml";

    testImport(tempFile, logFile, configFile, TEST_PID_IMEX_FILE, DATA_ENDING, ERROR_EXPECTED);
  }

}
