/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import de.langzeitarchivierung.kolibri.actionmodule.textgrid.GetUrisAbs;

/**
 * CHANGELOG
 * 
 * 2020-02-18 - Funk - Using more TG-like URIs now!
 * 
 * 2017-03-27 - Funk - First version.
 */

/**
 * <p>
 * Extension of ActionModule ProcessDfgViewerMetsAbs for testing. Fakes TextGrid URIs from cut
 * UUIDs.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-02-21
 * @since 2017-03-27
 */

public class GetUris extends GetUrisAbs {

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.textgrid.GetUrisAbs#getUris( int)
   */
  @Override
  protected List<String> getUris(int howMany) {

    List<String> result = new ArrayList<String>();

    for (int i = 0; i < howMany; i++) {
      result.add("textgrid:" + UUID.randomUUID().toString().replace("-", "").substring(0, 5));
    }

    return result;
  }

}
