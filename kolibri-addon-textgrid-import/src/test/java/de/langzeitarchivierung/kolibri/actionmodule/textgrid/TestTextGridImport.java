/**
 * This software is copyright (c) 2022 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import org.junit.Ignore;
import org.junit.Test;
import de.langzeitarchivierung.kolibri.WorkflowTool;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2022-06-10 - Funk - Ignoring large tests.
 * 
 * 2021-03-24 - Funk - Add revision import tests.
 * 
 * 2019-05-09 - Funk - Using URIs now instead of Unix file separators in strings.
 * 
 * 2017-03-22 - Funk - Copied from TestWorkflowTool.
 */

/**
 * @author Stefan E. Funk
 * @version 2022-06-10
 * @since 2017-03-23
 */

public class TestTextGridImport {

  // **
  // FINAL
  // **

  protected static final String BASE = "./src/test/resources";
  protected static final String WOTO_START = "-----------  WOTO START ";
  protected static final String WOTO_END = "-----------  WOTO END ";
  protected static final String TEST_START = "===========  TEST START ";
  protected static final String TEST_END = "===========  TEST END \n";
  protected static final String DATA_ENDING = "_data";
  protected static final String XML_ENDING = "_xml";
  protected static final boolean ERROR_EXPECTED = true;
  protected static final boolean NO_ERROR_EXPECTED = false;

  // Configuration

  // protected static final boolean DELETE_AFTER_TEST = true;
  protected static final boolean DELETE_AFTER_TEST = false;
  protected static final boolean TEST_PID_IMEX_FILE = true;
  protected static final boolean DONT_TEST_PID_IMEX_FILE = false;

  /**
   * <p>
   * Testing policy complete_import here. Assuming: A TG-lab import!
   * </p>
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testCompleteImportTglab() throws Exception {

    String tempFile = "/folders/temp/01_temp_config_complete_import_tglab/";
    String logFile = "/folders/log/01_log_config_complete_import_tglab/";
    String configFile = "/config/01_config_complete_import_tglab.xml";

    synchronized (configFile) {
      testImport(tempFile, logFile, configFile, DONT_TEST_PID_IMEX_FILE, DATA_ENDING,
          NO_ERROR_EXPECTED);
    }
  }

  /**
   * <p>
   * Testing policy aggregation_import here. Assuming: A TG-lab import!
   * </p>
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testAggregationImportTglab() throws Exception {

    String tempFile = "/folders/temp/02_temp_config_aggregation_import_tglab/";
    String logFile = "/folders/log/02_log_config_aggregation_import_tglab/";
    String configFile = "/config/02_config_aggregation_import_tglab.xml";

    testImport(tempFile, logFile, configFile, DONT_TEST_PID_IMEX_FILE, DATA_ENDING,
        NO_ERROR_EXPECTED);
  }

  /**
   * <p>
   * Testing policy aggregation_import here. Assuming: A TG-rep import!
   * </p>
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testAggregationImportTgrep() throws Exception {

    String tempFile = "/folders/temp/03_temp_config_aggregation_import_tgrep/";
    String logFile = "/folders/log/03_log_config_aggregation_import_tgrep/";
    String configFile = "/config/03_config_aggregation_import_tgrep.xml";

    testImport(tempFile, logFile, configFile, TEST_PID_IMEX_FILE, DATA_ENDING, NO_ERROR_EXPECTED);
  }

  /**
   * <p>
   * Testing policy complete_import here. Assuming: A TG-rep import!
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testCompleteImportTgrep() throws Exception {

    String tempFile = "/folders/temp/04_temp_config_complete_import_tgrep/";
    String logFile = "/folders/log/04_log_config_complete_import_tgrep/";
    String configFile = "/config/04_config_complete_import_tgrep.xml";

    testImport(tempFile, logFile, configFile, TEST_PID_IMEX_FILE, DATA_ENDING, NO_ERROR_EXPECTED);
  }

  /**
   * <p>
   * Testing policy complete_import here. Assuming: A TG-rep import! We have MORE data in here as in
   * the other tests!
   * </p>
   * 
   * NOTE: Please un-ignore if needed! Test takes too long for every build!
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  @Ignore
  public void testCompleteImportTgrepMore() throws Exception {

    String tempFile = "/folders/temp/05_temp_config_complete_import_tgrep_more/";
    String logFile = "/folders/log/05_log_config_complete_import_tgrep_more/";
    String configFile = "/config/05_config_complete_import_tgrep_more.xml";

    testImport(tempFile, logFile, configFile, TEST_PID_IMEX_FILE, DATA_ENDING, NO_ERROR_EXPECTED);
  }

  /**
   * <p>
   * Testing policy dfgviewermets_import here. Assuming: A TG-rep import!
   * </p>
   * 
   * TODO Take an example where we do get agent and date for works correctly...
   * 
   * TODO ...or edit work_tei stylesheet...!!
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testDfgviewermetsImportTgrep() throws Exception {

    String tempFile = "/folders/temp/06_temp_config_dfgviewermets_import_tgrep/";
    String logFile = "/folders/log/06_log_config_dfgviewermets_import_tgrep/";
    String configFile = "/config/06_config_dfgviewermets_import_tgrep.xml";

    testImport(tempFile, logFile, configFile, TEST_PID_IMEX_FILE, XML_ENDING, ERROR_EXPECTED);
  }

  /**
   * <p>
   * Testing policy complete_import to TG-lab with new revisions here.
   * </p>
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testCompleteImportTglabNewRevisions() throws Exception {

    String tempFile = "/folders/temp/14_temp_config_complete_new_revision_import_tglab/";
    String logFile = "/folders/log/14_log_config_complete_new_revision_import_tglab/";
    String configFile = "/config/14_config_complete_new_revision_import_tglab.xml";

    synchronized (configFile) {
      testImport(tempFile, logFile, configFile, DONT_TEST_PID_IMEX_FILE, DATA_ENDING,
          NO_ERROR_EXPECTED);
    }
  }

  /**
   * <p>
   * Testing policy aggregation_import here. Assuming: A TG-lab import with failing config check!
   * </p>
   * 
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testAggregationImportTglabFailConfigCheck() throws Exception {

    String tempFile = "/folders/temp/15_temp_config_aggregation_import_tglab_fail_config_check/";
    String logFile = "/folders/log/15_log_config_aggregation_import_tglab_fail_config_check/";
    String configFile = "/config/15_config_aggregation_import_tglab_fail_config_check.xml";

    testImport(tempFile, logFile, configFile, DONT_TEST_PID_IMEX_FILE, DATA_ENDING, ERROR_EXPECTED);
  }

  /**
   * @param theTempLocation
   * @param theLogLocation
   * @param theConfigLocation
   * @param testPidImexFile
   * @param dataEndsWith
   * @param errorExpected
   * @throws Exception
   */
  protected static void testImport(final String theTempLocation, final String theLogLocation,
      final String theConfigLocation, final boolean testPidImexFile, final String dataEndsWith,
      boolean errorExpected) throws Exception {

    File tempFile = new File(".", BASE + theTempLocation);
    URI tempLocation = tempFile.toURI();
    File configFile = new File(".", BASE + theConfigLocation);
    URI configLocation = configFile.toURI();
    File logFile = new File(".", BASE + theLogLocation);
    URI logLocation = logFile.toURI();

    System.out.println(TEST_START + " [" + configFile.getName() + "]");
    System.out.println("temp location [" + tempFile.getName() + "]");
    System.out.println("log location [" + logFile.getName() + "]");

    // Create temp and log folders.
    File tmp = new File(tempLocation);
    if (tmp.exists()) {
      deleteFile(tmp);
    }
    tmp = createFolder(tempLocation);
    File log = new File(logLocation);
    if (log.exists()) {
      deleteFile(log);
    }
    log = createFolder(logLocation);

    // Create, configure, and run a WorkflowTool.
    WorkflowTool woto = new WorkflowTool();
    woto.configure(new File(configLocation).getCanonicalPath());

    System.out.println(WOTO_START + "[" + woto + "]");

    woto.run();

    System.out.println(WOTO_END + "[" + woto + "]");

    if (woto.getErrorQueue().isEmpty()) {
      if (errorExpected) {
        finish(tmp, log);
        assertTrue("Error queue is empty, but should NOT be!", false);
      }
    } else {
      if (!errorExpected) {
        finish(tmp, log);
        assertTrue("Error queue is not empty, but SHOULD BE!", false);
      }
    }

    // Do some checks if no error is expected.
    if (!errorExpected) {
      if (!checkForUriImexFile(tmp)) {
        finish(tmp, log);
        assertTrue("URI IMEX file " + tmp.getName() + " not existing!", false);
      }
      if (testPidImexFile && !checkForPidImexFile(tmp)) {
        finish(tmp, log);
        assertTrue("PID IMEX file " + tmp.getName() + " not existing!", false);
      }
      if (!checkForDataFolder(tmp, dataEndsWith)) {
        finish(tmp, log);
        assertTrue("Data folder " + tmp.getName() + " not found!", false);
      }
    } else {
      System.out.println("*expected error**");
    }

    finish(tmp, log);
  }

  /**
   * @param tmp
   * @param log
   * @throws IOException
   */
  private static void finish(File tmp, File log) throws IOException {

    if (DELETE_AFTER_TEST) {
      deleteFile(tmp);
      deleteFile(log);
    }

    System.out.println(TEST_END);
  }

  /**
   * @param theFile
   * @return
   * @throws IOException
   */
  private static File createFolder(URI theFile) throws IOException {

    File result = new File(theFile);

    System.out.println("Creating folder: " + result.getCanonicalPath());

    boolean created = result.mkdirs();
    assertTrue("Folder creation failed!", created);

    return result;
  }

  /**
   * <p>
   * Delete recursively.
   * </p>
   * 
   * @param theFile
   * @return
   * @throws IOException
   */
  private static boolean deleteFile(File theFile) throws IOException {

    boolean result = false;

    if (theFile.isFile()) {
      // Do not delete .why files...
      if (!theFile.getName().endsWith(".why")) {
        result = theFile.delete();
        if (!result) {
          System.out.println("Coule not delete file: " + theFile.getCanonicalPath());
        }
      }
    } else {
      for (File f : theFile.listFiles()) {
        result = deleteFile(f);
        if (!result) {
          System.out.println("Coule not delete file: " + theFile.getCanonicalPath());
        }
      }
      result = theFile.delete();
      if (!result) {
        System.out.println("Coule not delete file: " + theFile.getCanonicalPath());
      }
    }

    return result;
  }

  /**
   * @param theFile
   */
  private static boolean checkForPidImexFile(final File theFile) {
    return checkForFileSuffix(theFile, "_PID.imex");
  }

  /**
   * @param theFile
   */
  private static boolean checkForUriImexFile(final File theFile) {
    return checkForFileSuffix(theFile, "_URI.imex");
  }

  /**
   * @param theFile
   * @param theSuffix
   */
  private static boolean checkForFileSuffix(final File theFile, final String theSuffix) {

    boolean result = false;

    if (theFile != null && theSuffix != null) {
      for (File f : theFile.listFiles()) {
        if (f.getName().endsWith(theSuffix)) {
          System.out.println("File found with suffix [" + theSuffix + "]: " + f.getName());
          result = true;
        }
      }
    }

    return result;
  }

  /**
   * @param theFile
   * @param endsWith
   */
  private static boolean checkForDataFolder(final File theFile, final String endsWith) {

    boolean result = false;

    if (theFile != null && endsWith != null) {
      for (File f : theFile.listFiles()) {
        if (f.getName().endsWith(endsWith) && f.isDirectory()) {
          System.out.println("Data temp folder found: " + f.getName());
          result = true;
        }
      }
    }

    return result;
  }

}
