/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @copyright SUB Göttingen (http://www.sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingn.de)
 */

/**
 * TODOLOG
 * 
 * TODO Please generalize!
 **
 * CHANGELOFG
 * 
 * 2020-02-21 - Funk - Taken from
 * /kolibri-tgpublish-service/src/test/java/de/langzeitarchivierung/kolibri/actionmodule/textgrid/publish/TestPublishCheck.java
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.apache.cxf.helpers.IOUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import de.langzeitarchivierung.kolibri.textgrid.util.ImportUtils;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBException;

/**
 *
 */

public class TestCheckRequiredMetadata {

  // **
  // FINALS
  // **

  private static final String TG_NAMESPACE =
      "tg=http://textgrid.info/namespaces/metadata/core/2010";
  private static final String WORK_REQUIRED_FIELDS_AGENT =
      "boolean(/tg:object/tg:work/tg:agent/text()!='')";
  private static final String WORK_REQUIRED_FIELDS_DOC =
      "/tg:object/tg:work/tg:dateOfCreation[@notBefore!='' and @notAfter!='']/@* or /tg:object/tg:work/tg:dateOfCreation/text()!=''";
  private static final String WORK_REQUIRED_FIELDS_GENRE =
      "boolean(/tg:object/tg:work/tg:genre/text()!='')";
  private static final String ITEM_REQUIRED_FIELD_RIGHTSHOLDER =
      "boolean(/tg:object/tg:item/tg:rightsHolder/text()!='')";
  private static final String REQUIRED_METADATA_FOLDER =
      "./src/test/resources/checkRequiredMetadata/";

  // **
  // STATICS
  // **

  private static String noItemMetadata;
  private static String itemMetadata;
  private static String dateOfCreationWorkNoContent;
  private static String dateOfCreationWorkNotBeforeNotAfterOnly;
  private static String dateOfCreationWorkNotBeforeOnly;
  private static String dateOfCreationWorkNotAfterOnly;
  private static String dateOfCreationWorkOnlyTagContent;
  private static String dateOfCreationWorkEverything;
  private static String dateOfCreationWorkNoAttributesAndTag;
  private static List<String> workRequiredFields = new ArrayList<String>();
  private static List<String> itemRequiredFields = new ArrayList<String>();
  private static Map<String, String> xpathNamespaces = new HashMap<String, String>();

  /**
   * @throws Exception
   */
  @SuppressWarnings("javadoc")
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    // Set up work required fields.
    workRequiredFields.add(WORK_REQUIRED_FIELDS_AGENT);
    workRequiredFields.add(WORK_REQUIRED_FIELDS_DOC);
    workRequiredFields.add(WORK_REQUIRED_FIELDS_GENRE);

    // Set up item required fields.
    itemRequiredFields.add(ITEM_REQUIRED_FIELD_RIGHTSHOLDER);

    // Set XPath namespace.
    String[] np = TG_NAMESPACE.split("=");
    xpathNamespaces.put(np[0], np[1]);

    // Set up files.
    itemMetadata = IOUtils.toString(
        new FileInputStream(new File(REQUIRED_METADATA_FOLDER + "item-metadata.meta.xml")));
    noItemMetadata = IOUtils.toString(
        new FileInputStream(new File(REQUIRED_METADATA_FOLDER + "no-item-metadata.meta.xml")));
    dateOfCreationWorkNoContent = IOUtils.toString(new FileInputStream(
        new File(REQUIRED_METADATA_FOLDER + "date-of-creation-work-no-content.meta.xml")));
    dateOfCreationWorkNotBeforeNotAfterOnly = IOUtils.toString(new FileInputStream(new File(
        REQUIRED_METADATA_FOLDER + "date-of-creation-work-not-before-not-after-only.meta.xml")));
    dateOfCreationWorkNotBeforeOnly = IOUtils.toString(new FileInputStream(
        new File(REQUIRED_METADATA_FOLDER + "date-of-creation-work-not-before-only.meta.xml")));
    dateOfCreationWorkNotAfterOnly = IOUtils.toString(new FileInputStream(
        new File(REQUIRED_METADATA_FOLDER + "date-of-creation-work-not-after-only.meta.xml")));
    dateOfCreationWorkOnlyTagContent = IOUtils.toString(new FileInputStream(
        new File(REQUIRED_METADATA_FOLDER + "date-of-creation-work-only-tag-content.meta.xml")));
    dateOfCreationWorkEverything = IOUtils.toString(new FileInputStream(
        new File(REQUIRED_METADATA_FOLDER + "date-of-creation-work-everything.meta.xml")));
    dateOfCreationWorkNoAttributesAndTag = IOUtils.toString(new FileInputStream(new File(
        REQUIRED_METADATA_FOLDER + "date-of-creation-work-no-attributes-and-tag.meta.xml")));
  }

  /**
   * 
   */
  @AfterClass
  public static void tearDownAfterClass() {}

  /**
   *   
  */
  @Before
  public void setUp() {}

  /**
   * @throws java.lang.Exception
   */
  @SuppressWarnings("javadoc")
  @After
  public void tearDown() throws Exception {}

  /**
   * @throws ParserConfigurationException
   * @throws JAXBException
   * @throws XPathExpressionException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testCheckItemRequiredMetadata()
      throws XPathExpressionException, JAXBException, ParserConfigurationException {

    System.out.println("Testing required metadata check for item [metadata existing]...");

    // Create TG object from TG metadata.
    ObjectType metadata = JAXB.unmarshal(new StringReader(itemMetadata), ObjectType.class);

    // Check if all item required metadata fields are set.
    List<String> itemMissing = ImportUtils.checkRequiredMetadata(itemRequiredFields, metadata,
        xpathNamespaces, null, "test:uri");

    // Item list must be empty!
    if (!itemMissing.isEmpty()) {
      System.out.println("\tERROR: Item missing list is not empty: " + itemMissing);
      assertTrue(false);
    } else {
      System.out.println("\tOK: Item missing list is empty");
    }
  }

  /**
   * @throws ParserConfigurationException
   * @throws JAXBException
   * @throws XPathExpressionException
   * 
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testCheckItemNoRequiredMetadata()
      throws XPathExpressionException, JAXBException, ParserConfigurationException {

    System.out.println("Testing required metadata check for item [metadata missing]...");

    // Create TG object from TG metadata.
    ObjectType metadata = JAXB.unmarshal(new StringReader(noItemMetadata), ObjectType.class);

    // Check if all item required metadata fields are set.
    List<String> itemMissing = ImportUtils.checkRequiredMetadata(itemRequiredFields, metadata,
        xpathNamespaces, null, "test:uri");

    // We have no item metadata!
    if (itemMissing.size() != 1 && !itemMissing.contains(ITEM_REQUIRED_FIELD_RIGHTSHOLDER)) {
      System.out.println("\tERROR: Item missing list must contain '"
          + ITEM_REQUIRED_FIELD_RIGHTSHOLDER + "', but doesn't: " + itemMissing);
      assertTrue(false);
    } else {
      System.out.println("\tOK: Item missing list contains " + itemMissing);
    }
  }

  /**
   * @throws ParserConfigurationException
   * @throws JAXBException
   * @throws XPathExpressionException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testWorkDateOfCreationNoTagContent()
      throws XPathExpressionException, JAXBException, ParserConfigurationException {

    System.out.println("Test dateOfCreation work check [no tag content]...");

    // Create TG object from TG metadata.
    ObjectType metadata =
        JAXB.unmarshal(new StringReader(dateOfCreationWorkNoContent), ObjectType.class);

    // Check if all work required metadata fields are set.
    List<String> workMissing = ImportUtils.checkRequiredMetadata(workRequiredFields, metadata,
        xpathNamespaces, null, "test:uri");

    // We have no dateOfCreation content at all --> workMissing must only contain the value of
    // WORK_REQUIRED_FIELDS_DOC only!
    if (workMissing.size() != 1 && !workMissing.contains(WORK_REQUIRED_FIELDS_DOC)) {
      System.out.println("\tERROR: Work missing list must contain '" + WORK_REQUIRED_FIELDS_DOC
          + "', but doesn't: " + workMissing);
      assertTrue(false);
    } else {
      System.out.println("\tOK: Work missing list contains " + workMissing);
    }
  }

  /**
   * @throws ParserConfigurationException
   * @throws JAXBException
   * @throws XPathExpressionException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testWorkDateOfCreationNotBeforeNotAfterOnly()
      throws XPathExpressionException, JAXBException, ParserConfigurationException {

    System.out.println("Test dateOfCreation work check [notBefore and notAfter only]...");

    // Create TG object from TG metadata.
    ObjectType metadata =
        JAXB.unmarshal(new StringReader(dateOfCreationWorkNotBeforeNotAfterOnly), ObjectType.class);

    // Check if all work required metadata fields are set.
    List<String> workMissing = ImportUtils.checkRequiredMetadata(workRequiredFields, metadata,
        xpathNamespaces, null, "test:uri");

    // We have notBefore and notAfter attributes only, workMissing list must be empty!
    if (!workMissing.isEmpty()) {
      System.out.println("\tERROR: Work missing list is not empty: " + workMissing);
      assertTrue(false);
    } else {
      System.out.println("\tOK: Work missing list is empty");
    }
  }

  /**
   * @throws ParserConfigurationException
   * @throws JAXBException
   * @throws XPathExpressionException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testWorkDateOfCreationEverything()
      throws XPathExpressionException, JAXBException, ParserConfigurationException {

    System.out
        .println("Test dateOfCreation work check [notBefore and notAfter and tag content]...");

    // Create TG object from TG metadata.
    ObjectType metadata =
        JAXB.unmarshal(new StringReader(dateOfCreationWorkEverything), ObjectType.class);

    // Check if all work required metadata fields are set.
    List<String> workMissing = ImportUtils.checkRequiredMetadata(workRequiredFields, metadata,
        xpathNamespaces, null, "test:uri");

    // We have notBefore and notAfter and tag content, workMissing list must be empty!
    if (!workMissing.isEmpty()) {
      System.out.println("\tERROR: Work missing list is not empty: " + workMissing);
      assertTrue(false);
    } else {
      System.out.println("\tOK: Work missing list is empty");
    }
  }

  /**
   * @throws ParserConfigurationException
   * @throws JAXBException
   * @throws XPathExpressionException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testWorkDateOfCreationNotBeforeOnly()
      throws XPathExpressionException, JAXBException, ParserConfigurationException {

    System.out.println("Test dateOfCreation work check [notBefore only]...");

    // Create TG object from TG metadata.
    ObjectType metadata =
        JAXB.unmarshal(new StringReader(dateOfCreationWorkNotBeforeOnly), ObjectType.class);

    // Check if all work required metadata fields are set.
    List<String> workMissing = ImportUtils.checkRequiredMetadata(workRequiredFields, metadata,
        xpathNamespaces, null, "test:uri");

    // We have the notBefore attribute only, we do also need notAfter!
    if (workMissing.size() != 1 && !workMissing.contains(WORK_REQUIRED_FIELDS_DOC)) {
      System.out.println("\tERROR: Work missing list must contain '" + WORK_REQUIRED_FIELDS_DOC
          + "', but doesn't: " + workMissing);
      assertTrue(false);
    } else {
      System.out.println("\tOK: Work missing list contains " + workMissing);
    }
  }

  /**
   * @throws ParserConfigurationException
   * @throws JAXBException
   * @throws XPathExpressionException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testWorkDateOfCreationNotAfterOnly()
      throws XPathExpressionException, JAXBException, ParserConfigurationException {

    System.out.println("Test dateOfCreation work check [notAfter only]...");

    // Create TG object from TG metadata.
    ObjectType metadata =
        JAXB.unmarshal(new StringReader(dateOfCreationWorkNotAfterOnly), ObjectType.class);

    // Check if all work required metadata fields are set.
    List<String> workMissing = ImportUtils.checkRequiredMetadata(workRequiredFields, metadata,
        xpathNamespaces, null, "test:uri");

    // We have the notAfter attribute only, we do also need notBefore!
    if (workMissing.size() != 1 && !workMissing.contains(WORK_REQUIRED_FIELDS_DOC)) {
      System.out.println("\tERROR: Work missing list must contain '" + WORK_REQUIRED_FIELDS_DOC
          + "', but doesn't: " + workMissing);
      assertTrue(false);
    } else {
      System.out.println("\tOK: Work missing list contains " + workMissing);
    }
  }

  /**
   * @throws ParserConfigurationException
   * @throws JAXBException
   * @throws XPathExpressionException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testWorkDateOfCreationOnlyTagContent()
      throws XPathExpressionException, JAXBException, ParserConfigurationException {

    System.out.println("Test dateOfCreation work check [only tag content]...");

    // Create TG object from TG metadata.
    ObjectType metadata =
        JAXB.unmarshal(new StringReader(dateOfCreationWorkOnlyTagContent), ObjectType.class);

    // Check if all work required metadata fields are set.
    List<String> workMissing = ImportUtils.checkRequiredMetadata(workRequiredFields, metadata,
        xpathNamespaces, null, "test:uri");

    // We have only tag content, workMissing list must be empty!
    if (!workMissing.isEmpty()) {
      System.out.println("\tERROR: Work missing list is not empty: " + workMissing);
      assertTrue(false);
    } else {
      System.out.println("\tOK: Work missing list is empty");
    }
  }

  /**
   * @throws ParserConfigurationException
   * @throws JAXBException
   * @throws XPathExpressionException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testWorkDateOfCreationNoAttributesAndTag()
      throws XPathExpressionException, JAXBException, ParserConfigurationException {

    System.out.println("Test dateOfCreation work check [no attributes and tag]...");

    // Create TG object from TG metadata.
    ObjectType metadata =
        JAXB.unmarshal(new StringReader(dateOfCreationWorkNoAttributesAndTag), ObjectType.class);

    // Check if all work required metadata fields are set.
    List<String> workMissing =
        ImportUtils.checkRequiredMetadata(workRequiredFields, metadata, xpathNamespaces, null,
            "test:uri");

    // We have only tag content, workMissing list must be empty!
    if (!workMissing.isEmpty()) {
      System.out.println("\tERROR: Work missing list is not empty: " + workMissing);
      assertTrue(false);
    } else {
      System.out.println("\tOK: Work missing list is empty");
    }
  }

}
