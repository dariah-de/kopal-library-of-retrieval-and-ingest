/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import static org.junit.Assert.assertTrue;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import javax.xml.stream.XMLStreamException;
import org.junit.Test;
import de.langzeitarchivierung.kolibri.util.FileUtils;
import info.textgrid._import.RewriteMethod;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter;
import info.textgrid.utils.linkrewriter.ImportMapping;
import jakarta.xml.bind.JAXB;

/**
 * CHANGELOG
 * 
 * 2017-03-18 - Funk - Tests added for reading TextGrid metadata files.
 * 
 * 2015-09-25 - Funk - Testing rewriting using URIs now! Tests re-ignored!
 * 
 * 2014-02-16 - Funk - Ignored some tests.
 * 
 * 2014-01-17 - Funk - Changed all canonicalPathes to canonicalFile.toURIs for better Windows
 * support.
 * 
 * 2012-03-12 - Funk - Added test for TEI XML files.
 * 
 * 2011-05-23 - Funk - Storing metadata files to disk now.
 * 
 * 2010-10-22 - Funk - Refactored to new metadata schema!
 * 
 * 2010-08-15 - Funk - Title now is taken from filename only, no prefix is added anymore.
 * 
 * 2010-08-11 - Funk - Finished CXF changes. Nice code indeed!
 * 
 * 2010-08-10 - Funk - Adapted to CXF stubs, added JHOVE mimetype extraction.
 * 
 * 2008-06-25 - Funk - Adapted to the new client stub, added metadata storing again.
 * 
 * 2011-06-29 - Funk - First version.
 */

/**
 * <p>
 * Some tests.
 * </p>
 * 
 * @author Stefan Funk, SUB Göttingen
 * @version 2020-05-14
 * @since 2011-06-29
 */

public class TestRenameAndRewrite {

  private static final String TEMPFILE_DELETION_FAILED = "Temporary file could not be deleted!";

  /**
   * <p>
   * Tests the metadata link rewrite.
   * </p>
   * 
   * <p>
   * NOTE The Method RenameAndRewrite is not really tested here! Just rewriting as it! Do write real
   * tests!!
   * </p>
   * 
   * @throws XMLStreamException
   * @throws IOException
   * @throws URISyntaxException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testMetadataLinkRewriting()
      throws IOException, XMLStreamException, URISyntaxException {

    System.out.println("Testing Metadata Link Rewriting:");

    // Set parameters.
    URI uriToRewrite = new URI("00006.work");
    URI rewriteToUri = new URI("textgrid:1234");

    // Get metadata file.
    URL resource = getClass().getClassLoader().getResource("./rewriting/edition_metadata.meta");
    File metadataFile = new File(resource.toURI());
    String resourceString = FileUtils.readFile(metadataFile);
    InputStream rewriteFrom =
        new ByteArrayInputStream(resourceString.getBytes(Charset.forName("UTF-8")));

    System.out.println("Metadata to rewrite: " + resourceString);

    // Create streams for rewriting (rename metadata files, just because we can not read and write
    // to the same file :-)
    File tempFile = File.createTempFile("urgl_", "_argl");
    FileOutputStream rewriteTo = new FileOutputStream(tempFile);

    // Create mapping.
    ImportMapping mapping = new ImportMapping();
    mapping.add(rewriteToUri, uriToRewrite, null, RewriteMethod.XML);

    // Get base path.
    URI basePath = metadataFile.getParentFile().getCanonicalFile().toURI();

    System.out.println("Base path: " + basePath);

    // Rewrite metadata file.
    ConfigurableXMLRewriter metadataRewriter = new ConfigurableXMLRewriter(mapping, false);
    metadataRewriter.recordMissingReferences();
    metadataRewriter.configure(URI.create("internal:textgrid#metadata"));
    metadataRewriter.rewrite(rewriteFrom, rewriteTo);

    String rewrittenString = FileUtils.readFile(tempFile);

    System.out.println("Rewritten metadata: " + rewrittenString);

    // Check for missing references.
    if (metadataRewriter.getMissingReferences() == null
        || !metadataRewriter.getMissingReferences().isEmpty()) {
      System.out.println("Missing reference: " + metadataRewriter.getMissingReferences());
    }

    // Check if rewriting did work.
    ObjectType metadata = JAXB.unmarshal(tempFile, ObjectType.class);
    try {
      if (!metadata.getEdition().getIsEditionOf().equals(rewriteToUri.toString())) {

        System.out.println("Rewriting did NOT work for metadata file");

        assertTrue(false);
      }
    }

    // Delete temp file.
    finally {
      if (!tempFile.delete()) {
        System.out.println("WARNING: " + TEMPFILE_DELETION_FAILED);
      }
    }
  }

  /**
   * <p>
   * Tests the TEI XML link rewriting.
   * </p>
   * 
   * <p>
   * NOTE Ignored because the Method RenameAndRewrite is not really tested here! Just rewriting as
   * it! Do write real tests!!
   * </p>
   * 
   * @throws XMLStreamException
   * @throws IOException
   * @throws URISyntaxException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testTeiXmlLinkRewritingStringBased()
      throws IOException, XMLStreamException, URISyntaxException {

    System.out.println("Testing TEI XML Link Rewriting (string based):");

    // Set parameters.
    URI uriToRewrite1 = new URI("first.pdf");
    URI rewriteToUri1 = new URI("textgrid:1234");
    URI uriToRewrite2 = new URI("folder/second.pdf");
    URI rewriteToUri2 = new URI("textgrid:5678");

    // Get TEI file.
    URL resource = getClass().getClassLoader().getResource("./rewriting/werther1_teilite.xml");
    File metadataFile = new File(resource.toURI());
    String resourceString = FileUtils.readFile(metadataFile);
    InputStream rewriteFrom =
        new ByteArrayInputStream(resourceString.getBytes(Charset.forName("UTF-8")));

    System.out.println("TEI XML to rewrite: " + resourceString);

    // Create streams for rewriting (rename TEI XML files, just because we can not read and write to
    // the same file :-)
    File tempFile = File.createTempFile("urgl_", "_argl");
    FileOutputStream rewriteTo = new FileOutputStream(tempFile);

    // Create mapping.
    ImportMapping mapping = new ImportMapping();
    mapping.add(rewriteToUri1, uriToRewrite1, null, RewriteMethod.XML);
    mapping.add(rewriteToUri2, uriToRewrite2, null, RewriteMethod.XML);

    // Get base path.
    URI basePath = metadataFile.getParentFile().getCanonicalFile().toURI();

    System.out.println("Base path: " + basePath);

    // Rewrite TEI file.
    ConfigurableXMLRewriter metadataRewriter = new ConfigurableXMLRewriter(mapping, false);
    metadataRewriter.recordMissingReferences();
    metadataRewriter.configure(URI.create("internal:tei#tei"));
    metadataRewriter.rewrite(rewriteFrom, rewriteTo);

    String rewrittenString = FileUtils.readFile(tempFile);

    System.out.println("Rewritten TEI XML: " + rewrittenString);

    // Check for missing references.
    if (metadataRewriter.getMissingReferences() == null
        || !metadataRewriter.getMissingReferences().isEmpty()) {
      System.out.println("Missing reference: " + metadataRewriter.getMissingReferences());
    }

    // Check if rewriting did work.
    try {
      if (!rewrittenString.contains("textgrid:1234")
          || !rewrittenString.contains("textgrid:5678")) {

        System.out.println("Rewriting did NOT work for TEI XML file");

        assertTrue(false);
      }
    }

    // Delete temp file.
    finally {
      if (!tempFile.delete()) {
        System.out.println("WARNING: " + TEMPFILE_DELETION_FAILED);
      }
    }
  }

  /**
   * <p>
   * Tests the TEI XML link rewriting.
   * </p>
   * 
   * <p>
   * NOTE Ignored because the Method RenameAndRewrite is not really tested here! Just rewriting as
   * it! Do write real tests!!
   * </p>
   * 
   * @throws XMLStreamException
   * @throws IOException
   * @throws URISyntaxException
   */
  @SuppressWarnings("javadoc")
  @Test
  public void testTeiXmlLinkRewritingFileBased()
      throws IOException, XMLStreamException, URISyntaxException {

    System.out.println("Testing TEI XML Link Rewriting (file based):");

    // Set parameters.
    File fileToRewrite1 =
        new File(getClass().getClassLoader().getResource("./rewriting/first.pdf").getPath());
    URI rewriteToUri1 = new URI("textgrid:1234");
    File fileToRewrite2 = new File(
        getClass().getClassLoader().getResource("./rewriting/folder/second.pdf").getPath());
    URI rewriteToUri2 = new URI("textgrid:5678");

    System.out.println("Linked file 1: " + fileToRewrite1.getCanonicalFile().toURI());
    System.out.println("Linked file 2: " + fileToRewrite2.getCanonicalFile().toURI());

    // Get TEI file.
    URL resource = getClass().getClassLoader().getResource("./rewriting/werther1_teilite.xml");
    File metadataFile = new File(resource.toURI());
    String resourceString = FileUtils.readFile(metadataFile);
    InputStream rewriteFrom =
        new ByteArrayInputStream(resourceString.getBytes(Charset.forName("UTF-8")));

    System.out.println("TEI XML to rewrite: " + resourceString);

    // Create streams for rewriting (rename TEI XML files, just because we can not read and write to
    // the same file :-)
    File tempFile = File.createTempFile("urgl_", "_argl");
    FileOutputStream rewriteTo = new FileOutputStream(tempFile);

    // Create mapping.
    ImportMapping mapping = new ImportMapping();
    mapping.add(rewriteToUri1, fileToRewrite1.getCanonicalFile().toURI(), null, RewriteMethod.XML);
    mapping.add(rewriteToUri2, fileToRewrite2.getCanonicalFile().toURI(), null, RewriteMethod.XML);

    // Get base path.
    URI basePath = metadataFile.getParentFile().getCanonicalFile().toURI();

    System.out.println("Base path: " + basePath);

    // Rewrite TEI file.
    ConfigurableXMLRewriter metadataRewriter = new ConfigurableXMLRewriter(mapping, false);
    metadataRewriter.setBase(basePath);
    metadataRewriter.recordMissingReferences();
    metadataRewriter.configure(URI.create("internal:tei#tei"));
    metadataRewriter.rewrite(rewriteFrom, rewriteTo);

    String rewrittenString = FileUtils.readFile(tempFile);

    System.out.println("Rewritten TEI XML: " + rewrittenString);

    // Check for missing references.
    if (metadataRewriter.getMissingReferences() == null
        || !metadataRewriter.getMissingReferences().isEmpty()) {
      System.out.println("Missing reference: " + metadataRewriter.getMissingReferences());
    }

    // Check if rewriting did work.
    try {
      if (!rewrittenString.contains("textgrid:1234")
          || !rewrittenString.contains("textgrid:5678")) {

        System.out.println("Rewriting did NOT work for TEI XML file");

        assertTrue(false);
      }
    }

    // Delete temp file.
    finally {
      if (!tempFile.delete()) {
        System.out.println("WARNING: " + TEMPFILE_DELETION_FAILED);
      }
    }
  }

}
