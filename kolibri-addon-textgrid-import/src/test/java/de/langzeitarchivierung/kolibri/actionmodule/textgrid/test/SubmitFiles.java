/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt GNU)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid.test;

import java.io.IOException;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.textgrid.SubmitFilesAbs;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import jakarta.activation.DataHandler;
import jakarta.xml.ws.Holder;

/**
 * TODOLOG
 **
 * CHANGELOG
 * 
 * 2017-03-30 - Funk - First version.
 */

/**
 * <p>
 * Extension of ActionModule ProcessDfgViewerMetsAbs for testing. Submits no files using TG-crud.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-02-21
 * @since 2017-03-30
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 */

public class SubmitFiles extends SubmitFilesAbs {

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.textgrid.SubmitFilesAbs#create(de.
   * langzeitarchivierung.kolibri.Step, java.lang.String, java.lang.String, java.lang.String,
   * boolean, java.lang.String, javax.xml.ws.Holder, javax.activation.DataHandler)
   */
  protected void create(Step theStep, String theRbacSessionId, String theLogParameter,
      String theUri, boolean createNewRevision, String theProjectId,
      Holder<MetadataContainerType> theMetadata, DataHandler theData) throws IOException {
    theStep.setStatus(Status.RUNNING, "Fake submission complete!");
  }

}
