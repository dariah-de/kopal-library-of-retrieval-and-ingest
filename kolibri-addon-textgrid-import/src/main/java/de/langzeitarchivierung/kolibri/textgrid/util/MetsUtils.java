/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://wtextgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.textgrid.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import gov.loc.mets.MetsDocument;
import gov.loc.mets.MetsDocument.Mets;
import gov.loc.mets.MetsType;

/**
 * TODOLOG
 **
 * CHANGELOG
 * 
 * 2016-11-25 - Funk - First version.
 */

/**
 * <p>
 * Utilities for METS handling.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-03-03
 * @since 2016-11-25
 */

public class MetsUtils {

  /**
   * <p>
   * Validates a METS file.
   * </p>
   * 
   * @param theMets
   * @return An empty string if valid, a String containing error messages, if not.
   * @throws XmlException
   * @throws IOException
   */
  public static String validateMets(File theMets) throws XmlException, IOException {

    String result = "";

    XmlOptions validateOptions = new XmlOptions();
    ArrayList<XmlError> errorList = new ArrayList<XmlError>();
    validateOptions.setErrorListener(errorList);
    boolean isValid = MetsDocument.Factory.parse(theMets).validate(validateOptions);

    if (!isValid) {
      result += "\n";
      for (int i = 0; i < errorList.size(); i++) {
        XmlError error = (XmlError) errorList.get(i);
        result += "Cause: " + error.getMessage() + "\n";
        result += "Fragment: " + error.getCursorLocation().xmlText() + "\n";
      }
    }

    return result;
  }

  /**
   * @param theMets
   * @param theGroups
   */
  public static Mets stripUnusedFileGroupsFromMetsFile(MetsType theMets, List<String> theGroups) {

    Mets result = null;

    // Delete FileGroup itself.
    // FIXME

    // Delete references in StructMap PHYSICAL.
    // FIXME

    return result;
  }

}
