/**
 * This software is copyright (c) 2021 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.textgrid.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.XMLConstants;
import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import info.textgrid._import.ImportObject;
import info.textgrid._import.ImportSpec;
import info.textgrid.clients.SearchClient;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgsearch.Revisions;
import info.textgrid.namespaces.middleware.tgsearch.TextgridUris;
import info.textgrid.utils.linkrewriter.ConfigurableXMLRewriter;
import info.textgrid.utils.linkrewriter.ImportMapping;

/**
 * TODOLOG
 * 
 * TODO Generalise methods from TGCrudServiceUtilities!
 * 
 **
 * CHANGELOG
 * 
 * 2021-03-23 - Funk - Correct typo. Add two methods from TGCrudServiceUtilities.
 * 
 * 2020-02-18 - Funk - Add methods for checking publish metadata.
 * 
 * 2017-03-19 - Funk - Add latestRevision from both TG-search and TG-search public!
 * 
 * 2017-03-16 - Funk - Add new method to check if we have got an metadata ObjectType or an
 * MetadataContainerType, correct, if necessary!
 * 
 * 2016-08-10 - Funk - Add TG-search queries for URI fetching.
 * 
 * 2015-09-21 - Funk - Add some URI extractions from IMEX files.
 * 
 * 2015-05-27 - Funk - Add some logging for rewriting things.
 * 
 * 2014-07-17 - Funk - First version.
 */

/**
 * <p>
 * Utilities for import policies.
 * </p>
 * 
 * @author Stefan Funk, DAASI International GmbH
 * @version 2021-03-23
 * @since 2014-07-17
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 */

public class ImportUtils {

  // **
  // FINALS
  // **

  public static final String IMEX_LOCAL2URI_FILESUFFIX = "_URI.imex";
  public static final String IMEX_URI2PID_FILESUFFIX = "_PID.imex";
  private static final String REVISION_SEPARATION_CHAR = ".";
  private static final String TEXTGRID_METADATA_SCHEMA_LOCATION =
      "https://textgridlab.org/schema/textgrid-metadata_2010.xsd";
  private static final String URI_PREFIX = "textgrid";

  /**
   * <p>
   * Ignores missing references if reference starts with an ignored prefix, throws exception
   * otherwise.
   * </p>
   * 
   * @param theRewriter
   * @param theUri
   * @param theIgnoredUrlPrefixes
   * @param theStep
   * @param theModeLogging
   * @throws IOException
   */
  private static void checkMissingReferences(ConfigurableXMLRewriter theRewriter, String theUri,
      List<String> theIgnoredUrlPrefixes, Step theStep, String theModeLogging) throws IOException {

    // If the list of missing references is not empty...
    if (!theRewriter.getMissingReferences().isEmpty()) {

      // ...loop missing references list...
      for (String s : theRewriter.getMissingReferences()) {
        // Workaround for LinkRewriter bug TG-1831.
        if (s.equals("null")) {
          continue;
        }

        // ...and go through all prefixes to ignore.
        boolean prefixError = true;
        for (String prefix : theIgnoredUrlPrefixes) {
          if (!s.startsWith(prefix)) {
            // Go to next prefix, if prefix does not apply.
            continue;
          } else {
            // Set prefix error to false if prefix does apply.
            prefixError = false;
          }
        }

        String mode = theRewriter.getImportMode().value();
        ImportMapping iMapping = theRewriter.getMapping();
        // Get import object for TextGrid URI or for local URI.
        ImportObject iObject = iMapping.getImportObjectForLocalURI(URI.create(theUri));
        String message = "for local file or URI '"
            + (iObject != null ? iObject.getTextgridUri()
                : iMapping.getImportObjectForTextGridURI(URI.create(theUri)).getLocalData())
            + "' [mode: " + mode + "]";

        if (prefixError) {
          // FIXME Check if we can do this both in lab import and rep import!
          // FIXME Check base URI check! :-)
          if (theUri.contains(".")) {
            theStep.setStatus(Status.WARNING, "[" + theUri + "] Ignoring revision URI " + theUri
                + " in " + theModeLogging + " file");
          } else {
            throw new IOException("[" + theUri + "] Could not find reference '" + s
                + "' in import map while rewriting " + theModeLogging + " " + message
                + " Do you need to ignore the prefix of the reference above in your config file?");
          }
        } else {
          theStep.setStatus(Status.WARNING, "[" + theUri + "] Ignoring missing " + theModeLogging
              + " reference '" + s + "' " + message);
        }
      }
    }
  }

  /**
   * <p>
   * This is a Generic rewriting method.
   * </p>
   * 
   * @param theUri
   * @param theFile
   * @param theMode
   * @param modeLogging
   * @param theIgnoredUrlPrefixes
   * @param theStep
   * @param theMapping
   * @param theRewritePrefix
   * @throws XMLStreamException
   * @throws IOException
   */
  public static void rewrite(String theUri, File theFile, String theMode, String modeLogging,
      List<String> theIgnoredUrlPrefixes, Step theStep, ImportMapping theMapping,
      String theRewritePrefix) throws XMLStreamException, IOException {

    theStep.setStatus(Status.RUNNING,
        "[" + theUri + "] Rewriting links in " + modeLogging + " file: " + theFile.getName());

    // Rename file temporarily.
    String rewriteFilename = theFile.getName();
    File tempFile = new File(theFile.getParentFile(), theRewritePrefix + theFile.getName());
    // Check for existence.
    if (!theFile.exists()) {
      throw new FileNotFoundException("Original " + modeLogging + " file does not exist!");
    }
    // Rename.
    if (!theFile.renameTo(tempFile)) {
      throw new IOException("Original " + modeLogging + " file could not be renamed!");
    }

    // Set base path.
    URI basePath = theFile.getParentFile().getCanonicalFile().toURI();

    theStep.setStatus(Status.RUNNING, "[" + theUri + "] Base path set to: " + basePath);

    // Create streams for rewriting.
    FileInputStream rewriteFrom = new FileInputStream(tempFile);
    FileOutputStream rewriteTo =
        new FileOutputStream(new File(theFile.getParent(), rewriteFilename));

    // Rewrite aggregation files.
    ConfigurableXMLRewriter rewriter = new ConfigurableXMLRewriter(theMapping, false);
    rewriter.recordMissingReferences();
    rewriter.setBase(basePath);
    rewriter.configure(URI.create(theMode));
    rewriter.rewrite(rewriteFrom, rewriteTo);

    // Close streams and delete temp file.
    rewriteFrom.close();
    rewriteTo.close();

    if (!tempFile.delete()) {
      throw new IOException("Temporary " + modeLogging + " file could not be deleted: "
          + tempFile.getCanonicalPath());
    }

    // Check for missing references, ignore ignored prefixes.
    checkMissingReferences(rewriter, theUri, theIgnoredUrlPrefixes, theStep, modeLogging);
  }

  /**
   * <p>
   * Creates the URI list from the import mapping.
   * </p>
   * 
   * @param theStep
   * @param theImportFolder
   * @return
   * @throws IOException
   */
  public static TextgridUris uriListFromMappingFile(Step theStep, final File theMappingFile)
      throws IOException {

    TextgridUris result = new TextgridUris();

    if (!theMappingFile.exists()) {
      throw new IOException("Import mapping file not found: " + theMappingFile.getAbsolutePath());
    }

    // Get import mapping from IMEX file.
    ImportSpec spec = JAXB.unmarshal(theMappingFile, ImportSpec.class);
    ImportMapping mapping = new ImportMapping(spec);

    // Get URI list from import mapping.
    Iterator<ImportObject> iter = mapping.iterator();
    while (iter.hasNext()) {
      ImportObject o = iter.next();
      if (theMappingFile.getName().endsWith(ImportUtils.IMEX_LOCAL2URI_FILESUFFIX)) {
        result.getTextgridUri().add(o.getTextgridUri());
      } else if (theMappingFile.getName().endsWith(ImportUtils.IMEX_URI2PID_FILESUFFIX)) {
        result.getTextgridUri().add(o.getLocalData());
      } else {
        throw new IOException("Import mapping files must either have file suffix "
            + ImportUtils.IMEX_URI2PID_FILESUFFIX + " or " + ImportUtils.IMEX_LOCAL2URI_FILESUFFIX
            + "!");
      }
    }

    return result;
  }

  /**
   * <p>
   * We want to be able to handle both ObjectType metadata files <object> and MetadataContainerType
   * <tgObjectMetadata> metadata files.
   * </p>
   * 
   * @param theFile
   * @return The metadata object
   */
  public static ObjectType getMetadata(File theFile) {

    ObjectType result;

    // First try to get MetadataContainerType.
    result = JAXB.unmarshal(theFile, MetadataContainerType.class).getObject();
    if (result == null) {
      // If no object is existing, get object and add to result MetadataContainerType.
      result = JAXB.unmarshal(theFile, ObjectType.class);
    }

    return result;
  }

  /**
   * <p>
   * Gets the latest revision of one URI from tgsearch and tgsearch-public.
   * </p>
   * 
   * @return
   * @throws IOException
   */
  public static URI getNewRevisionUri(SearchClient theFirstClient, SearchClient theSecondClient,
      String theUri) throws IOException {

    // Get revision list from first tgsearch client.
    Revisions revisionsResponse1 = theFirstClient.infoQuery().listRevisions(theUri);
    String baseUri = revisionsResponse1.getTextgridUri();
    List<BigInteger> revisionList1 = revisionsResponse1.getRevision();

    // Get revision list from second tgsearch client.
    Revisions revisionsResponse2 = theSecondClient.infoQuery().listRevisions(theUri);
    List<BigInteger> revisionList2 = revisionsResponse2.getRevision();

    // Check for empty lists, throw error if both are empty.
    if (revisionList1.isEmpty() && revisionList2.isEmpty()) {
      throw new IOException("No revision error! Please contact support!");
    }

    // Get latest revisions, set default to 0.
    int newRevision1 = 0;
    if (!revisionList1.isEmpty()) {
      newRevision1 = revisionList1.get(revisionList1.size() - 1).intValue() + 1;
    }
    int newRevision2 = 0;
    if (!revisionList2.isEmpty()) {
      newRevision2 = revisionList2.get(revisionList2.size() - 1).intValue() + 1;
    }

    // Take the biggest value of 1 and 2, and return new URI.
    return URI.create(baseUri + REVISION_SEPARATION_CHAR + Math.max(newRevision1, newRevision2));
  }

  /**
   * <p>
   * Match XPath expression in requiredFields against tgObject, return list of expressions which
   * failed.
   * </p>
   * 
   * TODO Gather with same method from:
   * /kolibri-tgpublish-service/src/main/java/de/langzeitarchivierung/kolibri/publish/util/TGPublishUtils.java
   * 
   * @param requiredFields
   * @param tgobject
   * @param xpathNamespaces
   * @param step
   * @param theURI
   * @return
   * @throws JAXBException
   * @throws ParserConfigurationException
   * @throws XPathExpressionException
   */
  public static List<String> checkRequiredMetadata(final List<String> requiredFields,
      final ObjectType tgobject, Map<String, String> xpathNamespaces, Step step,
      final String theURI)
      throws JAXBException, ParserConfigurationException, XPathExpressionException {

    List<String> missingFields = new ArrayList<String>();

    JAXBContext jc =
        JAXBContext.newInstance(info.textgrid.namespaces.metadata.core._2010.ObjectType.class);

    Marshaller m = jc.createMarshaller();

    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    Document doc = dbf.newDocumentBuilder().newDocument();
    m.marshal(tgobject, doc);

    XPath xpath = XPathFactory.newInstance().newXPath();
    xpath.setNamespaceContext(new NamespaceContextImpl(xpathNamespaces));

    for (String expression : requiredFields) {
      Boolean res = Boolean.parseBoolean(xpath.evaluate(expression, doc));

      if (res) {
        if (step != null) {
          step.setStatus(Status.RUNNING,
              "[" + theURI + "] XPath value existing for [" + expression + " = " + res + "]");
        }
      } else {
        missingFields.add(expression);
        if (step != null) {
          step.setStatus(Status.WARNING,
              "[" + theURI + "] XPath value MISSING for [" + expression + "]");
        }
      }
    }

    return missingFields;
  }

  /**
   * <p>
   * Validate TextGrid metadata file. We do allow both <object> (as in TextGrid metadata XSD schema)
   * and <metadataContainerType> (as in TG-crud WSDL)!
   * </p>
   * 
   * @param theMetadata
   * @throws JAXBException
   * @throws SAXException
   * @throws MalformedURLException
   */
  public static void validateMetadataFile(File theMetadata)
      throws JAXBException, SAXException, MalformedURLException {

    // Create schemaFactory with TextGrid metadata XSD.
    SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    Schema textgridSchema = sf.newSchema(new URL(TEXTGRID_METADATA_SCHEMA_LOCATION));

    // Create objectType unmarshaller.
    JAXBContext objectTypeContext = JAXBContext.newInstance(ObjectType.class);
    Unmarshaller objectTypeUnmarshaller = objectTypeContext.createUnmarshaller();
    objectTypeUnmarshaller.setSchema(textgridSchema);

    // Try unmarshalling...
    objectTypeUnmarshaller.unmarshal(theMetadata);
  }

  /**
   * Strips the revision from the given URI, if existing.
   * </p>
   * 
   * TODO Generalise with TGCrudServiceUtilities! Put it in common tools and packages?
   * 
   * @param theUri
   * @return The URI stripped from revision numbers.
   * @throws IOException
   * @throws IoFault
   */
  public static URI stripRevision(URI theUri) throws IOException {

    if (isBaseUri(theUri)) {
      return theUri;
    }

    return URI.create(theUri.toASCIIString().substring(0, theUri.toASCIIString().indexOf(".")));
  }

  /**
   * <p>
   * Checks if the given URI is a base URI.
   * </p>
   * 
   * TODO Generalise with TGCrudServiceUtilities! Put it in common tools and packages?
   * 
   * @param theUri
   * @return Is it a base URI or is it not a base URI?
   * @throws IOException
   */
  public static boolean isBaseUri(URI theUri) throws IOException {

    if (theUri == null) {
      throw new IOException("URI must not be null!");
    }

    return !theUri.toASCIIString().contains(REVISION_SEPARATION_CHAR);
  }

  /**
   * <p>
   * Removes the URI prefix and the ":", it's not nice to have ":"s in the ES database.
   * </p>
   * 
   * TODO Generalise with TGCrudServiceUtilities! Put it in common tools and packages?
   * 
   * @param theUri
   * @return The URI stripped from its prefix.
   */
  public static String stripUriPrefix(URI theUri) {
    return theUri.toASCIIString().replaceFirst(URI_PREFIX + ":", "");
  }

  // **
  // PRIVATE
  // **

  /**
   * <p>
   * For applying namespaces to XPath, copied from CXF, org.apache.cxf.jaxrs.ext.xml.XMLSource.
   * </p>
   */
  private static class NamespaceContextImpl implements NamespaceContext {

    private Map<String, String> namespaces;

    /**
     * @param namespaces
     */
    public NamespaceContextImpl(Map<String, String> namespaces) {
      this.namespaces = namespaces;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.xml.namespace.NamespaceContext#getNamespaceURI(java.lang.String )
     */
    public String getNamespaceURI(String prefix) {
      return this.namespaces.get(prefix);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.xml.namespace.NamespaceContext#getPrefix(java.lang.String)
     */
    public String getPrefix(String namespace) {

      for (Map.Entry<String, String> entry : this.namespaces.entrySet()) {
        if (entry.getValue().equals(namespace)) {
          return entry.getKey();
        }
      }

      return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.xml.namespace.NamespaceContext#getPrefixes(java.lang.String)
     */
    public Iterator<String> getPrefixes(String namespace) {

      String prefix = this.namespaces.get(namespace);
      if (prefix == null) {
        return null;
      }

      return Collections.singletonList(prefix).iterator();
    }
  }

}
