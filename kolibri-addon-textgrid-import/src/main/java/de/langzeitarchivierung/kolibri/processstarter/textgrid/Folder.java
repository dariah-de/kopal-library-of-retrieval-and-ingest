/*******************************************************************************
 * This software is copyright (c) 2017 by
 * 
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.processstarter.textgrid;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.ProcessQueue;
import de.langzeitarchivierung.kolibri.processstarter.ProcessStarter;
import de.langzeitarchivierung.kolibri.util.FileUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;

/*******************************************************************************
 * CHANGELOG
 * 
 * 	2018-02-12	Funk	Removed static HashMap and ArrayList to class variables.
 * 	2017-03-29	Funk	Copied from MonitorHotfolderBase.
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * This is a general process starter, which looks up a folder and if something
 * new is in it, the folder (or file if wished) is added to the process list.
 * </p>
 * 
 * <p>
 * PRECONDITION: Needs a valid directory out of the -i flag of the WorkflowTool
 * or out of the main config file.
 * </p>
 * 
 * <p>
 * RESULT: Adds all directories (and if wished also files) out of the source
 * directory to the process list and sets the ID (taken from the directory name)
 * and the pathToContentFiles.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2017-03-29
 * @since 2017-03-29
 * @see de.langzeitarchivierung.kolibri.processstarter
 ******************************************************************************/

public class Folder implements ProcessStarter {

	// **
	// CLASS
	// **

	private static Logger defaultLogger = Logger
			.getLogger("de.langzeitarchivierung.kolibri");

	// **
	// STATE
	// **

	private String				inputDirFromCli;
	private ProcessQueue		processQueue;
	private long				startingOffset		= 2000;
	private long				addingOffset		= 1000;
	private String				defaultPolicyName;
	private File				folderDir;
	private boolean				readDirectoriesOnly	= false;
	private boolean				ignoreHiddenFiles	= false;
	private ArrayList<File>		listOfFiles			= new ArrayList<File>();
	private HashMap<File, Long>	fileSizes			= new HashMap<File, Long>();

	/**
	 * Constructor.
	 */
	public Folder() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#run()
	 */
	public void run() {

		// Take the folder's path out of the command line if existing.
		if (this.inputDirFromCli != null && !this.inputDirFromCli.equals("")) {
			this.folderDir = new File(this.inputDirFromCli);
		}

		defaultLogger.log(Level.INFO,
				"Using policy '" + this.defaultPolicyName + "'");
		defaultLogger.log(Level.INFO, "'" + this.folderDir + "' is taken "
				+ "as input folder, waiting for input");
		defaultLogger.log(Level.INFO, "Folder scheduling starts in "
				+ KolibriTimestamp.getDurationInHours(this.startingOffset));

		try {
			Thread.sleep(this.startingOffset);

			processFolderLocation();

		} catch (InterruptedException e) {
			defaultLogger.log(Level.SEVERE,
					"ProcessStarter was interrupted: " + e.getMessage());
			return;
		} catch (IOException e) {
			defaultLogger.log(Level.SEVERE,
					"IOException during ProcessStarter processing: "
							+ e.getMessage());
			return;
		}
	}

	// **
	// INTERNAL
	// **

	/**
	 * <p>
	 * Check the folder for new files and folders.
	 * </p>
	 * 
	 * @throws IOException
	 */
	protected void processFolderLocation() throws IOException {

		File fileList[] = this.folderDir.listFiles();
		File currentFile = null;

		// Check if the given directory exists.
		if (!this.folderDir.exists() || !this.folderDir.isDirectory()) {
			String message = "The given path does not exist or is not a directory";
			defaultLogger.log(Level.SEVERE, message);
			throw new IOException(message);
		}

		defaultLogger.log(Level.INFO, "Checking folder "
				+ this.folderDir.getAbsolutePath() + " for new content");

		// Go through all the files in the given directory.
		int sipsToRun = fileList.length;

		for (int i = 0; i < sipsToRun; i++) {
			currentFile = fileList[i];

			// Process all files not contained in the list yet.
			if (!this.listOfFiles.contains(currentFile)) {
				this.listOfFiles.add(currentFile);

				// Check if file is restricted.
				if (sipIsRestricted(currentFile)) {
					// Add the file to the listOfFiles if so.
					defaultLogger.log(Level.FINEST, "File '" + currentFile
							+ "' is marked as restricted, skipping this file");
				}

				// If not, add the file to the scheduling list.
				else {
					// Calculate and save folder size.
					this.fileSizes.put(currentFile, FileUtils.getCompleteFileSize(currentFile));

					// Give some scheduling log messages.
					Long filesize = this.fileSizes.get(currentFile);
					String timestamp = KolibriTimestamp
							.getDurationInHours(Folder.this.addingOffset);
					defaultLogger.log(Level.INFO,
							"Scheduling file '" + currentFile.getName() + "' ("
									+ filesize
									+ " bytes) for adding to the process list, "
									+ "waiting " + timestamp);

					// Schedule the addition of the new SIP.
					addAssetTask(currentFile);
				}
			}
		}

		defaultLogger.log(Level.INFO, "All current files scheduled, exiting");
	}

	/**
	 * <p>
	 * The name of the process to identify the file's process in the process
	 * list.
	 * </p>
	 * 
	 * @param currentFile
	 *            The process name for this file is created here.
	 * @return String The process name to return.
	 */
	private static String getProcessName(File currentFile) {
		return currentFile.getName();
	}

	/**
	 * @param currentFile
	 *            The current file to set the custom identifier for.
	 * @return HashMap<String, String> The hash map containing all the custom
	 *         IDs.
	 */
	private static HashMap<String, String> getCustomIdList(File currentFile) {
		return new HashMap<String, String>();
	}

	/**
	 * @param currentFile
	 *            The file to create a persistent ID for.
	 * @return String The persistent ID to return.
	 */
	private static String getPersistentIdentifier(File currentFile) {
		return currentFile.getName();
	}

	/**
	 * <p>
	 * Here you have the ability to restrict the added files somehow, e.g. only
	 * add SIPs to the process list that includes a mets.xml file.
	 * </p>
	 * 
	 * @param currentFile
	 * @return TRUE if the file shall be added to the process list, FALSE if
	 *         not.
	 */
	private boolean sipIsRestricted(File currentFile) {

		if (this.readDirectoriesOnly && !currentFile.isDirectory()) {
			return true;
		}
		if (this.ignoreHiddenFiles && currentFile.isHidden()) {
			return true;
		}

		return false;
	}

	/**
	 * @param theCurrentFile
	 */
	private void addAssetTask(File theCurrentFile) {
		try {
			// Get the complete file size.
			long oldFileSize = (this.fileSizes.get(theCurrentFile)).longValue();

			log(Level.FINE, "Recalculating complete file size", theCurrentFile);

			// If it's equal to the latest calculated size, process the
			// element.
			if (oldFileSize == FileUtils.getCompleteFileSize(theCurrentFile)) {
				log(Level.FINE,
						"Complete file size does match the recently calculated size, prepare to process",
						theCurrentFile);

				// Remove the completeFileSize from the HashMap.
				this.fileSizes.remove(theCurrentFile);

				// If it's not existing, add the SIP to the process queue.
				addSipToProcessQueue(theCurrentFile);
			}

			// If the file size is not equal to the latest calculated one,
			// retry later.
			else {
				// Remove file from fileList to schedule again later.
				this.listOfFiles.remove(theCurrentFile);

				log(Level.INFO,
						"Scheduling of file delayed because of file size differences, maybe uploading is still in progress",
						theCurrentFile);
			}
		} catch (Exception e) {
			defaultLogger.log(Level.SEVERE,
					"Error adding file to the process list: " + e.getMessage(),
					theCurrentFile);
			e.printStackTrace();
		}
	}

	/**
	 * <p>
	 * Add the new element to the process queue.
	 * </p>
	 * 
	 * @throws Exception
	 */
	private void addSipToProcessQueue(File theCurrentFile) throws Exception {

		// Add the element using the default policy from the config file and
		// the directory name as a process name to identify the list
		// element.
		ProcessData pd = new ProcessData(Folder.this.defaultPolicyName,
				getProcessName(theCurrentFile));

		// Set the LMER persistent identifier (URN).
		pd.getMetadata().setObjectId(getPersistentIdentifier(theCurrentFile));

		// Set the path to the SIP's content files.
		log(Level.INFO, "Setting path to content files", theCurrentFile);
		pd.setPathToContentFiles(theCurrentFile.getAbsolutePath());

		// If there are some custom IDs, set the custom IDs in newly
		// created ID items.
		if (getCustomIdList(theCurrentFile) != null) {
			HashMap<String, String> customIdList = getCustomIdList(
					theCurrentFile);
			pd.getCustomIds().putAll(customIdList);

			Iterator<String> iter = customIdList.keySet().iterator();

			while (iter.hasNext()) {
				String name = iter.next();
				String value = customIdList.get(name);
				log(Level.FINE, "Added custom ID " + name + " = " + value,
						theCurrentFile);
			}
		}

		Folder.this.processQueue.addElement(pd);

		if (getPersistentIdentifier(theCurrentFile) == null) {
			log(Level.INFO, "Added to the process list", theCurrentFile);
			log(Level.WARNING, "Persistent identifier yet unknown",
					theCurrentFile);
		} else {
			log(Level.INFO,
					"Added to the process list, persistent identifier set to "
							+ getPersistentIdentifier(theCurrentFile),
					theCurrentFile);
		}
	}

	/**
	 * <p>
	 * Just a small logging method.
	 * </p>
	 * 
	 * @param level
	 * @param message
	 * @param theCurrentFile
	 */
	private static void log(Level level, String message, File theCurrentFile) {
		defaultLogger.log(level,
				getProcessName(theCurrentFile) + "  -->  " + message);
	}

	// **
	// GETTERS & SETTERS
	// **

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#setInput
	 * (java.lang.String)
	 */
	public void setInput(String input) {
		this.inputDirFromCli = input;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.langzeitarchivierung.kolibri.processstarter.ProcessStarter#
	 * setProcessQueue(de.langzeitarchivierung.kolibri.ProcessQueue)
	 */
	public void setProcessQueue(ProcessQueue processQueue) {
		this.processQueue = processQueue;
	}

	/**
	 * @param defPolicyName
	 *            The defaultPolicyName to set.
	 */
	public void setDefaultPolicyName(String defPolicyName) {
		this.defaultPolicyName = defPolicyName;
	}

	/**
	 * @return
	 */
	public String getDefaultPolicyName() {
		return this.defaultPolicyName;
	}

	/**
	 * @param hotfolderDir
	 *            The hotfolder directory to set.
	 */
	public void setHotfolderDir(String hotfolderDir) {
		this.folderDir = new File(hotfolderDir);
	}

	/**
	 * @param readDirectoriesOnly
	 *            Should only directories be processed as an asset?
	 */
	public void setReadDirectoriesOnly(boolean readDirectoriesOnly) {
		this.readDirectoriesOnly = readDirectoriesOnly;
	}

	/**
	 * @return Returns the addingOffset.
	 */
	public long getAddingOffset() {
		return this.addingOffset;
	}

	/**
	 * @param addingOffset
	 *            The addingOffset to set.
	 */
	public void setAddingOffset(long addingOffset) {
		this.addingOffset = addingOffset;
	}

	/**
	 * @return
	 */
	public long getStartingOffset() {
		return this.startingOffset;
	}

	/**
	 * @param startingOffset
	 *            The startingOffset to set.
	 */
	public void setStartingOffset(long startingOffset) {
		this.startingOffset = startingOffset;
	}

	/**
	 * @param ignoreHiddenFiles
	 */
	public void setIgnoreHiddenFiles(boolean ignoreHiddenFiles) {
		this.ignoreHiddenFiles = ignoreHiddenFiles;
	}

}
