/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.util.FileUtils;
import de.langzeitarchivierung.kolibri.util.FolderFileFilter;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;

/**
 * TODOLOG
 * 
 * TODO Create test cases for creating aggregation files!
 * 
 * TODO Check thread safety of the URI list! Please use with one thread only!
 */

/**
 * CHANGELOG
 * 
 * 2014-11-19 - Funk - Fix #10588: finally (in ProcessDfgViewerMets).
 * 
 * 2014-09-02 - Funk - Fix #10588: concerning false struct map references.
 * 
 * 2014-07-03 - Funk - Fix #10583: Changed File.separatorChar to URL_SEPARATOR_CHAR creating URIs.
 * 
 * 2014-02-18 - Funk - Fix things using the new URI link rewriter.
 * 
 * 2014-02-14 - Funk - Add delete suffix for deleting aggregations (see ProcessDfgViewerMets).
 * 
 * 2012-08-24 - Funk - Add configurable WORK reference handling.
 * 
 * 2012-02-20 - Funk - Add more documentation.
 * 
 * 2011-09-19 - Funk - Add baseUri flag.
 * 
 * 2011-05-26 - Funk - First version.
 */

/**
 * <p>
 * Creates the aggregation files if a folder is to be processed AND adds all the included files with
 * their filenames.
 * </p>
 * 
 * @author Stefan Funk, DAASI International GmbH
 * @version 2022-09-15
 * @since 2011-05-26
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 */

public class CreateAggregations implements ActionModule {

  // **
  // FINALS
  // **

  private static final String ORE_NS = "http://www.openarchives.org/ore/terms/";
  private static final String RDF_NS = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
  private static String URI_SEPARATOR_CHAR = "/";

  // **
  // STATE
  // **

  private ProcessData processData;
  private Step step;
  private String metadataSuffix = ".meta";
  private String aggregationSuffix = ".aggregation";
  private String collectionSuffix = ".collection";
  private String referenceSuffix = ".reference";
  private String workSuffix = ".work";
  private String deleteSuffix = ".DELETE";
  private boolean sortAlphabetically = false;
  private boolean useBaseUrisInAggregations = false;
  private boolean omitWorkReferences = false;

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>The pathToContentFiles must be set (normally done by the processStarter).</li>
   * <li>A folder structure containing folders :-)</li>
   * </ul>
   * <p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>Creates an aggregation file for each folder contained in the hotfolder.</li>
   * <li>Adds every TextGrid URI (recursively) to this aggregation file's OAI list (including
   * underlying aggregations!).</li>
   * </ul>
   * <p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Set start time.
    long startTime = System.currentTimeMillis();

    // Get import folder.
    File importFolder = new File(this.processData.getPathToContentFiles());

    this.step.setStatus(Status.RUNNING, "Starting to create aggregation files");

    // Loop through all folders.
    try {

      // Start with root folder: Create COLLECTIONs!
      createAggregations(importFolder, this.collectionSuffix);

    } catch (IOException | XMLStreamException e) {
      this.step.setStatus(Status.ERROR, "Error creating aggregations: " + e.getMessage());
      return;
    }

    // Log duration and set status to DONE.
    String duration =
        KolibriTimestamp.getDurationWithMillis(System.currentTimeMillis() - startTime);
    this.step.setStatus(Status.DONE,
        "All aggregations have been sucessfully created in " + duration);
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Loops through all folders and creates collection/aggregation files.
   * </p>
   * 
   * @param theFile
   * @param theSuffix
   * @throws IOException
   * @throws XMLStreamException
   */
  private void createAggregations(File theFile, String theSuffix)
      throws IOException, XMLStreamException {

    // Get file list.
    File files[] = theFile.listFiles(new FolderFileFilter());
    if (files == null) {
      throw new IOException(
          "File list " + theFile.getCanonicalPath() + " is null or empty or both!");
    }

    // Sort file list alphabetically if so configured.
    if (this.sortAlphabetically) {
      Arrays.sort(files);
    }

    for (File f : files) {

      // Create list of aggregated filenames (use relative pathes as in the TG-lab import!).
      List<URI> aggregationFilenames = new ArrayList<URI>();

      // Get file list.
      File aFileList[] = f.listFiles();
      if (aFileList == null) {
        throw new IOException("File list " + f.getCanonicalPath() + " is null or empty or both!");
      }

      // Sort file list alphabetically if so configured.
      if (this.sortAlphabetically) {
        Arrays.sort(aFileList);
      }

      // Get base URI.
      URI baseUri = f.getCanonicalFile().toURI();

      this.step.setStatus(Status.RUNNING, "Base URI set to: " + baseUri);

      for (File af : aFileList) {

        // Create aggregation entry if we have a metadata suffix as filename ending (so we do not
        // add a file twice) and we do have different aggregations, and not a reference file, and
        // not a work file and omitWorkReferences trigger set to TRUE.
        if (af.getName().endsWith(this.metadataSuffix)
            && !af.getName().startsWith(f.getName())
            && !af.getName().endsWith(this.referenceSuffix + this.metadataSuffix)
            && !(af.getName().endsWith(this.workSuffix + this.metadataSuffix)
                && this.omitWorkReferences)) {

          String s = af.getCanonicalFile().toURI().toString();
          URI localURI = URI.create("." + URI_SEPARATOR_CHAR
              + s.substring(baseUri.toString().length()).replace(this.metadataSuffix, ""));

          aggregationFilenames.add(localURI);
        }

        // Create aggregation entry if we do have a directory and not a delete suffix.
        // NOTE We have got NO collections here on non-root level, only aggregations!
        else if (af.isDirectory() && !af.getName().endsWith(this.deleteSuffix)) {

          String s = af.getCanonicalFile().toURI().toString();
          s = s.substring(0, s.lastIndexOf("/"));
          // Get relative local URI from absolute URI by cutting the base URI from the beginning of
          // the string. Remove the "/" at the end.
          URI localURI = URI.create("." + URI_SEPARATOR_CHAR
              + s.substring(baseUri.toString().length()) + URI_SEPARATOR_CHAR
              + s.substring(baseUri.toString().length()) + this.aggregationSuffix);

          aggregationFilenames.add(localURI);
        }

        // Create aggregation entry from reference file entry (created by ProcessDfgViewerMets).
        else if (af.getName().endsWith(this.referenceSuffix)) {

          // Read the link contained in the reference file and put it into the aggregation.
          String s = FileUtils.readFile(af);

          // We can use the string here for creating the URI, because we put an URI in the reference
          // file in ProcessDfgViewerMets!
          aggregationFilenames.add(URI.create(s));

          // Delete reference file and metadata file from disk.
          if (!af.delete()) {
            this.step.setStatus(Status.WARNING,
                "Failed to delete reference data file: " + af.getAbsolutePath());
          }
          File meta = new File(af.getAbsolutePath() + this.metadataSuffix);
          if (!meta.delete()) {
            this.step.setStatus(Status.WARNING,
                "Failed to delete reference metadata file: " + meta.getAbsolutePath());
          }
        }
      }

      // Create collection/aggregation data file in the aggregation folder (as in the TG-lab
      // import!), or not, if deleted suffix is existing.
      if (f.getName().endsWith(this.deleteSuffix)) {

        this.step.setStatus(Status.RUNNING, "Aggregation file NOT created: " + f.getName());

        // Delete aggregation metadata file, if we have a delete suffix.
        File deleteIt =
            new File(f.getAbsolutePath(), f.getName() + theSuffix + this.metadataSuffix);
        if (!deleteIt.delete()) {
          this.step.setStatus(Status.WARNING,
              "Failed to delete aggregation metadata file with delete suffix: "
                  + deleteIt.getCanonicalPath());
        } else {
          this.step.setStatus(Status.RUNNING,
              "Aggregation metadata file deleted: " + deleteIt.getName());
        }
      } else {

        createAggregationData(aggregationFilenames, new File(f, f.getName() + theSuffix));

        String whatDidWeCreate =
            (theSuffix.equals(this.collectionSuffix) ? "COLLECTION" : "AGGREGATION");
        this.step.setStatus(Status.RUNNING,
            whatDidWeCreate + " file created with " + aggregationFilenames.size()
                + " referenced file" + (aggregationFilenames.size() != 1 ? "s" : "") + ": "
                + f.getName() + theSuffix + " " + aggregationFilenames);
      }

      // Now go down into all subfolders.
      createAggregations(f, this.aggregationSuffix);
    }
  }

  /**
   * <p>
   * Creates an aggregation output stream.
   * </p>
   * 
   * TODO Put this method into an aggregation utility module!?
   * 
   * @param theUris
   * @param theFile
   * @throws XMLStreamException
   * @throws IOException
   */
  private void createAggregationData(List<URI> theUris, File theFile)
      throws XMLStreamException, IOException {

    FileOutputStream result = new FileOutputStream(theFile);
    XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
    XMLStreamWriter writer = outputFactory.createXMLStreamWriter(result);

    writer.writeStartDocument();
    writer.setPrefix("rdf", RDF_NS);
    writer.writeStartElement(RDF_NS, "RDF");
    writer.writeNamespace("rdf", RDF_NS);
    writer.writeNamespace("ore", ORE_NS);
    writer.writeStartElement(RDF_NS, "Description");

    for (URI u : theUris) {
      // Strip revision for TG-lab ingest, if configured AND URI is
      // conform to a TextGrid URI.
      if (testUri(u) && this.useBaseUrisInAggregations) {
        URI.create(u.toString().substring(0, u.toString().lastIndexOf('.')));
      }

      // Write elements.
      writer.writeStartElement(ORE_NS, "aggregates");
      writer.writeAttribute("rdf:resource", u.toString());
      writer.writeEndElement();
    }

    writer.writeEndElement();
    writer.writeEndElement();
    writer.writeEndDocument();
    writer.flush();
    writer.close();
    result.close();
  }

  /**
   * <p>
   * Tests if the given string is conform to a TextGrid URI.
   * </p>
   * 
   * @param theUri
   * @return
   */
  private static boolean testUri(URI theUri) {
    return theUri.toString().matches("textgrid:[a-z0-9]+\\.[0-9]+");
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   *
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   * @param step
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param metadataSuffix
   */
  public void setMetadataSuffix(String metadataSuffix) {
    this.metadataSuffix = metadataSuffix;
  }

  /**
   * @param referenceSuffix
   */
  public void setReferenceSuffix(String referenceSuffix) {
    this.referenceSuffix = referenceSuffix;
  }

  /**
   * @param aggregationSuffix
   */
  public void setAggregationSuffix(String aggregationSuffix) {
    this.aggregationSuffix = aggregationSuffix;
  }

  /**
   * @param collectionSuffix
   */
  public void setCollectionSuffix(String collectionSuffix) {
    this.collectionSuffix = collectionSuffix;
  }

  /**
   * @param useBaseUrisInAggregations
   */
  public void setUseBaseUrisInAggregations(boolean useBaseUrisInAggregations) {
    this.useBaseUrisInAggregations = useBaseUrisInAggregations;
  }

  /**
   * @param sortAlphabetically
   */
  public void setSortAlphabetically(boolean sortAlphabetically) {
    this.sortAlphabetically = sortAlphabetically;
  }

  /**
   * @param omitWorkReferences
   */
  public void setOmitWorkReferences(boolean omitWorkReferences) {
    this.omitWorkReferences = omitWorkReferences;
  }

  /**
   * @param workSuffix
   */
  public void setWorkSuffix(String workSuffix) {
    this.workSuffix = workSuffix;
  }

  /**
   * @param deleteSuffix
   */
  public void setDeleteSuffix(String deleteSuffix) {
    this.deleteSuffix = deleteSuffix;
  }

}
