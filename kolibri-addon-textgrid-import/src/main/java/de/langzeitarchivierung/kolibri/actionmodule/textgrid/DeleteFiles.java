/*******************************************************************************
 * This software is copyright (c) 2016 by
 * 
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import info.textgrid.clients.SearchClient;
import info.textgrid.middleware.tgpublish.client.PublishClient;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.RelationsExistFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;

/*******************************************************************************
 * TODOLOG
 * 
 *******************************************************************************
 * CHANGELOG
 * 
 * 	2016-10-07	Funk	Adapted to generic HandleFilesGeneric class.	
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * Action module that deletes all files from an ingest in TG-lab or an
 * nearlyPublic ingest.
 * </p>
 * 
 * @author Stefan Funk, DAASI International GmbH
 * @version 2016-10-11
 * @since 2011-09-30
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 ******************************************************************************/

public class DeleteFiles extends HandleFilesGeneric implements ActionModule {

	// **
	// ABSTRACT METHODS
	// **

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.langzeitarchivierung.kolibri.actionmodule.textgrid.HandleFilesGeneric
	 * #doSingleThing
	 * (info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice
	 * .TGCrudService, info.textgrid.clients.SearchClient,
	 * info.textgrid.middleware.tgpublish.client.PublishClient,
	 * java.lang.String, java.lang.String, java.lang.String, boolean)
	 */
	@Override
	protected void doSingleThing(final TGCrudService theTGcrud,
			final SearchClient theTGSearch, final PublishClient theTGpublish,
			final String theSessionId, final String theLogParameter,
			final String theUri, boolean dryRun) throws ObjectNotFoundFault,
			IoFault, RelationsExistFault, AuthFault {

		if (!dryRun) {
			theTGcrud.delete(theSessionId, theLogParameter, theUri);
		} else {
			try {
				Thread.sleep(472);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	/**
	 * @return
	 */
	@Override
	protected String doo() {
		return "<<delete>>";
	}

}
