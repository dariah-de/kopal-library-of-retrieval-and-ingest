/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.textgrid.util.ImportUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import jakarta.activation.DataHandler;
import jakarta.activation.FileDataSource;
import jakarta.xml.bind.JAXB;
import jakarta.xml.ws.Holder;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2017-03-17 - Funk - createNewRevision added. We do only need to call this class if URIs already
 * are existing... maybe!
 * 
 */

/**
 * <p>
 * Action module that imports all files, and files only, to the TextGrid via a TG-crud service
 * client.
 * </p>
 * 
 * <p>
 * <b>NOTE</b> This method actually CAN be called using threads, do it (actually it IS if using the
 * continue_import policy!)! But only as a single ActionModule in one policy! Either by using more
 * than one folder in the given hotfolder, or by increasing the number of threads in the koLibRI
 * main config file (maxNumberOfThreads)! NOT TESTED YET! Think I changed something, so it may NOT
 * work...
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-07-02
 * @since 2010-11-26
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 */

public abstract class SubmitFilesAbs implements ActionModule {

  // **
  // FINALS
  // **

  private static final String URI_SEPARATION_CHAR = "_";
  private static final String METADATA_NOT_SAVED = "Metadata could not be saved";

  // **
  // STATE (Instance variables)
  // **

  private ProcessData processData;
  private Step step;
  private String tgcrudServerUrl = "";
  private String rbacSessionId = "";
  private String logParameter = "";
  private String projectId = "";
  private boolean storeResponseMetadata = false;
  private boolean deleteIfIngested = false;
  private boolean createNewRevisions = false;
  private String metadataSuffix = ".meta";
  private String uriPrefix = "textgrid";
  private int files = 0;
  private int submitted = 0;
  private long overallDuration = 0;
  private long lastDuration = 0;

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>A working TG-crud service.</li>
   * <li>The pathToContentFiles to be set.</li>
   * <li>The import folder filled with files (and folders), one metadata file for each data file is
   * expected.</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>Imports every file in the import folder (not the folders!).</li>
   * <li>The folder structure is generated (after importing) by the before generated aggregation
   * files, if existing.</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Set start time.
    long startTime = System.currentTimeMillis();

    // Get import folder.
    File importFolder = new File(this.processData.getPathToContentFiles());

    this.step.setStatus(Status.RUNNING,
        "Submitting all files in: " + importFolder.getAbsolutePath());

    // Count TextGrid objects.
    countFiles(importFolder);

    // Submit all files, loop recursively.
    try {
      submitFiles(importFolder);
    } catch (IOException | XPathExpressionException | SAXException | ParserConfigurationException
        | DatatypeConfigurationException | XMLStreamException | InterruptedException
        | ExecutionException e) {
      e.printStackTrace();
      this.step.setStatus(Status.ERROR, "System message: " + e.getMessage());
      return;
    }

    // Log duration and set status to DONE.
    String duration = KolibriTimestamp.getDurationHMSOnly(System.currentTimeMillis() - startTime);
    this.step.setStatus(Status.DONE, "All files have been successfully submitted in " + duration);
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Submit files.
   * </p>
   * 
   * @param theFile
   * @throws IOException
   * @throws XPathExpressionException
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws DatatypeConfigurationException
   * @throws XMLStreamException
   * @throws InterruptedException
   * @throws ExecutionException
   */
  private void submitFiles(File theFile)
      throws IOException, XPathExpressionException, SAXException, ParserConfigurationException,
      DatatypeConfigurationException, XMLStreamException, InterruptedException, ExecutionException {

    File files[] = theFile.listFiles();
    if (files == null) {
      return;
    }

    // Loop all files.
    for (File f : files) {

      // Start recursion if directory is found.
      if (f.isDirectory()) {
        submitFiles(f);
      }

      // Submit files only.
      else if (f.isFile() && !f.getName().endsWith(this.metadataSuffix)) {

        long singleStart = System.currentTimeMillis();

        String uri = submitSingleFile(f);

        long singleDuration = System.currentTimeMillis() - singleStart;
        this.overallDuration += singleDuration;

        this.submitted++;

        // Log duration.
        this.step.setStatus(Status.RUNNING,
            "[" + uri + "] -- " + this.submitted + "/" + this.files + " -- Submission completed in "
                + KolibriTimestamp.getDurationWithMillis(singleDuration));

        // Some duration computations.
        this.lastDuration = this.overallDuration / this.submitted;

        if (this.submitted % 50 == 0) {
          long eta = (this.files - this.submitted) * this.lastDuration;

          this.step.setStatus(Status.RUNNING,
              "Overall duration of last " + this.submitted + ": "
                  + KolibriTimestamp.getDurationHMSOnly(this.overallDuration));
          this.step.setStatus(Status.RUNNING, "Average duration/object of last " + this.submitted
              + ": " + KolibriTimestamp.getDurationWithMillis(this.lastDuration));
          this.step.setStatus(Status.RUNNING,
              "Expected duration of remaining " + (this.files - this.submitted) + ": approx. "
                  + KolibriTimestamp.getDurationHMOnly(eta));
        }
      }
    }
  }

  /**
   * @param theFile
   * @return
   */
  private String submitSingleFile(File theFile) throws IOException {

    // Get data file (or aggregation data file), depending on filename.
    File metadataFile = new File(theFile.getParentFile(), theFile.getName() + this.metadataSuffix);
    ObjectType metadata = ImportUtils.getMetadata(metadataFile);
    DataHandler data = new DataHandler(new FileDataSource(theFile));

    if (!theFile.getName().contains(URI_SEPARATION_CHAR)) {
      throw new IOException("File was not created by koLibRI (prefix URI missing in filename): "
          + theFile.getName() + ", maybe you mixed up policies?");
    }

    String uri = this.uriPrefix + ":"
        + theFile.getName().substring(0, theFile.getName().indexOf(URI_SEPARATION_CHAR));

    // Check title and format tags.
    String title = metadata.getGeneric().getProvided().getTitle().get(0);
    String format = metadata.getGeneric().getProvided().getFormat();
    if (title == null || title.equals("") || format == null || format.equals("")) {
      throw new IOException("[" + uri + "] Both title and format must be set!");
    }

    // Create metadata holder.
    MetadataContainerType metadataContainer = new MetadataContainerType();
    metadataContainer.setObject(metadata);
    Holder<MetadataContainerType> metadataHolder =
        new Holder<MetadataContainerType>(metadataContainer);

    this.step.setStatus(Status.RUNNING,
        "[" + uri + "] " + title + " (" + format + ", " + theFile.length() + " bytes)");

    // Create!
    create(this.step, this.rbacSessionId, this.logParameter, uri, this.createNewRevisions,
        this.projectId, metadataHolder, data);

    // Log revision URI if newly revisioned!
    if (this.createNewRevisions) {
      this.step.setStatus(Status.WARNING,
          "[" + uri + "] New revision "
              + metadataHolder.value.getObject().getGeneric().getGenerated().getRevision()
              + " created!");
    }

    // Delete successfully imported data and metadata files.
    if (this.deleteIfIngested && (!metadataFile.delete() || !theFile.delete())) {
      this.step.setStatus(Status.WARNING,
          "[" + uri + "] Some files could not be removed from temp folder");
    }

    // Store metadata file, if configured. Use old file name.
    if (this.storeResponseMetadata) {
      try {
        String oldFileName = metadataFile.getName()
            .substring(metadataFile.getName().indexOf(URI_SEPARATION_CHAR) + 1);
        FileOutputStream metadataStream =
            new FileOutputStream(new File(metadataFile.getParentFile(), oldFileName));
        JAXB.marshal(metadataHolder.value.getObject(), metadataStream);
        metadataStream.close();

        this.step.setStatus(Status.RUNNING,
            "[" + uri + "] Stored response metadata to " + oldFileName);

      } catch (FileNotFoundException e) {
        this.step.setStatus(Status.WARNING,
            "[" + uri + "] Metadata could not be saved: " + e.getMessage());
      } catch (IOException e) {
        this.step.setStatus(Status.WARNING,
            "[" + uri + "] " + METADATA_NOT_SAVED + ": " + e.getMessage());
      }
    }

    return uri;
  }

  /**
   * <p>
   * Count TextGrid objects.
   * </p>
   * 
   * @param theFile
   */
  private void countFiles(File theFile) {

    File files[] = theFile.listFiles();
    if (files == null) {
      return;
    }

    for (File f : files) {
      // Count files.
      if (f.isFile() && !f.getName().endsWith(this.metadataSuffix)) {
        this.files++;
      }
      // Count recursively.
      else if (f.isDirectory()) {
        countFiles(f);
      }
    }
  }

  /**
   * @param theStep
   * @param theRbacSessionId
   * @param theLogParameter
   * @param theUri
   * @param createNewRevision
   * @param theProjectId
   * @param theMetadata
   * @param theData
   * @throws IOException
   */
  protected abstract void create(Step theStep, String theRbacSessionId, String theLogParameter,
      String theUri, boolean createNewRevision, String theProjectId,
      Holder<MetadataContainerType> theMetadata, DataHandler theData) throws IOException;

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param processData The processData to set.
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   * @param step The step to set.
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param rbacSessionId
   */
  public void setRbacSessionId(String rbacSessionId) {
    this.rbacSessionId = rbacSessionId;
  }

  /**
   * @param projectId
   */
  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  /**
   * @param tgcrudServerUrl
   */
  public void setTgcrudServerUrl(String tgcrudServerUrl) {
    this.tgcrudServerUrl = tgcrudServerUrl;
  }

  /**
   * @return
   */
  public String getTgcrudServerUrl() {
    return this.tgcrudServerUrl;
  }

  /**
   * @param logParameter
   */
  public void setLogParameter(String logParameter) {
    this.logParameter = logParameter;
  }

  /**
   * @param storeResponseMetadata
   */
  public void setStoreResponseMetadata(boolean storeResponseMetadata) {
    this.storeResponseMetadata = storeResponseMetadata;
  }

  /**
   * @param deleteIfIngested
   */
  public void setDeleteIfIngested(boolean deleteIfIngested) {
    this.deleteIfIngested = deleteIfIngested;
  }

  /**
   * @param createNewRevisions
   */
  public void setCreateNewRevisions(boolean createNewRevisions) {
    this.createNewRevisions = createNewRevisions;
  }

  /**
   * @param metadataSuffix
   */
  public void setMetadataSuffix(String metadataSuffix) {
    this.metadataSuffix = metadataSuffix;
  }

  /**
   * @param uriPrefix
   */
  public void setUriPrefix(String uriPrefix) {
    this.uriPrefix = uriPrefix;
  }

}
