/**
 * This software is copyright (c) 2021 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Wolfgang Pempe (pempe@saphor.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.transform.TransformerException;
import org.apache.cxf.jaxrs.ext.xml.XMLSource;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.textgrid.util.ImportUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import info.textgrid.clients.SearchClient;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import jakarta.xml.bind.JAXB;

/**
 * CHANGELOG
 * 
 * 2021-03-26 - Funk - Merge and format only!
 * 
 * 2020-05-18 - Funk - Using ImportUtils for reading metadata files now.
 * 
 * 2015-09-25- Funk - Added Tika support for mimetype extraction. Extending
 * DFGViewerMetadataProcessor now.
 *
 * 2013-03-08 - Veentjer - Copied from DfgViewerMetadataProcessor.
 */

/**
 * <p>
 * This metadata processor takes the generated TextGrid metadata files from the
 * TextgridMetadataProcessor and adds all information from the DFG Viewer METS file (MODS and/or TEI
 * metadata).
 * </p>
 * 
 * <p>
 * You can choose if the root aggregation will be an edition or a collection, all other aggregations
 * will be just that, aggregations.
 * </p>
 * 
 * @author Stefan Funk, DAASI International GmbH
 * @version 2021-03-26
 * @since 2012-02-20
 */

public class TeiHeaderMetadataProcessor extends DfgViewerMetadataProcessor {

  // **
  // FINALS
  // **

  private static final HashMap<String, String> NO_PARAMS = new HashMap<String, String>();

  // **
  // STATE
  // **

  private String rbacSessionId;
  private String tgsearchServerUrl;
  private String projectId;

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>Already generated TextGrid metadata files in the temp folder.</li>
   * <li>A path to the content files (normally provided by the processStarter).</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>Enhanced metadata from the DMD sections of the DFG Viewer METS file.</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Set start time.
    long startTime = System.currentTimeMillis();

    // Set status to RUNNING.
    this.step.setStatus(Status.RUNNING, "TEI metadata processing started");

    // Get import folder and METS file.
    File importFolder = new File(this.processData.getPathToContentFiles());

    // First XML file is chosen as TEI file, there should be only on XML TEI file per job.
    for (File file : this.processData.getFileList()) {
      if (file.getName().endsWith(".xml")) {
        this.sourceFile = file;
      }
    }

    // Checking if collection or edition shall be used for the root folder.
    if (!this.rootAggregationMimetype.equals(TextGridMimetypes.EDITION)
        && !this.rootAggregationMimetype.equals(TextGridMimetypes.COLLECTION)) {
      this.step.setStatus(Status.ERROR, "Invalid mimetype: " + this.rootAggregationMimetype
          + "! Only Edition or Collection can be used!");
      return;
    } else {
      this.step.setStatus(Status.RUNNING,
          "Root agregation mimetype: " + this.rootAggregationMimetype);
    }

    // Check rootAggregationMimetype, extract EDITION and WORK metadata or COLLECTION. AGGREGATION
    // metadata is transformed in ITEM section below.
    try {
      // Get root aggregation file.
      File rootAggregation = new File(this.sourceFile.getParent(),
          this.sourceFile.getParentFile().getName() + this.aggregationSuffix);

      // Check for root edition and work.
      if (this.rootAggregationMimetype.equals(TextGridMimetypes.EDITION)) {
        transformEditionAndWorkMetadata(rootAggregation);
      }

      // Loop through all the files, and get ITEM metadata out of the METS.
      processItemMetadata(importFolder);

      // Log duration and set status to DONE.
      String duration =
          KolibriTimestamp.getDurationWithMillis(System.currentTimeMillis() - startTime);
      this.step.setStatus(Status.DONE, "TEI metadata added to TextGrid metadata in " + duration);

    } catch (FileNotFoundException e) {
      this.step.setStatus(Status.ERROR, e.getClass().getSimpleName() + ": " + e.getMessage());
      return;
    } catch (TransformerException e) {
      this.step.setStatus(Status.ERROR, e.getClass().getSimpleName() + ": " + e.getMessage());
      return;
    } catch (IOException e) {
      this.step.setStatus(Status.ERROR, e.getClass().getSimpleName() + ": " + e.getMessage());
      return;
    }
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Adds additional metadata from the METS file via XSLT stylesheet, provided by the
   * itemTransformationFile. Title information can also be changed.
   * </p>
   * 
   * @param theFileList
   * @throws IOException
   * @throws TransformerException
   * @throws FileNotFoundException
   */
  private void processItemMetadata(File theFile)
      throws FileNotFoundException, TransformerException, IOException {

    File files[] = theFile.listFiles();
    if (files == null) {
      throw new IOException(
          "File list " + theFile.getCanonicalPath() + " is null or empty or both!");
    }

    // Loop files.
    for (File f : files) {

      String logName = "[" + f.getName() + "] ";

      // Recurse if directory is found.
      if (f.isDirectory()) {
        processItemMetadata(f);
      }

      // Check metadata files.
      else if (f.getName().endsWith(this.metadataSuffix)) {

        // Get metadata object from file and check format.
        ObjectType oldMetadata = ImportUtils.getMetadata(f);

        // Get original format.
        String format = oldMetadata.getGeneric().getProvided().getFormat();

        this.step.setStatus(Status.RUNNING, logName + "Found format: " + format);

        // Only for XML / TEI file for now.
        if (format.equals(TextGridMimetypes.XML)) {
          transformItemMetadata(f);
        }
      }
    }
  }

  /**
   * @param theFormat
   * @param theMetadataFile
   * @throws IOException
   * @throws TransformerException
   * @throws FileNotFoundException
   */
  private void transformItemMetadata(File theMetadataFile)
      throws FileNotFoundException, TransformerException, IOException {

    // Get metadata object.
    ObjectType oldMetadata = ImportUtils.getMetadata(theMetadataFile);

    // Get log name.
    String logName = "[" + theMetadataFile.getName() + "] ";

    String teiFileId = "";
    if (!theMetadataFile.getAbsolutePath()
        .equals(this.sourceFile.getAbsolutePath() + this.metadataSuffix)) {
      teiFileId = theMetadataFile.getName().substring(0,
          theMetadataFile.getName().lastIndexOf(this.metadataSuffix));
    }

    this.step.setStatus(Status.RUNNING,
        logName + "Transforming ITEM metadata from TEI using FILE ID: " + teiFileId);

    // FIXME Seems that JHOVE technical metadata from the custom TextGrid metadata is getting lost
    // here! Doesn't matter at the moment, because we do not want to store it inside the TextGrid
    // metadata anyway!
    // FIXME Implement with DARIAH compliant workflow if needed!

    // Transform.
    File itemTransformation = new File(this.itemTransformationFile);
    HashMap<String, String> parameters = new HashMap<String, String>();
    // TODO Do we need this for TEI?
    parameters.put("teiid", teiFileId);
    String itemMetadata = transform(new FileInputStream(this.sourceFile),
        new FileInputStream(itemTransformation), parameters);

    // TODO REMOV DEBUG OUTPUT!
    //
    // System.out.println("Item metadata: " + itemMetadata);
    //
    // TODO REMOV DEBUG OUTPUT!

    // Get item metadata object.
    ObjectType newMetadata = JAXB.unmarshal(new StringReader(itemMetadata), ObjectType.class);

    // Check mimetype as format (from METS file or from Tika).
    checkFormat(oldMetadata, newMetadata, logName);

    // Check if we have got a valid title list in the new metadata, if not,
    // take the old title list.
    List<String> oldTitles = oldMetadata.getGeneric().getProvided().getTitle();
    List<String> newTitles = newMetadata.getGeneric().getProvided().getTitle();
    if (newTitles.isEmpty() || newTitles.get(0).equals("")) {
      newMetadata.getGeneric().getProvided().getTitle().clear();
      newMetadata.getGeneric().getProvided().getTitle().addAll(oldTitles);
    } else {
      // Only change item titles, if item is the METS file itself.
      if (teiFileId.equals("")) {
        // TODO... ???
        // changeTitleInMetsMetadataFile(itemTitles);
      }
    }

    // Store new metadata object.
    MetadataContainerType newMetadataContainer = new MetadataContainerType();
    newMetadataContainer.setObject(newMetadata);
    JAXB.marshal(newMetadataContainer, theMetadataFile);

    this.step.setStatus(Status.RUNNING, logName + "ITEM metadata merge complete");
  }

  /**
   * @param theFile
   * @throws IOException
   * @throws TransformerException
   * @throws FileNotFoundException
   */
  private void transformEditionAndWorkMetadata(File theEditionFile)
      throws FileNotFoundException, TransformerException, IOException {

    // Get edition and work metadata file.
    File editionMetadataFile = new File(theEditionFile.getAbsolutePath() + this.metadataSuffix);

    ObjectType oldEditionMetadata = ImportUtils.getMetadata(editionMetadataFile);

    String workFileURI = getOrCreateWorkFile(theEditionFile);

    // Get log name and format.
    String logName = "[" + theEditionFile.getName() + "] ";
    List<String> oldEditionTitles = oldEditionMetadata.getGeneric().getProvided().getTitle();

    this.step.setStatus(Status.RUNNING,
        logName + "Transforming EDITION and WORK metadata from METS");

    // Transform edition and work metadata.
    File editionTransformation = new File(this.editionTransformationFile);
    HashMap<String, String> parameters = new HashMap<String, String>();
    parameters.put("tguriwork", workFileURI);
    String editionMetadata = transform(new FileInputStream(this.sourceFile),
        new FileInputStream(editionTransformation), parameters);

    // Get edition metadata object.
    ObjectType newEditionMetadata =
        JAXB.unmarshal(new StringReader(editionMetadata), ObjectType.class);

    // Set format to EDITION.
    newEditionMetadata.getGeneric().getProvided().setFormat(TextGridMimetypes.EDITION);

    // Check if we have got a valid title list in the new metadata, if not,
    // take the old title list.
    List<String> editionTitles = newEditionMetadata.getGeneric().getProvided().getTitle();

    if (editionTitles.isEmpty() || editionTitles.get(0).equals("")) {
      newEditionMetadata.getGeneric().getProvided().getTitle().clear();
      newEditionMetadata.getGeneric().getProvided().getTitle().addAll(oldEditionTitles);
    } else {
      changeTitleInMetadataFile(editionTitles);
    }

    // Store new edition metadata object.
    MetadataContainerType newEditionMetadataContainer = new MetadataContainerType();
    newEditionMetadataContainer.setObject(newEditionMetadata);
    JAXB.marshal(newEditionMetadataContainer, editionMetadataFile);

    this.step.setStatus(Status.RUNNING, logName + "EDITION metadata merge complete");
  }

  /**
   * @param theEditionFile
   * @return
   * @throws FileNotFoundException
   * @throws TransformerException
   * @throws IOException
   */
  private String getOrCreateWorkFile(File theEditionFile)
      throws FileNotFoundException, TransformerException, IOException {

    String workFileRepUri = workUriFromRep();
    if (workFileRepUri != null) {
      this.step.setStatus(Status.RUNNING,
          "Work with matching uniform title found in target project: " + workFileRepUri);
      return workFileRepUri;
    }

    // Get log name and format.
    String logName = "[" + theEditionFile.getName() + "] ";

    File workFile = new File(theEditionFile.getParentFile(),
        theEditionFile.getParentFile().getName() + this.workSuffix);

    File workMetadataFile = new File(workFile.getAbsolutePath() + this.metadataSuffix);

    ObjectType oldWorkMetadata = ImportUtils.getMetadata(workMetadataFile);
    List<String> oldWorkTitles = oldWorkMetadata.getGeneric().getProvided().getTitle();

    // Get WORK metadata out of the METS.
    File workTransformation = new File(this.workTransformationFile);
    String workMetadata = transform(new FileInputStream(this.sourceFile),
        new FileInputStream(workTransformation), NO_PARAMS);

    // Get work metadata object.
    ObjectType newWorkMetadata = JAXB.unmarshal(new StringReader(workMetadata), ObjectType.class);

    // Set format to WORK.
    newWorkMetadata.getGeneric().getProvided().setFormat(TextGridMimetypes.WORK);

    // Check if we have got a valid first title in the new work metadata, if not, take the old one.
    List<String> workTitles = newWorkMetadata.getGeneric().getProvided().getTitle();
    if (workTitles.isEmpty() || workTitles.get(0).equals("")) {
      newWorkMetadata.getGeneric().getProvided().getTitle().clear();
      newWorkMetadata.getGeneric().getProvided().getTitle().addAll(oldWorkTitles);
    }

    // Store new work metadata object.
    MetadataContainerType newWorkMetadataContainer = new MetadataContainerType();
    newWorkMetadataContainer.setObject(newWorkMetadata);
    JAXB.marshal(newWorkMetadataContainer, workMetadataFile);

    this.step.setStatus(Status.RUNNING, logName + "WORK metadata merge complete");

    return workFile.getName();
  }

  /**
   * @return
   * @throws FileNotFoundException
   */
  private String workUriFromRep() throws FileNotFoundException {

    this.step.setStatus(Status.RUNNING,
        "Querying tgsearch for work with matching uniform title in target project");

    String uniformTitle = getUniformTitleFromTeiFile(this.sourceFile);

    // TODO Using new textgrid-clients! Please check if this is correct!
    SearchClient sc = new SearchClient(this.tgsearchServerUrl);
    Response res = sc
        .searchQuery()
        .setSid(this.rbacSessionId)
        .setQuery("format:\"text/tg.work+xml\" project@id:.@\"" + this.projectId + "\"")
        .execute();

    for (ResultType r : res.getResult()) {
      for (String title : r.getObject().getGeneric().getProvided().getTitle()) {
        if (title.equals(uniformTitle)) {
          return r.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
        }
      }
    }

    this.step.setStatus(Status.RUNNING,
        "No work with matching uniform title found in target project");

    return null;
  }

  /**
   * @param teiFile
   * @return
   * @throws FileNotFoundException
   */
  public static String getUniformTitleFromTeiFile(File teiFile) throws FileNotFoundException {

    XMLSource source = new XMLSource(new FileInputStream(teiFile));
    source.setBuffering();

    Map<String, String> namespaces = new HashMap<String, String>();
    namespaces.put("tei", "http://www.tei-c.org/ns/1.0");
    String title = source
        .getValue("//tei:fileDesc/tei:titleStmt//tei:title[@type='uniform']/text()", namespaces);

    return title;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param rbacSessionId
   */
  public void setRbacSessionId(String rbacSessionId) {
    this.rbacSessionId = rbacSessionId;
  }

  /**
   * @param tgsearchServerUrl
   */
  public void setTgsearchServerUrl(String tgsearchServerUrl) {
    this.tgsearchServerUrl = tgsearchServerUrl;
  }

  /**
   * @param projectId
   */
  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

}
