/*******************************************************************************
 * This software is copyright (c) 2017 by
 * 
 * - TextGrid Consortium (http://www.textgrid.de) - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Wolfgang Pempe (pempe@saphor.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.textgrid.util.MetsUtils;
import de.langzeitarchivierung.kolibri.util.FileUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import gov.loc.mets.DivType;
import gov.loc.mets.DivType.Fptr;
import gov.loc.mets.FileType;
import gov.loc.mets.MetsDocument;
import gov.loc.mets.MetsType;
import gov.loc.mets.MetsType.FileSec.FileGrp;
import gov.loc.mets.StructLinkType.SmLink;
import gov.loc.mets.StructMapType;
import info.textgrid.middleware.common.TextGridMimetypes;

/**
 * TODOLOG
 * 
 * TODO Re-implement METS-downloading-via-URL issue!
 * 
 **
 * CHANGELOG
 *
 * 2016-11-23 - Funk - Added stripUnusedFileGroupsFromMetsFile.
 * 
 * 2016-11-08 - Funk - Corrected structMapLogicalAggregationName's name again!
 * 
 * 2016-11-02 - Funk - Corrected structMapLogicalAggregationName's name.
 * 
 * 2016-10-04 - Funk - Testing if all the physical structMap IDs are existing in the structLink
 * section as TO attributes.
 * 
 * 2014-11-19 - Funk - Fixed #10588 finally (in ProcessDfgViewerMets), using URIs for file
 * references in reference files now! 2014-09-01 Funk Removed METS file URL downloading support due
 * to errors.
 * 
 * 2014-08-15 - Funk - Fixed double METS download and wrong METS filename if using METS file URL
 * setting.
 * 
 * 2014-04-11 - Funk - Added URL downloading support.
 * 
 * 2014-02-13 - Funk - StructMapLogical can be processed or left out by configuration.
 * 
 * 2012-08-27 - Funk - Added StructMap aggregation names to configuration.
 * 
 * 2012-02-20 - Funk - First version.
 */

/**
 * <p>
 * Reads and processes a DFG Viewer METS file. DOES NOT CHANGE THE METS FILE!
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2017-03-30
 * @since 2012-02-20
 */

public abstract class ProcessDfgViewerMetsAbs implements ActionModule {

  // **
  // FINALS
  // **

  private final static String ROOT_ID = "ROOT";
  private static final String ROOT_SEPARATOR = "__";
  private final static String WORK_ID = "WORK";
  private final static String STRUCTMAP_LOGICAL_TOKEN = "LOGICAL";
  private final static String STRUCTMAP_PHYSICAL_TOKEN = "PHYSICAL";
  private final static String XLINK_FROM_TOKEN = "xlink:from";
  private final static String XLINK_TO_TOKEN = "xlink:to";
  private final static String NAME_TYPE_SEPARATOR = "__";

  // **
  // STATE
  // **

  private ProcessData processData;
  private Step step;
  private String aggregationSuffix = ".aggregation";
  private String workSuffix = ".work";
  private String referenceSuffix = ".reference";
  private String deleteSuffix = ".DELETE";
  private List<String> fileGroups = new ArrayList<String>();
  private HashMap<String, File> itemMap = new HashMap<String, File>();
  private HashMap<String, File> fileMap = new HashMap<String, File>();
  private String rootAggregationMimetype = TextGridMimetypes.COLLECTION;
  private String structMapPhysicalAggregationName = "StructMapPhysical";
  private String structMapLogicalAggregationName = "StructMapLogical";
  private boolean processStructMapPhysical = true;
  private boolean processStructMapLogical = true;
  private boolean ignoreMetsValidationFailures = false;
  private File rootFolder = null;

  /**
   * <p>
   * Reads an DFG Viewer METS file and creates files and folders from it.
   * </p>
   * 
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>The PathToContentFiles set (normally done by the appropriate processStarter) containing an
   * appropriate DFG Viewer METS file.</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>A complete folder structure according to the METS' StructMap (physical and logical).</li>
   * <li>A hash map of FILEIDs and URLs to map METS file URLs to PIDs later.</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Set start time.
    long startTime = System.currentTimeMillis();

    // Start and set status to RUNNING.
    this.step.setStatus(Status.RUNNING,
        "DFG Viewer METS file is beeing processed");

    // Get import folder.
    File importFolder = new File(this.processData.getPathToContentFiles());

    // Checking if collection or edition shall be used for the root folder.
    if (!this.rootAggregationMimetype.equals(TextGridMimetypes.EDITION)
        && !this.rootAggregationMimetype
            .equals(TextGridMimetypes.COLLECTION)) {
      this.step.setStatus(Status.ERROR,
          "Invalid mimetype: " + this.rootAggregationMimetype
              + "! Only Edition or Collection can be used!");
      return;
    } else {
      this.step.setStatus(Status.RUNNING, "Root aggregation mimetype: "
          + this.rootAggregationMimetype);
    }

    // Create root folder, and eventually work file.
    try {
      this.rootFolder = new File(importFolder, ROOT_ID + ROOT_SEPARATOR
          + this.processData.getProcessName());
      createFile(this.rootFolder, ROOT_ID, true);

      this.step.setStatus(Status.RUNNING, "Root folder created: "
          + this.rootFolder.getAbsolutePath());

      // Create work file, if edition shall be used.
      if (this.rootAggregationMimetype
          .equals(TextGridMimetypes.EDITION)) {
        File workFile = new File(this.rootFolder,
            this.processData.getProcessName() + this.workSuffix);
        createFile(workFile, WORK_ID, false);
        this.step.setStatus(Status.RUNNING,
            "Work object created: " + workFile.getAbsolutePath());
      }
    } catch (IOException e) {
      this.step.setStatus(Status.ERROR,
          e.getClass().getSimpleName() + ": " + e.getMessage());
      return;
    }

    // Then move METS file from import folder to root folder.
    File metsSrc = new File(importFolder,
        this.processData.getProcessName());
    File metsDst = new File(this.rootFolder,
        this.processData.getProcessName());
    if (!metsSrc.renameTo(metsDst)) {
      this.step.setStatus(Status.ERROR,
          "Failed to move METS file to root folder: "
              + metsSrc.getAbsolutePath());
      return;
    }

    // Remove file list from source METS file.
    this.processData.getFileList().remove(metsSrc);

    // Add file list to destination METS file.
    this.processData.getFileList().add(metsDst);

    // Read the METS file. Using the METS 1.4 with XMLBeans, koLibRI has got
    // METS 1.4 embedded I think, but to use a newer version of THE METS
    // should be no problem, I suppose... and maybe it works anyway :-)
    try {
      this.step.setStatus(Status.RUNNING,
          "Processing as METS file: " + metsDst.getAbsolutePath());

      // Validate the METS, if configured so.
      if (!this.ignoreMetsValidationFailures) {
        this.step.setStatus(Status.RUNNING, "Validating METS...");

        String validationErrors = MetsUtils.validateMets(metsDst);

        if (!validationErrors.equals("")) {
          this.step.setStatus(Status.ERROR,
              "METS validation failed: " + validationErrors);
          return;
        } else {
          this.step.setStatus(Status.RUNNING, "...OK");
        }
      }
    } catch (XmlException e) {
      this.step.setStatus(Status.ERROR,
          e.getClass().getSimpleName() + ": " + e.getMessage());
      return;
    } catch (IOException e) {
      this.step.setStatus(Status.ERROR,
          e.getClass().getSimpleName() + ": " + e.getMessage());
      return;
    }

    try {
      // Read the METS.
      // TODO REMOVE OLD CODE?? final MetsType mets = ((MetsDocument)
      // XmlObject.Factory.parse(metsDst)).getMets();
      final MetsType mets = MetsDocument.Factory.parse(metsDst).getMets();

      // Get struct map list.
      List<StructMapType> structMapList = mets.getStructMapList();

      // Loop through struct map list.
      StructMapType structMapLogical = null;
      StructMapType structMapPhysical = null;
      for (StructMapType smt : structMapList) {
        if (smt.getTYPE().equals(STRUCTMAP_LOGICAL_TOKEN)) {
          structMapLogical = smt;
        }
        if (smt.getTYPE().equals(STRUCTMAP_PHYSICAL_TOKEN)) {
          structMapPhysical = smt;
        }
      }

      // Process struct map PHYSICAL (if not configured, it is deleted
      // later on!).
      this.step.setStatus(Status.RUNNING,
          "Processing StructMap PHYSICAL");

      // Create folder for struct map PHYSICAL in the import folder.
      File structMapPhysicalFolder = new File(this.rootFolder,
          this.structMapPhysicalAggregationName);
      if (!this.processStructMapPhysical) {
        structMapPhysicalFolder = new File(this.rootFolder,
            this.structMapPhysicalAggregationName
                + this.deleteSuffix);
      }

      String physID = (structMapPhysical.getID() != null
          ? structMapPhysical.getID()
          : "NO ID");
      createFile(structMapPhysicalFolder, physID, true);

      // Recursively loop all DIVs.
      processDiv(mets, structMapPhysical.getDiv().getDivList(),
          structMapPhysicalFolder, STRUCTMAP_PHYSICAL_TOKEN);

      // Process struct map LOGICAL...
      // ...only if configured (that's easy :-)
      if (this.processStructMapLogical) {

        this.step.setStatus(Status.RUNNING,
            "Processing StructMap LOGICAL");

        // Create folder for struct map LOGICAL in the import folder.
        File structMapLogicalFolder = new File(this.rootFolder,
            this.structMapLogicalAggregationName);
        String logID = (structMapLogical.getID() != null
            ? structMapLogical.getID()
            : "NO ID");
        createFile(structMapLogicalFolder, logID, true);

        // Recursively loop all DIVs.
        List<DivType> divList = new ArrayList<DivType>();
        divList.add(structMapLogical.getDiv());
        processDiv(mets, divList, structMapLogicalFolder,
            STRUCTMAP_LOGICAL_TOKEN);
      }
      // Skipping StructMapLogical due to module configuration!
      else {
        this.step.setStatus(Status.RUNNING,
            "Skipping StructMap LOGICAL processing");
      }
    } catch (XmlException e) {
      this.step.setStatus(Status.ERROR,
          e.getClass().getSimpleName() + ": " + e.getMessage());
      return;
    } catch (IOException e) {
      e.printStackTrace();
      this.step.setStatus(Status.ERROR,
          e.getClass().getSimpleName() + ": " + e.getMessage());
      return;
    }

    // Log duration and set status to DONE.
    String duration = KolibriTimestamp
        .getDurationWithMillis(System.currentTimeMillis() - startTime);
    this.step.setStatus(Status.DONE,
        "METS file processing completed in " + duration);
  }

  // **
  // INTERNAL
  // **

  /**
   * @param theMets
   * @param theDivList
   * @param theFile
   * @param physicalOrLogical
   * @throws IOException
   */
  private void processDiv(MetsType theMets, List<DivType> theDivList,
      File theFile, String physicalOrLogical) throws IOException {

    // Loop.
    for (DivType div : theDivList) {

      // Use IDs for the folder name, for we want to extract the ID from
      // the filename later.
      String foldername = div.getID();

      // Create folder from DIV ID.
      File folder = new File(theFile, foldername);
      createFile(folder, div.getID(), true);

      // PLEASE NOTE DfgViewerMetadataProcessor cares about ADM and DMD
      // metadata!

      // Download and save files, if existing.
      if (!div.getFptrList().isEmpty()) {
        for (Fptr f : div.getFptrList()) {
          FileType fgtf = getFileByFILEID(theMets, f.getFILEID());
          if (fgtf != null) {
            downloadFile(fgtf, folder);
          }
        }
      }

      // Process struct link section from LOGICAL to PHYSICAL (in struct
      // map LOGICAL only, due to unique XML IDs without struct map
      // check).
      List<SmLink> smlinkList = theMets.getStructLink().getSmLinkList();
      List<String> allTheTOs = new ArrayList<String>();
      for (SmLink s : smlinkList) {
        // Get FROM and TO ID values.
        String fromValue = s.getDomNode().getAttributes()
            .getNamedItem(XLINK_FROM_TOKEN).getNodeValue();
        String toValue = s.getDomNode().getAttributes()
            .getNamedItem(XLINK_TO_TOKEN).getNodeValue();
        // If PHYSICAL: Put all the TOs into the extra TO list.
        if (physicalOrLogical.equals(STRUCTMAP_PHYSICAL_TOKEN)) {
          allTheTOs.add(toValue);
        }
        // If FROM value is equal to the current DIV ID, create a link
        // to the appropriate file of the TO value (we are using a new
        // file suffix here, and the file contains the path of the
        // linked file).
        if (fromValue.equals(div.getID())
            && div.getDivList().isEmpty()) {
          DivType thisDiv = getPhysicalDivById(theMets, toValue);
          File logicalLink = new File(
              folder, toValue
                  + (thisDiv != null ? NAME_TYPE_SEPARATOR
                      + thisDiv.getTYPE() : "")
                  + this.referenceSuffix);

          // Create link file with link URI to appropriate aggregation
          // (that is replaced in CreateAggregations).
          createFile(logicalLink, div.getID(), false);

          // Check for METS file errors (and wrong file map entries).
          if (this.fileMap.get(toValue) == null) {
            throw new IOException("File with ID '" + fromValue
                + "' not found in file map, please check your METS file's StructMap DIV IDs!");
          }

          String thisFilePath = this.fileMap.get(toValue)
              .getCanonicalPath();
          String thisFileName = this.fileMap.get(toValue).getName();
          String referenceContent = thisFilePath + File.separatorChar
              + thisFileName + this.aggregationSuffix;

          // File content to be stored (reference) must be an URI
          // already!
          FileUtils.storeFile(logicalLink,
              new File(referenceContent).toURI().toString());
        }
      }

      // If PHYSICAL: Check if the current DIV ID is existing in the extra
      // TO list, if not, do talk about it, if configured so!
      if (physicalOrLogical.equals(STRUCTMAP_PHYSICAL_TOKEN)
          && !allTheTOs.contains(div.getID())) {
        if (this.ignoreMetsValidationFailures) {
          this.step.setStatus(Status.RUNNING,
              "WARNING! Ignoring: Physical DIV ID '" + div.getID()
                  + "' not found in struct link section, please check your METS file's StructLink smLink xlink:to attributes!");
        } else {
          throw new IOException("Physical DIV ID '" + div.getID()
              + "' not found in struct link section, please check your METS file's StructLink smLink xlink:to attributes!");
        }
      }

      // Process all following DIVs.
      if (!div.getDivList().isEmpty()) {
        processDiv(theMets, div.getDivList(), folder,
            physicalOrLogical);
      }
    }
  }

  /**
   * @param theMets
   * @param theFileId
   * @return
   */
  private FileType getFileByFILEID(MetsType theMets, String theFileId) {

    FileType result = null;

    if (theMets.getFileSec().getFileGrpList().size() > 0) {
      for (FileGrp g : theMets.getFileSec().getFileGrpList()) {
        // Get file ID if files from FileGroup are configured to be
        // downloaded.
        if (this.fileGroups.contains(g.getUSE())
            && g.getFileList().size() > 0) {
          for (FileType f : g.getFileList()) {
            if (f.getID().equals(theFileId)) {
              result = f;
            }
          }
        }
      }
    }

    return result;
  }

  /**
   * @param theMets
   * @param theId
   * @return
   */
  private DivType getPhysicalDivById(MetsType theMets, String theId) {

    // Get struct map list.
    List<StructMapType> structMapList = theMets.getStructMapList();

    // Loop through struct map list.
    for (StructMapType smt : structMapList) {
      // Check DIVs for ID recursively, if first DIV is existing and has
      // ID.
      if (smt.getTYPE().equals(STRUCTMAP_PHYSICAL_TOKEN)
          && smt.getDiv() != null) {
        return checkDivId(smt.getDiv(), theId);
      }
    }

    return null;
  }

  /**
   * @param theDiv
   * @param theId
   * @return
   */
  private DivType checkDivId(DivType theDiv, String theId) {

    // Check DIV.
    if (theDiv != null) {
      if (theDiv.getID().equals(theId)) {
        return theDiv;
      }

      // Now check DIV list, if existing.
      if (!theDiv.getDivList().isEmpty()) {
        for (DivType dt : theDiv.getDivList()) {
          DivType result = checkDivId(dt, theId);
          if (result != null) {
            return result;
          }
        }
      }
    }

    return null;
  }

  /**
   * <p>
   * Downloads a file from HTTP.
   * </p>
   * 
   * @param theFileId
   * @param theFilename
   * @throws IOException
   */
  private void downloadFile(FileType theFile, File theDestinationFile)
      throws IOException {

    // Get URL and download using an HTTP client.
    String href = theFile.getFLocatList().get(0).getHref();
    URL url = new URL(href);

    // NOTE Taking the ID as filename is later used to get the file IDs to
    // rewrite METS file URLs to URIs or PIDs in class RewriteDfgViewerUrls!
    String filename = theFile.getID();

    this.step.setStatus(Status.RUNNING,
        "[" + theFile.getID() + "] Downloading file: " + filename + ", "
            + (theFile.getSIZE() > 0 ? "size: " + theFile.getSIZE()
                : "no size given in METS file"));

    File outputFile = goAndGetTheFile(url, theDestinationFile, filename);

    this.step.setStatus(Status.RUNNING, "[" + theFile.getID()
        + "] Download complete (" + outputFile.length() + " bytes)");

    // Put FileID and downloaded file into the file map.
    this.itemMap.put(theFile.getID(), outputFile);

    // Put file into the file list.
    this.processData.getFileList().add(outputFile);
  }

  /**
   * <p>
   * Goes and gets the file from URL.
   * </p>
   * 
   * @param theUrl
   * @param theDestinationFile
   * @param theFilename
   * @return
   * @throws IOException
   */
  protected abstract File goAndGetTheFile(URL theUrl, File theDestinationFile,
      String theFilename) throws IOException;

  /**
   * <p>
   * Creates a file or a folder.
   * </p>
   * 
   * @param theFile
   * @param theId
   * @param createFolder
   * @throws IOException
   */
  private void createFile(File theFile, String theId, boolean createFolder)
      throws IOException {

    if (createFolder) {
      // Skip already existing folders for robustness, probably a
      // structLink tag is existing twice, or other XML semantic
      // failure...
      if (theFile.exists()) {
        this.step.setStatus(Status.WARNING,
            "[" + theId + "] Folder already created: "
                + theFile.getAbsolutePath());
      } else if (!theFile.mkdir()) {
        throw new IOException(
            "[" + theId + "] Failed to create folder: "
                + theFile.getAbsolutePath()
                + "! Maybe duplicated XML ID?");
      }
      this.fileMap.put(theId, theFile);
      this.step.setStatus(Status.RUNNING,
          "[" + theId + "] Folder created and added to file list: "
              + theFile.getAbsolutePath());
    } else {
      // Skip already existing files for robustness, probably a structLink
      // tag is existing twice, or other XML semantic failure...
      if (theFile.exists()) {
        this.step.setStatus(Status.WARNING,
            "[" + theId + "] File already created: "
                + theFile.getAbsolutePath());
      } else if (!theFile.createNewFile()) {
        throw new IOException("[" + theId + "] Failed to create file: "
            + theFile.getAbsolutePath());
      }
      // Put file into the file list.
      this.processData.getFileList().add(theFile);

      this.step.setStatus(Status.RUNNING,
          "[" + theId + "] File created and added to file list: "
              + theFile.getAbsolutePath());
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param processData
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   * @param step
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param theFileGroupName
   */
  public void setFileGroups(String theFileGroupName) {
    this.fileGroups.add(theFileGroupName);
  }

  /**
   * @param rootAggregationMimetype
   */
  public void setRootAggregationMimetype(String rootAggregationMimetype) {
    this.rootAggregationMimetype = rootAggregationMimetype;
  }

  /**
   * @param referenceSuffix
   */
  public void setReferenceSuffix(String referenceSuffix) {
    this.referenceSuffix = referenceSuffix;
  }

  /**
   * @param aggregationSuffix
   */
  public void setAggregationSuffix(String aggregationSuffix) {
    this.aggregationSuffix = aggregationSuffix;
  }

  /**
   * @param structMapPhysicalAggregationName
   */
  public void setStructMapPhysicalAggregationName(
      String structMapPhysicalAggregationName) {
    this.structMapPhysicalAggregationName = structMapPhysicalAggregationName;
  }

  /**
   * @param structMapLogicalAggregationName
   */
  public void setStructMapLogicalAggregationName(
      String structMapLogicalAggregationName) {
    this.structMapLogicalAggregationName = structMapLogicalAggregationName;
  }

  /**
   * @param processStructMapPhysical
   */
  public void setProcessStructMapPhysical(boolean processStructMapPhysical) {
    this.processStructMapPhysical = processStructMapPhysical;
  }

  /**
   * @param processStructMapLogical
   */
  public void setProcessStructMapLogical(boolean processStructMapLogical) {
    this.processStructMapLogical = processStructMapLogical;
  }

  /**
   * @param ignoreMetsValidationFailures
   */
  public void setIgnoreMetsValidationFailures(
      boolean ignoreMetsValidationFailures) {
    this.ignoreMetsValidationFailures = ignoreMetsValidationFailures;
  }

}
