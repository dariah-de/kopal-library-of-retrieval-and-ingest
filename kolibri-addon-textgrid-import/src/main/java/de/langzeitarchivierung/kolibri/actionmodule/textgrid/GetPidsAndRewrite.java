/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.Status;
import info.textgrid.middleware.pid.api.TGPidService;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2017-03-27 - Funk - First version.
 */

/**
 * <p>
 * Abstract class for getting PIDs from PID service.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2020-03-03
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 * @since 2017-03-27
 */

public class GetPidsAndRewrite extends GetPidsAndRewriteAbs {

  // **
  // STATIC
  // **

  private static TGPidService tgpid;

  /**
   *
   */
  @Override
  protected String getPids(String theRbacSessionId, String theUriString, String theFilesizeString,
      String theChecksumString) {

    String result = "";

    // Get PID.
    if (tgpid == null) {
      tgpid = JAXRSClientFactory.create(this.tgpidServerUrl, TGPidService.class);
    }

    try {
      result = tgpid.getPids(theRbacSessionId, theUriString, theFilesizeString, theChecksumString,
          this.pidPublisherMetadata, CHECK_PIDS_FIRST);
    } catch (XPathExpressionException | IOException | SAXException
        | ParserConfigurationException e) {
      this.step.setStatus(Status.ERROR, "System message: " + e.getMessage());
    }

    return result;
  }

}
