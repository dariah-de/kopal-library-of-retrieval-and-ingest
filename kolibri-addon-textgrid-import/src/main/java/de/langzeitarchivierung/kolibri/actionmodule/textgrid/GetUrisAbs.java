/**
 * This software is copyright (c) 2021 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.textgrid.util.ImportUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;

/**
 * CHANGELOG
 * 
 * 2015-03-19 - Funk - Added support for new revisions. Takes URIs from the existing metadata files
 * and only fetches URIs for new objects.
 * 
 * 2012-05-26 - Funk - Added reference suffix. Reference files are NOT counted! They are only
 * temporary files anyway!
 * 
 * 2012-02-20 - Funk - More commenting.
 * 
 * 2011-05-23 - Funk - First version.
 */

/**
 * <p>
 * Fetches TextGrid URIs from TG-crud and stores them in the custom data. Counts files and folders
 * first.
 * </p>
 * 
 * TODO Check thread safety of the URI list! Please use with one thread only!
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-03-22
 * @since 2011-05-23
 */

public abstract class GetUrisAbs implements ActionModule {

  // **
  // FINALS
  // **

  protected static final String NO_PROJECTID = "";
  private static final String URI_DEVIDER = "...";
  protected static final String TG_URI_LIST = "urilist:";
  protected static final String ERROR_FETCHING = "Error fetching URIs: ";

  // **
  // STATE
  // **

  private ProcessData processData;
  protected Step step;
  private int files = 0;
  private int folders = 0;
  protected String tgcrudServerUrl = "";
  protected String rbacSessionId = "";
  protected String logParameter = "";
  private String metadataSuffix = ".meta";
  private String referenceSuffix = ".reference";
  private boolean urisForFolders = true;
  private boolean createNewRevisions = false;

  /**
   * <p>
   * This module fetches TextGrid URIs from the TG-crud, for each file (if no new revision shall be
   * created) and each folder (if urisForFolders is set to TRUE).
   * </p>
   * 
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>The PathToContentFiles set (normally done by the appropriate processStarter)</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>A list of URI strings is put to the customData HashMap of the processData. The key is
   * something like (just have a look at the code below, please use class finals!):
   * "urilist:processName".</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Set start time.
    long startTime = System.currentTimeMillis();

    // Get import folder.
    File importFolder = new File(this.processData.getPathToContentFiles());

    this.step.setStatus(Status.RUNNING,
        "Files are beeing counted in " + importFolder.getAbsolutePath());

    // Disable urisForFolders if createNewRevisions enabled!
    if (this.createNewRevisions && this.urisForFolders) {
      String message =
          "URIs for folders must be disabled if new revisions shall be created, or maybe you want to use the complete_import policy?";
      this.step.setStatus(Status.ERROR, message);
      return;
    }

    // Go through all files recursively and count recursively.
    List<String> existingUris;
    try {
      existingUris = countFilesAndFolders(importFolder);
    } catch (IOException e) {
      this.step.setStatus(Status.ERROR, "System message: " + e.getMessage());
      return;
    } catch (MetadataParseFault e) {
      this.step.setStatus(Status.ERROR, "System message: " + e.getMessage());
      return;
    }

    this.step.setStatus(Status.RUNNING,
        "Found " + this.folders + " folder" + (this.folders != 1 ? "s" : "") + " and " + this.files
            + " file" + (this.files != 1 ? "s" : "") + " to get URIs for"
            + (this.urisForFolders ? "" : " (no URIs for folders requested)"));

    // Get URIs for all counted files (and eventually folders).
    List<String> uriList = new ArrayList<String>();

    int howMany = 0;
    if (this.urisForFolders) {
      howMany = this.files + this.folders;
    } else {
      howMany = this.files;
    }

    if (howMany > 0) {
      this.step.setStatus(Status.RUNNING,
          "Requesting total of " + howMany + " URI" + (howMany != 1 ? "s" : ""));

      uriList = getUris(howMany);

      // Return is status was set to ERROR from implementing class.
      if (this.step.getStatus().getType() == Status.ERROR) {
        return;
      }
    }

    String message = " URI" + (uriList.size() != 1 ? "s" : "") + " generated";
    if (uriList.isEmpty()) {
      this.step.setStatus(Status.RUNNING, "No" + message);
    } else {
      this.step.setStatus(Status.RUNNING, uriList.size() + message + ": " + uriList.get(0)
          + URI_DEVIDER + uriList.get(uriList.size() - 1));
    }

    // Add existing URIs to URI list.
    uriList.addAll(existingUris);

    message = " URI" + (existingUris.size() != 1 ? "s" : "") + " added from metadata";
    if (!existingUris.isEmpty()) {
      this.step.setStatus(Status.RUNNING, existingUris.size() + message + ": " + existingUris.get(0)
          + URI_DEVIDER + existingUris.get(uriList.size() - 1));
    } else {
      this.step.setStatus(Status.RUNNING, "No" + message);
    }

    // Add the URIs to the custom data.
    // TODO Additionally store all fetched URIs to a hidden file to re-use them in case of an error!
    this.processData.getCustomData().put(TG_URI_LIST + this.processData.getProcessName(), uriList);

    // Log duration and set status to DONE.
    String duration =
        KolibriTimestamp.getDurationWithMillis(System.currentTimeMillis() - startTime);
    this.step.setStatus(Status.DONE, uriList.size() + " TextGrid URI"
        + (uriList.size() != 1 ? "s" : "") + " assembled in " + duration);
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Abstract class for getting URI list.
   * </p>
   * 
   * @param howMany
   * @return
   */
  protected abstract List<String> getUris(int howMany);

  /**
   * <p>
   * Count files and folders. We count all folders and all files that are not metadata or reference
   * files, but we leave out files, that have an URI given in the metadata.
   * </p>
   * 
   * @param theFile
   * @return Returns a list of existing URIs in files from this folder...
   * @throws IOException
   * @throws MetadataParseFault
   */
  private List<String> countFilesAndFolders(File theFile) throws IOException, MetadataParseFault {

    List<String> result = new ArrayList<String>();

    File files[] = theFile.listFiles();
    if (files == null) {
      return result;
    }

    for (File f : files) {

      // Ignore hidden files, metadata files, and reference files.
      if (f.isHidden() || f.getName().endsWith(this.metadataSuffix)
          || f.getName().endsWith(this.referenceSuffix)) {
        continue;
      }

      // Count files.
      if (f.isFile()) {

        // Ignore files with URI set in metadata.
        if (this.createNewRevisions) {
          File mFile = new File(f.getAbsolutePath() + this.metadataSuffix);

          // Check metadata file to be existing.
          if (!mFile.exists()) {
            String message = "Metadata file " + mFile.getCanonicalPath()
                + " can not be found! You need to have metadata files for every file in new revision creation mode!";
            throw new FileNotFoundException(message);
          }

          ObjectType metadata = ImportUtils.getMetadata(mFile);

          // Check for existing URI in generated metadata.
          if (metadata.getGeneric().getGenerated() == null
              || metadata.getGeneric().getGenerated().getTextgridUri() == null
              || "".equals(metadata.getGeneric().getGenerated().getTextgridUri().getValue())) {
            String message = "No URI found in metadata file " + mFile.getName()
                + "! Please provide existing URI for creating new revision!";
            throw new MetadataParseFault(message);
          } else {
            String uri = metadata.getGeneric().getGenerated().getTextgridUri().getValue();
            if (uri != null && !uri.equals("")) {

              this.step.setStatus(Status.RUNNING,
                  "URI " + uri + " found in metadata file " + mFile.getName());

              result.add(uri);
              continue;
            }
          }
        } else {
          this.files++;
        }
      }

      // Count folders and count recursively.
      else if (f.isDirectory()) {
        this.folders++;
        result.addAll(countFilesAndFolders(f));
      }
    }

    return result;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param processData
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   * @param step
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param tgcrudServerUrl
   */
  public void setTgcrudServerUrl(String tgcrudServerUrl) {
    this.tgcrudServerUrl = tgcrudServerUrl;
  }

  /**
   * @param rbacSessionId
   */
  public void setRbacSessionId(String rbacSessionId) {
    this.rbacSessionId = rbacSessionId;
  }

  /**
   * @param logParameter
   */
  public void setLogParameter(String logParameter) {
    this.logParameter = logParameter;
  }

  /**
   * @param metadataSuffix
   */
  public void setMetadataSuffix(String metadataSuffix) {
    this.metadataSuffix = metadataSuffix;
  }

  /**
   * @param urisForFolders
   */
  public void setUrisForFolders(boolean urisForFolders) {
    this.urisForFolders = urisForFolders;
  }

  /**
   * @param referenceSuffix
   */
  public void setReferenceSuffix(String referenceSuffix) {
    this.referenceSuffix = referenceSuffix;
  }

  /**
   * @param createNewRevisions
   */
  public void setCreateNewRevisions(boolean createNewRevisions) {
    this.createNewRevisions = createNewRevisions;
  }

}
