/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import org.apache.cxf.helpers.IOUtils;

/**
 * TODOLOG
 **
 * CHANGELOG
 * 
 * 2017-03-30 - Funk - First version.
 */

/**
 * <p>
 * Loads file from HTTP.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-02-21
 * @since 2017-03-30
 */

public class ProcessDfgViewerMets extends ProcessDfgViewerMetsAbs {

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.textgrid.
   * ProcessDfgViewerMetsAbs#goandgetthefile(java.net.URL, java.io.File, java.lang.String)
   */
  @Override
  protected File goAndGetTheFile(URL theUrl, File theDestinationFile, String theFilename)
      throws IOException {

    File result = new File(theDestinationFile, theFilename);
    FileOutputStream fo = new FileOutputStream(result);
    IOUtils.copyAndCloseInput(theUrl.openStream(), fo);
    fo.close();

    return result;
  }

}
