/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.JAXB;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPathExpressionException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.cxf.helpers.IOUtils;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.textgrid.util.ImportUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import info.textgrid._import.RewriteMethod;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.pid.api.TGPidService;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Pid;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;
import info.textgrid.utils.linkrewriter.ImportMapping;

/***
 * TODOLOG
 * 
 * TODO Implement FAKE URIs and retry parameters as in TG-publish's GetPids class!
 * 
 **
 * CHANGELOG
 * 
 * 2020-03-03 - Funk - Add "TextGrid" as PID publisher metadata.
 * 
 * 2016-08-08 - Funk - Set CHECK_PIDS to false. See comment below!
 */

/**
 * <p>
 * Action module that gets PIDs for all files, and puts all necessary metadata for publishing into
 * the object's metadata.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-03-03
 * @since 2010-07-21
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 */

public abstract class GetPidsAndRewriteAbs implements ActionModule {

  // **
  // FINALS
  // **

  private static final String URI_SEPARATION_CHAR = "_";
  private static final String PID_TYPE = "handle";
  protected static final String PID_PREFIX = "hdl:";
  private static final String URI_DEVIDER = "...";
  private static final TGPidService NO_PID_SERVICE = null;
  // TODO Checking PIDs first only makes sense, if we are using already existing TextGrid URIs
  // AGAIN! Are we doing that? Setting to false for the moment, because it is much faster that way!
  protected static final boolean CHECK_PIDS_FIRST = false;
  private static final String MESSAGE_DIGEST_TYPE = "MD5";
  private static final String SEPARATOR_CHAR = ",";
  private static final long TEN_SECS = 10000;

  private static final String ERROR_SPLITTING =
      "Failure splitting PIDs from PID string! Please check!";

  private static final String MODE_METADATA_REWRITE = "internal:textgrid#metadata";
  private static final String MODE_AGGREGATION_REWRITE = "internal:textgrid#aggregation";
  private static final String MODE_TEI_REWRITE = "internal:tei#tei";
  private static final String MODE_XSD_REWRITE = "internal:schema#xsd";
  private static final String MODE_LINKEDITOR_REWRITE = MODE_TEI_REWRITE;

  private static final String DEFAULT_PID_RESOLVER_HOST = "https://textgridrep.org/";
  private static final String DEV = "/dev.";
  private static final String DEV_PID_RESOLVER_HOST = "https://dev.textgridrep.org/";
  private static final String TEST = "/test.";
  private static final String TEST_PID_RESOLVER_HOST = "https://test.textgridrep.org/";

  // **
  // STATE (Instance variables)
  // **

  protected ProcessData processData;
  protected Step step;
  protected String tgpidServerUrl = "";
  private String rbacSessionId = "";
  private String logParameter = "";
  private String metadataSuffix = ".meta";
  private String uriPrefix = "textgrid";
  private List<String> uriList = new ArrayList<String>();
  private List<Long> filesizeList = new ArrayList<Long>();
  private List<String> checksumList = new ArrayList<String>();
  private ImportMapping mapping = new ImportMapping();
  private boolean getPids = false;
  private int amountOfPidsAtOnce = 10;
  private String pidResolverPrefix = DEFAULT_PID_RESOLVER_HOST;
  protected String pidPublisherMetadata = "TextGrid";
  private String rewritePrefix = "tmp.";
  private boolean rewriteMetadataUris = false;
  private boolean rewriteAggregationUris = false;
  private boolean rewriteTeiUris = false;
  private boolean rewriteLinkEditorUris = false;
  private boolean rewriteXsdUris = false;
  private List<String> ignoredUrlPrefixes = new ArrayList<String>();
  private boolean removeOldPids = false;
  private String teiRewriteSpec = MODE_TEI_REWRITE;

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>The pathToContentFiles (as always :-).</li>
   * <li>All files to import must start with the TextGrid URI as "vzjs.0" (without prefix
   * "textgrid"), as provided by RenameAndRewrite().</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>Generates a PID for every TextGrid object in the folder, using the pidResolverPrefix as a
   * resolving string.</li>
   * <li>Adds the PID to the TextGrid metadata.</li>
   * <li>Rewrites all URIs to PIDs, if rewriteUrisToPids set to TRUE.</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Set start time.
    long startTime = System.currentTimeMillis();

    // Get import folder.
    File importFolder = new File(this.processData.getPathToContentFiles());

    // Return if no PIDs shall be generated.
    if (!this.getPids || this.amountOfPidsAtOnce < 1) {
      this.step.setStatus(Status.DONE, "No PIDs shall be generated");
      return;
    }

    this.step.setStatus(Status.RUNNING,
        "Get PIDs for and rewrite all files in: " + importFolder.getAbsolutePath());

    // Get PIDs and create import map for textgrid URIs and PIDs.
    try {

      // Create URI, filesize, and checksum lists.
      createLists(importFolder);

      // Set PID resolver prefix from TG-pid server URL (default's to textgridrep.org).
      if (this.tgpidServerUrl.contains(DEV)) {
        this.pidResolverPrefix = DEV_PID_RESOLVER_HOST;
      } else if (this.tgpidServerUrl.contains(TEST)) {
        this.pidResolverPrefix = TEST_PID_RESOLVER_HOST;
      }

      this.step.setStatus(Status.RUNNING, "PID resolver prefix set to: " + this.pidResolverPrefix);

      // Get PIDs.
      getPids(NO_PID_SERVICE, this.tgpidServerUrl, this.rbacSessionId, this.logParameter,
          this.uriList, this.mapping, this.amountOfPidsAtOnce, this.pidResolverPrefix, URI_DEVIDER);

      // Return is status was set to ERROR from implementing class.
      if (this.step.getStatus().getType() == Status.ERROR) {
        return;
      }

      // Store mapping to IMEX file.
      File imex = new File(importFolder.getParentFile(),
          importFolder.getName() + ImportUtils.IMEX_URI2PID_FILESUFFIX);
      JAXB.marshal(this.mapping.toImportSpec(), imex);

      this.step.setStatus(Status.RUNNING,
          "PID mapping serialized to IMEX file: " + imex.getCanonicalPath());
    } catch (IOException e) {
      this.step.setStatus(Status.ERROR, "System message: " + e.getMessage());
      return;
    }

    // Prepare publish.
    try {
      preparePublish(importFolder);
    } catch (MetadataParseFault | IoFault | AuthFault | ObjectNotFoundFault | IOException
        | XPathExpressionException | SAXException | ParserConfigurationException
        | UpdateConflictFault | DatatypeConfigurationException | XMLStreamException e) {
      this.step.setStatus(Status.ERROR, "System message: " + e.getMessage());
      return;
    }

    // Log duration and set status to DONE.
    String duration =
        KolibriTimestamp.getDurationWithMillis(System.currentTimeMillis() - startTime);
    this.step.setStatus(Status.DONE,
        "All files have been sucessfully PIDified and rewritten in " + duration);
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Abstract method to get PIDs.
   * </p>
   * 
   * @param theRbacSessionId
   * @param theUriString
   * @param theFilesizeString
   * @param theChecksumString
   * @return
   */
  protected abstract String getPids(String theRbacSessionId, String theUriString,
      String theFilesizeString, String theChecksumString);

  /**
   * <p>
   * Prepare files for publishing.
   * </p>
   * 
   * @param theFile
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws AuthenticationFault
   * @throws IOException
   * @throws XPathExpressionException
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws UpdateConflictFault
   * @throws DatatypeConfigurationException
   * @throws XMLStreamException
   */
  private void preparePublish(File theFile) throws MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, IOException, XPathExpressionException, SAXException, ParserConfigurationException,
      UpdateConflictFault, DatatypeConfigurationException, XMLStreamException {

    File files[] = theFile.listFiles();
    if (files == null) {
      throw new IOException(
          "File list " + theFile.getCanonicalPath() + " is null or empty or both!");
    }

    for (File f : files) {

      if (f.isDirectory()) {
        preparePublish(f);
      }

      // Prepare files.
      else if (f.isFile() && !f.getName().endsWith(this.metadataSuffix)) {
        // Get data file (or aggregation data file), depending on filename.
        File metadataFile = new File(f.getParentFile(), f.getName() + this.metadataSuffix);
        ObjectType metadata = ImportUtils.getMetadata(metadataFile);

        processDataAndMetadata(metadata, f);
      }
    }
  }

  /**
   * <p>
   * Gets PID and adds it to the metadata.
   * </p>
   * 
   * TODO Use generic publish utilities, merge with TG-publish methods!!
   * 
   * @param theMetadata
   * @param theData
   * @throws DatatypeConfigurationException
   * @throws ObjectNotFoundFault
   * @throws MetadataParseFault
   * @throws UpdateConflictFault
   * @throws IoFault
   * @throws AuthFault
   * @throws IOException
   * @throws XMLStreamException
   */
  private void processDataAndMetadata(ObjectType theMetadata, File theData)
      throws DatatypeConfigurationException, ObjectNotFoundFault, MetadataParseFault,
      UpdateConflictFault, IoFault, AuthFault, IOException, XMLStreamException {

    // Get URI from filename.
    String uri = this.uriPrefix + ":"
        + theData.getName().substring(0, theData.getName().indexOf(URI_SEPARATION_CHAR));

    // Get handle out of the import mapping.
    String handle = this.mapping.getImportObjectForLocalURI(URI.create(uri)).getTextgridUri();

    // Process metadata: Create generated element if not yet existing.
    if (theMetadata.getGeneric().getGenerated() == null) {
      theMetadata.getGeneric().setGenerated(new GeneratedType());
    }

    // Remove old PIDs if configured.
    List<Pid> pids = theMetadata.getGeneric().getGenerated().getPid();
    if (pids != null && this.removeOldPids) {

      this.step.setStatus(Status.RUNNING, "[" + uri + "] Old PIDs removed from metadata: " + pids);

      pids.clear();
    }

    // Process metadata: add PID.
    Pid mPid = new Pid();
    mPid.setPidType(PID_TYPE);
    mPid.setValue(handle);

    if (!theMetadata.getGeneric().getGenerated().getPid().contains(mPid)) {
      theMetadata.getGeneric().getGenerated().getPid().add(mPid);

      this.step.setStatus(Status.RUNNING,
          "[" + uri + "] PID added to metadata: " + mPid.getValue());
    }

    // Get format from metadata.
    String format = theMetadata.getGeneric().getProvided().getFormat();

    this.step.setStatus(Status.RUNNING, "[" + uri + "] Found mimetype: " + format);

    // If PIDs have been generated, and format is an aggregation file, rewrite aggregations in data
    // file (URIs to PIDs), and overwrite data file if configured, skip rewrite if not.
    if (this.rewriteAggregationUris) {
      if (TextGridMimetypes.AGGREGATION_SET.contains(format)) {
        ImportUtils.rewrite(uri, theData, MODE_AGGREGATION_REWRITE, "aggregation data",
            this.ignoredUrlPrefixes, this.step, this.mapping, this.rewritePrefix);
      }
    } else {
      this.step.setStatus(Status.RUNNING,
          "[" + uri + "] Aggregation URIs not rewritten due to configuration");
    }

    // Rewrite TEI file if configured, skip rewrite if not.
    if (this.rewriteTeiUris) {
      if (format.equals(TextGridMimetypes.XML)) {
        ImportUtils.rewrite(uri, theData, this.teiRewriteSpec, "TEI data", this.ignoredUrlPrefixes,
            this.step, this.mapping, this.rewritePrefix);
      }
    } else {
      this.step.setStatus(Status.RUNNING,
          "[" + uri + "] TEI URIs not rewritten due to configuration");
    }

    // Rewrite link editor file if configured, skip rewrite if not.
    if (this.rewriteLinkEditorUris) {
      if (format.equals(TextGridMimetypes.LINKEDITOR)) {
        ImportUtils.rewrite(uri, theData, MODE_LINKEDITOR_REWRITE, "link editor",
            this.ignoredUrlPrefixes, this.step, this.mapping, this.rewritePrefix);
      }
    } else {
      this.step.setStatus(Status.RUNNING,
          "[" + uri + "] Linkeditor URIs not rewritten due to configuration");
    }

    // Rewrite XML schema file if configured, skip rewrite if not.
    if (this.rewriteXsdUris) {
      if (format.equals(TextGridMimetypes.XSD)) {
        ImportUtils.rewrite(uri, theData, MODE_XSD_REWRITE, "XSD data", this.ignoredUrlPrefixes,
            this.step, this.mapping, this.rewritePrefix);
      }
    } else {
      this.step.setStatus(Status.RUNNING,
          "[" + uri + "] XSD URIs not rewritten due to configuration");
    }

    // Update metadata file.
    File metadataFile = new File(theData.getAbsolutePath() + this.metadataSuffix);
    JAXB.marshal(theMetadata, metadataFile);

    this.step.setStatus(Status.RUNNING,
        "[" + uri + "] Metadata file updated, generated metadata added: " + metadataFile.getName());

    // Rewrite metadata file if configured, skip rewrite if not.
    if (this.rewriteMetadataUris) {
      if (TextGridMimetypes.AGGREGATION_SET.contains(format)) {
        ImportUtils.rewrite(uri, metadataFile, MODE_METADATA_REWRITE, "metadata",
            this.ignoredUrlPrefixes, this.step, this.mapping, this.rewritePrefix);
      }
    } else {
      this.step.setStatus(Status.RUNNING,
          "[" + uri + "] Metadata URIs not rewritten due to configuration");
    }
  }

  /**
   * <p>
   * Get all the URIs from the filenames, all file sizes and checksums from the file. It's easier
   * than getting it all from the Metadata, because we have the metadata AFTER submitting files, and
   * to submit files, we need the PIDs.... :-)
   * <p/>
   * 
   * @param theFile
   * @throws IOException
   */
  private void createLists(File theFile) throws IOException {

    File files[] = theFile.listFiles();
    if (files == null) {
      throw new IOException(
          "File list " + theFile.getCanonicalPath() + " is null or empty or both!");
    }

    for (File f : files) {
      if (f.isDirectory()) {
        createLists(f);
      }

      // Get URI from non-metadata files.
      else if (f.isFile() && !f.getName().endsWith(this.metadataSuffix)) {

        // Strip filename and get the URI.
        String uri = this.uriPrefix + ":"
            + f.getName().substring(0, f.getName().indexOf(URI_SEPARATION_CHAR));

        // Add URI to the URI list.
        this.uriList.add(uri);

        // Add filesize to filesize list.
        this.filesizeList.add(Long.valueOf(f.length()));

        // Compute checksum and add to checksum list.
        FileInputStream fis = new FileInputStream(f);
        String messageDigest = DigestUtils.md5Hex(IOUtils.readBytesFromStream(fis));
        fis.close();
        this.checksumList.add(MESSAGE_DIGEST_TYPE.toLowerCase() + ":" + messageDigest);
      }
    }
  }

  /**
   * <p>
   * Get PIDs for every Textgrid URI and fill import mapping.
   * </p>
   * 
   * @param thePidService
   * @param thePidServiceUrl
   * @param theRbacSessionId
   * @param theLogParameter
   * @param theUriList
   * @param theMapping
   * @param theAmount
   * @param theResolverPrefix
   * @param theDevider
   */
  private void getPids(TGPidService thePidService, String thePidServiceUrl, String theRbacSessionId,
      String theLogParameter, List<String> theUriList, ImportMapping theMapping, int theAmount,
      String theResolverPrefix, String theDevider) {

    // Get only theAmount URIs at once, until we have all of them.
    List<String> shortUriList = null;
    List<Long> shortFilesizeList = null;
    List<String> shortChecksumList = null;
    for (int i = 0; i < theUriList.size(); i = i + theAmount) {

      // Check if we have the rest, set x accordingly.
      if (i > theUriList.size() - theAmount) {
        theAmount = theUriList.size() - i;
      }

      shortUriList = theUriList.subList(i, i + theAmount);
      shortFilesizeList = this.filesizeList.subList(i, i + theAmount);
      shortChecksumList = this.checksumList.subList(i, i + theAmount);

      // Assemble a string full of URIs.
      StringBuffer uriBuffer = new StringBuffer();
      for (String uri : shortUriList) {
        uriBuffer.append(theResolverPrefix + uri);
        uriBuffer.append(SEPARATOR_CHAR);
      }
      String uriString = uriBuffer.toString();
      uriString = uriString.substring(0, uriString.length() - 1);

      // Assemble a string full of file sizes.
      StringBuffer filesizeBuffer = new StringBuffer();
      for (Long filesize : shortFilesizeList) {
        filesizeBuffer.append(filesize);
        filesizeBuffer.append(SEPARATOR_CHAR);
      }
      String filesizeString = filesizeBuffer.toString();
      filesizeString = filesizeString.substring(0, filesizeString.length() - 1);

      // Assemble a string full of checksums.
      StringBuffer checksumBuffer = new StringBuffer();
      for (String checksum : shortChecksumList) {
        checksumBuffer.append(checksum);
        checksumBuffer.append(SEPARATOR_CHAR);
      }
      String checksumString = checksumBuffer.toString();
      checksumString = checksumString.substring(0, checksumString.length() - 1);

      // Do some logging.
      String firstUri = (shortUriList.size() == 1 ? shortUriList.get(0) : shortUriList.get(0));
      Long firstFilesize =
          (shortFilesizeList.size() == 1 ? shortFilesizeList.get(0) : shortFilesizeList.get(0));
      String firstChecksum =
          (shortChecksumList.size() == 1 ? shortChecksumList.get(0) : shortChecksumList.get(0));
      String lastUri = shortUriList.get(shortUriList.size() - 1);
      Long lastFilesize = shortFilesizeList.get(shortFilesizeList.size() - 1);
      String lastChecksum = shortChecksumList.get(shortChecksumList.size() - 1);
      this.step.setStatus(Status.RUNNING, "URI list: " + firstUri + theDevider + lastUri);
      this.step.setStatus(Status.RUNNING,
          "Filesize list: " + firstFilesize + theDevider + lastFilesize);
      this.step.setStatus(Status.RUNNING,
          "Checksum list: " + firstChecksum + theDevider + lastChecksum);
      this.step.setStatus(Status.RUNNING,
          "Requesting " + shortUriList.size() + " PID" + (shortUriList.size() != 1 ? "s" : "")
              + " for " + (i + theAmount) + "/" + theUriList.size() + " TextGrid URI"
              + (theUriList.size() != 1 ? "s" : "") + " (check PIDs: " + CHECK_PIDS_FIRST + ")");

      // Get a PID for every object in the URI string.
      long pidRequestStartTime = System.currentTimeMillis();
      String pidString = "";
      while (pidString.equals("")) {
        try {
          pidString = getPids(theRbacSessionId, uriString, filesizeString, checksumString);
        } catch (Exception e) {
          this.step.setStatus(Status.WARNING, "Error getting PIDs due to " + e.getClass().getName()
              + ": " + e.getMessage() + "! Retrying in 10 seconds...");
          try {
            Thread.sleep(TEN_SECS);
          } catch (InterruptedException ee) {
            // Nothing has to be interrupted here!
          }
        }
      }

      // Do some logging and measuring.
      long duration = System.currentTimeMillis() - pidRequestStartTime;
      String durationString = KolibriTimestamp.getDurationInHours(duration);
      NumberFormat nf = NumberFormat.getNumberInstance();
      nf.setMinimumFractionDigits(2);
      nf.setMaximumFractionDigits(2);
      String average = nf.format((float) duration / (float) theAmount / 1000f);

      // Do some logging.
      String firstPid = (pidString.indexOf(SEPARATOR_CHAR) == -1 ? pidString
          : pidString.substring(0, pidString.indexOf(SEPARATOR_CHAR)));
      String lastPid = pidString.substring(pidString.lastIndexOf(SEPARATOR_CHAR) + 1);
      this.step.setStatus(Status.RUNNING, "PID response string retrieved in " + durationString
          + " (" + average + "s/PID): " + firstPid + theDevider + lastPid);

      String pairs[] = pidString.split(SEPARATOR_CHAR);
      for (String pair : pairs) {
        String uriAndPid[] = pair.split("@");
        // We need to have exactly two elements splitted here!
        if (uriAndPid.length == 2) {
          // TODO WHY? Add URI and PID to mapping (purge ".0").
          // String uri = uriAndPid[0].substring(0, uriAndPid[0].lastIndexOf("."));
          String uri = uriAndPid[0].substring(theResolverPrefix.length());
          String pid = PID_PREFIX + uriAndPid[1];
          theMapping.add(URI.create(pid), URI.create(uri), null, RewriteMethod.XML);
        } else {
          this.step.setStatus(Status.WARNING, ERROR_SPLITTING);
        }
      }
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param processData The processData to set.
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   * @param step The step to set.
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param tgpidServerUrl
   */
  public void setTgpidServerUrl(String tgpidServerUrl) {
    this.tgpidServerUrl = tgpidServerUrl;
  }

  /**
   * @param rbacSessionId
   */
  public void setRbacSessionId(String rbacSessionId) {
    this.rbacSessionId = rbacSessionId;
  }

  /**
   * @param logParameter
   */
  public void setLogParameter(String logParameter) {
    this.logParameter = logParameter;
  }

  /**
   * @param metadataSuffix
   */
  public void setMetadataSuffix(String metadataSuffix) {
    this.metadataSuffix = metadataSuffix;
  }

  /**
   * @param uriPrefix
   */
  public void setUriPrefix(String uriPrefix) {
    this.uriPrefix = uriPrefix;
  }

  /**
   * @param getPids
   */
  public void setGetPids(boolean getPids) {
    this.getPids = getPids;
  }

  /**
   * @param amountOfPidsAtOnce
   */
  public void setAmountOfPidsAtOnce(int amountOfPidsAtOnce) {
    this.amountOfPidsAtOnce = amountOfPidsAtOnce;
  }

  /**
   * @param rewritePrefix
   */
  public void setRewritePrefix(String rewritePrefix) {
    this.rewritePrefix = rewritePrefix;
  }

  /**
   * @param rewriteUris
   */
  public void setRewriteAggregationUris(boolean rewriteUris) {
    this.rewriteAggregationUris = rewriteUris;
  }

  /**
   * @param rewriteUris
   */
  public void setRewriteTeiUris(boolean rewriteUris) {
    this.rewriteTeiUris = rewriteUris;
  }

  /**
   * @param rewriteUris
   */
  public void setRewriteMetadataUris(boolean rewriteUris) {
    this.rewriteMetadataUris = rewriteUris;
  }

  /**
   * @param rewriteUris
   */
  public void setRewriteLinkEditorUris(boolean rewriteUris) {
    this.rewriteLinkEditorUris = rewriteUris;
  }

  /**
   * @param rewriteUris
   */
  public void setRewriteXsdUris(boolean rewriteUris) {
    this.rewriteXsdUris = rewriteUris;
  }

  /**
   * @param ignoredUrlPrefixes
   */
  public void setIgnoredUrlPrefixes(String ignoredUrlPrefixes) {
    this.ignoredUrlPrefixes.add(ignoredUrlPrefixes);
  }

  /**
   * @param removeOldPids
   */
  public void setRemoveOldPids(boolean removeOldPids) {
    this.removeOldPids = removeOldPids;
  }

  /**
   * @return
   */
  public String getTeiRewriteSpec() {
    return this.teiRewriteSpec;
  }

  /**
   * @param teiRewriteSpec
   */
  public void setTeiRewriteSpec(String teiRewriteSpec) {
    this.teiRewriteSpec = teiRewriteSpec;
  }

}
