/*******************************************************************************
 * This software is copyright (c) 2016 by
 * 
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.textgrid.util.ImportUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import info.textgrid.clients.SearchClient;
import info.textgrid.middleware.tgpublish.client.PublishClient;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.RelationsExistFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;
import info.textgrid.namespaces.middleware.tgsearch.TextgridUris;

/*******************************************************************************
 * TODOLOG
 * 
 *******************************************************************************
 * CHANGELOG
 * 	
 *	2016-10-07	Funk	Copied from DeleteFiles, and generalised.
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * Generic action module for deleting/publishing all files from projectID, root
 * URI, or mapping file.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2016-10-10
 * @since 2016-10-07
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 ******************************************************************************/

public abstract class HandleFilesGeneric implements ActionModule {

	// **
	// FINALS
	// **

	protected static final String	DRYRUN				= "[DRYRUN] ";
	protected static final String	WAITING				= "Who interrupted my waiting? Leave me alone!";
	private static final String		IGNORED				= "Ignoring TG-crud fault: ";
	private static final String		EMPTY_LIST			= "URI list is empty! Nothing to ";
	private static final String		FILE_PREFIX			= "file:";
	private static final String		TGURI_PREFIX		= "textgrid:";
	private static final String		PROJECT_PREFIX		= "project:";

	// **
	// STATE (Instance variables)
	// **

	protected ProcessData			processData;
	protected Step					step;
	protected String				rbacSessionId		= "";
	protected String				tgcrudServerUrl		= "";
	protected String				tgpublishServerUrl	= "";
	protected boolean				dryrun				= true;
	private String					logParameter		= "";
	private String					tgsearchServerUrl	= "";
	private String					objectUri			= "";

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
	 */
	public void go() {

		// Set start time and dryrun string.
		long startTime = System.currentTimeMillis();

		// Check object URI.
		if (this.objectUri == null || this.objectUri.equals("")) {
			this.step.setStatus(Status.ERROR, "Object URI must NOT be empty");
			return;
		}

		// Get the TG-crud client.
		TGCrudService tgcrud = null;
		try {
			tgcrud = TGCrudClientUtilities.getTgcrud(this.tgcrudServerUrl);

			this.step.setStatus(Status.RUNNING,
					(this.dryrun ? DRYRUN : "")
							+ "Created TG-crud web service client from endpoint "
							+ this.tgcrudServerUrl);

		} catch (MalformedURLException e) {
			this.step.setStatus(Status.ERROR, (this.dryrun ? DRYRUN : "")
					+ "Malformed TG-crud service URL: " + e.getMessage());
			return;
		}

		// Get TG-search client.
		SearchClient tgsearch = new SearchClient(this.tgsearchServerUrl);

		this.step.setStatus(Status.RUNNING,
				(this.dryrun ? DRYRUN : "")
						+ "Created TG-search web service client from endpoint "
						+ this.tgsearchServerUrl);

		// Create URI list.
		TextgridUris uriList = new TextgridUris();

		// Get TG-publish client.
		PublishClient tgpublish = new PublishClient(this.tgpublishServerUrl,
				this.rbacSessionId, this.logParameter);

		this.step.setStatus(Status.RUNNING,
				(this.dryrun ? DRYRUN : "")
						+ "Created TG-publish web service client from endpoint "
						+ this.tgpublishServerUrl);

		// =====================================================================
		// Get URIs from mapping file (objectUri starts with FILE_PREFIX).
		// =====================================================================

		if (this.objectUri.startsWith(FILE_PREFIX)) {

			// Omit "file:" from object ID.
			String filename = this.objectUri.replaceAll(FILE_PREFIX, "");
			File mappingFile = new File(filename);

			this.step.setStatus(Status.RUNNING,
					(this.dryrun ? DRYRUN : "")
							+ "Creating URI list from import mapping file "
							+ mappingFile.getAbsolutePath());

			try {
				uriList = ImportUtils.uriListFromMappingFile(this.step,
						mappingFile);

				if (uriList.getTextgridUri().isEmpty()) {
					this.step.setStatus(Status.DONE,
							(this.dryrun ? DRYRUN : "") + EMPTY_LIST + doo());
					return;
				}

				this.step.setStatus(Status.RUNNING,
						(this.dryrun ? DRYRUN : "") + doo() + " "
								+ uriList.getTextgridUri().size() + " URI"
								+ (uriList.getTextgridUri().size() != 1 ? "s"
										: "")
								+ " using import mapping file "
								+ mappingFile.getName());

			} catch (IOException e) {
				this.step.setStatus(Status.ERROR,
						(this.dryrun ? DRYRUN : "") + e.getMessage());
				return;
			}

			// Do delete current URI list.
			doThings(uriList, tgcrud, tgsearch, tgpublish);
		}

		// =====================================================================
		// Get URIs from TextGrid root URI (objectUri starts with TGURI_PREFIX).
		// =====================================================================

		else if (this.objectUri.startsWith(TGURI_PREFIX)) {

			this.step.setStatus(Status.RUNNING, (this.dryrun ? DRYRUN : "")
					+ "Creating URI list from root URI " + this.objectUri);

			// Get URIs. URI list contains only child objects of root URI!
			uriList = tgsearch.infoQuery().getChildren(this.objectUri);

			// Do things with current URI list, if not empty.
			if (!uriList.getTextgridUri().isEmpty()) {

				this.step.setStatus(Status.RUNNING,
						(this.dryrun ? DRYRUN : "") + doo() + " "
								+ uriList.getTextgridUri().size() + " object"
								+ (uriList.getTextgridUri().size() != 1 ? "s"
										: "")
								+ " aggregated by root URI " + this.objectUri);

				doThings(uriList, tgcrud, tgsearch, tgpublish);
			}

			// Handle root URI as last or single element!
			this.step.setStatus(Status.RUNNING,
					(this.dryrun ? DRYRUN : "") + "[" + this.objectUri
							+ "] -- Finally " + doo()
							+ " root URI or single object -- ");

			uriList = new TextgridUris();
			uriList.getTextgridUri().add(this.objectUri);

			doThings(uriList, tgcrud, tgsearch, tgpublish);
		}

		// =====================================================================
		// Get URIs from tgsearch#getAllObjects using a project ID
		// (objectUri starts with PROJECT_PREFIX).
		// =====================================================================

		else if (this.objectUri.startsWith(PROJECT_PREFIX)) {

			// Omit "project:" from object ID.
			String projectId = this.objectUri.replaceAll(PROJECT_PREFIX, "");

			this.step.setStatus(Status.RUNNING, (this.dryrun ? DRYRUN : "")
					+ "Creating URI list from project ID " + projectId);

			// Get (all! Sorry Ubbo!) URIs from project ID.
			int hits = Integer.parseInt(
					tgsearch.infoQuery().getAllObjects(projectId).getHits());
			uriList = tgsearch.infoQuery().setLimit(hits)
					.getAllObjects(projectId);

			// Return if list is empty.
			if (uriList.getTextgridUri().isEmpty()) {
				this.step.setStatus(Status.DONE,
						(this.dryrun ? DRYRUN : "") + EMPTY_LIST + doo());
				return;
			}

			this.step.setStatus(Status.RUNNING,
					(this.dryrun ? DRYRUN : "") + doo() + " "
							+ uriList.getTextgridUri().size() + " object"
							+ (uriList.getTextgridUri().size() != 1 ? "s" : "")
							+ " from project " + projectId);

			// Do handle current URI list.
			doThings(uriList, tgcrud, tgsearch, tgpublish);
		}

		// Invalid object URI!
		else {
			this.step.setStatus(Status.ERROR, (this.dryrun ? DRYRUN : "")
					+ "Invalid parameter prefix for objectUri: "
					+ this.objectUri
					+ "! Only 'file:', 'textgrid:', and 'project:' are permitted!");
			return;
		}

		String duration = KolibriTimestamp
				.getDurationHMSOnly(System.currentTimeMillis() - startTime);
		String message = (this.dryrun ? DRYRUN : "")
				+ (uriList.getTextgridUri().size() != 1 ? "All " : "One ")
				+ "object"
				+ (uriList.getTextgridUri().size() != 1 ? "s have" : " has")
				+ " been sucessfully " + doo()
				+ " from the TextGrid Repository in " + duration;

		this.step.setStatus(Status.DONE, message);
	}

	/**
	 * <p>
	 * Handles all objects contained in the given URI list, gives ETA.
	 * </p>
	 * 
	 * @param uriList
	 * @param tgcrud
	 * @param tgsearch
	 * @param tgpublish
	 */
	private void doThings(TextgridUris uriList, TGCrudService tgcrud,
			SearchClient tgsearch, PublishClient tgpublish) {

		// Set initial duration and count.
		long overallDuration = 0;
		int count = 0;

		// Loop and handle single URI.
		for (String uri : uriList.getTextgridUri()) {

			count++;

			long singleStart = System.currentTimeMillis();

			String infoBracket = (this.dryrun ? DRYRUN : "") + "[" + uri
					+ "] -- " + count + "/" + uriList.getTextgridUri().size()
					+ " -- ";

			try {
				doSingleThing(tgcrud, tgsearch, tgpublish, this.rbacSessionId,
						this.logParameter, uri, this.dryrun);

				long singleDuration = System.currentTimeMillis() - singleStart;
				overallDuration += singleDuration;

				this.step.setStatus(Status.RUNNING,
						infoBracket + doo() + " completed in "
								+ KolibriTimestamp
										.getDurationWithMillis(singleDuration));

			} catch (ObjectNotFoundFault e) {
				this.step.setStatus(Status.WARNING,
						infoBracket + IGNORED + "[" + e.getMessage() + "]");
			} catch (RelationsExistFault e) {
				this.step.setStatus(Status.WARNING,
						infoBracket + IGNORED + "[" + e.getMessage() + "]");
			} catch (IoFault e) {
				this.step.setStatus(Status.WARNING,
						infoBracket + IGNORED + "[" + e.getMessage() + "]");
			} catch (AuthFault e) {
				this.step.setStatus(Status.WARNING,
						infoBracket + IGNORED + "[" + e.getMessage() + "]");
			}

			// Some duration computations.
			int size = uriList.getTextgridUri().size();
			long last = overallDuration / count;

			if (count % 50 == 0) {
				long eta = (size - count) * last;

				this.step.setStatus(Status.RUNNING, (this.dryrun ? DRYRUN : "")
						+ "Overall duration of last " + count + ": "
						+ KolibriTimestamp.getDurationHMSOnly(overallDuration));
				this.step.setStatus(Status.RUNNING, (this.dryrun ? DRYRUN : "")
						+ "Average duration/object of last " + count + ": "
						+ KolibriTimestamp.getDurationWithMillis(last));
				this.step.setStatus(Status.RUNNING,
						(this.dryrun ? DRYRUN : "")
								+ "Expected duration of remaining "
								+ (size - count) + ": approx. "
								+ KolibriTimestamp.getDurationHMOnly(eta));
			}
		}
	}

	// **
	// ABSTRACT
	// **

	/**
	 * <p>
	 * Handle single object.
	 * </p>
	 * 
	 * @param theTGcrud
	 * @param theTGsearch
	 * @param theTGpublish
	 * @param theSessionId
	 * @param theLogParameter
	 * @param theUri
	 * @param dryRun
	 * @throws ObjectNotFoundFault
	 * @throws IoFault
	 * @throws RelationsExistFault
	 * @throws AuthFault
	 */
	protected abstract void doSingleThing(final TGCrudService theTGcrud,
			final SearchClient theTGsearch, final PublishClient theTGpublish,
			final String theSessionId, final String theLogParameter,
			final String theUri, boolean dryRun)
			throws ObjectNotFoundFault, IoFault, RelationsExistFault, AuthFault;

	/**
	 * @return Generic "Deletion" or "Publication" String :-)
	 */
	protected abstract String doo();

	// **
	// GETTERS AND SETTERS
	// **

	/**
	 * @param processData
	 *            The processData to set.
	 */
	public void setProcessData(ProcessData processData) {
		this.processData = processData;
	}

	/**
	 * @param step
	 *            The step to set.
	 */
	public void setStep(Step step) {
		this.step = step;
	}

	/**
	 * @param rbacSessionId
	 */
	public void setRbacSessionId(String rbacSessionId) {
		this.rbacSessionId = rbacSessionId;
	}

	/**
	 * @param tgcrudServerUrl
	 */
	public void setTgcrudServerUrl(String tgcrudServerUrl) {
		this.tgcrudServerUrl = tgcrudServerUrl;
	}

	/**
	 * @param logParameter
	 */
	public void setLogParameter(String logParameter) {
		this.logParameter = logParameter;
	}

	/**
	 * @param tgsearchServerUrl
	 */
	public void setTgsearchServerUrl(String tgsearchServerUrl) {
		this.tgsearchServerUrl = tgsearchServerUrl;
	}

	/**
	 * @param tgpublishServerUrl
	 */
	public void setTgpublishServerUrl(String tgpublishServerUrl) {
		this.tgpublishServerUrl = tgpublishServerUrl;
	}

	/**
	 * @param uri
	 */
	public void setObjectUri(String uri) {
		this.objectUri = uri;
	}

	/**
	 * @param dryrun
	 */
	public void setDryrun(boolean dryrun) {
		this.dryrun = dryrun;
	}

}
