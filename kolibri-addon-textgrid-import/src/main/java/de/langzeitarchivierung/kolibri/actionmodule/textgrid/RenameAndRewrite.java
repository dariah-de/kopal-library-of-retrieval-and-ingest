/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import org.apache.cxf.helpers.IOUtils;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.textgrid.util.ImportUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import info.textgrid._import.ImportObject;
import info.textgrid._import.RewriteMethod;
import info.textgrid.clients.SearchClient;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.utils.linkrewriter.ImportMapping;
import jakarta.xml.bind.JAXB;

/**
 * TODOLOG
 * 
 * TODO Create test cases for creating aggregation files!
 * 
 * TODO Generalise classes: RenameAndRewrite (kolibri-addon-textgrid-import), ModifyAndUpdate
 * (kolibri-tgpublish-service), ModifyAndCreate (kolibri-tgpublish-service)!
 * 
 **
 * CHANGELOG
 * 
 * 2023-01-26 - Funk - Add alphabetical sort mechanism for applying URIs to filenames.
 * 
 * 2020-03-03 - Funk - Add test for file's existence in rdf:about and rdsf:resource tags.
 * 
 * 2017-03-20 - Funk - Removing all the metadata from <generated>, keeping only URI and PID tags.
 */

/**
 * <p>
 * Renames all files and rewrites the URIs in the aggregation, tei, and metadata files.
 * </p>
 * 
 * TODO Check thread safety of the URI list! Please use with one thread only!
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2023-01-26
 * @since 2011-06-10
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 */

public class RenameAndRewrite implements ActionModule {

  // **
  // FINALS
  // **

  private static final String URI_SEPARATION_CHAR = "_";
  private static final String INITIAL_REVISION_SUFFIX = ".0";
  private static final String MODE_METADATA_REWRITE = "internal:textgrid#metadata";
  private static final String MODE_AGGREGATION_REWRITE = "internal:textgrid#aggregation";
  private static final String MODE_TEI_REWRITE = "internal:tei#tei";
  // private static final String MODE_XSD_REWRITE = "internal:schema#xsd";
  // private static final String MODE_LINKEDITOR_REWRITE = MODE_TEI_REWRITE;

  // **
  // STATE
  // **

  private ProcessData processData;
  private Step step;
  private List<String> uriList;
  private String metadataSuffix = ".meta";
  private String aggregationSuffix = ".aggregation";
  private String editionSuffix = ".edition";
  private String collectionSuffix = ".collection";
  private String xmlSuffix = ".xml";
  private String jhoveMetadataSuffix = ".jhove";
  private String uriPrefix = "textgrid";
  private String rewritePrefix = "tmp.";
  private String teiRewriteSpec = MODE_TEI_REWRITE;
  private boolean createNewRevisions = false;
  private boolean useBaseUrisInAggregations = false;
  private boolean sortAlphabetically = false;
  private ImportMapping mapping = new ImportMapping();
  private List<String> ignoredUrlPrefixes = new ArrayList<String>();
  private String tgsearchServerUrl = "";
  private String otherTgsearchServerUrl = "";
  private SearchClient tgsearchClient = null;
  private SearchClient otherTgsearchClient = null;

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>A URI list in the custom data, as created in the class GetURI.</li>
   * <li>The import folder (pathToContentFiles).</li>
   * <li>A complete set of TextGrid ready-to-import files, including data and metadata (including
   * aggregation and aggregation metadata) files. As links only relative links to files are existing
   * in the metadata, aggregation and TEI files.</li>
   * <li>In case of creating new revisions there must be Textgrid URIs existing as links!</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>The links in metadata, aggregation and TEI files rewritten from relative pathnames to
   * TextGrid URIs.</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  @SuppressWarnings("unchecked")
  public void go() {

    // Set start time.
    long startTime = System.currentTimeMillis();

    // Get URI list from custom data.
    this.uriList = (List<String>) this.processData.getCustomData()
        .get(GetUrisAbs.TG_URI_LIST + this.processData.getProcessName());
    // Sort URI list alphabetically if so configured.
    if (this.sortAlphabetically) {
      Collections.sort(this.uriList);
    }

    this.step.setStatus(Status.RUNNING, "URI list contains " + this.uriList.size() + " URI"
        + (this.uriList.size() != 1 ? "s" : ""));

    // Get tgsearch clients if revisions shall be created.
    if (this.createNewRevisions) {
      if (this.tgsearchClient == null) {
        this.tgsearchClient = new SearchClient(this.tgsearchServerUrl);

        this.step.setStatus(Status.RUNNING,
            "TG-search client created from endpoint " + this.tgsearchServerUrl);
      }
      if (this.otherTgsearchClient == null) {
        this.otherTgsearchClient = new SearchClient(this.otherTgsearchServerUrl);

        this.step.setStatus(Status.RUNNING,
            "TG-search client created from endpoint " + this.otherTgsearchServerUrl);
      }
    }

    // Get import folder.
    File importFolder = new File(this.processData.getPathToContentFiles());

    try {
      this.step.setStatus(Status.RUNNING, "Creating import map");

      // Create the import map (loops through all folders).
      createImportMap(importFolder);

      // Store mapping to IMEX file.
      File imex = new File(importFolder.getParentFile(),
          importFolder.getName() + ImportUtils.IMEX_LOCAL2URI_FILESUFFIX);
      JAXB.marshal(this.mapping.toImportSpec(), imex);

      this.step.setStatus(Status.RUNNING,
          "Import mapping serialized to IMEX file: " + imex.getCanonicalPath());

      this.step.setStatus(Status.RUNNING, "Rewriting pathes to TextGrid URIs");

      // Do rewrite!
      rewriteMetadata(importFolder);
      rewriteAggregations(importFolder);
      rewriteTEI(importFolder);

    } catch (IOException e) {
      this.step.setStatus(Status.ERROR, "Error rewriting files: " + e.getMessage());
      return;
    } catch (XMLStreamException e) {
      this.step.setStatus(Status.ERROR,
          "Rewriting filenames to URIs in aggregation files failed: " + e.getMessage());
      return;
    } catch (SAXException e) {
      this.step.setStatus(Status.ERROR, "Reading aggregation files failed: " + e.getMessage());
      return;
    } catch (ParserConfigurationException e) {
      this.step.setStatus(Status.ERROR, "Parsing aggregation file failed: " + e.getMessage());
      return;
    }

    this.step.setStatus(Status.RUNNING, "Renaming files");

    try {
      // Rename all the files (loops through all folders).
      renameFiles(importFolder);

    } catch (IOException e) {
      this.step.setStatus(Status.ERROR, "Error renaming files: " + e.getMessage());
      return;
    }

    // Write back the URI list to the custom data.
    this.processData.getCustomData().put(GetUrisAbs.TG_URI_LIST + this.processData.getProcessName(),
        this.uriList);

    // Log duration and set status to DONE.
    String duration =
        KolibriTimestamp.getDurationWithMillis(System.currentTimeMillis() - startTime);
    this.step.setStatus(Status.DONE, "All files have been rewritten and renamed in " + duration);
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Creates the import map.
   * </p>
   * 
   * @param theFile
   * @throws IOException
   */
  private void createImportMap(File theFile) throws IOException {

    File files[] = theFile.listFiles();

    // Sort file list alphabetically if so configured.
    if (this.sortAlphabetically) {
      Arrays.sort(files);
    }

    if (files == null) {
      throw new IOException(
          "File list " + theFile.getCanonicalPath() + " is null or empty or both!");
    }

    // Loop all files.
    for (File f : files) {

      // First go down into all subfolders.
      if (f.isDirectory()) {
        createImportMap(f);
      }

      // Then look for the data files and create import mapping.
      else if (f.isFile() && !f.getName().endsWith(this.metadataSuffix)
          && !f.getName().endsWith(this.jhoveMetadataSuffix)) {

        // Get URI. Remove it from the list, and use the import mapping afterwards for retrieving
        // URIs.
        if (this.uriList.isEmpty()) {
          this.step.setStatus(Status.RUNNING, "URI list is empty!");
        }

        // Get the URI from the list
        String uri = this.uriList.remove(0);

        // Now check if we create new revisions, use revision URI or base URI then according to
        // configuration!
        URI sourceURI = null;
        URI destinationURI = null;
        if (this.createNewRevisions) {

          // Check if we want to have base URIs in aggregations or revision URIs.
          if (this.useBaseUrisInAggregations) {
            sourceURI = URI.create(uri);
            destinationURI = ImportUtils.stripRevision(URI.create(uri));

            this.step.setStatus(Status.RUNNING,
                "[" + uri + "] Base URI for new revision will be (BADABUMM!): " + destinationURI
                    + " [see config value useBaseUrisInAggregations="
                    + this.useBaseUrisInAggregations + "]");
          }
          // Check the new revision via TG-search, and add old and new URIs.
          else {
            sourceURI = URI.create(uri);
            destinationURI =
                ImportUtils.getNewRevisionUri(this.tgsearchClient, this.otherTgsearchClient, uri);

            this.step.setStatus(Status.RUNNING,
                "[" + uri + "] New revision URI will be (TADAAA!): " + destinationURI
                    + " [see config value useBaseUrisInAggregations="
                    + this.useBaseUrisInAggregations + "]");
          }
        }
        // If no new revisions shall be created, just use local file pathes and URI for the mapping.
        else {
          // Add initial revision suffix if no base URIs shall be used.
          if (!this.useBaseUrisInAggregations) {
            uri = uri + INITIAL_REVISION_SUFFIX;
          }
          destinationURI = URI.create(uri);
          sourceURI = f.getCanonicalFile().toURI();
        }

        if (sourceURI == null || destinationURI == null) {
          throw new IOException("System error: URI must not be null!");
        }

        // Add URI to rewrite to: Use (a) base URI, (b) revision URI, or (c) local file URI!
        this.mapping.add(destinationURI, sourceURI, null, RewriteMethod.XML);

        this.step.setStatus(Status.RUNNING, "[" + uri + "] Import entry added: " + sourceURI
            + " > rewrites to > " + destinationURI);
      }
    }

  }

  /**
   * <p>
   * Renames all files recursively and builds up the filename rewriting list.
   * </p>
   * 
   * @param theFile
   * @throws IOException
   */
  private void renameFiles(File theFile) throws IOException {

    File files[] = theFile.listFiles();
    if (files == null) {
      throw new IOException(
          "File list " + theFile.getCanonicalPath() + " is null or empty or both!");
    }

    // Loop all files.
    for (File f : files) {

      // First go down into all subfolders.
      if (f.isDirectory()) {
        renameFiles(f);
      }

      // Then look for the data files and rename.
      else if (f.isFile() && !f.getName().endsWith(this.metadataSuffix)
          && !f.getName().endsWith(this.jhoveMetadataSuffix)) {

        // Get TextGrid URI. URI list should be empty by now.
        String uri = "";
        // Get old URI from metadata file first, then get revision URI from import mapping.
        if (this.createNewRevisions) {
          String oldUri = getTextGridUriFromMetadata(f);
          uri = this.mapping.getImportObjectForLocalURI(URI.create(oldUri)).getTextgridUri();
        }
        // Get URI from import mapping directly.
        else {
          uri = getTextGridUriFromImportMapping(f);
        }

        String uriWithoutPrefix = uri.replaceFirst(this.uriPrefix + ":", "");

        // Be sure not to use special chars in the filename (just to be on the safe side).
        // TODO Use URIs here, too?
        String specialCharFilename = f.getName();

        // Rename data and metadata file.
        File sourceMetadataFile = new File(f.getParentFile(), f.getName() + this.metadataSuffix);
        File destinationDataFile = new File(f.getParentFile(),
            uriWithoutPrefix + URI_SEPARATION_CHAR + specialCharFilename);
        File destinationMetadataFile = new File(f.getParentFile(),
            uriWithoutPrefix + URI_SEPARATION_CHAR + specialCharFilename + this.metadataSuffix);

        // Check for existence of data file.
        if (!f.exists()) {
          throw new FileNotFoundException("Data file " + f.getCanonicalPath() + " does not exist!");
        }
        // Rename data file.
        if (!moveFile(f, destinationDataFile)) {
          throw new IOException(
              "Data file could not be renamed to: " + destinationDataFile.getCanonicalPath());
        }
        // Check for existence of metadata file.
        if (!sourceMetadataFile.exists()) {
          throw new FileNotFoundException(
              "Metadata file " + sourceMetadataFile.getCanonicalPath() + " does not exist!");
        }
        // Rename metadata file.
        if (!moveFile(sourceMetadataFile, destinationMetadataFile)) {
          throw new IOException("Metadata file could not be renamed to: "
              + destinationMetadataFile.getCanonicalPath());
        }

        this.step.setStatus(Status.RUNNING, "[" + uri + "] Files successfully renamed to: "
            + destinationDataFile.getName() + ", " + destinationMetadataFile.getName());
      }
    }
  }

  /**
   * <p>
   * Rewrites the metadata files.
   * </p>
   * 
   * @param theFile
   * @throws IOException
   * @throws XMLStreamException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  private void rewriteMetadata(File theFile)
      throws IOException, XMLStreamException, SAXException, ParserConfigurationException {

    File files[] = theFile.listFiles();
    if (files == null) {
      throw new IOException(
          "File list " + theFile.getCanonicalPath() + " is null or empty or both!");
    }

    // Loop all files.
    for (File f : files) {

      // First go down into all subfolders.
      if (f.isDirectory()) {
        rewriteMetadata(f);
      }

      // Otherwise handle file.
      else if (f.isFile() && f.getName().endsWith(this.metadataSuffix)) {

        // Check if work file is existing, if mentioned in isEditionOf tag BEFORE rewriting, IF
        // isEditioOf is NOT empty! We can not force it here, because TG-lab import can also be done
        // here!
        if (f.getName().contains(this.editionSuffix)) {
          ObjectType metadata = ImportUtils.getMetadata(f);
          if (metadata.getEdition().getIsEditionOf() != null
              && !"".equals(metadata.getEdition().getIsEditionOf())) {
            String workString = metadata.getEdition().getIsEditionOf();
            File workFile = new File(f.getParentFile(), workString);
            if (!workFile.exists()) {
              throw new IOException("Work file references in Edition " + f.getCanonicalPath()
                  + " is not existing! Please check isEditionOf value: " + workString + "!");
            }
          }
        }

        // Omit metadata suffix for getting URI from import mapping.
        File g = new File(f.getCanonicalPath().substring(0,
            f.getCanonicalPath().lastIndexOf(this.metadataSuffix)));
        String uri = "";
        if (this.createNewRevisions) {
          uri = getTextGridUriFromMetadata(g);
        } else {
          uri = getTextGridUriFromImportMapping(g);
        }

        // Rewrite metadata with generic rewriting method.
        ImportUtils.rewrite(uri, f, MODE_METADATA_REWRITE, "metadata", this.ignoredUrlPrefixes,
            this.step, this.mapping, this.rewritePrefix);
      }
    }
  }

  /**
   * <p>
   * Rewrite aggregation files.
   * </p>
   * 
   * @param theFile
   * @throws IOException
   * @throws XMLStreamException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  private void rewriteAggregations(File theFile)
      throws IOException, XMLStreamException, SAXException, ParserConfigurationException {

    File files[] = theFile.listFiles();
    if (files == null) {
      throw new IOException(
          "File list " + theFile.getCanonicalPath() + " is null or empty or both!");
    }

    // Loop all files.
    for (File f : files) {

      // First go down into all subfolders.
      if (f.isDirectory()) {
        rewriteAggregations(f);
      }

      // Otherwise handle files.
      else if (f.isFile() && f.getName().endsWith(this.aggregationSuffix)
          || f.getName().endsWith(this.editionSuffix)
          || f.getName().endsWith(this.collectionSuffix)) {

        String uri = "";
        if (this.createNewRevisions) {
          uri = getTextGridUriFromMetadata(f);
        } else {
          uri = getTextGridUriFromImportMapping(f);
        }

        // Check rdf:about file for existence (if rdf:about is set at all).
        // TODO Maybe use RDF library for getting the tag value?
        String aggregation = IOUtils.toString(new FileReader(f));
        if (aggregation.indexOf("rdf:about=\"") != -1) {
          String about = aggregation.substring(aggregation.indexOf("rdf:about=\"") + 11);
          about = about.substring(0, about.indexOf("\""));

          File aggregationFile = new File(f.getParent(), about);
          // Warn if aggregation file in rdf:about is not existing.
          boolean aggregationFileNotExisting = false;
          if (!aggregationFile.exists()) {
            String message = "File at rdf:about [" + aggregationFile.getCanonicalPath()
                + "] is not existing! Please check rdf:about value in file " + f.getCanonicalPath();
            aggregationFileNotExisting = true;
            this.step.setStatus(Status.WARNING, message);
          }
          // Warn if TextGrid URI is not contained in the filename of the current aggregation file!
          boolean aggregationFileUriNotContained = false;
          String strippedUri = ImportUtils.stripUriPrefix(URI.create(uri));
          if (!aggregationFile.getName().contains(strippedUri)) {
            String message = "Object URI " + strippedUri
                + " is not contained in filename! Please check rdf:about value in file "
                + f.getCanonicalPath();
            aggregationFileUriNotContained = true;
            this.step.setStatus(Status.WARNING, message);
          }
          // Go to error state if both is not applicable!
          if (aggregationFileNotExisting && aggregationFileUriNotContained) {
            String message = "File at rdf:about [" + aggregationFile.getCanonicalPath()
                + "] is not existing AND object's URI " + uri
                + " is not contained in filename! Please check rdf:about value in file "
                + f.getCanonicalPath();
            this.step.setStatus(Status.ERROR, message);
            throw new IOException(message);
          }
        }

        // Check rdf:resource files for existence.
        // TODO Maybe use RDF library for getting the tag value?
        int index = 0;
        while ((index = aggregation.indexOf("rdf:resource=\"", index)) != -1) {
          String resource = aggregation.substring(index + 14);
          resource = resource.substring(0, (resource.indexOf("\"")));
          index++;

          File resourcesFile = new File(f.getParent(), resource);
          // Warn if resource file in rdf:resource is not existing.
          boolean resourceFileNotExisting = false;
          if (!resourcesFile.exists()) {
            String message = "File at rdf:resource [" + resourcesFile.getCanonicalPath()
                + "] is not existing! Please check rdf:resource value in file "
                + f.getCanonicalPath();
            resourceFileNotExisting = true;
            this.step.setStatus(Status.WARNING, message);
          }
          // Warn if TextGrid URI is not contained in the filename of the current resource file!
          boolean resourcesFileUriNotContained = false;
          String strippedUri = ImportUtils.stripUriPrefix(URI.create(resource));
          if (!resourcesFile.getName().contains(strippedUri)) {
            String message = "Object URI " + strippedUri
                + " is not contained in filename! Please check rdf:resource value in file "
                + f.getCanonicalPath();
            resourcesFileUriNotContained = true;
            this.step.setStatus(Status.WARNING, message);
          }
          // Go to error state if both is not applicable!
          if (resourceFileNotExisting && resourcesFileUriNotContained) {
            String message = "File at rdf:resource [" + resourcesFile.getCanonicalPath()
                + "] is not existing AND object's URI " + uri
                + "is not contained in filename! Please check rdf:about value in file "
                + f.getCanonicalPath();
            this.step.setStatus(Status.ERROR, message);
            throw new IOException(message);
          }
        }

        // Rewrite aggregations with generic rewriting method.
        ImportUtils.rewrite(uri, f, MODE_AGGREGATION_REWRITE, "aggregation data",
            this.ignoredUrlPrefixes, this.step, this.mapping, this.rewritePrefix);
      }
    }

  }

  /**
   * <p>
   * Rewrites TEI files.
   * </p>
   * 
   * @param theFile
   * @throws IOException
   * @throws XMLStreamException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  private void rewriteTEI(File theFile)
      throws IOException, XMLStreamException, SAXException, ParserConfigurationException {

    File files[] = theFile.listFiles();
    if (files == null) {
      throw new IOException(
          "File list " + theFile.getCanonicalPath() + " is null or empty or both!");
    }

    // Loop all files.
    for (File f : files) {

      // First go down into all subfolders.
      if (f.isDirectory()) {
        rewriteTEI(f);
      }

      // Otherwise handle files.
      else if (f.isFile() && f.getName().endsWith(this.xmlSuffix)) {

        String uri = "";
        if (this.createNewRevisions) {
          uri = getTextGridUriFromMetadata(f);
        } else {
          uri = getTextGridUriFromImportMapping(f);
        }

        // Rewrite TEI files with generic rewriting method.
        ImportUtils.rewrite(uri, f, this.teiRewriteSpec, "TEI data", this.ignoredUrlPrefixes,
            this.step, this.mapping, this.rewritePrefix);

      }
    }
  }

  /**
   * <p>
   * If creating new revisions, we do need to get the URI from the metadata files, for we do not
   * have the local file path in the mapping, we only have old and new URIs in there.
   * </p>
   * 
   * @param theFile
   * @return
   * @throws IOException
   */
  private String getTextGridUriFromMetadata(File theFile) throws IOException {

    File metadataFile = new File(theFile + this.metadataSuffix);
    ObjectType metadata = ImportUtils.getMetadata(metadataFile);

    return metadata.getGeneric().getGenerated().getTextgridUri().getValue();
  }

  /**
   * <p>
   * Get URI from import mapping file pathes.
   * </p>
   * 
   * @param theFile
   * @return
   * @throws IOException
   */
  private String getTextGridUriFromImportMapping(File theFile) throws IOException {

    URI uri = theFile.getCanonicalFile().toURI();
    if (uri == null) {
      throw new IOException("URI not existing for file '" + theFile + "'");
    }

    ImportObject object = this.mapping.getImportObjectForLocalURI(uri);
    if (object == null) {
      boolean nonExisting = new File(uri).exists();
      throw new IOException("No import object existing for file " + theFile + " [URI: " + uri + "]"
          + (nonExisting ? "" : " File not found!"));
    }

    return object.getTextgridUri();
  }

  /**
   * <p>
   * Merkwürdischer Trikk! But it's working indeed! Seems to avoid renaming file errors on
   * Windows...
   * </p>
   * 
   * @param theSrc
   * @param theDst
   * @return
   */
  private static boolean moveFile(File theSrc, File theDst) {

    // Loop!
    for (int i = 0; i < 10; i++) {
      // If renaming succeeded, just break and return true!
      if (theSrc.renameTo(theDst)) {
        return true;
      }

      // Otherwise clear garbage and yield thread to clean old references!
      System.gc();
      Thread.yield();
    }

    // Return false if renaming did not succeed after having completed loop!
    return false;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param processData
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   * @param step
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param metadataSuffix
   */
  public void setMetadataSuffix(String metadataSuffix) {
    this.metadataSuffix = metadataSuffix;
  }

  /**
   * @param aggregationSuffix
   */
  public void setAggregationSuffix(String aggregationSuffix) {
    this.aggregationSuffix = aggregationSuffix;
  }

  /**
   * @param editionSuffix
   */
  public void setEditionSuffix(String editionSuffix) {
    this.editionSuffix = editionSuffix;
  }

  /**
   * @param collectionSuffix
   */
  public void setCollectionSuffix(String collectionSuffix) {
    this.collectionSuffix = collectionSuffix;
  }

  /**
   * @param uriPrefix
   */
  public void setUriPrefix(String uriPrefix) {
    this.uriPrefix = uriPrefix;
  }

  /**
   * @param jhoveMetadataSuffix
   */
  public void setJhoveMetadataSuffix(String jhoveMetadataSuffix) {
    this.jhoveMetadataSuffix = jhoveMetadataSuffix;
  }

  /**
   * @param rewritePrefix
   */
  public void setRewritePrefix(String rewritePrefix) {
    this.rewritePrefix = rewritePrefix;
  }

  /**
   * @param xmlSuffix
   */
  public void setXmlSuffix(String xmlSuffix) {
    this.xmlSuffix = xmlSuffix;
  }

  /**
   * @param useBaseUrisInAggregations
   */
  public void setUseBaseUrisInAggregations(boolean useBaseUrisInAggregations) {
    this.useBaseUrisInAggregations = useBaseUrisInAggregations;
  }

  /**
   * @param ignoredUrlPrefixes
   */
  public void setIgnoredUrlPrefixes(String ignoredUrlPrefixes) {
    this.ignoredUrlPrefixes.add(ignoredUrlPrefixes);
  }

  /**
   * @param createNewRevisions
   */
  public void setCreateNewRevisions(boolean createNewRevisions) {
    this.createNewRevisions = createNewRevisions;
  }

  /**
   * @param sortAlphabetically
   */
  public void setSortAlphabetically(boolean sortAlphabetically) {
    this.sortAlphabetically = sortAlphabetically;
  }

  /**
   * @param tgsearchServerUrl
   */
  public void setTgsearchServerUrl(String tgsearchServerUrl) {
    this.tgsearchServerUrl = tgsearchServerUrl;
  }

  /**
   * @param otherTgsearchServerUrl
   */
  public void setOtherTgsearchServerUrl(String otherTgsearchServerUrl) {
    this.otherTgsearchServerUrl = otherTgsearchServerUrl;
  }

  /**
   * @return
   */
  public String getTeiRewriteSpec() {
    return this.teiRewriteSpec;
  }

  /**
   * @param teiRewriteSpec
   */
  public void setTeiRewriteSpec(String teiRewriteSpec) {
    this.teiRewriteSpec = teiRewriteSpec;
  }

}
