/*******************************************************************************
 * This software is copyright (c) 2015 by
 * 
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;
import jakarta.activation.DataHandler;
import jakarta.activation.FileDataSource;
import jakarta.xml.bind.JAXB;
import jakarta.xml.ws.Holder;

/*******************************************************************************
 * TODOLOG
 * 
 *******************************************************************************
 * CHANGELOG
 * 
 * 	2015-01-27	Funk	Removed the job list, if no response of each job is
 * 						needed, we do not need it. 
 * 	2014-12-04	Funk	Implemented metadata response storing.
 *	2014-11-27	Funk	Added threading, finally :-)
 * 	2012-06-22	Funk	Changed files and submitted variables from static to
 * 						class.
 * 	2011-07-06	Funk	Added rewriting for aggregation files, and publishing.
 *	2010-08-17	Funk	Added client-side MTOM configuration (something seems
 * 						to be wrong with the services' WSDL.
 *	2010-08-11	Funk	Finished CXF changes, nice code indeed!
 *	2008-06-30	Funk	New exception names included.
 * 	2008-06-25	Funk	Adapted to the new client stub, added metadata storing
 *						again.
 * 	2008-05-23	Funk	Adapted to the corrected TG-crud client stub.
 * 	2008-05-14	Funk	Using ADB binding now.
 * 	2008-04-02	Funk	Minor changes.
 * 	2008-03-31	Funk	Metadata now come from the HotfolderMetadataProcessor
 * 						module.
 * 	2008-02-25	Funk	Using metadata schema 2008-02-02 again...
 * 	2008-02-20	Funk	Using metadata schema 2008-02-18 now.
 * 	2008-02-15	Funk	Added storing and URI logging.
 * 	2008-02-14	Funk	Using the TextGrid crud client stubs.
 * 	2008-02-13	Funk	Copied from SubmitSipToDias.
 * 
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * Action module that imports all files, and files only, to the TextGrid via a
 * TG-crud service client.
 * </p>
 * 
 * <p>
 * <b>NOTE</b> This method actually CAN be called using threads, do it (actually
 * it IS if using the continue_import policy!)! But only as a single
 * ActionModule in one policy! Either by using more than one folder in the given
 * hotfolder, or by increasing the number of threads in the koLibRI main config
 * file (maxNumberOfThreads)!
 * </p>
 * 
 * <p>
 * <b>NOTE</b> There is now a thread pool inside of THIS method, so we can use
 * both, the threading of each process starter, and the threading of this class!
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2015-01-27
 * @since 2010-11-26
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 ******************************************************************************/

public class SubmitFilesThreadingTest implements ActionModule {

	// **
	// FINALS
	// **

	private static final boolean	NO_REVISION				= false;
	private static final String		URI_SEPARATION_CHAR		= "_";
	private static final String		CREATE_FAILED			= "TG-crud#CREATE failed due to ";

	// **
	// STATICS
	// **

	protected static int			submitted				= 0;

	// **
	// STATE (Instance variables)
	// **

	private ExecutorService			threadPool;
	// private ArrayBlockingQueue<Callable<String>> jobList;

	private ProcessData				processData;
	private Step					step;
	private String					tgcrudServerUrl			= "";
	private String					rbacSessionId			= "";
	private String					logParameter			= "";
	private String					projectId				= "";
	private boolean					storeResponseMetadata	= false;
	private boolean					deleteIfIngested		= false;
	private String					metadataSuffix			= ".meta";
	private String					uriPrefix				= "textgrid";
	private int						files;
	private int						submitThreads			= 3;

	/**
	 * <p>
	 * <b>NEEDS</b>
	 * <ul>
	 * <li>A working TG-crud service.</li>
	 * <li>The pathToContentFiles to be set.</li>
	 * <li>The import folder filled with files (and folders), one metadata file
	 * for each data file is expected.</li>
	 * </ul>
	 * </p>
	 * 
	 * <p>
	 * <b>GIVES</b>
	 * <ul>
	 * <li>Imports every file in the import folder (not the folders!).</li>
	 * <li>The folder structure is generated (after importing) by the before
	 * generated aggregation files, if existing.</li>
	 * </ul>
	 * </p>
	 * 
	 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
	 */
	public void go() {

		// Set start time.
		long startTime = System.currentTimeMillis();

		this.step.setStatus(Status.RUNNING, "TG-crud web service WSDL file: "
				+ this.tgcrudServerUrl);

		// Get import folder.
		File importFolder = new File(this.processData.getPathToContentFiles());

		this.step.setStatus(Status.RUNNING, "Submitting all files in: "
				+ importFolder.getAbsolutePath());

		// Get the TG-crud stub.
		TGCrudService tgcrud = null;
		try {
			tgcrud = TGCrudClientUtilities.getTgcrud(this.tgcrudServerUrl);
		} catch (MalformedURLException e) {
			this.step.setStatus(Status.ERROR,
					"Malformed Service URL: " + e.getMessage());
			return;
		}

		// Count TextGrid objects.
		countFiles(importFolder);

		// Set thread pool and max submit thread size.
		this.threadPool = Executors.newFixedThreadPool(this.submitThreads);

		this.step.setStatus(Status.RUNNING, "FixedThreadPool created with "
				+ this.submitThreads + " thread"
				+ (this.submitThreads != 1 ? "s" : ""));

		// Submit all files, loop recursively.
		try {
			submitFiles(tgcrud, importFolder);
		} catch (MetadataParseFault e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR,
					"System message: " + e.getMessage());
			return;
		} catch (IoFault e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR,
					"System message: " + e.getMessage());
			return;
		} catch (AuthFault e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR,
					"System message: " + e.getMessage());
			return;
		} catch (ObjectNotFoundFault e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR,
					"System message: " + e.getMessage());
			return;
		} catch (IOException e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR,
					"System message: " + e.getMessage());
			return;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR,
					"System message: " + e.getMessage());
			return;
		} catch (SAXException e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR,
					"System message: " + e.getMessage());
			return;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR,
					"System message: " + e.getMessage());
			return;
		} catch (UpdateConflictFault e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR,
					"System message: " + e.getMessage());
			return;
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR,
					"System message: " + e.getMessage());
			return;
		} catch (XMLStreamException e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR,
					"System message: " + e.getMessage());
			return;
		} catch (InterruptedException e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR,
					"System message: " + e.getMessage());
			return;
		} catch (ExecutionException e) {
			e.printStackTrace();
			this.step.setStatus(Status.ERROR,
					"System message: " + e.getMessage());
			return;
		}

		// Shut down thread pool.
		this.threadPool.shutdown();

		// Wait if thread pool is not terminated.
		while (!this.threadPool.isTerminated()) {
			//
		}

		// Log duration and set status to DONE.
		String duration = KolibriTimestamp.getDurationWithMillis(System
				.currentTimeMillis() - startTime);
		this.step.setStatus(Status.DONE,
				"All files have been sucessfully submitted in " + duration);
	}

	// **
	// INTERNAL
	// **

	/**
	 * <p>
	 * Submit files.
	 * </p>
	 * 
	 * @param theTgcrud
	 * @param theFile
	 * @throws MetadataParseFault
	 * @throws IoFault
	 * @throws ObjectNotFoundFault
	 * @throws AuthFault
	 * @throws AuthenticationFault
	 * @throws IOException
	 * @throws XPathExpressionException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws UpdateConflictFault
	 * @throws DatatypeConfigurationException
	 * @throws XMLStreamException
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	private void submitFiles(TGCrudService theTgcrud, File theFile)
			throws MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault,
			IOException, XPathExpressionException, SAXException,
			ParserConfigurationException, UpdateConflictFault,
			DatatypeConfigurationException, XMLStreamException,
			InterruptedException, ExecutionException {

		File files[] = theFile.listFiles();
		if (files == null) {
			throw new IOException("File list " + theFile.getCanonicalPath()
					+ " is null or empty or both!");
		}

		// Loop all files.
		for (File f : files) {

			// Start recursion if directory is found.
			if (f.isDirectory()) {
				submitFiles(theTgcrud, f);
			}

			// Submit files only.
			else if (f.isFile() && !f.getName().endsWith(this.metadataSuffix)) {
				// Strip filename and get the URI.
				if (!f.getName().contains(URI_SEPARATION_CHAR)) {
					throw new IOException(
							"File was not created by koLibRI (URI missing): "
									+ f.getName()
									+ ", maybe you mixed up policies?");
				}
				String uri = this.uriPrefix
						+ ":"
						+ f.getName().substring(0,
								f.getName().indexOf(URI_SEPARATION_CHAR));

				// Get data file (or aggregation data file), depending on
				// filename.
				File metadataFile = new File(f.getParentFile(), f.getName()
						+ this.metadataSuffix);
				MetadataContainerType metadata = JAXB.unmarshal(metadataFile,
						MetadataContainerType.class);
				DataHandler data = new DataHandler(new FileDataSource(f));

				// We want to accept MetadataContainerTypes and ObjectTypes as
				// XML. So we check if MetadataContainerType contains
				// ObjectType, if not, read ObjectType and set
				// MetadataContainerType.
				if (metadata.getObject() == null) {
					metadata.setObject(JAXB.unmarshal(metadataFile,
							ObjectType.class));
				}

				// Check title and format tags.
				String title = metadata.getObject().getGeneric().getProvided()
						.getTitle().get(0);
				String format = metadata.getObject().getGeneric().getProvided()
						.getFormat();
				if (title == null || title.equals("") || format == null
						|| format.equals("")) {
					throw new IoFault("[" + uri
							+ "] Both title and format must be set!");
				}

				// Create and execute submit job.
				TGCrudSubmitJob job = new TGCrudSubmitJob(theTgcrud,
						this.rbacSessionId, this.logParameter, uri,
						this.projectId, metadata, data, this.step,
						metadataFile, f, this.deleteIfIngested, this.files,
						format, title, this.storeResponseMetadata);

				this.threadPool.execute(job);

				Thread.sleep(5000);
			}
		}
	}

	/**
	 * <p>
	 * Count TextGrid objects.
	 * </p>
	 * 
	 * @param theFile
	 */
	private void countFiles(File theFile) {

		File files[] = theFile.listFiles();
		if (files == null) {
			return;
		}

		for (File f : files) {
			// Count files.
			if (f.isFile() && !f.getName().endsWith(this.metadataSuffix)) {
				this.files++;
			}
			// Count recursively.
			else if (f.isDirectory()) {
				countFiles(f);
			}
		}
	}

	// **
	// GETTERS AND SETTERS
	// **

	/**
	 * @param processData
	 *            The processData to set.
	 */
	public void setProcessData(ProcessData processData) {
		this.processData = processData;
	}

	/**
	 * @param step
	 *            The step to set.
	 */
	public void setStep(Step step) {
		this.step = step;
	}

	/**
	 * @param rbacSessionId
	 */
	public void setRbacSessionId(String rbacSessionId) {
		this.rbacSessionId = rbacSessionId;
	}

	/**
	 * @param projectId
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * @param tgcrudServerUrl
	 */
	public void setTgcrudServerUrl(String tgcrudServerUrl) {
		this.tgcrudServerUrl = tgcrudServerUrl;
	}

	/**
	 * @param logParameter
	 */
	public void setLogParameter(String logParameter) {
		this.logParameter = logParameter;
	}

	/**
	 * @param storeResponseMetadata
	 */
	public void setStoreResponseMetadata(boolean storeResponseMetadata) {
		this.storeResponseMetadata = storeResponseMetadata;
	}

	/**
	 * @param deleteIfIngested
	 */
	public void setDeleteIfIngested(boolean deleteIfIngested) {
		this.deleteIfIngested = deleteIfIngested;
	}

	/**
	 * @param metadataSuffix
	 */
	public void setMetadataSuffix(String metadataSuffix) {
		this.metadataSuffix = metadataSuffix;
	}

	/**
	 * @param uriPrefix
	 */
	public void setUriPrefix(String uriPrefix) {
		this.uriPrefix = uriPrefix;
	}

	/**
	 * @param submitThreads
	 */
	public void setSubmitThreads(int submitThreads) {
		this.submitThreads = submitThreads;
	}

	/**
	 * <p>
	 * Starts a new TG-crud client as a thread.
	 * </p>
	 */

	private class TGCrudSubmitJob implements Runnable {

		private TGCrudService			tgcrud;
		protected String				rbacSessionId;
		protected String				logParameter;
		protected String				uri;
		protected String				projectId;
		protected MetadataContainerType	metadata;
		protected DataHandler			data;
		protected Step					step;
		protected File					metadataFile;
		protected File					dataFile;
		protected boolean				deleteIfIngested;
		protected int					files;
		protected String				format;
		protected String				title;
		protected boolean				storeResponseMetadata;

		/**
		 * @param theTgcrud
		 * @param theRbacSessionId
		 * @param theLogParameter
		 * @param theUri
		 * @param theProjectId
		 * @param theMetadata
		 * @param theData
		 * @param theStep
		 * @param theMetadataFile
		 * @param theDataFile
		 * @param theDeleteIfIngested
		 * @param theFiles
		 * @param theFormat
		 * @param theTitle
		 * @param theMetadataStoring
		 */
		public TGCrudSubmitJob(TGCrudService theTgcrud,
				String theRbacSessionId, String theLogParameter, String theUri,
				String theProjectId, MetadataContainerType theMetadata,
				DataHandler theData, Step theStep, File theMetadataFile,
				File theDataFile, boolean theDeleteIfIngested, int theFiles,
				String theFormat, String theTitle, boolean theMetadataStoring) {

			this.tgcrud = theTgcrud;
			this.rbacSessionId = theRbacSessionId;
			this.logParameter = theLogParameter;
			this.uri = theUri;
			this.projectId = theProjectId;
			this.metadata = theMetadata;
			this.data = theData;
			this.step = theStep;
			this.metadataFile = theMetadataFile;
			this.dataFile = theDataFile;
			this.deleteIfIngested = theDeleteIfIngested;
			this.files = theFiles;
			this.format = theFormat;
			this.title = theTitle;
			this.storeResponseMetadata = theMetadataStoring;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		public void run() {

			// Create metadata holder.
			Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>(
					this.metadata);

			this.step.setStatus(Status.RUNNING, "[" + this.uri + "] "
					+ this.title + " (" + this.format + ", " + this.files
					+ " byte" + (this.files != 1 ? "s" : "") + ")");

			// Call TG-crud#CREATE.
			try {

				// Record create start time.
				long fileSubmitStartTime = System.currentTimeMillis();

				this.tgcrud.create(this.rbacSessionId, this.logParameter,
						this.uri, NO_REVISION, this.projectId, metadataHolder,
						this.data);

				// Compute create duration.
				String duration = KolibriTimestamp.getDurationWithMillis(System
						.currentTimeMillis() - fileSubmitStartTime);

				// Store metadata file, if configured.
				if (this.storeResponseMetadata) {
					try {
						FileOutputStream metadataStream = new FileOutputStream(
								new File(
										this.metadataFile.getParentFile(),
										this.uri
												+ SubmitFilesThreadingTest.this.metadataSuffix
												+ ".xml"));
						JAXB.marshal(metadataHolder.value.getObject(),
								metadataStream);
						metadataStream.close();

						this.step.setStatus(Status.RUNNING, "[" + this.uri
								+ "] Stored response metadata");

					} catch (FileNotFoundException e) {
						this.step.setStatus(
								Status.WARNING,
								"[" + this.uri
										+ "] Metadata could not be saved: "
										+ e.getMessage());
					} catch (IOException e) {
						this.step.setStatus(
								Status.WARNING,
								"[" + this.uri
										+ "] Metadata could not be saved: "
										+ e.getMessage());
					}
				}

				// Delete successfully imported data and metadata files.
				if (this.deleteIfIngested && this.metadataFile.delete()
						&& this.dataFile.delete()) {
					this.step.setStatus(Status.RUNNING, "[" + this.uri
							+ "] Successfully removed files from temp folder");
				}

				// Log duration.
				this.step.setStatus(Status.RUNNING, "[" + this.uri + "] -- "
						+ ++SubmitFilesThreadingTest.submitted + "/"
						+ this.files + " -- Submission completed in "
						+ duration);

			} catch (MetadataParseFault e) {
				this.step.setStatus(
						Status.WARNING,
						"[" + this.uri + "] " + CREATE_FAILED
								+ e.getClass().getName() + ": "
								+ e.getMessage());
			} catch (ObjectNotFoundFault e) {
				this.step.setStatus(
						Status.WARNING,
						"[" + this.uri + "] " + CREATE_FAILED
								+ e.getClass().getName() + ": "
								+ e.getMessage());
			} catch (IoFault e) {
				this.step.setStatus(
						Status.WARNING,
						"[" + this.uri + "] " + CREATE_FAILED
								+ e.getClass().getName() + ": "
								+ e.getMessage());
			} catch (AuthFault e) {
				this.step.setStatus(
						Status.WARNING,
						"[" + this.uri + "] " + CREATE_FAILED
								+ e.getClass().getName() + ": "
								+ e.getMessage());
			}
		}

	}

}
