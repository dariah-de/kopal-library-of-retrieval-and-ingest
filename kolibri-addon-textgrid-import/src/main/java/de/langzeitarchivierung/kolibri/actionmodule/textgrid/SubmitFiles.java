/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.IOException;
import java.net.MalformedURLException;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;
import jakarta.activation.DataHandler;
import jakarta.xml.ws.Holder;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2020-05-14 - Funk - Re-formatting.
 * 
 * 2017-03-30 - Funk - First version.
 */

/**
 * <p>
 * Submits a file using TG-crud.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-05-14
 * @since 2017-03-30
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 */

public class SubmitFiles extends SubmitFilesAbs {

  // **
  // STATIC
  // **

  private static TGCrudService tgcrud;

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.textgrid.SubmitFilesAbs#
   * create(de.langzeitarchivierung.kolibri.Step, java.lang.String, java.lang.String,
   * java.lang.String, boolean, java.lang.String, javax.xml.ws.Holder, javax.activation.DataHandler)
   */
  @Override
  protected void create(Step theStep, String theRbacSessionId,
      String theLogParameter, String theUri, boolean createNewRevision,
      String theProjectId, Holder<MetadataContainerType> theMetadata,
      DataHandler theData) throws IOException {

    // Check for and get TG-crud client.
    if (tgcrud == null) {

      theStep.setStatus(Status.RUNNING, "TG-crud web service URL: " + this.getTgcrudServerUrl());

      // Get the TG-crud stub.
      try {
        tgcrud = TGCrudClientUtilities.getTgcrud(this.getTgcrudServerUrl());
      } catch (MalformedURLException e) {
        theStep.setStatus(Status.ERROR, "Malformed Service URL: " + e.getMessage());
        return;
      }
    }

    // Do a crud CREATE.
    try {
      tgcrud.create(theRbacSessionId, theLogParameter, theUri, createNewRevision, theProjectId,
          theMetadata, theData);
    } catch (ObjectNotFoundFault | AuthFault | IoFault | MetadataParseFault e) {
      throw new IOException(e.getClass().getName() + ": " + e);
    }
  }

}
