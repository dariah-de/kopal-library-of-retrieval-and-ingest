/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import de.langzeitarchivierung.kolibri.Status;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;

/**
 * CHANGELOG
 * 
 * 2017-03-27 - Funk - First version.
 */

/**
 * <p>
 * Fetches TextGrid URIs from TG-crud.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-06-29
 * @since 2017-03-27
 */

public class GetUris extends GetUrisAbs {

  // **
  // STATIC
  // **

  private static TGCrudService tgcrud;

  /**
   *
   */
  @Override
  protected List<String> getUris(int howMany) {

    List<String> result = new ArrayList<String>();

    // Get the TG-crud stub.
    if (tgcrud == null) {
      try {

        this.step.setStatus(Status.RUNNING,
            "Creating TG-crud service from endpoint: " + this.tgcrudServerUrl);

        tgcrud = TGCrudClientUtilities.getTgcrud(this.tgcrudServerUrl);

      } catch (MalformedURLException e) {
        this.step.setStatus(Status.ERROR, "Malformed Service URL: " + e.getMessage());
      }
    }

    // Get URIs.
    try {
      result = tgcrud.getUri(this.rbacSessionId, this.logParameter, NO_PROJECTID, howMany);
    } catch (ObjectNotFoundFault | IoFault | AuthFault e) {
      this.step.setStatus(Status.ERROR, ERROR_FETCHING + e.getMessage());
    }

    return result;
  }

}
