/*******************************************************************************
 * This software is copyright (c) 2012 by
 * 
 * - TextGrid Consortion (http://www.textgrid.de) - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortion (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Wolfgang Pempe (pempe@saphor.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.cxf.jaxrs.ext.xml.XMLSource;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import info.textgrid.clients.SearchClient;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.namespaces.middleware.tgsearch.Response;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;

/**
 * CHANGELOG
 * 
 * 2013-03-08 - Veentjer - Copied from ProcessDfgViewerMets.
 */

/**
 * <p>
 * Reads and processes a DFG Viewer METS file.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2012-08-27
 * @since 2012-02-20
 */

public class PrepareTeiImport implements ActionModule {

  // **
  // FINALS
  // **

  private final static String WORK_ID = "WORK";

  // **
  // STATE
  // **

  private ProcessData processData;
  private Step step;
  private String workSuffix = ".work";
  private String rootAggregationMimetype = TextGridMimetypes.COLLECTION;
  private String rbacSessionId;
  private String projectId;
  private String tgsearchServerUrl;

  /**
   * <p>
   * Create Work File for TEI Import
   * </p>
   * 
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>The PathToContentFiles set (normally done by the appropriate processStarter) containing an
   * appropriate TEI file.</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>Work File Creation</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Set start time.
    long startTime = System.currentTimeMillis();

    // Start and set status to RUNNING.
    this.step.setStatus(Status.RUNNING, "PrepareTeiImport");

    File teiFile = null;
    // first xml file is chosen as teifile, there should be only on xml
    // (tei) file per job
    for (File file : this.processData.getFileList()) {
      if (file.getName().endsWith(".xml")) {
        teiFile = file;
      }
    }

    // Checking if collection or edition shall be used for the root folder.
    if (!this.rootAggregationMimetype.equals(TextGridMimetypes.EDITION)
        && !this.rootAggregationMimetype
            .equals(TextGridMimetypes.COLLECTION)) {
      this.step.setStatus(Status.ERROR, "Invalid mimetype: "
          + this.rootAggregationMimetype
          + "! Only Edition or Collection can be used!");
      return;
    } else {
      this.step.setStatus(Status.RUNNING, "Root agregation mimetype: "
          + this.rootAggregationMimetype);
    }

    // create work
    try {

      // Create work file, if edition shall be used.
      if (this.rootAggregationMimetype.equals(TextGridMimetypes.EDITION)) {
        getOrCreateWork(teiFile);

      }
    } catch (IOException e) {
      this.step.setStatus(Status.ERROR, "IOException: " + e.getMessage());
      e.printStackTrace();
      return;
    }

    // Log duration and set status to DONE.
    String duration = KolibriTimestamp.getDurationWithMillis(System
        .currentTimeMillis() - startTime);
    this.step.setStatus(Status.DONE, "TEI import preparation done"
        + duration);
  }

  private void getOrCreateWork(File teiFile) throws IOException {

    if (workExists(getUniformTitleFromTeiFile(teiFile))) {
      return;
    }

    File workFile = new File(teiFile.getParent(), teiFile.getParentFile()
        .getName() + this.workSuffix);

    if (workFile.exists()) {
      this.step.setStatus(Status.WARNING, "[" + WORK_ID
          + "] File already created: " + workFile.getAbsolutePath());
    } else if (!workFile.createNewFile()) {
      throw new IOException("[" + WORK_ID + "] Failed to create file: "
          + workFile.getAbsolutePath());
    }
    // Put file into the file list.
    this.processData.getFileList().add(workFile);

    this.step.setStatus(Status.RUNNING,
        "[" + WORK_ID + "] File created and added to file list: "
            + workFile.getAbsolutePath());

    this.step.setStatus(Status.RUNNING,
        "Work object created: " + workFile.getAbsolutePath());
  }

  private boolean workExists(String uniformTitle) {

    this.step
        .setStatus(Status.RUNNING,
            "Querying tgsearch for work with matching uniform title in target project");

    // TODO Using new textgrid-clients! Please check if this is correct!
    SearchClient sc = new SearchClient(this.tgsearchServerUrl);
    Response res = sc
        .searchQuery()
        .setSid(this.rbacSessionId)
        .setQuery(
            "format:\"text/tg.work+xml\" project@id:.@\""
                + this.projectId + "\"")
        .execute();

    for (ResultType r : res.getResult()) {
      for (String title : r.getObject().getGeneric().getProvided()
          .getTitle()) {
        if (title.equals(uniformTitle)) {
          this.step.setStatus(Status.RUNNING,
              "Work with matching uniform title found in target project: "
                  + r.getObject().getGeneric().getGenerated()
                      .getTextgridUri().getValue());
          return true;

        }

      }
    }

    this.step.setStatus(Status.RUNNING,
        "No work with matching uniform title found in target project");
    return false;
  }

  public static String getUniformTitleFromTeiFile(File teiFile)
      throws FileNotFoundException {

    XMLSource source = new XMLSource(new FileInputStream(teiFile));
    source.setBuffering();

    Map<String, String> namespaces = new HashMap<String, String>();
    namespaces.put("tei", "http://www.tei-c.org/ns/1.0");
    String title = source
        .getValue(
            "//tei:fileDesc/tei:titleStmt//tei:title[@type='uniform']/text()",
            namespaces);

    return title;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param processData
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   * @param rbacSessionId
   */
  public void setRbacSessionId(String rbacSessionId) {
    this.rbacSessionId = rbacSessionId;
  }

  /**
   * @param tgsearchServerUrl
   */
  public void setTgsearchServerUrl(String tgsearchServerUrl) {
    this.tgsearchServerUrl = tgsearchServerUrl;
  }

  /**
   * @param projectId
   */
  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  /**
   * @param step
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param rootAggregationMimetype
   */
  public void setRootAggregationMimetype(String rootAggregationMimetype) {
    this.rootAggregationMimetype = rootAggregationMimetype;
  }

}
