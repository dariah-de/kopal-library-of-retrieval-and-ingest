/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.IOException;
import java.util.List;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import info.textgrid.clients.SearchClient;
import info.textgrid.middleware.tgpublish.client.PublishClient;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.RelationsExistFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import jakarta.ws.rs.core.Response;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 
 * 2022-12-19 - Funk - fix #220
 * <https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/-/issues/220>
 * 
 * 2022-05-10 - Funk - Add error handling from TG-publish call.
 * 
 * 2019-03-11 - Funk - Check for nearlyPublished in DRY RUN mode, too.
 * 
 * 2016-10-07 - Funk - Adapt to generic HandleFilesGeneric class.
 * 
 * 2016-10-07 - Funk - Copy from DeleteFiles, and generalised.
 */

/**
 * <p>
 * Action module that finally publishes all files: Calls TG-publish#publish and that is deleting the
 * isNearlyPublished relation in the RDF and IDX databases and removes nearlyPublished flag in
 * TG-auth.
 * </p>
 * 
 * @author Stefan Funk, DAASI International GmbH
 * @author Stefan Funk, SUB Göttingen
 * @version 2022-06-10
 * @since 2011-10-06
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 */

public class PublishFiles extends HandleFilesGeneric implements ActionModule {

  private static final int MAX_NP_REQUEST_REPEATS = 5;
  private static final long NEXT_QUERY_OFFSET = 375;

  // **
  // ABSTRACT METHODS
  // **

  /**
   *
   */
  @Override
  protected void doSingleThing(final TGCrudService theTGcrud, final SearchClient theTGsearch,
      final PublishClient theTGpublish, final String theSessionId, final String theLogParameter,
      final String theUri, boolean dryRun)
      throws ObjectNotFoundFault, IoFault, RelationsExistFault, AuthFault {

    // Check for nearlyPublishedFlag using tgsearch.
    try {
      boolean nearlyPublished = isInSandbox(theTGsearch, theUri);
      if (!nearlyPublished) {
        throw new IoFault("Ignoring already published TextGrid object: " + theUri);
      }
    } catch (IOException e) {
      throw new IoFault(e.getMessage());
    }

    // Now do publish (if not dry-run!).
    this.step.setStatus(Status.RUNNING,
        (this.dryrun ? DRYRUN : "") + "[" + theUri + "] publishing sandbox data");

    if (!dryRun) {
      Response publishingStarted = theTGpublish.publishSandboxData(theUri);
      int statusCode = publishingStarted.getStatus();
      String reasonPhrase = publishingStarted.getStatusInfo().getReasonPhrase();

      // Check status code!
      if (statusCode != jakarta.ws.rs.core.Response.Status.OK.getStatusCode()) {
        throw new IoFault("Publish call does not succeed! Got " + statusCode + " " + reasonPhrase
            + "! Please try again later!");
      }
    }

    // Check for sandbox flag using tgsearch until false.
    for (int i = 0; i < MAX_NP_REQUEST_REPEATS; i++) {

      try {
        boolean nearlyPublished = isInSandbox(theTGsearch, theUri);

        // TODO Do let publishStatus get the correct publish status!
        // PublishResponse res = publishClient.getMinistatus(uri);

        // If we are in dryRun, just check nearlyPublished flag once, log, and break.
        if (dryRun) {
          this.step.setStatus(Status.RUNNING,
              DRYRUN + "[" + theUri + "]" + (nearlyPublished ? " STILL IS" : " IS NOT")
                  + " in the sandbox (according to TG-search).");
          break;
        }

        // Let's break if not nearlyPublished anymore!
        if (!nearlyPublished) {
          break;
        }
      } catch (IOException e) {
        throw new IoFault(e.getMessage());
      }

      try {
        Thread.sleep(NEXT_QUERY_OFFSET);
      } catch (InterruptedException e) {
        this.step.setStatus(Status.RUNNING, (this.dryrun ? DRYRUN : "") + WAITING);
      }
    }
  }

  /**
   *
   */
  @Override
  protected String doo() {
    return "<<publish>>";
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Checks object with given URI using TG-search for sandbox flag set.
   * </p>
   * 
   * @param theClient
   * @param theUri
   * @return
   * @throws IOException
   */
  private static boolean isInSandbox(SearchClient theTGsearch, String theUri)
      throws IOException {

    Boolean result;

    info.textgrid.namespaces.middleware.tgsearch.Response response =
        theTGsearch.infoQuery().getMetadata(theUri);
    List<ResultType> res = response.getResult();

    // Have we got a null or empty list? --> Not yet published!
    if (res != null && !res.isEmpty()) {
      result = res.get(0).isSandbox();
      // Haven't we got no flag at all? --> No sandbox!
      if (result == null) {
        result = false;
      }

    } else {
      throw new IOException("Got no nothin' from TG-search! Did ya really publish " + theUri + "?");
    }

    return result;
  }

}
