/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Wolfgang Pempe (pempe@saphor.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.textgrid.util.ImportUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import jakarta.activation.MimeType;
import jakarta.activation.MimeTypeParseException;
import jakarta.xml.bind.JAXB;

/**
 * TODOLOG
 ** 
 * CHANGELOG
 * 
 * 2020-05-18 - Funk - Using ImportUtils for reading metadata files now.
 * 
 * 2020-02-20 - Funk - Reformat class.
 */

/**
 * <p>
 * This metadata processor takes the generated TextGrid metadata files from the
 * TextgridMetadataProcessor and adds all information from the DFG Viewer METS file (MODS and/or TEI
 * metadata).
 * </p>
 * 
 * <p>
 * You can choose if the root aggregation will be an edition or a collection, all other aggregations
 * will be just that, aggregations.
 * </p>
 * 
 * @author Stefan Funk, DAASI International GmbH
 * @version 2020-05-18F
 * @since 2012-02-20
 */

public class DfgViewerMetadataProcessor implements ActionModule {

  // **
  // FINALS
  // **

  private static final String ROOT_ID = "ROOT";
  private static final String ROOT_SEPARATOR = "__";
  private static final HashMap<String, String> NO_PARAMS = new HashMap<String, String>();

  // **
  // STATE
  // **

  protected ProcessData processData;
  protected Step step;
  protected File sourceFile;

  protected String metadataSuffix = ".meta";
  protected String aggregationSuffix = ".aggregation";
  protected String workSuffix = ".work";
  protected String rootAggregationMimetype = TextGridMimetypes.AGGREGATION;
  protected String itemTransformationFile;
  protected String workTransformationFile;
  protected String editionTransformationFile;
  protected String collectionTransformationFile;
  protected String aggregationTransformationFile;
  protected boolean forceMimetypeExtraction = false;

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>Already generated TextGrid metadata files in the temp folder.</li>
   * <li>A path to the content files (normally provided by the processStarter).</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>Enhanced metadata from the DMD sections of the DFG Viewer METS file.</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Set start time.
    long startTime = System.currentTimeMillis();

    // Set status to RUNNING.
    this.step.setStatus(Status.RUNNING, "DFG Viewer metadata processing started");

    // Get import folder and METS file.
    File importFolder = new File(this.processData.getPathToContentFiles() + File.separatorChar
        + ROOT_ID + ROOT_SEPARATOR + this.processData.getProcessName());
    this.sourceFile = new File(importFolder, this.processData.getProcessName());

    // Checking if collection or edition shall be used for the root folder.
    if (!this.rootAggregationMimetype.equals(TextGridMimetypes.EDITION)
        && !this.rootAggregationMimetype.equals(TextGridMimetypes.COLLECTION)) {
      this.step.setStatus(Status.ERROR, "Invalid mimetype: " + this.rootAggregationMimetype
          + "! Only Edition or Collection can be used!");
      return;
    } else {
      this.step.setStatus(Status.RUNNING,
          "Root agregation mimetype: " + this.rootAggregationMimetype);
    }

    // Check rootAggregationMimetype, extract EDITION and WORK metadata or COLLECTION. AGGREGATION
    // metadata is transformed in ITEM section below.
    try {

      // Get root aggregation file.
      File rootAggregation = new File(importFolder,
          ROOT_ID + ROOT_SEPARATOR + this.processData.getProcessName() + this.aggregationSuffix);

      // Check for root collection.
      if (this.rootAggregationMimetype.equals(TextGridMimetypes.COLLECTION)) {
        transformCollectionMetadata(
            new File(rootAggregation.getAbsolutePath() + this.metadataSuffix));
      }

      // Check for root edition and work.
      else if (this.rootAggregationMimetype.equals(TextGridMimetypes.EDITION)) {
        transformEditonAndWorkMetadata(rootAggregation);
      }

      // Loop through all the files, and get ITEM metadata out of the METS.
      processItemMetadata(importFolder);

      // Log duration and set status to DONE.
      String duration =
          KolibriTimestamp.getDurationWithMillis(System.currentTimeMillis() - startTime);
      this.step.setStatus(Status.DONE,
          "DFG Viewer metadata added to TextGrid metadata in " + duration);

    } catch (TransformerException | IOException e) {
      this.step.setStatus(Status.ERROR, e.getClass().getSimpleName() + ": " + e.getMessage());
      return;
    }
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Adds additional metadata from the METS file via XSLT stylesheet, provided by the
   * itemTransformationFile. Title information can also be changed.
   * </p>
   * 
   * @param theFileList
   * @throws IOException
   * @throws TransformerException
   * @throws FileNotFoundException
   */
  private void processItemMetadata(File theFile)
      throws FileNotFoundException, TransformerException, IOException {

    File files[] = theFile.listFiles();
    if (files == null) {
      throw new IOException(
          "File list " + theFile.getCanonicalPath() + " is null or empty or both!");
    }

    // Loop files.
    for (File f : files) {

      String logName = "[" + f.getName() + "] ";

      // Recurse if directory is found.
      if (f.isDirectory()) {
        processItemMetadata(f);
      }

      // Check metadata files.
      else if (f.getName().endsWith(this.metadataSuffix)) {

        // Get metadata object from file and check format.
        ObjectType oldMetadata = ImportUtils.getMetadata(f);

        // Get original format.
        String format = oldMetadata.getGeneric().getProvided().getFormat();

        this.step.setStatus(Status.RUNNING,
            logName + "Found format: " + format);

        // If we have NOT an aggregation, edition, collection or work, transform item metadata and
        // merge (ITEMs only).
        if (!TextGridMimetypes.AGGREGATION_SET.contains(format)
            && !format.equals(TextGridMimetypes.WORK)) {
          transformItemMetadata(f);
        }

        // If we have an AGGREGATION (ROOT aggregation is included here!)
        else if (format.equals(TextGridMimetypes.AGGREGATION)) {
          transformAggregationMetadata(f);
        }
      }
    }
  }

  /**
   * @param theFormat
   * @param theMetadataFile
   * @throws IOException
   * @throws TransformerException
   * @throws FileNotFoundException
   */
  private void transformItemMetadata(File theMetadataFile)
      throws FileNotFoundException, TransformerException, IOException {

    // Get metadata object.
    ObjectType oldMetadata = ImportUtils.getMetadata(theMetadataFile);

    // Get log name.
    String logName = "[" + theMetadataFile.getName() + "] ";

    // Get METS file ID from filename, if item is NOT the METS file itself.
    String metsFileId = "";
    if (!theMetadataFile.getAbsolutePath()
        .equals(this.sourceFile.getAbsolutePath() + this.metadataSuffix)) {
      metsFileId = theMetadataFile.getName().substring(0,
          theMetadataFile.getName().lastIndexOf(this.metadataSuffix));
    }

    this.step.setStatus(Status.RUNNING,
        logName + "Transforming ITEM metadata from METS using FILE ID: " + metsFileId);

    // FIXME Seems that JHOVE technical metadata from the custom TextGrid metadata is getting lost
    // here! Doesn't matter at the moment, because we do not want to store it inside the TextGrid
    // metadata anyway!
    // FIXME Implement with DARIAH compliant workflow if needed!

    // Transform.
    File itemTransformation = new File(this.itemTransformationFile);
    HashMap<String, String> parameters = new HashMap<String, String>();
    parameters.put("metsid", metsFileId);
    String itemMetadata = transform(new FileInputStream(this.sourceFile),
        new FileInputStream(itemTransformation), parameters);

    // Get new item metadata object.
    ObjectType newMetadata = JAXB.unmarshal(new StringReader(itemMetadata), ObjectType.class);

    // Check mimetype as format (from METS file or from Tika).
    checkFormat(oldMetadata, newMetadata, logName);

    // Check if we have got a valid title list in the new metadata, if not, take the old title list.
    List<String> oldTitles = oldMetadata.getGeneric().getProvided().getTitle();
    List<String> newTitles = newMetadata.getGeneric().getProvided().getTitle();
    if (newTitles.isEmpty() || newTitles.get(0).equals("")) {
      newMetadata.getGeneric().getProvided().getTitle().clear();
      newMetadata.getGeneric().getProvided().getTitle().addAll(oldTitles);
    } else {
      // Only change item titles, if item is the METS file itself.
      if (metsFileId.equals("")) {
        changeTitleInMetadataFile(newTitles);
      }
    }

    // Store new metadata object.
    MetadataContainerType newMetadataContainer = new MetadataContainerType();
    newMetadataContainer.setObject(newMetadata);
    JAXB.marshal(newMetadataContainer, theMetadataFile);

    this.step.setStatus(Status.RUNNING, logName + "ITEM metadata merge complete");
  }

  /**
   * @param theOldMetadata
   * @param theMetadataFile
   * @throws IOException
   * @throws TransformerException
   * @throws FileNotFoundException
   */
  private void transformAggregationMetadata(File theMetadataFile)
      throws FileNotFoundException, TransformerException, IOException {

    // Get metadata object.
    ObjectType oldMetadata = ImportUtils.getMetadata(theMetadataFile);

    // Get log name and format.
    String logName = "[" + theMetadataFile.getName() + "] ";
    List<String> oldTitles = oldMetadata.getGeneric().getProvided().getTitle();

    // Get METS DIV ID from filename.
    String metsDivId = theMetadataFile.getName().substring(0,
        theMetadataFile.getName().lastIndexOf(this.aggregationSuffix + this.metadataSuffix));

    this.step.setStatus(Status.RUNNING,
        logName + "Transforming AGGREGATION metadata from METS using DIV ID: " + metsDivId);

    // Transform.
    File aggregationTransformation = new File(this.aggregationTransformationFile);
    HashMap<String, String> parameters = new HashMap<String, String>();
    parameters.put("metsdivid", metsDivId);
    String aggregationMetadata = transform(new FileInputStream(this.sourceFile),
        new FileInputStream(aggregationTransformation), parameters);

    // Get aggregation metadata object.
    ObjectType newMetadata =
        JAXB.unmarshal(new StringReader(aggregationMetadata), ObjectType.class);

    // Set format to AGGREGATION.
    newMetadata.getGeneric().getProvided().setFormat(TextGridMimetypes.AGGREGATION);

    // Check if we have got a valid title list in the new metadata, if not, take the old title list.
    List<String> aggregationTitles = newMetadata.getGeneric().getProvided().getTitle();
    if (aggregationTitles.isEmpty() || aggregationTitles.get(0).equals("")) {
      newMetadata.getGeneric().getProvided().getTitle().clear();
      newMetadata.getGeneric().getProvided().getTitle().addAll(oldTitles);
    }

    // Store new metadata object.
    MetadataContainerType newMetadataContainer = new MetadataContainerType();
    newMetadataContainer.setObject(newMetadata);
    JAXB.marshal(newMetadataContainer, theMetadataFile);

    this.step.setStatus(Status.RUNNING, logName + "AGGREGATION metadata merge complete");
  }

  /**
   * @param theOldMetadata
   * @param theFile
   * @throws IOException
   * @throws TransformerException
   * @throws FileNotFoundException
   */
  private void transformCollectionMetadata(File theMetadataFile)
      throws FileNotFoundException, TransformerException, IOException {

    // Get metadata file.
    ObjectType oldMetadata = ImportUtils.getMetadata(theMetadataFile);

    // Get log name and format.
    String logName = "[" + theMetadataFile.getName() + "] ";
    List<String> oldTitles = oldMetadata.getGeneric().getProvided().getTitle();

    this.step.setStatus(Status.RUNNING, logName + "Transforming COLLECTION metadata from METS");

    // Transform.
    File collectionTransformation = new File(this.collectionTransformationFile);
    String collectionMetadata = transform(new FileInputStream(this.sourceFile),
        new FileInputStream(collectionTransformation), NO_PARAMS);

    // Get collection object.
    ObjectType newMetadata = JAXB.unmarshal(new StringReader(collectionMetadata), ObjectType.class);

    // Set format to COLLECTION.
    newMetadata.getGeneric().getProvided().setFormat(TextGridMimetypes.COLLECTION);

    // Check if we have got a valid title list in the new metadata, if not, take the old title list.
    List<String> collectionTitles = newMetadata.getGeneric().getProvided().getTitle();
    if (collectionTitles.isEmpty() || collectionTitles.get(0).equals("")) {
      newMetadata.getGeneric().getProvided().getTitle().clear();
      newMetadata.getGeneric().getProvided().getTitle().addAll(oldTitles);
    } else {
      changeTitleInMetadataFile(collectionTitles);
    }

    // Store new metadata object.
    MetadataContainerType newMetadataContainer = new MetadataContainerType();
    newMetadataContainer.setObject(newMetadata);
    JAXB.marshal(newMetadataContainer, theMetadataFile);

    this.step.setStatus(Status.RUNNING, logName + "COLLECTION metadata merge complete");
  }

  /**
   * @param theFile
   * @throws IOException
   * @throws TransformerException
   * @throws FileNotFoundException
   */
  private void transformEditonAndWorkMetadata(File theEditionFile)
      throws FileNotFoundException, TransformerException, IOException {

    // Get edition and work metadata file.
    File editionMetadataFile = new File(theEditionFile.getAbsolutePath() + this.metadataSuffix);
    ObjectType oldEditionMetadata = ImportUtils.getMetadata(editionMetadataFile);

    File workFile = new File(theEditionFile.getParentFile(),
        this.processData.getProcessName() + this.workSuffix);
    File workMetadataFile = new File(workFile.getAbsolutePath() + this.metadataSuffix);
    ObjectType oldWorkMetadata = ImportUtils.getMetadata(workMetadataFile);

    // Get log name and format.
    String logName = "[" + theEditionFile.getName() + "] ";
    List<String> oldEditionTitles = oldEditionMetadata.getGeneric().getProvided().getTitle();
    List<String> oldWorkTitles = oldWorkMetadata.getGeneric().getProvided().getTitle();

    this.step.setStatus(Status.RUNNING,
        logName + "Transforming EDITION and WORK metadata from METS");

    // Transform edition and work metadata.
    File editionTransformation = new File(this.editionTransformationFile);
    HashMap<String, String> parameters = new HashMap<String, String>();
    parameters.put("tguriwork", workFile.getName());
    String editionMetadata = transform(new FileInputStream(this.sourceFile),
        new FileInputStream(editionTransformation), parameters);

    // Get edition metadata object.
    ObjectType newEditionMetadata =
        JAXB.unmarshal(new StringReader(editionMetadata), ObjectType.class);

    // Set format to EDITION.
    newEditionMetadata.getGeneric().getProvided().setFormat(TextGridMimetypes.EDITION);

    // Check if we have got a valid title list in the new metadata, if not, take the old title list.
    List<String> editionTitles = newEditionMetadata.getGeneric().getProvided().getTitle();
    if (editionTitles.isEmpty() || editionTitles.get(0).equals("")) {
      newEditionMetadata.getGeneric().getProvided().getTitle().clear();
      newEditionMetadata.getGeneric().getProvided().getTitle().addAll(oldEditionTitles);
    } else {
      changeTitleInMetadataFile(editionTitles);
    }

    // Store new edition metadata object.
    MetadataContainerType newEditionMetadataContainer = new MetadataContainerType();
    newEditionMetadataContainer.setObject(newEditionMetadata);
    JAXB.marshal(newEditionMetadataContainer, editionMetadataFile);

    this.step.setStatus(Status.RUNNING, logName + "EDITION metadata merge complete");

    // Get WORK metadata out of the METS.
    File workTransformation = new File(this.workTransformationFile);
    String workMetadata = transform(new FileInputStream(this.sourceFile),
        new FileInputStream(workTransformation), NO_PARAMS);

    // Get work metadata object.
    ObjectType newWorkMetadata = JAXB.unmarshal(new StringReader(workMetadata), ObjectType.class);

    // Set format to WORK.
    newWorkMetadata.getGeneric().getProvided().setFormat(TextGridMimetypes.WORK);

    // Check if we have got a valid first title in the new work metadata, if not, take the old one.
    List<String> workTitles = newWorkMetadata.getGeneric().getProvided().getTitle();
    if (workTitles.isEmpty() || workTitles.get(0).equals("")) {
      newWorkMetadata.getGeneric().getProvided().getTitle().clear();
      newWorkMetadata.getGeneric().getProvided().getTitle().addAll(oldWorkTitles);
    }

    // Store new work metadata object.
    MetadataContainerType newWorkMetadataContainer = new MetadataContainerType();
    newWorkMetadataContainer.setObject(newWorkMetadata);
    JAXB.marshal(newWorkMetadataContainer, workMetadataFile);

    this.step.setStatus(Status.RUNNING, logName + "WORK metadata merge complete");
  }

  /**
   * <p>
   * A text transformation method.
   * </p>
   * 
   * @param theInputXml
   * @param theAdaptor
   * @param theParameters
   * @return
   * @throws TransformerException
   * @throws IOException
   */
  protected static String transform(final InputStream theInputXml, InputStream theAdaptor,
      HashMap<String, String> theParameters) throws TransformerException, IOException {

    // Do prepare transforming.
    OutputStream out = new ByteArrayOutputStream();
    TransformerFactory tfactory = TransformerFactory.newInstance();
    final Transformer transformer = tfactory.newTransformer(new StreamSource(theAdaptor));

    // Set parameters.
    for (String p : theParameters.keySet()) {
      transformer.setParameter(p, theParameters.get(p));
    }

    // Now do transform.
    transformer.transform(new StreamSource(theInputXml), new StreamResult(out));

    // Return the transformed string.
    return out.toString();
  }

  /**
   * @param theTitles
   */
  protected void changeTitleInMetadataFile(List<String> theTitles) {

    // Get METS metadata file.
    File metsMetadataFile = new File(this.sourceFile.getAbsolutePath() + this.metadataSuffix);
    ObjectType metsMetadata = ImportUtils.getMetadata(metsMetadataFile);

    // Get log name and format.
    String logName = "[" + metsMetadataFile.getName() + "] ";

    // Change titles.
    metsMetadata.getGeneric().getProvided().getTitle().clear();
    metsMetadata.getGeneric().getProvided().getTitle().addAll(theTitles);

    // Store new METS metadata object.
    MetadataContainerType newMetadataContainer = new MetadataContainerType();
    newMetadataContainer.setObject(metsMetadata);
    JAXB.marshal(newMetadataContainer, metsMetadataFile);

    this.step.setStatus(Status.RUNNING, logName + "metadata file title adaption complete");
  }

  /**
   * @param theOldMetadata
   * @param theNewMetadata
   * @param theLogName
   */
  protected void checkFormat(ObjectType theOldMetadata, ObjectType theNewMetadata,
      String theLogName) {

    // Take the old metadata's format (extracted from TextGridMetadataProcessor) if there is no
    // format given in the METS file or extraction is forced in class configuration.
    String oldFormat = theOldMetadata.getGeneric().getProvided().getFormat();
    String newFormat = theNewMetadata.getGeneric().getProvided().getFormat();
    if (newFormat == null || newFormat.equals("")) {
      theNewMetadata.getGeneric().getProvided().setFormat(oldFormat);
    } else if (this.forceMimetypeExtraction) {
      this.step.setStatus(Status.RUNNING,
          theLogName + "Omitting mimetype " + newFormat + " from METS file, using " + oldFormat);
      theNewMetadata.getGeneric().getProvided().setFormat(oldFormat);
    }
    // Take the format from the METS file only if it is valid.
    else {
      // TODO Only syntactic validity is checked here! Mimetype "image/jpg" will be valid (and not
      // be recognised in the TG-lab, because it must correctly be "image/jpeg")! Please use
      // forceMimetypeExtraction trigger here!
      try {
        new MimeType(newFormat);
      } catch (MimeTypeParseException e) {
        this.step.setStatus(Status.RUNNING, theLogName + "Given mimetype " + newFormat
            + " from METS file is invalid, using " + oldFormat + " instead");
        theNewMetadata.getGeneric().getProvided().setFormat(oldFormat);
      }
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param processData The processData to set.
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   * @param step The step to set.
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param metadataSuffix
   */
  public void setMetadataSuffix(String metadataSuffix) {
    this.metadataSuffix = metadataSuffix;
  }

  /**
   * @param aggregationSuffix
   */
  public void setAggregationSuffix(String aggregationSuffix) {
    this.aggregationSuffix = aggregationSuffix;
  }

  /**
   * @param workSuffix
   */
  public void setWorkSuffix(String workSuffix) {
    this.workSuffix = workSuffix;
  }

  /**
   * @param transformationFile
   */
  public void setItemTransformationFile(String transformationFile) {
    this.itemTransformationFile = transformationFile;
  }

  /**
   * @param transformationFile
   */
  public void setEditionTransformationFile(String transformationFile) {
    this.editionTransformationFile = transformationFile;
  }

  /**
   * @param transformationFile
   */
  public void setCollectionTransformationFile(String transformationFile) {
    this.collectionTransformationFile = transformationFile;
  }

  /**
   * @param transformationFile
   */
  public void setWorkTransformationFile(String transformationFile) {
    this.workTransformationFile = transformationFile;
  }

  /**
   * @param transformationFile
   */
  public void setAggregationTransformationFile(String transformationFile) {
    this.aggregationTransformationFile = transformationFile;
  }

  /**
   * @param rootAggregationMimetype
   */
  public void setRootAggregationMimetype(String rootAggregationMimetype) {
    this.rootAggregationMimetype = rootAggregationMimetype;
  }

  /**
   * @param forceMimetypeExtraction
   */
  public void setForceMimetypeExtraction(boolean forceMimetypeExtraction) {
    this.forceMimetypeExtraction = forceMimetypeExtraction;
  }

}
