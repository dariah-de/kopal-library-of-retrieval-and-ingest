/*******************************************************************************
 * This software is copyright (c) 2015 by
 * 
 * - TextGrid Consortium (http://www.textgrid.de) - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Wolfgang Pempe (pempe@saphor.de)
 ******************************************************************************/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import gov.loc.mets.DivType;
import gov.loc.mets.FileType;
import gov.loc.mets.MdSecType.MdWrap.XmlData;
import gov.loc.mets.MetsDocument;
import gov.loc.mets.MetsType;
import gov.loc.mets.MetsType.FileSec.FileGrp;
import gov.loc.mets.StructMapType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Pid;
import info.textgrid.namespaces.metadata.core._2010.IdentifierType;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import jakarta.xml.bind.JAXB;

/*******************************************************************************
 * CHANGELOG
 * 
 * 2015-09-22 Funk Added dv:reference rewriting with PID or URI. Funk METS' file pretty print now
 * can be configured. 2012-08-07 Funk Added METS file's PID to METS' OBJID attribute. 2012-08-03
 * Funk Added PIDs to CONTENTIDS. 2012-06-08 Funk First version.
 * 
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * Rewrites the file URLs in the DFG-Viewer METS file with the appropriate Handle/TextGrid URI
 * resolver and Handle PID/TextGrid URI. Adds the METS file's PID as OBJID to the METS, furthermore
 * adds the PIDs (if existing) to each struct map's DIV as CONTENTIDS attribute.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2015-09-22
 * @since 2012-06-08
 ******************************************************************************/

public class RewriteDfgViewerUrls implements ActionModule {

  // **
  // FINALS
  // **

  private static final String URI_SEPARATION_CHAR = "_";
  private static final String ROOT_ID = "ROOT";
  private static final String ROOT_SEPARATOR = "__";

  // **
  // STATE
  // **

  private ProcessData processData;
  private Step step;
  private MetsType mets;
  private File metsFile;
  private String uriPrefix = "textgrid";
  private String metadataSuffix = ".meta";
  private String resolverPrefix = "http://textgridrep.org/";
  private boolean rewriteToPids = false;
  private String metadataIdType = "ID";
  private boolean rewriteDvReference = false;
  private boolean prettyPrint = true;

  /**
   * <p>
   * Rewrites the file's URLs to resolvable URLs with Handle PIDs/TextGrid URIs.
   * </p>
   * 
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>Metadata files containing the PID and/or the TextGrid URI.</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>A METS file with resolvable Handle PIDs or TextGrid URIs including resolver URL.</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Set start time.
    long startTime = System.currentTimeMillis();

    // Start and set status to RUNNING.
    this.step.setStatus(Status.RUNNING,
        "DFG Viewer METS file's item URLs are beeing rewritten");

    // Get import folder.
    File importFolder = new File(this.processData.getPathToContentFiles()
        + File.separatorChar + ROOT_ID + ROOT_SEPARATOR
        + this.processData.getProcessName());

    File importFiles[] = importFolder.listFiles();
    if (importFiles == null) {
      this.step.setStatus(Status.ERROR,
          "File list " + importFolder.getAbsolutePath()
              + " is null or empty or both!");
      return;
    }

    // Get mets file from filename (assuming METS file ends with process
    // name, which always should).
    for (File f : importFiles) {
      if (f.getName().endsWith(this.processData.getProcessName())) {
        this.metsFile = f;
      }
    }

    // Read the METS file. Using the METS 1.4 with XMLBeans, koLibRI has got
    // METS 1.4 embedded I think, but to use METS 1.7 would be not a big
    // problem, I think... and maybe it works anyway :-)
    try {
      this.step.setStatus(Status.RUNNING, "Processing as METS file: "
          + this.metsFile.getAbsolutePath());

      this.mets = ((MetsDocument) XmlObject.Factory.parse(this.metsFile)).getMets();

    } catch (XmlException e) {
      this.step.setStatus(Status.ERROR, e.getClass().getSimpleName()
          + ": " + e.getMessage());
      e.printStackTrace();
      return;
    } catch (IOException e) {
      this.step.setStatus(Status.ERROR, e.getClass().getSimpleName()
          + ": " + e.getMessage());
      e.printStackTrace();
      return;
    }

    // Read METS file's metadata and set PID as METS OBJID.
    // TODO Generalise due to duplicate code!
    MetadataContainerType metsMetadata = JAXB.unmarshal(new File(
        this.metsFile.getAbsolutePath() + this.metadataSuffix),
        MetadataContainerType.class);
    GeneratedType generated = metsMetadata.getObject().getGeneric()
        .getGenerated();
    if (generated != null) {
      List<Pid> pidList = generated.getPid();

      if (pidList != null && pidList.size() > 0) {
        // TODO Be sure to use our TextGrid Handle PIDs! We just take
        // the first PID at all here at the moment!
        // NOTE We must NOT encode the PID here because we have NO URL!
        this.mets.setOBJID(pidList.get(0).getValue());
      }
    }

    // Go through all files recursively and get textgrid URIs.
    rewriteURLs(importFolder);

    this.step.setStatus(Status.RUNNING, "CONTENTIDs are being created");

    // Create CONTENTIDS.

    createContentIDs(importFolder);

    // Store METS file.
    try {
      XmlOptions savingOpts = new XmlOptions();
      savingOpts.setSaveAggressiveNamespaces();
      if (this.prettyPrint) {
        savingOpts.setSavePrettyPrint();
      }
      savingOpts.setSaveOuter();

      this.mets.save(this.metsFile, savingOpts);

      this.step.setStatus(Status.RUNNING, "Rewriting METS file");

    } catch (IOException e) {
      this.step.setStatus(Status.ERROR, e.getClass().getSimpleName()
          + ": " + e.getMessage());
      e.printStackTrace();
      return;
    }

    // Log duration and set status to DONE.
    String duration = KolibriTimestamp.getDurationWithMillis(System
        .currentTimeMillis() - startTime);
    this.step.setStatus(Status.DONE, "METS file's item URLs rewritten in "
        + duration);
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Exchanges all file IDs (extracted from the object's filename) with the TextGrud URI or PID.
   * <p/>
   * 
   * @param theFile
   */
  private void rewriteURLs(File theFile) {

    File files[] = theFile.listFiles();
    if (files == null) {
      return;
    }

    // Do loop!
    for (File f : files) {
      if (f.isDirectory()) {
        rewriteURLs(f);
      }

      // Get URI from non-metadata files.
      else if (f.isFile() && !f.getName().endsWith(this.metadataSuffix)) {

        // Get URI and FILEID from filename.
        String urlSuffix = this.uriPrefix
            + ":"
            + f.getName().substring(0,
                f.getName().indexOf(URI_SEPARATION_CHAR));
        String fileId = f.getName().substring(
            f.getName().indexOf(URI_SEPARATION_CHAR) + 1);

        try {
          // Handle PIDs (or even better: Handle Handles! :-)

          // FIXME Fix NPE in
          // metadata.getObject().getGeneric().getGenerated().getPid();
          // if getPids is set to FALSE and rewriteToPids is set to
          // TRUE!!

          if (this.rewriteToPids) {
            // Get metadata container from metadata file.
            MetadataContainerType metadata = JAXB.unmarshal(
                new File(f.getAbsolutePath()
                    + this.metadataSuffix),
                MetadataContainerType.class);

            // Read metadata and extract PID if so configured.
            // NOTE The file ID is eventually contained in the
            // metadata, too. But we do not want to trust the XSLT
            // metadata scripts here, because they may depend on the
            // user!
            // TODO Generalise due to duplicate code!
            List<Pid> pidList = metadata.getObject().getGeneric()
                .getGenerated().getPid();
            if (pidList != null && pidList.size() > 0) {
              // TODO Be sure to use our TextGrid Handle PIDs! We
              // just take the first PID at all here at the
              // moment!
              // NOTE We must encode the PID because there is a
              // "/" in it!!! ARGL!!!
              urlSuffix = URLEncoder.encode(pidList.get(0)
                  .getValue(), "UTF-8");
            }
          }

          // Assemble URL.
          URL url = new URL(this.resolverPrefix + urlSuffix);

          // Rewrite dv:referece to PID or URI, if configured so.
          if (this.rewriteDvReference
              && f.getName().contains(ROOT_ID)) {

            this.step.setStatus(Status.RUNNING,
                "Rewriting dv:reference with URL: " + url);

            XmlData xmlData = this.mets.getAmdSecList().get(0)
                .getDigiprovMDList().get(0).getMdWrap()
                .getXmlData();
            XmlCursor cursor = xmlData.newCursor();
            cursor.toFirstChild();
            cursor.toFirstChild();
            cursor.setTextValue(url.toString());
          }

          // Get file via file ID and put TextGrid URI into file
          // groups, if fileId existing.
          FileType file = getFileByFILEID(fileId);

          if (file != null) {
            file.getFLocatList().get(0).setHref(url.toString());

            this.step.setStatus(Status.RUNNING, "[" + fileId
                + "] URL rewritten to: " + url);
          }

        } catch (MalformedURLException e) {
          this.step.setStatus(Status.WARNING, "[" + fileId
              + "] Malformed URL: " + this.resolverPrefix
              + urlSuffix);
        } catch (UnsupportedEncodingException e) {
          this.step.setStatus(Status.WARNING, "[" + fileId
              + "] This failure will NEVER be occuring!");
        }
      }
    }
  }

  /**
   * <p>
   * Adds for every METS XML ID that is an attribute of a DIV tag the CONTENTIDS attribute
   * containing the TextGrid Handle PID (if existing).
   * <p/>
   * 
   * @param theFile
   */
  private void createContentIDs(File theFile) {

    File files[] = theFile.listFiles();
    if (files == null) {
      return;
    }

    // Loop files.
    for (File f : files) {

      if (f.isDirectory()) {
        createContentIDs(f);
      }

      // Look into every metadata file.
      if (f.getName().endsWith(this.metadataSuffix)) {

        // Get metadata container from metadata file.
        MetadataContainerType metadata = JAXB.unmarshal(
            new File(f.getAbsolutePath()),
            MetadataContainerType.class);

        // Get ID attribute from the metadata (at the moment that is
        // (object:generic:provided:identifier type="ID").
        // TODO Make that configurable as it is in the XSLT sheets!
        List<IdentifierType> metsXmlIdList = metadata.getObject()
            .getGeneric().getProvided().getIdentifier();
        // ID attribute found, get ID and create CONTENTIDS attribute.
        if (metsXmlIdList != null && !metsXmlIdList.isEmpty()) {
          for (int i = 0; i < metsXmlIdList.size(); i++) {
            String metsXmlIdType = metsXmlIdList.get(i).getType();
            String metsXmlIdValue = metsXmlIdList.get(i).getValue();

            if (metsXmlIdType != null && !metsXmlIdType.equals("")
                && metsXmlIdType.equals(this.metadataIdType)) {
              // Read metadata and extract PID if so configured.
              // NOTE The file ID is eventually contained in the
              // metadata, too. But we do not want to trust the
              // XSLT metadata scripts here, because they may
              // depend on the user!
              // TODO Generalise due to duplicate code!
              GeneratedType generated = metadata.getObject()
                  .getGeneric().getGenerated();
              if (generated != null) {
                List<Pid> pidList = generated.getPid();

                if (pidList != null && pidList.size() > 0) {
                  // TODO Be sure to use our TextGrid Handle
                  // PIDs! We just take the first PID at all
                  // here at the moment!
                  // NOTE We must NOT encode the PID here
                  // because we have NO URL!
                  String pid = pidList.get(0).getValue();
                  List<String> contentIdList = new ArrayList<String>();
                  contentIdList.add(pid);

                  // Get DIV via METS XML ID and put TextGrid
                  // URI into the CONTENTIDS.
                  for (StructMapType s : this.mets
                      .getStructMapList()) {
                    DivType div = getDivByDIVID(s.getDiv(),
                        metsXmlIdValue);
                    if (div != null) {
                      div.setCONTENTIDS(contentIdList);
                      this.step
                          .setStatus(
                              Status.RUNNING,
                              "["
                                  + metsXmlIdValue
                                  + "] Added to CONTENTIDS: "
                                  + pid);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * @param theFileId
   * @return
   */
  private FileType getFileByFILEID(String theFileId) {

    FileType result = null;

    if (this.mets.getFileSec().getFileGrpList().size() > 0) {
      for (FileGrp g : this.mets.getFileSec().getFileGrpList()) {
        if (g.getFileList().size() > 0) {
          for (FileType f : g.getFileList()) {
            if (f.getID().equals(theFileId)) {
              result = f;
            }
          }
        }
      }
    }

    return result;
  }

  /**
   * @param theFileId
   * @return
   */
  private DivType getDivByDIVID(DivType theDiv, String theDivId) {

    DivType result = null;

    if (theDiv.getID().equals(theDivId)) {
      result = theDiv;
    } else {
      for (DivType d : theDiv.getDivList()) {
        if (result == null) {
          result = getDivByDIVID(d, theDivId);
        }
      }
    }

    return result;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param processData Please add a ProcessData
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   * @param step Please add a Step
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param metadataSuffix Please add a metadata suffix
   */
  public void setMetadataSuffix(String metadataSuffix) {
    this.metadataSuffix = metadataSuffix;
  }

  /**
   * @param resolverPrefix Please add a resolver prefix
   */
  public void setResolverPrefix(String resolverPrefix) {
    this.resolverPrefix = resolverPrefix;
  }

  /**
   * @param rewriteToPids Please add true or false
   */
  public void setRewriteToPids(boolean rewriteToPids) {
    this.rewriteToPids = rewriteToPids;
  }

  /**
   * @param uriPrefix Please add a URI prefix
   */
  public void setUriPrefix(String uriPrefix) {
    this.uriPrefix = uriPrefix;
  }

  /**
   * @param metadataIdType Please add a metadata ID type
   */
  public void setMetadataIdType(String metadataIdType) {
    this.metadataIdType = metadataIdType;
  }

  /**
   * @param prettyPrint Please add true or false
   */
  public void setPrettyPrint(boolean prettyPrint) {
    this.prettyPrint = prettyPrint;
  }

  /**
   * @param rewriteDvReference Please add a rewrite DV reference
   */
  public void setRewriteDvReference(boolean rewriteDvReference) {
    this.rewriteDvReference = rewriteDvReference;
  }

}
