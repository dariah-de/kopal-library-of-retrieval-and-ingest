/**
 * This software is copyright (c) 2021 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2021-03-25 - Funk - First version.
 */

/**
 * <p>
 * Action module that just checks some mandatory config values.
 * </p>
 * 
 * @author Stefan Funk, SUB Göttingen
 * @version 2021-03-25
 * @since 2021-03-25
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 */

public class ConfigCheck implements ActionModule {

  // **
  // STATE
  // **

  private ProcessData processData;
  private Step step;
  private String rbacSessionId;
  private String logParameter;
  private String projectId;

  /*
   * (non-Javadoc)
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  public void go() {

    // Set start time.
    long startTime = System.currentTimeMillis();
    int errors = 0;

    this.step.setStatus(Status.RUNNING, "Checking configuration...");

    // **
    // ERRORS
    // **

    // Check for existing RBAC session ID.
    if (this.rbacSessionId == null || "".equals(this.rbacSessionId)) {
      this.step.setStatus(Status.ERROR,
          "No RBAC session ID value existing for field rbacSessionId in configuration file!");
      errors += 1;
    } else {
      this.step.setStatus(Status.RUNNING, "RBAC Session ID check... passed");
    }

    // Check for existing project ID.
    if (this.projectId == null || "".equals(this.projectId)) {
      this.step.setStatus(Status.ERROR,
          "No project ID value existing for field projectId in configuration file!");
      errors += 1;
    } else {
      this.step.setStatus(Status.RUNNING, "Project ID check... passed");
    }

    // **
    // WARNINGS
    // **

    // Check for existing log parameter.
    if (this.logParameter == null || "".equals(this.logParameter)) {
      this.step.setStatus(Status.WARNING,
          "No log parameter value existing for field logParameter in configuration file!");
    }

    // Log duration and set status to DONE.
    String duration =
        KolibriTimestamp.getDurationWithMillis(System.currentTimeMillis() - startTime);
    if (errors > 0) {
      this.step.setStatus(Status.ERROR, "Config sucessfully checked in " + duration + ", we found "
          + errors + " erros" + (errors != 1 ? "s" : "")
          + ". Please check your configuration file!");
    } else {
      this.step.setStatus(Status.DONE, "Config sucessfully checked in " + duration);
    }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param processData Please add a process data
   */
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   * @param step Please add a step
   */
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param rbacSessionId Please add a RBAC session ID
   */
  public void setRbacSessionId(String rbacSessionId) {
    this.rbacSessionId = rbacSessionId;
  }

  /**
   * @param projectId Please add a project ID
   */
  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  /**
   * @param logParameter Please add a log parameter
   */
  public void setLogParameter(String logParameter) {
    this.logParameter = logParameter;
  }

}
