/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 **/

package de.langzeitarchivierung.kolibri.actionmodule.textgrid;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;
import de.langzeitarchivierung.kolibri.ProcessData;
import de.langzeitarchivierung.kolibri.Status;
import de.langzeitarchivierung.kolibri.Step;
import de.langzeitarchivierung.kolibri.actionmodule.ActionModule;
import de.langzeitarchivierung.kolibri.textgrid.util.ImportUtils;
import de.langzeitarchivierung.kolibri.util.KolibriTimestamp;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;

/**
 * TODOLOG
 * 
 * TODO Gather this classes methods with submitFiles, make abstract or so! Maybe see
 * tgpublish-service?
 * 
 **
 * CHANGELOG
 * 
 * 2021-03-29 - Funk - Always validate, add warning only if validation is set to false.
 * 
 * 2020-05-15 - Funk - Add metadata file validation.
 * 
 * 2020-02-13 - Funk - Copied from SubmitFilesAbs.
 **/

/**
 * <p>
 * Action module that publish checks all files.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule
 **/

public class PublishCheck implements ActionModule {

  // **
  // FINALS
  // **

  private static final String URI_SEPARATION_CHAR = "_";
  private static final String GENERATED_METADATA_FAILURE_MESSAGE =
      "Invalid content was found starting with element '{\"http://textgrid.info/namespaces/metadata/core/2010\":revision}";

  // **
  // STATE (Instance variables)
  // **

  private ProcessData processData;
  private Step step;

  private String metadataSuffix = ".meta";
  private String uriPrefix = "textgrid";
  private int files = 0;
  private int checked = 0;
  private int missing = 0;
  private int invalid = 0;

  private boolean validateMetadata = true;
  private List<String> editionRequiredFields = new ArrayList<String>();
  private List<String> collectionRequiredFields = new ArrayList<String>();
  private List<String> itemRequiredFields = new ArrayList<String>();
  private List<String> workRequiredFields = new ArrayList<String>();
  private Map<String, String> xpathNamespaces = new HashMap<String, String>();

  /**
   * <p>
   * <b>NEEDS</b>
   * <ul>
   * <li>The pathToContentFiles to be set.</li>
   * <li>The import folder filled with files (and folders), one metadata file for each data file is
   * expected.</li>
   * </ul>
   * </p>
   * 
   * <p>
   * <b>GIVES</b>
   * <ul>
   * <li>Checks every file for mandatory metadata.</li>
   * </ul>
   * </p>
   * 
   * @see de.langzeitarchivierung.kolibri.actionmodule.ActionModule#go()
   */
  @Override
  public void go() {

    // Set start time.
    long startTime = System.currentTimeMillis();

    // Get import folder.
    File importFolder = new File(this.processData.getPathToContentFiles());

    this.step.setStatus(Status.RUNNING,
        "Checking metadata for all files in: " + importFolder.getAbsolutePath());

    if (!this.validateMetadata) {
      this.step.setStatus(Status.WARNING, "Validating TextGrid metadata is DISABLED!");
    }

    // Count TextGrid objects.
    countFiles(importFolder);

    // FIXME Look for Edition or Collection on root level.
    // if (!checkForRootEditionOrCollection(importFolder)) {
    // this.step.setStatus(Status.ERROR,"You have no Edition or Collection on root level. Only
    // Editions or Collections can be published.");
    // this.missing += 1;
    // }

    // Check all files, loop recursively.
    try {
      checkFiles(importFolder);

    } catch (IOException | InterruptedException | XPathExpressionException | XMLStreamException
        | SAXException | DatatypeConfigurationException | ParserConfigurationException
        | ExecutionException | JAXBException e) {
      e.printStackTrace();
      this.step.setStatus(Status.ERROR, "System message: " + e.getMessage());
      return;
    }

    // Log duration.
    String duration = KolibriTimestamp.getDurationHMSOnly(System.currentTimeMillis()
        - startTime);
    this.step.setStatus(Status.RUNNING, "All " + this.files + " file "
        + (this.files != 1 ? "s" : "") + " have been checked in " + duration);

    // Output missing metadata and go to error state if any missing or invalid.
    if (this.invalid != 0) {
      this.step.setStatus(Status.ERROR,
          "There have been " + this.invalid + " invalid TextGrid metadata file"
              + (this.invalid != 1 ? "s" : "") + " detected, please correct them and try again!");
    } else if (this.missing != 0) {
      this.step.setStatus(Status.ERROR, "Missing mandatory metadata for " + this.missing + " file"
          + (this.missing != 1 ? "s" : "")
          + " detected! Please check your metadata generation routines and/or your metadata template file!");
    } else {
      this.step.setStatus(Status.DONE, "No invalid or missing metadata was detected");
    }
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * Check files.
   * </p>
   * 
   * @param theFile
   * @throws IOException
   * @throws XPathExpressionException
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws DatatypeConfigurationException
   * @throws XMLStreamException
   * @throws InterruptedException
   * @throws ExecutionException
   * @throws JAXBException
   */
  private void checkFiles(File theFile) throws IOException, XPathExpressionException,
      SAXException, ParserConfigurationException, DatatypeConfigurationException,
      XMLStreamException, InterruptedException, ExecutionException, JAXBException {

    File files[] = theFile.listFiles();
    if (files == null) {
      return;
    }

    // Loop all files.
    for (File f : files) {

      // Start recursion if directory is found.
      if (f.isDirectory()) {
        checkFiles(f);
      }

      // Check metadata files only.
      else if (f.isFile() && f.getName().endsWith(this.metadataSuffix)) {

        // Get metadata file.
        File metadataFile = new File(f.getParentFile(), f.getName());

        // Validate metadata file first.
        try {
          this.checked++;

          this.step.setStatus(Status.RUNNING, "[" + f.getName() + "] validating...");

          ImportUtils.validateMetadataFile(f);

          this.step.setStatus(Status.RUNNING, "[" + f.getName() + "] ...complete and valid");

        } catch (JAXBException | SAXException e) {
          if (this.validateMetadata) {
            // Ignore validation failures concerning incomplete <generated> element.
            if (e.getCause().getMessage().contains(GENERATED_METADATA_FAILURE_MESSAGE)) {
              this.step.setStatus(Status.WARNING,
                  "[" + f.getName() + "] ...ignoring invalid <generated> element in file ["
                      + f.getCanonicalPath() + "]: " + e.getCause().getMessage());
            } else {
              this.step.setStatus(Status.ERROR,
                  "[" + f.getName() + "] ...validation FAILED for file [" + f.getCanonicalPath()
                      + "]: " + e.getCause().getMessage());
              this.invalid++;
              continue;
            }
          } else {
            this.step.setStatus(Status.WARNING,
                "[" + f.getName() + "] ...validation warning for file [" + f.getCanonicalPath()
                    + "]: " + e.getCause().getMessage());
          }
        }

        // Get metadata.
        ObjectType metadata = ImportUtils.getMetadata(metadataFile);

        if (!f.getName().contains(URI_SEPARATION_CHAR)) {
          throw new IOException(
              "File was possibly not created by koLibRI (prefix URI missing in filename): "
                  + f.getName() + ", maybe you mixed up policies?");
        }

        // Assemble and set TextGrid URI.
        String uri = this.uriPrefix + ":"
            + f.getName().substring(0, f.getName().indexOf(URI_SEPARATION_CHAR));

        List<String> missingMetadata = checkMetadataForSingleFile(metadata, uri);

        if (missingMetadata.isEmpty()) {
          this.step.setStatus(Status.RUNNING,
              "[" + uri + "] -- " + this.checked + "/" + this.files + " -- Check complete");
        } else {
          this.missing++;

          this.step.setStatus(Status.ERROR, "[" + uri + "] -- " + this.checked + "/" + this.files
              + " -- Check FAILED for file [" + f.getCanonicalPath() + "]");
        }
      }
    }

  }

  /**
   * @param theMetadata
   * @param theURI
   * @return
   * @throws IOException
   * @throws XPathExpressionException
   * @throws JAXBException
   * @throws ParserConfigurationException
   */
  private List<String> checkMetadataForSingleFile(ObjectType theMetadata, final String theURI)
      throws IOException, XPathExpressionException, JAXBException, ParserConfigurationException {

    List<String> result = new ArrayList<String>();

    // Get format from format tags.
    String format = theMetadata.getGeneric().getProvided().getFormat();
    if (format == null || format.equals("")) {
      throw new IOException("[" + theURI + "] Format must be set!");
    }

    this.step.setStatus(Status.RUNNING, "[" + theURI + "] Mimetype " + format + " found!");

    // Test object metadata for missing mandatory metadata (EDITION).
    if (format.equals(TextGridMimetypes.EDITION)) {
      // TODO Maybe implement Work check here!
      // Get work URI from isEditionOf field, handle work, we already checked if we have one.
      // String workUri = theMetadata.getObject().getEdition().getIsEditionOf();
      // if (workUri != null && !"".equals(workUri)) {
      // checkWork(workUri);
      // }
      result = ImportUtils.checkRequiredMetadata(this.editionRequiredFields, theMetadata,
          this.xpathNamespaces, this.step, theURI);
    }
    // Test for missing mandatory metadata (WORK).
    else if (format.equals(TextGridMimetypes.WORK)) {
      result = ImportUtils.checkRequiredMetadata(this.workRequiredFields, theMetadata,
          this.xpathNamespaces, this.step, theURI);
    }
    // Test for missing mandatory metadata (COLLECTION).
    else if (format.equals(TextGridMimetypes.COLLECTION)) {
      result = ImportUtils.checkRequiredMetadata(this.collectionRequiredFields, theMetadata,
          this.xpathNamespaces, this.step, theURI);
    }
    // Item check for ITEMS and AGGREGATIONS only (they do have an <item> tag).
    else if (!format.equals(TextGridMimetypes.EDITION)
        && !format.equals(TextGridMimetypes.COLLECTION) && !format.equals(TextGridMimetypes.WORK)) {
      result = ImportUtils.checkRequiredMetadata(this.itemRequiredFields, theMetadata,
          this.xpathNamespaces, this.step, theURI);
    }

    return result;
  }

  /**
   * <p>
   * Count TextGrid objects.
   * </p>
   * 
   * @param theFile
   */
  private void countFiles(File theFile) {

    File files[] = theFile.listFiles();
    if (files == null) {
      return;
    }

    for (File f : files) {
      // Count files.
      if (f.isFile() && !f.getName().endsWith(this.metadataSuffix)) {
        this.files++;
      }
      // Count recursively.
      else if (f.isDirectory()) {
        countFiles(f);
      }
    }
  }

  /**
   * <p>
   * Check for Edition or Collection on root level. At least one of Edition or Collection must be
   * existing for import!
   * </p>
   * 
   * TODO What exactly are the requirements for root editions or root collections? I am not really
   * sure for TG-import here!
   * 
   * @param theImportFolder
   */
  private boolean checkForRootEditionOrCollection(final File theImportFolder) {

    boolean result = false;

    for (File f : theImportFolder.listFiles()) {
      String filename = f.getName();

      // Check for edition or collection file suffix.
      if (f.isFile() && (filename.endsWith(".edition") || filename.endsWith(".collection"))) {

        // Open file's metadata and check for format.
        File metadataFile = new File(filename + this.metadataSuffix);
        MetadataContainerType metadata = JAXB.unmarshal(metadataFile, MetadataContainerType.class);
        String format = metadata.getObject().getGeneric().getProvided().getFormat();
        if (format.equals(TextGridMimetypes.EDITION)
            || format.equals(TextGridMimetypes.COLLATION_EQUIV)) {
          result = true;
        }

      }
    }

    return result;
  }

  /**
   * TODO We do check works as it here, not from edition/isEditionOf information! Would be better to
   * also check the work <--> edition/isEditionOf relation!
   * 
   * @param workUri
   */
  protected void checkWork(final String workUri) {

    // FIXME
    // // Get TextGrid object of the work.
    // ResultType workResult = this.tgsearch.getMetadata(workUri).getResult().get(0);
    //
    // // Get work revision URI! For we cannot publish a work that has a base URI in the Edition
    // // metadata.
    // String workRevisionUri = workResult.getTextgridUri();
    //
    // // Add work to publishObjects. API is checking, if work already is included.
    // PublishObject workObject = new PublishObject(workRevisionUri);
    // this.response.addPublishObject(workObject);
    //
    // // Check if already published, then no check is needed.
    // if (alreadyPublished(workRevisionUri)) {
    // PublishUtils.logStatusRunning(this.step,
    // "[" + workRevisionUri + "] Work is already published, skipping metadata check",
    // this.dryRun);
    // } else {
    // // Get work type.
    // String workFormat = workResult.getObject().getGeneric().getProvided().getFormat();
    //
    // // Check if all work required metadata fields are set.
    // List<String> workMissing =
    // checkRequiredMetadata(this.workRequiredFields, workResult.getObject());
    //
    // if (workMissing.size() > 0) {
    // for (String miss : workMissing) {
    // setErrorWithoutLogging(workObject, ErrorType.MISSING_METADATA,
    // "[" + workRevisionUri + "] XPath: " + miss);
    // }
    // } else if (this.editionRequiredFields.size() == 0) {
    // PublishUtils.logStatusRunning(this.step,
    // "[" + workRevisionUri + "] No XPathes configured for type work", this.dryRun);
    // }
    //
    // // Check, is this really IS a work and if required work metadata is existing.
    // if (workFormat.equals(TextGridMimetypes.WORK)) {
    // PublishUtils.logStatusRunning(this.step,
    // "[" + workRevisionUri + "] Type of work checked and approved", this.dryRun);
    // } else {
    // setError(workObject, ErrorType.MISSING_METADATA, "[" + workRevisionUri
    // + "] Type of work must be " + TextGridMimetypes.WORK + " but is " + workFormat);
    // }
    // }
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param processData The processData to set.
   */
  @Override
  public void setProcessData(ProcessData processData) {
    this.processData = processData;
  }

  /**
   * @param step The step to set.
   */
  @Override
  public void setStep(Step step) {
    this.step = step;
  }

  /**
   * @param metadataSuffix
   */
  public void setMetadataSuffix(String metadataSuffix) {
    this.metadataSuffix = metadataSuffix;
  }

  /**
   * @param uriPrefix
   */
  public void setUriPrefix(String uriPrefix) {
    this.uriPrefix = uriPrefix;
  }

  /**
   * @param xpathExpression
   */
  public void setEditionRequiredFields(String xpathExpression) {
    if (!this.editionRequiredFields.contains(xpathExpression)) {
      this.editionRequiredFields.add(xpathExpression);
    }
  }

  /**
   * @param xpathExpression
   */
  public void setCollectionRequiredFields(String xpathExpression) {
    if (!this.collectionRequiredFields.contains(xpathExpression)) {
      this.collectionRequiredFields.add(xpathExpression);
    }
  }

  /**
   * @param xpathExpression
   */
  public void setItemRequiredFields(String xpathExpression) {
    if (!this.itemRequiredFields.contains(xpathExpression)) {
      this.itemRequiredFields.add(xpathExpression);
    }
  }

  /**
   * @param xpathExpression
   */
  public void setWorkRequiredFields(String xpathExpression) {
    if (!this.workRequiredFields.contains(xpathExpression)) {
      this.workRequiredFields.add(xpathExpression);
    }
  }

  /**
   * @param namespace
   */
  public void setXpathNamespaces(String namespace) {
    String[] np = namespace.split("=");
    this.xpathNamespaces.put(np[0], np[1]);
  }

  /**
   * @param validateMetadata
   */
  public void setValidateMetadata(boolean validateMetadata) {
    this.validateMetadata = validateMetadata;
  }
}
