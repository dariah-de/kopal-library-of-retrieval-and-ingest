.. tgimport documentation master file, created by
   sphinx-quickstart on Thu May 21 16:32:23 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

TextGrid Import
===============


Overview
--------

The TG-import tool can be used to import data either into the **TextGridLab**, or into the **TextGridRep** directly.

	**NOTE**: Before you do import data via the Import Tool External, please check out the `TG-lab import tool <https://doc.textgrid.de/Using-the-Import-Tool/>`_!

There are different policies to import data:

* You can import data just by **copying your files and folders into a hotfolder**, nothing TextGrid specific has to be done with your data: The needed TextGrid metadata files are created automatically and your folder structure is taken over by using TextGrid aggregations.

* You can provide a **DFG-Viewer METS file**: All linked files will be downloaded and the structure of the two StructMaps (logical and physical) will be imported as TextGrid aggregations.

* If you are able to provide TextGrid metadata files as well, you can use the **complete import** policy. No metadata files will be created automatically. This requires knowledge of the `TextGrid Metadata XML Schema <https://textgrid.info/namespaces/metadata/core/2010>`_ and the concept of `TextGrid Objects <https://doc.textgrid.de/TextGrid-Objects/>`_.

All three policies can be used to import data into the TG-lab or the TG-rep, you just have to configure the appropriate service endpoints. Importing into the TG-lab means that your data is visible only to you (and the people you want to see or edit it), and you can edit and work with it within a TextGrid project in the TG-lab, and publish it later on using the `TG-publish GUI <https://doc.textgrid.de/Publish/>`_. If you do import into the TG-rep, your data is first published into the `TextGrid Sandbox <https://sandbox.textgridrep.org/>`_ to be checked, and after checking you can finally publish your data into the TG-rep.

In every case you need a TextGrid project ID and an RBAC session ID, both is provided in the TextGridLab. Using your TextGrid (or Shibboleth) account, you can log in into the TextGridLab and create TextGrid projects and check out their project ID and also get your session ID from the menu **Help > Authentication**.


Import & Configuration
----------------------

.. toctree::
   :maxdepth: 3

   ./import_and_configuration.rst


Sources
-------
See dhimport_sources_


Bugtracking
-----------
See dhimport_bugtracking_


License
-------
See LICENCE_


.. _LICENCE: https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/blob/master/COPYING.LESSER.txt
.. _dhimport_sources: https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest
.. _dhimport_bugtracking: https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/issues?label_name[]=tg-import
