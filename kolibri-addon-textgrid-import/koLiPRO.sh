#!/bin/bash
# Updates the progress bar for aggregation_import policy
# Parameters: 1. Percentage value

# Please set hotfolder used here.
HOTFOLDER=./folders/hotfolder-ota;
# Please set amount of PIDs that you get in one call (default=10).
PID_AMOUNT=10;

#
# Updates the progress bar.
#
update_progress_bar() {
  if [ $# -eq 1 ];
  then
    if [[ $1 == [0-9]* ]];
    then
      if [ $1 -ge 0 ];
      then
        if [ $1 -le 100 ];
        then
          local val=$1
          local max=100

          echo -n "["

          for j in $(seq $max);
          do
            if [ $j -lt $val ];
            then
              echo -n "="
            else
              if [ $j -eq $max ];
              then
                echo -n "]"
              else
                echo -n "."
              fi
            fi
          done

          echo -ne " "$val"%\r"

          if [ $val -eq $max ];
          then
            echo ""
          fi
        fi
      fi
    fi
  fi
}

# Get more things.
FILENUM=$(find $HOTFOLDER/data -type f | wc -l)
COMPLETE="\n";

#
# Handle class MetadataGenerator.
#
echo "Pass 1 - Extracting technical metadata"
STATE=$(grep -c "JHOVE technical" nohup.out)
REL=0
while [ $REL -lt 100 ];
do
	STATE=$(grep -c "JHOVE technical" nohup.out)
	REL=$(perl -w -e "use POSIX; print floor($STATE/$FILENUM*100), qq{\n}")
	update_progress_bar $REL
	sleep 1;
done;
echo -e $COMPLETE

#
# Handle class textgrid.TextgridMetadataProcessor.
#
echo -e "Pass 2 - Processing TextGrid metadata"
STATE=$(grep -c "Title set from filename" nohup.out)
REL=0
FILENUM=$(find $HOTFOLDER/data/* | wc -l)
while [ $REL -lt 100 ];
do
	STATE=$(grep -c "Title set from filename" nohup.out)
	REL=$(perl -w -e "use POSIX; print floor($STATE/$FILENUM*100), qq{\n}")
	update_progress_bar $REL
	sleep 1;
done;
echo -e $COMPLETE

#
# Handle class textgrid.CreateAggregations.
#
echo -e "Pass 3 - Creating aggregations"
STATE=$(grep -c "Aggregation file created" nohup.out)
REL=0
FILENUM=$(find $HOTFOLDER/data/* -type d | wc -l)
while [ $REL -lt 100 ];
do
	STATE=$(grep -c "Aggregation file created" nohup.out)
	REL=$(perl -w -e "use POSIX; print floor($STATE/$FILENUM*100), qq{\n}")
	update_progress_bar $REL
	sleep 1;
done;
echo -e $COMPLETE

#
# Handle class textgrid.RenameAndRewrite.
#
echo -e "Pass 4 - Renaming and rewriting"
STATE=$(grep -c "Rewriting links in TEI" nohup.out)
REL=0
FILENUM=$(find $HOTFOLDER/data -type f -name "*.xml" | wc -l)
while [ $REL -lt 100 ];
do
	STATE=$(grep -c "Rewriting links in TEI" nohup.out)
	REL=$(perl -w -e "use POSIX; print floor($STATE/$FILENUM*100), qq{\n}")
	update_progress_bar $REL
	sleep 1;
done;
echo -e $COMPLETE

#
# Handle class textgrid.GetPidsAndRewrite.
#
echo -e "Pass 5.1 - Creating PIDs"
STATE=$(grep -c "PID response string retrieved" nohup.out)
REL=0
FILENUM=$(find $HOTFOLDER/data/* | wc -l)
while [ $REL -lt 100 ];
do
	STATE=$(grep -c "PID response string retrieved" nohup.out)
	REL=$(perl -w -e "use POSIX; print floor($STATE/$FILENUM*$PID_AMOUNT*2*100), qq{\n}")
	update_progress_bar $REL
	sleep 1;
done;
echo -e $COMPLETE

echo -e "Pass 5.2 - Rewriting URIs to PIDs"
STATE=$(grep -c "PID added to metadata" nohup.out)
REL=0
FILENUM=$(find $HOTFOLDER/data/* | wc -l)
while [ $REL -lt 100 ];
do
	STATE=$(grep -c "PID added to metadata" nohup.out)
	REL=$(perl -w -e "use POSIX; print floor($STATE/$FILENUM*2*100), qq{\n}")
	update_progress_bar $REL
	sleep 1;
done;
echo -e $COMPLETE

#
# Handle class textgrid.SubmitFiles.
#
echo -e "Pass 6 - Submitting files"
STATE=$(grep -c "Submission completed" nohup.out)
REL=0
FILENUM=$(find $HOTFOLDER/data/* | wc -l)
while [ $REL -lt 100 ];
do
	STATE=$(grep -c "Submission completed" nohup.out)
	REL=$(perl -w -e "use POSIX; print floor($STATE/$FILENUM*100), qq{\n}")
	update_progress_bar $REL
	sleep 1;
done;
echo -e $COMPLETE

exit 0
