## [12.0.3](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v12.0.2...v12.0.3) (2025-02-19)


### Bug Fixes

* fix status response issue for publishWorldReadable ([72f7c1b](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/72f7c1bf73e5f3d146a10bbc5ec681583ce16b5d))
* increase commons digester version ([9a63316](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/9a6331622e76afc8eba2b82fa43758cdf4967455))
* increase cxf version ([561c2f2](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/561c2f27a97948fb88422a5859b5fa97bcac047e))
* return URI only for publishWorldReadable and publishSandboxData ([cc37539](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/cc375390217987de74aeec549f1af4aabcd6a9c8))

## [12.0.2](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v12.0.1...v12.0.2) (2024-09-04)


### Bug Fixes

* adapt modifyAndUpdate to new PidService ([e7c7a18](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/e7c7a189d09d1a5c31fe6c8cccbd2abcfd708dec))
* add logging ([a9dbe0a](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/a9dbe0a2f97f4c855716b0edf7a88b8c52d9bf44))
* add more logging to client creation ([c248162](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/c248162d7946ca60d7f247e6e412f42d9238628d))
* add singletons for pid and search clients in tgpublish service ([4a151aa](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/4a151aa7e4b1f81fc6a0d83fdee16b647dc36e40))
* add warning in case of PID metadata update failure ([e81a488](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/e81a4888da6a4496930bc8bf10077847450ef3be))
* change adduser and addgroup to update docker build ([30565a6](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/30565a6a6bf76ad2d42f52309bb52229d2953dbf))
* create instead of update?? ([c8cbfe6](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/c8cbfe6a1723f7f20c5e021d264883d2695e0c38))
* fix http status failure ([0b802af](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/0b802afc4f625b1194651ed9e04ee5439849304b))
* increase tgpid version ([1f883f5](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/1f883f5a85e48087bc2b044aac789db451cceef8))
* remove unused setter ([44af003](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/44af003692e81440de647fcb9b86494329aa4129))
* skip checking publish rights on dry-run ([ff7ad85](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/ff7ad85d16f4d1e0b5d0e1a43c1c3cef3f2110a8))
* update pid filesize and checksum metadata after tgcrud update ([99f8897](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/99f889782aae9465dd0ae21c87c18abbaad29099))

## [12.0.1](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v12.0.0...v12.0.1) (2024-07-10)


### Bug Fixes

* add more error checks ([61e643b](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/61e643b513add8980ec4d24349218f7c441906f0))
* add mpe check for item, too ([aa22b32](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/aa22b32bca630ecab6ef22f52fc9cb5198a4b9c5))
* add npe check for collection and edition tags ([4544415](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/45444155a0698fd504728b674a744870ad4c632e))
* add npe checks (again) ([54aac83](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/54aac83fb888421eb89b5bb27fa144c592d2d8e7))
* add warnings to warning log ([930bf76](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/930bf76d2b3cd4d3b4e6adde305661f4832175a4))
* arg, setting errors without logging again... hmnpf! ([6066e49](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/6066e49727560408b3d1eaf91e3a0ae43183aa64))
* define sophisticated exit rules :-) ([c8755cf](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/c8755cf1e06e935d5926a70e7e9036b2474205f9))
* fix edition and item checks ([7f479ed](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/7f479edcd0665fe5f7f5a6c29efe5b1ebc9979f4))
* fix warning log for publish check ([da27517](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/da27517c4888c9fcaff3a3ae5a0263466e62566c))
* little krams added ([4e5b6e7](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/4e5b6e7b951589e5ea510bf5ac6fb1c1affc8cc0))
* remove tests for tiffimagemetadataprocessor due to jhove config xml validation errors ([5352cac](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/5352cac4edd1787ca65d350ed3759bcd898cc03f))
* set errors ([c4f1d40](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/c4f1d404291191fb1b4a4c83b3fb8975491fb873))

# [12.0.0](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v11.0.0...v12.0.0) (2024-06-18)


### Bug Fixes

* add destUri to publish response (resp. copy) ([bc67d02](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/bc67d02d91cd084ef8e6bd7b0729fee291802d22))
* add revision to destUri ([c4412ab](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/c4412abf8d01f72e2cf4c56ecd6a5b278a1a31ff))
* deliver URI only on tgpublish publish() call ([ae213e2](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/ae213e24b9fd0e8e8dfaef7aaa7e18fc810a4ebe))
* set status to QUEUED right away! ([05877a1](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/05877a154fea1e4f0f990667ef14fc475cee30e2))
* take project ID from source object on copy if new revision shall be  created ([9b81b5d](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/9b81b5de9bf0bc419b25acfb3290cf76d2a7b992))
* use projet id instead of project name! ([2e932e3](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/2e932e3ccc5126adc0f1a709055cf1bc6a2e9f61))


### Features

* deliver URI only on tgpublish publish() call ([022f14b](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/022f14bbc25fb93cdb8086f9bed72f21217bf0f5))


### BREAKING CHANGES

* result of tgpublish.publish() is now the URI only, not "TG-publish started [URI]" anymore

# [11.0.0](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.7.3...v11.0.0) (2024-05-23)


### Bug Fixes

* ad jakarta servlet api ([4b86217](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/4b86217708603aaac33e1b5e65791a8b0d656a19))
* adapt deps ([005b46d](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/005b46d59656457b211d74714bdf60cde77fd5ba))
* adapt mvn imgaes to java 17 ([80f7960](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/80f796037b2aca0e36446c07cd03de9a32bb4567))
* add another mvn image ([e1b38c2](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/e1b38c2aa3400163d4344478c48690e6ccc2e4a9))
* add correct comment ([d931767](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/d9317674cbd8908d735d8601f97149fcd695b53d))
* add diasingestfeedback again ([9b0f01d](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/9b0f01d3b844f4db41f28ec3e79ee4132bfd8329))
* add editorconfig, and reformat xml files (though no test files!) ([e1d8738](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/e1d8738c116b45aef1e537344ba779193a572c22))
* add jackson core to dhpublish-service ([c5002c8](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/c5002c8369671c88ed6604c015b421c8638f8816))
* add jaxrs client ([06ed269](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/06ed269b4fa96467c3a3356b24ae1ef9f3c49b2f))
* add log4j for runtime ([14ed00e](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/14ed00ed816c7f4d0109abedc0537845b6ec916c))
* add log4j to tgpublishservice also ([60af44d](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/60af44d0a255566ff9bae47ee91e2a69dec59184))
* add missing microprofile dependency ([753672d](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/753672d3eda0ae0fb0a1a0ae896dd6fcdeebf9ae))
* add mp client ([974a0a0](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/974a0a08f5682e68366674838477907f5db55de0))
* add new mets and mods xsd to new xsd folder for usage of new xmlbeans package ([93cf3d9](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/93cf3d91d8566acabc38fe952ef20e88d64cf6ae))
* add process status QUEUED right away after returning from publish() ([33ee556](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/33ee55678b8115b3c97732d377466b303577f0fa))
* add project root .editorconfig file ([34c1f4c](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/34c1f4c276f96c0d3a24ef53c0107a9117eef0c6))
* add response to response queue as soon as publish starts to override old responses ([f3bd1c2](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/f3bd1c26027d4c14e4ea122bdf8991313f912217))
* add spring to runtime ([d02a74a](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/d02a74a8bdd691f989639507768025323d981e1c))
* add transport ws (again) for runtime ([4d05059](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/4d05059df830af57ac31f3b61f15b321287ecd37))
* correct user and group settings in dockerfile ([7a9cba9](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/7a9cba90a1dc482a0a977fca0d855b887c6d5039))
* debug on server side ([14fd9d5](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/14fd9d50ca4993af6653b6f5e780ef74280d8d2a))
* delete uri from response map! not only put the new non-empty one in ([63a8ef1](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/63a8ef15f4fda99f0a3f57dea31e773901461f10))
* dockerfile updated ([579de8a](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/579de8a901456fc79dec7d911c6b4b34227f9b63))
* finally merging bugfixes from main ([463e465](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/463e46504254aa2b8ed45b1c44d8fb051df4e363))
* fix docker setup ([dbc59c7](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/dbc59c78264af2dd7a64c725220d033808f274e0))
* fix http status ([1bf9faa](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/1bf9faacd15a38167604b00aa7dc2f23cfc54b07))
* fix some tests concerning JHOVE and METS object instantiation ([a8804cc](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/a8804cc62040700e1f93155bca0c9a2d21d6a5b3))
* fix temporary links to readme and portalconfig examples ([1425bae](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/1425bae68759a5632f94aa5062c5fffe1e92a23d))
* fix test for file sizes... :-D ([2fbbb70](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/2fbbb70f757dd1ec141449b03baff01cbe85a5e4))
* fix version typo ([d7abd61](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/d7abd6169b2614be387966ab194605d111c87d0b))
* increase deploy plugin version ([7521e4e](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/7521e4e2f22c71a12a93d48bc0715927856fb772))
* increase ES version to 7 ([afc6f8e](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/afc6f8e8adbca50d38f60dd743e3e2fa2776f425))
* increase jackson version ([80f68bc](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/80f68bc8710af30ca5e17931db9bd7c9abe09001))
* increase more versions, remove unigoe mets and jhove packages, put them into schemabeans ([9715f4e](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/9715f4eb84e63ef13b72b4628838550844b181bc))
* increase some package versions ([1902430](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/1902430f0bfb496fc0db9dbe9b85527756b5a9b2))
* increase spring version ([d9ce607](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/d9ce607abac3f547d3629406f8a82e2eb517531d))
* last publish tests complete ([84bc012](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/84bc012b04bdb826fa7dff4e850a49fb93d2e737))
* merge back and set release versions ([7bb3c1a](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/7bb3c1a9a6ca996000118092c98d8a039091cb6d))
* merging ([0268193](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/0268193ef7d187eda46aa66f85a456cdae01a1c1))
* merging ([0746246](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/0746246645ef76c235e163d198e614df6140dc4a))
* more synchronizing ([7c84893](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/7c8489390d17a21422c68f43b69d3e902a0e82a9))
* needed fix for [#244](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/issues/244)! please put in main and merge to develop! ([8b6db81](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/8b6db818bc6a4facaebe5f4fbec4a9d9d6d7bbf1))
* re-add webapps ([acf3c69](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/acf3c69ea86b8842d2efd74391871ff1d6a0021a))
* refactor getStatus conditionals, add tests ([55b34ba](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/55b34bab7e304ddc295d6bf75bd5e8409bfef49e))
* reformat xml files with spaces, no tabs! ([0d74595](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/0d7459565bc773cc78fa45710bacad0800627db2))
* remove deprecated method ([6e2259b](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/6e2259b4f0e1e883229a64539f897b1ebcc99427))
* remove uri from map if response is null ([ba40197](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/ba4019701b4fc74bd894e421a89ef2e5713eacc7))
* remove validating for some tests, add http microservices ([e24c21c](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/e24c21c6c21c1bb8a3479c42ee06ecf9b3198a95))
* repair jhove junit test ([93848fb](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/93848fb8365c85c78261c9f45b59c3c2b0f904f8))
* resolve tgpublish-service deps ([afca43a](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/afca43a9393dde920ebe556e38f69e4d4fb7ecfd))
* resolved http requests for dias ingest, access, and retrieval ([7186f89](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/7186f89cfabbcbf7d26383d09f2b88d0efb1f5dd))
* set new response in responseMap right away after calling publish(), not only in customData ([5b00fbf](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/5b00fbf54ae7cfb3b43dfbfe7804cc9930da398c))
* set objectListComplete to true in finish method ([8277e2f](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/8277e2fa45e132c6bea9b33767b3701542f6006b))
* set release versions ([8893f8f](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/8893f8f4f0f5337b9d094d0b04acfa5d775f992d))
* setting to error state if wrong content type ([8a7b8ba](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/8a7b8baf1aed70e072a89dcfccde4bf916f0a960))
* spring on runtime only? ([6c97fcb](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/6c97fcb24ab457de80e2567bcef0effe66ad5a9d))
* synchronize publish method ([0e2e05a](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/0e2e05a06f6f0471364c6d058c402051146f2aac))
* udate build name ([a56c098](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/a56c098b8fec383408201594aafc26489e2376da))
* update commons http client ([5944ff9](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/5944ff98d90586b79807818a777159cde66f9a8c))
* update jackson provider ([3cd548f](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/3cd548f235950e0af9e84cc048d75c104c8012b9))
* updated pom ([1554e3c](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/1554e3c95bb552466195fc19ba9fa2cd846e70c5))
* use another war plugin? ([ba032be](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/ba032bee702fe57d46a4ed39430e48ed40f53735))
* workaround for using cxf with tomcat 10? ([576be6e](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/576be6ed2fd04ccb7e9b1624a3c8627b44c02747))


### Features

* increase java and cxf versions to 17 and 4, update some deps, too ([2c003f0](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/2c003f066d0c7d2f96609e60734aa0dca289c22b))
* removal of some unused classes ([9c0dae4](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/9c0dae4e09190597ce7a5b90843a18ae85c9e867))


### BREAKING CHANGES

* Many functionalities will not be provided anymore, such as migration and DIAS
admin/retrieval packages

## [10.7.3](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.7.2...v10.7.3) (2024-03-15)

* another hotfix for tgpublish

## [10.7.2](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.7.1...v10.7.2) (2024-03-09)

* hotfix for tgpublish


### Bug Fixes

* merge ([084ff1b](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/084ff1b4b7238e9093646789732f730f2f8dc689))
* merge cherries ([f4cfcde](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/f4cfcdece8f478bfad29372669a1ca14f6c58654))

## [10.7.1](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.7.0...v10.7.1) (2023-06-29)


### Bug Fixes

* add crid to publish status response again ([5e106ac](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/5e106acc79cbe69a7810f9e946a61d6932eb13c0))

# [10.7.0](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.6.0...v10.7.0) (2023-06-08)


### Bug Fixes

* adapt logging to CR collection creation (fixes [#226](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/issues/226)) ([a601260](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/a60126007d83c981e4fe8a5f799cdb8a89aa274f))
* clear dependencies for modules in pom files... do some formatting ([933f5c4](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/933f5c41923eeca55c5e734f66cfa5f6cccff0f2))
* decrease spring again, for Spring v6 needs more than Java8 ([4ba0fdf](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/4ba0fdf73e0d186992018fa56c0f82adc0de3d4d))
* fix dh- and tg-publish deps ([d026f1c](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/d026f1c865750e529c3e159c47baca0c80f20eae))
* fix last things in poms, add SID to new search client queries! ([eb4ff01](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/eb4ff01e57b390e549a0e45ff5bc255733b2a5c7))
* remove (unused) log4j package ([0baed56](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/0baed56dda4732b3da2f8d110760234dfb5de0bb))
* remove loggggs and unused outdated commented out code ([629da97](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/629da9764aa7c608f9f1d9f7d432595199f781ad))
* remove opac libs, delete unused classes ([90116b7](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/90116b7047d0309a2beb21665fb288f2e1779ec0))
* remove REGISTERED info from dhpublish service implementation ([d94c2e5](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/d94c2e5293b7633a8a9610429dab86f6dafe0ce6))
* revert to old develop ([75cdcbe](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/75cdcbe919669456c776d6acc226af46a6780c01))
* set develop versions to SNAPSHOT again ([c4dc3ea](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/c4dc3ea224fce455775fbeb2e39aabfa5a9648d3))
* take care of spring versions ([5eb07fc](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/5eb07fc4f6eb8881ffe056fa555832db6154153b))


### Features

* **tgpublish:** using new tgsearch java clients, removing old ones from tgsearch repo ([b403add](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/b403addd367da06ac7cd9fe421f75bc383393dad))

# [10.6.0](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.5.3...v10.6.0) (2023-05-02)


### Bug Fixes

* add jackson annotations, increase jackson version ([0b622c0](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/0b622c08b979d54035e2b13d04b0fb74c69e4b70))
* comment out all the db content ([a7e532b](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/a7e532b9ba2467d6d63a99007bf821cb25c8ad61))
* decrease jackson version again ([0bd5635](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/0bd56359e6334b1abfdefe96e24d94f7f599f417))
* increase jackson version ([29e9d5b](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/29e9d5bbc1989f4f7dc46138580a055000f55efc))
* increase jena version ([9b4e841](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/9b4e84134ed5f26acf40b4f5c01589d574e8eb8f))
* increase log4j version ([184e7bd](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/184e7bddaa3367b77f634100bf3a0d36192ca86d))
* increase versions, using new common now ([8539724](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/85397243a07c779c0998a45d1ead78ceed75a5ae))
* remove annotation package from publish api module ([6ad22ec](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/6ad22ec88053fead7741f6af4ce6bca890e9fc9b))
* remove commented-out db module from pom ([61b24c6](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/61b24c63fa0f94365086a1216da2d58cf63f6b1a))
* remove deb build and from gitlab-ci ([df9ede9](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/df9ede96bdf0c156264457d7d1766884a4bb0ada))
* remove deb control folders ([687165d](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/687165d0f539f18aa2009cfca6c56c9d1af45b22))
* remove deprecated plugin flags ([34fd629](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/34fd629a99c9eea201a556c464448ffb907e421d))
* remove deps for mysql and hibernate, finally ([b8c66f0](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/b8c66f09542322dc4efbb4d77f2677b746f94d64))
* remove more unused and db related files ([86dcde4](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/86dcde4f4cbee08ff6002d268dfc8cc2a5d60a02))
* remove unneeded db related classes ([c0bfb0a](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/c0bfb0a7f1c99fa47fbffbc6e43c56ebf6927b64))
* remove useDatabase flag from classes and configs ([ef6f7a2](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/ef6f7a21081bf346724debbafa8c7429c17d4998))


### Features

* add new feature name ([967eaa8](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/967eaa8271308321177f1db394e67c5847671896))

## [10.5.3](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.5.2...v10.5.3) (2023-03-07)


### Bug Fixes

* add kolibri zip to gitignore ([ce7a899](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/ce7a8995f216e99daefd10eda1782d725d7abd09))
* uRI to filename application now can be sorted alphabetically ([dad5811](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/dad581160c3b7c1edd18b82d90bb7a2885e939b4))

## [10.5.2](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.5.1...v10.5.2) (2022-12-19)


### Bug Fixes

* do dry-run publish on dry-run! fixes [#220](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/issues/220) ([8ecc497](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/8ecc49792bf7308aa1a64888a4332eb8170b28ea))

## [10.5.1](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.5.0...v10.5.1) (2022-09-16)


### Bug Fixes

* return correct status RUNING to ERROR if error state ([f5b975c](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/f5b975c1c8db33e51052ce4940c38106c15207d7))

# [10.5.0](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.4.1...v10.5.0) (2022-09-16)


### Bug Fixes

* add junit to deps ([00bc6e2](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/00bc6e273244df061b2b3c650b590145b366b6bc))
* add missing template file ([a19e0ed](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/a19e0ed545d230321e0ffaaa3c956aefac6f0afb))
* add test for omitting the SIDs from status messages ([4cb61e5](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/4cb61e5cd8479bc9238d5990fb163417cf8c318a))
* add version to kolibri-cli.jar, too ([516bdfd](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/516bdfd431ceddb5dfdf1b0d0a3611764a9be3c3))
* add version to start scripts again ([696dcae](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/696dcaedd912718518b105de11c7b959edd30b6a))
* take over portalconfig worldReadable config fix ([#215](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/issues/215)) ([49dcee3](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/49dcee3bc05b82ecdc845d74f31af9485c8c8387))
* update policies.xml ([98d6739](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/98d67391db0896cac2f53ec4fb85dde9422b43a8))
* use omitting SIDs from status messages in util class ([c762be8](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/c762be8c204429ea74edfc7b2d534095d4b866ac))


### Features

* create collections from root folders in aggregation_import policy ([2f0391a](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/2f0391a45bc5448b88f5b3a9bbd9b29b41a40dfd))

## [10.4.1](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.4.0...v10.4.1) (2022-08-17)


### Bug Fixes

* add pom version to package json asset creation ([5ea23d8](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/5ea23d8a15b51c2e951d74956b8bc5da6fd428dc))
* add version to package.json via ${} ([1b8d575](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/1b8d575c1960bc60ce8ffcde032d92c64820b3f7))
* increase default retry values for registering DOIs ([97cafc0](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/97cafc0409efbb9e15e8825c67661f446d826b54))

# [10.4.0](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.3.0...v10.4.0) (2022-08-10)


### Bug Fixes

* add dry-run for testing ([15ec453](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/15ec453acd453a1e3a5a1315dadba694643dd7e7))
* add label to url ([f3be79d](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/f3be79d51f4fcfa2489ff01669594a8a784cb5f1))
* add label to url (again) ([7814ff6](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/7814ff6f23e57862c4bd5e8b6768bacef4847b81))
* arg! how do i add an asset URL? ([c8a6f9e](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/c8a6f9e65773608be88b626fa838efd353d9dd3c))
* remove dry-run from semantic release run ([94bf361](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/94bf36146614692df9aa14f36bd59fc53d1104a0))
* testing asset add ([8befcd1](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/8befcd144571f3e26db4d84a1aef4bb0c1cf0ed4))
* url only? ([e0756e3](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/e0756e335cd00f53fb525385b21f5fe81a04dbb7))
* using path for url? ([587a461](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/587a4615a275eac0ed2419fb102dc0a481e3fbaf))
* why not an url setting? ([3737a3a](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/3737a3a390f78d6fa4bb15bdf65a62563a579722))


### Features

* increase semantic-release-gitlab version ([da41e29](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/da41e29b0002ec77a33a1605b403c47f2a8c9ed1))

# [10.3.0](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.2.3...v10.3.0) (2022-08-10)


### Bug Fixes

* generate pom with pom profile now ([44e1c04](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/44e1c04e730d170ebefcaf6856da6abc7f1b8fa8))


### Features

* add link to kolibri-cli as asset ([635f3a1](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/635f3a14bbe7b23063e64462dbb2683d0de42259))

## [10.2.3](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.2.2...v10.2.3) (2022-08-09)


### Bug Fixes

* put assets to release page, artifacts to ci/cd ([7023d8c](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/7023d8cc89ff775fb909b09a67f089a176afb26b))

## [10.2.2](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.2.1...v10.2.2) (2022-08-09)


### Bug Fixes

* add assets to gitlab ci and package.json files ([fa5032c](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/fa5032c88e7b6e64fed08907617977c80bdb1043))
* remove all the filtering of documentation, we will use gitlab assets for now ([9c29e13](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/9c29e133b7adc67d3e19a05a689ef37feda0cc64))
* remove build branch config from pom file ([cad9c24](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/cad9c241709f7051c4ebe0b035c21cc6971a689d))

## [10.2.1](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/compare/v10.2.0...v10.2.1) (2022-08-08)


### Bug Fixes

* add to CHANGELOG ([3e045d3](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/3e045d30475413fb2bded030bac50a4946440b07))
* increase SNAPSHOT POM version ([c56d5b2](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/c56d5b25e9c0381d359324fcdab8be62fea23663))
* remove dry-run from semantic release workflow ([9e2ba8b](https://gitlab.gwdg.de/dariah-de/kopal-library-of-retrieval-and-ingest/commit/9e2ba8b5ec94f1296e10a397fbad2583934ed044))

## 10.2.0

* Add new CI workflow including release deployment
* Please see <https://wiki.de.dariah.eu/pages/viewpage.action?spaceKey=DARIAH3&title=DARIAH-DE+Release+Management#DARIAHDEReleaseManagement-Gitlabflow/Gitflow(develop,main,featurebranchesundtags)>

## 10.1.0-TG

* Add CycloneDX to POM (fixxes #207)
* See if TG-publish#publish() publishes to the sandbox (fixes #204)
* TG-import docs point to 404 (fixes #201)
* Ignore large file tests from test suite (fixes #208)
* Errors publishing files from sandbox (fixes #209)
* Check title and format for content during validation (fixes #205)
* Add CI/CD to project (fixes #6)
* Remove unused and unmaintained Maven modules (fixes #4)
* Change CI/CD from `Jenkinsfile` to `.gitlab-ci.yml` ()

## 10.0.0-TG-RELEASE

* ElasticSearch6 update

## 9.4.0-TG

* MD5 checksums and file sizes are added to the PID metadata if publishing from TG-lab (fixes #31631)
* Fix ElasticSearch client close bug (fixes #31567)
* Update LinkRewriter to new release version 0.4.1-RELEASE (fixes #10652)
* PID resolver is now configured in TG-import class, not in user configuration anymore (fixes #14344)
* Implement PublishCheck for TG-import (fixes #31343)
